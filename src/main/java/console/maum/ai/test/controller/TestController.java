package console.maum.ai.test.controller;

import console.maum.ai.common.util.MailSender;
import console.maum.ai.member.service.MemberService;
import console.maum.ai.payment.model.PaymentVo;
import console.maum.ai.payment.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("/test")
public class TestController {

    @Autowired
    MemberService memberService;

    @Autowired
    PaymentService paymentService;

    @Autowired
    private MailSender mailSender;

    @RequestMapping("/page")
    public String testPage() {
        return "/kr/test/test.pg";
    }

    @RequestMapping("/usage")
    public void usageTest(int userNo) throws Exception {
        System.out.println(memberService.selectUsage(userNo));
    }

    @RequestMapping("/commonBilling")
    public void commonBilling() {

    }


    @RequestMapping(value = "/sendMails", method = RequestMethod.GET)
    @ResponseBody
    public String sendMails() {
        try {
            mailSender.batchMailSend();

        } catch (Exception e) {
            log.info("Batch schedule(batchMailSend) Exception!!");
        }

        return "send mails.... >> check log";
    }

    @RequestMapping(value = "/payment", method = RequestMethod.GET)
    @ResponseBody
    public String payment() {
        List<PaymentVo> RegularList = paymentService.selectRegularList();

        for(PaymentVo paymentVo : RegularList) {
            log.debug(paymentVo.toString());
            try {

                String paymentMethodStr = paymentService.getPaymentMethod(paymentVo.getUserNo());

                if( "Card".equalsIgnoreCase(paymentMethodStr) ) {
                    paymentService.routineBill(paymentVo);
                } else if ( "Cash".equalsIgnoreCase(paymentMethodStr) ) {
                    /* N/A */
                } else if ( "PayPal".equalsIgnoreCase(paymentMethodStr) ) {
                    /* N/A */
                } else {
                    /* N/A */
                }

            } catch(Exception e) {
                log.error(e.getMessage());
            }
        }

        return "/test/payment";
    }
}
