package console.maum.ai.superResolution.controller;

import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.superResolution.service.SuperResolutionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


//데이터를 주고 받겠다는 선언.
@RestController
public class SuperResolutionApiController {

    @Autowired //의존 자동 주입(스프링이 알아서 의존 객체를 찾아서 주입해준다)
    private SuperResolutionService superResolutionService;

    @RequestMapping(value = "/api/getSuperResolution")
    @ResponseBody // view 를 통해서 출력되지 않고, HTTP Response Body 에 직접 쓰여지게 됨.
    public ResponseEntity<byte[]> getSuperResolutionImage(@RequestParam(value = "file") MultipartFile imageFile, HttpServletRequest request){
        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getId();

        //API 요청을 서비스로 넘긴다
      return superResolutionService.getApiSuperResolution(apiId, apiKey,imageFile , PropertyUtil.getUploadPath()+"/superResolution/", savedId);

    }
}
