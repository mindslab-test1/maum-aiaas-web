package console.maum.ai.superResolution.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/*
 * 화면 Controller
 */

//MVC 주요 어노테이션 : @Controller, @RequestMapping, @RequestParam, @ModelAttribute, @SessionAttributes, @RequestPart, @CommandMap, @ControllerAdvice
@Controller
@RequestMapping(value = "/cloudApi/superResolution") // 클라이언트가 URL을 요청하면 특정 controller클래스나 메소드로 연결시키는 역할, superResolution 에 대한 http request url에 대
public class SuperResolutionController {

	private static final Logger logger = LoggerFactory.getLogger(SuperResolutionController.class);//디버그 용

	/**
	 * superResolution Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krSuperResolutionMain")
	public String krSuperResolutionMain(HttpServletRequest request, Model model) { //두개의 매개변수를 받아서 문자열로 반환하는 메소드
		logger.info("Welcome krSuperResolutionMain!");

		//세션은 웹 서버 쪽의 웹 컨테이너 상태를 유지하기 위한 정보를 저장(보안에 유리/서버에 부담) 웹서버 > 웹 컨테이너 > 세션(브라우저 하나당 한 개 생성)
		//HttpSession객체 매서드: setAttribute(), getAttribute(), removeAttribute(), getAttributeNames(), invalidate(), setMaxInactiveInterval(int초), getId(). getCreationTime(), getLastAccessedTime(),

		HttpSession httpSession = request.getSession(true);//세션 생성, 기존 session이 있으면 기존 session 객체를, 없으면 새로 생성하여 반환한다. getSession(): javax,servlet,http,HttpSession타입의 객체를 리턴
		httpSession.setAttribute("selectMenu", "superResolutionMain");//setAttribute("세션이름", "값"): 세션이름에 값을 할당한다.
		httpSession.setAttribute("menuName", "시각");
		httpSession.setAttribute("subMenuName", "Enhanced Super Resolution");
		httpSession.setAttribute("menuGrpName", "시각");

		model.addAttribute("lang", "ko");//value객체를 name 이름으로 추가한다.
		String returnUri = "/kr/superResolution/superResolutionMain.pg";
		
		return returnUri;
	}
	/**
	 * superResolution Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enSuperResolutionMain")
	public String enSuperResolutionMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enSuperResolutionMain!");

		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "superResolutionMain");
		httpSession.setAttribute("menuName", "Vision");
		httpSession.setAttribute("subMenuName", "Enhanced Super Resolution");
		httpSession.setAttribute("menuGrpName", "Vision");

		model.addAttribute("lang", "en");
		String returnUri = "/en/superResolution/superResolutionMain.pg";

		return returnUri;
	}

}