package console.maum.ai.poseRecog.controller;

import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.poseRecog.service.poseRecognitionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

@RestController
public class poseRecognitionApiController {
    private final static Logger logger = LoggerFactory.getLogger(poseRecognitionApiController.class);

    @Autowired
    private poseRecognitionService poseRecognitionService;

    @PostMapping(value="/api/poseRecog/poseApi")
    public ResponseEntity<byte[]> poseRecogApi(@RequestParam(value = "file")MultipartFile imageFile, HttpServletRequest request){

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getId();


        return poseRecognitionService.runPoseRecog(apiId, apiKey, imageFile, PropertyUtil.getUploadPath(), savedId);
    }
}
