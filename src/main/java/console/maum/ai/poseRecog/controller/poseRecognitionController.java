package console.maum.ai.poseRecog.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import console.maum.ai.textRemoval.controller.TextRemovalController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/*
 * 화면 Controller
 */

@Controller
@RequestMapping(value = "/cloudApi/poseRecog")

public class poseRecognitionController {


    private static final Logger logger = LoggerFactory.getLogger(poseRecognitionController.class);

    /**
     * poseRecog Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/krPoseRecogMain")
    public String krPoseRecogMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krPoseRecogMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "poseRecogMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "인물 포즈 인식");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/poseRecog/poseRecogMain.pg";

        return returnUri;
    }


    @RequestMapping(value = "/enPoseRecogMain")
    public String enPoseRecogMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enPoseRecogMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "poseRecogMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Pose Recognition");
        httpSession.setAttribute("menuGrpName", "Vision");

        model.addAttribute("lang", "en");
        String returnUri = "/en/poseRecog/poseRecogMain.pg";

        return returnUri;
    }

}
