package console.maum.ai.login.controller;

import com.google.gson.Gson;
import console.maum.ai.admin.apikey.dao.ApiKeyDao;
import console.maum.ai.admin.apikey.model.ApiKeyVo;
import console.maum.ai.login.model.RedirectStateVo;
import console.maum.ai.login.service.LoginService;
import console.maum.ai.member.constant.UserStatus;
import console.maum.ai.member.dao.MemberDao;
import console.maum.ai.member.model.MemberDto;
import console.maum.ai.member.model.MemberForm;
import console.maum.ai.member.model.MemberSecurityVo;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.member.service.MemberDetailsService;
import console.maum.ai.member.service.MemberService;
import console.maum.ai.payment.model.BillingVo;
import console.maum.ai.payment.model.PaymentVo;
import console.maum.ai.payment.service.PaymentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.connect.Connection;
import org.springframework.social.google.api.Google;
import org.springframework.social.google.api.impl.GoogleTemplate;
import org.springframework.social.google.api.plus.Person;
import org.springframework.social.google.api.plus.PlusOperations;
import org.springframework.social.google.connect.GoogleConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.GrantType;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",
		"classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@Controller
@RequestMapping(value = "/login")
public class LoginController {
	
	/* GoogleLogin */
	@Autowired
	private GoogleConnectionFactory googleConnectionFactory;
	@Autowired
	private LoginController loginController;
//	@Autowired
//	private OAuth2Parameters googleOAuth2Parameters;
	
    @Autowired
    private MemberDetailsService memberDetailsService;	
    
	private static int TIME = 60 * 60; // 1시간

	@Autowired
	private MemberService memberService;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private MemberDao memberDao;

	@Autowired
	private LoginService loginService;

	@Autowired
	private ApiKeyDao apiDao;

	@Value("${hq.URL}")
	private String hqUrl;
	@Value("${hq.client_id}")
	private String hqClientId;
	@Value("${hq.redirect_uri}")
	private String hqRedirectUrl;


	@Value("${datatool.url}")
	private String dataEditorTool;
	@Value("${mvp_maker.url}")
	private String mUrl_MVPMaker;
	@Value("${url.domain}")
	private String mDomain;
	@Value("${paypal.PAYPAL_CLIENT_ID}")
    private String paypalClientId;
	@Value("${paypal.PAYPAL_FREE_PLAN_ID}")
    private String paypalFreePlanId;
	@Value("${paypal.PAYPAL_INSTANT_PLAN_ID}")
	private String paypalInstantPlanId;
	@Value("${hq.URL}")
	private String sso_url;
	@Value("${hq.client_id}")
	private String client_id;
	@Value("${hq.redirect_uri}")
	private String redirect_uri;
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	Gson gson = new Gson();
	public final String loginBtnUrl = "/login/oauthLogin";



	/**
	 * login
//	 * @param model
	 * @return
	 */
	// [아래 주석 문제 없을 시 삭제] 기존 구글 로그인 -- 2020.03.27 YGE 작성
/*	@RequestMapping(value = "/loginForm")
	public String login(@RequestParam(value="error", required=false) String error, HttpServletRequest request, HttpServletResponse response, Model model) {


		logger.info("login()");

		//국문/영문 분기 처리
		String paramLang = request.getParameter("lang");

		if(!"".equals(paramLang) && paramLang != null) {
			response.addCookie(new Cookie("lang",paramLang));
		}else {

			Cookie[] getCookie = request.getCookies();
			if(getCookie != null){
				for(int i=0; i<getCookie.length; i++){
					Cookie c = getCookie[i];
					String name = c.getName(); // 쿠키 이름 가져오기
					String value = c.getValue(); // 쿠키 값 가져오기

					if("lang".equals(name) && !"".equals(value) ) {
						paramLang = c.getValue();
					}
				}
			}
		}

		logger.info("loginForm paramLang : " + paramLang);

		String lang = request.getLocale().toString();
		if(paramLang != null) {
			lang = paramLang;
		}

		// 구글code 발행
		OAuth2Parameters googleOAuth2Parameters = new OAuth2Parameters();
		googleOAuth2Parameters.setRedirectUri(mDomain + "/login/oauth2callback.do");
		googleOAuth2Parameters.setScope("https://www.googleapis.com/auth/userinfo.email");

		OAuth2Operations oauthOperations = googleConnectionFactory.getOAuthOperations();
		String url = oauthOperations.buildAuthorizeUrl(GrantType.AUTHORIZATION_CODE, googleOAuth2Parameters);

		//특정페이지 이동 쿠키 발행
		String loginmenu = request.getParameter("loginmenu");
//		logger.info("loginmenu : " + loginmenu);
		if(!"".equals(loginmenu)) {
			response.addCookie(new Cookie("loginmenu",loginmenu));
		}else {
			response.addCookie(new Cookie("loginmenu",null));
		}

		model.addAttribute("google_url", url);
		model.addAttribute("mvp_maker_url", mUrl_MVPMaker);
		model.addAttribute("error", error);
		model.addAttribute("lang", paramLang);
		model.addAttribute("errormsg", request.getParameter("errormsg"));
		model.addAttribute("maumDataLink", dataEditorTool);

//		logger.info("paramLang  : " + paramLang);
//		logger.info("lang  : " + lang);

		String returnUri = "";

//		logger.info("LoginController login lang ::::::  "+ lang);

		if("ko_KR".equals(lang) || "ko".equals(lang)){
			returnUri = "noLayout/kr/login/loginForm.pg";
		}else{
			returnUri = "noLayout/en/login/loginForm.pg";
		}

		return returnUri;
	}*/

	// [아래 주석 문제 없을 시 삭제] 기존 SSO 로그인, 현재 "/" 으로 로그인 url 바뀜 -- 2020.05.26 YGE 작성
	/*@RequestMapping(value = "/loginForm")
	public String loginForm(@RequestParam(value="error", required=false) String error, HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("loginForm()");

		String paramLang = loginService.setLang(request, response);

		logger.info(paramLang);

		//특정페이지 이동 쿠키 발행
		String loginmenu = request.getParameter("loginmenu");
		if(!"".equals(loginmenu)) {
			response.addCookie(new Cookie("loginmenu",loginmenu));
		}else {
			response.addCookie(new Cookie("loginmenu",null));
		}

		model.addAttribute("google_url", loginBtnUrl);
		model.addAttribute("mvp_maker_url", mUrl_MVPMaker);
		model.addAttribute("error", error);
		model.addAttribute("lang", paramLang);
		model.addAttribute("errormsg", request.getParameter("errormsg"));
		model.addAttribute("maumDataLink", dataEditorTool);

		String redirectUrl = "noLayout/"+ paramLang +"/login/loginForm.pg";

		return redirectUrl;
	}*/


	/*
	 * (로그인 화면에서 로그인 버튼 눌렀을 때) SSO 로그인 요청 -- 2020.03.27 YGE
	 * 로그인 후에 특정 페이지로 전환되도록 기능 추가 -- 2020.05.29 BELSNAKE
	 */
	@RequestMapping(value="/oauthLogin")
	public String oauthLogin(HttpServletRequest request,
							 HttpServletResponse response,
							 @RequestParam("targetUrl") String targetUrl){
		String paramLang = loginService.setLang(request, response);
		String state =  UUID.randomUUID().toString();

		RedirectStateVo redirectStateVo =  new RedirectStateVo();
		redirectStateVo.setState(state);
		redirectStateVo.setLang(paramLang);
		redirectStateVo.setTargetUrl(targetUrl);

		loginService.insertRedirectState(redirectStateVo);

		String params = "?response_type=code&client_id=" + hqClientId
				+ "&redirect_uri=" + hqRedirectUrl
				+ "&scope=read_profile&state=" + state;
		logger.info(params);
		return "redirect:" + hqUrl + "/hq/oauth/authorize" + params;
	}


	/**
	 * accessDenied
	 * @param model
	 * @return
	 */
	// [확인 후 수정 or 삭제] google 로그인 일때만 필요한건지 모르겠음... -- 2020.03.27 YGE 작성
	@RequestMapping(value = "/accessDenied")
	public String accessDenied(HttpServletRequest request, HttpServletResponse response, Model model) {
		logger.info("accessDenied() ... ");
		
		//국문/영문 분기 처리
		String lang = loginService.setLang(request, response);
/*

		if(!"".equals(paramLang) && paramLang != null) {
			response.addCookie(new Cookie("lang",paramLang));
		}else {

			Cookie[] getCookie = request.getCookies();
			if(getCookie != null){
				for(int i=0; i<getCookie.length; i++){
					Cookie c = getCookie[i];
					String name = c.getName(); // 쿠키 이름 가져오기
					String value = c.getValue(); // 쿠키 값 가져오기

					if("lang".equals(name) && !"".equals(value) ) {
						paramLang = c.getValue();
					}
				}
			}
		}
		logger.info("accessDenied paramLang : " + paramLang);

		String lang = request.getLocale().toString();
		if(paramLang != null) {
			lang = paramLang;
		}
*/

		String returnPage ="";
		HttpSession httpSession = request.getSession(true);		
		
		if(httpSession != null) {
			MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
			if(memberVo != null) {
				String insUserId = memberVo.getId();
				
				if("".equals(insUserId)) {
					returnPage = "noLayout/login/loginForm.pg";
				}else {
					if("ko_KR".equals(lang) || "ko".equals(lang)){
						returnPage = "/main/krMainHome";
					}else{
						returnPage = "/main/enMainHome";
					}
				}				
			}else {
				returnPage = "noLayout/login/loginForm.pg";
			}			
		}else {
			returnPage = "noLayout/login/loginForm.pg";
		}
		
		
		return returnPage;
	}
	
	// 구글 Callback호출 메소드
	@RequestMapping(value = "/oauth2callback.do", method = { RequestMethod.GET, RequestMethod.POST })
	public String googleCallback(HttpServletRequest request,
								 HttpServletResponse response,
								 RedirectAttributes redirectAttributes,
								 Model model, @RequestParam String code) throws IOException, Exception {
		logger.info("googleCallback");
		Gson gson = new Gson();

		OAuth2Operations oauthOperations = googleConnectionFactory.getOAuthOperations();
		
		AccessGrant accessGrant = null;
		try {
			OAuth2Parameters googleOAuth2Parameters = new OAuth2Parameters();
			googleOAuth2Parameters.setRedirectUri(mDomain + "/login/oauth2callback.do");
			googleOAuth2Parameters.setScope("https://www.googleapis.com/auth/userinfo.email");

			accessGrant = oauthOperations.exchangeForAccess(code, googleOAuth2Parameters.getRedirectUri(), null);
			
		}catch(HttpClientErrorException e) {
			
			logger.info("HttpClientErrorException  : {}", e.getMessage());
			
			throw new UsernameNotFoundException("");
		}
		
		
		String accessToken = accessGrant.getAccessToken();
		Long expireTime = accessGrant.getExpireTime();
		
		logger.info("accessToken  : {}", accessToken);
		logger.info("expireTime  : {}", expireTime);
		
		if (expireTime != null && expireTime < System.currentTimeMillis()) {
			accessToken = accessGrant.getRefreshToken();
			logger.info("accessToken is expired. refresh token = {}", accessToken);
		}

		Connection<Google> connection = googleConnectionFactory.createConnection(accessGrant);

		Google google = connection == null ? new GoogleTemplate(accessToken) : connection.getApi();

		PlusOperations plusOperations = google.plusOperations();
		Person person = plusOperations.getGoogleProfile();

		/*
		logger.info("plusOperations  : " + plusOperations);
		logger.info("person  : " + person);
		logger.info("person getAboutMe  : " + person.getAboutMe());
		logger.info("person getDisplayName  : " + person.getDisplayName().replaceAll(" ", ""));
		logger.info("person getAccountEmail  : " + person.getAccountEmail());
		logger.info("person getGender  : " + person.getGender());
		logger.info("person getFamilyName  : " + person.getFamilyName());
		logger.info("person getEmails  : " + person.getEmails());
		logger.info("person getEmailAddresses  : " + person.getEmailAddresses());

		logger.info("code  : " + code);
		*/

		String email = person.getAccountEmail();
		if(email == null){
			for (Map.Entry<String, String> entry : person.getEmails().entrySet()) {
				if (entry.getValue().equals("account") || entry.getValue().equals("ACCOUNT")) {
					email=  entry.getKey();
					logger.info("email : " +email);
				}
			}
		}

		MemberForm memberForm = new MemberForm();

		memberForm.setEmail(email);
		if(person.getDisplayName() != null && !"".equals(person.getDisplayName().replaceAll(" ", ""))) {
			memberForm.setName(person.getDisplayName().replaceAll(" ", ""));
		}else{
			//display이름이 없는 경우 이메일 주소  id로 설정!! (결제시 사용자 이름 필수) 2019-06-21
			memberForm.setName(email.substring(0,email.indexOf("@")));
		}
		
		MemberSecurityVo user = (MemberSecurityVo) memberDetailsService.loadUserByUsername(memberForm);		
		logger.info("user.getAuthorities()  : {}", user.getAuthorities());
		logger.info("user.getPrivacyAgree()  : {}", user.getPrivacyAgree());
		
		//특정 페이지 이동처리 (쿠키)
		String redirectUri = "";
		String loginMenuUri = "";
		String agree = "";
		String paramLang = "";
		Cookie[] getCookie = request.getCookies();

		if(getCookie != null){

			for(int i=0; i<getCookie.length; i++){
	
				Cookie c = getCookie[i];
		
				String name = c.getName(); // 쿠키 이름 가져오기		
				String value = c.getValue(); // 쿠키 값 가져오기
				
				logger.info("name : {}", name);
				logger.info("value : {}", value);
				
				if("loginmenu".equals(name) && !"".equals(value) ) {
					loginMenuUri = "redirect:"+c.getValue();
					response.addCookie(new Cookie("loginmenu", null));
				}

				
				if("agree".equals(name) && !"".equals(value) ) {
					agree = c.getValue();
				}
				
				if("lang".equals(name) && !"".equals(value) ) {
					paramLang = c.getValue();
				}				
			}
		}		
		
		String lang = request.getLocale().toString();
		if(paramLang != null && !"".equals(paramLang)) {
			lang = paramLang;
		}		
//		logger.info("LoginController login lang ::::::  {}", lang);

		HttpSession session = null;
		MemberVo memberVo = new MemberVo();

		try{
			memberVo = memberService.getLogInMemberDetail(email);
			logger.info("memberVo  : {}", gson.toJson(memberVo));
		}catch (Exception e){
			e.printStackTrace();
		}


		if("ko_KR".equals(lang) || "ko".equals(lang)){
			//개인정보 동의 여부 체크 (국문)
			// modified by LYJ - 2019. 10. 21 - getProduct --> getStatus
			if(user.getPrivacyAgree() == 0 || (memberVo.getStatus() != UserStatus.INIT && memberVo.getStatus() != UserStatus.FREE && memberVo.getStatus() != UserStatus.SUBSCRIBED && memberVo.getStatus() != UserStatus.UNSUBSCRIBING)) {
				logger.info("user email : {}",user.getEmail());
				if(memberVo != null){
					session = request.getSession();
					session.setAttribute("accessUser", memberVo);
				}
				return "redirect:/login/krSignupForm";
			}

			//특정 페이지로 이동 처리
			if(loginMenuUri.equals("")){
				redirectUri = "redirect:/main/krMainHome";
			}else{
				redirectUri = loginMenuUri;
			}

		}else {
			//개인정보 동의 여부 체크 (영문)
			//[영문] 신규 사용자 로그인시 Product 조건 체크 생략 -- 한시적 사용  (2019.07.25 ~ 2019.08.25) : start

			if(user.getPrivacyAgree() == 0 || (memberVo.getStatus() != UserStatus.INIT && memberVo.getStatus() != UserStatus.FREE && memberVo.getStatus() != UserStatus.SUBSCRIBED && memberVo.getStatus() != UserStatus.UNSUBSCRIBING)) {

			//[영문] 신규 사용자 로그인시  Product 조건 체크 생략  -- 한시적 사용  (2019.07.25 ~ 2019.08.25) : end
			//if(user.getPrivacyAgree() == 0) {
				logger.info("user email : {}",user.getEmail());
				if(memberVo != null){
					session = request.getSession();
					session.setAttribute("accessUser", memberVo);
				}
				return "redirect:/login/enSignupForm";
			}

			//특정 페이지로 이동 처리
			if(loginMenuUri.equals("")){
				redirectUri = "redirect:/main/enMainHome";
			}else{
				redirectUri = loginMenuUri;
			}
		}
		
		/*
		if("ko_KR".equals(lang) || "ko".equals(lang)){
			//개인정보 동의 여부 체크 (국문)
			if(user.getPrivacyAgree() == 0 || user.getProduct() != 3) {
				logger.info("user email : {}",user.getEmail());
				model.addAttribute("email",user.getEmail());
				return "redirect:/login/krSignupForm";
			}
			redirectUri = "redirect:/main/krMainHome";
		}else {
			logger.info("lang : " + lang);
			//개인정보 동의 여부 체크 (영문)
			if(user.getPrivacyAgree() == 0 && !"true".equals(agree)) {
				
				String errormsg = "Terms and Conditions, Check please!!";
				
				return "redirect:/login/enSignupForm?errormsg="+URLEncoder.encode(errormsg, "UTF-8");
			}else if("true".equals(agree)) {
				//개인정보 동의 여부 업데이트
				logger.info("agree : " + agree);
				memberService.updatePrivacyAgreeMember(memberForm);
			}
			redirectUri = "redirect:/main/enMainHome";
		}
		*/
		
		SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(email, "", user.getAuthorities()));

		if(memberVo != null){
			session = request.getSession();			
			session.setAttribute("accessUser", memberVo);
		}


		/* 차후에 보안 적용된 코드로 변환 예정 (by belsnake) */
		redirectAttributes.addAttribute("UserInfo", memberVo.getEmail());
		String applicationUrl = (String) request.getSession().getAttribute("ApplicationUrl");
		if(applicationUrl != null && applicationUrl.length() > 0) {
			redirectUri = "redirect:" + applicationUrl;
			request.getSession().invalidate();
		}
		request.getSession().removeAttribute("ApplicationUrl");
		logger.info("================================================> applicationUrl = {}", applicationUrl);

		return redirectUri;
	}
	
	/**
	 * signup (영문)
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enSignupForm")
	public String enSignupForm(@RequestParam(value="error", required=false) String error, HttpServletRequest request, HttpServletResponse response, Model model) {
		
		logger.info("LoginController/enSignupForm()");

		MemberVo memberVo;
		HttpSession session = request.getSession(true);
		memberVo = (MemberVo) session.getAttribute("accessUser");
		logger.info("Session accessUser : {}", memberVo);

		String email = memberVo.getEmail();

		//특정페이지 이동 쿠키 발행
		String loginmenu = request.getParameter("loginmenu");
//		logger.info("loginmenu : " + loginmenu);		
		if(!"".equals(loginmenu)) {
			response.addCookie(new Cookie("loginmenu",loginmenu));
			//response.addCookie(new Cookie("agree","true"));
		}else {
			response.addCookie(new Cookie("loginmenu",null));
			//response.addCookie(new Cookie("agree",null));
		}

		model.addAttribute("error", error);
		model.addAttribute("errormsg", request.getParameter("errormsg"));

        MemberDto memberDto = new MemberDto();
        try {
            memberDto = memberService.getMemberDetail(email);
            
            //사용자정보 누락시 예외처리 - unongko 
            if(memberDto == null) {
    			model.addAttribute("errormsg", "User information does not exist.");
    			return "redirect:/?lang=en";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("****{}*****.getDetail() Dto ::: {}  ", memberDto.getPrivacyAgree(), gson.toJson(memberDto));

        model.addAttribute("createFlag", memberDto.getCreateFlag());
		
		return "noLayout/en/login/signupForm.pg";
	}	
	
	/**
	 * signup (국문)
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krSignupForm")
	public String krSignupForm(@RequestParam(value="error", required=false) String error, HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("LoginController/krSignupForm()");

		MemberVo memberVo;
        HttpSession session = request.getSession(true);
		memberVo = (MemberVo) session.getAttribute("accessUser");
		logger.info("Session accessUser : {}", memberVo);

        String email = memberVo.getEmail();

		//특정페이지 이동 쿠키 발행
		String loginmenu = request.getParameter("loginmenu");
//		logger.info("loginmenu : " + loginmenu);		
		if(!"".equals(loginmenu)) {
			response.addCookie(new Cookie("loginmenu",loginmenu));
			//response.addCookie(new Cookie("agree","true"));
		}else {
			response.addCookie(new Cookie("loginmenu",null));
			//response.addCookie(new Cookie("agree",null));
		}

		model.addAttribute("error", error);
		model.addAttribute("errormsg", request.getParameter("errormsg"));

        MemberDto memberDto = new MemberDto();
        try {
            memberDto = memberService.getMemberDetail(email);
            
            //사용자정보 누락시 예외처리 - unongko 
            if(memberDto == null) {
    			model.addAttribute("errormsg", "사용자정보가 존재하지 않습니다.");
    			return "redirect:/?lang=ko";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        logger.info("*********.getDetail() Dto ::: {}  ", gson.toJson(memberDto));

        model.addAttribute("createFlag", memberDto.getCreateFlag());
		
		return "noLayout/kr/login/signupForm.pg";
	}
	

	//===== homeController로 이동 -- 2020.05.26 YGE 작성

	/*@RequestMapping(value = "/pricingPage")
	public String pricingPage(HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("LoginController/pricingPage()");

		//국문/영문 분기 처리
		String paramLang = loginService.setLang(request, response);

		model.addAttribute("google_url", loginBtnUrl);
		String returnUri = "";

		returnUri = "noLayout/"+ paramLang +"/support/outsidePricing.pg";

		return returnUri;
	}*/



	//===== homeController로 이동 -- 2020.05.26 YGE 작성

	/*@RequestMapping(value = "/academyForm")
	public String academyForm(HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("LoginController/academyForm()");

		//국문/영문 분기 처리
		String paramLang = loginService.setLang(request, response);

		String returnUri = "";
		model.addAttribute("google_url", loginBtnUrl);

		returnUri = "noLayout/"+ paramLang +"/login/academyForm.pg";

		return returnUri;
	}*/



	//===== homeController로 이동 -- 2020.05.26 YGE 작성

	/*@RequestMapping(value = "/krEmployeesMain")
	public String krEmployeesMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krEmployeesMain!");

		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("menuName", "Employees");
		model.addAttribute("google_url", loginBtnUrl);
		model.addAttribute("lang", "ko");
		String returnUri = "noLayout/kr/employees/employeesMain.pg";

		return returnUri;
	}*/



	//===== homeController로 이동 -- 2020.05.26 YGE 작성

	/*@RequestMapping(value = "/krTermsMain")
	public String krTermMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krTermMain!");

		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("menuName", "Term");
		model.addAttribute("google_url", loginBtnUrl);
		model.addAttribute("lang", "ko");
		String returnUri = "noLayout/kr/login/termsMain.pg";

		return returnUri;
	}
	@RequestMapping(value = "/enTermsMain")
	public String enTermsMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enTermsMain!");

		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("menuName", "Term");
		model.addAttribute("google_url", loginBtnUrl);
		model.addAttribute("lang", "en");
		String returnUri = "noLayout/en/login/termsMain.pg";

		return returnUri;
	}*/


	// 버려진 아바타... 삭제할 때 jsp나 다른 것들도 같이 지우기! -- 2020.05.26 YGE 작성
/**
	 * newLanding Main
	 * @param model
	 * @return
	 */
/*
	@RequestMapping(value = "/krLandingMain")
	public String krLandingMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krLandingMain!");

		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("menuName", "landing");

		model.addAttribute("mvp_maker_url", mUrl_MVPMaker);
		model.addAttribute("lang", "ko");
		String returnUri = "noLayout/kr/login/landingMain.pg";

		return returnUri;
	}*/

/**
	 * newLanding Main
	 * @param model
	 * @return
	 */
/*
	@RequestMapping(value = "/krLandingMain_Logic")
	public String krLandingMain_Logic(HttpServletRequest request, Model model) {
		logger.info("Welcome krLandingMain_Logic!");


		HttpSession httpSession = request.getSession(true);
		model.addAttribute("mvp_maker_url", mUrl_MVPMaker);
		model.addAttribute("lang", "ko");
		String returnUri = "noLayout/kr/login/landingMain_Logic.pg";

		return returnUri;
	}*/



	/** signup user정보 업데이트 */
	@RequestMapping(value = "/krSetDetail")
	@ResponseBody
	public Map<String, Object> krSetDetail(HttpServletRequest request,HttpServletResponse response, MemberForm memberForm) throws Exception {

		logger.info("LoginController/krSetDetail()");

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		MemberDto updatedMemberDto;
		String email = memberVo.getEmail();

		memberForm.setEmail(email);
		logger.info("requestedVo ::: {}", gson.toJson(memberForm));

		//update privacyAgree
		if(memberService.updatePrivacyAgreeMember(memberForm) == 1){
			logger.info("{} :: PrivacyAgree update success.", email);
		}else{
			logger.error("{} :: PrivacyAgree update FAIL !!!!", email);
		}

		updatedMemberDto = memberService.updateSignUpMemberInfo(memberForm);

		Map<String, Object> returnMap = new HashMap<>();

		if("SUCCESS".equals(updatedMemberDto.getResult())) {
			memberVo = memberService.getLogInMemberDetail(email);
			httpSession.setAttribute("accessUser", memberVo); // 세션 update
			returnMap.put("status", "success");
			//returnMap.put("redirectUri", "/login/krPayment");
			returnMap.put("redirectUri", httpSession.getAttribute("callback_uri"));
			logger.info("json: {}",returnMap);
		}else{
			logger.info("**** updateSignUpMemberInfo FAIL");
			logger.error("**** memberDto.getResult: {} :: update sign up user info :: FAIL", updatedMemberDto.getResult());
			returnMap.put("status", "fail");
			returnMap.put("redirectUri", "/?lang=ko");
		}

		return returnMap;
	}
	
	/** signup user정보 업데이트 */
	@RequestMapping(value = "/enSetDetail")
	@ResponseBody
	public Map<String, Object> enSetDetail(HttpServletRequest request,HttpServletResponse response, MemberForm memberForm) throws Exception {

		logger.info("LoginController/enSetDetail()");

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		MemberDto updatedMemberDto;
		Map<String, Object> returnMap = new HashMap<String, Object>();

		String userEmail = memberVo.getEmail();
		memberForm.setEmail(userEmail);
		logger.info("requestedVo ::: {}", gson.toJson(memberForm));

		//update privacyAgree
		if(memberService.updatePrivacyAgreeMember(memberForm) == 1){
			logger.info("{} :: PrivacyAgree update success.", userEmail);
		}else{
			logger.error("{} :: PrivacyAgree update FAIL !!!!", userEmail);
		}

		updatedMemberDto = memberService.updateSignUpMemberInfo(memberForm);

		if("SUCCESS".equals(updatedMemberDto.getResult())) {
			memberVo = memberService.getLogInMemberDetail(userEmail);
			httpSession.setAttribute("accessUser", memberVo); // 세션 update
			returnMap.put("status", "success");
			returnMap.put("redirectUri", "/login/enPayment");
			logger.info("json: {}",returnMap);
			
			// [영문] 신규 사용자 로그인시 추가 정보 입력받고 세션 저장 후 로그인 처리 -- 한시적 사용  (2019.07.25 ~ 2019.08.25) : start
/*	        try{
	            memberVo = memberService.getLogInMemberDetail(memberForm.getEmail());
	            logger.info("memberVo  : {}", gson.toJson(memberVo));
	        }catch (Exception e){
	            e.printStackTrace();
	        }

	        HttpSession session = null;
	        if(memberVo != null){
	            session = request.getSession();
	            session.setAttribute("accessUser", memberVo);
	        }			*/
			// [영문] 신규 사용자 로그인시 추가 정보 입력받고 세션 저장 후 로그인 처리-- 한시적 사용  (2019.07.25 ~ 2019.08.25) : end			

		}else{

			logger.error("memberDto.getResult: {} :: update sign up user info :: FAIL", updatedMemberDto.getResult());
			returnMap.put("status", "fail");
			returnMap.put("redirectUri", "/?lang=en");
		}

		return returnMap;
	}	

	//loginComplete**********


	@RequestMapping(value="/krPayment")
	public String krPayment(HttpServletRequest request, Model model){

		logger.info("LoginController/krPayment()");

		HttpSession httpSession = request.getSession(true);

		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		int userNo = memberVo.getUserno();

        if(memberVo.getStatus() == UserStatus.UNSUBSCRIBED) {
			request.setAttribute("checkFlag", "billingPay"); // JSTL 참조 변수
		} else {
        	logger.error("LoginController/krPayment() :: userNo={}, user status={}", userNo, memberVo.getStatus());
			request.setAttribute("freeFlag", "billingFree"); // JSTL 참조 변수
		}

        if(memberVo.getStatus() == UserStatus.UNSUBSCRIBED) {
			model.addAttribute("freeFlag", 1); // script 변수
		} else {
			model.addAttribute("freeFlag", 0); // script 변수
		}

		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		String returnUri = "noLayout/kr/login/loginPayment.pg";

		return returnUri;
	}
	
	@RequestMapping(value="/enPayment")
	public String enPayment(HttpServletRequest request, Model model){

		logger.info("LoginController/enPayment()");

		HttpSession httpSession = request.getSession(true);
		MemberVo member = (MemberVo) httpSession.getAttribute("accessUser");

		// Set billing data
		model.addAttribute("paypal_client_id", paypalClientId);

		int userStatus = -1;
		try {
			MemberForm memberForm = new MemberForm();
			memberForm.setUserNo(member.getUserno());
			userStatus = memberService.getUserStatus(memberForm); // user Status 조회

			logger.info("Before payment user STATUS : {}", userStatus);

			if(userStatus == 0){ // FREE 결제
				model.addAttribute("plan_id", paypalFreePlanId);
				model.addAttribute("payMethod", "FREE");
			}else{ // INSTANT 결제
				model.addAttribute("plan_id", paypalInstantPlanId);
				model.addAttribute("payMethod", "INSTANT");
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("getUserStatus error : ", e);
		}
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		model.addAttribute("userName", member.getName());
		model.addAttribute("userEmail", member.getEmail());

		logger.info("enPayment model: {}", model);

		return "noLayout/en/login/loginPayment.pg";
	}


	@RequestMapping(value="/paypalPaymentErr")
	public String paypalPaymentErr(@RequestParam(value = "errMsg") String errMsg){
		logger.error("==== Paypal Error Message ====\n {}", errMsg);
		return "noLayout/en/login/billingFail.pg";
	}

// [아래 주석 문제 없을 시 삭제] 기존 krSignupComplete -- 2020.03.30 YGE 작성
/*	@RequestMapping(value="/krSignupComplete")
	public String krSignupComplete(HttpServletRequest request, Model model) throws ParseException {

		logger.info("{} .krSignupComplete()", getClass().getSimpleName());

//        Map<String, ?> inputFlashMap = RequestContextUtils.getInputFlashMap(request);


		// SimpleDateFormat orgFromat = new SimpleDateFormat("yyyy-MM-dd");
		// Date date = orgFromat.parse((String)inputFlashMap.get("payDate"));
        // SimpleDateFormat format = new SimpleDateFormat("yyyy년 MM월 dd일");

		// logger.info("----------- payDate: {}", (String)inputFlashMap.get("payDate"));


		//model.addAttribute("payDate", format.format(date));

		return "noLayout/kr/login/signupComplete.pg";
	}*/


	/* API ID, KEY 발급 및 signupComplete 페이지 : paymentService에서 결제 후 redirect -- 2020.03.30 YGE  */
	@RequestMapping(value="/krSignupComplete")
    public String krSignupComplete(HttpServletRequest request, Model model) throws Exception {

        logger.info("{} .krSignupComplete()", getClass().getSimpleName());

        HttpSession session = request.getSession(true);
		MemberVo memberVo = (MemberVo)session.getAttribute("accessUser");

		ApiKeyVo api = apiDao.getUserApi(memberVo.getEmail());
		if(api.getApiId() == null && api.getApiKey() == null){   //apiId 정보가 없는 경우
			loginService.createOrResumeApiAccount(memberVo); // API 계정 create or resume
		}

        String callback_uri = (String)session.getAttribute("callback_uri");

        if(callback_uri != null){
        	logger.debug("callback_uri 있음!! : {}", callback_uri);
        	model.addAttribute("btnUri", callback_uri);
		}else{
			logger.debug("callback_uri 없음!! --> /login/krLoginMain ");
			model.addAttribute("btnUri", "/login/krLoginMain");
		}

        session.removeAttribute("callback_uri");

        return "noLayout/kr/login/signupComplete.pg";
    }

	@RequestMapping(value="/reSignupComplete")
	public String reSignupComplete(HttpServletRequest request, Model model) throws Exception {

		logger.info("{} .reSignupComplete()", getClass().getSimpleName());

		HttpSession session = request.getSession(true);
		MemberVo memberVo = (MemberVo)session.getAttribute("accessUser");

		ApiKeyVo api = apiDao.getUserApi(memberVo.getEmail());
		if(api.getApiId() == null && api.getApiKey() == null){   //apiId 정보가 없는 경우
			loginService.createOrResumeApiAccount(memberVo); // API 계정 create or resume
		}

		return "noLayout/kr/login/reSignupComplete.pg";
	}

    @RequestMapping(value="/enSignupComplete")
    public String enSignupComplete(HttpServletRequest request, Model model) throws Exception {

        logger.info("{} .enSignupComplete()", getClass().getSimpleName());

		HttpSession session = request.getSession(true);
		MemberVo member = (MemberVo) session.getAttribute("accessUser");

		ApiKeyVo api = apiDao.getUserApi(member.getEmail());
		if(api.getApiId() == null && api.getApiKey() == null){   //apiId 정보가 없는 경우
			loginService.createOrResumeApiAccount(member); // API 계정 create or resume
		}

		String payDate = "";
		int userStatus = -1;
//		Map<String, String> errMsg = new HashMap <>();
		String errorMsg = "";

		try {
			PaymentVo paymentVo = memberService.selectNextBillingInfo(member.getUserno()); //billing_t에서 payDate 조회
			payDate = paymentVo.getPayDate();

			MemberForm memberForm = new MemberForm();
			memberForm.setUserNo(member.getUserno());
			userStatus = memberService.getUserStatus(memberForm); // user Status 조회

		} catch (Exception e) {
			logger.error("Get user info error : ", e);
			errorMsg = "Get user info error";
		}

		SimpleDateFormat orgFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = orgFormat.parse(payDate);
		SimpleDateFormat format = new SimpleDateFormat(" MMM dd, yyyy", Locale.ENGLISH);
		payDate = format.format(date);
		logger.info("enSignupComplete ------- payDate: {}, userStatus: {}", payDate, userStatus);

		// model setting
		model.addAttribute("payDate", payDate);
		if(userStatus == 1){ // FREE 사용자
			model.addAttribute("freeFlag",1);
		}else if(userStatus == 2){ // SUBSCRIBE 사용자
			model.addAttribute("freeFlag",0);
		}else{
			logger.error("userStatus error : Not FREE or SUBSCRIBE user");
//			errMsg.put("userStatus error", ""+userStatus);
			errorMsg = "User status error";
		}

		model.addAttribute("errMsg", errorMsg);

		String callback_uri = (String)session.getAttribute("callback_uri");

		if(callback_uri != null){
			logger.debug("callback_uri 있음!! : {}", callback_uri);
			model.addAttribute("btnUri", callback_uri);
		}else{
			logger.debug("callback_uri 없음!! --> /login/enLoginMain ");
			model.addAttribute("btnUri", "/login/enLoginMain");
		}

		session.removeAttribute("callback_uri");


		logger.info("enSignupComplete ------- return model : {}", model);

        return "noLayout/en/login/signupComplete.pg";
    }    


    @RequestMapping(value="/billingFail")
    public String billingFail(HttpServletRequest request, Model model, String reason, String moid){

        logger.info("{}.billingFail()",getClass().getSimpleName());


// ////////////////////////////////////////////////////////////////////////////////////////////////
// 		아래의 함수를 통해 데이타가 직접 jsp에 전달된다.  (belsnake, 20190820)
//
//  	rediectAttr.addFlashAttribute("reason", paramMap.get("resultmsg"));
//		rediectAttr.addFlashAttribute("moid", " ");
// ----------------------------------------------------------
        model.addAttribute("reason", reason);
        model.addAttribute("moid", moid);
// ////////////////////////////////////////////////////////////////////////////////////////////////

        return "noLayout/kr/login/billingFail.pg";
    }

	@RequestMapping(value="/reBillingFail")
	public String reBillingFail(HttpServletRequest request, Model model, String reason, String moid){

		logger.info("{}.reBillingFail()",getClass().getSimpleName());


// ////////////////////////////////////////////////////////////////////////////////////////////////
// 		아래의 함수를 통해 데이타가 직접 jsp에 전달된다.  (belsnake, 20190820)
//
//  	rediectAttr.addFlashAttribute("reason", paramMap.get("resultmsg"));
//		rediectAttr.addFlashAttribute("moid", " ");
// ----------------------------------------------------------
		model.addAttribute("reason", reason);
		model.addAttribute("moid", moid);
// ////////////////////////////////////////////////////////////////////////////////////////////////

		return "noLayout/kr/login/reBillingFail.pg";
	}
	@RequestMapping(value="/krLoginMain")
	public String krLoginMain(HttpServletRequest request, Model model, String reason, String moid){

		logger.info("{}.krLoginMain()",getClass().getSimpleName());

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		logger.info("krLoginMain : memberVo :: {}",memberVo);

		//사용자정보, 결재정보 누락시 예외처리 - unongko
		// modified by LYJ - 2019. 10. 21 - getProduct --> getStatus
		if(memberVo == null) {
			model.addAttribute("errormsg", "사용자정보가 존재하지 않습니다.");
			return "redirect:/?lang=ko";
		}else if(memberVo.getStatus() != UserStatus.FREE && memberVo.getStatus() != UserStatus.SUBSCRIBED && memberVo.getStatus() != UserStatus.UNSUBSCRIBING) {
			model.addAttribute("errormsg", "결제정보가 존재하지 않습니다.");
			return "redirect:/?lang=ko";
		}
		
		MemberForm memberForm = new MemberForm();
		String email = memberVo.getEmail();
		String returnUri = "";

		memberForm.setEmail(email);
		MemberSecurityVo user = (MemberSecurityVo) memberDetailsService.loadUserByUsername(memberForm);
		logger.info("user.getAuthorities()  : {}" , user.getAuthorities());
		logger.info("user.getPrivacyAgree()  : {}" , user.getPrivacyAgree());

		returnUri = "redirect:/main/krMainHome";

		SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(email, "", user.getAuthorities()));

		try{
			memberVo = memberService.getLogInMemberDetail(email);
			logger.info("getMemberDetail === memberVo  : {}", gson.toJson(memberVo));
		}catch (Exception e){
			e.printStackTrace();
		}

		HttpSession session = null;
		if(memberVo != null){
			session = request.getSession();
			session.setAttribute("accessUser", memberVo);
		}

		return returnUri;
	}

	@RequestMapping(value="/enLoginMain")
	public String enLoginMain(HttpServletRequest request, Model model, String reason, String moid){

		logger.info("{}.enLoginMain()",getClass().getSimpleName());

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		logger.info("enLoginMain : memberVo :: {}",memberVo);

		//사용자정보, 결제정보 누락시 예외처리 - unongko
		if(memberVo == null) {
			model.addAttribute("errormsg", "사용자정보가 존재하지 않습니다.");
			return "redirect:/?lang=en";
		}else if(memberVo.getStatus() != UserStatus.FREE && memberVo.getStatus() != UserStatus.SUBSCRIBED && memberVo.getStatus() != UserStatus.UNSUBSCRIBING) {
			model.addAttribute("errormsg", "결제정보가 존재하지 않습니다.");
			return "redirect:/?lang=en";
		}
		
		MemberForm memberForm = new MemberForm();
		String email = memberVo.getEmail();
		String returnUri = "";

		memberForm.setEmail(email);
		MemberSecurityVo user = (MemberSecurityVo) memberDetailsService.loadUserByUsername(memberForm);
		logger.info("user.getAuthorities()  : {}" , user.getAuthorities());
		logger.info("user.getPrivacyAgree()  : {}" , user.getPrivacyAgree());

		returnUri = "redirect:/main/enMainHome";

		SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(email, "", user.getAuthorities()));

		try{
			memberVo = memberService.getLogInMemberDetail(email);
			logger.info("getMemberDetail === memberVo  : {}", gson.toJson(memberVo));
		}catch (Exception e){
			e.printStackTrace();
		}

		HttpSession session = null;
		if(memberVo != null){
			session = request.getSession();
			session.setAttribute("accessUser", memberVo);
		}

		return returnUri;
	}



	/*
	**
	**
	*/
	@RequestMapping(value="/linkExtService")
	public String linkExtService(HttpServletRequest request,
								 HttpServletResponse response, Model model, String serviceUrl) throws IOException {

		logger.info("=========================> linkExtService.");

		/* 구글code 발행 */
		OAuth2Parameters googleOAuth2Parameters = new OAuth2Parameters();
		googleOAuth2Parameters.setRedirectUri(mDomain + "/login/oauth2callback.do");
		googleOAuth2Parameters.setScope("https://www.googleapis.com/auth/userinfo.email");

		OAuth2Operations oauthOperations = googleConnectionFactory.getOAuthOperations();
		String url = oauthOperations.buildAuthorizeUrl(GrantType.AUTHORIZATION_CODE, googleOAuth2Parameters);


		HttpSession session = request.getSession(true);
		session.setAttribute("ApplicationUrl", serviceUrl + "/security/login");
		logger.info("=========================> linkExtService. ===> {} / {}", url, serviceUrl);

		return "redirect:" + url;

	}




}