package console.maum.ai.login.controller;

import com.google.gson.Gson;
import console.maum.ai.member.model.MemberDto;
import console.maum.ai.member.service.MemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/maum")
public class MaumLoginController {

    private static final Logger logger = LoggerFactory.getLogger(MaumLoginController.class);

    @Autowired
    MemberService memberService;

    @RequestMapping(value = "/user/info")
    public String maumUserInfo(@RequestParam("email") String email,
                               @RequestParam("callback_uri") String callback_uri,
                               HttpServletRequest request, HttpServletResponse response, Model model) {
        //logger.info(" @ Hello maumUserInfo Controller ! ");

        HttpSession session = request.getSession();
        MemberDto memberDto = new MemberDto();
        Gson gson = new Gson();
        String paramLang = request.getParameter("lang");

        try{
            memberDto = memberService.getMaumLogInMemberDetail(email);
            logger.info("memberDto  : {}", gson.toJson(memberDto));
        }catch (Exception e){
            logger.error("Error in getMaumLogInMemberDetail ==> " + e);
            e.printStackTrace();
        }

        if(memberDto != null){
            session.setAttribute("accessUser", memberDto);
            session.setAttribute("callback_uri", callback_uri);
        }

        // 쿠키 조회 후 결제 페이지로 redirect
        Cookie[] getCookie = request.getCookies();
        if(getCookie != null){
            for(int i=0; i<getCookie.length; i++){
                Cookie c = getCookie[i];
                String name = c.getName(); // 쿠키 이름 가져오기
                String value = c.getValue(); // 쿠키 값 가져오기

                if("lang".equals(name) && !"".equals(value) ) {
                    paramLang = c.getValue();
                }
            }
        }

        if("en".equalsIgnoreCase(paramLang)) {
            return "redirect:/login/enPayment";
        } else {
            return "redirect:/login/krPayment";
        }
    }
}
