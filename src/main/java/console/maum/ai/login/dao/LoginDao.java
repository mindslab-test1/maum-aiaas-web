package console.maum.ai.login.dao;

import console.maum.ai.login.model.RedirectStateVo;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public class LoginDao {

    @Resource(name = "sqlSession")
    private SqlSession sqlSession;

    private static final String NAMESPACE = "console.maum.ai.model.memberMapper";

    /* state로 Redirect 시킬 페이지 정보 저장 - 2020.04.06 YGE*/
    public int insertRedirectState(RedirectStateVo redirectStateVo) {
        return sqlSession.insert(NAMESPACE + ".insertRedirectState", redirectStateVo);
    }


    /* state로 Redirect 정보 조회 - 2020.04.06 YGE */
    public RedirectStateVo getRedirectState(String state){
        return sqlSession.selectOne(NAMESPACE + ".getRedirectState", state);
    }

    /* 사용한 state 삭제 - 2020.04.06 YGE */
    public void deleteState(String state){
        sqlSession.delete(NAMESPACE + ".deleteState", state);
    }

}

