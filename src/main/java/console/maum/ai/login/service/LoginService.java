package console.maum.ai.login.service;

import console.maum.ai.admin.apikey.dao.ApiKeyDao;
import console.maum.ai.admin.apikey.model.ApiKeyVo;
import console.maum.ai.admin.apikey.service.ApiKeyService;
import console.maum.ai.login.dao.LoginDao;
import console.maum.ai.login.model.RedirectStateVo;
import console.maum.ai.member.model.MemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class LoginService {

    @Autowired
    private LoginDao loginDao;

    @Autowired
    private ApiKeyDao apiDao;


    @Autowired
    private ApiKeyService registApiService;

    private static final Logger logger = LoggerFactory.getLogger(LoginService.class);

    public int insertRedirectState(RedirectStateVo redirectStateVo){
        return loginDao.insertRedirectState(redirectStateVo);
    }

    public RedirectStateVo getRedirectState(String state){
        return loginDao.getRedirectState(state);
    }

    public void deleteState(String state){
        loginDao.deleteState(state);
    }


    public void createOrResumeApiAccount(MemberVo memberVo){
        ApiKeyVo apiVo = null;

        try {
            apiVo = apiDao.getUserApi(memberVo.getEmail());
        } catch (Exception e) {
            logger.error("getUserAPI exception : " + e);
        }

        String apiId = apiVo.getApiId();
        String apiKey = apiVo.getApiKey();
        Map<String, String> result = new HashMap<>();

        if( apiId == null && apiKey == null) {
            logger.info("{} ==> API ID, Key 발급 시작", memberVo.getEmail());
            do {
                UUID uuid = UUID.randomUUID();
                apiId = memberVo.getId() + uuid.toString().split("-")[0] + uuid.toString().split("-")[1];

                Map<String, String> usrMap = new HashMap<>();
                usrMap.put("apiId", apiId);
                usrMap.put("email", memberVo.getEmail());
                usrMap.put("name", memberVo.getName());

                try {
                    result = registApiService.addApiKey(usrMap);
                } catch (Exception e) {
                    logger.error("API ID,key 발급 Exception : " + e);
                }
            } while (result.get("message").equals("IdOverlap"));

            if ("Success".equals(result.get("message"))) {
                logger.info("{}'s Get API account SUCCESS.", memberVo.getEmail());
            } else {
                logger.error("{}'s Get API account FAIL.", memberVo.getEmail());
            }

        }else{
            logger.info("{} already have API account ==> ID : {} , KEY : {}", memberVo.getEmail(), apiId, apiKey);
            String resumeStr = registApiService.resumeApiAccount(apiId); // account resume

            if(!resumeStr.equals("Success")){
                logger.info("실패 -- {}", resumeStr);
            }
        }

    }


    // Language setting -- 2020.03.27 YGE
    public String setLang(HttpServletRequest request, HttpServletResponse response){

        String lang = request.getParameter("lang");
        String cookieLang;

        if("".equals(lang) || lang == null) {
            Cookie[] cookies = request.getCookies();

            if(cookies != null){
                for (Cookie c : cookies) {
                    if("lang".equals(c.getName())) {
                        lang = c.getValue();
                    }
                }
            }

            if(lang == null || lang.equals("")) {
                lang= request.getLocale().toString();
            }
        }

        if("ko_KR".equals(lang) || "ko".equals(lang) || "kr".equals(lang)) {
            lang = "kr";
            cookieLang = "ko";
        } else {
            lang = "en";
            cookieLang = "en";
        }

        response.addCookie(new Cookie("lang", cookieLang));
        logger.info("addCookie lang : {}", cookieLang);

        return lang;
    }

    // change lang cookie - 2020.11.18 LYJ
    public void changeCookie(HttpServletRequest request, String lang) {
        Cookie[] cookies = request.getCookies();

        for(Cookie cookie:cookies) {
            if("lang".equalsIgnoreCase(cookie.getName())){
                cookie.setValue(lang);
            }
        }
    }
}
