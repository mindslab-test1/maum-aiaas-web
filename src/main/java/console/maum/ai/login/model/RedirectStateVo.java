package console.maum.ai.login.model;

import lombok.Data;

@Data
public class RedirectStateVo {

    int id;
    String state;
    String lang;
    String createDateTime;
    String targetUrl;           // 로그인 완료 후, 전환될 페이지
}
