package console.maum.ai.login.handler;

import console.maum.ai.oauth.service.OauthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class LogoutHandler implements LogoutSuccessHandler {

    private static final Logger logger = LoggerFactory.getLogger(LogoutHandler.class);

    @Autowired
    private OauthService oauthService;

    @Value("${hq.logout_redirect_url}")
    private String hqLogoutRedirectUrl;

    @Override
    public void onLogoutSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {

        /* --------------------------------------------------------------------------------------------------------- */
        /* 기존 로그아웃 */
/*        String deleteTokenUrl = oauthService.makeDeleteTokenUrl(request);

        String returnUrl = deleteTokenUrl + "&returnUrl=" + hqLogoutRedirectUrl;

        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null){
            logger.info("Authentication != null --> 수동 로그아웃...?");
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }

        request.getSession().invalidate();
        response.setStatus(HttpServletResponse.SC_OK);
        response.sendRedirect(returnUrl);*/
        /* --------------------------------------------------------------------------------------------------------- */


        /* --------------------------------------------------------------------------------------------------------- */
        /* JWT를 이용한 로그아웃 */
        
        request.getSession().invalidate();
        response.setStatus(HttpServletResponse.SC_OK);
        response.sendRedirect(oauthService.deleteJwt(request, response));
        /* --------------------------------------------------------------------------------------------------------- */
    }
}
