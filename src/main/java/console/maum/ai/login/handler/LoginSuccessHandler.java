package console.maum.ai.login.handler;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import console.maum.ai.member.model.MemberVo;
import console.maum.ai.member.service.MemberService;

public class LoginSuccessHandler implements AuthenticationSuccessHandler {
	private static int TIME = 60 * 60; // 1시간

	@Autowired
	private MemberService memberService;
	
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {
		
		request.getSession().setMaxInactiveInterval(TIME);
	
		String id = auth.getName();

		MemberVo memberVo = new MemberVo();
		try{
			memberVo = memberService.getLogInMemberDetail(id);
		}catch (Exception e){
			e.printStackTrace(); 
		}

//		if(memberVo != null){
//			HttpSession session = request.getSession();			
//			session.setAttribute("accessUser", memberVo);			
//		}
		
		//국문/영문 분기 처리
		String paramLang = request.getParameter("lang");
				
		if(!"".equals(paramLang) && paramLang != null) {
			response.addCookie(new Cookie("lang",paramLang));
		}else {
			
			Cookie[] getCookie = request.getCookies();
			if(getCookie != null){
				for(int i=0; i<getCookie.length; i++){	
					Cookie c = getCookie[i];		
					String name = c.getName(); // 쿠키 이름 가져오기		
					String value = c.getValue(); // 쿠키 값 가져오기
					
					if("lang".equals(name) && !"".equals(value) ) {
						paramLang = c.getValue();
					}				
				}
			}	
		}	
//		logger.info("loginForm paramLang : " + paramLang);
		
		String lang = request.getLocale().toString();
		if(paramLang != null) {
			lang = paramLang;
		}
		
		String returnUri = "";
		if("ko_KR".equals(lang) || "ko".equals(lang)){
			returnUri = "/main/krMainHome";
		}else{
			returnUri = "/main/enMainHome";
		}			
		
		response.sendRedirect(request.getContextPath() + returnUri);
	}
}