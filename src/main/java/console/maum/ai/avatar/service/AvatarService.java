package console.maum.ai.avatar.service;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})

@Service
public class AvatarService {

    private static final Logger logger = LoggerFactory.getLogger(AvatarService.class);

    @Value("${api.url}")
    private String mUrl_ApiServer;

    public ResponseEntity<byte[]> avatarService(String apiId, String apiKey, String saveId, String uploadPath,
                                                MultipartFile videoFile, MultipartFile imageFile) {

        if(videoFile == null) {
            throw new RuntimeException("Video file is not exist ! ");
        } else if(imageFile == null) {
            throw new RuntimeException("Image file is not exist ! ");
        }
        logger.debug(" @ Avatar Service's file size check ==> videoFile: {} and imageFile: {}", videoFile.getSize(), imageFile.getSize());

        try {

            String url = mUrl_ApiServer + "/avatar/download";

            String logMsg = "\n===========================================================================\n";
            logMsg += "Avatar API @ PARAMS \n";
            logMsg += String.format(":: %-10s = %s%n", "URL", url);
            logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
            logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
            logMsg += String.format(":: %-10s = %s%n", "videoFile", videoFile.getOriginalFilename());
            logMsg += String.format(":: %-10s = %s%n", "imageFile", imageFile.getOriginalFilename());
            logMsg += "===========================================================================";
            logger.info(logMsg);

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            File videoVarFile = new File(uploadPath);
            File imageVarFile = new File(uploadPath);

            if (videoVarFile.exists() == false)
                videoVarFile.mkdirs();
            if (imageVarFile.exists() == false)
                imageVarFile.mkdirs();

            videoVarFile = new File((uploadPath + apiId + "_" + videoFile.getOriginalFilename().substring(videoFile.getOriginalFilename().lastIndexOf("\\") + 1)));
            videoFile.transferTo(videoVarFile);
            imageVarFile = new File((uploadPath + apiId + "_" + imageFile.getOriginalFilename().substring(imageFile.getOriginalFilename().lastIndexOf("\\") + 1)));
            imageFile.transferTo(imageVarFile);

            FileBody videoBody = new FileBody(videoVarFile);
            FileBody imageBody = new FileBody(imageVarFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("video", videoBody);
            builder.addPart("images", imageBody);
            HttpEntity entity = builder.build();
            post.setEntity(entity);

            HttpResponse response = client.execute(post);
            HttpEntity responseEntity = response.getEntity();
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("Avatar responseCode = {}" , responseCode);

            // Response ERROR
            if (responseCode != 200) {
                videoVarFile.delete();
                imageVarFile.delete();
                String responseString = "";
                if(responseEntity!=null) {
                    responseString = EntityUtils.toString(responseEntity, "UTF-8");
                }
                logger.error("API avatar fail : \n{}", responseString);
                return null;
            }

            // Response OK
            HttpHeaders headers = new HttpHeaders();
            InputStream in = responseEntity.getContent();
            byte[] videoArray = IOUtils.toByteArray(in);
            logger.info("Avatar result videoArray length : {}", videoArray.length);

            File avatarFile = new File(uploadPath + saveId);
            if(!avatarFile.exists()) {
                try {
                    avatarFile.mkdirs();
                } catch (Exception e) {
                    logger.error(" @ Avatar service (create directory) exception ! ==> " + e);
                }
            }
            FileOutputStream fos = new FileOutputStream(avatarFile + "/avatarFile.wav");
            fos.write(videoArray);
            fos.flush();
            fos.close();

            headers.setCacheControl(CacheControl.noCache().getHeaderValue());

            ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(videoArray, headers, HttpStatus.OK);
            videoVarFile.delete();
            imageVarFile.delete();

            return resultEntity;

        } catch (Exception e) {
            logger.error(" @ Avatar Service Exception ! ==> " + e);
        }
        return null;
    }
}
