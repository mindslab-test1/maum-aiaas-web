package console.maum.ai.avatar.controller;

import console.maum.ai.avatar.service.AvatarService;
import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class AvatarApiController {

    @Autowired
    private AvatarService service;

    @RequestMapping(value = "/api/avatar/getAvatarApi")
    public ResponseEntity<byte[]> multiAvatar( @RequestParam(value = "video") MultipartFile video,
                                               @RequestParam(value = "image") MultipartFile image, HttpServletRequest request)
    {
        HttpSession httpSession = request.getSession(true);

        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getId();

        return service.avatarService(apiId, apiKey, savedId, PropertyUtil.getUploadPath() + "/avatar/", video, image);
    }
}
