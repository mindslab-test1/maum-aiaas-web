package console.maum.ai.avatar.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/avatar")
public class AvatarController {

    private static final Logger logger = LoggerFactory.getLogger(AvatarController.class);

    /**
     * avatar Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/avatarMain")
    public String avatarMain(HttpServletRequest request, Model model) {
        logger.info("Welcome avatarMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "avatarMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "Face-to-Face Avatar");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/avatar/avatarMain.pg";

        return returnUri;
    }

    /**
     * avatar Main (En)
     * @param model
     * @return
     */
    @RequestMapping(value = "/enAvatarMain")
    public String enAvatarMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enAvatarMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "avatarMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Face-to-Face Avatar");
        httpSession.setAttribute("menuGrpName", "Vision");

        model.addAttribute("lang", "en");
        String returnUri = "/en/avatar/avatarMain.pg";

        return returnUri;
    }


}
