package console.maum.ai.user;

import com.google.gson.Gson;
import console.maum.ai.login.service.LoginService;
import console.maum.ai.member.constant.UserStatus;
import console.maum.ai.member.model.MemberDto;
import console.maum.ai.member.model.MemberForm;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.member.service.MemberService;
import console.maum.ai.payment.dao.PaymentDao;
import console.maum.ai.payment.model.PaymentVo;
import console.maum.ai.payment.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.method.P;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Controller
@RequestMapping(value = "/user")
public class UserController {
    @Autowired
    private LoginService loginService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private PaymentService paymentService;
    @Autowired
    private PaymentDao paymentDao;


    @Value("${api.url}")
    private String mUrl_ApiServer;

    @Value("${admin.api.id}")
    private String mAdmin_ApiId;

    @Value("${admin.api.pw}")
    private String mAdmin_ApiKey;

    private static final Logger logger = LoggerFactory.getLogger(UserController.class);


    @RequestMapping(value = "/profileMain")
    public String profileMain(HttpServletRequest request, HttpServletResponse response, Model model) {
        logger.info("Welcome profileMain!");
        String paramLang = loginService.setLang(request, response);
        HttpSession httpSession = request.getSession(true);

        model.addAttribute("lang", paramLang);
        String returnUri = "noLayout/"+ paramLang +"/user/profileMain.pg";

        return returnUri;
    }

    @RequestMapping(value = "/apiAccountMain")
    public String apiAccountMain(HttpServletRequest request, HttpServletResponse response, Model model) throws Exception{
        logger.info("Welcome apiAccountMain!");
        String paramLang = loginService.setLang(request, response);
        HttpSession httpSession = request.getSession(true);

        model.addAttribute("lang", paramLang);
        String returnUri = "noLayout/"+ paramLang +"/user/apiAccountMain.pg";
        MemberVo member = (MemberVo) httpSession.getAttribute("accessUser");

        /* APIKEY 전달 */
        //MemberVo member = (MemberVo) httpSession.getAttribute("accessUser");
        try {
            MemberForm memberForm = new MemberForm();
            memberForm.setUserNo(member.getUserno());

            MemberDto memberDetail= memberService.getMemberDetail(member.getEmail());
            PaymentVo paymentVo = paymentService.getLastPayInfo(member.getUserno());
            String paymentMethod = paymentService.getPaymentMethod(member.getUserno());
            int memberStatus= memberService.getUserStatus(memberForm); // 사용자의 Status 조회

            if("Cash".equalsIgnoreCase(paymentMethod)||"Etc".equalsIgnoreCase(paymentMethod)) { // Cash,etc 사용자에 대한 처리
                paymentVo = new PaymentVo();
                String createDate = paymentDao.getCreateDate(member.getUserno());
                String dateTo = paymentDao.getPaymentDate(member.getUserno());

                log.info("Email: " + member.getEmail() + " / createDate: " + createDate + " / dateTo: " + dateTo);

                paymentVo.setDateFrom(createDate);
                paymentVo.setDateTo(dateTo);

            }else if(memberStatus == UserStatus.INIT){// 결제 안한 사람 조회
                paymentVo = new PaymentVo();
                String memberCreateDate= memberService.getUserCreateDate(member.getUserno());// 사용자의 CreateDate 조회
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
                Calendar cal = Calendar.getInstance();
                Date date = format.parse(memberCreateDate);

                cal.setTime(date);
                paymentVo.setDateFrom(format.format(cal.getTime()));
                cal.add(Calendar.MONTH, 1);
                paymentVo.setDateTo(format.format(cal.getTime()));
            }else {
                httpSession.setAttribute("dateTo", paymentVo.getDateTo());
                httpSession.setAttribute("dateFrom", paymentVo.getDateFrom());
            }
            httpSession.setAttribute("apiId", memberDetail.getApiId());
            httpSession.setAttribute("apikey", member.getApikey());

        } catch (Exception e) {
            httpSession.setAttribute("apiId", null);
            httpSession.setAttribute("apikey", null);
            httpSession.setAttribute("dateTo", null);
            httpSession.setAttribute("dateFrom", null);
            e.printStackTrace();
        }

        return returnUri;
    }


    @RequestMapping(value = "/paymentInfoMain")
    public String paymentInfoMain(HttpServletRequest request, HttpServletResponse response, Model model) {
        logger.info("Welcome paymentInfoMain!");
        String paramLang = loginService.setLang(request, response);
        HttpSession httpSession = request.getSession(true);

        try {
            Gson gson = new Gson();
            MemberVo member = (MemberVo) httpSession.getAttribute("accessUser");
            List<PaymentVo> paymentList = memberService.selectPaymentInfo(member.getUserno());

            logger.debug(paymentList.toString());

            MemberForm memberForm = new MemberForm();
            memberForm.setUserNo(member.getUserno());

            PaymentVo paymentVo = new PaymentVo();
            int memberStatus= memberService.getUserStatus(memberForm); // 사용자의 Status 조회

            String memberCreateDate= memberService.getUserCreateDate(member.getUserno());// 사용자의 CreateDate 조회
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

            Calendar cal = Calendar.getInstance();
            Date date = format.parse(memberCreateDate);
            cal.setTime(date);
            cal.add(Calendar.MONTH, 1);

            logger.debug(paymentVo.getStatus());

            for (PaymentVo vo : paymentList) {
                vo.setGoodName(paymentService.selectProductName(vo.getGoodCode()));
                vo.setIssuerName(paymentService.selectIssuerName(Integer.parseInt(vo.getIssuer())));
            }
            model.addAttribute("paymentList", gson.toJson(paymentList));
            if(memberStatus == UserStatus.INIT){

                PaymentVo tmpPaymentVo = new PaymentVo();
                tmpPaymentVo.setIssuerName("카드없음");

                model.addAttribute("billingInfo", gson.toJson(tmpPaymentVo));
                model.addAttribute("nextBillingInfo", gson.toJson(format.format(cal.getTime())));
                request.setAttribute("userStatus", "INIT");
            } else if(memberStatus == UserStatus.FREE){
//                model.addAttribute("paymentList", gson.toJson(paymentList));
                model.addAttribute("billingInfo", gson.toJson(memberService.selectBillingInfo(member.getUserno())));
                model.addAttribute("nextBillingInfo", gson.toJson(format.format(cal.getTime())));
                request.setAttribute("userStatus", "FREE");
            } else if(memberStatus == UserStatus.SUBSCRIBED){
//                model.addAttribute("paymentList", gson.toJson(paymentList));
                model.addAttribute("billingInfo", gson.toJson(memberService.selectBillingInfo(member.getUserno())));
                model.addAttribute("nextBillingInfo", gson.toJson(memberService.selectNextBillingInfo(member.getUserno())));
                request.setAttribute("userStatus", "SUBSCRIBED");
            } else {
                logger.info("User Status = 3 인 경우");
                if(paymentList.size() != 0){
                    logger.info("User Status = 3 인 경우:: 결제 정보 있음!");
//                    model.addAttribute("paymentList", gson.toJson(paymentList));
                    model.addAttribute("billingInfo", gson.toJson(memberService.selectBillingInfo(member.getUserno())));
                    model.addAttribute("nextBillingInfo", gson.toJson(memberService.selectNextBillingInfo(member.getUserno())));
                }else { // status = 0 이었다가 해지 한 경우
                    logger.info("User Status = 3 인 경우:: 결제 정보 없음!");
                    PaymentVo tmpPaymentVo = new PaymentVo();
                    tmpPaymentVo.setIssuerName("카드없음");
//                    model.addAttribute("paymentList", gson.toJson(paymentList));
                    model.addAttribute("billingInfo", gson.toJson(tmpPaymentVo));
                    model.addAttribute("nextBillingInfo", gson.toJson(format.format(cal.getTime())));
                }
                request.setAttribute("userStatus", "UNSUBSCRIBING");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        model.addAttribute("lang", paramLang);
        String returnUri = "noLayout/"+ paramLang +"/user/paymentInfoMain.pg";

        return returnUri;
    }


    /** 사용자 프로필 조회 */
    @RequestMapping(value = "/getDetail")
    @ResponseBody
    public MemberDto getMemberDetail(HttpServletRequest request, HttpServletResponse response, MemberVo memberVo) throws Exception {
        MemberDto memberDto;

        Gson gson = new Gson();

        logger.info("{} .getDetail()", gson.toJson(memberVo));
        System.out.println("getDetail() ======= " + gson.toJson(memberVo));

        memberDto = memberService.getMemberDetail(memberVo.getEmail());

        return memberDto;
    }

    /** 프로필 수정 */
    @RequestMapping(value = "/setUpdate")
    @ResponseBody
    public MemberDto setUpdate(HttpServletRequest request, HttpServletResponse response, MemberForm memberForm) throws Exception {

        MemberDto memberDto = new MemberDto();

        Gson gson = new Gson();

        logger.info("{} .setUpdate()", gson.toJson(memberForm));
        System.out.println("setUpdate() ======= " + gson.toJson(memberForm));

        memberDto = memberService.setUpdate(memberForm);

        if("SUCCESS".equals(memberDto.getResult())) {
            MemberVo ssMemberVo = new MemberVo();
            HttpSession session = request.getSession();
            ssMemberVo = (MemberVo)session.getAttribute("accessUser");

            ssMemberVo.setName(memberForm.getName());
            session.setAttribute("accessUser", ssMemberVo);
        }

        return memberDto;
    }

}
