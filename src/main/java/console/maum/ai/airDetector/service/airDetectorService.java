package console.maum.ai.airDetector.service;

import console.maum.ai.admin.apikey.controller.ApiKeyApiController;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})


@Service
public class airDetectorService {
    private static final Logger logger = LoggerFactory.getLogger(ApiKeyApiController.class);
    @Value("${api.url}")
    private String mUrl_ApiServer;


    public  ResponseEntity<byte[]> runairDetector(String apiId, String apiKey, MultipartFile file, String uploadPath, String saveId){


        if(file == null){
            throw new RuntimeException("You must select the a file for uploading");
        }

        String url = "https://182.162.19.14:9952/airDetect";
//        String url = mUrl_ApiServer + "/api/airDetector";

        String logMsg = "\n===========================================================================\n";
        logMsg += "airDetect API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "file", file.getOriginalFilename());
        logMsg += "===========================================================================";
        logger.info(logMsg);

        try{
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File airDetectFile = new File(uploadPath);

            if (!airDetectFile.exists()) {
                airDetectFile.mkdirs();
            }


            airDetectFile = new File((uploadPath + "/" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
            file.transferTo(airDetectFile);
            logger.info("uploadPath : {}", airDetectFile.getAbsolutePath());
            FileBody airDetectFileBody = new FileBody(airDetectFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiKey",new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("file", airDetectFileBody);

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            HttpEntity responseEntity = response.getEntity();
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);


            HttpHeaders headers = new HttpHeaders();
            InputStream in = responseEntity.getContent();
            byte[] imageArray = IOUtils.toByteArray(in);
            logger.info(responseEntity.getContent().toString());
            headers.setCacheControl(CacheControl.noCache().getHeaderValue());
            ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(imageArray, headers, HttpStatus.OK);

            airDetectFile.delete();

            return resultEntity;

        }catch(Exception e){
            System.out.println(e);
            e.printStackTrace();
        }

        return null;
    }
}
