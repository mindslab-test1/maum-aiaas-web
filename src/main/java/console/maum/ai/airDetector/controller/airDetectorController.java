package console.maum.ai.airDetector.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import console.maum.ai.admin.apikey.controller.ApiKeyApiController;
import console.maum.ai.textRemoval.controller.TextRemovalController;
import console.maum.ai.airDetector.service.airDetectorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


/*
 * 화면 Controller
 */

//MVC 주요 어노테이션 : @Controller, @RequestMapping, @RequestParam, @ModelAttribute, @SessionAttributes, @RequestPart, @CommandMap, @ControllerAdvice
@Controller
@RequestMapping(value = "/cloudApi/airDetector")

public class airDetectorController {
    private static final Logger logger = LoggerFactory.getLogger(ApiKeyApiController.class);

    /**
     * airDetect Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/krairDetectorMain")
    public String krairDetectorgMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krairDetectorMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "airDetectorMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "고령자 소지품 검출");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/airDetector/airDetectorMain.pg";

        return returnUri;
    }
}
