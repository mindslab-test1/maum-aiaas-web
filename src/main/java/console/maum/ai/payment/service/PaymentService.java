package console.maum.ai.payment.service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.MessageDigest;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.google.gson.Gson;
import com.inicis.inipay4.INIpay;
import com.inicis.inipay4.util.INIdata;
import com.inicis.std.util.HttpUtil;
import com.inicis.std.util.ParseUtil;
import console.maum.ai.admin.apikey.service.ApiKeyService;
import console.maum.ai.common.util.MailSender;
import console.maum.ai.member.constant.UserStatus;
import console.maum.ai.member.dao.MemberDao;
import console.maum.ai.member.model.MemberForm;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.member.model.UnsubsFeedbackVo;
import console.maum.ai.member.service.MemberService;
import console.maum.ai.payment.constant.PaymentPlan;
import console.maum.ai.payment.model.BillingVo;
import console.maum.ai.payment.model.LogPayVo;
import console.maum.ai.payment.model.PaypalVo;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import com.inicis.std.util.SignatureUtil;

import console.maum.ai.payment.dao.PaymentDao;
import console.maum.ai.payment.model.PaymentVo;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sun.rmi.runtime.Log;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.constraints.Null;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",
		"classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@Service
public class PaymentService {
	final String INICIS_MID = "mindslab01";
	final String INICIS_MKEY = "YTE4TUl0SnVxUkNIVUdaZ21zMTd6Zz09";

	@Value("${url.domain}")
	String mUrl_Domain;

	@Value("${file.inipayHome}")
	String mPath_InipayHome;

	@Value("${paypal.IPN_URL}")
	String ipnUrl;

	@Autowired
	private PaymentDao paymentDao;

	@Autowired
	private MemberDao memberDao;

    @Autowired
    private ApiKeyService apiKeyService;

    @Autowired
	private PaypalService paypalService;

    @Autowired
	private MailSender mailSender;

	@Autowired
	private MemberService memberService;


	private static final Logger logger = LoggerFactory.getLogger(PaymentService.class);

	public void commonBilling(PaymentVo paymentVo) {
		paymentDao.commonBilling(paymentVo);
	}

	public ModelAndView basicBilling(String method) throws Exception {
		ModelAndView mv = new ModelAndView("kr/payment/billing");
		String mid = "mindslab01";
		String signKey = "LzFjQlVaakQ1VWl3VkZCZy9BTnhxUT09";
		String timestamp = SignatureUtil.getTimestamp();
		String oid = mid + "_" + timestamp;
//		String basicPrice			= "1000";
		String basicPrice = "29000";
		String basicName = "Basic";
		String businessPrice = "99000";
		String businessName = "Business";
		String mKey = SignatureUtil.hash(signKey, "SHA-256");

		// 결제 제공기간 설정 (오늘 ~ 오늘 + 30일)
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Calendar cal = Calendar.getInstance();
		String dateTo = sdf.format(cal.getTime());
		cal.add(Calendar.DATE, 30);
		String dateFrom = sdf.format(cal.getTime());

		Map <String, String> signParam = new HashMap <String, String>();
		Map <String, String> basicSignParam = new HashMap <String, String>();
		Map <String, String> businessSignParam = new HashMap <String, String>();

		// BasicSignParam 과 BusinessSignParam 분리 관리
		basicSignParam.put("oid", oid);
		basicSignParam.put("price", basicPrice);
		basicSignParam.put("timestamp", timestamp);

		businessSignParam.put("oid", oid);
		businessSignParam.put("price", businessPrice);
		businessSignParam.put("timestamp", timestamp);

		String basicSignature = SignatureUtil.makeSignature(basicSignParam);
		String businessSignature = SignatureUtil.makeSignature(businessSignParam);

		mv.addObject("mid", mid);
		mv.addObject("timestamp", timestamp);
		mv.addObject("oid", oid);
		mv.addObject("basicPrice", basicPrice);
		mv.addObject("basicName", basicName);
		mv.addObject("businessPrice", businessPrice);
		mv.addObject("businessName", businessName);
		mv.addObject("mKey", mKey);
		mv.addObject("signParam", signParam);
		mv.addObject("basicSignature", basicSignature);
		mv.addObject("businessSignature", businessSignature);
		mv.addObject("siteDomain", mUrl_Domain);
		mv.addObject("dateTo", dateTo);
		mv.addObject("dateFrom", dateFrom);
		mv.addObject("method", method);

		return mv;
	}


	public ModelAndView commonPayForm() throws Exception {
		ModelAndView mv = new ModelAndView("kr/payment/commonPay");
		String mid = "mindslab01";
		String signKey = "LzFjQlVaakQ1VWl3VkZCZy9BTnhxUT09";
		String timestamp = SignatureUtil.getTimestamp();
		String oid = mid + "_" + timestamp;
		String price = "1000000";
		String goodName = "고도화 학습 서비스";
		String mKey = SignatureUtil.hash(signKey, "SHA-256");

		Map <String, String> signParam = new HashMap <String, String>();

		// BasicSignParam 과 BusinessSignParam 분리 관리
		signParam.put("oid", oid);
		signParam.put("price", price);
		signParam.put("timestamp", timestamp);

		String signature = SignatureUtil.makeSignature(signParam);

		mv.addObject("mid", mid);
		mv.addObject("timestamp", timestamp);
		mv.addObject("oid", oid);
		mv.addObject("price", price);
		mv.addObject("goodName", goodName);
		mv.addObject("mKey", mKey);
		mv.addObject("signature", signature);
		mv.addObject("siteDomain", mUrl_Domain);

		return mv;
	}

	public String mobileBillingFree(HttpServletRequest request, RedirectAttributes rediectAttr) {

		try {
			HttpSession session = request.getSession();
			MemberVo member = (MemberVo) session.getAttribute("accessUser");
			Calendar cal = Calendar.getInstance();


			// 결과 파라미터 일괄 수신
			Map<String,String> paramMap = new Hashtable<String,String>();
			Enumeration elems = request.getParameterNames();
			String temp = "";
			while(elems.hasMoreElements()) {
				temp = (String) elems.nextElement();
				paramMap.put(temp, request.getParameter(temp));
			}
			logger.debug("% INICIS MOBILE RESULT  : "+ paramMap.toString());

			LogPayVo logPayVo = new LogPayVo();
			logPayVo.setUserNo(member.getUserno());
			logPayVo.setType("Payment");
			logPayVo.setPayment("Free");

			if(!paramMap.get("resultcode").equals("00")) {
				rediectAttr.addFlashAttribute("reason", paramMap.get("resultmsg"));
				rediectAttr.addFlashAttribute("moid", " ");
				return "redirect:/login/billingFail";
			}


			String[] product = paramMap.get("p_noti").split(",");
			PaymentVo payment = new PaymentVo();

			payment.setUserNo(member.getUserno());
			payment.setPayment("Free");
			payment.setIssuer(paramMap.get("cardcd"));
			logPayVo.setCode(paramMap.get("cardcd"));
			payment.setBillingKey(paramMap.get("billkey"));
			logPayVo.setBillKey(paramMap.get("billkey"));
			payment.setCardNo(paramMap.get("cardno") + "****");
			logPayVo.setCardNumber(paramMap.get("cardno") + "****");
			payment.setStatus("00");
			logPayVo.setStatus(0); //성공
			payment.setGoodCode(Integer.parseInt(product[0]) /* product id */);
			logPayVo.setProduct(3);
			payment.setPrice(Integer.parseInt(product[2]));
			logPayVo.setPrice(0);
			payment.setPano(INICIS_MID + "_FREE");
			logPayVo.setPaNo(INICIS_MID + "_FREE");
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			payment.setPayDate(sdf.format(cal.getTime()));
			payment.setDateFrom(sdf.format(cal.getTime()));
			logPayVo.setDateFrom(sdf.format(cal.getTime()));
			cal.add(Calendar.MONTH, 1);
			payment.setDateTo(sdf.format(cal.getTime()));
			logPayVo.setDateTo(sdf.format(cal.getTime()));
			payment.setTid(paramMap.get("tid"));

			logger.debug(payment.toString());
			paymentDao.commonBilling(payment);
			payment.setPayment("Card");
			paymentDao.billInfoChange(payment);

			// LogPay_t insert - 2019. 12. 26 - LYJ
			paymentDao.insertLogPay(logPayVo);

			MemberVo memberRefresh = null;

			memberRefresh = memberDao.getLogInMemberDetail(member.getEmail());

			// User Status update 19. 09. 20 LYJ
            MemberForm memberForm = new MemberForm();
            memberForm.setUserNo(member.getUserno());
            memberForm.setStatus(UserStatus.FREE);
            memberDao.updateUserStatus(memberForm);

			/* Free 고객 환영 메일 - 2019. 11. 15 LYJ */
			mailSender.sendFreeUserMail(member.getEmail());

            // billing_t paymentDate(다음 결제일) 설정 - 2019. 10. 19 LYJ
            BillingVo billingVo = new BillingVo();
            billingVo.setUserNo(member.getUserno());
            billingVo.setPaymentDate(sdf.format(cal.getTime()));
            paymentDao.updatePaymentDate(billingVo);

            String payDate = paymentDao.selectPayDate(member.getUserno());

			session.setAttribute("accessUser", memberRefresh);

            rediectAttr.addFlashAttribute("payDate", payDate);
            rediectAttr.addFlashAttribute("freeFlag", 1);		// mobileBillingFree ------------------------------------------------------무료 체험 여부, mobile billing Pay 에서는 0

		} catch (Exception e) {
			e.printStackTrace();

			rediectAttr.addFlashAttribute("reason", e.getMessage());
			rediectAttr.addFlashAttribute("moid", " ");
			return "redirect:/login/billingFail";
		}

		return "redirect:/login/krSignupComplete";					// 결제 성공 시 이동 할 url

	}

	/*
	 ** Date: 2019. 09. 27. (By unongko)
	 ** Desc: 요금 정책 변경으로 첫달 무료, 이후 정기 결제로 변경됨.
	 ** Update: 2019. 10. 14. By YGE
	 ** Desc: Paypal 결제 ID 및 결제 정보 1차 insert
	 */
	public Map<String, Object> payPalBillingFree(HttpServletRequest request, String subscId) {

		Map<String, Object> resultMap = new HashMap <>();

		HttpSession session = request.getSession();
		MemberVo member = (MemberVo)session.getAttribute("accessUser");
		Calendar cal = Calendar.getInstance();

		PaymentVo payment = new PaymentVo();

		payment.setUserNo(member.getUserno());
		payment.setPayment("Free");
		payment.setStatus("00");
		payment.setGoodCode(paymentDao.selectProductId("BUSINESS"));
		payment.setPrice(Integer.parseInt("0"));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		payment.setPayDate(sdf.format(cal.getTime()));
		payment.setDateFrom(sdf.format(cal.getTime()));
		cal.add(Calendar.MONTH, 1);
		payment.setDateTo(sdf.format(cal.getTime()));

		payment.setIssuer("88"); // 페이팔 issuer 코드
		payment.setBillingKey(subscId);
		payment.setCardNo("paypalcard");

		logger.info(payment.toString());

		// Query part
		try{
			paymentDao.commonBilling(payment);
			payment.setPayment("PayPal");
			paymentDao.billInfoChange(payment);

			// User Status update 19. 09. 20 LYJ
			MemberForm memberForm = new MemberForm();
			memberForm.setUserNo(member.getUserno());
			memberForm.setStatus(UserStatus.FREE);
			memberDao.updateUserStatus(memberForm);

            // billing_t paymentDate(다음 결제일) 설정 - 2019. 10. 19 LYJ
            BillingVo billingVo = new BillingVo();
            billingVo.setUserNo(member.getUserno());
            billingVo.setPaymentDate(sdf.format(cal.getTime()));
            paymentDao.updatePaymentDate(billingVo);

			//*** payDate 와 freeFlag는 login/enSignupComplete 에서 setting함. (YGE) ***/

			resultMap.put("status", "success");
			resultMap.put("redirectUri", "/login/enSignupComplete");
			logger.info("Paypal free - Member update result: {}", resultMap);

		}catch(Exception e){
			logger.error(e.getMessage());
			resultMap.put("status", "fail");
			resultMap.put("redirectUri", "/?lang=en");
			logger.info("Paypal free - Member update result: {}", resultMap);

		} finally{
			try{
				MemberVo memberRefresh = memberDao.getLogInMemberDetail(member.getEmail());
				session.setAttribute("accessUser", memberRefresh);
			} catch (Exception ex) {
				logger.error(ex.getMessage());
				ex.printStackTrace();
			}
		}

		return resultMap;

	}


	/*
	 ** Date: 2019. 10. 21 -- YGE
	 ** Desc: Paypal 즉시 결제
	 */
	public Map<String, Object> payPalBillingInstant(HttpServletRequest request, String subscId) {
		Map<String, Object> resultMap = new HashMap <>();

		HttpSession session = request.getSession();
		MemberVo member = (MemberVo)session.getAttribute("accessUser");
		Calendar cal = Calendar.getInstance();

		PaymentVo payment = new PaymentVo();

		payment.setUserNo(member.getUserno());
		payment.setPayment("PayPal");
		payment.setStatus("00");
		payment.setGoodCode(paymentDao.selectProductId("BUSINESS"));
		payment.setPrice(Integer.parseInt("89"));
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		payment.setPayDate(sdf.format(cal.getTime()));
		payment.setDateFrom(sdf.format(cal.getTime()));
		cal.add(Calendar.MONTH, 1);
		payment.setDateTo(sdf.format(cal.getTime()));

		// 페이팔 관련 코드값 처리
		payment.setIssuer("88");
		payment.setBillingKey(subscId);
		payment.setActive(1);
		payment.setCardNo("paypalcard");

		logger.debug(payment.toString());
		try{
			//paypal 즉시결제시 빌링키 등록
			/** IPN을 받았을 때 pay_t에 insert함 (= commonBilling 안함) */
			paymentDao.insertBilling(payment);
//			paymentDao.billInfoChange(payment);
		}catch(Exception e){
			logger.error(e.getMessage());
		}


		try{
			// User Status update
			MemberForm memberForm = new MemberForm();
			memberForm.setUserNo(member.getUserno());
			memberForm.setStatus(UserStatus.SUBSCRIBED);
			memberDao.updateUserStatus(memberForm);

			// billing_t paymentDate(다음 결제일) 설정 - 2019. 10. 19 LYJ
			BillingVo billingVo = new BillingVo();
			billingVo.setUserNo(member.getUserno());
			billingVo.setPaymentDate(sdf.format(cal.getTime()));
			paymentDao.updatePaymentDate(billingVo);

			//*** [kr과 차이] payDate 와 freeFlag는 login/enSignupComplete 에서 setting함. (YGE) ***/

			resultMap.put("status", "success");
			resultMap.put("redirectUri", "/login/enSignupComplete");
			logger.info("Paypal free - Member update result: {}", resultMap);

		}catch(Exception e){
			logger.error(e.getMessage());
			resultMap.put("status", "fail");
			resultMap.put("redirectUri", "/?lang=en");
			logger.info("Paypal free - Member update result: {}", resultMap);

		} finally{
			try{
				MemberVo memberRefresh = memberDao.getLogInMemberDetail(member.getEmail());
				session.setAttribute("accessUser", memberRefresh);
			} catch (Exception ex) {
				logger.error(ex.getMessage());
				ex.printStackTrace();
			}
		}

		return resultMap;

	}


		/*
	** Date: 2019. 07. 12. (By belsnake)
	** Desc: 요금 정책 변경으로 첫달 무료, 이후 정기 결재로 변경됨.
	*/
    public String billingFree(String email, HttpServletRequest request, RedirectAttributes rediectAttr) {
        Map<String, String> resultMap = new HashMap <>();

        HttpSession session = request.getSession();
        MemberVo member = (MemberVo)session.getAttribute("accessUser");
        Calendar cal = Calendar.getInstance();

        if(member == null){
        	logger.info("결제 user 세션 없음.");
			try {
				member = memberService.getMemberDetail(email);
				session.setAttribute("accessUser", member);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

        try{

            //#############################
            // 인증결과 파라미터 일괄 수신
            //#############################
            request.setCharacterEncoding("UTF-8");

            Map<String,String> paramMap = new Hashtable <String,String>();

            Enumeration elems = request.getParameterNames();

            String temp = "";

            while(elems.hasMoreElements())
            {
                temp = (String) elems.nextElement();
                paramMap.put(temp, request.getParameter(temp));
            }

            logger.debug("paramMap : "+ paramMap.toString());

            //#####################
            // 인증이 성공일 경우만
            //#####################
            if("0000".equals(paramMap.get("resultCode"))){

                logger.debug("paramMap : " + paramMap.toString());

                //############################################
                // 1.전문 필드 값 설정(***가맹점 개발수정***)
                //############################################

                String mid 		= paramMap.get("mid");						// 가맹점 ID 수신 받은 데이터로 설정
                String signKey	= "LzFjQlVaakQ1VWl3VkZCZy9BTnhxUT09";		// 가맹점에 제공된 키(이니라이트키) (가맹점 수정후 고정) !!!절대!! 전문 데이터로 설정금지
                String timestamp= SignatureUtil.getTimestamp();				// util에 의해서 자동생성
                String charset 	= "UTF-8";								    // 리턴형식[UTF-8,EUC-KR](가맹점 수정후 고정)
                String format 	= "JSON";								    // 리턴형식[XML,JSON,NVP](가맹점 수정후 고정)
                String authToken= paramMap.get("authToken");			    // 취소 요청 tid에 따라서 유동적(가맹점 수정후 고정)
                String authUrl	= paramMap.get("authUrl");				    // 승인요청 API url(수신 받은 값으로 설정, 임의 세팅 금지)
                String netCancel= paramMap.get("netCancelUrl");			 	// 망취소 API url(수신 받은 값으로 설정, 임의 세팅 금지)
                String ackUrl 	= paramMap.get("checkAckUrl");			    // 가맹점 내부 로직 처리후 최종 확인 API URL(수신 받은 값으로 설정, 임의 세팅 금지)
                String merchantData = paramMap.get("merchantData");			// 가맹점 관리데이터 수신

                //#####################
                // 2.signature 생성
                //#####################
                Map<String, String> signParam = new HashMap<String, String>();

                signParam.put("authToken",	authToken);		// 필수
                signParam.put("timestamp",	timestamp);		// 필수

                // signature 데이터 생성 (모듈에서 자동으로 signParam을 알파벳 순으로 정렬후 NVP 방식으로 나열해 hash)
                String signature = SignatureUtil.makeSignature(signParam);

                //#####################
                // 3.API 요청 전문 생성
                //#####################
                Map<String, String> authMap = new Hashtable<String, String>();

                authMap.put("mid"			,mid);			// 필수
                authMap.put("authToken"		,authToken);	// 필수
                authMap.put("signature"		,signature);	// 필수
                authMap.put("timestamp"		,timestamp);	// 필수
                authMap.put("charset"		,charset);		// default=UTF-8
                authMap.put("format"		,format);		// default=XML
                //authMap.put("price" 		,price);		// 가격위변조체크기능 (선택사용)

                System.out.println("##승인요청 API 요청##");

                HttpUtil httpUtil = new HttpUtil();

                try{
                    //#####################
                    // 4.API 통신 시작
                    //#####################

                    String authResultString = "";

                    authResultString = httpUtil.processHTTP(authMap, authUrl);

                    //############################################################
                    //5.API 통신결과 처리(***가맹점 개발수정***)
                    //############################################################
                    String test = authResultString.replace(",", "&")
							                      .replace(":", "=")
							                      .replace("\"", "")
							                      .replace(" ","")
							                      .replace("\n", "")
							                      .replace("}", "")
							                      .replace("{", "");

                    //retrunHtml = retrunHtml + "<pre>"+authResultString.replaceAll("<", "&lt;").replaceAll(">", "&gt;")+"</pre>";

                    resultMap = ParseUtil.parseStringToMap(test); //문자열을 MAP형식으로 파싱


                    logger.debug("resultMap == {}", resultMap);

                    /*************************  결제보안 강화 2016-05-18 START ****************************/
                    Map<String , String> secureMap = new HashMap<String, String>();
                    secureMap.put("mid"			, mid);								//mid
                    secureMap.put("tstamp"		, timestamp);						//timestemp
                    secureMap.put("MOID"		, resultMap.get("MOID"));			//MOID
                    secureMap.put("TotPrice"	, resultMap.get("TotPrice"));		//TotPrice

                    // signature 데이터 생성
                    String secureSignature = SignatureUtil.makeSignatureAuth(secureMap);
                    /*************************  결제보안 강화 2016-05-18 END ****************************/

                    if("0000".equals(resultMap.get("resultCode")) && secureSignature.equals(resultMap.get("authSignature")) ){	//결제보안 강화 2016-05-18

                        PaymentVo payment = new PaymentVo();
                        LogPayVo logPayVo = new LogPayVo();
                        logPayVo.setType("Payment");

                        payment.setUserNo(member.getUserno());
                        logPayVo.setUserNo(member.getUserno());
                        payment.setPayment("Free");
						logPayVo.setPayment("Free");
                        payment.setIssuer(resultMap.get("CARD_Code"));
                        logPayVo.setCode(resultMap.get("CARD_Code"));
                        payment.setBillingKey(resultMap.get("CARD_BillKey"));
                        logPayVo.setBillKey(resultMap.get("CARD_BillKey"));
                        payment.setCardNo(resultMap.get("CARD_Num") + "****");
                        logPayVo.setCardNumber(resultMap.get("CARD_Num") + "****");
                        payment.setStatus("00");
                        logPayVo.setStatus(0);
                        payment.setGoodCode(paymentDao.selectProductId(resultMap.get("goodName")));
						logPayVo.setProduct(paymentDao.selectProductId(resultMap.get("goodName")));
                        payment.setPrice(Integer.parseInt(resultMap.get("TotPrice")));
                        logPayVo.setPrice(Integer.parseInt(resultMap.get("TotPrice")));
                        payment.setPano(resultMap.get("MOID"));
						logPayVo.setPaNo(resultMap.get("MOID"));
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        payment.setPayDate(sdf.format(cal.getTime()));
                        payment.setDateFrom(sdf.format(cal.getTime()));
                        logPayVo.setDateFrom(sdf.format(cal.getTime()));
                        cal.add(Calendar.MONTH, 1);
                        payment.setDateTo(sdf.format(cal.getTime()));
                        logPayVo.setDateTo(sdf.format(cal.getTime()));
                        payment.setTid(resultMap.get("tid"));

                        logger.debug(payment.toString());
                        paymentDao.commonBilling(payment);
                        payment.setPayment("Card");
                        paymentDao.billInfoChange(payment);

                        // LogPay_t insert - 2019. 12. 26 - LYJ
						paymentDao.insertLogPay(logPayVo);
						logger.info("Insert LogPay_t @ routineBill success : userNo" + logPayVo.getUserNo());

                        // User Status update 19. 09. 20 LYJ
                        MemberForm memberForm = new MemberForm();
                        memberForm.setUserNo(member.getUserno());
                        memberForm.setStatus(UserStatus.FREE);
                        memberDao.updateUserStatus(memberForm);

                        /* Free 고객 환영 메일 - 2019. 11. 15 - LYJ */
						mailSender.sendFreeUserMail(member.getEmail());

                        // billing_t paymentDate(다음 결제일) 설정 - 2019. 10. 19 LYJ
						BillingVo billingVo = new BillingVo();
						billingVo.setUserNo(member.getUserno());
						billingVo.setPaymentDate(sdf.format(cal.getTime()));
						paymentDao.updatePaymentDate(billingVo);

                        String payDate = paymentDao.selectPayDate(member.getUserno());

                        // session update
						MemberVo memberRefresh = memberDao.getLogInMemberDetail(member.getEmail());
						session.setAttribute("accessUser", memberRefresh);

						rediectAttr.addFlashAttribute("payMethod", "신용카드");
						rediectAttr.addFlashAttribute("payDate", payDate);
						rediectAttr.addFlashAttribute("freeFlag", 1);		// billingFree ------------------------------------------------------무료 체험하지 않은 사용자

                        return "redirect:/login/krSignupComplete";					// 결제 성공 시 이동 할 url

                        //////////////////////////////////
                    } else {

                        //결제보안키가 다른 경우
                        if (!secureSignature.equals(resultMap.get("authSignature")) && "0000".equals(resultMap.get("resultCode"))) {

                            //망취소
                            if ("0000".equals(resultMap.get("resultCode"))) {
                                throw new Exception("데이터 위변조 체크 실패");
                            }
                        }
                    }

                    // 수신결과를 파싱후 resultCode가 "0000"이면 승인성공 이외 실패
                    // 가맹점에서 스스로 파싱후 내부 DB 처리 후 화면에 결과 표시

                    // payViewType을 popup으로 해서 결제를 하셨을 경우
                    // 내부처리후 스크립트를 이용해 opener의 화면 전환처리를 하세요

                    //throw new Exception("강제 Exception");
                } catch (Exception ex) {

                    //####################################
                    // 실패시 처리(***가맹점 개발수정***)
                    //####################################

                    //---- db 저장 실패시 등 예외처리----//
                    logger.error(ex.getMessage());

                    //#####################
                    // 망취소 API
                    //#####################
//					String netcancelResultString = httpUtil.processHTTP(authMap, netCancel);	// 망취소 요청 API url(고정, 임의 세팅 금지)

                    ex.printStackTrace();
                }

            }else{

                //#############
                // 인증 실패시
                //#############
                logger.debug("message : " + resultMap.get("resultMsg"));
                logger.debug("moid : " + resultMap.get("MOID"));

                rediectAttr.addFlashAttribute("reason", resultMap.get("resultMsg"));
                rediectAttr.addFlashAttribute("moid", resultMap.get("MOID"));

                logger.debug(resultMap.toString());

                return "redirect:/login/billingFail";

            }

        }catch(Exception e){

            logger.debug(e.getMessage());
        }

        logger.debug("message : " + resultMap.get("resultMsg"));
        logger.debug("moid : " + resultMap.get("MOID"));

        rediectAttr.addAttribute("reason", resultMap.get("resultMsg"));
        rediectAttr.addAttribute("moid", resultMap.get("MOID").replace("mindslab01_", ""));

        logger.debug(resultMap.toString());

        return "redirect:/login/billingFail";

    }

	public String billingFreeRegistered(String email, HttpServletRequest request, RedirectAttributes rediectAttr) {
		Map<String, String> resultMap = new HashMap <>();

		HttpSession session = request.getSession();

		MemberVo member = (MemberVo)session.getAttribute("accessUser");
		Calendar cal = Calendar.getInstance();

		if(member == null){
			logger.info("결제 user 세션 없음.");
			try {
				member = memberService.getMemberDetail(email);
				session.setAttribute("accessUser", member);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try{

			//#############################
			// 인증결과 파라미터 일괄 수신
			//#############################
			request.setCharacterEncoding("UTF-8");

			Map<String,String> paramMap = new Hashtable <String,String>();

			Enumeration elems = request.getParameterNames();

			String temp = "";

			while(elems.hasMoreElements())
			{
				temp = (String) elems.nextElement();
				paramMap.put(temp, request.getParameter(temp));
			}

			logger.debug("paramMap : "+ paramMap.toString());

			//#####################
			// 인증이 성공일 경우만
			//#####################
			if("0000".equals(paramMap.get("resultCode"))){

				logger.debug("paramMap : " + paramMap.toString());

				//############################################
				// 1.전문 필드 값 설정(***가맹점 개발수정***)
				//############################################

				String mid 		= paramMap.get("mid");						// 가맹점 ID 수신 받은 데이터로 설정
				String signKey	= "LzFjQlVaakQ1VWl3VkZCZy9BTnhxUT09";		// 가맹점에 제공된 키(이니라이트키) (가맹점 수정후 고정) !!!절대!! 전문 데이터로 설정금지
				String timestamp= SignatureUtil.getTimestamp();				// util에 의해서 자동생성
				String charset 	= "UTF-8";								    // 리턴형식[UTF-8,EUC-KR](가맹점 수정후 고정)
				String format 	= "JSON";								    // 리턴형식[XML,JSON,NVP](가맹점 수정후 고정)
				String authToken= paramMap.get("authToken");			    // 취소 요청 tid에 따라서 유동적(가맹점 수정후 고정)
				String authUrl	= paramMap.get("authUrl");				    // 승인요청 API url(수신 받은 값으로 설정, 임의 세팅 금지)
				String netCancel= paramMap.get("netCancelUrl");			 	// 망취소 API url(수신 받은 값으로 설정, 임의 세팅 금지)
				String ackUrl 	= paramMap.get("checkAckUrl");			    // 가맹점 내부 로직 처리후 최종 확인 API URL(수신 받은 값으로 설정, 임의 세팅 금지)
				String merchantData = paramMap.get("merchantData");			// 가맹점 관리데이터 수신

				//#####################
				// 2.signature 생성
				//#####################
				Map<String, String> signParam = new HashMap<String, String>();

				signParam.put("authToken",	authToken);		// 필수
				signParam.put("timestamp",	timestamp);		// 필수

				// signature 데이터 생성 (모듈에서 자동으로 signParam을 알파벳 순으로 정렬후 NVP 방식으로 나열해 hash)
				String signature = SignatureUtil.makeSignature(signParam);

				//#####################
				// 3.API 요청 전문 생성
				//#####################
				Map<String, String> authMap = new Hashtable<String, String>();

				authMap.put("mid"			,mid);			// 필수
				authMap.put("authToken"		,authToken);	// 필수
				authMap.put("signature"		,signature);	// 필수
				authMap.put("timestamp"		,timestamp);	// 필수
				authMap.put("charset"		,charset);		// default=UTF-8
				authMap.put("format"		,format);		// default=XML
				//authMap.put("price" 		,price);		// 가격위변조체크기능 (선택사용)

				System.out.println("##승인요청 API 요청##");

				HttpUtil httpUtil = new HttpUtil();

				try{
					//#####################
					// 4.API 통신 시작
					//#####################

					String authResultString = "";

					authResultString = httpUtil.processHTTP(authMap, authUrl);

					//############################################################
					//5.API 통신결과 처리(***가맹점 개발수정***)
					//############################################################
					String test = authResultString.replace(",", "&")
							.replace(":", "=")
							.replace("\"", "")
							.replace(" ","")
							.replace("\n", "")
							.replace("}", "")
							.replace("{", "");

					//retrunHtml = retrunHtml + "<pre>"+authResultString.replaceAll("<", "&lt;").replaceAll(">", "&gt;")+"</pre>";

					resultMap = ParseUtil.parseStringToMap(test); //문자열을 MAP형식으로 파싱


					logger.debug("resultMap == {}", resultMap);

					/*************************  결제보안 강화 2016-05-18 START ****************************/
					Map<String , String> secureMap = new HashMap<String, String>();
					secureMap.put("mid"			, mid);								//mid
					secureMap.put("tstamp"		, timestamp);						//timestemp
					secureMap.put("MOID"		, resultMap.get("MOID"));			//MOID
					secureMap.put("TotPrice"	, resultMap.get("TotPrice"));		//TotPrice

					// signature 데이터 생성
					String secureSignature = SignatureUtil.makeSignatureAuth(secureMap);
					/*************************  결제보안 강화 2016-05-18 END ****************************/

					if("0000".equals(resultMap.get("resultCode")) && secureSignature.equals(resultMap.get("authSignature")) ){	//결제보안 강화 2016-05-18

						PaymentVo payment = new PaymentVo();
						LogPayVo logPayVo = new LogPayVo();
						logPayVo.setType("Payment");

						payment.setUserNo(member.getUserno());
						logPayVo.setUserNo(member.getUserno());
						payment.setPayment("Free");
						logPayVo.setPayment("Free");
						payment.setIssuer(resultMap.get("CARD_Code"));
						logPayVo.setCode(resultMap.get("CARD_Code"));
						payment.setBillingKey(resultMap.get("CARD_BillKey"));
						logPayVo.setBillKey(resultMap.get("CARD_BillKey"));
						payment.setCardNo(resultMap.get("CARD_Num") + "****");
						logPayVo.setCardNumber(resultMap.get("CARD_Num") + "****");
						payment.setStatus("00");
						logPayVo.setStatus(0);
						payment.setGoodCode(paymentDao.selectProductId(resultMap.get("goodName")));
						logPayVo.setProduct(paymentDao.selectProductId(resultMap.get("goodName")));
						payment.setPrice(Integer.parseInt(resultMap.get("TotPrice")));
						logPayVo.setPrice(Integer.parseInt(resultMap.get("TotPrice")));
						payment.setPano(resultMap.get("MOID"));
						logPayVo.setPaNo(resultMap.get("MOID"));
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

						payment.setPayDate(sdf.format(cal.getTime()));
						String memberCreateDate= memberService.getUserCreateDate(member.getUserno());
						Date date = sdf.parse(memberCreateDate);
						payment.setDateFrom(sdf.format(date));
						logPayVo.setDateFrom(sdf.format(date));

						cal.setTime(date);
						cal.add(Calendar.MONTH, 1);
						payment.setDateTo(sdf.format(cal.getTime()));
						logPayVo.setDateTo(sdf.format(cal.getTime()));
						payment.setTid(resultMap.get("tid"));

						logger.debug(payment.toString());
						paymentDao.commonBilling(payment);
						payment.setPayment("Card");
						paymentDao.billInfoChange(payment);

						// LogPay_t insert - 2019. 12. 26 - LYJ
						paymentDao.insertLogPay(logPayVo);
						logger.info("Insert LogPay_t @ routineBill success : userNo" + logPayVo.getUserNo());

						// User Status update 19. 09. 20 LYJ
						MemberForm memberForm = new MemberForm();
						memberForm.setUserNo(member.getUserno());
						memberForm.setStatus(UserStatus.FREE);
						memberDao.updateUserStatus(memberForm);

						/* Free 고객 환영 메일 - 2019. 11. 15 - LYJ */
						mailSender.sendFreeUserMail(member.getEmail());

						// billing_t paymentDate(다음 결제일) 설정 - 2019. 10. 19 LYJ
						BillingVo billingVo = new BillingVo();
						billingVo.setUserNo(member.getUserno());
						billingVo.setPaymentDate(sdf.format(cal.getTime()));
						paymentDao.updatePaymentDate(billingVo);

						String payDate = paymentDao.selectPayDate(member.getUserno());

						// session update
						MemberVo memberRefresh = memberDao.getLogInMemberDetail(member.getEmail());
						session.setAttribute("accessUser", memberRefresh);

						rediectAttr.addFlashAttribute("payMethod", "신용카드");
						rediectAttr.addFlashAttribute("payDate", payDate);
						rediectAttr.addFlashAttribute("freeFlag", 1);		// billingFree ------------------------------------------------------무료 체험하지 않은 사용자

						return "redirect:/login/reSignupComplete";					// 결제 성공 시 이동 할 url

						//////////////////////////////////
					} else {

						//결제보안키가 다른 경우
						if (!secureSignature.equals(resultMap.get("authSignature")) && "0000".equals(resultMap.get("resultCode"))) {

							//망취소
							if ("0000".equals(resultMap.get("resultCode"))) {
								throw new Exception("데이터 위변조 체크 실패");
							}
						}
					}

					// 수신결과를 파싱후 resultCode가 "0000"이면 승인성공 이외 실패
					// 가맹점에서 스스로 파싱후 내부 DB 처리 후 화면에 결과 표시

					// payViewType을 popup으로 해서 결제를 하셨을 경우
					// 내부처리후 스크립트를 이용해 opener의 화면 전환처리를 하세요

					//throw new Exception("강제 Exception");
				} catch (Exception ex) {

					//####################################
					// 실패시 처리(***가맹점 개발수정***)
					//####################################

					//---- db 저장 실패시 등 예외처리----//
					logger.error(ex.getMessage());

					//#####################
					// 망취소 API
					//#####################
//					String netcancelResultString = httpUtil.processHTTP(authMap, netCancel);	// 망취소 요청 API url(고정, 임의 세팅 금지)

					ex.printStackTrace();
				}

			}else{

				//#############
				// 인증 실패시
				//#############
				logger.debug("message : " + resultMap.get("resultMsg"));
				logger.debug("moid : " + resultMap.get("MOID"));

				rediectAttr.addFlashAttribute("reason", resultMap.get("resultMsg"));
				rediectAttr.addFlashAttribute("moid", resultMap.get("MOID"));

				logger.debug(resultMap.toString());

				return "redirect:/login/billingFail";

			}

		}catch(Exception e){

			logger.debug(e.getMessage());
		}

		logger.debug("message : " + resultMap.get("resultMsg"));
		logger.debug("moid : " + resultMap.get("MOID"));

		rediectAttr.addAttribute("reason", resultMap.get("resultMsg"));
		rediectAttr.addAttribute("moid", resultMap.get("MOID").replace("mindslab01_", ""));

		logger.debug(resultMap.toString());

		return "redirect:/login/billingFail";

	}

	public String billingPay(String email, HttpServletRequest request, RedirectAttributes rediectAttr) {
		Map<String, String> resultMap = new HashMap <>();

		HttpSession session = request.getSession();
		MemberVo member = (MemberVo)session.getAttribute("accessUser");
		Calendar cal = Calendar.getInstance();

		if(member == null){
			logger.info("결제 user 세션 없음.");
			try {
				member = memberService.getMemberDetail(email);
				session.setAttribute("accessUser", member);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try{

			//#############################
			// 인증결과 파라미터 일괄 수신
			//#############################
			request.setCharacterEncoding("UTF-8");

			Map<String,String> paramMap = new Hashtable <String,String>();

			Enumeration elems = request.getParameterNames();

			String temp = "";

			while(elems.hasMoreElements())
			{
				temp = (String) elems.nextElement();
				paramMap.put(temp, request.getParameter(temp));
			}

			logger.debug("paramMap : "+ paramMap.toString());

			//#####################
			// 인증이 성공일 경우만
			//#####################
			if("0000".equals(paramMap.get("resultCode"))){

				logger.debug("paramMap : " + paramMap.toString());

				//############################################
				// 1.전문 필드 값 설정(***가맹점 개발수정***)
				//############################################

				String mid 		= paramMap.get("mid");						// 가맹점 ID 수신 받은 데이터로 설정
				String signKey	= "LzFjQlVaakQ1VWl3VkZCZy9BTnhxUT09";		// 가맹점에 제공된 키(이니라이트키) (가맹점 수정후 고정) !!!절대!! 전문 데이터로 설정금지
				String timestamp= SignatureUtil.getTimestamp();				// util에 의해서 자동생성
				String charset 	= "UTF-8";								    // 리턴형식[UTF-8,EUC-KR](가맹점 수정후 고정)
				String format 	= "JSON";								    // 리턴형식[XML,JSON,NVP](가맹점 수정후 고정)
				String authToken= paramMap.get("authToken");			    // 취소 요청 tid에 따라서 유동적(가맹점 수정후 고정)
				String authUrl	= paramMap.get("authUrl");				    // 승인요청 API url(수신 받은 값으로 설정, 임의 세팅 금지)
				String netCancel= paramMap.get("netCancelUrl");			 	// 망취소 API url(수신 받은 값으로 설정, 임의 세팅 금지)
				String ackUrl 	= paramMap.get("checkAckUrl");			    // 가맹점 내부 로직 처리후 최종 확인 API URL(수신 받은 값으로 설정, 임의 세팅 금지)
				String merchantData = paramMap.get("merchantData");			// 가맹점 관리데이터 수신

				//#####################
				// 2.signature 생성
				//#####################
				Map<String, String> signParam = new HashMap<String, String>();

				signParam.put("authToken",	authToken);		// 필수
				signParam.put("timestamp",	timestamp);		// 필수

				// signature 데이터 생성 (모듈에서 자동으로 signParam을 알파벳 순으로 정렬후 NVP 방식으로 나열해 hash)
				String signature = SignatureUtil.makeSignature(signParam);

				//#####################
				// 3.API 요청 전문 생성
				//#####################
				Map<String, String> authMap = new Hashtable<String, String>();

				authMap.put("mid"			,mid);			// 필수
				authMap.put("authToken"		,authToken);	// 필수
				authMap.put("signature"		,signature);	// 필수
				authMap.put("timestamp"		,timestamp);	// 필수
				authMap.put("charset"		,charset);		// default=UTF-8
				authMap.put("format"		,format);		// default=XML
				//authMap.put("price" 		,price);		// 가격위변조체크기능 (선택사용)

				System.out.println("##승인요청 API 요청##");

				HttpUtil httpUtil = new HttpUtil();

				try{
					//#####################
					// 4.API 통신 시작
					//#####################

					String authResultString = "";

					authResultString = httpUtil.processHTTP(authMap, authUrl);

					//############################################################
					//5.API 통신결과 처리(***가맹점 개발수정***)
					//############################################################
					String test = authResultString.replace(",", "&").replace(":", "=").replace("\"", "").replace(" ","").replace("\n", "").replace("}", "").replace("{", "");

					//retrunHtml = retrunHtml + "<pre>"+authResultString.replaceAll("<", "&lt;").replaceAll(">", "&gt;")+"</pre>";

					resultMap = ParseUtil.parseStringToMap(test); //문자열을 MAP형식으로 파싱


					logger.debug("resultMap == " + resultMap);

					/*************************  결제보안 강화 2016-05-18 START ****************************/
					Map<String , String> secureMap = new HashMap<String, String>();
					secureMap.put("mid"			, mid);								//mid
					secureMap.put("tstamp"		, timestamp);						//timestemp
					secureMap.put("MOID"		, resultMap.get("MOID"));			//MOID
					secureMap.put("TotPrice"	, resultMap.get("TotPrice"));		//TotPrice

					// signature 데이터 생성
					String secureSignature = SignatureUtil.makeSignatureAuth(secureMap);
					/*************************  결제보안 강화 2016-05-18 END ****************************/

					if("0000".equals(resultMap.get("resultCode")) && secureSignature.equals(resultMap.get("authSignature")) ){	//결제보안 강화 2016-05-18
						/*****************************************************************************
						 * 여기에 가맹점 내부 DB에 결제 결과를 반영하는 관련 프로그램 코드를 구현한다.

						 [중요!] 승인내용에 이상이 없음을 확인한 뒤 가맹점 DB에 해당건이 정상처리 되었음을 반영함
						 처리중 에러 발생시 망취소를 한다.
						 ******************************************************************************/

						// 빌링 결제 이니시스 전송 - YBU 2019.04.03
						INIpay inipay = new INIpay();
						INIdata data = new INIdata();
						String uip = this.getUip(request);

						data.setData("inipayHome", mPath_InipayHome);         // INIpay45 절대경로 (key / log 디렉토리) 추후 위치 수정 예정
						data.setData("logMode", "DEBUG");               // 로그모드 (INFO / DEBUG)
						data.setData("type", "reqrealbill");            // type (고정)
						data.setData("paymethod", "Card");              // 지불수단 (고정)
						data.setData("crypto", "execure");              // 암복호화모듈 (고정)

						data.setData("uip", uip);
						data.setData("url", "https://maum.ai");
						data.setData("mid", "mindslab01");
						data.setData("uid", "mindslab01");
						data.setData("keyPW", "1111");

						data.setData("moid", resultMap.get("MOID"));
						data.setData("goodname", resultMap.get("goodName"));
						data.setData("price", resultMap.get("TotPrice"));
						data.setData("currency", "WON");

						data.setData("buyername", resultMap.get("buyerName"));
						data.setData("buyertel", resultMap.get("buyerTel"));
						data.setData("buyeremail", resultMap.get("buyerEmail"));

						data.setData("cardquota", resultMap.get("CARD_Quota"));
						data.setData("quotaInterest", "0");
						data.setData("billkey", resultMap.get("CARD_BillKey"));
						data.setData("authentification", "00");
						INIdata resultData = inipay.payRequest(data);
						logger.debug("INICIS RESULT DATA : " + resultData.toString());

						PaymentVo payment = new PaymentVo();
						LogPayVo logPayVo = new LogPayVo();
						logPayVo.setType("Payment");

						payment.setUserNo(member.getUserno());
						logPayVo.setUserNo(member.getUserno());
						payment.setPayment(resultData.getData("PayMethod"));
						logPayVo.setPayment(resultData.getData("PayMethod"));
						payment.setIssuer(resultData.getData("CardResultCode"));
						logPayVo.setCode(resultData.getData("CardResultCode"));
						payment.setBillingKey(resultData.getData("billkey"));
						logPayVo.setBillKey(resultData.getData("billkey"));
						payment.setCardNo(resultData.getData("CardResultNumber") + "****");
						logPayVo.setCardNumber(resultData.getData("CardResultNumber") + "****");
						payment.setStatus(resultData.getData("ResultCode"));
						logPayVo.setStatus(0);
						payment.setGoodCode(paymentDao.selectProductId(resultData.getData("goodname")));
						logPayVo.setProduct(paymentDao.selectProductId(resultData.getData("goodname")));
						payment.setPrice(Integer.parseInt(resultData.getData("Price")));
						logPayVo.setPrice(Integer.parseInt(resultData.getData("Price")));
						payment.setPano(resultData.getData("moid"));
						logPayVo.setPaNo(resultData.getData("moid"));
						payment.setPayDate(resultData.getData("PGauthdate"));
						SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
						payment.setDateFrom(sdf.format(cal.getTime()));
						logPayVo.setDateFrom(sdf.format(cal.getTime()));
						cal.add(Calendar.MONTH, 1);
						payment.setDateTo(sdf.format(cal.getTime()));
						logPayVo.setDateTo(sdf.format(cal.getTime()));
						payment.setTid(resultData.getData("tid"));

						if (resultData.getData("ResultCode").equals("00")) {

							logger.debug(payment.toString());
							paymentDao.commonBilling(payment);
//							this.cancelPay(request, resultData.getData("tid"));

							// LogPay_t insert - 2019. 12. 26 - LYJ
							paymentDao.insertLogPay(logPayVo);
							logger.info("Insert LogPay_t @ billingPay success : userNo" + logPayVo.getUserNo());

							// 사용자 payCount update - 2020. 01. 20 - LYJ
							memberDao.increaseUserPayCount(member.getUserno());
							int userPayCount = memberDao.getUserPayCount(member.getUserno());
							logger.debug(" @ billingPay payCount update success : " + member.getEmail() + " / update payCount = " + userPayCount);

                            // User Status update - 19. 09. 20 - LYJ
                            MemberForm memberForm = new MemberForm();
                            memberForm.setUserNo(member.getUserno());
                            memberForm.setStatus(UserStatus.SUBSCRIBED);
                            memberDao.updateUserStatus(memberForm);

							// billing_t paymentDate(다음 결제일) 설정 - 2019. 10. 19 LYJ
							BillingVo billingVo = new BillingVo();
							billingVo.setUserNo(member.getUserno());
							billingVo.setPaymentDate(sdf.format(cal.getTime()));
							paymentDao.updatePaymentDate(billingVo);

							// 결제 완료 메일 전송 - 2020. 11. 13 - MRS, LYJ
							payment.setUserEmail(email);
							payment.setIssuerName(selectIssuerName(Integer.parseInt(payment.getIssuer())));
							payment.setPaymentDate(billingVo.getPaymentDate());
							mailSender.sendPaymentCompleteMail(payment, PaymentPlan.BUSINESS);

							/* 재구독 고객 환영 메일 - 2020. 05. 12 - LYJ */
							//mailSender.sendSubscribeUserMail(member.getEmail());

                            String payDate = paymentDao.selectPayDate(member.getUserno());

							MemberVo memberRefresh = memberDao.getLogInMemberDetail(member.getEmail());
							session.setAttribute("accessUser", memberRefresh);

							rediectAttr.addFlashAttribute("payMethod", "신용카드");
                            rediectAttr.addFlashAttribute("payDate", payDate);
							rediectAttr.addFlashAttribute("freeFlag", 0);	// billingPay ------------------------------------------------------무료 체험 여부

							return "redirect:/login/krSignupComplete";              // 결제 성공 시 이동 할 url
//							return "redirect:/payment/payResult";					// 기존 결제 성공 시 url
						} else {

							logger.debug("paymentVo : " + payment.toString());

							String reason = resultData.getData("ResultDetailMsg");

							StringTokenizer st = new StringTokenizer(reason, "|");
							st.nextToken();

							// LogPay_t insert - 2019. 12. 26 - LYJ
							logPayVo.setFailReason(reason);
							logPayVo.setStatus(-1);
							paymentDao.insertLogPay(logPayVo);
							logger.info("Insert LogPay_t @ billingPay fail : userNo" + logPayVo.getUserNo());

							rediectAttr.addAttribute("reason", reason);
							rediectAttr.addAttribute("moid", "");

							//rediectAttr.addFlashAttribute("reason", st.nextToken()).addFlashAttribute("moid", resultData.getData("moid"));

							return "redirect:/login/billingFail";
							//return "redirect:/support/krPricingFail";			// 결제 실패 시 이동 할 view
						}

						//////////////////////////////////
					} else {

						//결제보안키가 다른 경우
						if (!secureSignature.equals(resultMap.get("authSignature")) && "0000".equals(resultMap.get("resultCode"))) {

							//auth signature 가 어떤 상황(사용자의 입력 정보 오류 or 시그니처 변경)에 의해 달라졌을 경우, 망취소
							if ("0000".equals(resultMap.get("resultCode"))) {
								throw new Exception("데이터 위변조 체크 실패");
							}
						}
					}

					// 수신결과를 파싱후 resultCode가 "0000"이면 승인성공 이외 실패
					// 가맹점에서 스스로 파싱후 내부 DB 처리 후 화면에 결과 표시

					// payViewType을 popup으로 해서 결제를 하셨을 경우
					// 내부처리후 스크립트를 이용해 opener의 화면 전환처리를 하세요

					//throw new Exception("강제 Exception");
				} catch (Exception ex) {

					//####################################
					// 실패시 처리(***가맹점 개발수정***)
					//####################################

					//---- db 저장 실패시 등 예외처리----//
					logger.error(ex.getMessage());

					//#####################
					// 망취소 API
					//#####################
//					String netcancelResultString = httpUtil.processHTTP(authMap, netCancel);	// 망취소 요청 API url(고정, 임의 세팅 금지)

					ex.printStackTrace();
				}

			}else{
				//#############
				// 인증 실패시
				//#############
				logger.debug("message : " + resultMap.get("resultMsg"));
				logger.debug("moid : " + resultMap.get("MOID"));

				rediectAttr.addFlashAttribute("reason", resultMap.get("resultMsg"));
				rediectAttr.addFlashAttribute("moid", resultMap.get("MOID"));

				logger.debug(resultMap.toString());

				return "redirect:/login/billingFail";
				//return "redirect:/support/krPricingFail";

			}

		}catch(Exception e){
			logger.debug(e.getMessage());
		}
		logger.debug("message : " + resultMap.get("resultMsg"));
		logger.debug("moid : " + resultMap.get("MOID"));

		rediectAttr.addAttribute("reason", resultMap.get("resultMsg"));
		rediectAttr.addAttribute("moid", resultMap.get("MOID").replace("mindslab01_", ""));
		logger.debug(resultMap.toString());

		return "redirect:/login/billingFail";
		//return "redirect:/support/krPricingFail";

	}

	public String billingChange(String email,HttpServletRequest request, RedirectAttributes rediectAttr) throws Exception{

		logger.info("{} .billingChange()", getClass().getSimpleName());
		Map<String, String> resultMap = new HashMap <>();

		HttpSession session = request.getSession();
		MemberVo member = (MemberVo)session.getAttribute("accessUser");
		if(member == null) {
			logger.error("memberVo(session value) is null !");
			try {
				member = memberService.getMemberDetail(email);
				session.setAttribute("accessUser", member);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try {

			//#############################
			// 인증결과 파라미터 일괄 수신
			//#############################
			request.setCharacterEncoding("UTF-8");

			Map <String, String> paramMap = new Hashtable <String, String>();

			Enumeration elems = request.getParameterNames();

			String temp = "";

			while (elems.hasMoreElements()) {
				temp = (String) elems.nextElement();
				paramMap.put(temp, request.getParameter(temp));
			}

			logger.debug("paramMap : " + paramMap.toString());

			//#####################
			// 인증이 성공일 경우만
			//#####################
			if ("0000".equals(paramMap.get("resultCode"))) {

				logger.debug("paramMap : " + paramMap.toString());

				//############################################
				// 1.전문 필드 값 설정(***가맹점 개발수정***)
				//############################################

				String mid = paramMap.get("mid");                        // 가맹점 ID 수신 받은 데이터로 설정
				String signKey = "LzFjQlVaakQ1VWl3VkZCZy9BTnhxUT09";        // 가맹점에 제공된 키(이니라이트키) (가맹점 수정후 고정) !!!절대!! 전문 데이터로 설정금지
				String timestamp = SignatureUtil.getTimestamp();                // util에 의해서 자동생성
				String charset = "UTF-8";                                    // 리턴형식[UTF-8,EUC-KR](가맹점 수정후 고정)
				String format = "JSON";                                    // 리턴형식[XML,JSON,NVP](가맹점 수정후 고정)
				String authToken = paramMap.get("authToken");                // 취소 요청 tid에 따라서 유동적(가맹점 수정후 고정)
				String authUrl = paramMap.get("authUrl");                    // 승인요청 API url(수신 받은 값으로 설정, 임의 세팅 금지)
				String netCancel = paramMap.get("netCancelUrl");                // 망취소 API url(수신 받은 값으로 설정, 임의 세팅 금지)
				String ackUrl = paramMap.get("checkAckUrl");                // 가맹점 내부 로직 처리후 최종 확인 API URL(수신 받은 값으로 설정, 임의 세팅 금지)
				String merchantData = paramMap.get("merchantData");            // 가맹점 관리데이터 수신

				//#####################
				// 2.signature 생성
				//#####################
				Map <String, String> signParam = new HashMap <String, String>();

				signParam.put("authToken", authToken);        // 필수
				signParam.put("timestamp", timestamp);        // 필수

				// signature 데이터 생성 (모듈에서 자동으로 signParam을 알파벳 순으로 정렬후 NVP 방식으로 나열해 hash)
				String signature = SignatureUtil.makeSignature(signParam);

				//#####################
				// 3.API 요청 전문 생성
				//#####################
				Map <String, String> authMap = new Hashtable <String, String>();

				authMap.put("mid", mid);            // 필수
				authMap.put("authToken", authToken);    // 필수
				authMap.put("signature", signature);    // 필수
				authMap.put("timestamp", timestamp);    // 필수
				authMap.put("charset", charset);        // default=UTF-8
				authMap.put("format", format);        // default=XML

				System.out.println("##승인요청 API 요청##");

				HttpUtil httpUtil = new HttpUtil();

				try {
					//#####################
					// 4.API 통신 시작
					//#####################

					String authResultString = "";

					authResultString = httpUtil.processHTTP(authMap, authUrl);

					//############################################################
					//5.API 통신결과 처리(***가맹점 개발수정***)
					//############################################################
					String test = authResultString.replace(",", "&").replace(":", "=").replace("\"", "").replace(" ", "").replace("\n", "").replace("}", "").replace("{", "");

					resultMap = ParseUtil.parseStringToMap(test); //문자열을 MAP형식으로 파싱

					logger.debug("resultMap == " + resultMap);
					String resultCode = "";

					if ( "0000".equals(resultMap.get("resultCode")) ) {
						PaymentVo payment = new PaymentVo();
						payment.setUserNo(member.getUserno());
						payment.setPayment("Card");
						payment.setIssuer(resultMap.get("CARD_Code"));
						payment.setBillingKey(resultMap.get("CARD_BillKey"));
						payment.setCardNo(resultMap.get("CARD_Num") + "****");
						payment.setStatus("00");
						payment.setTid(resultMap.get("tid"));

						paymentDao.billInfoChange(payment);
						String payDate = paymentDao.selectPayDate(member.getUserno());

						rediectAttr.addFlashAttribute("payMethod", "신용카드");
						rediectAttr.addFlashAttribute("payDate", payDate);
						//return "redirect:/user/krPaymentInfoMain";
						return "redirect:/login/reSignupComplete";
					}else{

						//#############
						// 인증 실패시
						//#############
						logger.debug("message : " + resultMap.get("resultMsg"));
						logger.debug("moid : " + resultMap.get("MOID"));

						rediectAttr.addAttribute("reason", resultMap.get("resultMsg"));
						rediectAttr.addAttribute("moid", resultMap.get("MOID"));

						logger.debug(resultMap.toString());

						return "redirect:/login/reBillingFail";

					}

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "redirect:/support/pricing";

	}

//	public void cancelPay(HttpServletRequest request, String tid) {
//		String uip =   this.getUip(request);
//
//		cancelPay(uip, tid);
//	}
//
//	public void cancelPay(String uip, String tid) {
//		INIpay inipay = new INIpay();
//		INIdata data = new INIdata();
//
//		data.setData("inipayHome", inipayHome);         // INIpay45 절대경로 (key / log 디렉토리) 추후 위치 수정 예정
//		data.setData("logMode", "DEBUG");               // 로그모드 (INFO / DEBUG)
//		data.setData("type", "cancel");                 // type (고정)
//		data.setData("crypto", "execure");
//		data.setData("uip", uip);
//		data.setData("url", "https://maum.ai");
//		data.setData("mid", "mindslab01");
//		data.setData("uid", "mindslab01");
//		data.setData("keyPW", "1111");
//		data.setData("tid", tid);
//
//		INIdata resultData = inipay.payRequest(data);
//		logger.debug(resultData.toString());
//	}

	public String getUip(HttpServletRequest request) {

		String uip = request.getHeader("X-FORWARDED-FOR");

		if (uip == null) {
			uip = request.getHeader("Proxy-Client-IP");
		}

		if (uip == null) {
			uip = request.getHeader("WL-Proxy-Client-IP");
		}

		if (uip == null) {
			uip = request.getHeader("HTTP_CLIENT_IP");
		}

		if (uip == null) {
			uip = request.getHeader("HTTP_X_FORWARDED_FOR");
		}

		if (uip == null) {
			uip = request.getRemoteAddr();
		}

		return uip;
	}

	public int planConfirm(PaymentVo paymentVo) {
		return paymentDao.planConfirm(paymentVo);
	}

	public int selectProductId(String productName) {
		return paymentDao.selectProductId(productName);
	}

	public String selectIssuerName(int issuer) {
		return paymentDao.selectIssuerName(issuer);
	}

	public String selectProductName(int goodCode) {
		return paymentDao.selectProductName(goodCode);
	}

	public void planChange(PaymentVo payment) {
		paymentDao.planChange(payment);
	}

	public int planChangeConfirm(PaymentVo paymentVo) {	return paymentDao.planChangeConfirm(paymentVo); }

	public List <PaymentVo> selectRegularList() {
    	return paymentDao.selectRegularList();
    }

    public void planCancelProcedure(int userNo) {
        paymentDao.planCancel(userNo);
    }


    public void insertFeedbackOnCancel(int userNo, int feedbackUsability, int feedbackDemo, String feedbackEtc){

		logger.info("{} .feedbackOnCancel()", getClass().getSimpleName());

		try{
			UnsubsFeedbackVo feedback = new UnsubsFeedbackVo();
			feedback.setUserNo(userNo);
			feedback.setFeedbackUsability(feedbackUsability);
			feedback.setFeedbackDemo(feedbackDemo);
			feedback.setFeedbackEtc(feedbackEtc);
			memberDao.insertUnsubsFeedback(feedback);
		}
		catch(Exception e){
			logger.info("Insert feedback FAIL");
			logger.error(e.toString());
		}

	}

	public Map<String, Object> planCancel(int userNo) {

		logger.info("{} .planCancel() :: 이니시스", getClass().getSimpleName());

		Map<String, Object> returnMap = new HashMap<String, Object>();

        MemberForm memberForm = new MemberForm();  // User life cycle 관리 19. 09. 30 LYJ
        memberForm.setUserNo(userNo);

		MemberForm detailForm = memberDao.getUserInfoByNo(memberForm);

        // '구독해지' 시, status 변경
		// User Status update 19. 09. 20 LYJ
		memberForm.setStatus(UserStatus.UNSUBSCRIBING);
		memberDao.updateUserStatus(memberForm);

		/* 구독 해지 신청한 고객 안내 메일
		 * Modified : 국문/영문 분기 - 2020. 05. 11 - LYJ
		 * */
		try {
			String paymentMethodStr = getPaymentMethod(userNo);
			if( paymentMethodStr == null || "Card".equalsIgnoreCase(paymentMethodStr))
				mailSender.sendReqCancelUserMail(detailForm.getEmail());
			else if( "PayPal".equalsIgnoreCase(paymentMethodStr) )
				mailSender.enSendReqCancelUserMail(detailForm.getEmail());

		} catch (Exception e) {
			logger.info("planCancel Error ==> User : " + detailForm.getEmail() + ", Reason : " + e);
		}

		logger.info("function planCancel's event : --> unsubscribing");
		returnMap.put("msg", "SUCCESS");
		return returnMap;

//		  무료 사용자 '구독해지' 시, 즉시 해제
//        int userStatus = memberDao.getUserStatus(memberForm);
//        if(userStatus == UserStatus.FREE || userStatus == UserStatus.INIT) {
//            MemberForm resultMemberForm;
//            resultMemberForm = memberDao.getUserInfoByNo( memberForm );
//            try {
//				if(resultMemberForm.getApiId() != null) {
//					Map<String, String> apiAccountResultMap;
//					apiAccountResultMap = apiKeyService.pauseApiId( resultMemberForm.getEmail() );    // 구독 pause
//					logger.info("API account pause result : {}", apiAccountResultMap.toString());
//				}
//				memberForm.setStatus(UserStatus.UNSUBSCRIBING);    // Status 값 변경 ---  20210316 SMR 수정
//                memberDao.updateUserStatus(memberForm);
//                planCancelProcedure(userNo);
//				deleteBillInfo(userNo); // 사용자의 결제 정보 삭제
//
//				/* 구독 해지된 고객 안내 메일 -> 무료 고객 - 2019. 11. 15 - LYJ */
//				mailSender.sendExpiredUserMail(detailForm.getEmail());
//
//                logger.info("function planCancel's event : {}  --> unsubscribed", resultMemberForm.getEmail());
//                returnMap.put("msg", "SUCCESS");
//
//                return returnMap;
//
//            } catch (Exception e) {
//                logger.info("function planCancel's event : {}", e);
//				returnMap.put("msg", "FAIL");
//				returnMap.put("errMsg", e);
//
//				return returnMap;
//            }
//
//        } else {
//            // User Status update 19. 09. 20 LYJ
//            memberForm.setStatus(UserStatus.UNSUBSCRIBING);
//            memberDao.updateUserStatus(memberForm);
//
//			/* 구독 해지 신청한 고객 안내 메일 -> 유료 고객 - 2019. 11. 15 - LYJ
//			* Modified : 국문/영문 분기 - 2020. 05. 11 - LYJ
//			* */
//			try {
//				String paymentMethodStr = getPaymentMethod(userNo);
//				if( "Card".equalsIgnoreCase(paymentMethodStr) )
//					mailSender.sendReqCancelUserMail(detailForm.getEmail());
//				else if( "PayPal".equalsIgnoreCase(paymentMethodStr) )
//					mailSender.enSendReqCancelUserMail(detailForm.getEmail());
//
//			} catch (Exception e) {
//				logger.info("planCancel Error ==> User : " + detailForm.getEmail() + ", Reason : " + e);
//			}
//
//			logger.info("function planCancel's event : --> unsubscribing");
//			returnMap.put("msg", "SUCCESS");
//			return returnMap;
//        }
    }

	/* Paypal Subscription Cancel */
    public Map<String, Object> paypalPlanCancel(int userNo){
		logger.info("{} .paypalPlanCancel() :: 페이팔", getClass().getSimpleName());

		Map<String, Object> returnMap = new HashMap<String, Object>();

		MemberForm memberForm = new MemberForm();
		memberForm.setUserNo(userNo);

		// 무료 사용자 '구독해지' 시, 즉시 해제
		int userStatus = memberDao.getUserStatus(memberForm);
		String subscId = paymentDao.getBillKey(userNo);

		/* 정책 변경으로 free 사용자도 unsubscribing 으로 변경 됨 2021.03.22. SMR YGE
		if(userStatus == UserStatus.FREE) {

			MemberForm resultMemberForm;
			resultMemberForm = memberDao.getUserInfoByNo( memberForm );

			try {

			    if(resultMemberForm.getApiId() != null) {
			    	Map<String, String> apiAccountResultMap;
					apiAccountResultMap = apiKeyService.pauseApiId( resultMemberForm.getEmail() );    // 구독 pause
					logger.info("API account pause result : {}", apiAccountResultMap.toString());
				}

				paypalService.subscCancelAPI(subscId); // Cancel API

				memberForm.setStatus(UserStatus.UNSUBSCRIBED);    // Status 값 변경
				memberDao.updateUserStatus(memberForm);


				*//* IPN에서 결제 정보(Billin	g_t) 삭제 및 메일 전송할 것임 *//*

				logger.info("Paypal plan cancel : {}  --> unsubscribed", resultMemberForm.getEmail());
				returnMap.put("msg", "SUCCESS");

				return returnMap;

			} catch (Exception e) {
				logger.info("Paypal plan cancel FAIL : {0}", e);
				returnMap.put("msg", "FAIL");
				returnMap.put("errMsg",e);

				return returnMap;
			}

		} else {*/
			try{
				paypalService.subscCancelAPI(subscId); // Cancel API
			}catch (Exception e){

				logger.error(subscId);/////////////////////////////////////////////////////////
				logger.info("Paypal plan cancel --- unsubscribing FAIL : {0}", e);
				returnMap.put("msg", "FAIL");
				returnMap.put("errMsg",e);

				return returnMap;
			}

			memberForm.setStatus(UserStatus.UNSUBSCRIBING);
			memberDao.updateUserStatus(memberForm);

			/* 구독 해지된 고객 안내 메일 => 유료고객 - 2020. 04. 20 - LYJ */
			// IPN으로 메일 처리
			/*MemberForm detailForm = memberDao.getUserInfoByNo(memberForm);
			try {
				mailSender.enSendReqCancelUserMail( detailForm.getEmail() );
				//mailSender.sendReqCancelUserMail(detailForm.getEmail());
			} catch (Exception e) {
				logger.info("paypalPlanCancel Error ==> User : {}, Reason : {}",detailForm.getEmail(), e);
			}*/

			logger.info("Paypal plan cancel : --> unsubscribing success");
			returnMap.put("msg", "SUCCESS");

			return returnMap;
		/*}*/
	}

	/*
	**
	*/
	public String mobilePay(HttpServletRequest request) {
		String param = null;

		SimpleDateFormat timestampFmt = new SimpleDateFormat("yyyyMMddHHmmss");
		SimpleDateFormat dateFmt = new SimpleDateFormat("yyyyMMdd");
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date());

		String beginDate = dateFmt.format(cal.getTime());
		cal.add(Calendar.MONTH, 1);
		String endDate = dateFmt.format(cal.getTime());

		request.getSession();
		MemberVo member = (MemberVo) request.getSession().getAttribute("accessUser");
		System.out.println("#####################################################  : "+ member.toString());

		try {
			// 인증결과 파라미터 일괄 수신
			request.setCharacterEncoding("UTF-8");

			Map<String,String> paramMap = new Hashtable<String,String>();

			Enumeration elems = request.getParameterNames();

			String temp = "";

			while(elems.hasMoreElements()) {
				temp = (String) elems.nextElement();
				paramMap.put(temp, request.getParameter(temp));
			}

			System.out.println("#####################################################  : "+ paramMap.toString());

			String mid = INICIS_MID;
			String oid = UUID.randomUUID().toString();
			String timestamp = timestampFmt.format(new Date());
			String mkey = INICIS_MKEY;
			String hashdata = null;

			/* HASH DATA 생성 */
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest((mid + oid + timestamp + mkey).getBytes("UTF-8"));
			StringBuffer hexString = new StringBuffer();

			for (int i = 0; i < hash.length; i++) {
				String hex = Integer.toHexString(0xff & hash[i]);
				if(hex.length() == 1) hexString.append('0');
				hexString.append(hex);
			}
			hashdata = hexString.toString();


			param = "mid=" + mid;
			param += "&buyername=" + member.getName();
			param += "&goodname=" + request.getParameter("goodname");
			param += "&price=" + request.getParameter("price");
			param += "&orderid=" + oid;
			param += "&returnurl=" + mUrl_Domain + "/payment/mobileBillingFree";
			param += "&merchantreserved=";
			param += "&timestamp=" + timestamp;
			param += "&period=" + beginDate + endDate;
			//param += "&period_custom=" + beginDate + endDate;
			// param += "carduse=";				// 설정하지 않으면, 사용자 선택 (법인 여부)
			param += "&p_noti=" + request.getParameter("product") + "," + request.getParameter("goodname") + "," + request.getParameter("price") /* 3,BUSINESS,99000 */;		// 응답시 돌려받을 데이타
			param += "&hashdata=" + hashdata;

		} catch(Exception ex){
			throw new RuntimeException(ex);
		}

    	return param;
	}

	public void commonPay(HttpServletRequest request) {
		try{

			//#############################
			// 인증결과 파라미터 일괄 수신
			//#############################
			request.setCharacterEncoding("UTF-8");

			Map<String,String> paramMap = new Hashtable<String,String>();

			Enumeration elems = request.getParameterNames();

			String temp = "";

			while(elems.hasMoreElements())
			{
				temp = (String) elems.nextElement();
				paramMap.put(temp, request.getParameter(temp));
			}

			System.out.println("paramMap : "+ paramMap.toString());

			//#####################
			// 인증이 성공일 경우만
			//#####################
			if("0000".equals(paramMap.get("resultCode"))){

				System.out.println("####인증성공/승인요청####");

				//############################################
				// 1.전문 필드 값 설정(***가맹점 개발수정***)
				//############################################

				String mid 		= paramMap.get("mid");						// 가맹점 ID 수신 받은 데이터로 설정
				String signKey	= "LzFjQlVaakQ1VWl3VkZCZy9BTnhxUT09";		// 가맹점에 제공된 키(이니라이트키) (가맹점 수정후 고정) !!!절대!! 전문 데이터로 설정금지
				String timestamp= SignatureUtil.getTimestamp();				// util에 의해서 자동생성
				String charset 	= "UTF-8";								    // 리턴형식[UTF-8,EUC-KR](가맹점 수정후 고정)
				String format 	= "JSON";								    // 리턴형식[XML,JSON,NVP](가맹점 수정후 고정)
				String authToken= paramMap.get("authToken");			    // 취소 요청 tid에 따라서 유동적(가맹점 수정후 고정)
				String authUrl	= paramMap.get("authUrl");				    // 승인요청 API url(수신 받은 값으로 설정, 임의 세팅 금지)
				String netCancel= paramMap.get("netCancelUrl");			 	// 망취소 API url(수신 받은 값으로 설정, 임의 세팅 금지)
				String ackUrl 	= paramMap.get("checkAckUrl");			    // 가맹점 내부 로직 처리후 최종 확인 API URL(수신 받은 값으로 설정, 임의 세팅 금지)
				String merchantData = paramMap.get("merchantData");			// 가맹점 관리데이터 수신

				//#####################
				// 2.signature 생성
				//#####################
				Map<String, String> signParam = new HashMap<String, String>();

				signParam.put("authToken",	authToken);		// 필수
				signParam.put("timestamp",	timestamp);		// 필수

				// signature 데이터 생성 (모듈에서 자동으로 signParam을 알파벳 순으로 정렬후 NVP 방식으로 나열해 hash)
				String signature = SignatureUtil.makeSignature(signParam);

				String price = "";  // 가맹점에서 최종 결제 가격 표기 (필수입력아님)

				// 1. 가맹점에서 승인시 주문번호가 변경될 경우 (선택입력) 하위 연결.
				// String oid = "";

				//#####################
				// 3.API 요청 전문 생성
				//#####################
				Map<String, String> authMap = new Hashtable<String, String>();

				authMap.put("mid"			,mid);			  // 필수
				authMap.put("authToken"		,authToken);	// 필수
				authMap.put("signature"		,signature);	// 필수
				authMap.put("timestamp"		,timestamp);	// 필수
				authMap.put("charset"		,charset);		// default=UTF-8
				authMap.put("format"		 ,format);		  // default=XML
				//authMap.put("price" 		,price);		    // 가격위변조체크기능 (선택사용)

				System.out.println("##승인요청 API 요청##");

				HttpUtil httpUtil = new HttpUtil();

				try{
					//#####################
					// 4.API 통신 시작
					//#####################

					String authResultString = "";

					authResultString = httpUtil.processHTTP(authMap, authUrl);

					//############################################################
					//5.API 통신결과 처리(***가맹점 개발수정***)
					//############################################################

					String test = authResultString.replace(",", "&").replace(":", "=").replace("\"", "").replace(" ","").replace("\n", "").replace("}", "").replace("{", "");

					Map<String, String> resultMap = new HashMap<String, String>();

					resultMap = ParseUtil.parseStringToMap(test); //문자열을 MAP형식으로 파싱

					System.out.println("resultMap == " + resultMap);

					/*************************  결제보안 강화 2016-05-18 START ****************************/
					Map<String , String> secureMap = new HashMap<String, String>();
					secureMap.put("mid"			, mid);								//mid
					secureMap.put("tstamp"		, timestamp);						//timestemp
					secureMap.put("MOID"		, resultMap.get("MOID"));			//MOID
					secureMap.put("TotPrice"	, resultMap.get("TotPrice"));		//TotPrice

					// signature 데이터 생성
					String secureSignature = SignatureUtil.makeSignatureAuth(secureMap);
					/*************************  결제보안 강화 2016-05-18 END ****************************/

					if("0000".equals(resultMap.get("resultCode")) && secureSignature.equals(resultMap.get("authSignature")) ){	//결제보안 강화 2016-05-18
						/*****************************************************************************
						 * 여기에 가맹점 내부 DB에 결제 결과를 반영하는 관련 프로그램 코드를 구현한다.

						 [중요!] 승인내용에 이상이 없음을 확인한 뒤 가맹점 DB에 해당건이 정상처리 되었음을 반영함
						 처리중 에러 발생시 망취소를 한다.
						 ******************************************************************************/

					} else {

						//결제보안키가 다른 경우
						if (!secureSignature.equals(resultMap.get("authSignature")) && "0000".equals(resultMap.get("resultCode"))) {
							//결과정보

							//망취소
							if ("0000".equals(resultMap.get("resultCode"))) {
								throw new Exception("데이터 위변조 체크 실패");
							}
						}
					}

				} catch (Exception ex) {

					//####################################
					// 실패시 처리(***가맹점 개발수정***)
					//####################################

					//---- db 저장 실패시 등 예외처리----//
					System.out.println(ex);

					//#####################
					// 망취소 API
					//#####################
					String netcancelResultString = httpUtil.processHTTP(authMap, netCancel);	// 망취소 요청 API url(고정, 임의 세팅 금지)


					// 취소 결과 확인
				}

			}else{

				//#############
				// 인증 실패시
				//#############

			}

		}catch(Exception e){

			System.out.println(e);
		}
	}

	public MemberVo getUserInfo(HttpServletRequest request) {
		HttpSession session = request.getSession();
		MemberVo member = (MemberVo)session.getAttribute("accessUser");
		return member;
	}

	/** 정기결제 진행
	 * Modified by LYJ - 2019. 11. 12 - 결제 성공 시, 프로시저 'Routine_Bill'을 타지 않도록 변경
	 * */
	public void routineBill(PaymentVo paymentVo) throws Exception {
		INIpay inipay = new INIpay();
		INIdata data = new INIdata();
		Calendar cal = Calendar.getInstance();
		String mid = "mindslab01";

		int productNo = memberDao.getMemberDetail(paymentVo.getUserEmail()).getProduct();
		int routineBillPrice = 0;
		
		if(productNo == 3) {
			routineBillPrice = 99000; //정기 결제 가격
		} else if(productNo == 2) {
			routineBillPrice = 29000;
		} else {
			logger.error(" @ routineBill error ==> user's productNo is not 2 or 3");
		}
		logger.info(" @ routineBill ==> productNo = " + productNo + " / routineBillPrice = " + routineBillPrice);

		LogPayVo logPayVo = new LogPayVo();
		logPayVo.setUserNo(paymentVo.getUserNo());
		logPayVo.setType("Payment");
		logPayVo.setPayment("Card");
		logPayVo.setBillKey(paymentVo.getBillingKey());
		logPayVo.setPrice(routineBillPrice);
		logPayVo.setProduct(productNo);


		data.setData("inipayHome", mPath_InipayHome);         // INIpay45 절대경로 (key / log 디렉토리) 추후 위치 수정 예정
		data.setData("logMode", "DEBUG");               // 로그모드 (INFO / DEBUG)
		data.setData("type", "reqrealbill");            // type (고정)
		data.setData("paymethod", "Card");              // 지불수단 (고정)
		data.setData("crypto", "execure");              // 암복호화모듈 (고정)

		data.setData("uip", "127.0.0.1");
		data.setData("url", "https://maum.ai");
		data.setData("mid", mid);
		data.setData("uid", mid);
		data.setData("keyPW", "1111");

		data.setData("moid", mid + "_" + cal.getTimeInMillis());
		data.setData("goodname", paymentDao.selectProductName(paymentVo.getGoodCode()));
		data.setData("price", paymentVo.getPrice() + "");
		data.setData("currency", "WON");

		data.setData("buyername", paymentVo.getUserName());
		data.setData("buyertel", "010-0000-0000");
		data.setData("buyeremail", paymentVo.getUserEmail());

		data.setData("cardquota", "0");
		data.setData("quotaInterest", "0");
		data.setData("billkey", paymentVo.getBillingKey());
		data.setData("authentification", "00");

		INIdata resultData = inipay.payRequest(data);
		logger.debug("INICIS RESULT DATA : " + resultData.toString());

		if (resultData.getData("ResultCode").equals("00")) {
			logger.debug("routinBillSuccess : " + paymentVo.toString());
			logger.debug(resultData.toString());

			paymentVo.setGoodCode(productNo); //정기결제 상품 코드
			paymentVo.setPayment(resultData.getData("PayMethod"));
			paymentVo.setIssuer(resultData.getData("CardResultCode"));
			paymentVo.setCardNo(resultData.getData("CardResultNumber") + "****");
			paymentVo.setPrice(routineBillPrice); //정기결제 상품 가격
			paymentVo.setStatus(resultData.getData("ResultCode"));
			paymentVo.setPano(resultData.getData("moid"));
			paymentVo.setPayDate(resultData.getData("PGauthdate"));

			logPayVo.setPaNo(resultData.getData("moid"));
			logPayVo.setCode(resultData.getData("CardResultCode"));
			logPayVo.setCardNumber(resultData.getData("CardResultNumber") + "****");

			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			BillingVo billingVo = new BillingVo();

			// 다음 결제일 (billing_t paymentDate, pay_t DateTo) update - 2019. 10. 22 - LYJ, LWJ
			// paymentDate(결제예정일) 조회
			String paymentDateTime = paymentDao.getPaymentDate(paymentVo.getUserNo());

			cal.set( Integer.parseInt(paymentDateTime.split("-")[0]), Integer.parseInt( paymentDateTime.split("-")[1])-1, Integer.parseInt( paymentDateTime.split("-")[2]) );

			paymentVo.setDateFrom(paymentDateTime);
			billingVo.setSdateFrom(paymentDateTime);
			paymentVo.setCreateDate(paymentDateTime);
			logPayVo.setDateFrom(paymentDateTime);
			cal.add(Calendar.MONTH, 1);

			// create Date를 통해 결제 시작 날짜 설정
			String createDateTime = paymentDao.getCreateDate(paymentVo.getUserNo());
			int createDate = Integer.parseInt( createDateTime.substring(8, 10) );

			int lastDayOfMonth = cal.getActualMaximum(Calendar.DAY_OF_MONTH); //다음 달의 마지막 날짜

			if(lastDayOfMonth >= createDate) {
				cal.set( Calendar.DAY_OF_MONTH, createDate );
			} else {
				cal.set( Calendar.DAY_OF_MONTH, lastDayOfMonth );
			}


			paymentVo.setDateTo(sdf.format(cal.getTime()));
			billingVo.setSdateTo(sdf.format(cal.getTime()));
			logPayVo.setDateTo(sdf.format(cal.getTime()));
			paymentVo.setTid(resultData.getData("tid"));

			billingVo.setPaymentDate(sdf.format(cal.getTime()) );
			billingVo.setUserNo(paymentVo.getUserNo());
			paymentDao.updatePaymentDate(billingVo);

			//billingVo 설정 (billlog_t & billing_t)
			billingVo.setPayment(resultData.getData("PayMethod"));
			billingVo.setIssuer(resultData.getData("CardResultCode"));
			billingVo.setBillKey(paymentVo.getBillingKey());
			billingVo.setNumber(resultData.getData("CardResultNumber") + "****");
			billingVo.setResult("00");
			billingVo.setProduct(productNo);
			billingVo.setPrice(routineBillPrice);
			billingVo.setAcCode(resultData.getData("moid"));
			billingVo.setTid(resultData.getData("tid"));

            // paymentDao.routineBill(paymentVo); //정기결제 프로시저
			/**
			 * 프로시저 대신 수행할 기능들 - 2019. 11. 12. - LYJ
			 * 1. billlog_t insert --> BillingVo
			 * 2. billing_t update --> BillingVo
			 * 3. pay_y insert --> PaymentVo
			 * 4. user_t update --> BillingVo
			 */
			paymentDao.insertBilllog(billingVo);
			paymentDao.updateBillingInfo(billingVo);
			paymentDao.insertPayInfo(paymentVo);
            memberDao.updateUpdateDate(paymentVo.getUserNo());

            // LogPay_t insert - 2019. 12. 26 - LYJ
			logPayVo.setStatus(0);
			paymentDao.insertLogPay(logPayVo);
			logger.info("Insert LogPay_t @ routineBill success : userNo" + logPayVo.getUserNo());

			// 결제 완료 메일 전송 - 2020. 11. 16 - LYJ
			paymentVo.setUserEmail(paymentVo.getUserEmail());
			paymentVo.setIssuerName(selectIssuerName(Integer.parseInt(paymentVo.getIssuer())));
			paymentVo.setPaymentDate(billingVo.getPaymentDate());
			mailSender.sendPaymentCompleteMail(paymentVo, PaymentPlan.BUSINESS);

			// 사용자 payCount update - 2020. 01. 20 - LYJ
			memberDao.increaseUserPayCount(paymentVo.getUserNo());
			int userPayCount = memberDao.getUserPayCount(paymentVo.getUserNo());
			logger.debug(" @ routineBill payCount increase success : " + paymentVo.getUserEmail() + " / update payCount = " + userPayCount);

//			this.cancelPay("127.0.0.1", resultData.getData("tid"));

			// user Status update - 2019. 10. 04 - LYJ
			MemberForm memberForm = new MemberForm();
			memberForm.setUserNo(paymentVo.getUserNo());
			if(memberDao.getUserStatus(memberForm) != UserStatus.SUBSCRIBED) {
				memberForm.setStatus(UserStatus.SUBSCRIBED);
				memberDao.updateUserStatus(memberForm);
			}

		} else {

			logger.debug("routineBillFail : " + paymentVo.toString());
			logger.debug(resultData.toString());

            paymentVo.setFailReason(resultData.get("ResultDetailMsg").toString());
			paymentDao.updateBillingFail(paymentVo);
            // paymentDao.planCancel(paymentVo.getUserNo());

			// LogPay_t update - 2019. 12. 26 - LYJ
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			String paymentDateTime = paymentDao.getPaymentDate(paymentVo.getUserNo());
			cal.set( Integer.parseInt(paymentDateTime.split("-")[0]), Integer.parseInt( paymentDateTime.split("-")[1])-1, Integer.parseInt( paymentDateTime.split("-")[2]) );
			logPayVo.setDateFrom(paymentDateTime);
			cal.add(Calendar.MONTH, 1);
			logPayVo.setDateTo(sdf.format(cal.getTime()));

			logPayVo.setFailReason(resultData.get("ResultDetailMsg").toString());
			logPayVo.setStatus(-1);
			paymentDao.insertLogPay(logPayVo);
			logger.info("Insert LogPay_t @ routineBill fail : " + paymentVo.getUserEmail());

			/* 구독 회원의 결제 오류 시, 메일 전송 - 2019. 11. 18. - LYJ */
			try {
				mailSender.sendPaymentErrorUserMail(paymentVo.getUserEmail());
			} catch (Exception e) {
				logger.info(" @ routineBill payment fail ==> User : " + paymentVo.getUserEmail() + ", Reason : " + resultData.get("ResultDetailMsg").toString());
			}
		}
	}

	/** 무료 체험 여부 파악 - LYJ */
	public int selectFreeUsage(int userNo) {
		return paymentDao.selectFreeUsage( userNo );
	}

	/** 계정별 구독 완료 일자 파악 - LYJ */
	public String getUserExpirationDate(MemberForm memberForm) {
		return paymentDao.getUserExpirationDate( memberForm.getUserNo() );
	}

	/** 계정별 결제 방법 파악 - LYJ */
	public String getPaymentMethod(int userNo) {
		return paymentDao.getPaymentMethod( userNo );
	}

	/** batch 해지 시, 사용자 결제 정보 삭제 - LYJ */
	public int deleteBillInfo(int userNo) {
		return paymentDao.deleteBillInfo( userNo );
	}

	/** 5일 이상 미결제자 목록 조회 - LYJ */
	public List<PaymentVo> getUnpaymentVoList() throws ParseException {

		System.out.println("PaymentService - getUnpaymentVoList");
		List <PaymentVo> paymentVoList = paymentDao.getUnpaymentVoList();

		logger.info("unsubscribe user list : " + paymentVoList.toString());
		return paymentVoList;
    }

    /** 5일이상 미결제자에 대한 billing_t update - LYJ */
    public int deletePlanCancelUser(int userNo) {
    	return paymentDao.deletePlanCancelUser(userNo);
	}

	/*
	** 1회성 결제 테스트용
	*/
	public void pay() throws Exception {
		PaymentVo paymentVo = new PaymentVo();
		paymentVo.setUserNo(44);
		paymentVo.setBillingKey("9285b264107e83306df6e1bc5fdbebacaac62d98");
		paymentVo.setUserEmail("belsnake@mindslab.ai");
		paymentVo.setUserName("김종범");
		paymentVo.setGoodCode(3);
		paymentVo.setPrice(99000);

		routineBill(paymentVo);
	}

	/*
	 * Paypal IPN Listener
	 */
	public String paypalIPNListener(HttpServletRequest request) {

		String strResult ="";
		PaypalVo paypalvo = new PaypalVo();

		PaymentVo payment = null;
		LogPayVo logPayVo = null;

		String userEmail = "";
		String billKey = "";
		int userNo = 0;
		int isHandled = 0;

		try {
			HttpClient client = HttpClientBuilder.create().build();
			List<NameValuePair> params = new ArrayList<>();

			params.add(new BasicNameValuePair("cmd", "_notify-validate")); //You need to add this parameter to tell PayPal to verify
			for (Enumeration<String> e = request.getParameterNames(); e.hasMoreElements();) {
				String name = e.nextElement();
				String value = request.getParameter(name);
				logger.info("{} | {} | {}", name, value, value.length());
				params.add(new BasicNameValuePair(name, value));

				//request data paypalvo  담기
				setPaypalVo(paypalvo, name.trim(), value.trim());
			}
			Gson gson = new Gson();
			logger.info("paypalvo  : {}\n", gson.toJson(paypalvo));


			// send POST to Paypal
			HttpPost post = new HttpPost(ipnUrl); // for live IPNs ===> https://ipnpb.paypal.com/cgi-bin/webscr
			post.setEntity(new UrlEncodedFormEntity(params));

			HttpResponse resp = client.execute(post);
			InputStream is = resp.getEntity().getContent();
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			StringBuilder sb = new StringBuilder();
			String line;

			while ((line = br.readLine()) != null) {
				sb.append(line);
			}
			String rc = sb.toString().trim();
			logger.info("Paypal IPN Listener Result :: {}", rc);



			// INVALID인 경우 로그만 일단...
			if (! "VERIFIED".equalsIgnoreCase(rc)) {
				logger.info("************ Paypal IPN Listener INVALID ************");
			}

			// paypalVo INSERT
			paymentDao.insertPaypalIPN(paypalvo);


			payment = new PaymentVo();
			logPayVo = new LogPayVo();

			billKey = paypalvo.getRecurringPaymentId();
			userNo = paymentDao.getUserNoFromBillingT(billKey);
			userEmail = memberDao.getUserEmail(userNo);

			if(userNo == 0 || billKey.equals("") || billKey == null){
				logger.error("User 정보가 없습니다. :: userNo = {}, billKey = {}", userNo, billKey);
				throw new Exception("User information not found");
			}

			payment.setUserNo(userNo);
			payment.setBillingKey(billKey);
			payment.setGoodCode(3);

			logPayVo.setUserNo(userNo);
			logPayVo.setCode("88");
			logPayVo.setBillKey(billKey);
			logPayVo.setCardNumber("paypalcard");
			logPayVo.setStatus(0);

		} catch (Exception e) {
			logger.error("Paypal IPN Listener Exception before handling TxnType: " + e);
			strResult = "FAIL";
			e.printStackTrace();

			// 페이팔 오류 발생시 이메일로 알림 (yge0723@mindslab.ai) - 2020.09.04 YGE
			String notiMsg = "Error message : Paypal IPN Listener Exception before handling TxnType. <br>" + e.getMessage();
			try {
				mailSender.sendNotiToConnieMail(userEmail, notiMsg);
			} catch (Exception exception) {
				exception.printStackTrace();
			}

			return strResult;
		}



		try {

			/* 1.payapl create */
			if ("recurring_payment_profile_created".equals(paypalvo.getTxnType())) {

				logger.info("paypal IPN create");
				isHandled = 1;

				if (paypalvo.getProductName().contains("FREE")) { // Instant plan(재구독자)는 created와 recurring_payment 두개의 IPN이 오기 때문에 FREE만

					// pay_t UPDATE
					PaymentVo tmpVo = new PaymentVo();
					tmpVo.setUserNo(userNo);
					tmpVo.setPayment("Free");

					PaymentVo newPaymentVo = paymentDao.getOneLatestPay(tmpVo); // 가장 최근의 pay_t 1개 조회 (id 기준 정렬)
					newPaymentVo.setUserNo(userNo); // db column명이 userId 여서 다시 setting
					newPaymentVo.setGoodCode(3);    // db column명이 product 여서 다시 setting
					newPaymentVo.setPano(paypalvo.getIpnTrackId());

					logger.info("newPaymentVo : {}", newPaymentVo);

					paymentDao.updatePayT(newPaymentVo);

					// logPay_t INSERT
					logPayVo.setPaNo(paypalvo.getIpnTrackId());
					logPayVo.setType("Payment");
					logPayVo.setPayment("Free");
					logPayVo.setProduct(3);
					logPayVo.setPrice(0);
					logPayVo.setDateFrom(newPaymentVo.getDateFrom());
					logPayVo.setDateTo(newPaymentVo.getDateTo());
					logPayVo.setPaymentDate(newPaymentVo.getPayDate());

					logger.info("logPayVo : {}", logPayVo);

					paymentDao.insertLogPay(logPayVo);

					// 영문 이메일 발송 ( Thank you for joining mail : 구독자에게 보내는 메일 ) - 2020. 05. 07 - LYJ
					mailSender.enSendFreeUserMail(userEmail);
				} else {
					// 영문 이메일 발송 ( Thank you for joining mail_2 : 재구독자에게 보내는 메일 ) - 2020. 05. 12 - LYJ
					mailSender.enSendSubscribeUserMail(userEmail);
				}

				// billing_t UPDATE
				payment.setActive(1);
				paymentDao.updateBillingPaymentDate(payment);

			}


			/* 2.paypal billing */
			if ("recurring_payment".equals(paypalvo.getTxnType())) {

				logger.info("paypal IPN billing");
				isHandled = 1;

				String dateFrom = paypalvo.getPaymentDate();
				String dateTo = paypalvo.getNextPaymentDate();
				String payDate = paypalvo.getPaymentDate();

				// user_t UPDATE
				MemberForm memberForm = new MemberForm();
				memberForm.setUserNo(userNo);
				memberForm.setStatus(2);

				logger.info("memberForm : {}", memberForm);

				memberDao.updateUserStatus(memberForm);

				// billing_t UPDATE
				payment.setActive(1);
				payment.setPayDate(paypalvo.getNextPaymentDate());
				paymentDao.updateBillingPaymentDate(payment);

				// pay_t INSERT
				/** paypal 정기결제(실결제)는 바로 pay_T에 insert 안 함. 여기서 따로 insert 해줌. */
				payment.setPayment("PayPal"); // 결제수단
				payment.setIssuer("88");
				payment.setCardNo("paypalcard");
				payment.setTid(paypalvo.getTxnId());
				payment.setPano(paypalvo.getIpnTrackId());
				payment.setDateFrom(dateFrom);
				payment.setDateTo(dateTo);
				payment.setPrice(89);
				payment.setPayDate(payDate);

				if ("Completed".equals(paypalvo.getPaymentStatus())) { // 결제 성공
					payment.setStatus("00");
					logPayVo.setStatus(0);

					// payCount update - 2020. 01. 21 - LYJ
					memberDao.increaseUserPayCount(userNo);
					int userPayCount = memberDao.getUserPayCount(userNo);
					logger.debug(" @ paypalIPNListener payCount increase success : userNo = {} / update payCount = {}", userNo, userPayCount);

				} else {  // 결제 실패?
					logger.info(" @ payPal paymentStatus (not COMPLETE!) => " + paypalvo.getPaymentStatus());

					payment.setStatus("--");
					logPayVo.setStatus(-1);
				}

				logger.info("paymentVo : {}", payment);

				paymentDao.insertPayInfo(payment);

				// logPay_t INSERT
				logPayVo.setPaNo(paypalvo.getIpnTrackId());
				logPayVo.setType("Payment");
				logPayVo.setPayment("PayPal");
				logPayVo.setProduct(3);
				logPayVo.setPrice(89);
				logPayVo.setDateFrom(dateFrom);
				logPayVo.setDateTo(dateTo);
				logPayVo.setPaymentDate(payDate);

				logger.info("logPayVo : {}", logPayVo);

				paymentDao.insertLogPay(logPayVo);
			}


			/* 3.paypal cancel */
			if ("recurring_payment_profile_cancel".equals(paypalvo.getTxnType())) {

				logger.info("payapl IPN subscription cancel");
				isHandled = 1;

				MemberForm memberForm = new MemberForm();
				memberForm.setUserNo(userNo);
				int userStat = memberDao.getUserStatus(memberForm);

				/*
				 *  maum.ai 에서 구독 취소한 경우 IPN이 왔을 때 userStat= 3 or 4
				 *  paypal  에서 직접 취소한 경우 IPN이 왔을 때 userStat= 1 or 2
				 *  즉, 1 or 4는 Free O 였고, 2 or 3은 Free X 였던 사용자임.
				 */

				logger.info("current userStat : {}", userStat);

				if (userStat == 1 || userStat == 4) {
					// user_t UPDATE
					memberForm.setStatus(4);
					memberDao.updateUserStatus(memberForm);
					//billing_t DELETE
					deleteBillInfo(userNo);

					// 구독 해지를 신청한 무료 사용자에게 보내는 메일 (FREE -> UNSUBSCRIBED 사용자) - 2020. 09 .04 - YGE
					mailSender.enSendExpiredUserMail(userEmail);

				} else if (userStat == 2 || userStat == 3) {
					// user_t UPDATE
					memberForm.setStatus(3);
					memberDao.updateUserStatus(memberForm);
					//billing_t UPDATE
					payment.setActive(0);
					paymentDao.updateBillingPaymentDate(payment);

					// 구독 해지를 신청한 유료 사용자에게 보내는 메일 (SUBSCRIBED -> UNSUBSCRIBING 사용자) - 2020. 05 .07 - LYJ
					mailSender.enSendReqCancelUserMail(userEmail);
				}
			}


			/**
			 * paypal 결제 실패 관련 참고사항
			 *  [결제 실패 시나리오]
			 *    1. 결제일      결제 실패시 -> IPN 오지 않음
			 *    2. 결제일+5일  결제 실패시 -> IPN : "recurring_payment_skipped"
			 *    3. 결제일+10일 결제 실패시 -> IPN : "recurring_payment_skipped"
			 *    4. 결제일+1달  결제 실패시 -> IPN : "recurring_payment_failed"
			 *
			 *  [결제 실패 처리 방침] (위의 시나리오 번호 참고)
			 *    1. 결제일      : 그냥 넘어감
			 *    2. 결제일+5일  : 결제 실패 mail 발송
			 *    3. 결제일+10일 : 직접 구독 취소 (+ API계정 중지 및 user_t status 변경 )
			 * */

			/* 4. paypal  실패 : skipped */
			if ("recurring_payment_skipped".equals(paypalvo.getTxnType())) {

				logger.info("payapl IPN payment skipped");
				isHandled = 1;

				logger.debug(" @ paypalIPNListener 결제 실패 : userNo = {} / IPN = {}", userNo, paypalvo.getTxnType());

				int skippedCnt = paypalService.getPaypalSkippedCount(paypalvo);
				logger.info("skipped count : {}", skippedCnt);

				if (skippedCnt < 2) { //첫 번째 skipped

					// 결제 실패에 대한 영문 이메일 발송 - 2020. 05. 11 - LYJ
					mailSender.enSendPaymentErrorUserMail(userEmail);
					logger.debug(" @ paypalIPNListener 결제 실패자에게 메일 발송 완료 : Email(userNo) ==> " + userEmail + "(" + userNo + ")");

				} else { // 두 번째 skipped == 해지 대상

					logger.info("skipped 2번째 user: {}", userEmail);

					MemberForm memberForm = new MemberForm();
					memberForm.setUserNo(userNo);
					memberForm = memberDao.getUserInfoByNo(memberForm);


					if (memberForm.getApiId() != null) {
						Map<String, String> apiAccountResultMap;
						apiAccountResultMap = apiKeyService.pauseApiId( memberForm.getEmail() );    // 구독 pause
						logger.info("API account pause result : {}", apiAccountResultMap.toString());
					}

					paypalService.subscCancelAPI(billKey); // paypal cancel API

					memberForm.setStatus(UserStatus.UNSUBSCRIBED);    // Status 값 변경
					memberDao.updateUserStatus(memberForm);

					logger.info("updated user info : {}", memberForm.toString());
					mailSender.enSendExpiredUserMail(userEmail);

					/* IPN cancel에서 결제 정보(Billing_t) 삭제할 것임 */

				}

				// 자주 발생하는 경우가 아니라 한동안 이메일로 알림 보내기 (yge0723@mindslab.ai) : 2020.09.04 YGE
				String notiMsg = "paypal 결제 실패(skipped) 처리 알림 메세지입니다. <br> skipped count : " + skippedCnt + "<br> 잘 처리되었는지 확인해 보세요!";
				mailSender.sendNotiToConnieMail(userEmail, notiMsg);
			}


			/* 5. paypal 실패 : failed */
			// 위의 4.skipped 에서 처리가 잘 된다면 여기에 올 일은 없겠지만 혹시 모르는 상황에 대비해서 추가함
			if ("recurring_payment_failed".equals(paypalvo.getTxnType())) {

				logger.info("payapl IPN payment failed");
				isHandled = 1;

				logger.info("failed user: {}", userEmail);

				MemberForm memberForm = new MemberForm();
				memberForm.setUserNo(userNo);
				memberForm = memberDao.getUserInfoByNo(memberForm);

				if (memberForm.getApiId() != null) {
					Map<String, String> apiAccountResultMap;
					apiAccountResultMap = apiKeyService.pauseApiId( memberForm.getEmail() );    // 구독 pause
					logger.info("API account pause result : {}", apiAccountResultMap.toString());
				}

				paypalService.subscCancelAPI(billKey); // paypal cancel API

				memberForm.setStatus(UserStatus.UNSUBSCRIBED); // Status 값 변경
				memberDao.updateUserStatus(memberForm);

				logger.info("updated user info : {}", memberForm.toString());
				mailSender.enSendExpiredUserMail(userEmail);

				/* IPN에서 결제 정보(Billing_t) 삭제할 것임 */

			}


			/* 6.paypal refund */
			if ("Refunded".equalsIgnoreCase(paypalvo.getPaymentStatus())) { // paypalvo의 TxnType이 null이라서 예외적으로 PaymentStatus로 확인!

				logger.info("payapl IPN payment refund");
				isHandled = 1;

				// pay_t UPDATE
				String tid = paypalvo.getParentTxnId();
				PaymentVo tmpVo = paymentDao.getOnePayByTid(tid);
				tmpVo.setUserNo(userNo); // db column명이 userId 여서 다시 setting
				tmpVo.setGoodCode(3);    // db column명이 product 여서 다시 setting
				tmpVo.setStatus("99");
				tmpVo.setCancelledDate(paypalvo.getPaymentDate());
				paymentDao.updatePayT(tmpVo);

				// logPay_t INSERT
				logPayVo.setPaNo(paypalvo.getIpnTrackId());
				logPayVo.setType("cancel");
				logPayVo.setPayment("PayPal");
				logPayVo.setProduct(3);
				logPayVo.setPrice(89);
				logPayVo.setDateFrom(tmpVo.getDateFrom());
				logPayVo.setDateTo(tmpVo.getDateTo());
				logPayVo.setPaymentDate(tmpVo.getPayDate());
				paymentDao.insertLogPay(logPayVo);

				// user_t payCount decrease update - 2020. 01. 21 - LYJ
				memberDao.decreaseUserPayCount(userNo);
				int userPayCount = memberDao.getUserPayCount(userNo);
				logger.debug(" @ paypalIPNListener payCount decrease success : userNo = {} / update payCount = {}", userNo, userPayCount);
			}


			if (isHandled == 0) {
				logger.info("Paypal IPN 케이스중 처리되지 않은 케이스입니다!!!");
				logger.info("Userno : {} \nTxnType : {} , Paypal paymentStatus : {}", userNo, paypalvo.getTxnType(), paypalvo.getPaymentStatus());
			}

			strResult = "SUCCCESS";

		} catch (Exception e) {
			logger.error("Paypal IPN Listener Exception  : " + e);
			strResult = "FAIL";
			e.printStackTrace();

			// 페이팔 오류 발생시 이메일로 알림 (yge0723@mindslab.ai) - 2020.09.04 YGE
			String notiMsg = "Error message : Paypal IPN Listener Exception while handling TxnType(" + paypalvo.getTxnType() + ") <br>" + e.getMessage();
			try {
				mailSender.sendNotiToConnieMail(userEmail, notiMsg);
			} catch (Exception exception) {
				exception.printStackTrace();
			}
		}

		return strResult;
	}

	/*
	** 사용자의 가장 최근 결제 내역 조회
	 */
	public PaymentVo getLastPayInfo(int userNo) {
		PaymentVo paymentVo = paymentDao.getLastPayInfo(userNo);
		return paymentVo;
	}

	/*
	** request data paypalvo  담기
	 */
	public PaypalVo setPaypalVo(PaypalVo paypalvo, String name, String value) throws ParseException {

		switch(name){
			case "recurring_payment_id" :	paypalvo.setRecurringPaymentId(value);	break;
			case "payment_cycle" :	paypalvo.setPaymentCycle(value);	break;
			case "txn_type" :	paypalvo.setTxnType(value);	break;
			case "last_name" :	paypalvo.setLastName(value);	break;
			case "initial_payment_status" :	paypalvo.setInitialPaymentStatus(value);	break;
			case "next_payment_date" :	paypalvo.setNextPaymentDate(convertDateType(value));	break;
			case "residence_country" :	paypalvo.setResidenceCountry(value);	break;
			case "initial_payment_amount" :	paypalvo.setInitialPaymentAmount(value);	break;
			case "currency_code" :	paypalvo.setCurrencyCode(value);	break;
			case "time_created" :	paypalvo.setTimeCreated(convertDateType(value));	break;
			case "verify_sign" :	paypalvo.setVerifySign(value);	break;
			case "period_type" :	paypalvo.setPeriodType(value);	break;
			case "payer_status" :	paypalvo.setPayerStatus(value);	break;
			case "test_ipn" :	paypalvo.setTestIpn(value);	break;
			case "tax" :	paypalvo.setTax(value);	break;
			case "payer_email" :	paypalvo.setPayerEmail(value);	break;
			case "parent_txn_id" :  paypalvo.setParentTxnId(value); break;
			case "first_name" :	paypalvo.setFirstName(value);	break;
			case "receiver_email" :	paypalvo.setReceiverEmail(value);	break;
			case "payer_id" :	paypalvo.setPayerId(value);	break;
			case "product_type" :	paypalvo.setProductType(value);	break;
			case "initial_payment_txn_id" :	paypalvo.setInitialPaymentTxnId(value);	break;
			case "shipping" :	paypalvo.setShipping(value);	break;
			case "amount_per_cycle" :	paypalvo.setAmountPerCycle(value);	break;
			case "profile_status" :	paypalvo.setProfileStatus(value);	break;
			case "charset" :	paypalvo.setCharset(value);	break;
			case "notify_version" :	paypalvo.setNotifyVersion(value);	break;
			case "amount" :	paypalvo.setAmount(value);	break;
			case "outstanding_balance" :	paypalvo.setOutstandingBalance(value);	break;
			case "product_name" :	paypalvo.setProductName(value);	break;
			case "ipn_track_id" :	paypalvo.setIpnTrackId(value);	break;
			case "mc_gross" :	paypalvo.setMcGross(value);	break;
			case "protection_eligibility" :	paypalvo.setProtectionEligibility(value);	break;
			case "address_status" :	paypalvo.setAddressStatus(value);	break;
			case "address_street" :	paypalvo.setAddressStreet(value);	break;
			case "payment_date" :	paypalvo.setPaymentDate(convertDateType(value));	break;
			case "payment_status" :	paypalvo.setPaymentStatus(value);	break;
			case "address_zip" :	paypalvo.setAddressZip(value);	break;
			case "mc_fee" :	paypalvo.setMcFee(value);	break;
			case "address_country_code" :	paypalvo.setAddressCountryCode(value);	break;
			case "address_name" :	paypalvo.setAddressName(value);	break;
			case "business" :	paypalvo.setBusiness(value);	break;
			case "address_country" :	paypalvo.setAddressCountry(value);	break;
			case "address_city" :	paypalvo.setAddressCity(value);	break;
			case "txn_id" :	paypalvo.setTxnId(value);	break;
			case "payment_type" :	paypalvo.setPaymentType(value);	break;
			case "address_state" :	paypalvo.setAddressState(value);	break;
			case "payment_fee" :	paypalvo.setPaymentFee(value);	break;
			case "receiver_id" :	paypalvo.setReceiverId(value);	break;
			case "mc_currency" :	paypalvo.setMcCurrency(value);	break;
			case "transaction_subject" :	paypalvo.setTransactionSubject(value);	break;
			case "payment_gross" :	paypalvo.setPaymentGross(value);	break;
			default:
				break;
		}

		return paypalvo;
	}

	public String convertDateType(String value) throws ParseException {
		String strResult ="";
		if(!"".equals(value) && !"N/A".equals(value)){
            SimpleDateFormat fommatter = new SimpleDateFormat("HH:mm:ss MMM dd, yyyy z");
            Date newdate = fommatter.parse(value);
            SimpleDateFormat newfommatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            //logger.info("convertDateType ------- old {} : {}",  value, fommatter.format(newdate));
            //logger.info("convertDateType ------- new {} : {}",  value, newfommatter.format(newdate));
			strResult = newfommatter.format(newdate);
        }
		return strResult;
	}

}