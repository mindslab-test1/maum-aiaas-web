package console.maum.ai.payment.service;

import console.maum.ai.payment.dao.PaymentDao;
import console.maum.ai.payment.model.PaypalVo;
import org.apache.commons.codec.binary.Base64;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.nio.charset.Charset;


@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})

@Service
public class PaypalService {

    @Value("${paypal.API_URL}")
    private String apiUrl;
    @Value("${paypal.PAYPAL_CLIENT_ID}")
    private String paypalClientId;
    @Value("${paypal.PAYPAL_CLIENT_SECRET}")
    private String paypalClientSecret;

    @Autowired
    private PaymentDao paymentDao;

    private static final Logger logger = LoggerFactory.getLogger(PaypalService.class);

    /* paypal 결제 실패시 skipped 된 횟수 count -- 2020. 09. 04 YGE */
    public int getPaypalSkippedCount(PaypalVo paypalVo){
        return paymentDao.getPaypalSkippedCount(paypalVo);
    }


    /* paypal 구독 취소 API -- 2019. 10. 20 YGE */
    public void subscCancelAPI(String subscId) throws Exception{

        logger.info("----- Paypal Cancel API start ------");

        String token = getPaypalOauth2Token();
        logger.info("token : {}", token);
        if(token.contains("Get token error")){
            throw new Exception(token);
        }else{
            // Request setting start
            HttpHeaders headers = new  HttpHeaders();
            headers.add("Host", apiUrl);
            headers.add("Content-Type","application/json");
            headers.add("Authorization", "Bearer " + token);

            MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
            body.add("\"reason\"", "\"Cancel the subscription.\"");

            HttpEntity<?> httpEntity = new HttpEntity<>(body,headers);

            logger.info("Cancel API request HttpEntity :: {}",httpEntity);
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity response = restTemplate.exchange(apiUrl+"/v1/billing/subscriptions/" + subscId +"/cancel", HttpMethod.POST, httpEntity, String.class);

            logger.info("Status code :: {}", response.getStatusCode());

            if(!"204".equals(response.getStatusCode().toString())){ // 정상적인 경우 204(No Content)가 return 됨.
                throw new Exception("API Cancel Error");
            }else{
                logger.info("Cancel result : Status 204 success");
            }
        }

    }

    /* paypal get access_token API -- 2019. 10. 20 YGE */
    private String getPaypalOauth2Token(){
        try{
            RestTemplate restTemplate = new RestTemplate();

            String auth = paypalClientId + ":" + paypalClientSecret;
            byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(Charset.forName("US-ASCII")));
            String authHeader = "Basic "+ new String(encodedAuth);

            HttpHeaders headers = new  HttpHeaders();
            headers.add("Host", apiUrl);
            headers.add("Content-Type","application/x-www-form-urlencoded");
            headers.add("Authorization", authHeader);

            MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
            body.add("grant_type", "client_credentials");

            HttpEntity<?> httpEntity = new HttpEntity<>(body,headers);

            logger.info("token API request HttpEntity :: {}",httpEntity);

            ResponseEntity response = restTemplate.exchange(apiUrl+"/v1/oauth2/token", HttpMethod.POST, httpEntity, String.class);

            logger.info("response : {}", response.getBody().toString());

            JSONObject jsonResponse = (JSONObject) new JSONParser().parse(response.getBody().toString());

            return jsonResponse.get("access_token").toString();

        }catch (Exception e){
            e.printStackTrace();

            return "Get token error :: " + e;
        }
    }


}
