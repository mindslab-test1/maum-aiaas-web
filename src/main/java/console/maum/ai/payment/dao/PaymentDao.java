package console.maum.ai.payment.dao;

import javax.annotation.Resource;

import console.maum.ai.member.model.MemberForm;
import console.maum.ai.payment.model.BillingVo;
import console.maum.ai.payment.model.LogPayVo;
import console.maum.ai.payment.model.PaypalVo;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import console.maum.ai.payment.model.PaymentVo;

import java.util.List;

@Repository
public class PaymentDao {


	@Resource(name = "sqlSession")
	private SqlSession sqlSession;

	private static final String NAMESPACE = "console.maum.ai.model.paymentMapper";

	/** 상품명으로 상품 코드 조회*/
	public int selectProductId(String productName) {
		return sqlSession.selectOne(NAMESPACE + ".selectProductId", productName);
	}

	/** 일반 결제 (정기 결제 제외한 모든 상황에 사용)*/
	public void commonBilling(PaymentVo paymentVo) {
		sqlSession.update(NAMESPACE + ".commonBilling", paymentVo);
	}

	/** 정기 결제 해지*/
	public void planCancel(int userNo) {
		sqlSession.update(NAMESPACE + ".planCancel", userNo);
	}

	/** 요금제 변경*/
	public void planChange(PaymentVo paymentVo) {
		sqlSession.update(NAMESPACE + ".planChange", paymentVo);
	}

	/** 플랜 적용 가능 여부 확인*/
	public int planConfirm(PaymentVo paymentVo) {
		return sqlSession.selectOne(NAMESPACE + ".planConfirm", paymentVo);
	}

	/** 정기 결제자 리스트 검색*/
	public List <PaymentVo> selectRegularList()	{	return sqlSession.selectList(NAMESPACE + ".selectRegularList");	}

	/** 정기 결제 카드 변경*/
	public void billInfoChange(PaymentVo paymentVo) {
		sqlSession.update(NAMESPACE + ".billInfoChange", paymentVo);
	}

	/** 정기 결제 진행*/
	public void routineBill(PaymentVo paymentVo) {	sqlSession.update(NAMESPACE + ".routineBill", paymentVo); }

	/** 상품 코드로 상품명 조회 */
	public String selectProductName(int goodCode) { return sqlSession.selectOne(NAMESPACE + ".selectProductName", goodCode); }

	/** 금융사 코드로 금융사명 조회 */
	public String selectIssuerName(int issuer) { return sqlSession.selectOne(NAMESPACE + ".selectIssuerName", issuer); }

	/** 플랜 변경 가능 여부 조회 */
	public int planChangeConfirm(PaymentVo paymentVo) { return sqlSession.selectOne(NAMESPACE + ".planChangeConfirm", paymentVo); }

	/** 정기결제 실패 관련 정보 업데이트 */
	public int updateBillingFail(PaymentVo paymentVo) { return sqlSession.update(NAMESPACE + ".updateBillingFail", paymentVo); }

	/** Free결제 후, 결제일 조회 - 2019. 09. 10 YGE */
	public String selectPayDate(int userNo){ return sqlSession.selectOne(NAMESPACE + ".selectPayDate", userNo);	}

	/** 계정별 무료 체험 여부 파악 - 2019. 09. 11 LYJ */
	public int selectFreeUsage(int userNo) {return  sqlSession.selectOne(NAMESPACE + ".selectFreeUsage", userNo); }

	/** 계정별 구독 만료 일자 파악 - 2019. 09. 25 LYJ */
	public String getUserExpirationDate(int userNo) {
		return sqlSession.selectOne(NAMESPACE + ".getUserExpirationDate", userNo);
	}

	/** billing_t createDate 조회 - 2019. 10. 08 - LYJ */
	public String getCreateDate(int userNo) {
	    return sqlSession.selectOne(NAMESPACE + ".getCreateDate", userNo);
    }

	/** billing_t paymentDate(다음 결제일) 업데이트 - 2019. 10. 08 - LYJ */
	public int updatePaymentDate(BillingVo billingVo) {
	    return sqlSession.update(NAMESPACE + ".updatePaymentDate", billingVo);
    }


	/** paypal 즉시결제 후 billing_t 등록 */
	public int insertBilling(PaymentVo paymentVo) throws Exception {
		return sqlSession.insert(NAMESPACE + ".insertBilling", paymentVo);
	}

	/** paypal 결제 후 ipn 응답 데이터 등록 */
	public int insertPaypalIPN(PaypalVo paypalVo) throws Exception {
		return sqlSession.insert(NAMESPACE + ".insertPaypalIPN", paypalVo);
	}

	/** paypal 결제 후 ipn 응답 데이터 - billing_t 업데이트	 */
	public int updateBillingPaymentDate(PaymentVo paymentVo) throws Exception {
		return sqlSession.update(NAMESPACE + ".updateBillingPaymentDate", paymentVo);
	}

	/** paypal 구독 API 요청시 필요한 billKey 조회 - 2019. 10. 20 - YGE */
	public String getBillKey(int userNo){
		return sqlSession.selectOne(NAMESPACE + ".getBillKey", userNo);
	}

	/** BillKey로 UserNo 조회 (paypal IPN 업데이트시 사용) - 2020. 01. 07 - YGE */
	public int getUserNoFromBillingT(String billKey){
		return sqlSession.selectOne(NAMESPACE + ".getUserNoFromBillingT", billKey);
	}

	/**  pay_t에서 최근 결제내역 1개 조회 - 2020. 01. ?? - YGE */
	public PaymentVo getOneLatestPay(PaymentVo paymentVo){
		return sqlSession.selectOne(NAMESPACE + ".getOneLatestPay", paymentVo);
	}

	/**  Tid로 Pay_t 조회 - 2020. 01. 16 - YGE */
	public PaymentVo getOnePayByTid(String tid){
		return sqlSession.selectOne(NAMESPACE + ".getOnePayByTid", tid);
	}

	/** BillKey로 skipped 횟수 조회 (paypal 결제 실패 처리시 사용) - 2020. 09. 03 - YGE */
	public int getPaypalSkippedCount(PaypalVo paypalVo){
		return sqlSession.selectOne(NAMESPACE + ".getPaypalSkippedCount", paypalVo);
	}


	/** billing_t의 결제 방법 조회 - 2019. 10. 19 - LYJ */
	public String getPaymentMethod(int userNo) {
		return sqlSession.selectOne(NAMESPACE + ".getPaymentMethod", userNo);
	}

	/** 5일 이상 미결제자 목록 조회 - 2020. 03. 02 - LYJ */
	public List<PaymentVo> getUnpaymentVoList() {
		return sqlSession.selectList(NAMESPACE + ".getUnpaymentList");
	}

	/** 해지 batch시, billing_t의 bill 정보 삭제 - 2019. 10. 21 - LYJ */
	public int deleteBillInfo(int userNo) {
		return sqlSession.delete(NAMESPACE + ".deleteBillInfo", userNo);
	}

	/** billing_t의 paymentDate(다음결제일) 조회 - 2019. 10. 22 - LYJ */
	public String getPaymentDate(int userNo) { return sqlSession.selectOne(NAMESPACE + ".getPaymentDate", userNo); }

	/** 정기 결제 성공 후 billing_t update = 2019. 11. 12 - LYJ */
	public int updateBillingInfo(BillingVo billingVo) {
		return sqlSession.update(NAMESPACE + ".updateBillingInfo", billingVo);
	}

	/** pay_t update = 2020. 01. 16 - YGE */
	public int updatePayT(PaymentVo paymentVo){ return sqlSession.update(NAMESPACE + ".updatePayT", paymentVo); }

	/** 정기 결제 성공 후 pay_t insert - 2019. 11. 12 - LYJ */
	public int insertPayInfo(PaymentVo paymentVo) {
		return sqlSession.insert(NAMESPACE + ".insertPayInfo", paymentVo);
	}

	/** 정기 결제 성공 후 billlog_t insert - 2019. 11. 12 - LYJ */
	public int insertBilllog(BillingVo billingVo) {
		return sqlSession.insert(NAMESPACE + ".insertBilllog", billingVo);
	}

	/** 결제 요청 시 logPay_t insert - 2019. 12. 24 - LYJ */
	public int insertLogPay(LogPayVo logPayVo) {
		return sqlSession.insert(NAMESPACE + ".insertLogPay", logPayVo);
	}

	/** 5일 이상 미결제자에 대한 billing_t update - 2020. 01. 09 - LYJ */
	public int deletePlanCancelUser(int userNo) {
		System.out.println(userNo);
		return sqlSession.delete(NAMESPACE + ".deletePlanCancelUser", userNo);
	}

    /** 사용자의 가장 최근 결제 내역 조회 - 2020. 12. 29 - LYJ */
    public PaymentVo getLastPayInfo(int userNo) {
    	return sqlSession.selectOne(NAMESPACE + ".getLastPayInfo", userNo);
	}
}