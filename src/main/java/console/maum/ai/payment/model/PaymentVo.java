package console.maum.ai.payment.model;

public class PaymentVo {

	private int id;					// id
	private int userNo;				// 회원번호
	private String userName	;		// 회원이름
	private String userEmail;		// 회원이메일
	private String payment;			// 결제수단
	private String issuer;			// 카드사
	private String issuerName;		// 카드사 이름
	private String billingKey;		// 빌링키
	private String cardNo;			// 카드번호
	private String status;			// 결제결과값
	private String goodName;		// 상품 이름
	private int goodCode;			// 상품 정보 코드
	private int price;				// 가격
	private int priceUs;			// paypal 가격
	private String pano;			// 결제승인번호
	private String createDate;		// 데이터생성일
	private String dateFrom;		// 결제시작일
	private String dateTo;			// 결제종료일
	private String payDate;			// 결제승인일
	private String tid;				// 이니시스 결제 취소를 위해 필요한 정보
	private String paymentDate;		// 다음결제일 (billing_t)
	private String cancelledDate;   // 결제 취소일자
	private String cancelAdmin;		// admin에서 취소시 userId

	private int imp;				// 할부여부
	private int impMonth;			// 할부개월수

	private String failReason;		// 결제실패이유 (ResultDetailMsg)

//	private String amount;			// 가격(paypal)

	private int active;			// 활성화여부(billing_t active)

	public int getActive() { return active;	}

	public void setActive(int active) { this.active = active;}

	public int getUserNo() {
		return userNo;
	}
	
	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}
	
	public String getPayment() {
		return payment;
	}
	
	public void setPayment(String payment) {
		this.payment = payment;
	}
	
	public String getIssuer() {
		return issuer;
	}
	
	public void setIssuer(String issuer) {
		this.issuer = issuer;
	}
	
	public String getBillingKey() {
		return billingKey;
	}
	
	public void setBillingKey(String billingKey) {
		this.billingKey = billingKey;
	}
	
	public String getCardNo() {
		return cardNo;
	}
	
	public void setCardNo(String cardNo) {
		this.cardNo = cardNo;
	}
	
	public String getStatus() {
		return status;
	}
	
	public void setStatus(String status) {
		this.status = status;
	}
	
	public int getGoodCode() {
		return goodCode;
	}
	
	public void setGoodCode(int goodCode) {
		this.goodCode = goodCode;
	}
	
	public int getPrice() {
		return price;
	}
	
	public void setPrice(int price) {
		this.price = price;
	}

	public int getPriceUs() {
		return priceUs;
	}

	public void setPriceUs(int priceUs) {
		this.priceUs = priceUs;
	}

	public String getPano() {
		return pano;
	}
	
	public void setPano(String pano) {
		this.pano = pano;
	}
	
	public String getCreateDate() {
		return createDate;
	}
	
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	
	public String getDateFrom() {
		return dateFrom;
	}
	
	public void setDateFrom(String dateFrom) {
		this.dateFrom = dateFrom;
	}
	
	public String getDateTo() {
		return dateTo;
	}
	
	public void setDateTo(String dateTo) {
		this.dateTo = dateTo;
	}
	
	public String getPayDate() {
		return payDate;
	}
	
	public void setPayDate(String payDate) {
		this.payDate = payDate;
	}

	public String getPaymentDate() {return paymentDate;}

	public void setPaymentDate(String paymentDate) {
		this.paymentDate = paymentDate;
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getCancelledDate() {
		return cancelledDate;
	}

	public void setCancelledDate(String cancelledDate) {
		this.cancelledDate = cancelledDate;
	}

	public String getCancelAdmin() {
		return cancelAdmin;
	}

	public void setCancelAdmin(String cancelAdmin) {
		this.cancelAdmin = cancelAdmin;
	}

	public int getImp() {
		return imp;
	}

	public void setImp(int imp) {
		this.imp = imp;
	}

	public int getImpMonth() {
		return impMonth;
	}

	public void setImpMonth(int impMonth) {
		this.impMonth = impMonth;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getGoodName() {
		return goodName;
	}

	public void setGoodName(String goodName) {
		this.goodName = goodName;
	}

	public String getIssuerName() {
		return issuerName;
	}

	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}

	public String getFailReason() {
		return failReason;
	}
	public void setFailReason(String reason) {
		this.failReason = reason;
	}

//	public String getAmount() {
//		return amount;
//	}
//
//	public void setAmount(String amount) {
//		this.amount = amount;
//	}

	@Override
	public String toString() {
		return "PaymentVo{" +
				"userNo=" + userNo +
				", userName='" + userName + '\'' +
				", userEmail='" + userEmail + '\'' +
				", payment='" + payment + '\'' +
				", issuer='" + issuer + '\'' +
				", issuerName='" + issuerName + '\'' +
				", billingKey='" + billingKey + '\'' +
				", cardNo='" + cardNo + '\'' +
				", status='" + status + '\'' +
				", goodName='" + goodName + '\'' +
				", goodCode=" + goodCode +
				", price=" + price +
				", pano='" + pano + '\'' +
				", createDate='" + createDate + '\'' +
				", dateFrom='" + dateFrom + '\'' +
				", dateTo='" + dateTo + '\'' +
				", payDate='" + payDate + '\'' +
				", tid='" + tid + '\'' +
				", imp=" + imp +
				", impMonth=" + impMonth +
				", cancelledDate=" + cancelledDate +
				", cancelAdmin=" + cancelAdmin +
				'}';
	}
}
