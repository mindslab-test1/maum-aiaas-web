package console.maum.ai.payment.model;

import lombok.Data;

@Data
public class PaypalVo {

	private int id;
	private String recurringPaymentId;
	private String paymentCycle;
	private String txnType;
	private String lastName;
	private String initialPaymentStatus;
	private String nextPaymentDate;
	private String residenceCountry;
	private String initialPaymentAmount;
	private String currencyCode;
	private String timeCreated;
	private String verifySign;
	private String periodType;
	private String payerStatus;
	private String testIpn;
	private String tax;
	private String payerEmail;
	private String parentTxnId;
	private String firstName;
	private String receiverEmail;
	private String payerId;
	private String productType;
	private String initialPaymentTxnId;
	private String shipping;
	private String amountPerCycle;
	private String profileStatus;
	private String charset;
	private String notifyVersion;
	private String amount;
	private String outstandingBalance;
	private String productName;
	private String ipnTrackId;
	private String mcGross;
	private String protectionEligibility;
	private String addressStatus;
	private String addressStreet;
	private String paymentDate;
	private String paymentStatus;
	private String addressZip;
	private String mcFee;
	private String addressCountryCode;
	private String addressName;
	private String business;
	private String addressCountry;
	private String addressCity;
	private String txnId;
	private String paymentType;
	private String addressState;
	private String paymentFee;
	private String receiverId;
	private String mcCurrency;
	private String transactionSubject;
	private String paymentGross;

}
