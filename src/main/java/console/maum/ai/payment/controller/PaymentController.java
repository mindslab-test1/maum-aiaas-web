package console.maum.ai.payment.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.inicis.inipay4.module.ExecureCryptoModule;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.SessionScope;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import console.maum.ai.member.model.MemberVo;
import console.maum.ai.payment.model.PaymentVo;
import console.maum.ai.payment.service.PaymentService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Handles requests for the application home page.
 */
@Controller
@RequestMapping(value = "/payment")
public class PaymentController {

	@Autowired
	private PaymentService paymentService;

	private static final Logger logger = LoggerFactory.getLogger(PaymentController.class);

	// 테스트 빌링
	@RequestMapping(value = "/test")
	public String testBilling(HttpServletRequest request,
                              @RequestParam String userNo,
                              @RequestParam String billingKey) {

		PaymentVo paymentVo = new PaymentVo();
		paymentVo.setUserNo(Integer.parseInt(userNo));
		paymentVo.setBillingKey(billingKey);
		paymentVo.setUserEmail("belsnake@mindslab.ai");
		paymentVo.setUserName("김종범");
		paymentVo.setGoodCode(3);
		paymentVo.setPrice(99000);

		try {
			paymentService.routineBill(paymentVo);
		} catch(Exception e) {
			logger.error(e.getMessage());
		}

		return "";
	}


	// 테스트 빌링
	@RequestMapping(value = "/getTargetBillingList")
	public String getTargetBillingList(HttpServletRequest request) {

		logger.error("getTargetBillingList ================================================");
		List<PaymentVo> RegularList = paymentService.selectRegularList();
		for(PaymentVo paymentVo : RegularList) {
			logger.error(paymentVo.toString());
		}
		logger.error("---------------------------------------------------------------------");

		return "";
	}

	// 빌링 결제 폼
	@RequestMapping(value = "/billingForm")
	public ModelAndView basicBilling(String method) throws Exception {
		
		logger.info("{} .basicBilling()", getClass().getSimpleName());

		return paymentService.basicBilling(method);
	}

	// 기본 결제 폼
	@RequestMapping(value = "/commonPayForm")
	public ModelAndView commonPayForm() throws Exception {

		logger.info("{} .commonPayForm()", getClass().getSimpleName());

		return paymentService.commonPayForm();
	}

	// 모바일 빌링
	@RequestMapping(value = "/mobilePay")
	public String mobileBilling(HttpServletRequest request) {
		String param = paymentService.mobilePay(request);

		return "redirect:https://inilite.inicis.com/inibill/inibill_card.jsp?" + param;
	}

	@RequestMapping(value = "/commonPay")
	public String commonPay(HttpServletRequest request) {
		paymentService.commonPay(request);

		return "redirect:/main/krMainHome";
	}

    @RequestMapping(value = "/billingChange")
    public String billingChange(@RequestParam(value = "email") String email,HttpServletRequest request, RedirectAttributes rediectAttr) throws Exception {

        String result = "";
        try {
            result = paymentService.billingChange(email, request, rediectAttr);
        } catch (Exception e) {
            logger.error(e.getMessage());
            return "redirect:/error/noti";
        }
        return result;
    }

    @RequestMapping(value = "/billingFreeRegistered")
    public String billingFreeRegistered(@RequestParam(value = "email") String email,
                                        HttpServletRequest request, RedirectAttributes rediectAttr) throws Exception {

        logger.info("{} .billingFreeRegistered()", getClass().getSimpleName());

        return paymentService.billingFreeRegistered(email, request, rediectAttr);
    }

	@RequestMapping(value = "/billingPay")
	public String billingPay(@RequestParam(value="email") String email,
							 @RequestParam(value="callback_uri") String callback_uri,
							 HttpServletRequest request, RedirectAttributes rediectAttr) {
		
		logger.info("{} .billingPay()", getClass().getSimpleName());
		request.getSession().setAttribute("callback_uri", callback_uri);
		return paymentService.billingPay(email, request, rediectAttr);
	}

	/*
	** Date: 2019. 07. 12. (By belsnake)
	** Desc: 요금 정책 변경으로 첫달 무료, 이후 정기 결제로 변경됨.
	*/
	@RequestMapping(value = "/billingFree")
	public String billingFree(@RequestParam(value="email") String email,
							  @RequestParam(value="callback_uri") String callback_uri,
							  HttpServletRequest request, RedirectAttributes rediectAttr) {

		logger.info("{} .billingFree()", getClass().getSimpleName());

		request.getSession().setAttribute("callback_uri", callback_uri);
		return paymentService.billingFree(email, request, rediectAttr);
	}

	@RequestMapping(value = "/mobileBillingFree")
	public String mobileBillingFree(HttpServletRequest request, RedirectAttributes rediectAttr) {

		logger.info("{} .mobileBillingFree()", getClass().getSimpleName());

		return paymentService.mobileBillingFree(request, rediectAttr);
	}

	/*
	 * 페이팔 결제 (by unongko)
	 * Modified by YGE - 2019. 10. 14
	 */

	@PostMapping(value = "/payPalBilling")
	@ResponseBody
	public Map<String, Object> payPalBilling(@RequestParam(value = "subsc_id") String subscId,
											 @RequestParam(value = "payMethod") String payMethod,
											 HttpServletRequest request, RedirectAttributes rediectAttr) {

		if( payMethod.equals("FREE")){ // 페이팔 무료 결제
			logger.info("{} .payPalBilling()  :: FREE", getClass().getSimpleName());
			return paymentService.payPalBillingFree(request, subscId);

		}else if( payMethod.equals("INSTANT")){ // 페이팔 즉시 결제
			logger.info("{} .payPalBilling()  :: INSTANT", getClass().getSimpleName());
			return paymentService.payPalBillingInstant(request, subscId);

		}else {
			logger.error(".paypalBilling() :: payMethod mismatched.");
			Map<String, Object> resultMap = new HashMap<>();
			resultMap.put("status", "fail");
			resultMap.put("redirectUri", "/?lang=en");
			return resultMap;
		}

	}

	@RequestMapping(value = "/confirmFail")
	public String confimFail(RedirectAttributes rediectAttr) {
		rediectAttr.addFlashAttribute("reason", "동일한 요금제로는 재결제 하실 수 없습니다.").addFlashAttribute("moid", "유효하지 않은 거래번호");
		return "redirect:/support/krPricingFail";
	}

	@RequestMapping(value = "/planConfirm")
	@ResponseBody
	public int planConfirm(String goodName, int userNo) {
		logger.debug("planConfirm");

		PaymentVo paymentVo = new PaymentVo();
		paymentVo.setGoodCode(paymentService.selectProductId(goodName));
		paymentVo.setUserNo(userNo);

		logger.debug(paymentVo.toString());

		return paymentService.planConfirm(paymentVo);
	}

	@RequestMapping(value = "/close")
	public String close() {
		
		logger.info("{} .close()", getClass().getSimpleName());	

		return "/kr/payment/close.pg";
	}
		
//	@RequestMapping(value = "/cancel")
//	public void cancelPay(HttpServletRequest request, String tid) {
//		paymentService.cancelPay(request,tid);
//	}
//
//	@RequestMapping("/cancelTid")
//	public void cancelPay(HttpServletRequest request) {
//		paymentService.cancelPay(request, "StdpayCARDmindslab0120190521161114037644");
//	}

	@RequestMapping("/planChangeConfirm")
	@ResponseBody
	public int planChangeConfirm(PaymentVo paymentVo) {
		paymentVo.setGoodCode(paymentService.selectProductId(paymentVo.getGoodName()));

		int result = paymentService.planChangeConfirm(paymentVo);
		logger.debug(paymentVo.getUserNo() + ", " + paymentVo.getGoodCode() + ", " + result);

		return result;
	}

	@RequestMapping("/planCancel")
	@ResponseBody
	public Map<String, Object> planCancel(HttpServletRequest request,
						   @RequestParam String payMethod,
						   @RequestParam int feedbackUsability,
						   @RequestParam int feedbackDemo,
						   @RequestParam String feedbackEtc) {

		logger.info("{} .planCancel() ", getClass().getSimpleName());

		MemberVo member = paymentService.getUserInfo(request);
		int userNo = member.getUserno();

		// 피드백 등록
		paymentService.insertFeedbackOnCancel(userNo, feedbackUsability, feedbackDemo, feedbackEtc);

		// 결제 방식에 따라 취소 나누기
		if("paypal".equalsIgnoreCase(payMethod)){
		    return paymentService.paypalPlanCancel(userNo);
        }else{
            return paymentService.planCancel(userNo);
        }
	}

	@RequestMapping("/payResult")
	public String payResult() {
		return "/kr/payment/payResult.pg";
	}

	/** 무료 체험 여부 파악 - LYJ 2019. 09. 11 */
	@RequestMapping("/checkFreeUsage")
	@ResponseBody
	public int checkFreeUsage(HttpServletRequest request) {

		HttpSession httpSession = request.getSession(true);

		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		int userNo = memberVo.getUserno();
		int result = paymentService.selectFreeUsage(userNo);
		
		return  result;
	}

//	@Scheduled(cron = "0 0 12 * * ?")
//	public void routineBill() {
//		if("start".equals(batchStatus)) {
//			logger.info("Batch schedule(routineBill) start!!");
//			
//			List <PaymentVo> RegularList = paymentService.selectRegularList();
//
//			for(PaymentVo paymentVo : RegularList) {
//				logger.debug(paymentVo.toString());
//				try {
//					paymentService.routineBill(paymentVo);
//				} catch(Exception e) {
//					logger.error(e.getMessage());
//				}
//			}			
//		}else{
//			logger.info("Batch schedule(routineBill) stop!!");
//		}
//
//	}

//	@Scheduled(cron = "0 */5 * * * *")
//	public void routineTest() {
//
//	}
}
