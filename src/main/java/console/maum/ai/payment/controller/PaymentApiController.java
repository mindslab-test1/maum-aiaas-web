package console.maum.ai.payment.controller;

import console.maum.ai.member.model.MemberVo;
import console.maum.ai.payment.model.PaymentVo;
import console.maum.ai.payment.service.PaymentService;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

@RestController
public class PaymentApiController {
	private final static Logger logger = LoggerFactory.getLogger(PaymentApiController.class);

	@Autowired
	private PaymentService paymentService;

	// Paypal IPN Listener
	@RequestMapping(value= "/payment/paypalIPN", method =  {RequestMethod.GET, RequestMethod.POST})
	public @ResponseBody String paypalIPNListener(HttpServletRequest request, HttpServletResponse response) throws IOException {
		logger.info("------- PaymentApiController paypalIPN listener --------");

		String strResult = "";
		strResult = paymentService.paypalIPNListener(request);

		return strResult;
	}
}
