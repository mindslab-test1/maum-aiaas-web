package console.maum.ai.conversion.controller;


import console.maum.ai.conversion.service.ConversionService;
import console.maum.ai.member.model.MemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping(value = "/api/konglish")
public class ConversionApiController {

    @Autowired
    private ConversionService conversionService;
    private static final Logger logger = LoggerFactory.getLogger(ConversionApiController.class);



    //한글 변환 단어

    @RequestMapping(value = "/words", produces = "application/text; charset=utf8")
    @ResponseBody
    public String getApiWord (@RequestParam(value = "word") List word, HttpServletRequest request) throws IOException {

        logger.info("===== getApiWord ApiController =====");

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        String result = conversionService.getApiWord(apiId, apiKey, word);
        return result;

    }


    //한글 변환 문장

    @RequestMapping(value = "/sentence",  produces = "application/text; charset=utf8")
    @ResponseBody
    public String getApiSentence(@RequestParam(value = "sentence") String sentence, HttpServletRequest request) throws IOException{

        logger.info("===== getApiSentence ApiController =====");

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        String result =conversionService.getApigetApiSentence(apiId, apiKey, sentence);
        return result;
    }
}
