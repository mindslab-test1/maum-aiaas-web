package console.maum.ai.conversion.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/conversion")
public class ConversionController {

    private static final Logger logger = LoggerFactory.getLogger(ConversionController.class);

    /**
     * conversion Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/conversionMain")
    public String conversionMain(HttpServletRequest request, Model model) {
        logger.info("Welcome conversionMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "conversionMain");
        httpSession.setAttribute("menuName", "언어");
        httpSession.setAttribute("subMenuName", "한글 변환");
        httpSession.setAttribute("menuGrpName", "언어");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/conversion/conversionMain.pg";

        return returnUri;
    }
    /**
     * conversion Main (EN)
     * @param model
     * @return
     */
    @RequestMapping(value = "/enConversionMain")
    public String enConversionMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enConversionMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "conversionMain");
        httpSession.setAttribute("menuName", "Languages");
        httpSession.setAttribute("subMenuName", "Konglish");
        httpSession.setAttribute("menuGrpName", "Languages");

        model.addAttribute("lang", "en");
        String returnUri = "/en/conversion/conversionMain.pg";

        return returnUri;
    }
}
