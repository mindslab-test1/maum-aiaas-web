package console.maum.ai.conversion.service;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.List;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})

@Service
public class ConversionService {
    @Value("${api.url}")
    private String mUrl_ApiServer;
    private static final Logger logger = LoggerFactory.getLogger(ConversionService.class);

    public String getApiWord(String apiId, String apiKey, List word)throws IOException {

        try {
            String url = mUrl_ApiServer + "/konglish/words";


            String logMsg = "\n===========================================================================\n";
            logMsg += "Conversion word API @ PARAMS \n";
            logMsg += String.format(":: %-10s = %s%n", "URL", url);
            logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
            logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
            logMsg += String.format(":: %-10s = %s%n", "word", word);
            logMsg += "===========================================================================";
            logger.info(logMsg);


            JSONObject json = new JSONObject();
            json.put("apiId", apiId);
            json.put("apiKey", apiKey);
            json.put("wordList", word);

            CloseableHttpClient client = HttpClientBuilder.create().build();

            HttpPost post = new HttpPost(url);
            post.setHeader("Content-Type", "application/json");
            StringEntity entity = new StringEntity(json.toString(), "UTF-8");

            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            String resultMsg = EntityUtils.toString(response.getEntity(), "UTF-8");
            logger.info("Response Code : {}", response.getStatusLine().getStatusCode());
            logger.info("Response Data : {}", resultMsg);

            return resultMsg;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    public String getApigetApiSentence(String apiId, String apiKey, String sentence)throws IOException {

        try {
            String url = mUrl_ApiServer + "/konglish/sentence";

            String logMsg = "\n===========================================================================\n";
            logMsg += "Conversion sentence API @ PARAMS \n";
            logMsg += String.format(":: %-10s = %s%n", "URL", url);
            logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
            logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
            logMsg += String.format(":: %-10s = %s%n", "sentence", sentence);
            logMsg += "===========================================================================";
            logger.info(logMsg);

            JSONObject json = new JSONObject();
            json.put("apiId", apiId);
            json.put("apiKey", apiKey);
            json.put("sentence", sentence);

            CloseableHttpClient client = HttpClientBuilder.create().build();

            HttpPost post = new HttpPost(url);
            post.setHeader("Content-Type", "application/json");
            StringEntity entity = new StringEntity(json.toString(), "UTF-8");

            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            String resultMsg = EntityUtils.toString(response.getEntity(), "UTF-8");
            logger.info("Response Code : {}", response.getStatusLine().getStatusCode());
            logger.info("Response Data : {}", resultMsg);


            return resultMsg;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
