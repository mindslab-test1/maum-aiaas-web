package console.maum.ai.idr.service;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.BodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",
		"classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@Service
public class IdrService {

	private static final Logger logger = LoggerFactory.getLogger(IdrService.class);

	@Value("${api.smartX}")
	private String mUrl_smartX;
	@Value("${api.url.idr}")
	private String mUrl_idr;

	public Map<Integer, ResponseEntity> hospitalReceiptRecog(MultipartFile file, String uploadPath) {

		if(file == null) {
			throw new RuntimeException("File is not exist!");
		}
		logger.debug(" @ Idr_Hospital image file size : " + file.getSize());

		HashMap<Integer, ResponseEntity> map = new HashMap<>();

		try {
			String url = mUrl_smartX + "/HospitalReceipt"; // 임시방편 : 엔진 서버를 직접 찌르는 것으로 설정 -> smartX의 수정 필요
			HttpClient client = HttpClients.createDefault(); //요청할 client 생성
			HttpPost post = new HttpPost(url); // post 방식으로 요청

			File hospitalReceiptVarFile = new File(uploadPath);

			if(!hospitalReceiptVarFile.exists()) {
				hospitalReceiptVarFile.mkdirs();
			}

			hospitalReceiptVarFile = new File((uploadPath + "/" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
			file.transferTo(hospitalReceiptVarFile);

			logger.debug("Dest file name = {}", hospitalReceiptVarFile.getAbsolutePath());
			logger.info(" @ Idr_Hospital uploadPath : {}", uploadPath);

			FileBody fileBody = new FileBody(hospitalReceiptVarFile);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();

			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

			builder.addPart("receipt_img", fileBody);
			//builder.addPart("apiId", new StringBody(mAdmin_ApiId, ContentType.MULTIPART_FORM_DATA));
			//builder.addPart("apiKey", new StringBody(mAdmin_APiKey, ContentType.MULTIPART_FORM_DATA));

			HttpEntity entity = builder.build();
			post.setEntity(entity);
			HttpResponse response = client.execute(post);
			int responseCode = response.getStatusLine().getStatusCode();

			if (responseCode != 200) {
				throw new RuntimeException(" @ Idr_Hospital ErrCode : " + responseCode);
			}

			InputStream in = new BufferedInputStream(response.getEntity().getContent());

			ByteArrayDataSource dataSource = new ByteArrayDataSource(in, "multipart/form-data");
			MimeMultipart multipart = new MimeMultipart(dataSource);

			int count = multipart.getCount();
			logger.info("response count = {}", count);

			for(int i=0 ; i<count ; i++) {

				BodyPart bodyPart = multipart.getBodyPart(i);
				HttpHeaders headers = new HttpHeaders();
				headers.setCacheControl(CacheControl.noCache().getHeaderValue());

				if(bodyPart.isMimeType("image/jpeg")){
					logger.info(bodyPart.getContentType());

					BufferedReader rd = new BufferedReader(new InputStreamReader(bodyPart.getInputStream()));
					StringBuffer result = new StringBuffer();
					String line = "";
					while( (line=rd.readLine()) != null) {
						result.append(line);
					}

					String strResult = result.toString();
//					logger.info(strResult);
					ResponseEntity<String> resultEntity = new ResponseEntity<String>(strResult, headers, HttpStatus.OK);

					map.put(i, resultEntity);
				} else if(bodyPart.isMimeType("text/csv")) {
					logger.info(bodyPart.getContentType());

					BufferedReader rd = new BufferedReader(new InputStreamReader(bodyPart.getInputStream(),"utf-8"));
//					StringBuffer result = new StringBuffer();
					List<String> result = new ArrayList<>();
					String line = "";
					while( (line=rd.readLine()) != null ) {
//						result.append(line);
                        result.add(line);
					}

					String strResult = result.toString();
                    logger.info(strResult);

                    ResponseEntity<List> resultEntity = new ResponseEntity<>(result, headers, HttpStatus.OK);

					map.put(i, resultEntity);
				} else {
					logger.warn("It is not a image/jpeg & text/csv : {}", bodyPart.getContentType());
				}

			}
			//hospitalReceiptVarFile.delete();
		} catch (Exception e) {
			logger.error(" @ Idr_Hospital Exception : " + e);
		}
		return map;
	}

	public String callRecog(MultipartFile file, String uploadPath) {
		
		System.out.println("CallRecog");
		
		String callUrl = mUrl_idr + "/call_recog";
		String crlf = "\r\n";
		String boundary =  "*****";
		String result = "";

		try {
			
			URL url = new URL(callUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			
			conn.setRequestProperty("Content-Type", "multipart/form-data;charset=utf-8;boundary=" + boundary);
			conn.setRequestMethod("POST");
			conn.setUseCaches(false);
			conn.setDoInput(true);
			conn.setDoOutput(true);
			
			OutputStream out = conn.getOutputStream();
			PrintWriter writer = new PrintWriter(new OutputStreamWriter(out, "UTF-8"), true);



			writer.append("--" + boundary).append(crlf);
            writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + file.getName() + "\"").append(crlf);
            writer.append("Content-Type: " + URLConnection.guessContentTypeFromName(file.getName())).append(crlf);
            writer.append("Content-Transfer-Encoding: binary").append(crlf);
            writer.append(crlf);
            writer.flush();



            File filePath = new File(uploadPath + "/idr/");
            if (!filePath.exists()) {
				filePath.mkdirs();
			}
			File uploadFile = new File(uploadPath + "/idr/" + file.getName());
            file.transferTo(uploadFile);
            
            FileInputStream inputStream = new FileInputStream(uploadFile);
            byte[] buffer = new byte[(int)uploadFile.length()];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                out.write(buffer, 0, bytesRead);
            }
            out.flush();
            inputStream.close();
            writer.append(crlf);
            writer.flush();

            writer.append("--" + boundary + "--").append(crlf);
            writer.close();

			int code = conn.getResponseCode();
			System.out.println("Response Code : " + code);
			
			BufferedReader br = null;
			
			if (code == 200) {
				br = new BufferedReader(new InputStreamReader((conn.getInputStream()), "utf-8"));
			} else {
				br = new BufferedReader(new InputStreamReader((conn.getErrorStream()), "utf-8"));
			}
			
			while(true) {
				String line = br.readLine();
				if(line == null) break;
				result = result +line;

//				System.out.println(line);
			}

			return result;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

}