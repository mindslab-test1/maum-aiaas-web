package console.maum.ai.idr.controller;

import java.sql.Blob;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import console.maum.ai.common.util.PropertyUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import console.maum.ai.idr.service.IdrService;


@Controller
@RequestMapping(value = "/cloudApi/idr")
public class IdrController {

	private static final Logger logger = LoggerFactory.getLogger(IdrController.class);

	@Autowired
	private IdrService idrService;

	/**
	 * idr Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krIdrMain")
	public String krIdrMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krIdrMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "idrMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "문서 이미지 분석");
		httpSession.setAttribute("menuGrpName", "시각");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/idr/idrMain.pg";

		return returnUri;
	}
	
	/**
	 * idr Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enIdrMain")
	public String enIdrMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enIdrMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "idrMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "Document Recognition");	
		httpSession.setAttribute("menuGrpName", "Vision");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/idr/idrMain.pg";
		
		return returnUri;
	}	

	@RequestMapping(value = "/callRecog")
	@ResponseBody
	public String callRecog(@RequestParam MultipartFile file) {
		
		return idrService.callRecog(file, PropertyUtil.getUploadPath());
	}
	
}
