package console.maum.ai.idr.controller;

import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.idr.service.IdrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
public class IdrApiController {

    @Autowired
    private IdrService idrService;

    @RequestMapping(value = "/api/hospitalReceipt")
    @ResponseBody
    public Map<Integer, ResponseEntity> multiHospitalReceipt(@RequestParam MultipartFile file){
        return idrService.hospitalReceiptRecog(file, PropertyUtil.getUploadPath());
    }
}
