package console.maum.ai.positioning.controller;


import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.positioning.service.PositioningService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class PositioningApiController {

    @Autowired
    private PositioningService positioningService;

    @RequestMapping(value="/api/bracket/Ab")
    public ResponseEntity<byte[]> apiBracketAb(@RequestParam MultipartFile file, HttpServletRequest request){

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        return positioningService.getApiBracketAb(apiId, apiKey, file, PropertyUtil.getUploadPath() + "/bracketAb/");


    }

    @RequestMapping(value="/api/bracket/Region")
    public ResponseEntity<byte[]> apiBracketRegion(@RequestParam MultipartFile file, HttpServletRequest request){

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        return positioningService.getApiBracketRegion(apiId, apiKey, file, PropertyUtil.getUploadPath() + "/bracketRegion/");


    }

}
