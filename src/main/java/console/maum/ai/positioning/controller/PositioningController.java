package console.maum.ai.positioning.controller;


import console.maum.ai.conversion.controller.ConversionController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/positioning")
public class PositioningController {

    private static final Logger logger = LoggerFactory.getLogger(ConversionController.class);

    /**
     * positioning Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/positioningMain")
    public String positioningMain(HttpServletRequest request, Model model) {
        logger.info("Welcome positioningMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "positioningMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "치아 교정기 포지셔닝");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/positioning/positioningMain.pg";

        return returnUri;
    }
    /**
     * positioning Main (EN)
     * @param model
     * @return
     */
    @RequestMapping(value = "/enPositioningMain")
    public String enPositioningMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enPositioningMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "positioningMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Bracket Positioning");
        httpSession.setAttribute("menuGrpName", "Vision");

        model.addAttribute("lang", "en");
        String returnUri = "/en/positioning/positioningMain.pg";

        return returnUri;
    }
}

