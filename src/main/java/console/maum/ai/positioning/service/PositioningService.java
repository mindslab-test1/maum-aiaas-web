package console.maum.ai.positioning.service;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@Service
public class PositioningService {

    private static final Logger logger = LoggerFactory.getLogger(PositioningService.class);

    @Value("${api.url}")
    private String mUrl_ApiServer;


    public ResponseEntity<byte[]> getApiBracketAb(String apiId, String apiKey, MultipartFile file, String uploadPath){

        if(file == null) {
            throw new RuntimeException("File is not exist!");
        }

        String url = mUrl_ApiServer + "/bracket/downloadAb";

        String logMsg = "\n===========================================================================\n";
        logMsg += "BracketAb API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "file", file.getOriginalFilename());
        logMsg += "===========================================================================";
        logger.info(logMsg);

        return apiRequest(url,apiId, apiKey, file, uploadPath);
    }


    public ResponseEntity<byte[]> getApiBracketRegion(String apiId, String apiKey, MultipartFile file, String uploadPath){

        if(file == null) {
            throw new RuntimeException("File is not exist!");
        }

        String url = mUrl_ApiServer + "/bracket/downloadRegion";

        String logMsg = "\n===========================================================================\n";
        logMsg += "BracketRegion API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "file", file.getOriginalFilename());
        logMsg += "===========================================================================";
        logger.info(logMsg);

        return apiRequest(url,apiId, apiKey, file, uploadPath);
    }



    private ResponseEntity<byte[]> apiRequest(String url, String apiId, String apiKey, MultipartFile file, String uploadPath){

        try{
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File positioningFile = new File(uploadPath);

            if(!positioningFile.exists()) {
                logger.info("create Dir : {}", positioningFile.getPath());
                positioningFile.mkdirs();
            }

            positioningFile = new File(uploadPath + apiId + "_" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1));
            file.transferTo(positioningFile);
            FileBody fileBody = new FileBody(positioningFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiKey",new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("file", fileBody);

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();
            String api = url.substring(url.lastIndexOf("/")+1);
            logger.info("responseCode = {} || API = {} || apiId = {}" , responseCode, api, apiId);


            if (responseCode != 200) {
                positioningFile.delete();
                logger.error("API fail : {}", response.getEntity().getContent());
                return ResponseEntity.status(responseCode).body(null);
                // return null;
                // throw new Error("API fail : " + responseCode);
            }

            HttpEntity responseEntity = response.getEntity();
            HttpHeaders headers = new HttpHeaders();
            InputStream in = responseEntity.getContent();
            byte[] resultArray = IOUtils.toByteArray(in);

            ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(resultArray, headers, HttpStatus.OK);

            positioningFile.delete();

            return resultEntity;

        } catch (Exception e) {
            logger.error("API exception : {}", e.toString());
            e.printStackTrace();
        }

        return null;
    }

}
