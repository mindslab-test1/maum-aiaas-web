package console.maum.ai.anomalyLoitering.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/anomalyLoitering")
public class AnomalyLoiteringController {

    private static final Logger logger = LoggerFactory.getLogger(AnomalyLoiteringController.class);

    /**
     * anomalyLoitering Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/krAnomalyLoiteringMain")
    public String krAnomalyLoiteringMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krAnomalyLoiteringMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "anomalyLoiteringMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "배회 감지");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/anomalyLoitering/anomalyLoiteringMain.pg";

        return returnUri;
    }

    @RequestMapping(value = "/enAnomalyLoiteringMain")
    public String enAnomalyLoiteringMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enAnomalyLoiteringMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "anomalyLoiteringMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Loitering Detection");
        httpSession.setAttribute("menuGrpName", "Vision");

        model.addAttribute("lang", "en");
        String returnUri = "/en/anomalyLoitering/anomalyLoiteringMain.pg";

        return returnUri;
    }


}