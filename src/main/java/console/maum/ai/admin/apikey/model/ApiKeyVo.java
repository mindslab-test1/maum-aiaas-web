package console.maum.ai.admin.apikey.model;

import console.maum.ai.common.model.CommonDto;

public class ApiKeyVo extends CommonDto {
    private String id;
    private String email;
    private String apiKey;
    private String apiId;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getApiKey() {
        return apiKey;
    }
    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
    public String getApiId() {
        return apiId;
    }
    public void setApiId(String apiId) {
        this.apiId = apiId;
    }
}
