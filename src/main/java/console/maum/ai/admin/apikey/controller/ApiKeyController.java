package console.maum.ai.admin.apikey.controller;

import console.maum.ai.member.dao.MemberDao;
import console.maum.ai.member.model.MemberDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(value = "/admin/apikey")
public class ApiKeyController {

    private static final Logger logger = LoggerFactory.getLogger(ApiKeyController.class);

    @Autowired
    private MemberDao memDao;

    @RequestMapping(value = "/registerApiKeyMain")
    public String krResistApiMain(@RequestParam("email") String email, Model model) throws Exception{
        logger.info("welcome registerApiKeyMain");

        String returnUrl = "noLayout/kr/apikey/registerApiKeyMain.pg";
        MemberDto userInfo = memDao.getMemberDetail(email);

        logger.debug(userInfo.getEmail());
        logger.debug(userInfo.getName());
        logger.debug(userInfo.getPhone());

        model.addAttribute("user", userInfo);

        return returnUrl;
    }
}
