package console.maum.ai.admin.apikey.dao;

import console.maum.ai.admin.apikey.model.ApiKeyVo;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class ApiKeyDao {
    private final static Logger logger = LoggerFactory.getLogger(ApiKeyDao.class);

    @Resource(name = "sqlSession")
    private SqlSession sqlSession;

    private static final String NAMESPACE = "console.maum.ai.model.registerApiIdMapper";

    public List<String> getAllApiId() throws Exception{
        return sqlSession.selectList(NAMESPACE+".getAllApiId");
    }

    public ApiKeyVo getUserApi(String email) throws Exception{
        return sqlSession.selectOne(NAMESPACE+".getUserApi", email);
    }

    public void registApiId(ApiKeyVo registApiVo) throws Exception{
        sqlSession.update(NAMESPACE+".registApiId", registApiVo);
    }

}
