package console.maum.ai.tts.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
@RequestMapping(value = "/cloudApi/tts")
public class TtsController {

	private static final Logger logger = LoggerFactory.getLogger(TtsController.class);

	/**
	 * tts Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krTtsMain")
	public String krTtsMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krTtsMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "ttsMain");
		httpSession.setAttribute("menuName", "음성");
		httpSession.setAttribute("subMenuName", "음성생성(TTS)");
		httpSession.setAttribute("menuGrpName", "음성");

		model.addAttribute("lang", "ko");
		return "/kr/tts/ttsMain.pg";
	}
	
	/**
	 * tts Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enTtsMain")
	public String enTtsMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enTtsMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "ttsMain");
		httpSession.setAttribute("menuName", "Voice");
		httpSession.setAttribute("subMenuName", "Speech Generation");
		httpSession.setAttribute("menuGrpName", "Speech");
		
		model.addAttribute("lang", "en");
		return "/en/tts/ttsMain.pg";
	}

}
