package console.maum.ai.tts.controller;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.tts.service.TtsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.Map;


@Controller
@RequestMapping(value = "/api/tts")
public class TtsApiController {

	private static final Logger logger = LoggerFactory.getLogger(TtsApiController.class);

	@Autowired
	private TtsService ttsService;


	@RequestMapping(value = "/ttsStream")
	@ResponseBody
	public ResponseEntity<StreamingResponseBody> ttsApi(
			@RequestBody String requestBody,
			HttpServletRequest request
	){

		JsonParser parser = new JsonParser();

		JsonElement element =parser.parse(requestBody);

		String text = element.getAsJsonObject().get("text").getAsString();
		String voiceName = element.getAsJsonObject().get("voiceName").getAsString();

		HttpSession httpSession = request.getSession(true);

		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		String apiId = memberVo.getApiId();
		String apiKey = memberVo.getApiKey();
		String savedId = memberVo.getId();

		return ttsService.ttsApi(apiId, apiKey,savedId, text, voiceName, PropertyUtil.getUploadPath());

	}



	/** 결과파일 다운로드 */
	@RequestMapping(value = "/ttsDwn")
	public ModelAndView fileDownload(HttpServletRequest request, @RequestParam("fileNameKey") String fileNameKey
			, @RequestParam("fileName") String fileName) {

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		String savedId = memberVo.getId();

		/* 첨부파일 정보 조회 */
		Map<String, Object> fileInfo = new HashMap<>();
		fileInfo.put("fileNameKey", fileNameKey);
		fileInfo.put("fileName", fileName);
		fileInfo.put("filePath", PropertyUtil.getUploadPath()+"/tts/"+savedId+"/");

		logger.info("fileDownload fileInfo ::::::  {}", fileInfo);

		return new ModelAndView("fileDownloadUtil", "fileInfo", fileInfo);
	}
}
