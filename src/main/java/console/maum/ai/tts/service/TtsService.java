package console.maum.ai.tts.service;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",
		"classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@Service
public class TtsService {
	private static final Logger logger = LoggerFactory.getLogger(TtsService.class);

	@Value("${api.url}")
	private String apiDomainUrl;

	public ResponseEntity<StreamingResponseBody> ttsApi(String apiId, String apiKey, String saveId,
														String text, String voiceName, String uploadPath){

		String url = apiDomainUrl + "/tts/stream";

		String logMsg = "\n===========================================================================\n";
		logMsg += "TTS API @ PARAMS \n";
		logMsg += String.format(":: %-10s = %s%n", "URL", url);
		logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
		logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
		logMsg += String.format(":: %-10s = %s%n", "text", text);
		logMsg += String.format(":: %-10s = %s%n", "voiceName", voiceName);
		logMsg += "===========================================================================";
		logger.info(logMsg);

		try {

			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(url);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addPart("text", new StringBody(text, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("apiKey",new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("voiceName", new StringBody(voiceName, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			HttpEntity entity = builder.build();
			post.setEntity(entity);

			HttpResponse response = client.execute(post);
			int responseCode = response.getStatusLine().getStatusCode();
			HttpEntity responseEntity = response.getEntity();
			logger.info("TTS responseCode = {}" , responseCode);

			// Response Error
			if (responseCode != 200) {
				String responseString = "";
				if(responseEntity!=null) {
					responseString = EntityUtils.toString(responseEntity, "UTF-8");
				}
				logger.error("API TTS fail : \n{}", responseString);
				return null;
			}

			// Response OK
			// For saving file to server
			File ttsFile = new File(uploadPath+"/tts/"+saveId);
			if(!ttsFile.exists()){
				logger.info("create out dir : {}", ttsFile.getPath());
				ttsFile.mkdirs();
			}

			FileOutputStream fos = new FileOutputStream(ttsFile+"/TTS.wav");
			InputStream inputStream = responseEntity.getContent();

			// streaming으로 보낼 데이터
			StreamingResponseBody responseBody = outputStream -> {
				int numberOfBytesToWrite;
				byte[] data = new byte[1024];

				try{

					logger.info("TTS writing bytes start!");
					while ((numberOfBytesToWrite = inputStream.read(data, 0, data.length)) != -1) {
						outputStream.write(data, 0, numberOfBytesToWrite);
						fos.write(data, 0, numberOfBytesToWrite);
					}
					logger.info("TTS writing bytes end!");

					fos.flush();
					logger.info("data flushed to file ==> {}", ttsFile.getPath() + "/TTS.wav");

					inputStream.close();
					fos.close();

				} catch (Exception e){
					// 데이터 스트리밍 중 사용자가 새로고침 or 이동 한 경우 exception이 발생하여 간단히 log만 찍어줌
					logger.error("API TTS stream response exception : {}", e.getCause().toString());
				} finally {
					inputStream.close();
					fos.close();
				}
			};

			return ResponseEntity.ok()
					.body(responseBody);

		} catch (Exception e) {
			logger.error("API TTS exception : {}", e.toString());
			e.printStackTrace();
		}

		return null;
	}

}