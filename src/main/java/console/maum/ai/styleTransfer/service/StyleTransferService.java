package console.maum.ai.styleTransfer.service;

import console.maum.ai.common.data.CommonMsg;
import console.maum.ai.common.data.CommonResponse;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

import java.io.IOException;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@Service
public class StyleTransferService {
    private static final Logger logger = LoggerFactory.getLogger(StyleTransferService.class);

    @Value("${api.url}")
    private String mUrl_ApiServer;

    public CommonResponse<Object> getApiStyleTransfer(String apiId, String apiKey, String originalText, int numReturnSequences, String targetStyle) throws IOException {

        String url = mUrl_ApiServer + "/text-style/transfer";

        String logMsg = "\n===========================================================================\n";
        logMsg += "TextRemoval API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "originalText", originalText);
        logMsg += String.format(":: %-10s = %s%n", "numReturnSequences", numReturnSequences);
        logMsg += String.format(":: %-10s = %s%n", "targetStyle", targetStyle);
        logMsg += "===========================================================================";
        logger.info(logMsg);

        JSONObject json = new JSONObject();
        json.put("apiId", apiId);
        json.put("apiKey", apiKey);
        json.put("originalText", originalText);
        json.put("numReturnSequences", numReturnSequences);
        json.put("targetStyle", targetStyle);

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            HttpPost post = new HttpPost(url);
            post.setHeader("Content-Type", "application/json");
            StringEntity entity = new StringEntity(json.toString(), "UTF-8");

            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            String resultMsg = EntityUtils.toString(response.getEntity(), "UTF-8");
            logger.info("Response Code : {}", response.getStatusLine().getStatusCode());
            logger.info("Response Data : {}", resultMsg);

            JSONObject jsonObject = new JSONObject(resultMsg);

            CommonResponse<Object> result = new CommonResponse<>();
            if(response.getStatusLine().getStatusCode() == 200) {
                result.setCode(CommonMsg.ERR_CODE_SUCCESS);
                result.setMsg(CommonMsg.ERR_MSG_SUCCESS);
                result.setData(jsonObject.get("payload"));
            } else {
                result.setCode(response.getStatusLine().getStatusCode());
                result.setMsg(String.valueOf(jsonObject.get("message")));
                result.setData(null);
            }
            return result;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}