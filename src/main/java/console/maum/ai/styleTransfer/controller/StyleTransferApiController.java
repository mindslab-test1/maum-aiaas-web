package console.maum.ai.styleTransfer.controller;

import console.maum.ai.common.data.CommonResponse;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.styleTransfer.service.StyleTransferService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@RestController
public class StyleTransferApiController {

    @Autowired
    private StyleTransferService service;

    @PostMapping(value = "/api/styleTransfer/styleTransferApi")
    @ResponseBody
    public CommonResponse<Object> getStyleTransfer(@RequestParam(value = "originalText") String originalText,
                                           @RequestParam(value = "numReturnSequences") int numReturnSequences,
                                           @RequestParam(value = "targetStyle") String targetStyle,
                                           HttpServletRequest request) throws IOException {

        HttpSession httpSession= request.getSession(true);

        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        return service.getApiStyleTransfer(apiId, apiKey, originalText, numReturnSequences, targetStyle);
    }
}
