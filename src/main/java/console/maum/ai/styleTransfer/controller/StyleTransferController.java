package console.maum.ai.styleTransfer.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/styleTransfer")
public class StyleTransferController {

    private static final Logger logger = LoggerFactory.getLogger(StyleTransferController.class);

    @RequestMapping(value = "/krStyleTransferMain")
    public String krTextStyleTransferMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krTextStyleTransferMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "styleTransfer");
        httpSession.setAttribute("menuName", "언어");
        httpSession.setAttribute("subMenuName", "스타일 변환");
        httpSession.setAttribute("menuGrpName", "언어");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/styleTransfer/styleTransferMain.pg";

        return returnUri;
    }

    @RequestMapping(value = "/enStyleTransferMain")
    public String enTextStyleTransferMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enTextStyleTransferMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "styleTransfer");
        httpSession.setAttribute("menuName", "Languages");
        httpSession.setAttribute("subMenuName", "TextStyleTransfer");
        httpSession.setAttribute("menuGrpName", "Languages");

        model.addAttribute("lang", "en");
        String returnUri = "/en/styleTransfer/styleTransferMain.pg";

        return returnUri;
    }

}
