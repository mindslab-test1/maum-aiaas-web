package console.maum.ai.main.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import console.maum.ai.login.service.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/main")
public class MainController {
	@Value("${datatool.url}")
	private String dataEditorTool;

	@Autowired
	private LoginService loginService;

	private static final Logger logger = LoggerFactory.getLogger(MainController.class);

	/**
	 * 메인
	 * @param model
	 * @return
	 */	
	@RequestMapping(value = "/krMainHome")
	public String krMainHome(HttpServletRequest request, Model model) {
		logger.info("Welcome krMainHome!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "mainHome");
		httpSession.setAttribute("menuName", "HOME");
		httpSession.setAttribute("subMenuName", "");		
		httpSession.setAttribute("lang", "ko");
		httpSession.setAttribute("menuGrpName", "");
		model.addAttribute("maumDataLink", dataEditorTool);
		model.addAttribute("lang", "ko");

		loginService.changeCookie(request, "ko");

		String returnUri = "/kr/main/mainHome.pg";	
		
		return returnUri;
	}
	
	@RequestMapping(value = "/enMainHome")
	public String enMainHome(HttpServletRequest request, Model model) {
		logger.info("Welcome enMainHome!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "mainHome");
		httpSession.setAttribute("menuName", "HOME");
		httpSession.setAttribute("subMenuName", "");
		httpSession.setAttribute("lang", "en");
		httpSession.setAttribute("menuGrpName", "");
		
		model.addAttribute("lang", "en");		
		String returnUri = "/en/main/mainHome.pg";

		loginService.changeCookie(request, "en");

		return returnUri;
	}


}