package console.maum.ai.serviceLanding;

import console.maum.ai.login.service.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/serviceLanding")
public class ServiceLandingController {

    @Autowired
    private LoginService loginService;

    @Value("${url.domain}")
    private String mDomain;

    @Value("${footer.email}")
    private String mFooterEmail;

    @Value("${hq.URL}")
    private String sso_url;

    @Value("${hq.client_id}")
    private String client_id;

    @Value("${hq.redirect_uri}")
    private String redirect_uri;

    private static final Logger logger = LoggerFactory.getLogger(ServiceLandingController.class);



    /**
     * subLanding Main
     * @param model
     * @return
     */

    @RequestMapping(value = "/krMaumMinutesMain")
    public String krMaumMinutesMain(HttpServletRequest request, Model model) {

        logger.info("Welcome krMaumMinutesMain!");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("menuName", "maum Minutes");
        model.addAttribute("lang", "ko");
        String returnUri = "noLayout/kr/serviceLanding/maumMinutesMain.pg";

        return returnUri;
    }




    @RequestMapping(value = "/krAiBuilderMain")
    public String krAiBuilderMain(HttpServletRequest request, Model model) {

        logger.info("Welcome krAiBuilderMain!");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("menuName", "ai Builder");
        model.addAttribute("lang", "ko");
        String returnUri = "noLayout/kr/serviceLanding/aiBuilderMain.pg";

        return returnUri;
    }

    @RequestMapping(value = "/krcloudApiServiceMain")
    public String krCloudApiServiceMain(HttpServletRequest request, Model model) {

        logger.info("Welcome krcloudApiServiceMain!");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("menuName", "cloudApiServiceMain");
        model.addAttribute("lang", "ko");
        String returnUri = "noLayout/kr/login/cloudApiServiceMain.pg";

        return returnUri;
    }

    /**
     * subLanding 공통 footer 페이지
     * @param model
     * @return
     */
    @RequestMapping(value = "/krFooter")
    public String krFooter(HttpServletRequest request,
                           HttpServletResponse response,
                           @RequestParam(value="email", required = false) String email,
                           Model model) {
//        String lang = loginController.setLang(request, response);

        logger.info("Welcome krFooter!");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("menuName", "");

        model.addAttribute("lang", "ko");
        model.addAttribute("email", email != null ? email : mFooterEmail);

        String returnUri = "noLayout/kr/serviceLanding/footer.pg";

        return returnUri;
    }

    @RequestMapping(value = "/cloudApi")
    public String CloudApi(HttpServletRequest request, Model model) {
        logger.info("Welcome CloudApi!");

        String returnUri = "redirect:/serviceLanding/enCloudApiMain";
        Cookie[]cookies = request.getCookies();
        for(Cookie cookie: cookies) {
            if(cookie.getName().equalsIgnoreCase("lang") && cookie.getValue().equalsIgnoreCase("ko_KR")) returnUri = "redirect:/serviceLanding/krCloudApiMain";
            if(cookie.getName().equalsIgnoreCase("lang") && cookie.getValue().equalsIgnoreCase("ko")) returnUri = "redirect:/serviceLanding/krCloudApiMain";
            if(cookie.getName().equalsIgnoreCase("lang") && cookie.getValue().equalsIgnoreCase("kr")) returnUri = "redirect:/serviceLanding/krCloudApiMain";
        }

        return returnUri;
    }

    // cloud api - 한글
    @RequestMapping(value = "/krCloudApiMain")
    public String krCloudApiMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krCloudApiMain!");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("menuName", "cloud Api");

        model.addAttribute("lang", "ko");
        model.addAttribute("sso_url", sso_url);
        model.addAttribute("client_id", client_id);
        model.addAttribute("redirect_uri", redirect_uri);

        loginService.changeCookie(request, "ko");

        String returnUri = "noLayout/kr/serviceLanding/cloudApiMain.pg";

        return returnUri;
    }


    // cloud api - 영문
    @RequestMapping(value = "/enCloudApiMain")
    public String enCloudApiMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enCloudApiMain!");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("menuName", "cloud Api");

        model.addAttribute("lang", "en");
        model.addAttribute("sso_url", sso_url);
        model.addAttribute("client_id", client_id);
        model.addAttribute("redirect_uri", redirect_uri);

        loginService.changeCookie(request, "en");

        String returnUri = "noLayout/en/serviceLanding/cloudApiMain.pg";

        return returnUri;
    }

    /**
     * subLanding 공통 footer 페이지
     * @param model
     * @return
     */
    @RequestMapping(value = "/enFooter")
    public String enFooter(HttpServletRequest request,
                           HttpServletResponse response,
                           @RequestParam(value="email", required = false) String email,
                           Model model) {
//        String lang = loginController.setLang(request, response);

        logger.info("Welcome enFooter!");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("menuName", "");

        model.addAttribute("lang", "en");
        model.addAttribute("email", email != null ? email : mFooterEmail);

        String returnUri = "noLayout/en/serviceLanding/footer.pg";

        return returnUri;
    }

}
