package console.maum.ai.clothingDetection.controller;


import console.maum.ai.clothingDetection.service.ClothingDetectionService;
import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@RestController
public class ClothingDetectionApiController {

    @Autowired
    private ClothingDetectionService service;

    @RequestMapping(value="/api/feat/getClothFeature")
    @ResponseBody
    public ResponseEntity<byte[]> clothingDetection(@RequestParam(value="image") MultipartFile file, HttpServletRequest request) {
        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getId();

        return service.recog(apiId, apiKey, file, PropertyUtil.getUploadPath()+"/feat/getClothFeature", savedId);

    }




}
