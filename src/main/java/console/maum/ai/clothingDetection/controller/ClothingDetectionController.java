package console.maum.ai.clothingDetection.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;




/*
 * 화면 Controller
 */

@Controller
@RequestMapping(value="/cloudApi/clothingDetection")
public class ClothingDetectionController {
    private static final Logger logger = LoggerFactory.getLogger(ClothingDetectionController.class);

    /**
     * ClothingDetectionController Main
     * @param model
     * @return
     *
     */
    @RequestMapping(value = "/clothingDetectionMain")
    public String krClothingDetection(HttpServletRequest request, Model model){
        logger.info("Welcome krClothingDetection");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "clothingDetectionMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "의상 특징 인식");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/clothingDetection/clothingDetectionMain.pg";

        return returnUri;

    }

    /**
     * ClothingDetectionController Main
     * @param model
     * @return
     *
     */
    @RequestMapping(value = "/enclothingDetectionMain")
    public String enclothingDetectionMain(HttpServletRequest request, Model model){
        logger.info("Welcome enclothingDetectionMain");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "clothingDetectionMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Clothing Multi-Attributes Detection");
        httpSession.setAttribute("menuGrpName", "Vision");

        model.addAttribute("lang", "en");
        String returnUri = "/en/clothingDetection/clothingDetectionMain.pg";

        return returnUri;

    }

}
