package console.maum.ai.aiBuilder;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/aiBuilder")
public class AiBuilderController {

    private static final Logger logger = LoggerFactory.getLogger(AiBuilderController.class);


    /**
     * AI Builder Main
     * @param model
     * @return
     */

    @RequestMapping(value = "/krAiBuilderMain")
    public String krAiBuilderMain(HttpServletRequest request, Model model){
        logger.info("Welcome krAiBuilderMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "aiBuilder");
        httpSession.setAttribute("menuName", "Application Services");
        httpSession.setAttribute("subMenuName", "AI Builder");
        httpSession.setAttribute("menuGrpName", "");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/aiBuilder/aiBuilderMain.pg";

        return returnUri;
    }
}
