package console.maum.ai.itf.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/cloudApi/itf")
public class ItfController {

	private static final Logger logger = LoggerFactory.getLogger(ItfController.class);

	/**
	 * itf Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krItfMain")
	public String krItfMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krItfMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "itfMain");
		httpSession.setAttribute("menuName", "언어");
		httpSession.setAttribute("subMenuName", "의도 분류");
		httpSession.setAttribute("menuGrpName", "언어");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/itf/itfMain.pg";

		return returnUri;
	}
	
	/**
	 * itf Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enItfMain")
	public String enItfMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enItfMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "itfMain");
		httpSession.setAttribute("menuName", "Languages");
		httpSession.setAttribute("subMenuName", "ITF");
		httpSession.setAttribute("menuGrpName", "Languages");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/itf/itfMain.pg";
		
		return returnUri;
	}	
	
}
