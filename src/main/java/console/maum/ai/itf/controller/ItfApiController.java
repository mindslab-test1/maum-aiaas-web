package console.maum.ai.itf.controller;

import console.maum.ai.itf.service.ItfApiService;
import console.maum.ai.member.model.MemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
public class ItfApiController {

    @Autowired
    private ItfApiService itfApiService;

    private static final Logger logger = LoggerFactory.getLogger(ItfApiController.class);
    /**
     * 답변 찾기 클릭 이벤트
     * @param utter
     * @param lang
     * @return 응답 본문 String
     * @throws IOException
     */
    @RequestMapping(value="/api/itf", produces = "application/text; charset=utf8")
    @ResponseBody
    public String getApiItf(
            @RequestParam(value = "utter") String utter
            ,@RequestParam(value = "lang") String lang, HttpServletRequest request) throws IOException {

        logger.info("{} .getApiItf()", getClass().getSimpleName());
        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        return itfApiService.getApiItf(apiId, apiKey,utter, lang);
    }
}
