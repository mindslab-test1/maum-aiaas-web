package console.maum.ai.data;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/data")
public class DataController {
    @Value("${datatool.url}")
    private String dataEditorTool;

    private static final Logger logger = LoggerFactory.getLogger(DataController.class);

    /**
     * data Main
     * @param model
     * @return
     */

    @RequestMapping(value = "/krSellDataMain")
    public String krEngeduSttMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krSellDataMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "sellDataMain");
        httpSession.setAttribute("menuName", "Data");
        httpSession.setAttribute("subMenuName", "판매 데이터");
        httpSession.setAttribute("menuGrpName", "");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/data/sellDataMain.pg";

        return returnUri;
    }

    @RequestMapping(value = "/krDataServiceMain")
    public String krEngeduPronMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krDataServiceMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "dataServiceMain");
        httpSession.setAttribute("menuName", "Data");
        httpSession.setAttribute("subMenuName", "데이터 정제 서비스");
        httpSession.setAttribute("menuGrpName", "");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/data/dataServiceMain.pg";

        return returnUri;
    }
    @RequestMapping(value = "/krMaumDataMain")
    public String krMaumDataMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krMaumDataMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "maumDataMain");
        httpSession.setAttribute("menuName", "Data");
        httpSession.setAttribute("subMenuName", "maum Data");
        httpSession.setAttribute("menuGrpName", "");

        model.addAttribute("lang", "ko");
        model.addAttribute("maumDataLink", dataEditorTool);

        String returnUri = "/kr/data/maumDataMain.pg";

        return returnUri;
    }

    /**
     * data Main
     * @param model
     * @return
     */

    @RequestMapping(value = "/enSellDataMain")
    public String enEngeduSttMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enSellDataMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "sellDataMain");
        httpSession.setAttribute("menuName", "Data");
        httpSession.setAttribute("subMenuName", "Data available for purchase");
        httpSession.setAttribute("menuGrpName", "");

        model.addAttribute("lang", "en");
        String returnUri = "/en/data/sellDataMain.pg";

        return returnUri;
    }

    @RequestMapping(value = "/enDataServiceMain")
    public String enEngeduPronMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enDataServiceMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "dataServiceMain");
        httpSession.setAttribute("menuName", "Data");
        httpSession.setAttribute("subMenuName", "Data Refinery Services");
        httpSession.setAttribute("menuGrpName", "");

        model.addAttribute("lang", "en");
        String returnUri = "/en/data/dataServiceMain.pg";

        return returnUri;
    }


}
