package console.maum.ai.xdc.service;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",
		"classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@Service
public class XdcService {

	private static final Logger logger = LoggerFactory.getLogger(XdcService.class);

	@Value("${api.url}")
	private String mUrl_ApiServer;

	// @RequestMapping("/api/mrc_search_news/{search_TXT}")
	// public String home(@AuthenticationPrincipal SecurityMember securityMember) {
	// public String search_news(@PathVariable String search_TXT) {  
	public List getApiSearchNews(String keyword) {
		System.out.println("==== 검색 API 호출====");
		System.out.println("==== 파라미터 ====:: " + keyword);
		List datalist = new ArrayList<Object>();
	    Map<String, Object> map = null;		

		String clientId = "JStsmzcya8858LoqGLfa";
		String clientSecret = "Iji2yIi7L7";
		try {
			String text = URLEncoder.encode(keyword, "UTF-8");
			String apiURL = "https://openapi.naver.com/v1/search/news.xml?query=" + text + "&display=100&sort=sim";  //https://openapi.naver.com/v1/search/news.xml
			System.out.println("apiURL=" + apiURL);
			URL url = new URL(apiURL);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");
			con.setRequestProperty("X-Naver-Client-Id", clientId);
			con.setRequestProperty("X-Naver-Client-Secret", clientSecret);
			// response 수신
			int responseCode = con.getResponseCode();
			System.out.println("responseCode=" + responseCode);

			if (responseCode == 200) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
				String inputLine;
				StringBuffer responsexml = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					responsexml.append(inputLine);
				}
				in.close();
				
				//System.out.println("result == "+ responsexml.toString());
				// System.out.println(responsexml.toString());
				String responsexmlString = ((((responsexml.toString().replace("&quot;", " ")).replace("&lt;/b&gt;",
						" ")).replace("&lt;b&gt;", " ")).replace("<b>", " ")).replace("/<b>", " ");
				System.out.println("responsexmlString == "+ responsexmlString.toString());

				// XML Document 객체 생성
				InputSource is = new InputSource(new StringReader(responsexmlString));
				Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(is);

				// 인터넷 상의 XML 문서는 요렇게 생성하면 편리함.
				// Document document = DocumentBuilderFactory.newInstance().newDocumentBuilder()
				// .parse("http://www.example.com/test.xml");
				// NodeList 가져오기 : row 아래에 있는 모든 col1 을 선택

				// xpath 생성
				XPath xpath = XPathFactory.newInstance().newXPath();

				NodeList title_node = (NodeList) xpath.evaluate("//item/title", document, XPathConstants.NODESET);
				//NodeList description_node = (NodeList) xpath.evaluate("//item/description", document,XPathConstants.NODESET);
				NodeList link_node = (NodeList) xpath.evaluate("//item/link", document,XPathConstants.NODESET);

				JSONObject jsonObject = new JSONObject();
				JSONArray req_array = new JSONArray();
				int cntlist = 0;
				for (int idx = 0; idx < title_node.getLength(); idx++) {
					//if(link_node.item(idx).getTextContent().indexOf("http://news.naver.com")>-1 || link_node.item(idx).getTextContent().indexOf("https://news.naver.com")>-1) {
					if (cntlist<5) {
						if(link_node.item(idx).getTextContent().indexOf("http://news.naver.com")==0 || link_node.item(idx).getTextContent().indexOf("https://news.naver.com")==0) {
							String TextContent = getHTML(link_node.item(idx).getTextContent().replace("news.naver.com/main/", "m.news.naver.com/"));
							if(TextContent != "FAIL") {
								JSONObject data = new JSONObject();
								data.put("desc", TextContent);
								data.put("title", title_node.item(idx).getTextContent());
								req_array.put(data);
								
								map = new HashMap<String, Object>();
								map.put("desc", TextContent);
								map.put("title", title_node.item(idx).getTextContent());
								datalist.add(map);
								
								cntlist++;
							}
						}
					}
//					// data.put("desc", description_node.item(idx).getTextContent());
//					NodeList textNodeList = description_node.item(idx).getChildNodes();
//					StringBuilder textBuilder = new StringBuilder();
//					for (int j = 0; j < textNodeList.getLength(); j++) {
//						Node textNode = textNodeList.item(j);
//						if (textNode.getNodeType() == Node.TEXT_NODE) {
//							textBuilder.append(textNode.getNodeValue());
//						}
//					}
//					data.put("desc", textBuilder.toString());

					
					//req_array.put(data);
					// System.out.println(title_node.item(idx).getTextContent());
					// System.out.println(description_node.item(idx).getTextContent());
				}
//              data = {'title': json_output['items'][i]['title'].replace("</b>", " ").replace("<b>", " "),
//              'desc': parse_news(json_output['items'][i]['link']
//                                 .replace("news.naver.com/main/", "m.news.naver.com/")).replace("\n", " ").replace("\t", " ")}

				jsonObject.put("items", req_array);

				System.out.println("jsonObject : " + jsonObject);
				System.out.println("==== 검색 API 호출 끝====");
				return datalist;

			} else {
				System.out.println("API 호출 에러 발생 : 에러코드=" + responseCode);
				return datalist;
			}
		} catch (Exception e) {
			System.out.println(e);
			return datalist;
		}
	}
	
	public String getHTML(String URL) {
		try {
			URL url = new URL(URL);
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("GET");

			// response 수신
			int responseCode = con.getResponseCode();
			System.out.println("responseCode=" + responseCode);
			
			BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream(), "UTF-8"));
			String inputLine;
			StringBuffer responseStr = new StringBuffer();
			while ((inputLine = in.readLine()) != null) {
				responseStr.append(inputLine);
			}
			in.close();				
			System.out.println("result responseStr == "+ responseStr.toString());
			
			if (responseCode == 200) {
				org.jsoup.nodes.Document doc = Jsoup.parse(responseStr.toString());
				String contentStr = doc.getElementById("dic_area").text();
				System.out.println("result contentStr == "+ contentStr);
				System.out.println("==== 검색 API 호출 끝====");
				return contentStr.toString();

//			}else if(responseCode == 301 || responseCode == 302){
//				org.jsoup.nodes.Document doc = Jsoup.parse(responseStr.toString());
//				String newlinkStr = doc.getElementsByAttribute("href").attr("href");
//				System.out.println("result contentStr == "+ newlinkStr);
//				return getHTML(newlinkStr);
//				
			}else {
				return "FAIL";
			}
		}catch(Exception e) {
			System.out.println(e);
			return "FAIL";
		}
	}
	
	public String xdcApiV20(String apiId, String apiKey, String text) {

		String url = mUrl_ApiServer + "/api/bert.xdc";

		String logMsg = "\n===========================================================================\n";
		logMsg += "XDC API @ PARAMS \n";
		logMsg += String.format(":: %-10s = %s%n", "URL", url);
		logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
		logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
		logMsg += String.format(":: %-10s = %s%n", "text", text);
		logMsg += "===========================================================================";
		logger.info(logMsg);

		Map<String, Object> paramMap = new HashMap<String, Object>();
		paramMap.put("apiId", apiId);
		paramMap.put("apiKey", apiKey);
//		paramMap.put("lang", lang);
		paramMap.put("context", text);
		
		String resultMsg = "";
		try {
			ObjectMapper mapper = new ObjectMapper();
			String json = mapper.writeValueAsString(paramMap);
			resultMsg = sendPost(url, json);
		} catch (Exception ex) {
			return "{ \"status\": \"error\" }";
		}
		return resultMsg;
	}
	
	private String sendPost(String sendUrl, String jsonValue) throws IOException {
		StringBuilder result = new StringBuilder();

		URL url = new URL(sendUrl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Accetp-Charset", "UTF-8");
		conn.setConnectTimeout(10000);
		conn.setReadTimeout(60000);

		OutputStream os = conn.getOutputStream();
		os.write(jsonValue.getBytes("UTF-8"));
		os.flush();
		
		int nResponseCode = conn.getResponseCode();

		BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
		String inputLine = "";
		while((inputLine = br.readLine()) != null) {
			try {
				result.append(URLDecoder.decode(inputLine, "UTF-8"));
			} catch(Exception ex) {
				result.append(inputLine);
			}
		}

		logger.info("Response Result : {}", result.toString().replace('+',' '));

		if(nResponseCode != HttpURLConnection.HTTP_OK) {
			logger.info("API fail.");
			return "{ \"status\": \"error\" }";
		}

		br.close();
		conn.disconnect();
		
		return result.toString();
	}
	
	/*public String xdcApi(String text) {
		System.out.println("==== 검색 API 호출====");
		System.out.println("==== 파라미터 ====text:: " + text);

		try {
			String url = "https://api.maum.ai/api/xdc/";

			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(url);
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addPart("text", new StringBody(text, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("ID", new StringBody("minds-api-sales-demo", ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("key",new StringBody("814826d160684a489dcbeb7445ecd644", ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("cmd", new StringBody("runXDC", ContentType.TEXT_PLAIN.withCharset("UTF-8")));

			HttpEntity entity = builder.build();

			post.setEntity(entity);
			HttpResponse response = client.execute(post);

			int responseCode = response.getStatusLine().getStatusCode();
			// System.out.println("\nSending 'POST' request to URL : " + url);
			// System.out.println("Post parameters : " + post.getEntity());
			System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

			if (responseCode == 200) {
				BufferedReader rd = new BufferedReader(
						new InputStreamReader(response.getEntity().getContent(), "utf-8"), 8);
				StringBuffer result = new StringBuffer();
				String line = "";
				while ((line = rd.readLine()) != null) {
					// result.append(URLDecoder.decode(line, "UTF-8"));
					// result.append(line);
					result.append(new String(URLDecoder.decode(line, "UTF-8")));
				}
				System.out.println("Response Result : " + result.toString());

				JsonParser jsonParser = new JsonParser();
				JsonElement jsonElement = jsonParser.parse(result.toString());
				String datastring = jsonElement.getAsJsonObject().get("data").toString();
				System.out.println("Response Result : " + datastring);

				return result.toString();
			} else {
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

				StringBuffer result = new StringBuffer();
				String line = "";
				while ((line = rd.readLine()) != null) {
					result.append(line);
				}

				System.out.println("Response Result : " + result.toString().replace('+', ' '));
				System.out.println("API 호출 에러 발생 : 에러코드=" + responseCode);
				return "{ \"status\": \"error\" }";

			}
		} catch (Exception e) {
			System.out.println(e);
			return "{ \"status\": \"error\" }";
		}
	}*/
}