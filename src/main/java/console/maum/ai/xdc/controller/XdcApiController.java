package console.maum.ai.xdc.controller;

import console.maum.ai.member.model.MemberVo;
import console.maum.ai.xdc.service.XdcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping(value = "/api/xdc")
public class XdcApiController {

	@Autowired
	private XdcService xdcService;	

	/**
	 * 기사검색 버튼 클릭시
	 * @param keyword
	 * @return
	 */
	@RequestMapping(value = "/search_news")
	@ResponseBody
	public List getApiSearchNews(@RequestParam(value = "keyword") String keyword) {
		
		return xdcService.getApiSearchNews(keyword);
	}
	
	/**
	 * 분석하기
	 * @param text
	 * @return
	 */
	/*@RequestMapping(value = "/xdcApi", produces = "application/json; charset=utf8")
	@ResponseBody
	public String xdcApi(
			@RequestParam(value = "text") String text) 
	{
		String result = xdcService.xdcApi(text);
		return result;
	}*/
	
	/**
	 * 분석하기
	 * @param text
	 * @return
	 */
	@RequestMapping(value = "/xdcApiv20", produces = "application/json; charset=utf8")
	@ResponseBody
	public String xdcApiv20(
			@RequestParam(value = "context") String text, HttpServletRequest request)
//			,@RequestParam(value = "language") String lang) 
	{

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		String apiId = memberVo.getApiId();
		String apiKey = memberVo.getApiKey();

		return xdcService.xdcApiV20(apiId, apiKey,text);

	}
}
