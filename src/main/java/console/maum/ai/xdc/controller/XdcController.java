package console.maum.ai.xdc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/xdc")
public class XdcController {

	private static final Logger logger = LoggerFactory.getLogger(XdcController.class);
	
	/**
	 * xdc Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krXdcMain")
	public String krXdcMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krXdcMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "xdcMain");
		httpSession.setAttribute("menuName", "언어");
		httpSession.setAttribute("subMenuName", "텍스트 분류");	
		httpSession.setAttribute("menuGrpName", "언어");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/xdc/xdcMainNew.pg";
		return returnUri;
	}
	
	/**
	 * xdc Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enXdcMain")
	public String enXdcMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enXdcMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "xdcMain");
		httpSession.setAttribute("menuName", "Languages");
		httpSession.setAttribute("subMenuName", "Text Classifier");	
		httpSession.setAttribute("menuGrpName", "Languages");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/xdc/xdcMainNew.pg";	
		
		return returnUri;
	}	
}
