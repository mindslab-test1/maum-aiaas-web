package console.maum.ai.fr.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.entity.BasicHttpEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.client.HttpClient;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.Buffer;
import java.util.HashMap;
import java.util.Map;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})

@Service
public class FrService {

    private static final Logger logger = LoggerFactory.getLogger(FrService.class);

    @Value("${api.url}")
    private String mUrl_ApiServer;

    /*@Value("${file.upload}")
    String uploadPath;*/

    public void setFace(String apiId, String apiKey, String faceId, String dbId, MultipartFile file, String uploadPath, String saveId){


        String url = mUrl_ApiServer+"/insight/app/setFace";
        String logMsg = "\n===========================================================================\n";
        logMsg += "FaceRecog(setFace) API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "file", file.getOriginalFilename());
        logMsg += String.format(":: %-10s = %s%n", "dbId", dbId);
        logMsg += String.format(":: %-10s = %s%n", "faceId", faceId);
        logMsg += "===========================================================================";
        logger.info(logMsg);


        try{
            HttpClient clinet = HttpClients.createDefault();
            HttpPut put = new HttpPut(url);
            File faceRecogFile = new File(uploadPath);

            if (!faceRecogFile.exists()) {
                faceRecogFile.mkdirs();
            }

          //  faceId = URLEncoder.encode(faceId, "UTF-8");

            faceRecogFile= new File((uploadPath +"/"+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));

            logger.info("Dest File Name = {}", faceRecogFile.getAbsolutePath());

            file.transferTo(faceRecogFile);

            FileBody txtrFileBody = new FileBody(faceRecogFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            logger.debug("{}", txtrFileBody);

            builder.addPart("file", txtrFileBody);
            builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));
            //builder.addPart("faceId", new StringBody(faceId, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("faceId", new StringBody(faceId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("dbId", new StringBody(dbId, ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();
            put.setEntity(entity);
            HttpResponse response = clinet.execute(put);

            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);

            if(responseCode != 200){
                throw new Error("setFace api fail");
            }

        }catch(Exception e){
           logger.error(e.getMessage());
        }
    }

    public void deleteFace(String apiId, String apiKey, String faceId, String dbId){
        logger.info("FaceRecog ========== deleteFace()");

        String url = mUrl_ApiServer+"/insight/app/deleteFace";
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "3FS4PKI7YH9S";


        String logMsg = "\n===========================================================================\n";
        logMsg += "FaceRecog(deleteFace) API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "dbId", dbId);
        logMsg += String.format(":: %-10s = %s%n", "faceId", faceId);
        logMsg += "===========================================================================";
        logger.info(logMsg);

        try {
            URL connectURL = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) connectURL.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Connection", "Keep-Alive");    //연결을 지속하기 위해
            conn.setRequestProperty("Content-Type","multipart/form-data;boundary=" + boundary); //multipart/form으로 보낼 때는 boundary를 넣어줘야 함
            conn.setConnectTimeout(15000);

            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());

            StringBuffer pd = new StringBuffer();
            pd.append(twoHyphens+boundary+lineEnd);
            pd.append("Content-Disposition: form-data; name=\"apiId\"" + lineEnd);

            pd.append(lineEnd);
            pd.append(apiId+lineEnd);

            pd.append(twoHyphens+boundary+lineEnd);
            pd.append("Content-Disposition: form-data; name=\"apiKey\"" + lineEnd);
            pd.append(lineEnd);
            pd.append(apiKey+lineEnd);

            pd.append(twoHyphens+boundary+lineEnd);
            pd.append("Content-Disposition: form-data; name=\"faceId\"" + lineEnd);
            pd.append(lineEnd);
            pd.append(faceId+lineEnd);
            //         dos.writeUTF(faceId);
            //        dos.writeBytes(lineEnd);

            pd.append(twoHyphens+boundary+lineEnd);
            pd.append("Content-Disposition: form-data; name=\"dbId\"" + lineEnd);
            pd.append(lineEnd);
            pd.append(dbId+lineEnd);

            pd.append(twoHyphens+boundary+twoHyphens+lineEnd);

            dos.writeUTF(pd.toString());
            dos.flush();

            int responseCode = conn.getResponseCode();
            logger.info("responseCode = {}" , responseCode);

            if(responseCode != 200){
                throw new Error("delete FAIL...");
            }
        }catch(Exception e){
            logger.error(e.getMessage());
        }
    }

    public ResponseEntity<String> recogFace(String apiId, String apiKey, MultipartFile file, String dbId, String uploadPath,  String saveId){
        String url = mUrl_ApiServer + "/insight/app/recogFace";

        String logMsg = "\n===========================================================================\n";
        logMsg += "FaceRecog(recogFace) API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "file", file.getOriginalFilename());
        logMsg += String.format(":: %-10s = %s%n", "dbId", dbId);
        logMsg += "===========================================================================";
        logger.info(logMsg);

        try {
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File recogFaceFile = new File(uploadPath);

            if (!recogFaceFile.exists()) {
                recogFaceFile.mkdirs();
            }

            recogFaceFile = new File((uploadPath + "/" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));

            logger.info("Dest File Name = {}", recogFaceFile.getAbsolutePath());

            file.transferTo(recogFaceFile);

            FileBody txtrFileBody = new FileBody(recogFaceFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("file", txtrFileBody);
            builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("dbId", new StringBody(dbId, ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();
           // post.setEntity(entity);
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);

            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json;charset=UTF-8");

            if(responseCode == 200){
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));

                StringBuffer result = new StringBuffer();
                String line = "";
                while((line = rd.readLine()) != null){
                    result.append(line);
                }

                logger.debug("Result : {}", result.toString());
                ResponseEntity<String> resultEntity = new ResponseEntity<String>(result.toString(), headers, HttpStatus.OK);

                recogFaceFile.delete();

                return resultEntity;
            }

/*
            StringBuffer outResult = new StringBuffer();
            BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));

            StringBuffer result = new StringBuffer();
            String line = "";
            while ((line = rd.readLine()) != null) {
                result.append(line);
            }
            System.out.println("Response Result : " + result.toString());

            ResponseEntity<String> resultEntity = new ResponseEntity<String>(result.toString(), headers, HttpStatus.OK);
            recogFaceFile.delete();
            return resultEntity;*/
        }catch(Exception e){
            logger.error(e.getMessage());
        }

        return null;
    }
}
