package console.maum.ai.fr.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/cloudApi/fr")
public class FrController {

    private static final Logger logger = LoggerFactory.getLogger(FrController.class);

    /**
     * faceRecog Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/krFaceRecogMain")
    public String krfaceRecogMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krfaceRecogMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "faceRecogMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "Face Recognition");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/fr/faceRecogMain.pg";

        return returnUri;
    }


    /**
     * faceRecog Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/enFaceRecogMain")
    public String enfaceRecogMainMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enfaceRecogMainMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "faceRecogMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Face Recognition");
        httpSession.setAttribute("menuGrpName", "Vision");

        model.addAttribute("lang", "en");
        String returnUri = "/en/fr/faceRecogMain.pg";

        return returnUri;
    }
}
