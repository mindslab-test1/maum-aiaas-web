package console.maum.ai.cnnoise.service;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@Service
public class CnnnoiseService {

    private static final Logger logger = LoggerFactory.getLogger(CnnnoiseService.class);

    @Value("${api.url}")
    private String mUrl_ApiServer;

    public ResponseEntity<byte[]> getApiCnnoise (String apiId, String apiKey, MultipartFile noiseFile, String uploadPath, String saveId) {

        if (noiseFile == null) {
            throw new RuntimeException("You must select the a file for uploading");
        }

        String url = mUrl_ApiServer + "/denoise/stream";

        String logMsg = "\n===========================================================================\n";
        logMsg += "Denoise API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "file", noiseFile.getOriginalFilename());
        logMsg += "===========================================================================";
        logger.info(logMsg);

        try (CloseableHttpClient client = HttpClients.createDefault()){

//            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File noiseVarFile = new File(uploadPath);

            if (!noiseVarFile.exists()) {
                logger.info("create Dir : {}", noiseVarFile.getPath());
                noiseVarFile.mkdirs();
            }
            noiseVarFile = new File((uploadPath +"/"+ noiseFile.getOriginalFilename().substring(noiseFile.getOriginalFilename().lastIndexOf("\\") + 1)));
            noiseFile.transferTo(noiseVarFile);
            FileBody noiseFileBody = new FileBody(noiseVarFile);
            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("file", noiseFileBody);
            builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);

            if (responseCode != 200) {
//                throw new RuntimeException("ErrCode : " + response);
                noiseVarFile.delete();
                logger.error("API fail : {}", response.getEntity());
                return null;
            }

            HttpEntity responseEntity = response.getEntity();

            HttpHeaders headers = new HttpHeaders();
            InputStream in = responseEntity.getContent();
            byte[] audioArray = IOUtils.toByteArray(in);
            File cnnoiseFile = new File(uploadPath+saveId);
            if(!cnnoiseFile.exists()){
                cnnoiseFile.mkdirs();
                logger.info("create out dir : {}", cnnoiseFile.getPath());
            }

            FileOutputStream fos = new FileOutputStream(cnnoiseFile+"/cnnoise.wav");

            fos.write(audioArray);
            fos.flush();
            fos.close();

            headers.setCacheControl(CacheControl.noCache().getHeaderValue());
            ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(audioArray,headers, HttpStatus.OK);
            noiseVarFile.delete();

            return resultEntity;

        } catch (Exception e) {
            logger.error("API exception : {}", e.toString());
        }
        return null;
    }

}
