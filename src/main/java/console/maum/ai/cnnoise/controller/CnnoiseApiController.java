package console.maum.ai.cnnoise.controller;

import console.maum.ai.cnnoise.service.CnnnoiseService;
import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/*
 * API Controller
 */

@RestController
public class CnnoiseApiController {

    @Autowired
    private CnnnoiseService cnnoiseService;


    @PostMapping("/api/denoise/stream")
    @ResponseBody
    public ResponseEntity<byte[]> getApiCnnoise (@RequestParam("noiseFile")MultipartFile noiseFile, HttpServletRequest request){

        HttpSession httpSession= request.getSession(true);

        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getId();

        return cnnoiseService.getApiCnnoise(apiId, apiKey, noiseFile, PropertyUtil.getUploadPath() +"/cnnoise/",savedId);

    }

}
