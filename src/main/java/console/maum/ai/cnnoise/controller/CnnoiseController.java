package console.maum.ai.cnnoise.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/cloudApi/cnnoise")
public class CnnoiseController {

	private static final Logger logger = LoggerFactory.getLogger(CnnoiseController.class);

	/**
	 * cnnoise Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krCnnoiseMain")
	public String krCnnoiseMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krCnnoiseMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "cnnoiseMain");
		httpSession.setAttribute("menuName", "음성");
		httpSession.setAttribute("subMenuName", "음성 정제");
		httpSession.setAttribute("menuGrpName", "음성");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/cnnoise/cnnoiseMain.pg";
		
		return returnUri;
	}
	
	/**
	 * cnnoise Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enCnnoiseMain")
	public String enCnnoiseMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enCnnoiseMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "cnnoiseMain");
		httpSession.setAttribute("menuName", "Voice");
		httpSession.setAttribute("subMenuName", "Denoise");
		httpSession.setAttribute("menuGrpName", "Speech");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/cnnoise/cnnoiseMain.pg";
		
		return returnUri;
	}
}