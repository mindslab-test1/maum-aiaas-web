package console.maum.ai.minutes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
@RequestMapping(value = "/minutes")
public class MinutesController {

    private static final Logger logger = LoggerFactory.getLogger(MinutesController.class);

    /**
     * maumMinutes Main
     * @param model
     * @return
     */

    @RequestMapping(value = "/krMinutesMain")
    public String krMinutesMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krMinutesMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "minutesMain");
        httpSession.setAttribute("menuName", "Application Services");
        httpSession.setAttribute("subMenuName", "maum 회의록");
        httpSession.setAttribute("menuGrpName", "");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/minutes/minutesMain.pg";

        return returnUri;
    }

    @RequestMapping(value = "/enMinutesMain")
    public String enMinutesMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enMinutesMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "minutesMain");
        httpSession.setAttribute("menuName", "Application Services");
        httpSession.setAttribute("subMenuName", "maum Minutes");
        httpSession.setAttribute("menuGrpName", "");

        model.addAttribute("lang", "en");
        String returnUri = "/en/minutes/minutesMain.pg";

        return returnUri;
    }



}
