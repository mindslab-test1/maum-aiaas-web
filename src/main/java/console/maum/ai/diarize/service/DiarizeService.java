package console.maum.ai.diarize.service;


import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@Service
public class DiarizeService {
    @Value("${api.url}")
    private String mUrl_ApiServer;

    public String getApiDiarize(MultipartFile file,String uploadPath){

        if(file==null){
            throw new RuntimeException("You must select the a file for uploading");
        }
        try {
            String url = mUrl_ApiServer + "/api/dap/diarize";

            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            File varfile = new File(uploadPath);

            // 디렉토리가 없으면 생성
            if (varfile.exists() == false) {
                varfile.mkdirs();
            }

            varfile = new File((uploadPath +"/"+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
            file.transferTo(varfile);

            FileBody fileBody = new FileBody(varfile);


            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("reqVoice", fileBody);
            builder.addPart("apiId", new StringBody("minds-api-performance-test", ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody("4SBRGrq9", ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();
            //
            post.setEntity(entity);
            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();
            System.out.println("\nSending 'POST' request to URL : " + url);
            System.out.println("Post parameters : " + post.getEntity());
            System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

            if (responseCode == 200) {
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    // result.append(URLDecoder.decode(line, "UTF-8"));
                    result.append(line);
                }
                System.out.println("Response Result : " + result.toString());

                //STT 의 텍스트 결과값만 추출
                JsonParser jsonParser = new JsonParser();
                JsonElement jsonElement = jsonParser.parse(result.toString());
                String dataString = jsonElement.getAsJsonObject().get("data").toString();
                System.out.println("Response Result 2 : " + dataString);
                // return result.toString().replace('+',' ');


                return result.toString();
            } else {
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

                StringBuffer result = new StringBuffer();
                String line = "";
                while ((line = rd.readLine()) != null) {
                    result.append(line);
                }

                System.out.println("Response Result : " + result.toString().replace('+', ' '));
                System.out.println("API 호출 에러 발생 : 에러코드=" + responseCode);
                return "{ \"status\": \"error\" }";

            }
        } catch (Exception e) {
            System.out.println(e);
            return "{ \"status\": \"error\" }";
        }
    }
}
