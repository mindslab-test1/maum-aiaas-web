package console.maum.ai.diarize.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/cloudApi/diarize")
public class DiarizeController {

	private static final Logger logger = LoggerFactory.getLogger(DiarizeController.class);

	/**
	 * diarize Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krDiarizeMain")
	public String krDiarizeMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krDiarizeMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "diarizeMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "화자 분리");
		httpSession.setAttribute("menuGrpName", "음성");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/diarize/diarizeMain.pg";

		return returnUri;
	}
	
	/**
	 * diarize Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enDiarizeMain")
	public String enDiarizeMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enDiarizeMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "diarizeMain");
		httpSession.setAttribute("menuName", "API Services");
		httpSession.setAttribute("subMenuName", "Diarization");
		httpSession.setAttribute("menuGrpName", "Speech");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/diarize/diarizeMain.pg";
		
		return returnUri;
	}	

}