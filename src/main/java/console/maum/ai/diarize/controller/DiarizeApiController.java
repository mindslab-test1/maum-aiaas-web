package console.maum.ai.diarize.controller;

import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.diarize.service.DiarizeService;
import console.maum.ai.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/*
 * API Controller
 */

@RestController
public class DiarizeApiController {

    @Autowired
    private DiarizeService diarizeService;

    @PostMapping("/api/dap/diarize")
    public String getApiDiarize(@RequestParam("file")MultipartFile file, HttpServletRequest request){

        HttpSession httpSession= request.getSession(true);

        String result = diarizeService.getApiDiarize(file, PropertyUtil.getUploadPath() +"/diarize/");
        System.out.println("diarize result    :::: " + result);
        return result;
    }
}
