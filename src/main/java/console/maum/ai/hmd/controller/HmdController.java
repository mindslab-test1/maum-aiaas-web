package console.maum.ai.hmd.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import console.maum.ai.hmd.service.HmdService;

@Controller
@RequestMapping(value = "/cloudApi/hmd")
public class HmdController {

	private static final Logger logger = LoggerFactory.getLogger(HmdController.class);

	@Autowired
	private HmdService hmdService;	
	
	/**
	 * hmd Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krHmdMain")
	public String krHmdMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krHmdMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "hmdMain");
		httpSession.setAttribute("menuName", "언어");
		httpSession.setAttribute("subMenuName", "패턴 분류");
		httpSession.setAttribute("menuGrpName", "언어");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/hmd/hmdMain.pg";
		
		return returnUri;
	}

	/**
	 * hmd Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enHmdMain")
	public String enHmdMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enHmdMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "hmdMain");
		httpSession.setAttribute("menuName", "Languages");
		httpSession.setAttribute("subMenuName", "HMD");
		httpSession.setAttribute("menuGrpName", "Languages");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/hmd/hmdMain_Tmp.pg";
		
		return returnUri;
	}	
}
