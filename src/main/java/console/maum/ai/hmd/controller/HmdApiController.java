package console.maum.ai.hmd.controller;

import console.maum.ai.hmd.service.HmdService;
import console.maum.ai.member.model.MemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/*
 * API Controller
 */

@RestController
public class HmdApiController {
    private final static Logger logger = LoggerFactory.getLogger(HmdApiController.class);
    @Autowired
    private HmdService hmdService;

    @PostMapping(value= "/api/hmd/hmdApi")
    public String hmdApi(@RequestParam(value="lang") String lang, @RequestParam(value="reqText") String text, HttpServletRequest request) throws UnsupportedEncodingException {
        logger.info("===== HMD ApiController =====");

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String result = hmdService.hmdApi(apiId, apiKey, lang, text);
        String encodedResult = URLEncoder.encode(result, "UTF-8");

        return encodedResult;
    }
}
