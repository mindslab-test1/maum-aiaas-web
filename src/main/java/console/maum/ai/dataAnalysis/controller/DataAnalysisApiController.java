package console.maum.ai.dataAnalysis.controller;

import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.dataAnalysis.service.DataAnalysisService;
import console.maum.ai.subtitleRecog.service.SubtitleRecogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

@RestController
public class DataAnalysisApiController {

    @Autowired
    private DataAnalysisService service;

    @RequestMapping(value = "/api/dataAnalysis")
    @ResponseBody
    public Map<Integer, ResponseEntity> dataAnalysis(@RequestParam MultipartFile var_file, @RequestParam MultipartFile data_file) {

        return service.dataAnalysis(var_file, data_file, PropertyUtil.getUploadPath());
    }
}
