package console.maum.ai.dataAnalysis.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



@Controller
@RequestMapping(value = "/cloudApi/dataAnalysis")

public class DataAnalysisController {

    private static final Logger logger = LoggerFactory.getLogger(DataAnalysisController.class);


    /**
     * Data Analysis Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/krDataAnalysis")

    public String krDataAnalysis(HttpServletRequest request, Model model) {

        logger.info("Welcome krDataAnalysis!");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("menuGrpName", "분석");
        httpSession.setAttribute("selectMenu", "dataAnalysis");
        httpSession.setAttribute("menuName", "분석");
        httpSession.setAttribute("subMenuName", "데이터 상관분석");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/dataAnalysis/dataAnalysisMain.pg";

        return returnUri;
    }



    @RequestMapping(value = "/enDataAnalysis")

    public String enDataAnalysis(HttpServletRequest request, Model model) {

        logger.info("Welcome enDataAnalysis!");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("menuGrpName", "Analysis");
        httpSession.setAttribute("selectMenu", "dataAnalysis");
        httpSession.setAttribute("menuName", "Analysis");
        httpSession.setAttribute("subMenuName", "Correlation analysis");

        model.addAttribute("lang", "en");
        String returnUri = "/en/dataAnalysis/dataAnalysisMain.pg";

        return returnUri;
    }

}
