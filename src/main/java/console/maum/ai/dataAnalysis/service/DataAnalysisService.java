package console.maum.ai.dataAnalysis.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.BodyPart;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import java.io.*;
import java.util.HashMap;
import java.util.Map;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})

@Slf4j
@Service
public class DataAnalysisService {

    @Value("${api.smartX}")
    private String mUrl_smartX;

/*
    @Value("admin.api.id")
    private String mAdmin_ApiId;

    @Value("admin.api.pw")
    private String mAdmin_APiKey;
*/

    public Map<Integer, ResponseEntity> dataAnalysis(MultipartFile var_file, MultipartFile data_file, String uploadPath){
        log.debug("# dataAnalysis >> var_file size = {} : data_file size = {}", var_file.getSize(), data_file.getSize());

        Map<Integer, ResponseEntity> map = new HashMap<>();

        try{
            String url = mUrl_smartX + "/FeatureSelection"; // 임시방편 : 엔진 서버를 직접 찌르는 것으로 설정 -> smartX의 수정 필요
            HttpClient client = HttpClients.createDefault(); // 요청할 client를 만듬
            HttpPost post = new HttpPost(url); // post 방식으로 요청

            File varFile = new File(uploadPath);
            if(!varFile.exists()) {
                varFile.mkdirs();
            }

            varFile = new File((uploadPath + "/" + var_file.getOriginalFilename().substring(var_file.getOriginalFilename().lastIndexOf("\\") + 1)));
            var_file.transferTo(varFile);

            File dataFile = new File((uploadPath + "/" + data_file.getOriginalFilename().substring(data_file.getOriginalFilename().lastIndexOf("\\") + 1)));
            data_file.transferTo(dataFile);

            log.debug("Dest File Name = {}, {}", varFile.getAbsolutePath(), dataFile.getAbsolutePath());
            log.info(" @ FaceTracking uploadPath : {}", uploadPath);

            FileBody varFileBody = new FileBody(varFile);
            FileBody dataFileBody = new FileBody(dataFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);

            builder.addPart("var_file", varFileBody);
            builder.addPart("data_file", dataFileBody);
            //builder.addPart("apiId", new StringBody(mAdmin_ApiId, ContentType.MULTIPART_FORM_DATA));
            //builder.addPart("apiKey", new StringBody(mAdmin_APiKey, ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();

            if (responseCode != 200) {
                throw new RuntimeException("# FeatureSelection >>  ErrCode : " + response);
            }

            InputStream in = new BufferedInputStream(response.getEntity().getContent());

            ByteArrayDataSource dataSource = new ByteArrayDataSource(in, "multipart/form-data");
            MimeMultipart multipart = new MimeMultipart(dataSource);

            int count = multipart.getCount();

            for(int i=0 ; i<count ; i++) {

                BodyPart bodyPart = multipart.getBodyPart(i);
                HttpHeaders headers = new HttpHeaders();
                headers.setCacheControl(CacheControl.noCache().getHeaderValue());

                if(bodyPart.isMimeType("text/plain")){
                    BufferedReader rd = new BufferedReader(new InputStreamReader(bodyPart.getInputStream(), "UTF-8"));
                    StringBuffer result = new StringBuffer();
                    String line = "";
                    while((line=rd.readLine()) != null) {
                        result.append(line + "\n");
                    }

                    String strResult = result.toString();
                    ResponseEntity<String> resultEntity = new ResponseEntity<String>(strResult, headers, HttpStatus.OK);
                    map.put(i, resultEntity);

                    log.info("# FeatureSelection >> result = {}", strResult);
                } else {
                    log.warn("# FeatureSelection >> Unsupported ContentType : {}", bodyPart.getContentType());
                }
            }

            varFile.delete();
            dataFile.delete();
        } catch (Exception e) {
            log.error("# FeatureSelection >> Exception : {}", e.getMessage());
        }

        return map;
    }
}
