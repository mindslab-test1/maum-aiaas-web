package console.maum.ai.engedu.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",
		"classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@Controller
@RequestMapping(value = "/cloudApi/engedu")
public class EngeduController {

	@Value("${api.host.engedu}")
	String mHost_Engedu;

	private static final Logger logger = LoggerFactory.getLogger(EngeduController.class);

	/**
	 * engeduStt Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krEngeduSttMain")
	public String krEngeduSttMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krEngeduSttMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "engeduSttMain");
		httpSession.setAttribute("menuName", "영어교육");
		httpSession.setAttribute("subMenuName", "영어교육 음성인식(STT)");
		httpSession.setAttribute("menuGrpName", "영어교육");
		
		model.addAttribute("lang", "ko");
		model.addAttribute("engedu_host", mHost_Engedu);
		model.addAttribute("engedu_surfix", (mHost_Engedu.contains("dev") ? ".dev" : ""));
		String returnUri = "/kr/engedu/engeduSttMain.pg";

		return returnUri;
	}

	@RequestMapping(value = "/krEngeduPronMain")
	public String krEngeduPronMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krEngeduPronMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "engeduPronMain");
		httpSession.setAttribute("menuName", "영어교육");
		httpSession.setAttribute("subMenuName", "영어교육 발음 평가");
		httpSession.setAttribute("menuGrpName", "영어교육");

		model.addAttribute("lang", "ko");
		model.addAttribute("engedu_host", mHost_Engedu);
		model.addAttribute("engedu_surfix", (mHost_Engedu.contains("dev") ? ".dev" : ""));
		String returnUri = "/kr/engedu/engeduPronMain.pg";

		return returnUri;
	}

	@RequestMapping(value = "/krEngeduPhonicsMain")
	public String krEngeduPhonicsMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krEngeduPhonicsMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "engeduPhonicsMain");
		httpSession.setAttribute("menuName", "영어교육");
		httpSession.setAttribute("subMenuName", "영어교육 파닉스");
		httpSession.setAttribute("menuGrpName", "영어교육");

		model.addAttribute("lang", "ko");
		model.addAttribute("engedu_host", mHost_Engedu);
		model.addAttribute("engedu_surfix", (mHost_Engedu.contains("dev") ? ".dev" : ""));
		String returnUri = "/kr/engedu/engeduPhonicsMain.pg";

		return returnUri;
	}

	@RequestMapping(value = "/enEngeduSttMain")
	public String enEngeduSttMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enEngeduSttMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "engeduSttMain");
		httpSession.setAttribute("menuName", "English Education");
		httpSession.setAttribute("subMenuName", "STT for English Education");
		httpSession.setAttribute("menuGrpName", "English Education");

		model.addAttribute("lang", "en");
		model.addAttribute("engedu_host", mHost_Engedu);
		model.addAttribute("engedu_surfix", (mHost_Engedu.contains("dev") ? ".dev" : ""));
		String returnUri = "/en/engedu/engeduSttMain.pg";

		return returnUri;
	}

	@RequestMapping(value = "/enEngeduPronMain")
	public String enEngeduPronMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enEngeduPronMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "engeduPronMain");
		httpSession.setAttribute("menuName", "English Education");
		httpSession.setAttribute("subMenuName", "Evaluation of English Pronunciation\n");
		httpSession.setAttribute("menuGrpName", "English Education");

		model.addAttribute("lang", "en");
		model.addAttribute("engedu_host", mHost_Engedu);
		model.addAttribute("engedu_surfix", (mHost_Engedu.contains("dev") ? ".dev" : ""));
		String returnUri = "/en/engedu/engeduPronMain.pg";

		return returnUri;
	}

	@RequestMapping(value = "/enEngeduPhonicsMain")
	public String enEngeduPhonicsMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enEngeduPhonicsMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "engeduPhonicsMain");
		httpSession.setAttribute("menuName", "English Education");
		httpSession.setAttribute("subMenuName", "Phonics Assessment");
		httpSession.setAttribute("menuGrpName", "English Education");

		model.addAttribute("lang", "en");
		model.addAttribute("engedu_host", mHost_Engedu);
		model.addAttribute("engedu_surfix", (mHost_Engedu.contains("dev") ? ".dev" : ""));
		String returnUri = "/en/engedu/engeduPhonicsMain.pg";

		return returnUri;
	}



}