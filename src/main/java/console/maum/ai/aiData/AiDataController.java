package console.maum.ai.aiData;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



@Controller
@RequestMapping(value = "/aiData")
public class AiDataController {


    private static final Logger logger = LoggerFactory.getLogger(AiDataController.class);

    /**
     * AI Data labeling Main
     * @param model
     * @return
     */

    @RequestMapping(value = "/krAiDataLabeling")
    public String krAiDataLabeling(HttpServletRequest request, Model model) {


        logger.info("Welcome krAiDataLabeling!");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("menuName", "AiData");
        model.addAttribute("lang", "ko");
        String returnUri = "noLayout/kr/aiData/aiDataLabeling.pg";

        return returnUri;
    }
    /**
     * AI Data labeling Main
     * @param model
     * @return
     */

    @RequestMapping(value = "/enAiDataLabeling")
    public String enAiDataLabeling(HttpServletRequest request, Model model) {


        logger.info("Welcome enAiDataLabeling!");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("menuName", "AiData");
        model.addAttribute("lang", "en");
        String returnUri = "noLayout/en/aiData/aiDataLabeling.pg";

        return returnUri;
    }

}
