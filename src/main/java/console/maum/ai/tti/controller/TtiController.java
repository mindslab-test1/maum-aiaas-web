package console.maum.ai.tti.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/cloudApi/tti")
public class TtiController {

	private static final Logger logger = LoggerFactory.getLogger(TtiController.class);

	/**
	 * tti Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krTtiMain")
	public String krTtiMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krTtiMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "ttiMain");
		httpSession.setAttribute("menuName", "시각");
		httpSession.setAttribute("subMenuName", "AI 스타일링");
		httpSession.setAttribute("menuGrpName", "시각");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/tti/ttiMain.pg";	
		
		return returnUri;
	}

	/**
	 * tti Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enTtiMain")
	public String enTtiMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enTtiMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "ttiMain");
		httpSession.setAttribute("menuName", "Vision");
		httpSession.setAttribute("subMenuName", "AI Styling");
		httpSession.setAttribute("menuGrpName", "Vision");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/tti/ttiMain.pg";
		
		return returnUri;
	}
}