package console.maum.ai.avr.service;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",
		"classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@Service
public class AvrService {

	private static final Logger logger = LoggerFactory.getLogger(AvrService.class);

	@Value("${api.url}")
	private String apiUrl;


	public  ResponseEntity<byte[]> runAvr(String apiId, String apiKey, String option, MultipartFile file, String uploadPath){

		String url = apiUrl + "/api/avr";
		String avrOptionStr = "AVR(" + option + ")";

		String logMsg = "\n===========================================================================\n";
		logMsg += avrOptionStr + " API @ PARAMS \n";
		logMsg += String.format(":: %-10s = %s%n", "URL", url);
		logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
		logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
		logMsg += String.format(":: %-10s = %s%n", "option", option);
		logMsg += String.format(":: %-10s = %s%n", "file", file.getOriginalFilename());
		logMsg += "===========================================================================";
		logger.info(logMsg);

		try(CloseableHttpClient client = HttpClients.createDefault()){

			HttpPost post = new HttpPost(url);
			File varFile = new File(uploadPath);

			if(!varFile.exists()) {
				varFile.mkdirs();
			}

			varFile = new File((uploadPath + "/" + apiId +"_"+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
			logger.info(" @ {} input file uploadPath : {}", avrOptionStr, varFile.getAbsolutePath());
			file.transferTo(varFile);
			FileBody fileBody = new FileBody(varFile);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("apiKey",new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("option",new StringBody(option, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("file", fileBody);
			HttpEntity entity = builder.build();
			post.setEntity(entity);

			HttpResponse response = client.execute(post);
			int responseCode = response.getStatusLine().getStatusCode();
			HttpEntity responseEntity = response.getEntity();
			logger.info("{} responseCode = {}" , avrOptionStr, responseCode);

			varFile.delete();

			if (responseCode != 200) {
				String responseString = "";
				if(responseEntity!=null) {
					responseString = EntityUtils.toString(responseEntity, "UTF-8");
				}
				logger.error("API {} fail : \n{}", avrOptionStr, responseString);
				return null;
//				throw new RuntimeException(" @ FaceTracking ErrCode : " + response);
			}

			HttpHeaders headers = new HttpHeaders();
			InputStream in = responseEntity.getContent();
			byte[] imageArray = IOUtils.toByteArray(in);

			return new ResponseEntity<>(imageArray, headers, HttpStatus.OK);

		} catch (Exception e) {
			logger.error("API {} exception : {}", avrOptionStr, e.toString());
			e.printStackTrace();
		}

		return null;
	}


	public ResponseEntity<byte[]> AvrMultipart(String apiId, String apiKey, MultipartFile file, String uploadPath){

		String url = apiUrl + "/smartXLoad/PlateRecog";

		String logMsg = "\n===========================================================================\n";
		logMsg += "AVR(plate) API @ PARAMS \n";
		logMsg += String.format(":: %-10s = %s%n", "URL", url);
		logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
		logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
		logMsg += String.format(":: %-10s = %s%n", "car_img", file.getOriginalFilename());
		logMsg += "===========================================================================";
		logger.info(logMsg);

		try (CloseableHttpClient client = HttpClients.createDefault()){

			HttpPost post = new HttpPost(url);
			File varFile = new File(uploadPath);

			if (!varFile.exists()) {
				varFile.mkdirs();
			}

			varFile = new File((uploadPath + "/" + apiId +"_"+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
			logger.info(" @ AVR(plate) input file uploadPath : {}", varFile.getAbsolutePath());
			file.transferTo(varFile);
			FileBody fileBody = new FileBody(varFile);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("apiKey",new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
			builder.addPart("car_img", fileBody);
			HttpEntity entity = builder.build();
			post.setEntity(entity);

			HttpResponse response = client.execute(post);
			int responseCode = response.getStatusLine().getStatusCode();
			HttpEntity responseEntity = response.getEntity();
			logger.info("AVR(plate) responseCode = {}" , responseCode);

			varFile.delete();

			if (responseCode != 200) {
				String responseString = "";
				if(responseEntity!=null) {
					responseString = EntityUtils.toString(responseEntity, "UTF-8");
				}
				logger.error("API AVR(plate) fail : \n{}", responseString);
				return null;
//				throw new RuntimeException("ErrCode : " + response);
			}

			HttpHeaders headers = new HttpHeaders();
			InputStream in = responseEntity.getContent();
			byte[] imageArray = IOUtils.toByteArray(in);

			return new ResponseEntity<>(imageArray, headers, HttpStatus.OK);

		}catch(Exception e){
			logger.error(" @ AVR multipart Exception : {}", e.getCause().toString());
		}

		return null;
	}

}

