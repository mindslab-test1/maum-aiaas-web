package console.maum.ai.avr.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
@RequestMapping(value = "/cloudApi/avr")
public class AvrController {

	private static final Logger logger = LoggerFactory.getLogger(AvrController.class);

	/**
	 * avr Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krAvrMain")
	public String krAvrMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krAvrMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "avrMain");
		httpSession.setAttribute("menuName", "시각");
		httpSession.setAttribute("subMenuName", "차량 유리창 마스킹");
		httpSession.setAttribute("menuGrpName", "시각");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/avr/avrMain.pg";
		
		return returnUri;
	}	
	
	/**
	 * avr Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enAvrMain")
	public String enAvrMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enAvrMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "avrMain");
		httpSession.setAttribute("menuName", "Vision");
		httpSession.setAttribute("subMenuName", "Windshield Detection");
		httpSession.setAttribute("menuGrpName", "Vision");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/avr/avrMain.pg";

		return returnUri;
	}

	/**
	 * 얼굴 마스킹
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krFaceDetectionMain")
	public String krFaceDetectionMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krFaceDetectionMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "faceDetactionMain");
		httpSession.setAttribute("menuName", "시각");
		httpSession.setAttribute("subMenuName", "얼굴 마스킹");
		httpSession.setAttribute("menuGrpName", "시각");

		model.addAttribute("lang", "ko");
		String returnUri = "/kr/avr/faceDetactionMain.pg";

		return returnUri;
	}
	/**
	 * 얼굴 마스킹
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enFaceDetectionMain")
	public String enFaceDetectionMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enFaceDetectionMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "faceDetactionMain");
		httpSession.setAttribute("menuName", "Vision");
		httpSession.setAttribute("subMenuName", "Face Detection");
		httpSession.setAttribute("menuGrpName", "Vision");

		model.addAttribute("lang", "en");
		String returnUri = "/en/avr/faceDetactionMain.pg";

		return returnUri;
	}

	/**
	 * 차량 번호판 인식
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krPlateRecogMain")
	public String krPlateRecogMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krFaceDetectionMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "plateRecogMain");
		httpSession.setAttribute("menuName", "시각");
		httpSession.setAttribute("subMenuName", "차량 번호판 인식");
		httpSession.setAttribute("menuGrpName", "시각");

		model.addAttribute("lang", "ko");
		String returnUri = "/kr/avr/plateRecogMain.pg";

		return returnUri;
	}
	/**
	 * 차량 번호판 인식
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enPlateRecogMain")
	public String enPlateRecogMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enPlateRecogMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "plateRecogMain");
		httpSession.setAttribute("menuName", "Vision");
		httpSession.setAttribute("subMenuName", "License Plate Recognition");
		httpSession.setAttribute("menuGrpName", "Vision");

		model.addAttribute("lang", "en");
		String returnUri = "/en/avr/plateRecogMain.pg";

		return returnUri;
	}

}