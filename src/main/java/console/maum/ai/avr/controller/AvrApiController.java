package console.maum.ai.avr.controller;

import console.maum.ai.avr.service.AvrService;
import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
@RequestMapping(value = "/api/avr")
public class AvrApiController {

	@Autowired
	private AvrService avrService;

	@RequestMapping(value = "/runAvr")
	@ResponseBody
	public ResponseEntity<byte[]> runAvr(String option, @RequestParam MultipartFile file, HttpServletRequest request) {

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		String apiId = memberVo.getApiId();
		String apiKey = memberVo.getApiKey();
		return avrService.runAvr(apiId, apiKey, option, file, PropertyUtil.getUploadPath() + "/avr");
	}

	@RequestMapping(value="/multiAvr")
	@ResponseBody
	public ResponseEntity<byte[]> multiAvr(@RequestParam MultipartFile file, HttpServletRequest request){

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		String apiId = memberVo.getApiId();
		String apiKey = memberVo.getApiKey();

		return avrService.AvrMultipart(apiId, apiKey, file, PropertyUtil.getUploadPath() + "/avr");
	}


}