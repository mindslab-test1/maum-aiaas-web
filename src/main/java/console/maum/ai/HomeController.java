package console.maum.ai;

import console.maum.ai.academy.service.AcademyService;
import console.maum.ai.login.controller.LoginController;
import console.maum.ai.login.service.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {

	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);


	@Autowired
	private LoginService loginService;

	@Autowired
	private LoginController loginController;

	@Autowired
	private AcademyService academyService;

	@Value("${datatool.url}")
	private String dataEditorTool;

	@Value("${mvp_maker.url}")
	private String mUrl_MVPMaker;

	@Value("${hq.URL}")
	private String sso_url;

	@Value("${hq.client_id}")
	private String client_id;

	@Value("${hq.redirect_uri}")
	private String redirect_uri;

	/**
	 * Simply selects the home view to render by returning its name.
	 */
	/*@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, HttpServletRequest request,  HttpServletResponse response, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		HttpSession httpSession = request.getSession(true);
		String strLang = (String) httpSession.getAttribute("lang");
		//국문/영문 분기 처리
		String paramLang = request.getParameter("lang");
				
		if(!"".equals(paramLang) && paramLang != null) {
			response.addCookie(new Cookie("lang",paramLang));
		}else {
			
			Cookie[] getCookie = request.getCookies();
			if(getCookie != null){
				for(int i=0; i<getCookie.length; i++){	
					Cookie c = getCookie[i];		
					String name = c.getName(); // 쿠키 이름 가져오기		
					String value = c.getValue(); // 쿠키 값 가져오기
					
					if("lang".equals(name) && !"".equals(value) ) {
						paramLang = c.getValue();
					}				
				}
			}	
		}	
		logger.info("loginForm paramLang : " + paramLang);
		logger.info("loginForm strLang : " + strLang);
		
		String lang = request.getLocale().toString();
		if(paramLang != null) {
			lang = strLang;
		}		
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("lang", strLang);
		model.addAttribute("serverTime", formattedDate );
		
		return "home";
	}*/
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(@RequestParam(value="error", required=false) String error, HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("loginForm()");

		String paramLang = loginService.setLang(request, response);

		logger.info(paramLang);

		//특정페이지 이동 쿠키 발행
		String loginmenu = request.getParameter("loginmenu");
		if(!"".equals(loginmenu)) {
			response.addCookie(new Cookie("loginmenu",loginmenu));
		}else {
			response.addCookie(new Cookie("loginmenu",null));
		}

		model.addAttribute("google_url", loginController.loginBtnUrl);	// 영문 페이지 구글 로그인을 위해 남겨 둠
		model.addAttribute("mvp_maker_url", mUrl_MVPMaker);
		model.addAttribute("error", error);
		model.addAttribute("lang", paramLang);
		model.addAttribute("errormsg", request.getParameter("errormsg"));
		model.addAttribute("maumDataLink", dataEditorTool);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);

		String redirectUrl = "noLayout/"+ paramLang +"/login/loginForm.pg";

		return redirectUrl;
	}

	@RequestMapping(value = "/inquiry", method = RequestMethod.GET)
	public String inquiry(@RequestParam(value="error", required=false) String error, HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("inquiry()");

		String paramLang = loginService.setLang(request, response);

		//특정페이지 이동 쿠키 발행
		String loginmenu = request.getParameter("loginmenu");
		if(!"".equals(loginmenu)) {
			response.addCookie(new Cookie("loginmenu",loginmenu));
		}else {
			response.addCookie(new Cookie("loginmenu",null));
		}

		model.addAttribute("google_url", loginController.loginBtnUrl);	// 영문 페이지 구글 로그인을 위해 남겨 둠
		model.addAttribute("mvp_maker_url", mUrl_MVPMaker);
		model.addAttribute("error", error);
		model.addAttribute("lang", paramLang);
		model.addAttribute("errormsg", request.getParameter("errormsg"));
		model.addAttribute("maumDataLink", dataEditorTool);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);

		String redirectUrl = "noLayout/"+ paramLang +"/common/maum_inquiry.pg";

		return redirectUrl;
	}

	/**
	 * pricing_page
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/home/pricingPage", method = RequestMethod.GET)
	public String pricingPage(@RequestParam(value="error", required=false) String error, HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("homeController/pricingPage()");

		//국문/영문 분기 처리
		String paramLang = loginService.setLang(request, response);

		//특정페이지 이동 쿠키 발행
		String loginmenu = request.getParameter("loginmenu");
		if(!"".equals(loginmenu)) {
			response.addCookie(new Cookie("loginmenu",loginmenu));
		}else {
			response.addCookie(new Cookie("loginmenu",null));
		}

		model.addAttribute("mvp_maker_url", mUrl_MVPMaker);
		model.addAttribute("error", error);
		model.addAttribute("lang", paramLang);
		model.addAttribute("errormsg", request.getParameter("errormsg"));
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);

		String redirectUrl = "noLayout/"+ paramLang +"/support/outsidePricing.pg";

		return redirectUrl;
	}


	/**
	 * academy_page
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/home/academyForm")
	public String academyForm(HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("homeController/academyForm()");

		//국문/영문 분기 처리
//        String paramLang = loginService.setLang(request, response);

		String returnUri = "";
		model.addAttribute("google_url", loginController.loginBtnUrl);

        returnUri = "noLayout/kr/login/academyForm.pg";

		return returnUri;
	}
	/**
	 * maumEdge_page
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/home/maumEdge")
	public String maumEdge(HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("homeController/maumEdge()");

		//국문/영문 분기 처리
//        String paramLang = loginService.setLang(request, response);
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		String returnUri = "noLayout/kr/login/maumEdge.pg";

		return returnUri;
	}

	/**
	 * Employees Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/home/krEmployeesMain")
	public String krEmployeesMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krEmployeesMain!");

		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("menuName", "Employees");
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("lang", "ko");
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		String returnUri = "noLayout/kr/employees/employeesMain.pg";

		return returnUri;
	}


	/**
	 * Term Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/home/krTermsMain")
	public String krTermMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krTermMain!");

		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("menuName", "Term");
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		model.addAttribute("lang", "ko");
		String returnUri = "noLayout/kr/login/termsMain.pg";

		return returnUri;
	}

	@RequestMapping(value = "/home/enTermsMain")
	public String enTermsMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enTermsMain!");

		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("menuName", "Term");
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		model.addAttribute("lang", "en");
		String returnUri = "noLayout/en/login/termsMain.pg";

		return returnUri;
	}
	/**
	 * ecoMinds Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/home/krEcomindsMain")
	public String krEcomindsMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krEcomindsMain!");

		HttpSession httpSession = request.getSession(true);
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("lang", "ko");
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		String returnUri = "noLayout/kr/login/ecomindsMain.pg";

		return returnUri;
	}
	/**
	 * ecoMinds Main
	 * @param model
	 * @return
	 */

	@RequestMapping(value = "/home/krAcademyMain", method = RequestMethod.GET)
	public String krAcademyMain(HttpServletRequest request, Model model) throws Exception {
		logger.info("Welcome krAcademyMain!");

		HttpSession httpSession = request.getSession(true);
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("module_name_list", academyService.getAcademyModuleList()); // 모듈명 리스트
		model.addAttribute("academy_map", academyService.getAcademyList()); // 맵 형식의 리스트
		model.addAttribute("academy_list", academyService.getAcademyAllList()); // 전체 리스트
		//model.addAttribute("module_list", academyService.getAcademyModuleList(module_name));

		model.addAttribute("lang", "ko");
		String returnUri = "noLayout/kr/login/academyMain.pg";

		return returnUri;
	}

	@RequestMapping(value = "/home/seminarPage", method = RequestMethod.GET)
	public String seminarPage(HttpServletRequest request, Model model) throws Exception {
		logger.info("Welcome seminarPage!");

		HttpSession httpSession = request.getSession(true);
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("module_name_list", academyService.getAcademyModuleList()); // 모듈명 리스트
		model.addAttribute("academy_map", academyService.getAcademyList()); // 맵 형식의 리스트
		model.addAttribute("academy_list", academyService.getAcademyAllList()); // 전체 리스트
		//model.addAttribute("module_list", academyService.getAcademyModuleList(module_name));

		model.addAttribute("lang", "ko");
		String returnUri = "noLayout/kr/login/seminarPage.pg";

		return returnUri;
	}

	//	결제정보 등록 페이지
	@RequestMapping(value = "/home/registration", method = RequestMethod.GET)
	public String registration(@RequestParam(value="error", required=false) String error, HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("registration Main()");
		String paramLang = loginService.setLang(request, response);
		logger.info(paramLang);

		//특정페이지 이동 쿠키 발행
		String loginmenu = request.getParameter("loginmenu");
		if(!"".equals(loginmenu)) {
			response.addCookie(new Cookie("loginmenu",loginmenu));
		}else {
			response.addCookie(new Cookie("loginmenu",null));
		}

		model.addAttribute("error", error);
		model.addAttribute("lang", paramLang);
		model.addAttribute("errormsg", request.getParameter("errormsg"));

		String redirectUrl = "noLayout/"+ paramLang +"/login/registration.pg";

		return redirectUrl;
	}

	@RequestMapping(value = "/error/noti")
	public String error (HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("noti Main()");

		String paramLang = loginService.setLang(request, response);
		model.addAttribute("lang", paramLang);

		String redirectUrl = "noLayout/"+ paramLang +"/error/noti.pg";
		return redirectUrl;
	}

	/**
	 * AVA 공모전 상세 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/home/avaDetail")
	public String avaDetail(HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("homeController/avaDetail()");

		//국문/영문 분기 처리
//        String paramLang = loginService.setLang(request, response);
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		String returnUri = "noLayout/kr/login/ava_detail.pg";

		return returnUri;
	}

	/**
	 * maum Book 이벤트 한국어 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/event/maumBook")
	public String maumBook(HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("homeController/maumBook()");

		//국문/영문 분기 처리
        String paramLang = loginService.setLang(request, response);
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		String returnUri = "noLayout/kr/event/maumBook.pg";

		return returnUri;
	}

	/**
	 * maum Book 이벤트 영어 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/event/maumBookEng")
	public String maumBookEng(HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("homeController/maumBook_eng()");

		//국문/영문 분기 처리
		String paramLang = loginService.setLang(request, response);
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		String returnUri = "noLayout/en/event/maumBook.pg";

		return returnUri;
	}

	/**
	 * maum Book 이벤트 일본어 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/event/maumBookJp")
	public String maumBookJapan(HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("homeController/maumBook_jp()");

		//국문/영문 분기 처리
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);

		String returnUri = "noLayout/jp/event/maumBook.pg";
		return returnUri;
	}

	/**
	 * Kiosk 이벤트 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/event/kiosk")
	public String kioskEventMain(HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("homeController/kioskEventMain()");

		//국문/영문 분기 처리
//        String paramLang = loginService.setLang(request, response);
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		String returnUri = "noLayout/kr/event/kioskEvent.pg";

		return returnUri;
	}

	/**
	 * Edge 이벤트 페이지
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/event/edgeMain")
	public String edgeMain(HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("homeController/edgeMain()");

		//국문/영문 분기 처리
//        String paramLang = loginService.setLang(request, response);
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		String returnUri = "noLayout/kr/event/edgeMain.pg";

		return returnUri;
	}

	/**
	 * hospitalReceipt
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/home/hospitalReceipt", method = RequestMethod.GET)
	public String hospitalReceiptDemoPage(@RequestParam(value="error", required=false) String error, HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("homeController/hospitalReceiptDemoPage()");

		//국문/영문 분기 처리
		String paramLang = loginService.setLang(request, response);

		//특정페이지 이동 쿠키 발행
		String loginmenu = request.getParameter("loginmenu");
		if(!"".equals(loginmenu)) {
			response.addCookie(new Cookie("loginmenu",loginmenu));
		}else {
			response.addCookie(new Cookie("loginmenu",null));
		}

		model.addAttribute("mvp_maker_url", mUrl_MVPMaker);
		model.addAttribute("error", error);
		model.addAttribute("lang", paramLang);
		model.addAttribute("errormsg", request.getParameter("errormsg"));
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);

		String redirectUrl = "noLayout/"+ paramLang +"/support/hospitalReceiptDemo.pg";

		return redirectUrl;
	}

	@RequestMapping(value="/home/estimate/krAhb2", method = RequestMethod.GET)
	public String krAhb2(@RequestParam(value="error", required=false) String error, HttpServletRequest request, HttpServletResponse response, Model model) {

		logger.info("homeController/krAhb2()");
		//국문/영문 분기 처리
		String paramLang = loginService.setLang(request, response);

		//특정페이지 이동 쿠키 발행
		String loginmenu = request.getParameter("loginmenu");
		if(!"".equals(loginmenu)) {
			response.addCookie(new Cookie("loginmenu",loginmenu));
		}else {
			response.addCookie(new Cookie("loginmenu",null));
		}

		model.addAttribute("mvp_maker_url", mUrl_MVPMaker);
		model.addAttribute("error", error);
		model.addAttribute("lang", paramLang);
		model.addAttribute("errormsg", request.getParameter("errormsg"));
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);

		String redirectUrl = "noLayout/"+ paramLang+"/builder/ahbMain.pg";

		return redirectUrl;
	}

	/**
	 * cloudApiServiceMain Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/home/krcloudApiServiceMain")
	public String krcloudApiServiceMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krcloudApiServiceMain!");

		HttpSession httpSession = request.getSession(true);
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("lang", "ko");
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		String returnUri = "noLayout/kr/login/cloudApiServiceMain.pg";

		return returnUri;
	}

	/**
	 * cloudApiServiceMain Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/home/orcerrorsolution")
	public String orcerrorsolution(HttpServletRequest request, Model model) {
		logger.info("Welcome krcloudApiServiceMain!");

		HttpSession httpSession = request.getSession(true);
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("lang", "ko");
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		String returnUri = "noLayout/kr/orcerrorsolution/orcerrorsolution.pg";

		return returnUri;
	}

	/**
	 * cloudApiServiceMain Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/home/orcttsMain")
	public String orcttsMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krcloudApiServiceMain!");

		HttpSession httpSession = request.getSession(true);
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("lang", "ko");
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		String returnUri = "noLayout/kr/orctts/orcttsMain.pg";

		return returnUri;
	}

	/**
	 * cloudApiServiceMain Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/home/orcsttMain")
	public String orcsttMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krcloudApiServiceMain!");

		HttpSession httpSession = request.getSession(true);
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("lang", "ko");
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		String returnUri = "noLayout/kr/orcstt/orcsttMain.pg";

		return returnUri;
	}

	/**
	 * cloudApiServiceMain Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/home/orcttlMain")
	public String orcttlMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krcloudApiServiceMain!");

		HttpSession httpSession = request.getSession(true);
		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("lang", "ko");
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);
		String returnUri = "noLayout/kr/orcttl/orcttlMain.pg";

		return returnUri;
	}



}

