package console.maum.ai.voicebot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/voicebot")
public class VoiceBotController {

	private static final Logger logger = LoggerFactory.getLogger(VoiceBotController.class);

	@RequestMapping(value = "/krVoiceBotMain")
	public String krVoiceBotMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krVoiceBotMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "Voice 봇");		
		httpSession.setAttribute("subMenuName", "Voice 봇");
		httpSession.setAttribute("menuName", "Application Services");
		httpSession.setAttribute("menuGrpName", "");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/voicebot/voiceBotMain.pg";
		
		return returnUri;
	}
	
	@RequestMapping(value = "/enVoiceBotMain")
	public String enVoiceBotMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enVoiceBotMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "Voice Bot");		
		httpSession.setAttribute("subMenuName", "Voice Bot");
		httpSession.setAttribute("menuName", "Application Services");
		httpSession.setAttribute("menuGrpName", "");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/voicebot/voiceBotMain.pg";
		
		return returnUri;
	}	
	
}
