package console.maum.ai.holistic3d.service;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})

@Service
public class Holistic3dService {

    private static final Logger logger = LoggerFactory.getLogger(Holistic3dService.class);

    @Value("${api.url}")
    private String mUrl_ApiServer;

    public  ResponseEntity<String> apiHolistic3d(String apiId, String apiKey, MultipartFile file, String uploadPath, String saveId){

        String url = mUrl_ApiServer + "/holistic3d/upload";

        String logMsg = "\n===========================================================================\n";
        logMsg += "Holistic3d API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "image", file.getOriginalFilename());
        logMsg += "===========================================================================";
        logger.info(logMsg);

        try (CloseableHttpClient client = HttpClients.createDefault()){

            HttpPost post = new HttpPost(url);
            File varFile = new File(uploadPath);

            if(!varFile.exists()) {
                varFile.mkdirs();
            }

            varFile = new File((uploadPath + "/" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
            file.transferTo(varFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addBinaryBody("image", varFile, ContentType.create("image/png"), file.getOriginalFilename());


            HttpEntity entity = builder.build();
            post.setEntity(entity);

            HttpResponse response = client.execute(post);
            HttpEntity responseEntity = response.getEntity();
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("Holistic3d responseCode = {}" , responseCode);

            varFile.delete();

            // Response ERROR
            if (responseCode != 200) {
                String responseString = "";
                if(responseEntity!=null) {
                    responseString = EntityUtils.toString(responseEntity, "UTF-8");
                }
                logger.error("Holistic3d API fail : \n{}", responseString);
//                throw new RuntimeException(" @ Holistic3d ErrCode : " + response);
                return null;
            }

            // Response OK
            String resultMsg = EntityUtils.toString(responseEntity, "UTF-8");
            logger.debug(resultMsg);
            return new ResponseEntity<>(resultMsg, HttpStatus.OK);

        } catch (Exception e) {
            logger.error(" @ Holistic3d API exception : {}" , e.getMessage());
            e.printStackTrace();
        }

        return null;
    }
}
