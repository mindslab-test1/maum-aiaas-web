package console.maum.ai.holistic3d.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
@RequestMapping(value = "/cloudApi/holistic3d")


public class Holistic3dController {
    private static final Logger logger = LoggerFactory.getLogger(Holistic3dController.class);


    @Value("${api.url}")
    private String apiUrl;

    /**
     * holistic3d Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/krHolistic3dMain")
    public String krHolistic3dMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krHolistic3dMain");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "holistic3dMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "3D 인테리어");
        httpSession.setAttribute("menuGrpName", "시각");

        httpSession.setAttribute("apiUrl", apiUrl);

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/holistic3d/holistic3dMain.pg";

        return returnUri;

    }

    @RequestMapping(value = "/enHolistic3dMain")
    public String enHolistic3dMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enHolistic3dMain");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "holistic3dMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Holistic 3D");
        httpSession.setAttribute("menuGrpName", "Vision");

        httpSession.setAttribute("apiUrl", apiUrl);

        model.addAttribute("lang", "en");
        String returnUri = "/en/holistic3d/holistic3dMain.pg";

        return returnUri;

    }
}
