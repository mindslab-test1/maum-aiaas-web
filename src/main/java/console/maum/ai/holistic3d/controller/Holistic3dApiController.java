package console.maum.ai.holistic3d.controller;

import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.holistic3d.service.Holistic3dService;
import console.maum.ai.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class Holistic3dApiController {

    @Autowired
    private Holistic3dService service;

    @RequestMapping(value = "/api/holistic3d/upload")
    public ResponseEntity<String> apiHolistic3d(@RequestParam MultipartFile image, HttpServletRequest request) {
        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getId();

        return service.apiHolistic3d(apiId, apiKey, image, PropertyUtil.getUploadPath() + "/holistic3d/", savedId);
    }
}
