package console.maum.ai.support.service;

import console.maum.ai.support.model.MAUForm;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import console.maum.ai.common.model.ResultDto;
import console.maum.ai.support.dao.SupportDao;
import console.maum.ai.support.model.SupportForm;

@Service
public class SupportService {

	private final static Logger logger = LoggerFactory.getLogger(SupportService.class);

	@Autowired
	private SupportDao supportDao;

	public ResultDto insertSupportIdKey(SupportForm supportForm) throws Exception {

		ResultDto resultDto = new ResultDto();

		String toaddr = "support@mindslab.ai";
		String fromaddr = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String subject = "ID/Key 발급 신청";					// 제목
		String message = 						 		// 내용
				"* 사용자 email : " + supportForm.getUserEmail() + "<br><br>" +
						"* 서비스 내용 : " + supportForm.getContent() + "<br>" +
						"* 사용 API : " + supportForm.getName() + "<br>" +
						"* 사용 회사 : " + supportForm.getCompany() + "<br>" +
						"* ID,Key 받을  email : " + supportForm.getMailAddr() + "<br><br>" +
						"해당 메일은 maum.ai 이용내역 ID,Key 발급신청을 통해 발송되었습니다.";

		supportForm.setFromaddr(fromaddr);
		supportForm.setToaddr(toaddr);
		supportForm.setSubject(subject);
		supportForm.setMessage(message);
		supportForm.setStatus(0);

		int insertCnt = 0;

		insertCnt = supportDao.insertSupportMail(supportForm);

		if (insertCnt > 0) {
			resultDto.setState("SUCCESS");
		} else {
			resultDto.setState("FAIL");
		}

		return resultDto;
	}

	public ResultDto insertSupportContact(SupportForm supportForm) throws Exception {

		ResultDto resultDto = new ResultDto();

		String toaddr = "support@mindslab.ai";
		String fromaddr = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String subject = "[Cloud API]"+ supportForm.getName() +"님의 문의 사항입니다.";	// 제목
		String message = 						 		// 내용
				"이  름 : " + supportForm.getName() + "<br>" +
						"회사명 : " + supportForm.getCompany() + "<br>" +
						"이메일 : " + supportForm.getMailAddr() + "<br>" +
						"연락처 : " + supportForm.getPhone() + "<br>" +
						"내  용 <br>" + supportForm.getContent();

		supportForm.setFromaddr(fromaddr);
		supportForm.setToaddr(toaddr);
		supportForm.setSubject(subject);
		supportForm.setMessage(message);
		supportForm.setStatus(0);

		int insertCnt = 0;

		insertCnt = supportDao.insertSupportMail(supportForm);

		if (insertCnt > 0) {
			resultDto.setState("SUCCESS");
		} else {
			resultDto.setState("FAIL");
		}

		return resultDto;
	}

	public int getTodayMAU() throws Exception{
		return supportDao.getTodayMAU();
	}

	public void insertMAU(MAUForm mauForm) throws Exception{
		supportDao.insertMAU(mauForm);
	}

}