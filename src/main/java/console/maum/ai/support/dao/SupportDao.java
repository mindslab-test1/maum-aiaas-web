package console.maum.ai.support.dao;

import java.util.List;

import javax.annotation.Resource;

import console.maum.ai.support.model.MAUForm;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import console.maum.ai.member.model.MemberForm;
import console.maum.ai.support.model.SupportDto;
import console.maum.ai.support.model.SupportForm;

@Repository
public class SupportDao {


	@Resource(name = "sqlSession")
	private SqlSession sqlSession;

	private static final String NAMESPACE = "console.maum.ai.model.supportMapper";

	/**
	 * id,key 신청 메일 - 등록
	 */
	public int insertSupportMail(SupportForm supportForm) throws Exception {
		return sqlSession.insert(NAMESPACE + ".insertSupportMail", supportForm);
	}
	
	/** id,key 신청 메일 발송 대상 - 목록 조회 */
	public List<SupportDto> getSupportMailList(int status) throws Exception {

		return sqlSession.selectList(NAMESPACE + ".getSupportMailList", status);
	}
	
	/**
	 * id,key 신청 메일 발송 완료 업데이트
	 */
	public int updateSupportMail(SupportForm supportForm) throws Exception {

		return sqlSession.update(NAMESPACE + ".updateSupportMail", supportForm);
	}

	public void insertMAU(MAUForm mauForm) throws Exception {
		sqlSession.insert(NAMESPACE+".insertMAU", mauForm);
	}

	public int getTodayMAU() throws Exception{
		return sqlSession.selectOne(NAMESPACE+".getTodayMAU");
	}

}