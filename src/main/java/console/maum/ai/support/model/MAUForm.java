package console.maum.ai.support.model;

import lombok.Data;

@Data
public class MAUForm {
    String Category;
    String Item;
    String Period;
    String RegDateTime;
    int Value;
    String Type;
}
