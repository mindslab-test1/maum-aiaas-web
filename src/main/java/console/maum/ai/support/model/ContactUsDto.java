package console.maum.ai.support.model;

public class ContactUsDto {
	private String name;
	private String company;
	private String mailAddr;
	private String phone;
	private String content;
	private String userEmail;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getMailAddr() {
		return mailAddr;
	}
	public void setMailAddr(String mailAddr) {
		this.mailAddr = mailAddr;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getUserEmail() {
		return userEmail;
	}
	public void setUserEmail(String email) {
		this.userEmail = email;
	}
	@Override
	public String toString() {
		return "ContactUsModel [name=" + name + ", company=" + company + ", mailAddr=" + mailAddr + ", phone=" + phone
				+ ", content=" + content + "]";
	}
}
