package console.maum.ai.support.model;

public class SupportDto extends SupportVo {

	private String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
