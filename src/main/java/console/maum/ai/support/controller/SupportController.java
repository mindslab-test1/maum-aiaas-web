package console.maum.ai.support.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import console.maum.ai.admin.apikey.service.ApiKeyService;
import console.maum.ai.common.data.CommonResponse;
import console.maum.ai.member.model.MemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import console.maum.ai.common.model.ResultDto;
import console.maum.ai.common.util.MailSender;
import console.maum.ai.support.model.ContactUsDto;
import console.maum.ai.support.model.SupportForm;
import console.maum.ai.support.service.SupportService;

import console.maum.ai.common.data.CommonMsg;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Controller
@RequestMapping(value = "/support")
public class SupportController {

	@Autowired
	private SupportService supportService;

	@Autowired
	private ApiKeyService registApiService;

	@Autowired
	private MailSender mailSender;


//	private String batchStatus = PropertyUtil.getProperty("batch.status");

	private static final Logger logger = LoggerFactory.getLogger(SupportController.class);

	/**
	 * request
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/requests")
	public String requests(HttpServletRequest request, Model model) {
		logger.info("Welcome request!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "requests");

		String lang = request.getLocale().toString();
		String returnUri = "";

//		System.out.println("SupportController ::::::  "+ lang);

		if("ko_KR".equals(lang) || "ko".equals(lang)){
			httpSession.setAttribute("menuName", "고객지원");
			httpSession.setAttribute("subMenuName", "My Request");
			returnUri = "/kr/support/requests.pg";
		}else{
			httpSession.setAttribute("menuName", "Support");
			httpSession.setAttribute("subMenuName", "My Request");
			returnUri = "/en/support/requests.pg";
		}

		return returnUri;
	}

	/**
	 * contact us
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krContactUs")
	public String krContactUs(HttpServletRequest request, Model model) {
		logger.info("Welcome krContactUs!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "contactUs");
		httpSession.setAttribute("menuName", "");
		httpSession.setAttribute("subMenuName", "Contact Us");
		httpSession.setAttribute("menuGrpName", "");

		model.addAttribute("lang", "ko");
		String returnUri = "/kr/support/contactUs.pg";

		return returnUri;
	}

	/**
	 * contact us
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enContactUs")
	public String enContactUs(HttpServletRequest request, Model model) {
		logger.info("Welcome enContactUs!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "contactUs");
		httpSession.setAttribute("menuName", "");
		httpSession.setAttribute("subMenuName", "Contact Us");
		httpSession.setAttribute("menuGrpName", "");

		model.addAttribute("lang", "en");
		String returnUri = "/en/support/contactUs.pg";

		return returnUri;
	}

	@RequestMapping(value = "/sendMail")
	@ResponseBody
	public String sendMail(ContactUsDto contactUsInfo){
		try {
			MailSender mailSender = new MailSender();
			mailSender.mailSend(contactUsInfo);
		} catch (Exception e) {
			return "error";
		}
		return "success";
	}

	@RequestMapping(value = "/sendMailApi")
	@ResponseBody
	public ResultDto sendMailApi(ContactUsDto contactUsInfo){
		ResultDto resultDto = new ResultDto();
		resultDto.setState("SUCCESS");
		try {
			MailSender mailSender = new MailSender();
			mailSender.mailSendApi(contactUsInfo);

		} catch (Exception e) {
			resultDto.setState("FAIL");
		}

		return resultDto;
	}

	/**
	 * pricing
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krPricing")
	public String krPricing(HttpServletRequest request, Model model) {
		logger.info("Welcome krPricing!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "pricing");
		httpSession.setAttribute("menuName", "가격정책");
		httpSession.setAttribute("subMenuName", "");
		httpSession.setAttribute("menuGrpName", "");

		model.addAttribute("lang", "ko");
		String returnUri = "/kr/support/pricing.pg";

		return returnUri;
	}

	/**
	 * pricing
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enPricing")
	public String enPricing(HttpServletRequest request, Model model) {
		logger.info("Welcome enPricing!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "pricing");
		httpSession.setAttribute("menuName", "Pricing");
		httpSession.setAttribute("subMenuName", "");
		httpSession.setAttribute("menuGrpName", "");

		model.addAttribute("lang", "en");
		String returnUri = "/en/support/pricing.pg";

		return returnUri;
	}

	@RequestMapping(value = "/krPricingFail")
	public String krPricingFail(HttpServletRequest request, Model model, String reason, String moid) {
		logger.info("Welcome krPricingFail!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "pricing");
		httpSession.setAttribute("menuName", "가격정책");
		httpSession.setAttribute("subMenuName", "");
		httpSession.setAttribute("menuGrpName", "");

		model.addAttribute("lang", "ko");
		model.addAttribute("reason", reason);
		model.addAttribute("moid", moid);
		String returnUri = "/kr/support/pricingFail.pg";

		return returnUri;
	}

	@RequestMapping(value = "/enPricingFail")
	public String pricingFail(HttpServletRequest request, Model model, String reason, String moid) {
		logger.info("Welcome pricingFail!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "pricing");
		httpSession.setAttribute("menuName", "Pricing");
		httpSession.setAttribute("subMenuName", "");
		httpSession.setAttribute("menuGrpName", "");

		model.addAttribute("lang", "en");
		model.addAttribute("reason", reason);
		model.addAttribute("moid", moid);

		String returnUri = "/en/support/pricingFail.pg";

		return returnUri;
	}

	@RequestMapping(value="/faq")
	public String faq(HttpServletRequest request, Model model) {
		logger.info("Welcome faq!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "faq");
		httpSession.setAttribute("subMenuName", "FAQ");
		httpSession.setAttribute("menuGrpName", "");

		String lang = request.getLocale().toString();
		String returnUri = "";

//		System.out.println("SupportController ::::::  "+ lang);

		if("ko_KR".equals(lang) || "ko".equals(lang)){
			httpSession.setAttribute("menuName", "고객지원");
			returnUri = "/kr/support/faq.pg";
		}else{
			httpSession.setAttribute("menuName", "Support");
			returnUri = "/en/support/faq.pg";
		}

		return returnUri;
	}

	@RequestMapping(value="/faq_write")
	public String faq_write(HttpServletRequest request, Model model) {
		logger.info("Welcome faq_write!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "faq");
		httpSession.setAttribute("subMenuName", "FAQ");
		httpSession.setAttribute("menuGrpName", "");

		String lang = request.getLocale().toString();
		String returnUri = "";

//		System.out.println("SupportController ::::::  "+ lang);

		if("ko_KR".equals(lang) || "ko".equals(lang)){
			httpSession.setAttribute("menuName", "고객지원");
			returnUri = "/kr/support/faq_write.pg";
		}else{
			httpSession.setAttribute("menuName", "Support");
			returnUri = "/en/support/faq_write.pg";
		}

		return returnUri;
	}

	/**
	 * 이용약관
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krTerms")
	public String krTerms(HttpServletRequest request, Model model) {
		logger.info("Welcome krTerms!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "terms");
		httpSession.setAttribute("menuName", "");
		httpSession.setAttribute("subMenuName", "이용약관");
		httpSession.setAttribute("menuGrpName", "");

		model.addAttribute("lang", "ko");
		String returnUri = "/kr/support/terms.pg";

		return returnUri;
	}

	/**
	 * 이용약관
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enTerms")
	public String enTerms(HttpServletRequest request, Model model) {
		logger.info("Welcome enTerms!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "terms");
		httpSession.setAttribute("menuName", "");
		httpSession.setAttribute("subMenuName", "Terms & Conditions");
		httpSession.setAttribute("menuGrpName", "");

		model.addAttribute("lang", "en");
		String returnUri = "/en/support/terms.pg";

		return returnUri;
	}

	/** id,key 발급 신청 등록 */
	@RequestMapping(value = "/insertSupportIdKey")
	@ResponseBody
	public ResultDto insertSupportIdKey(HttpServletRequest request, HttpServletResponse response, SupportForm supportForm) throws Exception {

		ResultDto resultDto = new ResultDto();

		Gson gson = new Gson();

		logger.info("{} .insertSupportIdKey()", gson.toJson(supportForm));
		System.out.println("insertSupportIdKey() ======= " + gson.toJson(supportForm));

		resultDto = supportService.insertSupportIdKey(supportForm);

		return resultDto;
	}

	/** 문의 사항 등록 */
	@RequestMapping(value = "/insertSupportContact")
	@ResponseBody
	public ResultDto insertSupportContact(HttpServletRequest request, HttpServletResponse response, SupportForm supportForm) throws Exception {

		ResultDto resultDto = new ResultDto();

		Gson gson = new Gson();

		logger.info("{} .insertSupportContact()", gson.toJson(supportForm));
		System.out.println("insertSupportContact() ======= " + gson.toJson(supportForm));

		resultDto = supportService.insertSupportContact(supportForm);

		return resultDto;
	}

	@RequestMapping(value ="/createApiKeyId")
	@ResponseBody
	public ResponseEntity<String> createApiKeyId(HttpServletRequest request) throws Exception{

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		Map<String, String> result = new HashMap<>();

		/* apiId가 중복될 경우 다시 실행 */
		do {
			UUID uuid = UUID.randomUUID();
			String apiId = memberVo.getId() + uuid.toString().split("-")[0] + uuid.toString().split("-")[1];

			Map<String, String> usrMap = new HashMap<>();
			usrMap.put("apiId", apiId);
			usrMap.put("email", memberVo.getEmail());
			usrMap.put("name", memberVo.getName());

			result = registApiService.addApiKey(usrMap);
		}while(result.get("message").equals("IdOverlap"));

		System.out.println("this is result @@@@ "+result.toString());

		if("Success".equals(result.get("message")))
			return new ResponseEntity<String>("success", HttpStatus.OK);
		else
			return new ResponseEntity<String>("fail", HttpStatus.BAD_REQUEST);
	}

	/** 사용자가 contact 메일 보내기 - 2020. 01. 10 - LYJ */
	@RequestMapping(value = "/sendContactMail", method={RequestMethod.POST})
	@ResponseBody
	public String sendContactMail(HttpServletRequest request, HttpServletResponse response,
								@RequestParam(value = "fromaddr") String fromaddr,
								@RequestParam(value = "toaddr") String toaddr,
								@RequestParam(value = "subject") String subject,
								@RequestParam(value = "message") String msg) throws Exception {

		System.out.println("sendContactMail ==> " + fromaddr);

		SupportForm supportForm = new SupportForm();
		supportForm.setFromaddr(fromaddr);
		supportForm.setToaddr(toaddr);
		supportForm.setSubject(subject);
		supportForm.setMessage(msg);
		supportForm.setStatus(0);

		Gson gson = new Gson();

		logger.info("{} .sendContactMail()", gson.toJson(supportForm));
		System.out.println("sendContactMail() ======= " + gson.toJson(supportForm));
		mailSender.sendContactMail(supportForm);

		return "";
/*		ResultDto resultDto = mailSender.sendContactMail(supportForm);
		return resultDto;*/
	}


	@RequestMapping(value = "/sendMail", method={RequestMethod.POST})
	@ResponseBody
	public CommonResponse<Object> sendMail(HttpServletRequest request, HttpServletResponse response,
										   @RequestParam(value = "fromaddr") String fromaddr,
										   @RequestParam(value = "toaddr") String toaddr,
										   @RequestParam(value = "subject") String subject,
										   @RequestParam(value = "message") String msg) throws Exception {

		System.out.println("sendContactMail ==> " + fromaddr);

		SupportForm supportForm = new SupportForm();
		supportForm.setFromaddr(fromaddr);
		supportForm.setToaddr(toaddr);
		supportForm.setSubject(subject);
		supportForm.setMessage(msg);
		supportForm.setStatus(0);

		Gson gson = new Gson();

		logger.info("{} .sendContactMail()", gson.toJson(supportForm));

		CommonResponse<Object> resp = new CommonResponse<>();

		if( isValidEmail(fromaddr) && isValidEmail(toaddr) ) { //mail 형식 정규식 검사
			mailSender.sendContactMail(supportForm);

			resp.setCode(CommonMsg.ERR_CODE_SUCCESS);
			resp.setMsg(CommonMsg.ERR_MSG_SUCCESS);

		} else {

			resp.setCode(CommonMsg.ERR_CODE_PARAMS_INVALID);

			String resultMsg = CommonMsg.ERR_MSG_PARAMS_INVALID + " -";
			if( isValidEmail(fromaddr) )
				resultMsg = resultMsg + " fromaddr";
			if( isValidEmail(toaddr) )
				resultMsg = resultMsg + " toaddr";

			resp.setMsg(resultMsg);
		}

		return resp;
	}


	/** 이메일 유효성 검사 - 2020. 08. 18 - LYJ */
	public static boolean isValidEmail(String email) {
		boolean err = false;

		String regex = "^[_a-z0-9-]+(.[_a-z0-9-]+)*@(?:\\w+\\.)+\\w+$";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(email);

		if (m.matches()) {
			err = true;
		}

		return err;
	}
}