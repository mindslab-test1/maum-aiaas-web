package console.maum.ai.nlu.controller;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import console.maum.ai.member.model.MemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import console.maum.ai.nlu.service.NluService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/*
 * API Controller
 */

@RestController
public class NluApiController {
    private final static Logger logger = LoggerFactory.getLogger(NluApiController.class);
    @Autowired
    private NluService nluService;

    @PostMapping(value= "/api/nlu/nluApi")
    public String nluApi(@RequestParam(value="lang") String lang, @RequestParam(value="reqText") String text, HttpServletRequest request) throws UnsupportedEncodingException {
        logger.info("===== NLU ApiController =====");

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        String result = nluService.nluApi(apiId, apiKey, lang, text);
        String encodedResult = URLEncoder.encode(result, "UTF-8");

        return encodedResult;
    }
}
