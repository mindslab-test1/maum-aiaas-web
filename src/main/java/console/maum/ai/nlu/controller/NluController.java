package console.maum.ai.nlu.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import console.maum.ai.nlu.service.NluService;

@Controller
@RequestMapping(value = "/cloudApi/nlu")
public class NluController {

	private static final Logger logger = LoggerFactory.getLogger(NluController.class);

	@Autowired
	private NluService nluService;	
	
	/**
	 * nlu Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krNluMain")
	public String krNluMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krNluMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "nluMain");
		httpSession.setAttribute("menuName", "언어");
		httpSession.setAttribute("subMenuName", "자연어 이해");
		httpSession.setAttribute("menuGrpName", "언어");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/nlu/nluMain.pg";	
		
		return returnUri;
	}
	
	/**
	 * nlu Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enNluMain")
	public String enNluMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enNluMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "nluMain");
		httpSession.setAttribute("menuName", "Languages");
		httpSession.setAttribute("subMenuName", "Natural Language");
		httpSession.setAttribute("menuGrpName", "Languages");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/nlu/nluMain_Tmp.pg";
		
		return returnUri;
	}	

}
