package console.maum.ai.anomalyIntrusion.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/anomalyIntrusion")
public class AnomalyIntrusionController {

    private static final Logger logger = LoggerFactory.getLogger(AnomalyIntrusionController.class);

    /**
     * anomalyIntrusion Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/krAnomalyIntrusionMain")
    public String krAnomalyIntrusionMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krAnomalyIntrusionMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "anomalyIntrusionMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "침입 감지");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/anomalyIntrusion/anomalyIntrusionMain.pg";

        return returnUri;
    }

    @RequestMapping(value = "/enAnomalyIntrusionMain")
    public String enAnomalyIntrusionMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enAnomalyIntrusionMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "anomalyIntrusionMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Intrusion Detection");
        httpSession.setAttribute("menuGrpName", "Vision");

        model.addAttribute("lang", "en");
        String returnUri = "/en/anomalyIntrusion/anomalyIntrusionMain.pg";

        return returnUri;
    }


}