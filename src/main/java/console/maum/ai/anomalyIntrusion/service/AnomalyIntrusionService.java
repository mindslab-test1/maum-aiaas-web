package console.maum.ai.anomalyIntrusion.service;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})

@Service
public class AnomalyIntrusionService {

    private static final Logger logger = LoggerFactory.getLogger(AnomalyIntrusionService.class);

    @Value("${api.url}")
    private String mUrl_ApiServer;

    public ResponseEntity<byte[]> apiAnomalyIntrusion(String apiId, String apiKey, MultipartFile orgFile, String roiList, String uploadPath){

        try {
            String url = mUrl_ApiServer + "/abnormal-behavior/intrusion:detect";

            String logMsg = "\n===========================================================================\n";
            logMsg += "AnomalyIntrusion API @ PARAMS \n";
            logMsg += String.format(":: %-10s = %s%n", "URL", url);
            logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
            logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
            logMsg += String.format(":: %-10s = %s%n", "video", orgFile.getOriginalFilename());
            logMsg += String.format(":: %-10s = %s%n", "roiList", roiList);
            logMsg += "===========================================================================";
            logger.info(logMsg);



            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File AnomalyFile = new File(uploadPath);


            if (!AnomalyFile.exists()) {
                logger.info("create Dir : {}", AnomalyFile.getPath());
                AnomalyFile.mkdirs();
            }

            AnomalyFile = new File((uploadPath + "/" + orgFile.getOriginalFilename().substring(orgFile.getOriginalFilename().lastIndexOf("\\") + 1)));
            orgFile.transferTo(AnomalyFile);

            roiList = roiList.replaceAll("action", "intrusion");

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset(StandardCharsets.UTF_8)));
            builder.addPart("apiKey",new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset(StandardCharsets.UTF_8)));
            builder.addBinaryBody("video", AnomalyFile, ContentType.create("video/*"), orgFile.getOriginalFilename());
            builder.addPart("roiList", new StringBody(roiList, ContentType.TEXT_PLAIN.withCharset(StandardCharsets.UTF_8)) );

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("AnomalyIntrusion responseCode = {}" , responseCode);


            if (responseCode != 200) {
//                throw new RuntimeException("ErrCode : " + response);
                AnomalyFile.delete();
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), StandardCharsets.UTF_8));
                StringBuilder result = new StringBuilder();
                String line = "";
                while((line = rd.readLine()) != null){
                    result.append(line);
                }
                logger.error("AnomalyIntrusion API fail : {}", response.getEntity());
                return null;
            }

            HttpEntity responseEntity = response.getEntity();
            HttpHeaders headers = new HttpHeaders();
            InputStream in = responseEntity.getContent();
            byte[] resultArray = IOUtils.toByteArray(in);

            ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(resultArray, headers, HttpStatus.OK);

            AnomalyFile.delete();

            return resultEntity;

        }catch(Exception e){
            logger.error("AnomalyIntrusion API exception : {}", e.toString());
            e.printStackTrace();
        }

        return null;
    }



}