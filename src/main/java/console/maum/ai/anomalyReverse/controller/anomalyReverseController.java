package console.maum.ai.anomalyReverse.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/anomalyReverse")
public class anomalyReverseController {

    private static final Logger logger = LoggerFactory.getLogger(anomalyReverseController.class);

    /**
     * anomalyReverse Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/krAnomalyReverseMain")
    public String krAnomalyReverseMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krAnomalyReverseMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "anomalyReverseMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "역주행 감지");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/anomalyReverse/anomalyReverseMain.pg";

        return returnUri;
    }

    @RequestMapping(value = "/enAnomalyReverseMain")
    public String enAnomalyReverseMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enAnomalyReverseMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "anomalyReverseMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Reverse Direction Detection");
        httpSession.setAttribute("menuGrpName", "Vision");

        model.addAttribute("lang", "en");
        String returnUri = "/en/anomalyReverse/anomalyReverseMain.pg";

        return returnUri;
    }


}