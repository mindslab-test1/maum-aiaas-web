package console.maum.ai.anomalyReverse.controller;


import console.maum.ai.anomalyReverse.service.AnomalyReverseService;
import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class anomalyReverseApiController {

    @Autowired
    private AnomalyReverseService anomalyReverseService;


    @RequestMapping(value="/api/multiAnomaly")
    @ResponseBody
    public ResponseEntity<byte[]> multiAnomaly(@RequestParam MultipartFile file, HttpServletRequest request){

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        return anomalyReverseService.AnomalyMultipart(apiId, apiKey, file, PropertyUtil.getUploadPath() + "/anomaly/");
    }


}