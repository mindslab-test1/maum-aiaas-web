package console.maum.ai.anomalyReverse.service;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})

@Service
public class AnomalyReverseService {

    private static final Logger logger = LoggerFactory.getLogger(AnomalyReverseService.class);

    @Value("${api.url}")
    private String mUrl_ApiServer;

    public ResponseEntity<byte[]> AnomalyMultipart(String apiId, String apiKey, MultipartFile orgFile, String uploadPath){
        Map<Integer, ResponseEntity> map = new HashMap<>();

        try {
            String url = mUrl_ApiServer + "/smartXLoad/anomalyDetect";

            String logMsg = "\n===========================================================================\n";
            logMsg += "Anomaly API @ PARAMS \n";
            logMsg += String.format(":: %-10s = %s%n", "URL", url);
            logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
            logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
            logMsg += String.format(":: %-10s = %s%n", "video", orgFile.getOriginalFilename());
            logMsg += "===========================================================================";
            logger.info(logMsg);



            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File AnomalyFile = new File(uploadPath);


            if (!AnomalyFile.exists()) {
                logger.info("create Dir : {}", AnomalyFile.getPath());
                AnomalyFile.mkdirs();
            }

            AnomalyFile = new File((uploadPath + "/" + orgFile.getOriginalFilename().substring(orgFile.getOriginalFilename().lastIndexOf("\\") + 1)));
            orgFile.transferTo(AnomalyFile);
            FileBody anomalyFileBody = new FileBody(AnomalyFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiKey",new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("video", anomalyFileBody);

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);


            if (responseCode != 200) {
//                throw new RuntimeException("ErrCode : " + response);
                AnomalyFile.delete();
                logger.error("API fail : {}", response.getEntity());
                return null;
            }

            HttpEntity responseEntity = response.getEntity();
            HttpHeaders headers = new HttpHeaders();
            InputStream in = responseEntity.getContent();
            byte[] resultArray = IOUtils.toByteArray(in);

            ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(resultArray, headers, HttpStatus.OK);

            AnomalyFile.delete();

            return resultEntity;

        }catch(Exception e){
            logger.error("API exception : {}", e.toString());
            e.printStackTrace();
        }

        return null;
    }



}