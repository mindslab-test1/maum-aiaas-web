package console.maum.ai.aihuman.dao;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class AiHumanBuilderDao {

    @Resource(name = "sqlSession")
    private SqlSession sqlSession;

    private static final String NAMESPACE = "console.maum.ai.aihuman.aihumanbuilderMapper";

    public List<Map<String, Object>> getJobList() {
        return sqlSession.selectList(NAMESPACE + ".getJobList");
    }

    public Map<String, Object> getJobByCode(String jobCode) {

        Map<String, String> param = new HashMap<>();
        param.put("jobCode", jobCode);
        return sqlSession.selectOne(NAMESPACE + ".getJobByCode", param);
    }

    public List<Map<String, Object>> getActorList() {
        return sqlSession.selectList(NAMESPACE + ".getActorList");
    }

    public Map<String, Object> getActorByCode(String actorCode) {

        Map<String, String> param = new HashMap<>();
        param.put("actorCode", actorCode);
        return sqlSession.selectOne(NAMESPACE + ".getActorByCode", param);
    }

    public List<Map<String, Object>> getWorkList() {
        return sqlSession.selectList(NAMESPACE + ".getWorkList");
    }

    public List<Map<String, Object>> getWorkListByCodes(String[] workIdList) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("workIdList", workIdList);
        return sqlSession.selectList(NAMESPACE + ".getWorkListByCodes", parameter);
    }

    public List<Map<String, Object>> getDisplayList() {
        return sqlSession.selectList(NAMESPACE + ".getDisplayList");
    }

    public List<Map<String, Object>> getDisplayListByCodes(String[] displayIdList) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("dvcCodeList", displayIdList);
        return sqlSession.selectList(NAMESPACE + ".getDisplayListByCodes");
    }

    public Map<String, Object> getDisplayByCode(String dvcCode) {

        Map<String, String> param = new HashMap<>();
        param.put("dvcCode", dvcCode);
        return sqlSession.selectOne(NAMESPACE + ".getDisplayByCode", param);
    }

    public List<Map<String, Object>> getToolsList() {
        return sqlSession.selectList(NAMESPACE + ".getToolsList");
    }

    public List<Map<String, Object>> getToolsListByCodes(String[] toolIdList) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("toolCodeList", toolIdList);

        return sqlSession.selectList(NAMESPACE + ".getToolsListByCode", parameter);
    }

    public List<Map<String, Object>> getWorkByJobId(String jobId) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("jobId", jobId);
        parameter.put("useYn", "Y");
        return sqlSession.selectList(NAMESPACE + ".getWorkByJobId", parameter);
    }

    public List<Map<String, Object>> getDisplayByJobId(String jobId) {
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("jobId", jobId);
        parameter.put("useYn", "Y");
        return sqlSession.selectList(NAMESPACE + ".getDisplayByJobId", parameter);
    }

    public List<Map<String, Object>> getToolsByWorkId(String[] workIdList) {
//        String[] workIdArray = new String[workIdList.size()];
//        for(int i=0; i<workIdList.size(); i++) {
//            workIdArray[i] = workIdList.get(i);
//        }
        Map<String, Object> parameter = new HashMap<>();
        parameter.put("workIdList", workIdList);
        parameter.put("useYn", "Y");
        return sqlSession.selectList(NAMESPACE + ".getToolsByWorkId", parameter);
    }

    public int registReceiptInfo(Map<String, Object> parameter) {
        return sqlSession.insert(NAMESPACE + ".insertReceipt", parameter);
    }
}