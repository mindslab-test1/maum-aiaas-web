package console.maum.ai.aihuman.service;

import console.maum.ai.aihuman.dao.AiHumanBuilderDao;
import console.maum.ai.common.util.MailSender;
import console.maum.ai.shopify.service.ShopifyService;
import console.maum.ai.support.controller.SupportController;
import console.maum.ai.support.model.ContactUsDto;
import console.maum.ai.support.model.SupportForm;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
public class AiHumanBuilderService {
    private static final Logger logger = LoggerFactory.getLogger(AiHumanBuilderService.class);

    @Autowired
    private AiHumanBuilderDao aiHumanBuilderDao;

    @Autowired
    private MailSender mailSender;

    public List<Map<String, Object>> getJobList() {
        return aiHumanBuilderDao.getJobList();
    }

    public List<Map<String, Object>> getActorList() {
        return aiHumanBuilderDao.getActorList();
    }

    public List<Map<String, Object>> getWorkList() {
        return aiHumanBuilderDao.getWorkList();
    }

    public List<Map<String, Object>> getDisplayList() {
        return aiHumanBuilderDao.getDisplayList();
    }

    public List<Map<String, Object>> getToolsList() {
        return aiHumanBuilderDao.getToolsList();
    }

    public List<Map<String, Object>> getWorkByJobId(String jobId) {
        return aiHumanBuilderDao.getWorkByJobId(jobId);
    }
    public List<Map<String, Object>> getDisplayByJobId(String jobId) {
        return aiHumanBuilderDao.getDisplayByJobId(jobId);
    }
    public List<Map<String, Object>> getToolsByWorkId(String[] workIdList) {
        return aiHumanBuilderDao.getToolsByWorkId(workIdList);
    }

    public boolean registReceiptInfo(Map<String, String[]> param) throws UnsupportedEncodingException {
        Map<String, Object> parameter = new HashMap<>();

        // for mail
        if(param.get("name") != null) {
            String[] name = param.get("name");
            parameter.put("name", name[0]);
        }
        if(param.get("tel") != null) {
            String[] tel = param.get("tel");
            parameter.put("tel", tel[0]);
        }
        if(param.get("email") != null) {
            String[] email = param.get("email");
            parameter.put("email", email[0]);
        }
        if(param.get("company") != null) {
            String[] company = param.get("company");
            parameter.put("company", company[0]);
        }
        if(param.get("time") != null) {
            String[] time = param.get("time");
            parameter.put("time", time[0]);
        }

        // for receipt
        if(param.get("jobCode") != null) {
            String[] jobCode = param.get("jobCode");
            parameter.put("jobCode", jobCode[0]);
            Map<String, Object> jobInfo = aiHumanBuilderDao.getJobByCode(jobCode[0]);
            if(jobInfo != null) {
                parameter.put("jobName", jobInfo.get("jobName"));
            }
        }
        if(param.get("actorCode") != null) {
            String[] actorCode = param.get("actorCode");
            parameter.put("actorCode", actorCode[0]);
            Map<String, Object> actorInfo = aiHumanBuilderDao.getActorByCode(actorCode[0]);
            if(actorInfo != null) {
                parameter.put("actorName", actorInfo.get("actorName"));
            }
        }
        if(param.get("workflow[]") != null) {
            String[] workCode = param.get("workflow[]");
            StringBuffer workCodeSb = new StringBuffer();
            StringBuffer workNameSb = new StringBuffer();
            for(String work : workCode) {
                if(workCodeSb.toString().length() > 0) workCodeSb.append(",");
                workCodeSb.append(work);
            }
            parameter.put("workCode", workCodeSb.toString());
            List<Map<String, Object>> workInfoList = aiHumanBuilderDao.getWorkListByCodes(workCode);
            if(workInfoList != null) {
                for(Map<String, Object> workInfo : workInfoList) {
                    if(workNameSb.toString().length() > 0) workNameSb.append(",");
                    workNameSb.append(workInfo.get("workName"));
                }
                parameter.put("workName", workNameSb.toString());
            }
        }
        if(param.get("device") != null) {
            String[] device = param.get("device");
            parameter.put("dvcCode", device[0]);
            Map<String, Object> deviceInfo = aiHumanBuilderDao.getDisplayByCode(device[0]);
            if(deviceInfo != null) {
                parameter.put("dvcName", deviceInfo.get("dvcName"));
            }
        }
        if(param.get("tools[]") != null) {
            String[] tools = param.get("tools[]");
            StringBuffer toolCodeSb = new StringBuffer();
            StringBuffer toolNameSb = new StringBuffer();
            for(String tool : tools) {
                if(toolCodeSb.toString().length() > 0) toolCodeSb.append(",");
                toolCodeSb.append(tool);
            }
            parameter.put("toolCode", toolCodeSb.toString());
            List<Map<String, Object>> toolInfoList = aiHumanBuilderDao.getToolsListByCodes(tools);
            if(toolInfoList != null) {
                for(Map<String, Object> toolInfo : toolInfoList) {
                    if(toolNameSb.toString().length() > 0) toolNameSb.append(",");
                    toolNameSb.append(toolInfo.get("toolName"));
                }
                parameter.put("toolName", toolNameSb.toString());
            }
        }
        if(param.get("totPrice") != null) {
            String[] totPrice = param.get("totPrice");
            parameter.put("totPrice", totPrice[0]);
        }
        if(aiHumanBuilderDao.registReceiptInfo(parameter) > 0) {
            return sendGenContentAndMail(parameter);
        } else {
            return false;
        }
    }

    private boolean sendGenContentAndMail(Map<String, Object> parameter) {
        SupportForm supportForm = new SupportForm();
        supportForm.setFromaddr("hello@mindslab.ai");
//        supportForm.setToaddr((String)parameter.get("email"));
        supportForm.setToaddr("hello@mindslab.ai");
        supportForm.setSubject("["+(String)parameter.get("name")+"]님의 AI Human 견적요청 메일입니다.");
        supportForm.setStatus(0);

        StringBuffer sendMessage = new StringBuffer();
        if(parameter.get("name") != null) {
            sendMessage.append("이름 : ").append((String)parameter.get("name")).append("<br>");
        }
        if(parameter.get("tel") != null) {
            sendMessage.append("연락처 : ").append((String)parameter.get("tel")).append("<br>");
        }
        if(parameter.get("email") != null) {
            sendMessage.append("E-MAIL : ").append((String)parameter.get("email")).append("<br>");
        }
        if(parameter.get("company") != null) {
            sendMessage.append("회사명 : ").append((String)parameter.get("company")).append("<br>");
        }
        sendMessage.append("============= 견적 내용 ==============").append("<br>");
        if(parameter.get("jobCode") != null) {
            if(StringUtils.isNotEmpty((String)parameter.get("jobName"))) {
                sendMessage.append("[JOB] : ").append((String)parameter.get("jobName")).append("<br>");
            } else {
                sendMessage.append("[JOB] : ").append((String)parameter.get("jobCode")).append("<br>");
            }
        } else {
            sendMessage.append("[JOB]: ").append("미선택").append("<br>");
        }
        if(parameter.get("actorCode") != null) {
            if(StringUtils.isNotEmpty((String)parameter.get("actorName"))) {
                sendMessage.append("[WHO] : ").append((String)parameter.get("actorName")).append("<br>");
            } else {
                sendMessage.append("[WHO] : ").append((String)parameter.get("actorCode")).append("<br>");
            }
        } else {
            sendMessage.append("[WHO]: ").append("미선택").append("<br>");
        }
        if(parameter.get("workCode") != null) {
            if(StringUtils.isNotEmpty((String)parameter.get("workName"))) {
                sendMessage.append("[WORK] : ").append((String)parameter.get("workName")).append("<br>");
            } else {
                sendMessage.append("[WORK] : ").append((String)parameter.get("workCode")).append("<br>");
            }
        } else {
            sendMessage.append("[WORK]: ").append("미선택").append("<br>");
        }
        if(parameter.get("dvcCode") != null) {
            if(StringUtils.isNotEmpty((String)parameter.get("dvcName"))) {
                sendMessage.append("[DISPLAY] : ").append((String)parameter.get("dvcName")).append("<br>");
            } else {
                sendMessage.append("[DISPLAY] : ").append((String)parameter.get("dvcCode")).append("<br>");
            }
        } else {
            sendMessage.append("[DISPLAY]: ").append("미선택").append("<br>");
        }
        if(parameter.get("toolCode") != null) {
            if(StringUtils.isNotEmpty((String)parameter.get("toolName"))) {
                sendMessage.append("[TOOLS] : ").append((String)parameter.get("toolName")).append("<br>");
            } else {
                sendMessage.append("[TOOLS] : ").append((String)parameter.get("toolCode")).append("<br>");
            }
        } else {
            sendMessage.append("[TOOLS]: ").append("미선택").append("<br>");
        }
        if(parameter.get("time") != null) {
            sendMessage.append("연락가능시간: ").append((String)parameter.get("time")).append("<br>");
        } else {
            sendMessage.append("연락가능시간: ").append("미작성").append("<br>");
        }

        supportForm.setMessage(sendMessage.toString());
        try {
            mailSender.sendContactMail(supportForm);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
