package console.maum.ai.aihuman.controller;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import console.maum.ai.aihuman.service.AiHumanBuilderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.UnsupportedEncodingException;
import java.util.*;

@RestController
@RequestMapping(value = "/recept/estimate")
public class AiHumanBuilderApiController {
    private static final Logger logger = LoggerFactory.getLogger(AiHumanBuilderApiController.class);

    @Autowired
    private AiHumanBuilderService aiHumanBuilderService;

    @RequestMapping(value = "/getJobList", method = {RequestMethod.POST})
    @ResponseBody
    public List<Map<String, Object>> getJobList(
            HttpServletRequest request
    ) {
        List<Map<String, Object>> rtnBodyList = aiHumanBuilderService.getJobList();
        return rtnBodyList;
    }

    @RequestMapping(value = "/getActorList", method = {RequestMethod.POST})
    @ResponseBody
    public List<Map<String, Object>> getActorList(
            HttpServletRequest request
    ) {
        List<Map<String, Object>> rtnBodyList = aiHumanBuilderService.getActorList();
        return rtnBodyList;
    }

    @RequestMapping(value = "/getWorkList", method = {RequestMethod.POST})
    @ResponseBody
    public List<Map<String, Object>> getWorkList(
            HttpServletRequest request
    ) {
        List<Map<String, Object>> rtnBodyList = aiHumanBuilderService.getWorkList();
        return rtnBodyList;
    }

    @RequestMapping(value = "/getDisplayList", method = {RequestMethod.POST})
    @ResponseBody
    public List<Map<String, Object>> getDisplayList(
            HttpServletRequest request
    ) {
        List<Map<String, Object>> rtnBodyList = aiHumanBuilderService.getDisplayList();
        return rtnBodyList;
    }

    @RequestMapping(value = "/getToolsList", method = {RequestMethod.POST})
    @ResponseBody
    public List<Map<String, Object>> getToolsList(
            HttpServletRequest request
    ) {
        List<Map<String, Object>> rtnBodyList = aiHumanBuilderService.getToolsList();
        return rtnBodyList;
    }

    @RequestMapping(value = "/getWorkByJobId", method = {RequestMethod.POST})
    @ResponseBody
    public List<Map<String, Object>> getWorkByJobId(
            HttpServletRequest request
    ) {
        Map<String, String[]> map = request.getParameterMap();
        String[] jobIdList = map.get("jobId");

        List<Map<String, Object>> rtnBodyList = aiHumanBuilderService.getWorkByJobId(jobIdList[0]);
        return rtnBodyList;
    }
    @RequestMapping(value = "/getDisplayByJobId", method = {RequestMethod.POST})
    @ResponseBody
    public List<Map<String, Object>> getDisplayByJobId(
            HttpServletRequest request
    ) {
        Map<String, String[]> map = request.getParameterMap();
        String[] jobIdList = map.get("jobId");

        List<Map<String, Object>> rtnBodyList = aiHumanBuilderService.getDisplayByJobId(jobIdList[0]);
        return rtnBodyList;
    }
    @RequestMapping(value = "/getToolsByWorkId", method = {RequestMethod.POST})
    @ResponseBody
    public List<Map<String, Object>> getToolsByWorkId(
            HttpServletRequest request
    ) {
        Map<String, String[]> map = request.getParameterMap();
        String[] workIdList = map.get("workId[]");

        List<Map<String, Object>> rtnBodyList = aiHumanBuilderService.getToolsByWorkId(workIdList);
        return rtnBodyList;
    }

    @RequestMapping(value = "/receiptPrice", method = {RequestMethod.POST})
    @ResponseBody
    public boolean sendReceiptPrice(
            HttpServletRequest request
    ) throws UnsupportedEncodingException {
       Map<String, String[]> map = request.getParameterMap();

        boolean bRtn = aiHumanBuilderService.registReceiptInfo(map);
        return bRtn;
    }

    private String change(String source, String before, String after) {
        int i = 0;
//        int j = false;
        if (source == null) {
            return "";
        } else {
            StringBuffer sb;
            int j;
            for(sb = new StringBuffer(); (j = source.indexOf(before, i)) >= 0; i = j + before.length()) {
                sb.append(source.substring(i, j));
                sb.append(after);
            }

            sb.append(source.substring(i));
            return sb.toString();
        }
    }
}
