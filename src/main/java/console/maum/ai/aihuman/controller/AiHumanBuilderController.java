package console.maum.ai.aihuman.controller;

import console.maum.ai.academy.service.AcademyService;
import console.maum.ai.login.controller.LoginController;
import console.maum.ai.login.service.LoginService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(value="/recept/estimate")
public class AiHumanBuilderController {
    private static final Logger logger = LoggerFactory.getLogger(AiHumanBuilderController.class);

    @Autowired
    private LoginService loginService;

    @Autowired
    private LoginController loginController;

    @Autowired
    private AcademyService academyService;

    @Value("${datatool.url}")
    private String dataEditorTool;

    @Value("${mvp_maker.url}")
    private String mUrl_MVPMaker;

    @Value("${hq.URL}")
    private String sso_url;

    @Value("${hq.client_id}")
    private String client_id;

    @Value("${hq.redirect_uri}")
    private String redirect_uri;

    @RequestMapping(value="/krAhb2", method = RequestMethod.GET)
    public String krAhb2(@RequestParam(value="error", required=false) String error, HttpServletRequest request, HttpServletResponse response, Model model) {

        //국문/영문 분기 처리
        String paramLang = loginService.setLang(request, response);

        //특정페이지 이동 쿠키 발행
        String loginmenu = request.getParameter("loginmenu");
        if(!"".equals(loginmenu)) {
            response.addCookie(new Cookie("loginmenu",loginmenu));
        }else {
            response.addCookie(new Cookie("loginmenu",null));
        }

        model.addAttribute("mvp_maker_url", mUrl_MVPMaker);
        model.addAttribute("error", error);
        model.addAttribute("lang", paramLang);
        model.addAttribute("errormsg", request.getParameter("errormsg"));
        model.addAttribute("google_url", loginController.loginBtnUrl);
        model.addAttribute("sso_url", sso_url);
        model.addAttribute("client_id", client_id);
        model.addAttribute("redirect_uri", redirect_uri);

        String redirectUrl = "noLayout/"+ paramLang+"/builder/ahbMain.pg";

        return redirectUrl;
    }
}
