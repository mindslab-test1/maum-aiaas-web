package console.maum.ai.shopify.service;

import console.maum.ai.shopify.dao.ShopifyDao;
import console.maum.ai.shopify.model.ShopifyLicenseVo;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class ShopifyService {

    private static final Logger logger = LoggerFactory.getLogger(ShopifyService.class);

    @Autowired
    ShopifyDao shopifyDao;

    public List<ShopifyLicenseVo> getLicenseListByEmail(String email) throws Exception {
        logger.info(" @ Hello ShopifyService.getLicenseListByEmail ! ");
        List<ShopifyLicenseVo> shopifyLicenseVoList = shopifyDao.getLicenseListByEmail(email);

        return shopifyLicenseVoList;
    }

    public int insertShopifyLicense(ShopifyLicenseVo shopifyLicenseVo) throws Exception {
        logger.info(" @ Hello ShopifyService.insertShopifyLicense ! ");
        return shopifyDao.insertShopifyLicense(shopifyLicenseVo);
    }

    public int validateLicense(String license_key) throws Exception {
        logger.info(" @ Hello ShopifyService.validateLicense ! ");
        return shopifyDao.validateLicense(license_key);
    }

    public int checkUserLicense(ShopifyLicenseVo shopifyLicenseVo) throws Exception {
        logger.info(" @ Hello ShopifyService.checkUserLicense ! ");
        return shopifyDao.checkUserLicense(shopifyLicenseVo);
    }
}
