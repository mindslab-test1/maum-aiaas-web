package console.maum.ai.shopify.controller;

import console.maum.ai.common.data.CommonMsg;
import console.maum.ai.common.data.CommonResponse;
import console.maum.ai.shopify.model.ShopifyLicenseVo;
import console.maum.ai.shopify.service.ShopifyService;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Slf4j
@Controller
public class ShopifyApiController {

    private static final Logger logger = LoggerFactory.getLogger(ShopifyApiController.class);

    @Autowired
    private ShopifyService shopifyService;

    @RequestMapping(value = "/support/shopify/getLicenseList", method={RequestMethod.POST})
    @ResponseBody
    public List<ShopifyLicenseVo> getLicenseListByEmail(@RequestParam(value="email") String email) throws Exception {
        logger.info(" @ Hello ShopifyApiController.getLicenseListByEmail ! ");
        List<ShopifyLicenseVo> shopifyLicenseVoList = shopifyService.getLicenseListByEmail(email);

        return shopifyLicenseVoList;
    }

    @RequestMapping(value = "/support/shopify/insertLicense", method = {RequestMethod.POST})
    @ResponseBody
    public CommonResponse<Object> insertShopifyLicense(@RequestParam(value="email") String email,
                                                       @RequestParam(value="license_key") String licenseKey) throws Exception {
        logger.info(" @ Hello ShopifyApiController.insertShopifyLicense ! ");

        ShopifyLicenseVo shopifyLicenseVo = new ShopifyLicenseVo();
        shopifyLicenseVo.setEmail(email);
        shopifyLicenseVo.setLicense_key(licenseKey);

        CommonResponse<Object> resp = new CommonResponse<>();

        // 중복된 라이센스 키 등록 가능하도록 변경
        if (shopifyService.insertShopifyLicense(shopifyLicenseVo) > 0) {
            resp.setCode(CommonMsg.ERR_CODE_SUCCESS);
            resp.setMsg(CommonMsg.ERR_MSG_SUCCESS);
        } else {
            resp.setCode(CommonMsg.ERR_CODE_FAILURE);
            resp.setMsg(CommonMsg.ERR_MSG_FAILURE);
        }
        return  resp;
    }

    @RequestMapping(value = "/support/shopify/validateLicense", method = {RequestMethod.POST})
    @ResponseBody
    public CommonResponse<Object> validateLicense(@RequestParam(value="license_key") String license_key) throws Exception {
        logger.info(" @ Hello ShopifyApiController.validateLicense ! ");

        CommonResponse<Object> resp = new CommonResponse<>();

        if(shopifyService.validateLicense(license_key) > 0) {
            resp.setCode(CommonMsg.ERR_CODE_SUCCESS);
            resp.setMsg(CommonMsg.ERR_MSG_SUCCESS);
        } else {
            resp.setCode(CommonMsg.ERR_CODE_INVALID_LICENSE);
            resp.setMsg(CommonMsg.ERR_MSG_INVALID_LICENSE);
        }
        return  resp;
    }
}
