package console.maum.ai.shopify.dao;

import console.maum.ai.shopify.model.ShopifyLicenseVo;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class ShopifyDao {

    @Resource(name = "sqlSession")
    private SqlSession sqlSession;

    private static final String NAMESPACE = "console.maum.ai.model.shopifyMapper";

    /**
     * 사용자 이메일로 license 리스트 조회 - 2021. 04. 21 LYJ
     */
    public List<ShopifyLicenseVo> getLicenseListByEmail(String email) throws Exception {
        return sqlSession.selectList(NAMESPACE + ".getLicenseListByEmail", email);
    }

    /**
     * license 등록 - 2021. 04. 21 LYJ
     * */
    public int insertShopifyLicense(ShopifyLicenseVo shopifyLicenseVo) throws Exception {
        return sqlSession.insert(NAMESPACE + ".insertShopifyLicense", shopifyLicenseVo);
    }

    /**
     * license 유효성 체크 - 2021. 04. 23 LYJ
     * */
    public int validateLicense(String license_key) throws Exception {
        return sqlSession.selectOne(NAMESPACE + ".validateLicense", license_key);
    }

    /**
     * license 중복 등록 방지 - 2021. 04. 26 LYJ
     * */
    public int checkUserLicense(ShopifyLicenseVo shopifyLicenseVo) throws Exception {
        return sqlSession.selectOne(NAMESPACE + ".checkUserLicense", shopifyLicenseVo);
    }
}
