package console.maum.ai.shopify.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@EqualsAndHashCode
public class ShopifyLicenseVo {
    private String email;
    private String license_key;
    private String reg_date;
    private String mdf_date;
}
