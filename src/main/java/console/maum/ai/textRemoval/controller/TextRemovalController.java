package console.maum.ai.textRemoval.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

/*
 * 화면 Controller
 */

@Controller
@RequestMapping(value = "/cloudApi/textRemoval")
public class TextRemovalController {

	private static final Logger logger = LoggerFactory.getLogger(TextRemovalController.class);

	/**
	 * textRemoval Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krTextRemovalMain")
	public String krTextRemovalMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krTextRemovalMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "textRemovalMain");
		httpSession.setAttribute("menuName", "시각");
		httpSession.setAttribute("subMenuName", "텍스트 제거");
		httpSession.setAttribute("menuGrpName", "시각");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/textRemoval/textRemovalMain.pg";
		
		return returnUri;
	}
	
	/**
	 * textRemoval Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enTextRemovalMain")
	public String enTextRemovalMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enTextRemovalMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "textRemovalMain");
		httpSession.setAttribute("menuName", "Vision");
		httpSession.setAttribute("subMenuName", "Text Removal");
		httpSession.setAttribute("menuGrpName", "Vision");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/textRemoval/textRemovalMain.pg";
		
		return returnUri;
	}	

}