package console.maum.ai.textRemoval.controller;

import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.common.util.Util;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.textRemoval.service.TextRemovalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/*
 * API Controller
 */

@RestController
public class TextRemovalApiController {

    @Autowired
    private TextRemovalService textRemovalService;

    @RequestMapping(value = "/api/getTextRemoval")
    @ResponseBody
    public ResponseEntity<byte[]> getTextRemovalImage(@RequestParam(value = "file") MultipartFile imageFile, HttpServletRequest request){

        System.out.println(imageFile.getOriginalFilename());

        HttpSession httpSession= request.getSession(true);

        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getId();

        return textRemovalService.getApiTextRemoval(apiId, apiKey, imageFile , PropertyUtil.getUploadPath()+"/textRemoval/", savedId);
    }
	
}
