package console.maum.ai.hotelBot.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/hotelbot")

public class HotelBotController {

    private static final Logger logger = LoggerFactory.getLogger(HotelBotController.class);

    /**
     * HotelBot Main
     * @param model
     * @return
     */

    @RequestMapping(value = "/krHotelBot")
    public String krHotelBot(HttpServletRequest request, Model model) {
        logger.info("Welcome krHotelBot!");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "hotelbotMain");
        httpSession.setAttribute("menuName", "대화");
        httpSession.setAttribute("subMenuName", "호텔 컨시어지 챗봇");
        httpSession.setAttribute("menuGrpName", "대화");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/hotelBot/hotelBotMain.pg";

        return returnUri;
    }


    @RequestMapping(value = "/enHotelBot")
    public String enHotelBot(HttpServletRequest request, Model model) {
        logger.info("Welcome enHotelBot!");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "hotelbotMain");
        httpSession.setAttribute("menuName", "Conversation");
        httpSession.setAttribute("subMenuName", "Hotel Concierge Chatbot");
        httpSession.setAttribute("menuGrpName", "Conversation");

        model.addAttribute("lang", "en");
        String returnUri = "/en/hotelBot/hotelBotMain.pg";

        return returnUri;
    }
}
