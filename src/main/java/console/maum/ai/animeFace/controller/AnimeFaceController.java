package console.maum.ai.animeFace.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/animeFace")
public class AnimeFaceController {

    private static final Logger logger = LoggerFactory.getLogger(AnimeFaceController.class);

    @RequestMapping(value = "/krAnimeFaceMain")
    public String krAnimeFaceMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krAnimeFaceMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "animeFaceMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "얼굴 애니메이션 필터");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/animeFace/animeFaceMain.pg";

        return returnUri;
    }

    @RequestMapping(value = "/enAnimeFaceMain")
    public String enAnimeFaceMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enAnimeFaceMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "animeFaceMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Anime Face");
        httpSession.setAttribute("menuGrpName", "Vision");

        model.addAttribute("lang", "en");
        String returnUri = "/en/animeFace/animeFaceMain.pg";

        return returnUri;
    }
}
