package console.maum.ai.animeFace.controller;

import console.maum.ai.animeFace.service.AnimeFaceService;
import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.member.model.MemberVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Slf4j
@RestController
public class AnimeFaceApiController {

    @Autowired
    AnimeFaceService service;

    @PostMapping(value = "/api/animeFace/sample:make")
    @ResponseBody
    public ResponseEntity<byte[]> animeFaceApiSampleMake(HttpServletRequest request) throws Exception {

        log.debug(" @ Hello animeFaceApiSampleMake ! ");

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        return service.sampleMake(apiId, apiKey);
    }

    @PostMapping(value = "/api/animeFace/face:make")
    @ResponseBody
    public ResponseEntity<byte[]> animeFaceApiFaceMake(@RequestParam MultipartFile file,
                                                       @RequestParam int steps,
                                                       //@RequestParam(required = false) int steps,
                                                       HttpServletRequest request) throws Exception {

        log.debug(" @ Hello animeFaceApiFaceMake ! ");

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        return service.faceMake(apiId, apiKey, file, steps, PropertyUtil.getUploadPath() + "/animeFace/");
    }

    @PostMapping(value = "/api/animeFace/style:edit")
    @ResponseBody
    public ResponseEntity<byte[]> animeFaceApiStyleEdit(@RequestParam MultipartFile latent,
                                                        @RequestParam MultipartFile dict,
                                                        HttpServletRequest request) throws Exception {
        log.debug(" @ Hello animeFaceApiStyleEdit ! ");

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        return service.styleEdit(apiId, apiKey, latent, dict, PropertyUtil.getUploadPath() + "/animeFace/");
    }
}
