package console.maum.ai.animeFace.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})

@Slf4j
@Service
public class AnimeFaceService {

    @Value("${api.url}")
    private String mUrl_ApiServer;

    public ResponseEntity<byte[]> sampleMake(String apiId, String apiKey) throws Exception {
        String url = mUrl_ApiServer + "/anime/sample:make";

        String logMsg = "\n===========================================================================\n";
        logMsg += "AnimeFace sample:make API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += "===========================================================================";
        log.info(logMsg);

        JSONObject json = new JSONObject();
        json.put("apiId", apiId);
        json.put("apiKey", apiKey);

        try {
            CloseableHttpClient client = HttpClientBuilder.create().build();

            HttpPost post = new HttpPost(url);
            post.setHeader("Content-Type", "application/json");
            StringEntity entity = new StringEntity(json.toString(), "UTF-8");

            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            HttpEntity responseEntity = response.getEntity();
            HttpHeaders headers = new HttpHeaders();
            InputStream in = responseEntity.getContent();
            byte[] resultArray = IOUtils.toByteArray(in);

            ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(resultArray, headers, HttpStatus.OK);

            log.info("AnimeFace sample:make API @ Response OK");

            return resultEntity;

        } catch (Exception e) {
            log.error("API exception : {}", e.toString());
            e.printStackTrace();
        }
        return null;
    }

    public ResponseEntity<byte[]> faceMake(String apiId, String apiKey, MultipartFile file, int steps, String uploadPath) throws Exception {
        String url = mUrl_ApiServer + "/anime/face:make";

        String logMsg = "\n===========================================================================\n";
        logMsg += "AnimeFace face:make API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "image", file.getOriginalFilename());
        logMsg += String.format(":: %-10s = %s%n", "steps", steps);
        logMsg += "===========================================================================";
        log.info(logMsg);

        try {
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File varFile = new File(uploadPath);

            if (!varFile.exists()) {
                varFile.mkdirs();
            }

            varFile = new File((uploadPath + "/" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
            file.transferTo(varFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addBinaryBody("image", varFile, ContentType.create("image/*"), file.getOriginalFilename());
            builder.addPart("steps", new StringBody(String.valueOf(steps), ContentType.TEXT_PLAIN.withCharset("UTF-8")));

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            log.info("responseCode = {}", responseCode);

            if (responseCode != 200) {
                varFile.delete();
                log.error("API fail.");
                throw new RuntimeException(" @ AnimeFace API Face:make ErrCode : " + response);
            }

            HttpEntity responseEntity = response.getEntity();
            HttpHeaders headers = new HttpHeaders();
            InputStream in = responseEntity.getContent();
            byte[] imageArray = IOUtils.toByteArray(in);

            ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(imageArray, headers, HttpStatus.OK);

            varFile.delete();

            return resultEntity;

        } catch (Exception e) {
            log.error("API exception : {}", e.toString());
            e.printStackTrace();
        }

        return null;
    }

    public ResponseEntity<byte[]> styleEdit(String apiId, String apiKey, MultipartFile latent, MultipartFile dict, String uploadPath) throws Exception {
        String url = mUrl_ApiServer + "/anime/style:edit";

        String logMsg = "\n===========================================================================\n";
        logMsg += "AnimeFace style:edit API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "latent", latent.getOriginalFilename());
        logMsg += String.format(":: %-10s = %s%n", "dict", dict.getOriginalFilename());
        logMsg += "===========================================================================";
        log.info(logMsg);

        try {
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File varLatentFile = new File(uploadPath);
            File varDictFile = new File(uploadPath);

            if (!varLatentFile.exists()) {
                varLatentFile.mkdirs();
            }
            if (!varDictFile.exists()) {
                varDictFile.mkdirs();
            }


            varLatentFile = new File((uploadPath + "/" + latent.getOriginalFilename().substring(latent.getOriginalFilename().lastIndexOf("\\") + 1)));
            latent.transferTo(varLatentFile);
            FileBody latentFileBody = new FileBody(varLatentFile);

            varDictFile = new File((uploadPath + "/" + dict.getOriginalFilename().substring(dict.getOriginalFilename().lastIndexOf("\\") + 1)));
            dict.transferTo(varDictFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("latent", latentFileBody);
            builder.addBinaryBody("dict", varDictFile, ContentType.TEXT_PLAIN, dict.getOriginalFilename());

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            HttpEntity responseEntity = response.getEntity();
            int responseCode = response.getStatusLine().getStatusCode();
            log.info("AnimeFace responseCode = {}", responseCode);


            varLatentFile.delete();
            varDictFile.delete();

            if (responseCode != 200) {
                String responseString = "";
                if(responseEntity!=null) {
                    responseString = EntityUtils.toString(responseEntity, "UTF-8");
                }
                log.error("AnimeFace API fail : {}", responseString);
                return null;
//                throw new RuntimeException(" @ AnimeFace API style:edit ErrCode : " + response);
            }

            HttpHeaders headers = new HttpHeaders();
            InputStream in = responseEntity.getContent();
            byte[] imageArray = IOUtils.toByteArray(in);

            return new ResponseEntity<>(imageArray, headers, HttpStatus.OK);

        } catch (Exception e) {
            log.error("AnimeFace API exception : {}", e.toString());
            e.printStackTrace();
        }

        return null;
    }
}
