package console.maum.ai.batch.service;

import console.maum.ai.batch.dao.BatchDao;
import console.maum.ai.batch.model.AlarmItemVo;
import console.maum.ai.batch.model.AlarmLogVo;
import console.maum.ai.common.util.slack.SlackUtils;
import console.maum.ai.member.dao.ApiUsageDao;
import console.maum.ai.apiUsage.model.ApiUsageSttVo;
import console.maum.ai.member.model.ApiUsageVo;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

@Service
public class BatchService implements InitializingBean {

    private static final Logger logger = LoggerFactory.getLogger(BatchService.class);

    @Value("${api.url}")
    private String apiUrl;
    @Value("${api.host.engedu}")
    private String engeduUrl;
    @Value("${datatool.url}")
    private String dataUrl;
    @Value("${batch.failure.threshold}")
    private int failureThreshold;

    @Value("${admin.api.id}")
    private String apiId;
    @Value("${admin.api.pw}")
    private String apiKey;

    @Autowired
    BatchDao batchDao;

    @Autowired
    ApiUsageDao apiUsageDao;

    @Autowired
    ServletContext servletContext;

    @Override
    /* 서버 실행시 status 초기화 */
    public void afterPropertiesSet(){
        logger.info("--- Alarm server status initialize---");
        batchDao.initAlarmStatus();
    }


    public void callAPI(){
        ttsAPI();
        sttAPI();
    }


    /* 서버 상태 확인 후 변경됐을 시 Slack으로 알림 */
    private void statusChangeCheck(String category, String item, int curStatus){
        logger.debug("{} server - Response code : {}", item, curStatus);

        if(curStatus == 200) curStatus = 0;
        else curStatus = 1;

        AlarmItemVo alarmItemVo = batchDao.getAlarmItem(item);
        String msg = "";

        if(curStatus == 0){

            if(alarmItemVo.getStatus() == 1){ // status 1 -> 0 : 서버 정상화
                logger.info("서버 정상화 확인");

                // log update
                batchDao.updateLogClearedTime(alarmItemVo.getLogId());
                AlarmLogVo alarmLogVo = batchDao.getAlarmLog(alarmItemVo.getLogId());

                if(alarmItemVo.getFailCount() >= failureThreshold){ // failureThreshold 이상인 상태에서 해지된 경우 알림 발송
                    logger.info("장애 해지 알림!");
                    // select unresolved engine
                    List<String> failureList = batchDao.selectFailureEngineList(item);

                    msg += "> `장애 해지`   <!channel> \n"
                            +  "> 해지 장애 : *" + item+ "*\n"
                            +  "> 누적 장애 : *" + failureList + "*\n"
                            +  "> 발생 시각 : *" + alarmLogVo.getOccurredDateTime() + "*\n"
                            +  "> 해지 시각 : *" + alarmLogVo.getClearedDateTime() + "*\n";
                }

                // alarm update
                alarmItemVo.setStatus(0);
                alarmItemVo.setLogId(-1);
                alarmItemVo.setFailCount(0);

            }
            // update alarm item
            batchDao.updateAlarmItem(alarmItemVo);

        }
        else{ // curStatus == 1

            if(alarmItemVo.getStatus() == 0){ // status 0 -> 1 :  장애 발생
                logger.info("서버 문제 발생");

                // log insert
                AlarmLogVo alarmLogVo = new AlarmLogVo();
                alarmLogVo.setCategory(category);
                alarmLogVo.setItem(item);
                alarmLogVo.setStatus(curStatus);
                batchDao.insertAlarmLog(alarmLogVo);
                int logId = alarmLogVo.getLogId();

                // alarm update
                alarmItemVo.setStatus(1);
                alarmItemVo.setLogId(logId);
            }

            else{ // status 1 -> 1 :  장애 미해결
                logger.info("서버 장애 미해결..!");

                if(alarmItemVo.getFailCount()+1 == failureThreshold){
                    // select unresolved engine
                    List<String> failureList = batchDao.selectFailureEngineList(item);
                    AlarmLogVo alarmLogVo = batchDao.getAlarmLog(alarmItemVo.getLogId());

                    msg += "> `장애 발생`   <!channel> :fast_parrot:\n"
                            +  "> 신규 장애 : *" + item+ "*\n"
                            +  "> 누적 장애 : *" + failureList + "*\n"
                            +  "> 발생 시각 : *" + alarmLogVo.getOccurredDateTime() + "*\n"
                            +  "> 모듈 담당 : *" + alarmItemVo.getContact() + "*\n";
                }
            }

            alarmItemVo.setFailCount(alarmItemVo.getFailCount()+1); // failCount +1
            // update alarm item
            batchDao.updateAlarmItem(alarmItemVo);
        }

        logger.debug(msg);
        if(!msg.equals("")) {SlackUtils.sendSlack(msg);}

    }



    private void ttsAPI(){
        String category = "API";
        String item = "TTS";
        String url = apiUrl + "/tts/stream";
        String sampleTxt = "안녕하세요.";
        String[] ttsVoice = {"baseline_kor", "baseline_eng", "kor_kids_m", "kor_kids_f"};

        try{
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("text", new StringBody(sampleTxt, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("voiceName", new StringBody(ttsVoice[0], ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiKey",new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);

            statusChangeCheck(category, item, response.getStatusLine().getStatusCode());

        }catch (Exception e){
            logger.error("Batch API TTS ERR : {}", e);
        }
    }


    private void sttAPI(){
        String category = "API";
        String item = "STT";
        String url = apiUrl + "/api/stt/";
        String[] sttLang = {"kor", "eng"};
        String[] sttLevel = {"baseline", "address"};
        String[] sttSampleRate = {"16000", "8000"}; // kor, baseline일 때만 16000

        try {
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);

            File file = new File(servletContext.getRealPath("/aiaas/common/audio/apiTestFiles/STT_KOR.mp3"));
            FileBody fileBody = new FileBody(file);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("file", fileBody);
            builder.addPart("lang", new StringBody(sttLang[0], ContentType.MULTIPART_FORM_DATA));
            builder.addPart("level", new StringBody(sttLevel[0], ContentType.MULTIPART_FORM_DATA));
            builder.addPart("sampling", new StringBody(sttSampleRate[0], ContentType.MULTIPART_FORM_DATA));

            builder.addPart("ID", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("key", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("cmd", new StringBody("runFileStt", ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);

            statusChangeCheck(category, item, response.getStatusLine().getStatusCode());

        } catch (Exception e) {
            logger.error("Batch API STT ERR : {}", e);
        }


    }


    /* 삭제되지 않은 AuthStateInfo 데이터 제거 - 2020.04.08 YGE */
    public void deletePastUserAuthStateInfo(){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        String baseDate = sdf.format(cal.getTime());

        logger.debug(baseDate);

        batchDao.deletePastUserAuthStateInfo(baseDate);

    }

    /* API 서버에서 사용량 정보 받아와서 API_USAGE 테이블에 insert - 2020.12.24 LYJ */
    public int insertApiUsageList() {
        // 현재 API_USAGE 테이블의 가장 최신 데이터 받아오기
        ApiUsageVo lastDateApiUsage = batchDao.getLastDateApiUsage();

        // API 서버에서 데이터 조회
        List<ApiUsageVo> apiUsageVos = apiUsageDao.getApiUsage(lastDateApiUsage);

        int result = 0;
        for (ApiUsageVo apiUsageVo : apiUsageVos) {
            int success = batchDao.insertApiUsageList(apiUsageVo);
            if (success > 0) {
                result++;
            } else {
                logger.error("insert Error!");
                return 0;
            }
        }
        return result;  // 최종적으로 insert된 record 개수 return
    }

    /* api.maum.ai 서버에서 사용량 정보 받아와서 API_USAGE 테이블에 insert - 2021.01.12 LYJ */
    public int insertSttApiUsageList() {
        // 현재 API_USAGE 테이블의 가장 최신 STT 사용량 데이터 받아오기
        ApiUsageVo lastDateSttApiUsage = batchDao.getLastDateSttApiUsage();

        ApiUsageSttVo requestVo = new ApiUsageSttVo();
        requestVo.setClient_id(lastDateSttApiUsage.getApiId());
        requestVo.setUpdate_date(lastDateSttApiUsage.getDate());

        // API 서버에서 STT 데이터 조회
        List<ApiUsageSttVo> apiUsageSttVos = apiUsageDao.getLastDateSttApiUsage(requestVo);

        int result = 0;
        for(ApiUsageSttVo apiUsageSttVo : apiUsageSttVos) {

            ApiUsageVo apiUsageVo = new ApiUsageVo();

            apiUsageVo.setService("STT");
            apiUsageVo.setApiId(apiUsageSttVo.getClient_id());
            apiUsageVo.setDate(apiUsageSttVo.getUpdate_date());
            apiUsageVo.setUsage(apiUsageSttVo.getStt_length());
            apiUsageVo.setUserIp(null);

            int success = batchDao.insertApiUsageList(apiUsageVo);
            if (success > 0) {
                result++;
            } else {
                logger.error("insert Error!");
                return 0;
            }
        }
        return result;  // 최종적으로 insert된 record 개수 return
    }
}
