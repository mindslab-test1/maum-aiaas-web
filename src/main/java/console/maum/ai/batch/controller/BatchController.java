package console.maum.ai.batch.controller;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import console.maum.ai.admin.apikey.service.ApiKeyService;
import console.maum.ai.batch.service.BatchService;
import console.maum.ai.member.constant.UserStatus;
import console.maum.ai.member.model.MemberForm;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.member.service.MemberService;
import console.maum.ai.support.model.MAUForm;
import console.maum.ai.support.service.SupportService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import console.maum.ai.common.util.MailSender;
import console.maum.ai.payment.model.PaymentVo;
import console.maum.ai.payment.service.PaymentService;

/**
 * BatchController
 */

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",
		"classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@Controller
public class BatchController {
	@Value("${batch.status}")
	private String mBatchStatus;

	@Value("${batch.alive_check}")
	private int mFlag_AliveCheck;

	@Autowired
	private PaymentService paymentService;
	
	@Autowired
	private MailSender mailSender;

	@Autowired
	private MemberService memberService;

	@Autowired
	private ApiKeyService apiKeyService;

	@Autowired
	private SupportService supportService;

	@Autowired
    private BatchService batchService;

	private static final Logger logger = LoggerFactory.getLogger(BatchController.class);

	/*
	 * routineBill 주기 : 매일 12시
	 * Modified by LYJ - 결제 방법에 따른 분기 추가 - 2019. 10. 19
	 */
	@Scheduled(cron = "0 0 12 * * ?")
	public void routineBill() {
		if("start".equals(mBatchStatus)) {
			logger.info("Batch schedule(routineBill) start!!");
			
			List <PaymentVo> RegularList = paymentService.selectRegularList();

			for(PaymentVo paymentVo : RegularList) {
				logger.debug(paymentVo.toString());
				try {

					String paymentMethodStr = paymentService.getPaymentMethod(paymentVo.getUserNo());

					if( "Card".equalsIgnoreCase(paymentMethodStr) ) {
						paymentService.routineBill(paymentVo);
					} else if ( "Cash".equalsIgnoreCase(paymentMethodStr) ) {
						/* N/A */
					} else if ( "PayPal".equalsIgnoreCase(paymentMethodStr) ) {
						/* N/A */
					} else {
						/* N/A */
					}

				} catch(Exception e) {
					logger.error(e.getMessage());
				}
			}
		}else{
			logger.info("Batch schedule(routineBill) stop!!");
		}

	}

	/*
	 * Batch MailSend 주기 : 5분마다 (기존에는 1시간이었음)
	 * 수정 일시 : 2020. 07. 30
	 */
	@Scheduled(cron = "0 0/5 * * * ?")
	public void batchMailSend() {
		if("start".equals(mBatchStatus)) {
			logger.info("Batch schedule(batchMailSend) start!!");
			
			try {
				mailSender.batchMailSend();

			} catch (Exception e) {
				logger.info("Batch schedule(batchMailSend) Exception!!");	
			}	
		}else{
			//logger.info("Batch schedule(batchMailSend) stop!!");
		}
	}		

	/*
	 * UNSUBSCRIBING(3) 사용자 구독 해지 - 주기 : 1시간마다
	 * 19.09.25 이예준
	 * INIT-> UNSUBSCRIBING인 사용자에 대한 처리 추가 - 2021.03.18 YGE
 	 */
	@Scheduled(cron = "0 0 0/1 * * ?")
	public void batchUnsubscribe() {
		if ("start".equals(mBatchStatus)) {
			String logTit = "[BatchController/batchUnsubscribe()]";
			logger.info(logTit + "start!!");

			try {

				MemberForm memberForm = new MemberForm();
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

				// 현재 날짜
				Date dDate = new Date();
				String sCurrentDate1 = dateFormat.format(dDate);
				Date currentDate = dateFormat.parse(sCurrentDate1);

				// UNSUBSCRIBING 사용자 조회
				List<MemberVo> cancelReqUsers = memberService.getCancelReqUser();

				for (MemberVo user : cancelReqUsers) {

					String paymentMethodStr = paymentService.getPaymentMethod(user.getUserno());
					memberForm.setUserNo(user.getUserno());
					memberForm.setStatus(UserStatus.UNSUBSCRIBED);

					// INIT -> UNSUBSCRIBING인 경우 (billing 없음)
					if(paymentMethodStr == null){

						Calendar cal = Calendar.getInstance();
						String userExpireDate = memberService.getUserCreateDate(user.getUserno());
						Date expireDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.sss").parse(userExpireDate);
						cal.setTime(expireDate);
						cal.add(Calendar.MONTH, 1);
						expireDate = cal.getTime();

						logger.info(logTit + "===================INIT -> UNSUBSCRIBING====================");
						//만료 날짜, 현재 날짜 비교 (만료 날짜가 지났을 경우 해지)
						if (currentDate.compareTo(expireDate) > 0) {
							logger.info(logTit + " ExpUser = email: {}, userNo = {}, expireDate : {}", user.getEmail(), user.getUserno(), expireDate);
							if (user.getApiId() != null) {
								Map<String, String> apiResult = apiKeyService.pauseApiId(user.getEmail());    // API pause
								if(apiResult.get("SUCCESS").equals("1")){
									logger.info("{} pauseApiId({}) success", logTit, user.getApiId());
									memberService.updateUserStatus(memberForm);        // Status 값 변경
									mailSender.sendExpiredUserMail(user.getEmail()); // 구독해지 메일 전송
									logger.info(logTit + " update " + user.getEmail() + " to UNSUBSCRIBED success");
								}else{
									logger.error(logTit + " " + apiResult.get("MESSAGE"));
								}
							}
						}
					}
					// FREE/SUBSCRIBED -> UNSUBSCRIBING인 경우 (billing 있음)
					else if ("Card".equalsIgnoreCase(paymentMethodStr) || "PayPal".equalsIgnoreCase(paymentMethodStr)) {

						// 만료 날짜
						String userExpireDate = paymentService.getUserExpirationDate(memberForm);
						Date expireDate = dateFormat.parse(userExpireDate);

						logger.info(logTit + "===================FREE/SUBSCRIBED -> UNSUBSCRIBING====================");
						//만료 날짜, 현재 날짜 비교 (만료 날짜가 지났을 경우 해지)
						if (currentDate.compareTo(expireDate) > 0) {
							logger.info(logTit + " ExpUser = email: {}, userNo = {}, expireDate : {}", user.getEmail(), user.getUserno(), expireDate);
							if (user.getApiId() != null) {
								Map<String, String> apiResult = apiKeyService.pauseApiId(user.getEmail());    // API pause
								if(apiResult.get("SUCCESS").equals("1")){
									logger.info("{} pauseApiId({}) success", logTit, user.getApiId());
									memberService.updateUserStatus(memberForm);           // Status 값 변경
									paymentService.planCancelProcedure(user.getUserno()); // 프로시저를 이용해 정기결제 해지
									paymentService.deleteBillInfo(user.getUserno());      // 사용자의 결제 정보 삭제

									// 구독해지 메일 전송
									switch (paymentMethodStr){
										case "Card":
											mailSender.sendExpiredUserMail(user.getEmail()); break;
										case "PayPal":
											mailSender.enSendExpiredUserMail(user.getEmail()); break;
									}
									logger.info(logTit + " update " + user.getEmail() + " to UNSUBSCRIBED success");
								}else{
									logger.error(logTit + " " + apiResult.get("MESSAGE"));
								}
							}
						}
					}
//					else if ("Cash".equalsIgnoreCase(paymentMethodStr)) {
//						/* N/A */
//					}


				}
			} catch (Exception e) {
				logger.info(logTit + " Exception :: " + e.getMessage());
				e.printStackTrace();
			}
		} else {
			logger.info("Batch schedule(batchUnsubscribe) stop!!");
		}
	}

    /*
     * Batch 5일동안 미결제 사용자에 대한 해지 - 주기 : 12시간마다
     * 20.01.30 이예준
     * Modified 20.03.02 by LYJ : 5일이상 미결제한 사용자 분류하는 로직 이슈
     */
	@Scheduled(cron = "0 0 0/12 * * ?")
	public void batchUnpaymentUser() {
		if ("start".equals(mBatchStatus)) {
			logger.info("Batch schedule (batchUnpaymentUser) start!!");

			try {
				List<PaymentVo> unpaymentUsers = paymentService.getUnpaymentVoList();

				for (PaymentVo user : unpaymentUsers) {
					apiKeyService.pauseApiId(user.getUserEmail());    // 구독 pause
					logger.info("pauseId : " + user.getUserEmail() + "");

					MemberForm memberForm = new MemberForm();
					memberForm.setUserNo(user.getUserNo());
					memberForm.setStatus(UserStatus.UNSUBSCRIBED);
					memberService.updateUserStatus(memberForm);

					paymentService.deletePlanCancelUser(user.getUserNo());

					logger.info(" @ batchUnpaymentUser : " + user.getUserEmail() + " ==> UNSUBSCRIBED & pause_API & deleteBillingInfo");

					mailSender.sendExpiredUserMail(user.getUserEmail()); // 사용자에게 메일 알림
					logger.info(" @ batchUnpaymentUser : sendExpiredUserMail ==> " + user.getUserEmail());
				}
			} catch (Exception e) {
				logger.info("Batch schedule (batchUnpaymentUser) Exception!!" + e);
				e.printStackTrace();
			}
		} else {
			logger.info("Batch schedule (batchUnpaymentUser) stop!!");
		}
	}


	@Scheduled(cron = "0 50 23 * * ?")
	public void addMAU() throws Exception{

		if(mFlag_AliveCheck == 0) return; // 이중화 서버에서 중복되지 않도록!

		final String DATE_PATTERN = "yyyy-MM-dd";

		SimpleDateFormat sdf = new SimpleDateFormat(DATE_PATTERN);

		Date currentDate = new Date();
		String current = sdf.format(currentDate);

		MAUForm mauForm =  new MAUForm();

		mauForm.setCategory("User");
		mauForm.setItem("MAU");
		mauForm.setPeriod("1day");
		mauForm.setRegDateTime(current);
		mauForm.setValue(supportService.getTodayMAU());
		mauForm.setType("count");

		supportService.insertMAU(mauForm);
	}

	/* 10분마다 배치로 서버 상태 체크 후 상태 변경시 Slack
	*  - 2020.01.22 - YGE
	*/
	@Scheduled(cron = "0 0/1 * * * *")
	public void checkApiServerAlive(){
		/*
		** 차후엔 Batch 전체 동작 여부에 대한 설정도 필요.
		*/

		if(mFlag_AliveCheck == 0) return;

		batchService.callAPI();
	}


	/* 지난 AuthStateInfo 데이터 삭제 - 2020.04.08 - YGE */
	@Scheduled(cron = "0 0 2 * * ?")
	public void deletePastUserAuthStateInfo(){

		batchService.deletePastUserAuthStateInfo();

	}

	/* API 서버 사용량 정보 받아오기 - 2020.12.28 - LYJ */
/*	@Scheduled(cron = "0 0 4 * * ?")
	public void insertApiUsageList() {
		if ("start".equals(mBatchStatus)) {
			logger.info("Batch schedule(insertApiUsageList) start ! ");

			int result = batchService.insertApiUsageList();

			if(result > 1)
				logger.info("Batch schedule(insertApiUsageList) complete ! ");
			else
				logger.error("Batch schedule(insertApiUsageList) error ! ");
		}
	}*/

	/* api.maum.ai 서버 STT 사용량 정보 받아오기 - 2021.01.12 - LYJ */
/*	@Scheduled(cron = "0 30 4 * * ?")
	public void insertSttApiUsageList() {
		if ("start".equals(mBatchStatus)) {
			logger.info("Batch schedule(insertSttApiUsageList) start ! ");

			int result = batchService.insertSttApiUsageList();

			if(result > 1)
				logger.info("Batch schedule(insertSttApiUsageList) complete ! ");
			else
				logger.error("Batch schedule(insertSttApiUsageList) error ! ");
		}
	}*/
}
