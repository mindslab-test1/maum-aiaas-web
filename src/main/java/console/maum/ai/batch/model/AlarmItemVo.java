package console.maum.ai.batch.model;

import lombok.Data;

@Data
public class AlarmItemVo {
    private String category;
    private String item;
    private String contact;
    private int status;
    private String createDateTime;
    private String updateDateTime;
    private int logId;
    private int failCount;

}
