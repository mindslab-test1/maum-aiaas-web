package console.maum.ai.batch.model;

import lombok.Data;

@Data
public class AlarmLogVo {
    private int logId;
    private String category;
    private String item;
    private int status;
    private String occurredDateTime;
    private String clearedDateTime;
}
