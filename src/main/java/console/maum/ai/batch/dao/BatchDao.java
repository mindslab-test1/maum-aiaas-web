package console.maum.ai.batch.dao;

import console.maum.ai.batch.model.AlarmItemVo;
import console.maum.ai.batch.model.AlarmLogVo;
import console.maum.ai.member.model.ApiUsageVo;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class BatchDao {

    @Resource(name="sqlSession")
    private SqlSession sqlSession;

    private static final String NAMESAPCE = "console.maum.ai.model.batchMapper";

    /* alarm 데이터 init - 2020.01.22 - YGE */
    public void initAlarmStatus(){ sqlSession.update(NAMESAPCE + ".initAlarmStatus"); }

    /* alarm 데이터 select - 2020.01.21 - YGE */
    public AlarmItemVo getAlarmItem(String item){
        return sqlSession.selectOne(NAMESAPCE+".getAlarmItem", item);
    }

    /* alarm 데이터 update - 2020.01.21 - YGE */
    public void updateAlarmItem(AlarmItemVo alarmItemVo){ sqlSession.update(NAMESAPCE + ".updateAlarmItem", alarmItemVo); }

    /* alarmLog 데이터 select - 2020.01.21 - YGE */
    public AlarmLogVo getAlarmLog(int logId){
        return sqlSession.selectOne(NAMESAPCE + ".getAlarmLog", logId);
    }

    /* alarmLog insert - 2020.01.21 - YGE */
    public int insertAlarmLog(AlarmLogVo alarmLogVo){ return sqlSession.insert(NAMESAPCE+".insertAlarmLog", alarmLogVo); }

    /* alarmLog update - 2020.01.21 - YGE */
    public void updateLogClearedTime(int logId){
        sqlSession.update(NAMESAPCE+".updateLogClearedTime", logId);
    }

    /* 누적 장애 엔진 조회 - 2020.03.24 - YGE */
    public List<String> selectFailureEngineList(String item)	{	return sqlSession.selectList(NAMESAPCE + ".selectFailureEngineList", item);	}

    /* 삭제되지 않은 AuthStateInfo 데이터 제거 - 2020.04.08 YGE */
    public void deletePastUserAuthStateInfo(String baseDate){ sqlSession.delete(NAMESAPCE + ".deletePastUserAuthStateInfo", baseDate); }

    /* 현재 API_USAGE 테이블의 가장 최신 데이터 받아오기 - 2020.12.24 LYJ */
    public ApiUsageVo getLastDateApiUsage() {
        return sqlSession.selectOne(NAMESAPCE + ".getLastDateApiUsage");
    }

    /* 현재 API_USAGE 테이블의 가장 최신 STT 사용 데이터 받아오기 - 2021.01.12 LYJ */
    public ApiUsageVo getLastDateSttApiUsage() {
        return sqlSession.selectOne(NAMESAPCE + ".getLastDateSttApiUsage");
    }

    /* 사용량 정보 insert - 2020.12.24 LYJ */
    public int insertApiUsageList(ApiUsageVo apiUsageVo) {
        return sqlSession.insert(NAMESAPCE + ".insertApiUsageList", apiUsageVo);
    };
}
