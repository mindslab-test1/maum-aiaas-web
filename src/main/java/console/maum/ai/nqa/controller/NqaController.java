package console.maum.ai.nqa.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/cloudApi/nqa")
public class NqaController {

	private static final Logger logger = LoggerFactory.getLogger(NqaController.class);
	
	/**
	 * nqa Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krNqaMain")
	public String krNqaMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krNqaMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "nqaMain");
		httpSession.setAttribute("menuName", "대화");
		httpSession.setAttribute("subMenuName", "금융봇");
		httpSession.setAttribute("menuGrpName", "대화");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/nqa/nqaMain.pg";	
		
		return returnUri;
	}
	
	/**
	 * nqa Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enNqaMain")
	public String enNqaMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enNqaMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "nqaMain");
		httpSession.setAttribute("menuName", "Conversation");
		httpSession.setAttribute("subMenuName", "NQA Bot");
		httpSession.setAttribute("menuGrpName", "Conversation");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/nqa/nqaMain.pg";
		
		return returnUri;
	}	

}
