package console.maum.ai.oauth.model;

import lombok.Data;

import java.io.Serializable;

@Data
public class AuthToken implements Serializable {

    private String accessToken;
    private String accessTokenExpTime;
    private String refreshToken;
    private String refreshTokenExpTime;
}
