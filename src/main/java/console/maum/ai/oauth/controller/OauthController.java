package console.maum.ai.oauth.controller;

import console.maum.ai.oauth.service.OauthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping(value="/oauth")
public class OauthController {

    private static final Logger logger = LoggerFactory.getLogger(OauthController.class);

    @Autowired
    private OauthService oauthService;

    /* SSO 로그인 요청에 대한 callback을 받는 부분 -- 2020.03.27 YGE */
    @RequestMapping(value="/callback")
    public String oAuthCallback(@RequestParam(value="code") String code,
                              @RequestParam(value="state") String state,
                                HttpServletRequest request) throws Exception {

        logger.info("CODE : {}", code);
        logger.info("STATE : {}", state);

        return oauthService.getAccessToken(code, state, request);
    }


}
