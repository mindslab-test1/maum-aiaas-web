package console.maum.ai.oauth.service;


import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import console.maum.ai.login.model.RedirectStateVo;
import console.maum.ai.login.service.LoginService;
import console.maum.ai.member.model.MemberSecurityVo;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.member.service.MemberDetailsService;
import console.maum.ai.member.service.MemberService;
import console.maum.ai.oauth.model.AuthToken;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Service
public class OauthService {

    private static final Logger logger = LoggerFactory.getLogger(OauthService.class);


    @Autowired
    private MemberDetailsService memberDetailsService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private LoginService loginService;

    @Value("${hq.URL}")
    private String hqUrl;
    @Value("${hq.client_id}")
    private String hqClientId;
    @Value("${hq.redirect_uri}")
    private String hqRedirectUrl;
    @Value("${hq.tokenReqPath}")
    private String hqTokenReq;
    @Value("${hq.logoutPath}")
    private String hqCleanToken;
    @Value("${hq.logout_redirect_url}")
    private String logoutRedirectUrl;


    private String backToLoginForm = "redirect:/";


    /* AccessToken 요청 및 사용자 email 받기 + security login 수행 -- 2020.03.27 YGE */
    public String getAccessToken(String code, String state, HttpServletRequest request){

        String url = hqUrl + hqTokenReq;
        logger.info("getAccessToken() --> URL : {}", url);

        HttpClient client = HttpClients.createDefault();
        HttpPost post = new HttpPost(url);
        int responseCode;
        JsonElement responseJson;

        try{
            RedirectStateVo stateVO = loginService.getRedirectState(state);

            List<NameValuePair> nameValuePairs = new ArrayList<>();
            nameValuePairs.add(new BasicNameValuePair("grant_type", "authorization_code"));
            nameValuePairs.add(new BasicNameValuePair("code", code));
            nameValuePairs.add(new BasicNameValuePair("redirect_uri", hqRedirectUrl));

            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = client.execute(post);
            responseCode = response.getStatusLine().getStatusCode();

            logger.info("Response code : {}", responseCode);

            if (responseCode == 200) {

                responseJson = readJsonResponse(response);

                String userEmail = responseJson.getAsJsonObject().get("email").getAsString();

                if(userEmail == null || userEmail.equals("")){
                    logger.error("로그인 : getAccessToken() -->  resp에 email 없음");
                    return backToLoginForm;
                }

                // security 로그인 후 url return
                if(securityLogin(responseJson, request).equals("SUCCESS")){
                    loginService.deleteState(state);

                    return "redirect:" + stateVO.getTargetUrl();

                }
                else
                    return backToLoginForm;

            }

            // TODO : flow 그려지면 response 에러시 처리 수정
            else { // response error
                logger.error("로그인 : getAccessToken() --> response error ");
                return backToLoginForm;
            }

        }catch (Exception e){
            logger.error("로그인 : getAccessToken() --> Exception 발생 ");
            logger.error(e.toString());
            return backToLoginForm;
        }
    }


    /*
    security login & token setting -- 2020.03.27 YGE
    */
    private String securityLogin(JsonElement responseJson, HttpServletRequest request){

        logger.info("------------------ SECURITY START -----------------------");

        String email = responseJson.getAsJsonObject().get("email").getAsString();

        try{

            MemberVo memberVo = memberService.getLogInMemberDetail(email);

            // Token 세팅
            AuthToken token = new AuthToken();
            token.setAccessToken(responseJson.getAsJsonObject().get("access_token").getAsString());
            token.setAccessTokenExpTime(responseJson.getAsJsonObject().get("access_expire_time").getAsString());
            token.setRefreshToken(responseJson.getAsJsonObject().get("refresh_token").getAsString());
            token.setRefreshTokenExpTime(responseJson.getAsJsonObject().get("refresh_expire_time").getAsString());

            // security login
            MemberSecurityVo user = (MemberSecurityVo) memberDetailsService.loadUserByUsername(email);
            SecurityContext sc = SecurityContextHolder.getContext();
            sc.setAuthentication(new UsernamePasswordAuthenticationToken(email, "", user.getAuthorities()));

            HttpSession session = request.getSession();
            session.setAttribute("accessUser", memberVo);
            session.setAttribute("token", token);
            session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, sc);

            return "SUCCESS";

        }catch (Exception e){
            logger.error("---------- SECURITY 로그인 실패 ------");
            logger.error(e.toString());
            return "FAIL";
        }
    }


    /* refreshToken 요청 : TokenAuthInterceptor 에서 사용 -- 2020.03.27 YGE */
    public JsonElement getAccessTokenWithRefresh(String refreshToken){

        String url = hqUrl + hqTokenReq;
        logger.debug("getAccessToken() --> URL : {}", url);
        HttpClient client = HttpClients.createDefault();
        HttpPost post = new HttpPost(url);

        List<NameValuePair> nameValuePairs = new ArrayList<>();
        nameValuePairs.add(new BasicNameValuePair("grant_type", "refresh_token"));
        nameValuePairs.add(new BasicNameValuePair("refresh_token", refreshToken));

        try {
            post.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();

            if(responseCode == 200){
                return readJsonResponse(response);
            }else
                return null;

        } catch (Exception e) {
            logger.error("getAccessTokenWithRefresh() Exception" + e);
            return null;
        }

    }

    /* 토큰 삭제 URL 생성 : LogoutController에서 사용 - 2020.08.04 LYJ */
    public String makeDeleteTokenUrl(HttpServletRequest servletRequest){

        HttpSession session = servletRequest.getSession();
        MemberVo memberVo = (MemberVo) session.getAttribute("accessUser");
        AuthToken token = (AuthToken) session.getAttribute("token");

        String url = hqUrl + hqCleanToken;
        // hqUrl = https://hq-dev.maum.ai:10080
        // hqCleanToken = /hq/oauth/deleteToken

        String params = "?client_id=" + hqClientId + "&access_token=" + token.getAccessToken();
        // hqClientId = maum.ai-dev
        // hqCleanToken = /oauth/cleanToken

        String resultUrl = url + params;
        /* https://hq-dev.maum.ai:10080/hq/oauth/deleteToken?client_id=maum.ai-dev&access_token=accessToken */

        logger.debug("user : {}, makeUrlAfterDeleteToken : {}", memberVo.getEmail(), resultUrl);

        return resultUrl;
    }

    public String deleteJwt(HttpServletRequest request, HttpServletResponse response) {

        String params = "?client_id=" + hqClientId + "&returnUrl=" + logoutRedirectUrl;

        String url = hqUrl + hqCleanToken + params;

        return url;
    }


    private JsonElement readJsonResponse(HttpResponse response) throws IOException {
        BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));

        StringBuilder result = new StringBuilder();
        String line = "";
        while ((line = rd.readLine()) != null) {
            result.append(line);
        }
        logger.info("Response Result : {}", result);

        JsonParser jsonParser = new JsonParser();
        return jsonParser.parse(result.toString());
    }


    private String setLang(String locale){
        if("ko_KR".equals(locale) || "ko".equals(locale)) return "kr";
        else return "en";
    }

}
