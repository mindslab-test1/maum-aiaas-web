package console.maum.ai.styleComment.controller;

import console.maum.ai.admin.apikey.controller.ApiKeyApiController;
import console.maum.ai.styleComment.service.styleCommentService;
import console.maum.ai.member.model.MemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class styleCommentApiController {
    private final static Logger logger = LoggerFactory.getLogger(ApiKeyApiController.class);

    @Autowired
    private console.maum.ai.styleComment.service.styleCommentService styleCommentService;

    //    @PostMapping(value="/api/airDetector")
    @PostMapping(value="https://182.162.19.14:9941/styleComment")
    @ResponseBody
    public ResponseEntity<byte[]> styleCommentApi(@RequestParam(value = "file") MultipartFile file, HttpServletRequest request){
        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getId();


        return styleCommentService.runstyleComment(apiId, apiKey, file, "https://182.162.19.14:9941/styleComment" , savedId);

    }
}
