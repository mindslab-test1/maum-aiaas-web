package console.maum.ai.styleComment.controller;

import console.maum.ai.admin.apikey.controller.ApiKeyApiController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/*
 * 화면 Controller
 */

//MVC 주요 어노테이션 : @Controller, @RequestMapping, @RequestParam, @ModelAttribute, @SessionAttributes, @RequestPart, @CommandMap, @ControllerAdvice
@Controller
@RequestMapping(value = "/cloudApi/styleComment")

public class styleCommentController {
    private static final Logger logger = LoggerFactory.getLogger(ApiKeyApiController.class);

    /**
     * styleComment Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/krstyleCommentMain")
    public String krstyleCommentMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krstyleCommentMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "styleCommentMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "고령자 소지품 검출");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/styleComment/styleCommentMain.pg";

        return returnUri;
    }
}
