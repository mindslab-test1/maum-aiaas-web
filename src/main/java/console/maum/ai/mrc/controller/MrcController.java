package console.maum.ai.mrc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/cloudApi/mrc")
public class MrcController {

	private static final Logger logger = LoggerFactory.getLogger(MrcController.class);

	/**
	 * mrc Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krMrcMain")
	public String krMrcMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krMrcMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "mrcMain");
		httpSession.setAttribute("menuName", "언어");
		httpSession.setAttribute("subMenuName", "AI 독해");
		httpSession.setAttribute("menuGrpName", "언어");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/mrc/mrcMainNew.pg";
		
		return returnUri;
	}
	
	/**
	 * mrc Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enMrcMain")
	public String enMrcMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enMrcMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "mrcMain");
		httpSession.setAttribute("menuName", "Languages");
		httpSession.setAttribute("subMenuName", "Machine RC");
		httpSession.setAttribute("menuGrpName", "Languages");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/mrc/mrcMainNew.pg";
		
		return returnUri;
	}	

}