package console.maum.ai.voicealbum;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/voiceAlbum")
public class VoiceAlbumController {

    private static final Logger logger = LoggerFactory.getLogger(VoiceAlbumController.class);

    /**
     * voiceAlbum Main
     * @param model
     * @return
     */

    @RequestMapping(value = "/krVoiceAlbum")
    public String krVoiceAlbumMain(HttpServletRequest request, Model model){
        logger.info("Welcome krVoiceAlbumMain");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("menuName", "Voice Album");
        model.addAttribute("lang","ko");
        String returnUri = "noLayout/kr/voiceAlbum/voiceAlbumMain.pg";

        return returnUri;


    }

}
