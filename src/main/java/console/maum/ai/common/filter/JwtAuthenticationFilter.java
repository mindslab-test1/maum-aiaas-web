package console.maum.ai.common.filter;


import console.maum.ai.common.sso.SSOExternalAPI;
import console.maum.ai.member.model.MemberSecurityVo;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.member.service.MemberDetailsService;
import console.maum.ai.member.service.MemberService;
import io.jsonwebtoken.*;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.context.HttpSessionSecurityContextRepository;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
public class JwtAuthenticationFilter extends OncePerRequestFilter {

    private final SSOExternalAPI ssoExternalAPI;

    @Autowired
    private MemberService memberService;

    @Autowired
    private MemberDetailsService memberDetailsService;

    @Value("${url.domain}")
    private String myDomain;

    @Value("${maum.SigningKey}")
    private String singKey;

    @Value("${hq.URL}")
    private String ssoUrl;

    @Value("${hq.client_id}")
    private String clientId;

    @Value("${hq.logoutPath}")
    private String logoutPath;

    @SneakyThrows
    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

        HttpSession session = httpServletRequest.getSession(true);
        MemberVo memberVo = (MemberVo) session.getAttribute("accessUser");

        if (memberVo == null || SecurityContextHolder.getContext().getAuthentication() == null) {
            /* --------------------------------- 세션 미존재 --------------------------------- */
            Map<String, String> tokenMap = getCookie(httpServletRequest, httpServletResponse);
            if (tokenMap == null) {                                                                 // 최초 접속 시,
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            }
            String accessToken = tokenMap.get("accessToken");
            String refreshToken = tokenMap.get("refreshToken");

            if (accessToken == null) {
                if(refreshToken != null) {                              // 세션 X, accessToken X, refreshToken O
                    if(validateToken(refreshToken)) {
                        ssoExternalAPI.refreshJwtAccessToken(refreshToken, httpServletResponse);
                        setSessionAccessUser(refreshToken, httpServletRequest);
                    } else {
                        expireSessionAndLogout(httpServletRequest, httpServletResponse);
                    }
                } else {                                                // 세션 X, accessToken X, refreshToken X
                    // 정상적 현상
                }
            }

            if (accessToken != null) {
                if(refreshToken != null) {                              // 세션 X, accessToken O, refreshToken O
                    if (validateToken(accessToken) && validateToken(refreshToken)) {
                        setSessionAccessUser(refreshToken, httpServletRequest);
                    }
                } else {                                                // 세션 X, accessToken O, refreshToken X
                    log.warn(" @ JwtAuthenticationFilter: no session, no refreshToken");
                    expireSessionAndLogout(httpServletRequest, httpServletResponse);
                }
            }
            filterChain.doFilter(httpServletRequest, httpServletResponse);

        } else {
            /* --------------------------------- 세션 존재 --------------------------------- */
            Map<String, String> tokenMap = getCookie(httpServletRequest, httpServletResponse);
            if (tokenMap == null) {
                filterChain.doFilter(httpServletRequest, httpServletResponse);
            }
            String accessToken = tokenMap.get("accessToken");
            String refreshToken = tokenMap.get("refreshToken");

            if (accessToken == null) {
                if(refreshToken != null) {                              // 세션 O, accessToken X, refreshToken O
                    ssoExternalAPI.refreshJwtAccessToken(refreshToken, httpServletResponse);
                } else {                                                // 세션 O, accessToken X, refreshToken X
                    session.removeAttribute("accessUser"); // 기존 세션 만료
                    httpServletResponse.sendRedirect(reqLogout());
                }
            }

            if (accessToken != null) {
                if(refreshToken != null) {                              // 세션 O, accessToken O, refreshToken O
                    if (validateToken(accessToken) && validateToken(refreshToken)) {
                        // 정상적 현상
                    }
                } else {                                                // 세션 O, accessToken O, refreshToken X
                    expireSessionAndLogout(httpServletRequest, httpServletResponse);
                }
            }
            filterChain.doFilter(httpServletRequest, httpServletResponse);
        }
    }

    // Token 기간 만료 확인
    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(singKey).parseClaimsJws(authToken).getBody();
            return true;
        } catch (SignatureException ex) {
            log.error("Invalid JWT signature");
        } catch (MalformedJwtException ex) {
            log.error("Invalid JWT token");
        } catch (ExpiredJwtException ex) {
            log.error("Expired JWT token");
        } catch (UnsupportedJwtException ex) {
            log.error("Unsupported JWT token");
        } catch (IllegalArgumentException ex) {
            log.error("JWT claims string is empty.");
        }
        return false;
    }

    // 로그아웃
    public String reqLogout() {
        String url = ssoUrl + logoutPath;
        String params = "?client_id=" + clientId + "&returnUrl=" + myDomain + "/";

        return url + params;
    }

    // session 만료, 쿠키 만료
    public void expireSessionAndLogout(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) throws IOException {

        HttpSession session = httpServletRequest.getSession(true);
        session.removeAttribute("accessUser"); // 기존 세션 만료

        httpServletResponse.sendRedirect(reqLogout());
    }

    // session 설정
    public void setSessionAccessUser(String refreshToken, HttpServletRequest httpServletRequest) throws Exception {

        HttpSession session = httpServletRequest.getSession(true);

        Claims claims = Jwts.parser().setSigningKey(singKey).parseClaimsJws(refreshToken).getBody();
        String email = claims.getSubject();

        MemberSecurityVo user = (MemberSecurityVo) memberDetailsService.loadUserByUsername(email);
        SecurityContext sc = SecurityContextHolder.getContext();
        sc.setAuthentication(new UsernamePasswordAuthenticationToken(email, "", user.getAuthorities()));

        MemberVo memberVo = memberService.getLogInMemberDetail(email);
        session.setAttribute("accessUser", memberVo);
        session.setAttribute(HttpSessionSecurityContextRepository.SPRING_SECURITY_CONTEXT_KEY, sc);
    }

    // 쿠키를 통해 token 확인
    public Map<String, String> getCookie(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {

        Map<String, String> result = new HashMap<>();

        Cookie[] cookies = httpServletRequest.getCookies();
        String accessToken;
        String refreshToken;

        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equalsIgnoreCase("MAUM_AID")) {
                    accessToken = cookie.getValue();
                    log.info("@ OAuth.token access_token value: {}", accessToken);

                    result.put("accessToken", accessToken);
                }
                if (cookie.getName().equalsIgnoreCase("MAUM_RID")) {
                    refreshToken = cookie.getValue();
                    log.info("@ OAuth.token refresh_token value: {}", refreshToken);

                    result.put("refreshToken", refreshToken);
                }
            }
        } else {
            return null;
        }
        return result;
    }
}
