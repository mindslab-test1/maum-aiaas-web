package console.maum.ai.common.model;

public class CommonDto extends CommonVo {
	
	private String createDate;	// 생성일
	private String updateDate;	// 수정일	
	
	public String getCreateDate() {
		return createDate;
	}
	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}
	public String getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}		
}
