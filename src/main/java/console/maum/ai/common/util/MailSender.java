package console.maum.ai.common.util;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import console.maum.ai.common.model.ResultDto;
import console.maum.ai.member.dao.MemberDao;
import console.maum.ai.member.model.MemberDto;
import console.maum.ai.payment.model.PaymentVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import console.maum.ai.support.dao.SupportDao;
import console.maum.ai.support.model.ContactUsDto;
import console.maum.ai.support.model.SupportDto;
import console.maum.ai.support.model.SupportForm;

@Service
public class MailSender {
	
    private final static Logger logger = LoggerFactory.getLogger(MailSender.class);
	
	@Autowired
	private SupportDao supportDao;

	@Autowired
	private MemberDao memberDao;
	
	public void mailSend(ContactUsDto contactUsInfo) throws UnsupportedEncodingException{
		System.out.println("mailSend : " + contactUsInfo.toString());
		
		try {
			String emailReceiver = "support@mindslab.ai";       // 메일 받는 사람
			String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
			String emailSubject = "[Cloud API] "+ contactUsInfo.getName()+"님의 문의 사항입니다.";			// 제목
			String emailMsgTxt = 						 		// 내용
					"이  름 : " + contactUsInfo.getName() + "<br>" + 
					"회사명 : " + contactUsInfo.getCompany() + "<br>" + 
					"이메일 : " + contactUsInfo.getMailAddr() + "<br>" + 
					"연락처 : " + contactUsInfo.getPhone() + "<br>" + 
					"내  용 <br>" + contactUsInfo.getContent() + "<br><br>" + 
					"이 문의는 Cloud API에서 보낸 메일입니다.";

			// 메일보내기 
			postMail(emailReceiver, emailSubject, emailMsgTxt, emailSender);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public void mailSendApi(ContactUsDto contactUsInfo) throws UnsupportedEncodingException{
		System.out.println("mailSendApi : " + contactUsInfo.toString());
		
		try {
			String emailReceiver = "support@mindslab.ai";       // 메일 받는 사람
			String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
			String emailSubject = "ID/Key 발급 신청";					// 제목
			String emailMsgTxt = 						 		// 내용
					"* 사용자 email : " + contactUsInfo.getUserEmail() + "<br><br>" +
					"* 서비스 내용 : " + contactUsInfo.getContent() + "<br>" +
					"* 사용 API : " + contactUsInfo.getName() + "<br>" + 
					"* 사용 회사 : " + contactUsInfo.getCompany() + "<br>" + 
					"* ID,Key 받을  email : " + contactUsInfo.getMailAddr() + "<br><br>" +  
					"해당 메일은 service.maum.ai 이용내역 ID,Key 발급신청을 통해 발송되었습니다.";	

			// 메일보내기 
			postMail(emailReceiver, emailSubject, emailMsgTxt, emailSender);
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	public void batchMailSend() throws Exception{
		logger.info("MailSender.batchMailSend()!! ");
		
		SupportForm supportForm = new SupportForm();
		
		//메일 발송 대상 조회
		int status = 0;
		List<SupportDto> list = supportDao.getSupportMailList(status);
		
		Gson gson = new Gson();
		logger.info("MailSender batchMailSend list ======= " + gson.toJson(list));	
		
		
		for(SupportDto sdto : list) {
			logger.info("batchMailSend sdto getId ======= " + sdto.getId());
			
			//메일 발송
			try {
				String emailReceiver = sdto.getToaddr();    // 메일 받는 사람
				String emailSender = sdto.getFromaddr();	// 메일 보내는 사람
				String emailSubject = sdto.getSubject();	// 제목
				String emailMsgTxt = sdto.getMessage();		// 내용

				// 메일보내기 
				postMail(emailReceiver, emailSubject, emailMsgTxt, emailSender);
				
				//메일 발송 상태 업데이트
				supportForm.setId(sdto.getId());
				supportForm.setStatus(1);
				supportDao.updateSupportMail(supportForm);				
				
			} catch (MessagingException e) {
				e.printStackTrace();
			}

		}		

	}	

	public void postMail(String receiver, String subject, String message, String from) throws MessagingException {
		boolean debug = false;
		java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());

		String SMTP_HOST_NAME = "gmail-smtp.l.google.com";

		// Properties 설정
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.starttls.enable","true");
		props.put("mail.smtp.host", SMTP_HOST_NAME);
		props.put("mail.smtp.auth", "true");

		Authenticator auth = new SMTPAuthenticator();
		Session session = Session.getDefaultInstance(props, auth);

		session.setDebug(debug);

		// create a message
		Message msg = new MimeMessage(session);

		// set the from and to address
		InternetAddress addressFrom = new InternetAddress();
		addressFrom = new InternetAddress("마음AI<"+from+">") ;
		msg.setFrom(addressFrom);

		InternetAddress addressTo = new InternetAddress(receiver);
		msg.setRecipient(Message.RecipientType.TO, addressTo);

		// Setting the Subject and Content Type
		msg.setSubject(subject);
		msg.setContent(message, "text/html; charset=utf-8");
		Transport.send(msg);
	}

	/**
	 * Welcome mail - 2019. 11. 15 - LYJ
	 * DESC : 구글 로그인을 통해 접속한 사용자에게 보내는 메일 (INIT 사용자)
	 */
	public void sendWelcomeMail(String receiver) throws Exception {
		logger.info(" @ MailSender.sendWelcomeMail()!! ");

		SupportForm supportForm = new SupportForm();

		MemberDto memberDto = memberDao.getMemberDetail(receiver);

		String emailReceiver = receiver;       		// 메일 받는 사람
		String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String emailSubject = memberDto.getName() + " 님, maum.ai에 오신걸 환영합니다.";	// 제목
		String emailMsgTxt = "<!DOCTYPE html>\n" +
				"<html lang =\"ko\">\n" +
				"<head>\n" +
				"    <meta charset =\"utf-8\">\n" +
				"    <title>welcome page</title>\n" +
				"</head>\n" +
				"<body style =\"margin:0; padding:100px 0; width:100%; height:100%;font-family: '나눔고딕',NanumGothic,'맑은고딕',Malgun Gothic,'돋움',Dotum,Helvetica,'Apple SD Gothic Neo',Sans-serif; background-color: #f7f8fc; font-size:12px; color:#666; line-height:18px;\">\n" +
				"    <div id =\"wrap\" style=\"width:650px;margin:0 auto ;  border: solid 1px #cfd5eb;\">\n" +
				"        <div id =\"header\" style =\"height:60px;position: relative;background: #fff;text-align: center;padding:0 10px; \">\n" +
				"            <h1 style =\"margin: 0;border-bottom:1px solid rgba(182, 197, 209, 0.3)\">\n" +
				"\t\t\t\t<a href=\"https://maum.ai\" target=\"_blank\" title=\"마음에이아이\">\n" +
				"\t\t\t\t\t<img src=\"https://maum.ai/aiaas/common/images/maumai.png\" alt=\"logo\" style=\"height:32px;margin:0 auto;padding:17px 0 10px;\"/>\n" +
				"\t\t\t\t</a>\n" +
				"            </h1>\n" +
				"        </div>\n" +
				"        <div id =\"container\" style =\"width:648px;margin:0 auto;\">\n" +
				"            <div id =\"content\" style =\"font-size:16px;color: #000;background: #fff;padding: 28px 24px 72px; box-sizing: border-box;text-align: center;\">\n" +
				"                <h5 style =\" font-size: 18px;font-weight: 600;letter-spacing: -0.26px;text-align: center;color: #14b6cb; margin: 0 0 28px 0;\">반갑습니다.</h5>\t\n" +
				"\t\t\t\t<img src =\"https://maum.ai/aiaas/common/images/mail/ico_welcome_woman.png\" alt =\"computer image\" style =\"margin: 28px 0 32px 0;width:113px;\"/>\n" +
				"                <p style =\"font-weight: 500;line-height: 1.71;letter-spacing: -0.4px;text-align: center;color: #34424e;font-size: 14px;margin: 0 0 10px 0;\">" + memberDto.getName() +" 님의 아이디어와 비즈니스를 최고의 인공지능 플랫폼 마음 AI에서 실현할 수 있습니다. <br>서비스 등록을 통해 1개월의 무료 체험과 다양한 혜택을 경험해보세요. </p>\n" +
				"                <strong style =\"font-size:14px; color: #407ae5;font-weight: bold;display: block;line-height: 2.5;\">바로 받는 마음 AI 혜택</strong>\n" +
				"               \n" +
				"                <ul style =\"text-align:center;list-style-position : inside; font-size: 14px;font-weight: bold;line-height: 1.79;letter-spacing: -0.4px;text-align: center;color: #407ae5;padding:0;margin:0 0 30px 0;\">\n" +
				"\t\t\t\t\t<li style =\"margin:0;\">다양한 AI 엔진 쉽게 써보기</li>\n" +
				"                    <li style =\"margin:0;\">비즈니스 사례 보기</li>\n" +
				"                    <li style =\"margin:0;\">1:1 밀착 AI 컨설팅</li>\n" +
				"                    <li style =\"margin:0;\">Open API 사용 매뉴얼</li>\n" +
				"                    <li style =\"margin:0;\">마음아카데미 무료 수강</li>\n" +
				"                </ul>\n" +
				"            \n" +
				"                <a href =\"https://maum.ai/\" target =\"_blank\" style =\"display: inline-block;width: calc(50% -  6px); height: 48px; border-radius: 2px; background-color: #14b6cb;line-height: 48px;color:#fff; font-size: 15px;letter-spacing: -0.21px;text-decoration: none\">maum.ai 바로가기</a>\n" +
				"\t\t\t\t<a href =\"https://maum.ai/#inquiry\" target =\"_blank\" style =\"display: inline-block;width:calc(50% -  6px);margin:0 0 0 5px; height: 48px; border-radius: 2px; background-color: #14b6cb;line-height: 48px;color:#fff; font-size: 15px;letter-spacing: -0.21px;text-decoration: none\">AI 컨설팅 문의하기 </a>\n" +
				"            </div>\n" +
				"            <div class =\"contact\" style =\"background: #fff;padding: 0 10px 20px; font-size:13px;\">\n" +
				"\t\t\t\t<div style=\"text-align: center;border-top:1px solid rgba(182, 197, 209, 0.3);color: rgba(52, 66, 78, 0.3); padding: 20px 0 0;\">\n" +
				"\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">이용약관</a>  ㅣ  \n" +
				"\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain#conditions\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">개인정보처리방침</a>  ㅣ  \n" +
				"\t\t\t\t\t<a href=\"mailto:hello@mindslab.ai\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">고객센터</a>\n" +
				"\t\t\t\t</div>\n" +
				"\t\t\t\t<p style=\"font-size: 13px;letter-spacing: -0.24px;text-align: center;color: #34424e;margin: 10px 0;\">Copyright © 2020 주식회사 마인즈랩. All rights reserved.</p>\n" +
				"            </div>\n" +
				"        </div>\n" +
				"    </div>\n" +
				"</body>\n" +
				"</html>";
		// 메일보내기
		supportForm.setFromaddr(emailSender);
		supportForm.setToaddr(emailReceiver);
		supportForm.setSubject(emailSubject);
		supportForm.setMessage(emailMsgTxt);
		supportForm.setStatus(0);

		supportDao.insertSupportMail(supportForm);

		logger.info("======> sendWelcomeMailTo : " + receiver);
	}

	/**
	 * Thank you for joining mail - 2019. 11. 15 - LYJ
	 * DESC : 최초 카드 등록한 사용자에게 보내는 메일 (FREE 사용자)
	 */
	public void sendFreeUserMail(String receiver) throws Exception {
		logger.info(" @ MailSender.sendFreeUserMail()!! ");

		SupportForm supportForm = new SupportForm();

		MemberDto memberDto = memberDao.getMemberDetail(receiver);

		String emailReceiver = receiver;       		// 메일 받는 사람
		String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String emailSubject = "서비스 구독을 신청해주셔서 감사합니다.";	// 제목
		String emailMsgTxt = "<!DOCTYPE html>\n" +
				"<html lang =\"ko\">\n" +
				"<head>\n" +
				"    <meta charset =\"utf-8\">\n" +
				"    <title>subscribe page</title>\n" +
				"</head>\n" +
				"<body style =\"margin:0; padding:100px 0; width:100%; height:100%;font-family: '나눔고딕',NanumGothic,'맑은고딕',Malgun Gothic,'돋움',Dotum,Helvetica,'Apple SD Gothic Neo',Sans-serif; background-color: #f7f8fc; font-size:12px; color:#666; line-height:18px;\">\n" +
				"    <div id =\"wrap\" style=\"width:650px;margin:0 auto;  border: solid 1px #cfd5eb;\">\n" +
				"        <div id =\"header\" style =\"height:60px;position: relative;background: #fff;text-align: center;padding:0 10px; \">\n" +
				"            <h1 style =\"margin: 0;border-bottom:1px solid rgba(182, 197, 209, 0.3)\">\n" +
				"\t\t\t\t<a href=\"https://maum.ai\" target=\"_blank\" title=\"마음에이아이\">\n" +
				"\t\t\t\t\t<img src=\"https://maum.ai/aiaas/common/images/maumai.png\" alt=\"logo\" style=\"height:32px;margin:0 auto;padding:17px 0 10px;\"/>\n" +
				"\t\t\t\t</a>\n" +
				"            </h1>\n" +
				"        </div>\n" +
				"        <div id =\"container\" style =\"width:648px;margin:0 auto;\">\n" +
				"            <div id =\"content\" style =\"font-size:16px;color: #000;background: #fff;padding: 28px 24px 44px; box-sizing: border-box;text-align: center;\">\n" +
				"                <h5 style =\" font-size: 18px;font-weight: 600;letter-spacing: -0.26px;text-align: center;color: #14b6cb;    margin: 0 0 28px 0;\">마음 AI 가입을 환영합니다.</h5>\t\n" +
				"\t\t\t\t<img src =\"https://maum.ai/aiaas/common/images/mail/ico_thankyou_woman.png\" alt =\"computer image\" style =\"margin: 0 0 15px 0;height:100px;\"/>\n" +
				"                <p style =\"font-weight: 500;line-height: 1.71;letter-spacing: -0.4px;text-align: center;color: #34424e;font-size: 14px;margin: 0 0 30px 0;\">"+memberDto.getName()+" 님, 최고의 인공지능 플랫폼 마음 AI 비즈니스 플랜 가입을 환영합니다. <br>\n" +
				"지금부터 1개월의 무료 체험이 시작됩니다. 다양한 서비스를 놓치지 마세요! </p>\n" +
				"                <strong style =\"font-size:14px; color: #407ae5;font-weight: bold;display: block;line-height: 2.5;\">바로 받는 마음 AI 혜택</strong>\n" +
				"               \n" +
				"                <ul style =\"text-align:center;list-style-position : inside; font-size: 14px;font-weight: bold;line-height: 1.79;letter-spacing: -0.4px;text-align: center;color: #407ae5;padding:0;margin:0 0 30px 0;\">\n" +
				"\t\t\t\t\t<li style =\"margin:0;\">다양한 AI 엔진 쉽게 써보기</li>\n" +
				"                    <li style =\"margin:0;\">비즈니스 사례 보기</li>\n" +
				"                    <li style =\"margin:0;\">1:1 밀착 AI 컨설팅</li>\n" +
				"                    <li style =\"margin:0;\">Open API 사용 매뉴얼</li>\n" +
				"                    <li style =\"margin:0;\">마음아카데미 무료 수강</li>\n" +
				"                </ul>\n" +
				"            \n" +
				"                <a href =\"https://maum.ai/\" target =\"_blank\" style =\"display: inline-block;width: 360px; height: 48px; border-radius: 2px; background-color: #14b6cb;line-height: 48px;color:#fff; font-size: 15px;letter-spacing: -0.21px;text-decoration: none\">maum.ai 바로가기</a>\n" +
				"\t\t\t\n" +
				"            </div>\n" +
				"            <div class =\"contact\" style =\"background: #fff;padding: 0 10px 20px; font-size:13px;\">\n" +
				"\t\t\t\t<div style=\"text-align: center;border-top:1px solid rgba(182, 197, 209, 0.3);color: rgba(52, 66, 78, 0.3); padding: 20px 0 0;\">\n" +
				"\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">이용약관</a>  ㅣ  \n" +
				"\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain#conditions\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">개인정보처리방침</a>  ㅣ  \n" +
				"\t\t\t\t\t<a href=\"mailto:hello@mindslab.ai\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">고객센터</a>\n" +
				"\t\t\t\t</div>\n" +
				"\t\t\t\t<p style=\"font-size: 13px;letter-spacing: -0.24px;text-align: center;color: #34424e;margin: 10px 0;\">Copyright © 2020 주식회사 마인즈랩. All rights reserved.</p>\n" +
				"            </div>\n" +
				"        </div>\n" +
				"    </div>\n" +
				"</body>\n" +
				"</html>";							// 내용

		// 메일보내기
		supportForm.setFromaddr(emailSender);
		supportForm.setToaddr(emailReceiver);
		supportForm.setSubject(emailSubject);
		supportForm.setMessage(emailMsgTxt);
		supportForm.setStatus(0);

		supportDao.insertSupportMail(supportForm);

		logger.info("======> sendFreeUserMailTo : " + receiver);
	}


	/**
	 * Thank you for joining mail_2 - 2020. 05. 12 - LYJ
	 * DESC : 즉시 결제 사용자(구독 취소 이후 다시 카드를 등록)에게 보내는 메일
	 * 결제 시 메일 전송으로 인해 해당 기능은 사용하지 않음 - 2020. 11. 16
	 */
	public void sendSubscribeUserMail(String receiver) throws Exception {
		logger.info(" @ MailSender.sendSubscribeUserMail()!! ");

		SupportForm supportForm = new SupportForm();

		MemberDto memberDto = memberDao.getMemberDetail(receiver);

		String emailReceiver = receiver;       		// 메일 받는 사람
		String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String emailSubject = "서비스 구독을 신청해주셔서 감사합니다.";	// 제목
		String emailMsgTxt = "<!DOCTYPE html> \n" +
				"<html lang=\"ko\"> \n" +
				"<head> \n" +
				"<meta charset=\"utf-8\"> \n" +
				"<title>Email page</title> \n" +
				"</head> \n" +
				"<body style=\"margin:0; padding:0; width:100%; height:100%; font-family:'Noto Sans KR', sans-serif;background:#f9f9f9; font-size:12px; color:#666; line-height:18px;\"> \n" +
				"<!-- #wrap --> \n" +
				"<div id=\"wrap\" style=\"width:648px;margin:0 auto;\"> \n" +
				"\t\n" +
				"\t <!-- #header --> \n" +
				"\t<div id=\"header\" style=\"height:96px;position: relative;background: #fff; background: url(https://maum.ai/aiaas/common/images/mail/header_web_maumlogo.png) center center no-repeat;;\"> \t\t\n" +
				"\t\t<h1 style=\"float: left;margin: 24px 0;\"><a href=\"https://maum.ai\" alt=\"로고\" style=\"margin: 0 auto;display: block;height:100%; width: 200px; position: absolute; top: 0; left:0;\"></a></h1>\n" +
				"\t</div> \n" +
				"\t<!-- //#header --> \n" +
				"\t<!-- //#container --> \n" +
				"\t<div id=\"container\" style=\"width:648px;margin:0 auto;\"> \n" +
				"\t\t<!-- content --> \n" +
				"\t\t<div id=\"content\" style=\"font-size:16px;color: #000;background: #fff;padding: 40px 24px 32px; box-sizing: border-box;text-align: center;\"> \n" +
				"\t\t\t<h5 style=\"font-size:27px;margin: 0 0 24px 0;height:25px;line-height: 25px;text-align: left;\">감사합니다!</h5>\n" +
				"\t\t\t<p style=\"margin:0 0 48px 0;line-height: 1.5;text-align: left;\">"+memberDto.getName()+"님, 최고의 인공지능 플랫폼 마음 AI 비즈니스 플랜 가입을 환영합니다. <br><br>\n" +
				"\n" +
				"지금부터 <strong>모든 서비스 이용</strong>이 가능합니다. <strong>다양한 서비스</strong>를 놓치지 마세요! \n" +
				" </p>\n" +
				"\t\t\t<strong style=\"color: #e66f83;font-weight: bold;display: block;margin:0 0 24px 0;\">바로 받는 마음 AI의 혜택</strong>\n" +
				"\n" +
				"\t\t\t\n" +
				"\t\t\t<img src=\"https://maum.ai/aiaas/common/images/mail/ico_thankyou_woman.png\" alt=\"computer image\" style=\"margin: 0 0 24px 0;width:99px;\"/>\n" +
				"\t\t\t<ul style=\"text-align:center;list-style-position : inside;font-size: 20px;font-weight: bold; line-height: 1.85;padding:0;margin:0 0 24px 0;\">\n" +
				"\t\t\t\t<li style=\"margin:0;\">다양한 AI 엔진 쉽게 써보기</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">비즈니스 사례 보기</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">1:1 밀착 AI 컨설팅</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">Open API 사용 매뉴얼</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">마음아카데미 무료 수강</li>\n" +
				"\t\t\t</ul>\t\t\t\n" +
				"\t\t\t\n" +
				"\t\t\t<a href=\"https://maum.ai/\" target=\"_blank\" style=\"display: block; width: 214px;height: 48px;margin:0 auto;border-radius: 8px;background-color: #0080df;color:#fff;line-height: 48px;text-align: center;font-weight: 300;\">maum.ai 바로가기</a>\n" +
				"\t\t</div> \n" +
				"\t\t<!-- //#content -->\n" +
				"\t\t<div class=\"contact\" style=\"background: #fff; padding: 0 24px 40px; color: #000;font-size:14px; line-height: 1.43; font-weight: 600;\">\n" +
				"\t\t\t<p style=\"margin:0 0 24px 0;\">고객의 성공을 돕는 <br>\t\t\t\t\n" +
				"마음 AI 팀 드림</p>\n" +
				"\n" +
				"\t\t\t<p><strong>문의</strong><br>\n" +
				"\t\t\t\tsupport@mindslab.ai<br>\n" +
				"\t\t\t\t1661 - 3222</p> \n" +
				"\t\t</div>\n" +
				"\t</div> \n" +
				"\t<!-- //#container --> \n" +
				"\t\n" +
				"</div> \n" +
				"<!-- //#wrap --> \n" +
				"\t<!-- # footer -->\n" +
				"\t<div id=\"footer\" style=\"text-align: center; padding: 32px 0 96px;\"> \n" +
				"\t\t<p style=\"font-size:16px;color: #818181;\">follow us</p>\n" +
				"\t\t<div class=\"social_link\" style=\"margin:10px 0 32px 0;\">\n" +
				"\t\t\t<a href=\"https://www.facebook.com/mindsinsight/\" alt=\"facebook\" target=\"_blank\" style=\"width:31px;height:31px; display: inline-block;margin:0 20px 0 0;background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_facebook.png) center center no-repeat; background-size: cover;\"></a>\n" +
				"\t\t\t<a href=\"https://kr.linkedin.com/company/mindslabai\" alt=\"linkedin\" target=\"_blank\" style=\"width:32px;height:31px; display: inline-block;margin:0 20px 0 0;background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_linkedin.png) center center no-repeat; background-size: cover;\"></a>\n" +
				"\t\t\t<a href=\"https://www.youtube.com/channel/UC8ocQaJFp-adzGtwcBuS3iQ\" target=\"_blank\" alt=\"youtube\" style=\"width:32px;height:32px; display: inline-block;margin:0 20px 0 0; background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_youtube.png) center center no-repeat;margin:0; background-size: cover;\" ></a>\n" +
				"\t\t</div>\n" +
				"\t\t<p class=\"address\" style=\"font-size:11px; line-height: 1.62;\">경기도 성남시 분당구 대왕판교로644번길 49 다산타워 6층</p>\n" +
				"\t\t<div class=\"link\" style=\"\">\n" +
				"\t\t\t<a href=\"https://maum.ai/\" target=\"_blank\" style=\"text-align: center;color: #818181;font-size:13px;text-decoration: underline;\">maum.ai</a>\n" +
				"\t\t</div>\n" +
				"\t</div> \n" +
				"\t<!-- //# footer --> \n" +
				"<script>\n" +
				"\n" +
				"</script>\t\n" +
				"</body> \n" +
				"</html>\n";							// 내용
		// 메일보내기

		System.out.println("Length : " + emailMsgTxt.length());
		supportForm.setFromaddr(emailSender);
		supportForm.setToaddr(emailReceiver);
		supportForm.setSubject(emailSubject);
		supportForm.setMessage(emailMsgTxt);
		supportForm.setStatus(0);

		supportDao.insertSupportMail(supportForm);

		logger.info("======> sendSubscribeUserMail : " + receiver);
	}

	/**
	 * Payment error - 2019. 11. 15 - LYJ
	 * DESC : 정기 결제 실패 시, 사용자에게 보내는 메일 (SUBSCRIBED 사용자)
	 */
	public void sendPaymentErrorUserMail(String receiver) throws Exception {
		logger.info(" @ MailSender.sendPaymentErrorUserMail()!! ");

		SupportForm supportForm = new SupportForm();

		MemberDto memberDto = memberDao.getMemberDetail(receiver);

		String emailReceiver = receiver;       		// 메일 받는 사람
		String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String emailSubject = "결제 상태를 확인해주세요!";	// 제목
		String emailMsgTxt = "<!DOCTYPE html>\n" +
				"<html lang =\"ko\">\n" +
				"<head>\n" +
				"    <meta charset =\"utf-8\">\n" +
				"    <title>fail page</title>\n" +
				"</head>\n" +
				"<body style =\"margin:0; padding:100px 0; width:100%; height:100%;font-family: '나눔고딕',NanumGothic,'맑은고딕',Malgun Gothic,'돋움',Dotum,Helvetica,'Apple SD Gothic Neo',Sans-serif; background-color: #f7f8fc; font-size:12px; color:#666; line-height:18px;\">\n" +
				"    <div id =\"wrap\" style=\"width:650px;margin:0 auto ;  border: solid 1px #cfd5eb;\">\n" +
				"        <div id =\"header\" style =\"height:60px;position: relative;background: #fff;text-align: center;padding:0 10px; \">\n" +
				"            <h1 style =\"margin: 0;border-bottom:1px solid rgba(182, 197, 209, 0.3)\">\n" +
				"\t\t\t\t<a href=\"https://maum.ai\" target=\"_blank\" title=\"마음에이아이\">\n" +
				"\t\t\t\t\t<img src=\"https://maum.ai/aiaas/common/images/maumai.png\" alt=\"logo\" style=\"height:32px;margin:0 auto;padding:17px 0 10px;\"/>\n" +
				"\t\t\t\t</a>\n" +
				"            </h1>\n" +
				"        </div>\n" +
				"        <div id =\"container\" style =\"width:648px;margin:0 auto;\">\n" +
				"            <div id =\"content\" style =\"font-size:16px;color: #000;background: #fff;padding: 28px 24px 108px; box-sizing: border-box;text-align: center;\">\n" +
				"                <h5 style =\" font-size: 18px;font-weight: 600;letter-spacing: -0.26px;text-align: center;color: #14b6cb;margin: 0 0 20px 0;\">마음 AI 결제 실패</h5>\t\n" +
				"\t\t\t\t<img src =\"https://maum.ai/aiaas/common/images/mail/ico_payerror_man.png\" alt =\"computer image\" style =\"margin: 0 0 15px 0;height:100px;\"/>\n" +
				"                <p style =\"font-weight: 500;line-height: 1.71;letter-spacing: -0.4px;text-align: center;color: #34424e;font-size: 14px;margin: 0 0 22px 0;\">"+memberDto.getName()+" 님, 구독중인 마음 AI 결제가 실패하였습니다. <br>\n" +
				"결제 오류로 인해 서비스 이용이 바로 중지되지 않습니다. <br>\n" +
				"담당 컨설턴트에게 연락 주시면 문제 해결을 도와 드립니다.<br>\n" +
				"담당자가 없는 경우 대표 번호, 홈페이지, 이메일 등으로 연락주시면 바로 지원 해드립니다. </p>\n" +
				"              \n" +
				"                <ul style =\"text-align:center;list-style-position : inside; font-size: 14px;font-weight: bold;line-height: 1.79;letter-spacing: -0.4px;text-align: center;color: #407ae5;padding:0;margin:0 0 30px 0;\">\n" +
				"\t\t\t\t\t<li style =\"margin:0;\">카드 유효기간 (만료일) 확인</li>\n" +
				"                    <li style =\"margin:0;\">분실신고/이용정지 여부 확인</li>\n" +
				"                    <li style =\"margin:0;\">카드 결제일과 한도 금액 확인 </li>\n" +
				"                </ul>\n" +
				"            \n" +
				"                <a href =\"https://maum.ai/\" target =\"_blank\" style =\"display: inline-block;width:360px; height: 48px; border-radius: 2px; background-color: #14b6cb;line-height: 48px;color:#fff; font-size: 15px;letter-spacing: -0.21px;text-decoration: none\">maum.ai 바로가기</a>\n" +
				"\t\t\t\n" +
				"            </div>\n" +
				"            <div class =\"contact\" style =\"background: #fff;padding: 0 10px 20px; font-size:13px;\">\n" +
				"\t\t\t\t<div style=\"text-align: center;border-top:1px solid rgba(182, 197, 209, 0.3);color: rgba(52, 66, 78, 0.3); padding: 20px 0 0;\">\n" +
				"\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">이용약관</a>  ㅣ  \n" +
				"\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain#conditions\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">개인정보처리방침</a>  ㅣ  \n" +
				"\t\t\t\t\t<a href=\"mailto:hello@mindslab.ai\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">고객센터</a>\n" +
				"\t\t\t\t</div>\n" +
				"\t\t\t\t<p style=\"font-size: 13px;letter-spacing: -0.24px;text-align: center;color: #34424e;margin: 10px 0;\">Copyright © 2020 주식회사 마인즈랩. All rights reserved.</p>\n" +
				"            </div>\n" +
				"        </div>\n" +
				"    </div>\n" +
				"</body>\n" +
				"</html>";

		// 메일보내기
		supportForm.setFromaddr(emailSender);
		supportForm.setToaddr(emailReceiver);
		supportForm.setSubject(emailSubject);
		supportForm.setMessage(emailMsgTxt);
		supportForm.setStatus(0);

		supportDao.insertSupportMail(supportForm);

		logger.info("======> sendPaymentErrorUserMailTo : " + receiver);
	}


	/**
	 * Request cancel subscription - 2019. 11. 15 - LYJ
	 * DESC : 구독 해지를 신청한 유료 사용자에게 보내는 메일 (SUBSCRIBED -> UNSUBSCRIBING 사용자)
	 */
	public void sendReqCancelUserMail(String receiver) throws Exception {
		logger.info(" @ MailSender.sendReqCancelUserMail()!! ");

		SupportForm supportForm = new SupportForm();

		MemberDto memberDto = memberDao.getMemberDetail(receiver);

		String emailReceiver = receiver;       		// 메일 받는 사람
		String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String emailSubject = "서비스 구독 해지 신청하셨습니다.";	// 제목
		String emailMsgTxt = "<!DOCTYPE html>\n" +
				"<html lang =\"ko\">\n" +
				"<head>\n" +
				"    <meta charset =\"utf-8\">\n" +
				"    <title>cancel page</title>\n" +
				"</head>\n" +
				"<body style =\"margin:0; padding:100px 0; width:100%; height:100%;font-family: '나눔고딕',NanumGothic,'맑은고딕',Malgun Gothic,'돋움',Dotum,Helvetica,'Apple SD Gothic Neo',Sans-serif; background-color: #f7f8fc; font-size:12px; color:#666; line-height:18px;\">\n" +
				"    <div id =\"wrap\" style=\"width:650px;margin:0 auto ;  border: solid 1px #cfd5eb;\">\n" +
				"        <div id =\"header\" style =\"height:60px;position: relative;background: #fff;text-align: center;padding:0 10px; \">\n" +
				"            <h1 style =\"margin: 0;border-bottom:1px solid rgba(182, 197, 209, 0.3)\">\n" +
				"\t\t\t\t<a href=\"https://maum.ai\" target=\"_blank\" title=\"마음에이아이\">\n" +
				"\t\t\t\t\t<img src=\"https://maum.ai/aiaas/common/images/maumai.png\" alt=\"logo\" style=\"height:32px;margin:0 auto;padding:17px 0 10px;\"/>\n" +
				"\t\t\t\t</a>\n" +
				"            </h1>\n" +
				"        </div>\n" +
				"        <div id =\"container\" style =\"width:648px;margin:0 auto;\">\n" +
				"            <div id =\"content\" style =\"font-size:16px;color: #000;background: #fff;padding: 28px 24px 155px; box-sizing: border-box;text-align: center;\">\n" +
				"                <h5 style =\" font-size: 18px;font-weight: 600;letter-spacing: -0.26px;text-align: center;color: #14b6cb; margin: 0 0 28px 0;\">마음 AI 구독 취소</h5>\t\n" +
				"\t\t\t\t<img src =\"https://maum.ai/aiaas/common/images/mail/ico_unsubing_woman.png\" alt =\"computer image\" style =\"margin: 0 0 20px 0;height:100px;;\"/>\n" +
				"                <p style =\"font-weight: 500;line-height: 1.71;letter-spacing: -0.4px;text-align: center;color: #34424e;font-size: 14px;margin: 0 0 34px 0;\">"+memberDto.getName()+" 님의 마음 AI 서비스 구독이 취소되었습니다. <br> \n" +
				"최종 결제일로부터 1달 까지만 서비스는 유지되며, <br>\n" +
				"이 후 모든 마음 AI의 서비스와 혜택을 사용하실 수 없습니다. <br>\n" +
				"구독을 연장하고 싶으시면 언제든 연락해주세요. <br>\n" +
				"마음 AI 전문 컨설턴트가 문제점을 해결하고 밀착지원 해드리도록 하겠습니다.</p>\n" +
				"               \n" +
				"                <a href =\"https://maum.ai/\" target =\"_blank\" style =\"display: block;margin:0 auto;width: 360px; height: 48px; border-radius: 2px; background-color: #14b6cb;line-height: 48px;color:#fff; font-size: 15px;letter-spacing: -0.21px;text-decoration: none\">maum.ai 구독 연장하기</a>\n" +
				"\n" +
				"            </div>\n" +
				"            <div class =\"contact\" style =\"background: #fff;padding: 0 10px 20px; font-size:13px;\">\n" +
				"\t\t\t\t<div style=\"text-align: center;border-top:1px solid rgba(182, 197, 209, 0.3);color: rgba(52, 66, 78, 0.3); padding: 20px 0 0;\">\n" +
				"\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">이용약관</a>  ㅣ  \n" +
				"\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain#conditions\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">개인정보처리방침</a>  ㅣ  \n" +
				"\t\t\t\t\t<a href=\"mailto:hello@mindslab.ai\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">고객센터</a>\n" +
				"\t\t\t\t</div>\n" +
				"\t\t\t\t<p style=\"font-size: 13px;letter-spacing: -0.24px;text-align: center;color: #34424e;margin: 10px 0;\">Copyright © 2020 주식회사 마인즈랩. All rights reserved.</p>\n" +
				"            </div>\n" +
				"        </div>\n" +
				"    </div>\n" +
				"    \n" +
				"</body>\n" +
				"</html>";
		// 메일보내기
		supportForm.setFromaddr(emailSender);
		supportForm.setToaddr(emailReceiver);
		supportForm.setSubject(emailSubject);
		supportForm.setMessage(emailMsgTxt);
		supportForm.setStatus(0);

		supportDao.insertSupportMail(supportForm);

		logger.info("======> sendReqCancelUserMailTo : " + receiver);
	}

	/**
	 * Expired date - 2019. 11. 15 - LYJ
	 * DESC : 구독 해지 신청한 사용자의 사용 기한이 만료되거나, 결제 실패 후 일정 기한까지 재 결제가 안 됐을 때
	 *        구독 사용자에게 보내는 메일 (UNSUBSCRIBED 사용자)
	 */
	public void sendExpiredUserMail(String receiver) throws Exception {
		logger.info(" @ MailSender.sendExpiredUserMail()!! ");

		SupportForm supportForm = new SupportForm();

		MemberDto memberDto = memberDao.getMemberDetail(receiver);

		String emailReceiver = receiver;       		// 메일 받는 사람
		String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String emailSubject = memberDto.getName() + " 님, maum.ai 서비스 중단을 안내드립니다.";	// 제목
		String emailMsgTxt = "<!DOCTYPE html>\n" +
				"<html lang =\"ko\">\n" +
				"<head>\n" +
				"    <meta charset =\"utf-8\">\n" +
				"    <title>fail page</title>\n" +
				"</head>\n" +
				"<body style =\"margin:0; padding:100px 0; width:100%; height:100%;font-family: '나눔고딕',NanumGothic,'맑은고딕',Malgun Gothic,'돋움',Dotum,Helvetica,'Apple SD Gothic Neo',Sans-serif; background-color: #f7f8fc; font-size:12px; color:#666; line-height:18px;\">\n" +
				"    <div id =\"wrap\" style=\"width:650px;margin:0 auto ;  border: solid 1px #cfd5eb;\">\n" +
				"        <div id =\"header\" style =\"height:60px;position: relative;background: #fff;text-align: center;padding:0 10px; \">\n" +
				"            <h1 style =\"margin: 0;border-bottom:1px solid rgba(182, 197, 209, 0.3)\">\n" +
				"\t\t\t\t<a href=\"https://maum.ai\" target=\"_blank\" title=\"마음에이아이\">\n" +
				"\t\t\t\t\t<img src=\"https://maum.ai/aiaas/common/images/maumai.png\" alt=\"logo\" style=\"height:32px;margin:0 auto;padding:17px 0 10px;\"/>\n" +
				"\t\t\t\t</a>\n" +
				"            </h1>\n" +
				"        </div>\n" +
				"        <div id =\"container\" style =\"width:648px;margin:0 auto;\">\n" +
				"            <div id =\"content\" style =\"font-size:16px;color: #000;background: #fff;padding: 28px 24px 108px; box-sizing: border-box;text-align: center;\">\n" +
				"                <h5 style =\" font-size: 18px;font-weight: 600;letter-spacing: -0.26px;text-align: center;color: #14b6cb;margin: 0 0 20px 0;\">다시 만나길 바랍니다.</h5>\t\n" +
				"\t\t\t\t<img src =\"https://maum.ai/aiaas/common/images/mail/ico_payerror_man.png\" alt =\"computer image\" style =\"margin: 0 0 15px 0;height:100px;\"/>\n" +
				"                <p style =\"font-weight: 500;line-height: 1.71;letter-spacing: -0.4px;text-align: center;color: #34424e;font-size: 14px;margin: 0 0 22px 0;\">" + memberDto.getName() + " 님의 마음 AI 서비스 구독이 만료되었습니다.  <br><br>\n" +				"                    아쉽지만 지금부터 모든 마음 AI의 서비스와 혜택은 제공되지 않습니다. <br>\n" +
				"                    인공지능 서비스가 필요하면 언제든 연락해주세요.\n마음 AI의 전문 컨설턴트가 문제점을 해결할 수 있도록 밀착지원 해드립니다. </p>\n" +
				"                    <strong style =\"font-size:14px; color: #407ae5;font-weight: bold;display: block;line-height: 2.5;\">바로 받는 마음 AI 혜택</strong>\n" +
				"                <ul style =\"text-align:center;list-style-position : inside; font-size: 14px;font-weight: bold;line-height: 1.79;letter-spacing: -0.4px;text-align: center;color: #407ae5;padding:0;margin:0 0 30px 0;\">\n" +
				"\t\t\t\t\t<li style =\"margin:0;\">다양한 AI 엔진 쉽게 써보기</li>\n" +
				"                    <li style =\"margin:0;\">비즈니스 사례 보기</li>\n" +
				"                    <li style =\"margin:0;\">1:1 밀착 AI 컨설팅 </li>\n" +
				"                    <li style =\"margin:0;\">Open API 사용 매뉴얼 </li>\n" +
				"                    <li style =\"margin:0;\">마음아카데미 무료 수강 </li>\n" +
				"                </ul>\n" +
				"            \n" +
				"                <a href =\"https://maum.ai/\" target =\"_blank\" style =\"display: inline-block;width:360px; height: 48px; border-radius: 2px; background-color: #14b6cb;line-height: 48px;color:#fff; font-size: 15px;letter-spacing: -0.21px;text-decoration: none\">maum.ai 바로가기</a>\n" +
				"\t\t\t\n" +
				"            </div>\n" +
				"            <div class =\"contact\" style =\"background: #fff;padding: 0 10px 20px; font-size:13px;\">\n" +
				"\t\t\t\t<div style=\"text-align: center;border-top:1px solid rgba(182, 197, 209, 0.3);color: rgba(52, 66, 78, 0.3); padding: 20px 0 0;\">\n" +
				"\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">이용약관</a>  ㅣ  \n" +
				"\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain#conditions\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">개인정보처리방침</a>  ㅣ  \n" +
				"\t\t\t\t\t<a href=\"mailto:hello@mindslab.ai\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">고객센터</a>\n" +
				"\t\t\t\t</div>\n" +
				"\t\t\t\t<p style=\"font-size: 13px;letter-spacing: -0.24px;text-align: center;color: #34424e;margin: 10px 0;\">Copyright © 2020 주식회사 마인즈랩. All rights reserved.</p>\n" +
				"            </div>\n" +
				"        </div>\n" +
				"    </div>\n" +
				"</body>\n" +
				"</html>";

		// 메일보내기
		supportForm.setFromaddr(emailSender);
		supportForm.setToaddr(emailReceiver);
		supportForm.setSubject(emailSubject);
		supportForm.setMessage(emailMsgTxt);
		supportForm.setStatus(0);

		supportDao.insertSupportMail(supportForm);

		logger.info("======> sendExpiredUserMailTo : " + receiver);
	}

	/**
	 * 결제 시 메일 전송 - 2020. 11. 13 - MRS
	 * */

	public void sendPaymentCompleteMail(PaymentVo paymentVo, String paymentPlan) throws Exception {
		String paymentDate = changeSimpleDateFormat(paymentVo.getPayDate());	// 결제 승인일
		String plan = paymentPlan;	// 결제 플랜
		String issuerName = paymentVo.getIssuerName();	// 카드사
		String cardNum = paymentVo.getCardNo();	// 결제 카드 넘버
		int price = paymentVo.getPrice();	// 결제 금액
		String nextPaymentDate = paymentVo.getPaymentDate();	// 다음 결제일

		logger.info(" @ MailSender.sendPaymentCompleteMail()!! ");

		SupportForm supportForm = new SupportForm();

		MemberDto memberDto = memberDao.getMemberDetail(paymentVo.getUserEmail());

		String emailReceiver = paymentVo.getUserEmail();       		// 메일 받는 사람
		String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String emailSubject = memberDto.getName() + " 님, maum.ai 서비스가 결제 되었습니다.";	// 제목
		String emailMsgTxt = "<!DOCTYPE html>\n" +
				"<html lang =\"ko\">\n" +
				"<head>\n" +
				"    <meta charset =\"utf-8\">\n" +
				"    <title>Payment details page</title>\n" +
				"</head>\n" +
				"<body style =\"margin:0; padding:100px 0; width:100%; height:100%;font-family: '나눔고딕',NanumGothic,'맑은고딕',Malgun Gothic,'돋움',Dotum,Helvetica,'Apple SD Gothic Neo',Sans-serif; background-color: #f7f8fc; font-size:12px; color:#666; line-height:18px;\">\n" +
				"    <div id =\"wrap\" style=\"width:650px;margin:0 auto ;  border: solid 1px #cfd5eb;\">\n" +
				"        <div id =\"header\" style =\"height:60px;position: relative;background: #fff;text-align: center;padding:0 10px; \">\n" +
				"            <h1 style =\"margin: 0;border-bottom:1px solid rgba(182, 197, 209, 0.3)\">\n" +
				"\t\t\t\t<a href=\"https://maum.ai\" target=\"_blank\" title=\"마음에이아이\">\n" +
				"\t\t\t\t\t<img src=\"https://maum.ai/aiaas/common/images/maumai.png\" alt=\"logo\" style=\"height:32px;margin:0 auto;padding:17px 0 10px;\"/>\n" +
				"\t\t\t\t</a>\n" +
				"            </h1>\n" +
				"        </div>\n" +
				"        <div id =\"container\" style =\"width:648px;margin:0 auto;\">\n" +
				"            <div id =\"content\" style =\"font-size:16px;color: #000;background: #fff;padding: 28px 24px 108px; box-sizing: border-box;text-align: center;\">\n" +
				"                <h5 style =\" font-size: 18px;font-weight: 600;letter-spacing: -0.26px;text-align: center;color: #14b6cb;margin: 0 0 20px 0;\">마음 AI 결제 내역 안내</h5>\t\n" +
				"\t\t\t\t<img src =\"https://maum.ai/aiaas/common/images/mail/ico_payerror_man.png\" alt =\"computer image\" style =\"margin: 0 0 15px 0;height:100px;\"/>\n" +
				"                <p style =\"font-weight: 500;line-height: 1.71;letter-spacing: -0.4px;text-align: center;color: #34424e;font-size: 14px;margin: 0 0 22px 0;\">" + memberDto.getName() + " 님, 구독중인 마음 AI 서비스의 정상 결제 내역을 알려드립니다.<br>\n" +
				"                    서비스 이용에 차질이 없도록 아래 내용을 확인해주세요. </p>\n" +
				"              \n" +
				"                <ul style =\"text-align:center;list-style:none; width:400px;margin:0 auto 20px;font-size: 14px;font-weight: bold;line-height: 1.79;letter-spacing: -0.4px;text-align: center;color: #407ae5;padding:0;  padding: 10px 20.5px 10px 18.5px; background-color: #f7f8fc;\">\n" +
				"\t\t\t\t\t<li style =\"list-style: none;width:360px;margin:0 auto;height:30px;border-bottom:1px dotted #b6c5d1; font-size: 14px;font-weight: 600; letter-spacing: -0.4px;color: #002b49;text-align: left;\"><strong style=\"display: inline-block;width:40%; height: 30px;line-height: 30px; font-size: 14px;font-weight: normal; letter-spacing: -0.4px;color: #002b49;\">결제일</strong>"+paymentDate+"</li>\n" +
				"                    <li style =\"list-style: none;width:360px;margin:0 auto;height:30px;border-bottom:1px dotted #b6c5d1; font-size: 14px;font-weight: 600; letter-spacing: -0.4px;color: #002b49;text-align: left;\"><strong style=\"display: inline-block;width:40%; height: 30px;line-height: 30px;font-size: 14px;font-weight: normal; letter-spacing: -0.4px;color: #002b49;\">결제플랜</strong>"+plan+"</li>\n" +
				"                    <li style =\"list-style: none;width:360px;margin:0 auto;height:30px;border-bottom:1px dotted #b6c5d1; font-size: 14px;font-weight: 600; letter-spacing: -0.4px;color: #002b49;text-align: left;\"><strong style=\"display: inline-block;width:40%; height: 30px;line-height: 30px;font-size: 14px;font-weight: normal; letter-spacing: -0.4px;color: #002b49;\">결제카드</strong>"+issuerName + " " + cardNum+"</li>\n" +
				"                    <li style =\"list-style: none;width:360px;margin:0 auto;height:30px;border-bottom:1px dotted #b6c5d1; font-size: 14px;font-weight: 600; letter-spacing: -0.4px;color: #002b49;text-align: left;\"><strong style=\"display: inline-block;width:40%; height: 30px;line-height: 30px;font-size: 14px;font-weight: normal; letter-spacing: -0.4px;color: #002b49;\">결제금액</strong>"+price+" 원 (VAT포함)</li>\n" +
				"                    <li style =\"list-style: none;width:360px;margin:0 auto;height:30px; font-size: 14px;font-weight: 600; letter-spacing: -0.4px;color: #002b49;text-align: left;\"><strong style=\"display: inline-block;width:40%; height: 30px;line-height: 30px;font-size: 14px;font-weight: normal; letter-spacing: -0.4px;color: #002b49;\">다음 결제 예정일</strong>"+nextPaymentDate+"</li>\n" +
				"                </ul>\n" +
				"            \n" +
				"                <a href =\"https://maum.ai/\" target =\"_blank\" style =\"display: inline-block;width:360px; height: 48px; border-radius: 2px; background-color: #14b6cb;line-height: 48px;color:#fff; font-size: 15px;letter-spacing: -0.21px;text-decoration: none\">maum.ai 바로가기</a>\n" +
				"\t\t\t\n" +
				"            </div>\n" +
				"            <div class =\"contact\" style =\"background: #fff;padding: 0 10px 20px; font-size:13px;\">\n" +
				"\t\t\t\t<div style=\"text-align: center;border-top:1px solid rgba(182, 197, 209, 0.3);color: rgba(52, 66, 78, 0.3); padding: 20px 0 0;\">\n" +
				"\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">이용약관</a>  ㅣ  \n" +
				"\t\t\t\t\t<a href=\"https://maum.ai/home/krTermsMain#conditions\" target=\"_blank\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">개인정보처리방침</a>  ㅣ  \n" +
				"\t\t\t\t\t<a href=\"mailto:hello@mindslab.ai\" style=\"opacity: 0.8;font-family: NotoSansCJKkr;font-size: 13px;letter-spacing: -0.24px;color: #34424e;text-decoration: none\">고객센터</a>\n" +
				"\t\t\t\t</div>\n" +
				"\t\t\t\t<p style=\"font-size: 13px;letter-spacing: -0.24px;text-align: center;color: #34424e;margin: 10px 0;\">Copyright © 2020 주식회사 마인즈랩. All rights reserved.</p>\n" +
				"            </div>\n" +
				"        </div>\n" +
				"    </div>\n" +
				"</body>\n" +
				"</html>";

		// 메일보내기
		supportForm.setFromaddr(emailSender);
		supportForm.setToaddr(emailReceiver);
		supportForm.setSubject(emailSubject);
		supportForm.setMessage(emailMsgTxt);
		supportForm.setStatus(0);

		supportDao.insertSupportMail(supportForm);

		logger.info("======> sendPaymentCompleteMail : " + paymentVo.getUserEmail());
	}


	/**
	 * contact 메일 보내기 - 2020. 01. 10 - LYJ
	 * 사용자가 문의하는 내용을 사내 contact 메일로 보내기
	 */
	public void sendContactMail(SupportForm supportForm) throws Exception {
		logger.info(" @ MailSender.sendContactMail()!! ");

		ResultDto resultDto = new ResultDto();
		int resultCnt;

		logger.info("Msg ==> " + supportForm.getMessage());
		try {
			resultCnt = supportDao.insertSupportMail(supportForm);
			System.out.println("resultCnt = " + resultCnt);

			if (resultCnt > 0) {
				resultDto.setState("SUCCESS");
				System.out.println("Success");
				logger.info("======> sendContactMail success : " + supportForm.getFromaddr() + " / To : " + supportForm.getToaddr());
			} else {
				resultDto.setState("FAIL");
				System.out.println("Fail");
				logger.info("======> sendContactMail fail : " + supportForm.getFromaddr() + " / To : " + supportForm.getToaddr());
			}

		} catch (Exception e) {
			System.out.println(e);
		}
	}

	/**
	 * 구글 사용자 메일 계정 아이디/패스 정보
	 */
	private class SMTPAuthenticator extends javax.mail.Authenticator {
		public PasswordAuthentication getPasswordAuthentication() {
			String username =  "sendmail@mindslab.ai"; 	// gmail 사용자;
			String password = "minds1234"; 					// 패스워드;
			return new PasswordAuthentication(username, password);
		}
	}



//*****************************************************************************************************************
/* 회원 status 변경에 따른 영어 이메일 */
//*****************************************************************************************************************

	/**
	 * Welcome mail - 2020.04.14 - MRS
	 * DESC : 구글 로그인을 통해 접속한 사용자에게 보내는 메일 (INIT 사용자)
	 */
	public void enSendWelcomeMail(String receiver) throws Exception {
		logger.info(" @ MailSender.enSendWelcomeMail()!! ");

		SupportForm supportForm = new SupportForm();

		MemberDto memberDto = memberDao.getMemberDetail(receiver);

		String emailReceiver = receiver;       		// 메일 받는 사람
		String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String emailSubject = memberDto.getName() + ", Welcome to maum.ai.";	// 제목
		String emailMsgTxt = "<!DOCTYPE html> \n<html lang=\"en\"> \n<head> \n<meta charset=\"utf-8\"> \n<title>Email page</title> \n</head> \n" +
				"<body style=\"margin:0; padding:0; width:100%; height:100%; font-family:'Noto Sans KR', sans-serif;background:#f9f9f9; font-size:12px; color:#666; line-height:18px;\"> \n" +
				"\n<div id=\"wrap\" style=\"width:648px;margin:0 auto;\"> \n\t\n\t\n" +
				"\t<div id=\"header\" style=\"height:96px;position: relative;background: #fff; background: url(https://maum.ai/aiaas/common/images/mail/header_web_maumlogo.png) center center no-repeat;;\"> \t\t\n" +
				"\t\t<h1 style=\"float: left;margin: 24px 0;\"><a href=\"https://maum.ai\" alt=\"logo\" style=\"margin: 0 auto;display: block;height:100%; width: 200px; position: absolute; top: 0; left:0;\"></a></h1>\n\t</div> \n" +
				"\t\n\t\n\t<div id=\"container\" style=\"width:648px;margin:0 auto;\"> \n\t\t\n" +
				"\t\t<div id=\"content\" style=\"font-size:16px;color: #000;background: #fff;padding: 40px 24px 32px; box-sizing: border-box;text-align: center;\"> \n" +
				"\t\t\t<h5 style=\"font-size:27px;margin: 0 0 24px 0;height:25px;line-height: 25px;text-align: left;\">Welcome!</h5>\n" +
				"\t\t\t<p style=\"margin:0 0 48px 0;line-height: 1.5;text-align: left;\">"+memberDto.getName()+" thanks for signing up for maum.ai. Just a few more steps for your 1-month free trial service. You can explore 30+ AI engines and applications within our platform.\n <br><br>Here are some ways how maum.ai can help you with artificial intelligence.  </p>\n" +
				"\t\t\t<strong style=\"color: #e66f83;font-weight: bold;display: block;margin:0 0 40px 0;\">maum.ai Platform</strong>\n\n\t\t\t\n\t\t\t<img src=\"https://maum.ai/aiaas/common/images/mail/ico_welcome_woman.png\" alt=\"computer image\" style=\"margin: 0 0 40px 0;width:169px;\"/>\n" +
				"\t\t\t<ul style=\"text-align:center;list-style-position : inside;font-size: 20px;font-weight: bold; line-height: 1.85;padding:0;margin:0 0 88px 0;\">\n\t\t\t\t<li style=\"margin:0;\">User friendly AI engines</li>\n\t\t\t\t<li style=\"margin:0;\">Tips for business cases</li>\n\t\t\t\t<li style=\"margin:0;\">1:1 AI consulting in demand</li>\n\t\t\t\t<li style=\"margin:0;\">Open API documentation</li>\n\t\t\t\t<li style=\"margin:0;\">Tutorial by maum Academy</li>\n\t\t\t</ul>\n\t\t\t\n" +
				"\t\t\t<strong style=\"color:#00a0b0;font-weight: bold;display: block;margin:0 0 40px 0;\">Beyond the Platform</strong>\n\t\t\t<img src=\"https://maum.ai/aiaas/common/images/mail/ico_welcome_man.png\" alt=\"computer image\" style=\"margin: 0 0 40px 0;width:85px;\"/>\n\t\t\t<ul style=\"text-align:center;list-style-position : inside;font-size: 20px;font-weight: bold; line-height: 1.85;padding:0;margin:0 0 48px 0;\">\n" +
				"\t\t\t\t<li style=\"margin:0;\">Customized AI Service</li>\n\t\t\t\t<li style=\"margin:0;\">Data cleansing & processing</li>\n\t\t\t\t<li style=\"margin:0;\">Purchase data set</li>\t\t\t\t\n\t\t\t</ul>\n\t\t\t\n" +
				"\t\t\t<a href=\"https://maum.ai/\" target=\"_blank\" style=\"display: block; width: 214px;height: 48px;margin:0 auto;border-radius: 8px;background-color: #0080df;color:#fff;line-height: 48px;text-align: center;font-weight: 300;\">Sign in to maum.ai</a>\n" +
				"\t\t</div> \n\t\t\n\t\t<div class=\"contact\" style=\"background: #fff; padding: 0 24px 40px; color: #000;font-size:14px; line-height: 1.43; font-weight: 600;\">\n\t\t\t<p style=\"margin:0 0 24px 0;\">Your success is our business <br>maumAI</p>\n\n" +
				"\t\t\t<p><strong>Contact Us</strong><br>\n\t\t\t\tsupport@mindslab.ai<br>\n\t\t\t\t1661 - 3222</p> \n\t\t</div>\n\t</div> \n\t\n\t\n</div> \n\n\t\n\t<div id=\"footer\" style=\"text-align: center; padding: 32px 0 96px;\"> \n" +
				"\t\t<p style=\"font-size:16px;color: #818181;\">follow us</p>\n\t\t<div class=\"social_link\" style=\"margin:10px 0 32px 0;\">\n" +
				"\t\t\t<a href=\"https://www.facebook.com/mindsinsight/\" alt=\"facebook\" target=\"_blank\" style=\"width:31px;height:31px; display: inline-block;margin:0 20px 0 0;background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_facebook.png) center center no-repeat; background-size: cover;\"></a>\n" +
				"\t\t\t<a href=\"https://kr.linkedin.com/company/mindslabai\" alt=\"linkedin\" target=\"_blank\" style=\"width:32px;height:31px; display: inline-block;margin:0 20px 0 0;background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_linkedin.png) center center no-repeat; background-size: cover;\"></a>\n" +
				"\t\t\t<a href=\"https://www.youtube.com/channel/UC8ocQaJFp-adzGtwcBuS3iQ\" target=\"_blank\" alt=\"youtube\" style=\"width:32px;height:32px; display: inline-block;margin:0 20px 0 0; background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_youtube.png) center center no-repeat;margin:0; background-size: cover;\" ></a>\n\t\t</div>\n" +
				"\t\t<p class=\"address\" style=\"font-size:11px; line-height: 1.62;\">601 Dasan Tower, 49 Daewangpangyo-ro 644beon-gil, Bundang-gu, Seongnam-si, Gyeonggi-do, Republic of Korea</p>\n" +
				"\t\t<div class=\"link\" style=\"\">\n\t\t\t<a href=\"https://maum.ai/\" target=\"_blank\" style=\"text-align: center;color: #818181;font-size:13px;text-decoration: underline;\">maum.ai</a>\n\t\t</div>\n\t</div> \n\t\n<script>\n\n</script>\t\n</body> \n</html>\n";
		// 메일보내기
		supportForm.setFromaddr(emailSender);
		supportForm.setToaddr(emailReceiver);
		supportForm.setSubject(emailSubject);
		supportForm.setMessage(emailMsgTxt);
		supportForm.setStatus(0);

		supportDao.insertSupportMail(supportForm);

		logger.info("======> enSendWelcomeMail : " + receiver);
	}



	/**
	 * Thank you for joining mail - 2020.04.14 - MRS
	 * DESC : 최초 카드 등록한 사용자에게 보내는 메일 (FREE 사용자)
	 */
	public void enSendFreeUserMail(String receiver) throws Exception {
		logger.info(" @ MailSender.enSendFreeUserMail()!! ");

		SupportForm supportForm = new SupportForm();

		MemberDto memberDto = memberDao.getMemberDetail(receiver);

		String emailReceiver = receiver;       		// 메일 받는 사람
		String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String emailSubject = memberDto.getName() + ", thank you for your subscription.";	// 제목
		String emailMsgTxt = "<!DOCTYPE html> \n" +
				"<html lang=\"en\"> \n" +
				"<head> \n" +
				"<meta charset=\"utf-8\"> \n" +
				"<title>Email page</title> \n" +
				"</head> \n" +
				"<body style=\"margin:0; padding:0; width:100%; height:100%; font-family:'Noto Sans KR', sans-serif;background:#f9f9f9; font-size:12px; color:#666; line-height:18px;\"> \n" +
				"<!-- #wrap --> \n" +
				"<div id=\"wrap\" style=\"width:648px;margin:0 auto;\"> \n" +
				"\t\n" +
				"\t <!-- #header --> \n" +
				"\t<div id=\"header\" style=\"height:96px;position: relative;background: #fff; background: url(https://maum.ai/aiaas/common/images/mail/header_web_maumlogo.png) center center no-repeat;;\"> \t\t\n" +
				"\t\t<h1 style=\"float: left;margin: 24px 0;\"><a href=\"https://maum.ai\" alt=\"logo\" style=\"margin: 0 auto;display: block;height:100%; width: 200px; position: absolute; top: 0; left:0;\"></a></h1>\n" +
				"\t</div> \n" +
				"\t<!-- //#header --> \n" +
				"\t<!-- //#container --> \n" +
				"\t<div id=\"container\" style=\"width:648px;margin:0 auto;\"> \n" +
				"\t\t<!-- content --> \n" +
				"\t\t<div id=\"content\" style=\"font-size:16px;color: #000;background: #fff;padding: 40px 24px 32px; box-sizing: border-box;text-align: center;\"> \n" +
				"\t\t\t<h5 style=\"font-size:27px;margin: 0 0 24px 0;height:25px;line-height: 25px;text-align: left;\">Thank you for joining us!</h5>\n" +
				"\t\t\t<p style=\"margin:0 0 48px 0;line-height: 1.5;text-align: left;\">"+memberDto.getName()+", welcome to maum.ai. Now, you have full access to all of our artificial intelligence services. <br><br>\n" +
				"\n" +
				"Your 1-month free trial begins from now. We are here for you to get the most out of maum.ai! \n" +
				" </p>\n" +
				"\t\t\t<strong style=\"color: #e66f83;font-weight: bold;display: block;margin:0 0 24px 0;\">maum.ai Platform</strong>\n" +
				"\n" +
				"\t\t\t\n" +
				"\t\t\t<img src=\"https://maum.ai/aiaas/common/images/mail/ico_thankyou_woman.png\" alt=\"computer image\" style=\"margin: 0 0 24px 0;width:99px;\"/>\n" +
				"\t\t\t<ul style=\"text-align:center;list-style-position : inside;font-size: 20px;font-weight: bold; line-height: 1.85;padding:0;margin:0 0 24px 0;\">\n" +
				"\t\t\t\t<li style=\"margin:0;\">User friendly AI engines</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">Tips for business cases</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">1:1 AI consulting in demand</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">Open API documentation</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">Tutorial by maum Academy</li>\n" +
				"\t\t\t</ul>\t\t\t\n" +
				"\t\t\t\n" +
				"\t\t\t<a href=\"https://maum.ai/\" target=\"_blank\" style=\"display: block; width: 214px;height: 48px;margin:0 auto;border-radius: 8px;background-color: #0080df;color:#fff;line-height: 48px;text-align: center;font-weight: 300;\">Sign in to maum.ai</a>\n" +
				"\t\t</div> \n" +
				"\t\t<!-- //#content -->\n" +
				"\t\t<div class=\"contact\" style=\"background: #fff; padding: 0 24px 40px; color: #000;font-size:14px; line-height: 1.43; font-weight: 600;\">\n" +
				"\t\t\t<p style=\"margin:0 0 24px 0;\">Your success is our business <br>\t\t\t\t\n" +
				"maumAI</p>\n" +
				"\n" +
				"\t\t\t<p><strong>Contact Us</strong><br>\n" +
				"\t\t\t\tsupport@mindslab.ai<br>\n" +
				"\t\t\t\t1661 - 3222</p> \n" +
				"\t\t</div>\n" +
				"\t</div> \n" +
				"\t<!-- //#container --> \n" +
				"\t\n" +
				"</div> \n" +
				"<!-- //#wrap --> \n" +
				"\t<!-- # footer -->\n" +
				"\t<div id=\"footer\" style=\"text-align: center; padding: 32px 0 96px;\"> \n" +
				"\t\t<p style=\"font-size:16px;color: #818181;\">follow us</p>\n" +
				"\t\t<div class=\"social_link\" style=\"margin:10px 0 32px 0;\">\n" +
				"\t\t\t<a href=\"https://www.facebook.com/mindsinsight/\" alt=\"facebook\" target=\"_blank\" style=\"width:31px;height:31px; display: inline-block;margin:0 20px 0 0;background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_facebook.png) center center no-repeat; background-size: cover;\"></a>\n" +
				"\t\t\t<a href=\"https://kr.linkedin.com/company/mindslabai\" alt=\"linkedin\" target=\"_blank\" style=\"width:32px;height:31px; display: inline-block;margin:0 20px 0 0;background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_linkedin.png) center center no-repeat; background-size: cover;\"></a>\n" +
				"\t\t\t<a href=\"https://www.youtube.com/channel/UC8ocQaJFp-adzGtwcBuS3iQ\" target=\"_blank\" alt=\"youtube\" style=\"width:32px;height:32px; display: inline-block;margin:0 20px 0 0; background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_youtube.png) center center no-repeat;margin:0; background-size: cover;\" ></a>\n" +
				"\t\t</div>\n" +
				"\t\t<p class=\"address\" style=\"font-size:11px; line-height: 1.62;\">601 Dasan Tower, 49 Daewangpangyo-ro 644beon-gil, Bundang-gu, Seongnam-si, Gyeonggi-do, Republic of Korea</p>\n" +
				"\t\t<div class=\"link\" style=\"\">\n" +
				"\t\t\t<a href=\"https://maum.ai/\" target=\"_blank\" style=\"text-align: center;color: #818181;font-size:13px;text-decoration: underline;\">maum.ai</a>\n" +
				"\t\t</div>\n" +
				"\t</div> \n" +
				"\t<!-- //# footer --> \n" +
				"<script>\n" +
				"\n" +
				"</script>\t\n" +
				"</body> \n" +
				"</html>\n";							// 내용
		// 메일보내기

		System.out.println("Length : " + emailMsgTxt.length());
		supportForm.setFromaddr(emailSender);
		supportForm.setToaddr(emailReceiver);
		supportForm.setSubject(emailSubject);
		supportForm.setMessage(emailMsgTxt);
		supportForm.setStatus(0);

		supportDao.insertSupportMail(supportForm);

		logger.info("======> enSendFreeUserMail : " + receiver);
	}


	/**
	 * Thank you for joining mail - 2020.05.12 - LYJ
	 * DESC : 재구독한 사용자(구독 취소 후 카드 재등록한 사용자)에게 보내는 메일
	 */
	public void enSendSubscribeUserMail(String receiver) throws Exception {
		logger.info(" @ MailSender.enSendSubscribeUserMail()!! ");

		SupportForm supportForm = new SupportForm();

		MemberDto memberDto = memberDao.getMemberDetail(receiver);

		String emailReceiver = receiver;       		// 메일 받는 사람
		String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String emailSubject = memberDto.getName() + ", thank you for your subscription.";	// 제목
		String emailMsgTxt = "<!DOCTYPE html> \n" +
				"<html lang=\"en\"> \n" +
				"<head> \n" +
				"<meta charset=\"utf-8\"> \n" +
				"<title>Email page</title> \n" +
				"</head> \n" +
				"<body style=\"margin:0; padding:0; width:100%; height:100%; font-family:'Noto Sans KR', sans-serif;background:#f9f9f9; font-size:12px; color:#666; line-height:18px;\"> \n" +
				"<!-- #wrap --> \n" +
				"<div id=\"wrap\" style=\"width:648px;margin:0 auto;\"> \n" +
				"\t\n" +
				"\t <!-- #header --> \n" +
				"\t<div id=\"header\" style=\"height:96px;position: relative;background: #fff; background: url(https://maum.ai/aiaas/common/images/mail/header_web_maumlogo.png) center center no-repeat;;\"> \t\t\n" +
				"\t\t<h1 style=\"float: left;margin: 24px 0;\"><a href=\"https://maum.ai\" alt=\"logo\" style=\"margin: 0 auto;display: block;height:100%; width: 200px; position: absolute; top: 0; left:0;\"></a></h1>\n" +
				"\t</div> \n" +
				"\t<!-- //#header --> \n" +
				"\t<!-- //#container --> \n" +
				"\t<div id=\"container\" style=\"width:648px;margin:0 auto;\"> \n" +
				"\t\t<!-- content --> \n" +
				"\t\t<div id=\"content\" style=\"font-size:16px;color: #000;background: #fff;padding: 40px 24px 32px; box-sizing: border-box;text-align: center;\"> \n" +
				"\t\t\t<h5 style=\"font-size:27px;margin: 0 0 24px 0;height:25px;line-height: 25px;text-align: left;\">Welcome Back" + memberDto.getName() + "!</h5>\n" +
				"\t\t\t<p style=\"margin:0 0 48px 0;line-height: 1.5;text-align: left;\">"+memberDto.getName()+", welcome to maum.ai. Now, you have full access to all of our artificial intelligence services. <br><br>\n" +
				"\n" +
				"We are here for you to get the most out of maum.ai! \n" +
				" </p>\n" +
				"\t\t\t<strong style=\"color: #e66f83;font-weight: bold;display: block;margin:0 0 24px 0;\">maum.ai Platform</strong>\n" +
				"\n" +
				"\t\t\t\n" +
				"\t\t\t<img src=\"https://maum.ai/aiaas/common/images/mail/ico_thankyou_woman.png\" alt=\"computer image\" style=\"margin: 0 0 24px 0;width:99px;\"/>\n" +
				"\t\t\t<ul style=\"text-align:center;list-style-position : inside;font-size: 20px;font-weight: bold; line-height: 1.85;padding:0;margin:0 0 24px 0;\">\n" +
				"\t\t\t\t<li style=\"margin:0;\">User friendly AI engines</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">Tips for business cases</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">1:1 AI consulting in demand</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">Open API documentation</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">Tutorial by maum Academy</li>\n" +
				"\t\t\t</ul>\t\t\t\n" +
				"\t\t\t\n" +
				"\t\t\t<a href=\"https://maum.ai/\" target=\"_blank\" style=\"display: block; width: 214px;height: 48px;margin:0 auto;border-radius: 8px;background-color: #0080df;color:#fff;line-height: 48px;text-align: center;font-weight: 300;\">Sign in to maum.ai</a>\n" +
				"\t\t</div> \n" +
				"\t\t<!-- //#content -->\n" +
				"\t\t<div class=\"contact\" style=\"background: #fff; padding: 0 24px 40px; color: #000;font-size:14px; line-height: 1.43; font-weight: 600;\">\n" +
				"\t\t\t<p style=\"margin:0 0 24px 0;\">Your success is our business <br>\t\t\t\t\n" +
				"maumAI</p>\n" +
				"\n" +
				"\t\t\t<p><strong>Contact Us</strong><br>\n" +
				"\t\t\t\tsupport@mindslab.ai<br>\n" +
				"\t\t\t\t1661 - 3222</p> \n" +
				"\t\t</div>\n" +
				"\t</div> \n" +
				"\t<!-- //#container --> \n" +
				"\t\n" +
				"</div> \n" +
				"<!-- //#wrap --> \n" +
				"\t<!-- # footer -->\n" +
				"\t<div id=\"footer\" style=\"text-align: center; padding: 32px 0 96px;\"> \n" +
				"\t\t<p style=\"font-size:16px;color: #818181;\">follow us</p>\n" +
				"\t\t<div class=\"social_link\" style=\"margin:10px 0 32px 0;\">\n" +
				"\t\t\t<a href=\"https://www.facebook.com/mindsinsight/\" alt=\"facebook\" target=\"_blank\" style=\"width:31px;height:31px; display: inline-block;margin:0 20px 0 0;background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_facebook.png) center center no-repeat; background-size: cover;\"></a>\n" +
				"\t\t\t<a href=\"https://kr.linkedin.com/company/mindslabai\" alt=\"linkedin\" target=\"_blank\" style=\"width:32px;height:31px; display: inline-block;margin:0 20px 0 0;background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_linkedin.png) center center no-repeat; background-size: cover;\"></a>\n" +
				"\t\t\t<a href=\"https://www.youtube.com/channel/UC8ocQaJFp-adzGtwcBuS3iQ\" target=\"_blank\" alt=\"youtube\" style=\"width:32px;height:32px; display: inline-block;margin:0 20px 0 0; background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_youtube.png) center center no-repeat;margin:0; background-size: cover;\" ></a>\n" +
				"\t\t</div>\n" +
				"\t\t<p class=\"address\" style=\"font-size:11px; line-height: 1.62;\">601 Dasan Tower, 49 Daewangpangyo-ro 644beon-gil, Bundang-gu, Seongnam-si, Gyeonggi-do, Republic of Korea</p>\n" +
				"\t\t<div class=\"link\" style=\"\">\n" +
				"\t\t\t<a href=\"https://maum.ai/\" target=\"_blank\" style=\"text-align: center;color: #818181;font-size:13px;text-decoration: underline;\">maum.ai</a>\n" +
				"\t\t</div>\n" +
				"\t</div> \n" +
				"\t<!-- //# footer --> \n" +
				"<script>\n" +
				"\n" +
				"</script>\t\n" +
				"</body> \n" +
				"</html>\n";							// 내용
		// 메일보내기

		System.out.println("Length : " + emailMsgTxt.length());
		supportForm.setFromaddr(emailSender);
		supportForm.setToaddr(emailReceiver);
		supportForm.setSubject(emailSubject);
		supportForm.setMessage(emailMsgTxt);
		supportForm.setStatus(0);

		supportDao.insertSupportMail(supportForm);

		//postMail(emailReceiver, emailSubject, emailMsgTxt, emailSender);

		logger.info("======> enSendSubscribeUserMail : " + receiver);
	}


	/**
	 * Payment error - 2020.04.14 - MRS
	 * DESC : 정기 결제 실패 시, 사용자에게 보내는 메일 (SUBSCRIBED 사용자)
	 */
	public void enSendPaymentErrorUserMail(String receiver) throws Exception {
		logger.info(" @ MailSender.enSendPaymentErrorUserMail()!! ");

		SupportForm supportForm = new SupportForm();

		MemberDto memberDto = memberDao.getMemberDetail(receiver);

		String emailReceiver = receiver;       		// 메일 받는 사람
		String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String emailSubject = "Please check your payment status!";	// 제목
		String emailMsgTxt = "<!DOCTYPE html> \n" +
				"<html lang=\"en\"> \n" +
				"<head> \n" +
				"<meta charset=\"utf-8\"> \n" +
				"<title>Email page</title> \n" +
				"</head> \n" +
				"<body style=\"margin:0; padding:0; width:100%; height:100%; font-family:'Noto Sans KR', sans-serif;background:#f9f9f9; font-size:12px; color:#666; line-height:18px;\"> \n" +
				"<!-- #wrap --> \n" +
				"<div id=\"wrap\" style=\"width:648px;margin:0 auto;\"> \n" +
				"\t\n" +
				"\t <!-- #header --> \n" +
				"\t<div id=\"header\" style=\"height:96px;position: relative;background: #fff; background: url(https://maum.ai/aiaas/common/images/mail/header_web_maumlogo.png) center center no-repeat;;\"> \t\t\n" +
				"\t\t<h1 style=\"float: left;margin: 24px 0;\"><a href=\"https://maum.ai\" alt=\"로고\" style=\"margin: 0 auto;display: block;height:100%; width: 200px; position: absolute; top: 0; left:0;\"></a></h1>\n" +
				"\t</div> \n" +
				"\t<!-- //#header --> \n" +
				"\t<!-- //#container --> \n" +
				"\t<div id=\"container\" style=\"width:648px;margin:0 auto;\"> \n" +
				"\t\t<!-- content --> \n" +
				"\t\t<div id=\"content\" style=\"font-size:16px;color: #000;background: #fff;padding: 40px 24px 32px; box-sizing: border-box;text-align: center;\"> \n" +
				"\t\t\t<h5 style=\"font-size:27px;margin: 0 0 24px 0;height:25px;line-height: 25px;text-align: left;\">Please confirm!</h5>\n" +
				"\t\t\t<p style=\"margin:0 0 48px 0;line-height: 1.5;text-align: left; color:#000\">"+memberDto.getName()+", we have a problem with your payment. <br><br>\n" +
				"\n" +
				"Please check your payment information in order to resume your service. If you still have some problems, just email us at support@mindslab.ai. Our team is always ready to help you." +
				" </p>\n" +
				"\t\t\t<strong style=\"color: #e66f83;font-weight: bold;display: block;margin:0 0 24px 0;\">Check your card’s</strong>\n" +
				"\n" +
				"\t\t\t\n" +
				"\t\t\t<img src=\"https://maum.ai/aiaas/common/images/mail/ico_payerror_man.png\" alt=\"computer image\" style=\"margin: 0 0 16px 0;width:117px;\"/>\n" +
				"\t\t\t<ul style=\"text-align:center;list-style-position : inside;font-size: 20px;font-weight: bold; line-height: 1.85;padding:0;margin:0 0 88px 0;\">\n" +
				"\t\t\t\t<li style=\"margin:0;\">Expiration date</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">Lost or suspended card</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">Payment date and limit amount</li>\t\t\t\t\n" +
				"\t\t\t</ul>\t\t\t\n" +
				"\t\t\t\n" +
				"\t\t\t<a href=\"https://maum.ai/\" target=\"_blank\" style=\"display: block; width: 214px;height: 48px;margin:0 auto;border-radius: 8px;background-color: #0080df;color:#fff;line-height: 48px;text-align: center;font-weight: 300;\">Move to maum.ai</a>\n" +
				"\t\t</div> \n" +
				"\t\t<!-- //#content -->\n" +
				"\t\t<div class=\"contact\" style=\"background: #fff; padding: 0 24px 40px; color: #000;font-size:14px; line-height: 1.43; font-weight: 600;\">\n" +
				"\t\t\t<p style=\"margin:0 0 24px 0;\">Your success is our business <br>\t\t\t\t\n" +
				"maumAI </p>\n" +
				"\n" +
				"\t\t\t<p><strong>Contact Us</strong><br>\n" +
				"\t\t\t\tsupport@mindslab.ai<br>\n" +
				"\t\t\t\t1661 - 3222</p> \n" +
				"\t\t</div>\n" +
				"\t</div> \n" +
				"\t<!-- //#container --> \n" +
				"\t\n" +
				"</div> \n" +
				"<!-- //#wrap --> \n" +
				"\t<!-- # footer -->\n" +
				"\t<div id=\"footer\" style=\"text-align: center; padding: 32px 0 96px;\"> \n" +
				"\t\t<p style=\"font-size:16px;color: #818181;\">follow us</p>\n" +
				"\t\t<div class=\"social_link\" style=\"margin:10px 0 32px 0;\">\n" +
				"\t\t\t<a href=\"https://www.facebook.com/mindsinsight/\" alt=\"facebook\" target=\"_blank\" style=\"width:31px;height:31px; display: inline-block;margin:0 20px 0 0;background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_facebook.png) center center no-repeat; background-size: cover;\"></a>\n" +
				"\t\t\t<a href=\"https://kr.linkedin.com/company/mindslabai\" alt=\"linkedin\" target=\"_blank\" style=\"width:32px;height:31px; display: inline-block;margin:0 20px 0 0;background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_linkedin.png) center center no-repeat; background-size: cover;\"></a>\n" +
				"\t\t\t<a href=\"https://www.youtube.com/channel/UC8ocQaJFp-adzGtwcBuS3iQ\" alt=\"youtube\" target=\"_blank\" style=\"width:32px;height:32px; display: inline-block;margin:0 20px 0 0; background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_youtube.png) center center no-repeat;margin:0; background-size: cover;\" ></a>\n" +
				"\t\t</div>\n" +
				"\t\t<p class=\"address\" style=\"font-size:11px; line-height: 1.62;\">601 Dasan Tower, 49 Daewangpangyo-ro 644beon-gil, Bundang-gu, Seongnam-si, Gyeonggi-do, Republic of Korea</p>\n" +
				"\t\t<div class=\"link\" style=\"\">\n" +
				"\t\t\t<a href=\"https://maum.ai/\" target=\"_blank\" style=\"text-align: center;color: #818181;font-size:13px;text-decoration: underline;\">maum.ai</a>\n" +
				"\t\t</div>\n" +
				"\t</div> \n" +
				"\t<!-- //# footer --> \n" +
				"<script>\n" +
				"\n" +
				"</script>\t\n" +
				"</body> \n" +
				"</html>\n";

		// 메일보내기
		supportForm.setFromaddr(emailSender);
		supportForm.setToaddr(emailReceiver);
		supportForm.setSubject(emailSubject);
		supportForm.setMessage(emailMsgTxt);
		supportForm.setStatus(0);

		supportDao.insertSupportMail(supportForm);

		//postMail(emailReceiver, emailSubject, emailMsgTxt, emailSender);

		logger.info("======> enSendPaymentErrorUserMail : " + receiver);
	}


	/**
	 * Request cancel subscription - 2020.04.14 - MRS
	 * DESC : 구독 해지를 신청한 유료 사용자에게 보내는 메일 (SUBSCRIBED -> UNSUBSCRIBING 사용자)
	 */
	public void enSendReqCancelUserMail(String receiver) throws Exception {
		logger.info(" @ MailSender.enSendReqCancelUserMail()!! ");

		SupportForm supportForm = new SupportForm();

		MemberDto memberDto = memberDao.getMemberDetail(receiver);

		String emailReceiver = receiver;       		// 메일 받는 사람
		String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String emailSubject = "You have canceled all services from maum.ai.";	// 제목
		String emailMsgTxt = "<!DOCTYPE html> \n" +
				"<html lang=\"en\"> \n" +
				"<head> \n" +
				"<meta charset=\"utf-8\"> \n" +
				"<title>Email page</title> \n" +
				"</head> \n" +
				"<body style=\"margin:0; padding:0; width:100%; height:100%; font-family:'Noto Sans KR', sans-serif;background:#f9f9f9; font-size:12px; color:#666; line-height:18px;\"> \n" +
				"<!-- #wrap --> \n" +
				"<div id=\"wrap\" style=\"width:648px;margin:0 auto;\"> \n" +
				"\t\n" +
				"\t <!-- #header --> \n" +
				"\t<div id=\"header\" style=\"height:96px;position: relative;background: #fff; background: url(https://maum.ai/aiaas/common/images/mail/header_web_maumlogo.png) center center no-repeat;;\"> \t\t\n" +
				"\t\t<h1 style=\"float: left;margin: 24px 0;\"><a href=\"https://maum.ai\" alt=\"logo\" style=\"margin: 0 auto;display: block;height:100%; width: 200px; position: absolute; top: 0; left:0;\"></a></h1>\n" +
				"\t</div> \n" +
				"\t<!-- //#header --> \n" +
				"\t<!-- //#container --> \n" +
				"\t<div id=\"container\" style=\"width:648px;margin:0 auto;\"> \n" +
				"\t\t<!-- content --> \n" +
				"\t\t<div id=\"content\" style=\"font-size:16px;color: #000;background: #fff;padding: 40px 24px 32px; box-sizing: border-box;text-align: center;\"> \n" +
				"\t\t\t<h5 style=\"font-size:27px;margin: 0 0 24px 0;height:25px;line-height: 25px;text-align: left;\">Subscription cancelled!</h5>\n" +
				"\t\t\t<p style=\"margin:0 0 48px 0;line-height: 1.5;text-align: left;\">Hi "+memberDto.getName()+", this is a confirmation that your subscription has been canceled at  your request. <br><br> \n" +
				"\n" +
				"You still have access to your account until your next payment date. Later, you will no longer available to access your account.<br><br> \n" +
				"\n" +
				"To reactivate your account please visit maum.ai or just email us at support@mindslab.ai.\n" +
				" </p>\n" +
				"\t\t<img src=\"https://maum.ai/aiaas/common/images/mail/ico_unsubing_woman.png\" alt=\"computer image\" style=\"margin: 0;width:243px;\"/>\n" +
				"\t\t<a href=\"https://maum.ai/\" target=\"_blank\" style=\"display: block; width: 214px;height: 48px;margin:0 auto;border-radius: 8px;background-color: #0080df;color:#fff;line-height: 48px;text-align: center;font-weight: 300;\">Subscription Renewal</a>\n" +
				"\t\t</div> \n" +
				"\t\t<!-- //#content -->\n" +
				"\t\t<div class=\"contact\" style=\"background: #fff; padding: 0 24px 40px; color: #000;font-size:14px; line-height: 1.43; font-weight: 600;\">\n" +
				"\t\t\t<p style=\"margin:0 0 24px 0;\">Your success is our business<br>\t\t\t\t\n" +
				"maumAI </p>\n" +
				"\n" +
				"\t\t\t<p><strong>Contact Us</strong><br>\n" +
				"\t\t\t\tsupport@mindslab.ai<br>\n" +
				"\t\t\t\t1661 - 3222</p> \n" +
				"\t\t</div>\n" +
				"\t</div> \n" +
				"\t<!-- //#container --> \n" +
				"\t\n" +
				"</div> \n" +
				"<!-- //#wrap --> \n" +
				"\t<!-- # footer -->\n" +
				"\t<div id=\"footer\" style=\"text-align: center; padding: 32px 0 96px;\"> \n" +
				"\t\t<p style=\"font-size:16px;color: #818181;\">follow us</p>\n" +
				"\t\t<div class=\"social_link\" style=\"margin:10px 0 32px 0;\">\n" +
				"\t\t\t<a href=\"https://www.facebook.com/mindsinsight/\" alt=\"facebook\" target=\"_blank\" style=\"width:31px;height:31px; display: inline-block;margin:0 20px 0 0;background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_facebook.png) center center no-repeat; background-size: cover;\"></a>\n" +
				"\t\t\t<a href=\"https://kr.linkedin.com/company/mindslabai\" alt=\"linkedin\" target=\"_blank\" style=\"width:32px;height:31px; display: inline-block;margin:0 20px 0 0;background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_linkedin.png) center center no-repeat; background-size: cover;\"></a>\n" +
				"\t\t\t<a href=\"https://www.youtube.com/channel/UC8ocQaJFp-adzGtwcBuS3iQ\" target=\"_blank\" alt=\"youtube\" style=\"width:32px;height:32px; display: inline-block;margin:0 20px 0 0; background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_youtube.png) center center no-repeat;margin:0; background-size: cover;\" ></a>\n" +
				"\t\t</div>\n" +
				"\t\t<p class=\"address\" style=\"font-size:11px; line-height: 1.62;\">601 Dasan Tower, 49 Daewangpangyo-ro 644beon-gil, Bundang-gu, Seongnam-si, Gyeonggi-do, Republic of Korea</p>\n" +
				"\t\t<div class=\"link\" style=\"\">\n" +
				"\t\t\t<a href=\"https://maum.ai/\" target=\"_blank\" style=\"text-align: center;color: #818181;font-size:13px;text-decoration: underline;\">maum.ai</a>\n" +
				"\t\t</div>\n" +
				"\t</div> \n" +
				"\t<!-- //# footer --> \n" +
				"<script>\n" +
				"\n" +
				"</script>\t\n" +
				"</body> \n" +
				"</html>\n";
		// 메일보내기
		supportForm.setFromaddr(emailSender);
		supportForm.setToaddr(emailReceiver);
		supportForm.setSubject(emailSubject);
		supportForm.setMessage(emailMsgTxt);
		supportForm.setStatus(0);

		supportDao.insertSupportMail(supportForm);

		//postMail(emailReceiver, emailSubject, emailMsgTxt, emailSender);

		logger.info("======> enSendReqCancelUserMail : " + receiver);
	}


	/**
	 * Complete cancel subscription - 2020.04.14 - MRS
	 * DESC : 구독 해지 신청한 사용자의 사용 기한이 만료되거나, 결제 실패 후 일정 기한까지 재 결제가 안 됐을 때
	 *        구독 사용자에게 보내는 메일 (UNSUBSCRIBED 사용자)
	 */
	public void enSendExpiredUserMail(String receiver) throws Exception {
		logger.info(" @ MailSender.enSendExpiredUserMail()!! ");

		SupportForm supportForm = new SupportForm();

		MemberDto memberDto = memberDao.getMemberDetail(receiver);

		String emailReceiver = receiver;       		// 메일 받는 사람
		String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String emailSubject = memberDto.getName() + ", your account has been expired from maum.ai.";	// 제목
		String emailMsgTxt = "<!DOCTYPE html> \n" +
				"<html lang=\"en\"> \n" +
				"<head> \n" +
				"<meta charset=\"utf-8\"> \n" +
				"<title>Email page</title> \n" +
				"</head> \n" +
				"<body style=\"margin:0; padding:0; width:100%; height:100%; font-family:'Noto Sans KR', sans-serif;background:#f9f9f9; font-size:12px; color:#666; line-height:18px;\"> \n" +
				"<!-- #wrap --> \n" +
				"<div id=\"wrap\" style=\"width:648px;margin:0 auto;\"> \n" +
				"\t\n" +
				"\t <!-- #header --> \n" +
				"\t<div id=\"header\" style=\"height:96px;position: relative;background: #fff; background: url(https://maum.ai/aiaas/common/images/mail/header_web_maumlogo.png) center center no-repeat;;\"> \t\t\n" +
				"\t\t<h1 style=\"float: left;margin: 24px 0;\"><a href=\"https://maum.ai\" alt=\"logo\" style=\"margin: 0 auto;display: block;height:100%; width: 200px; position: absolute; top: 0; left:0;\"></a></h1>\n" +
				"\t</div> \n" +
				"\t<!-- //#header --> \n" +
				"\t<!-- //#container --> \n" +
				"\t<div id=\"container\" style=\"width:648px;margin:0 auto;\"> \n" +
				"\t\t<!-- content --> \n" +
				"\t\t<div id=\"content\" style=\"font-size:16px;color: #000;background: #fff;padding: 40px 24px 32px; box-sizing: border-box;text-align: center;\"> \n" +
				"\t\t\t<h5 style=\"font-size:27px;margin: 0 0 24px 0;height:25px;line-height: 25px;text-align: left;\">Hope to see you again</h5>\n" +
				"\t\t\t<p style=\"margin:0 0 48px 0;line-height: 1.5;text-align: left;\">Hi "+memberDto.getName()+", your subscription of maum.ai has been expired.\n <br><br>\n" +
				"\n" +
				"Unfortunately, all services from maum.ai will be no longer provided. <br><br>\n" +
				"\n" +
				"Thank you for using maum.ai, and we hope to have you re-join us in the future! Our team is always be available for you.\n" +
				" </p>\n" +
				"\t\t\t\t\t\t\n" +
				"\t\t\t<img src=\"https://maum.ai/aiaas/common/images/mail/ico_unsubed_man.png\" alt=\"computer image\" style=\"margin: 0 0 24px 0;width:238px;\"/>\n" +
				"\t\t\t<strong style=\"color: #e66f83;font-weight: bold;display: block;margin:0 0 20px 0;\">Reminder of our benefits</strong>\n" +
				"\n" +
				"\t\t\t<ul style=\"text-align:center;list-style-position : inside;font-size: 20px;font-weight: bold; line-height: 1.85;padding:0;margin:0 0 24px 0;\">\n" +
				"\t\t\t\t<li style=\"margin:0;\">User friendly AI engines</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">Tips for business cases</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">1:1 AI consulting in demand</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">Open API documentation</li>\n" +
				"\t\t\t\t<li style=\"margin:0;\">Tutorial by maum Academy  </li>\n" +
				"\t\t\t</ul>\t\t\t\n" +
				"\t\t\t\n" +
				"\t\t\t<a href=\"https://maum.ai/\" target=\"_blank\" style=\"display: block; width: 214px;height: 48px;margin:0 auto;border-radius: 8px;background-color: #0080df;color:#fff;line-height: 48px;text-align: center;font-weight: 300;\">Resubscribe maum.ai</a>\n" +
				"\t\t</div> \n" +
				"\t\t<!-- //#content -->\n" +
				"\t\t<div class=\"contact\" style=\"background: #fff; padding: 0 24px 40px; color: #000;font-size:14px; line-height: 1.43; font-weight: 600;\">\n" +
				"\t\t\t<p style=\"margin:0 0 24px 0;\">Your success is our business<br>\t\t\t\t\n" +
				"maumAI </p>\n" +
				"\n" +
				"\t\t\t<p><strong>Contact Us</strong><br>\n" +
				"\t\t\t\tsupport@mindslab.ai<br>\n" +
				"\t\t\t\t1661 - 3222</p> \n" +
				"\t\t</div>\n" +
				"\t</div> \n" +
				"\t<!-- //#container --> \n" +
				"\t\n" +
				"</div> \n" +
				"<!-- //#wrap --> \n" +
				"\t<!-- # footer -->\n" +
				"\t<div id=\"footer\" style=\"text-align: center; padding: 32px 0 96px;\"> \n" +
				"\t\t<p style=\"font-size:16px;color: #818181;\">follow us</p>\n" +
				"\t\t<div class=\"social_link\" style=\"margin:10px 0 32px 0;\">\n" +
				"\t\t\t<a href=\"https://www.facebook.com/mindsinsight/\" alt=\"facebook\" target=\"_blank\" style=\"width:31px;height:31px; display: inline-block;margin:0 20px 0 0;background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_facebook.png) center center no-repeat; background-size: cover;\"></a>\n" +
				"\t\t\t<a href=\"https://kr.linkedin.com/company/mindslabai\" alt=\"linkedin\" target=\"_blank\" style=\"width:32px;height:31px; display: inline-block;margin:0 20px 0 0;background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_linkedin.png) center center no-repeat; background-size: cover;\"></a>\n" +
				"\t\t\t<a href=\"https://www.youtube.com/channel/UC8ocQaJFp-adzGtwcBuS3iQ\" target=\"_blank\" alt=\"youtube\" style=\"width:32px;height:32px; display: inline-block;margin:0 20px 0 0; background:url(https://maum.ai/aiaas/common/images/mail/ico_footer_social_youtube.png) center center no-repeat;margin:0; background-size: cover;\" ></a>\n" +
				"\t\t</div>\n" +
				"\t\t<p class=\"address\" style=\"font-size:11px; line-height: 1.62;\">601 Dasan Tower, 49 Daewangpangyo-ro 644beon-gil, Bundang-gu, Seongnam-si, Gyeonggi-do, Republic of Korea</p>\n" +
				"\t\t<div class=\"link\" style=\"\">\n" +
				"\t\t\t<a href=\"https://maum.ai/\" target=\"_blank\" style=\"text-align: center;color: #818181;font-size:13px;text-decoration: underline;\">maum.ai</a>\n" +
				"\t\t</div>\n" +
				"\t</div> \n" +
				"\t<!-- //# footer --> \n" +
				"<script>\n" +
				"\n" +
				"</script>\t\n" +
				"</body> \n" +
				"</html>\n";

		// 메일보내기
		supportForm.setFromaddr(emailSender);
		supportForm.setToaddr(emailReceiver);
		supportForm.setSubject(emailSubject);
		supportForm.setMessage(emailMsgTxt);
		supportForm.setStatus(0);

		supportDao.insertSupportMail(supportForm);

		//postMail(emailReceiver, emailSubject, emailMsgTxt, emailSender);

		logger.info("======> enSendExpiredUserMail : " + receiver);
	}


	public void sendNotiToConnieMail(String customerEmail, String problemMsg) throws Exception {
		logger.info(" @ MailSender.sendNotiToConnieMail()!! ");

		SupportForm supportForm = new SupportForm();

//		MemberDto memberDto = memberDao.getMemberDetail(receiver);
		String receiver = "yge0723@mindslab.ai";
		String emailReceiver = receiver;       		// 메일 받는 사람
		String emailSender = "sendmail@mindslab.ai";		// 메일 보내는 사람
		String emailSubject = "Paypal 개발 확인용 메일";	// 제목
		String emailMsgTxt = "<!DOCTYPE html> \n" +
				"<html lang=\"ko\"> \n" +
				"<head> \n" +
				"<meta charset=\"utf-8\"> \n" +
				"<title>Email page</title> \n" +
				"</head> \n" +

				"<body style=\"margin:0; padding:0; width:100%; height:100%; font-family:'Noto Sans KR', sans-serif;background:#f9f9f9; font-size:12px; color:#666; line-height:18px;\"> \n" +
				"<!-- #wrap --> \n" +
				"<div id=\"wrap\" style=\"width:648px;margin:0 auto;\"> \n" +
				"\t\n" +

				"\t <!-- #header --> \n" +
				"\t<div id=\"header\" style=\"height:96px;position: relative;background: #fff; background: url(https://maum.ai/aiaas/common/images/mail/header_web_maumlogo.png) center center no-repeat;;\"> \t\t\n" +
				"\t\t<h1 style=\"float: left;margin: 24px 0;\"><a href=\"https://maum.ai\" alt=\"로고\" style=\"margin: 0 auto;display: block;height:100%; width: 200px; position: absolute; top: 0; left:0;\"></a></h1>\n" +
				"\t</div> \n" +
				"\t<!-- //#header --> \n" +

				"\t<!-- //#container --> \n" +
				"\t<div id=\"container\" style=\"width:648px;margin:0 auto;\"> \n" +

				"\t\t<!-- content --> \n" +
				"\t\t<div id=\"content\" style=\"font-size:16px;color: #000;background: #fff;padding: 40px 24px 32px; box-sizing: border-box;text-align: center;\"> \n" +
				"\t\t\t<h5 style=\"font-size:27px;margin: 0 0 24px 0;height:25px;line-height: 25px;text-align: left;\">paypal 결제 실패했습니다!</h5>\n" +
				"\t\t\t<p style=\"margin:0 0 48px 0;line-height: 1.5;text-align: left;\">"+customerEmail+"님의 페이팔 결제 처리중 보내진 알림입니다. <br><br> \n" +
				"\n" +
				problemMsg + "<br><br> \n" +
				"로그와 DB를 확인하세요!! <br><br> \n" +
				"\n" +
				" </p>\n" +
				"\t\t<img src=\"https://maum.ai/aiaas/common/images/mail/ico_unsubing_woman.png\" alt=\"computer image\" style=\"margin: 0;width:243px;\"/>\n" +
				"\t\t<a href=\"#none\" target=\"_blank\" style=\"display: block; width: 214px;height: 48px;margin:0 auto;border-radius: 8px;background-color: #0080df;color:#fff;line-height: 48px;text-align: center;font-weight: 300;\">테스트용 메일입니다.</a>\n" +
				"\t\t</div> \n" +
				"\t\t<!-- //#content -->\n" +

				"\t\t<div class=\"contact\" style=\"background: #fff; padding: 0 24px 40px; color: #000;font-size:14px; line-height: 1.43; font-weight: 600;\">\n" +
				"\n" +
				"\t\t</div>\n" +
				"\t</div> \n" +
				"\t<!-- //#container --> \n" +

				"\t\n" +
				"</div> \n" +
				"<!-- //#wrap --> \n" +
				"</body> \n" +
				"</html>\n";
		// 메일보내기
		supportForm.setFromaddr(emailSender);
		supportForm.setToaddr(emailReceiver);
		supportForm.setSubject(emailSubject);
		supportForm.setMessage(emailMsgTxt);
		supportForm.setStatus(0);

		supportDao.insertSupportMail(supportForm);

		//postMail(emailReceiver, emailSubject, emailMsgTxt, emailSender);

		logger.info("======> sendReqCancelUserMailTo : " + receiver);
	}

	public String changeSimpleDateFormat(String strDate) {
		try {
			SimpleDateFormat dtFormat = new SimpleDateFormat("yyyyMMdd");
			SimpleDateFormat newDtFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date formatDate = dtFormat.parse(strDate); // Date타입의 변수를 새롭게 지정한 포맷으로 변환
			String strNewDtFormat = newDtFormat.format(formatDate);
			return strNewDtFormat;
		} catch(java.text.ParseException e) {
			logger.error(" @ changeSimpleDateFormat error !");
			return null;
		}
	}
}
