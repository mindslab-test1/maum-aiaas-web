package console.maum.ai.common.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import console.maum.ai.common.model.MasterCodeForm;
import console.maum.ai.common.model.ResultDto;
import console.maum.ai.common.service.CommonService;

@Controller
@RequestMapping(value="/common")
public class CommonController {
	
	@Autowired
	private CommonService commonService;


	private static final Logger logger = LoggerFactory.getLogger(CommonController.class);
	
	/**국가 목록 조회 */
	@RequestMapping(value = "/getNationSelectList")
	@ResponseBody
	public ResultDto getNationSelectList(HttpServletRequest request, HttpServletResponse response, MasterCodeForm masterCodeForm) throws Exception {
		
		Gson gson = new Gson();
		
		logger.info("{} .getNationSelectList()", gson.toJson(masterCodeForm));
		// System.out.println("getNationSelectList() ======= " + gson.toJson(masterCodeForm));
		
		ResultDto resultDto = commonService.getNationSelectList(masterCodeForm);

		return resultDto;
	}	
}
