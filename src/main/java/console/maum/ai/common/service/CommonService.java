package console.maum.ai.common.service;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;

import console.maum.ai.common.dao.CommonDao;
import console.maum.ai.common.model.MasterCodeDto;
import console.maum.ai.common.model.MasterCodeForm;
import console.maum.ai.common.model.ResultDto;

import javax.servlet.http.HttpServletRequest;

@Service
public class CommonService {

    private final static Logger logger = LoggerFactory.getLogger(CommonService.class);

	@Autowired
	private CommonDao commonDao;	
	
	/**국가 목록 조회 */
	public ResultDto getNationSelectList(MasterCodeForm masterCodeForm) throws Exception {

		ResultDto resultDto = new ResultDto();

		List<MasterCodeDto> list = commonDao.getNationSelectList(masterCodeForm);
		
		// Gson gson = new Gson();
		// System.out.println("MasterCodeDto getNationSelectList ======= " + gson.toJson(list));

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);

		resultDto.setData(resultMap);
		resultDto.setState("SUCCESS");

		return resultDto;
	}

}