package console.maum.ai.common.data;

public interface CommonMsg {

    public static final int ERR_CODE_SUCCESS = 200;
    public static final String ERR_MSG_SUCCESS = "SUCCESS";

    public static final int ERR_CODE_DUPLICATE_LICENSE = 300;
    public static final String ERR_MSG_DUPLICATE_LICENSE = "ALREADY REGISTERED";

    public static final int ERR_CODE_NOT_FOUND = 404;
    public static final String ERR_MSG_NOT_FOUND = "NOT FOUND";

    public static final int ERR_CODE_FAILURE = 500;
    public static final String ERR_MSG_FAILURE = "FAILURE";

    public static final int ERR_CODE_PARAMS_INVALID = 502;
    public static final String ERR_MSG_PARAMS_INVALID = "INVALID PARAMETERS";

    public static final int ERR_CODE_INVALID_LICENSE = 503;
    public static final String ERR_MSG_INVALID_LICENSE = "INVALID LICENSE KEY";
}
