package console.maum.ai.common.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * created by yejoon3117@mindslab.ai - 2020. 11. 20
 * Desc : lang 설정을 위한 interceptor
 * */
public class LangInterceptor implements HandlerInterceptor{

    // controller로 보내기 전에 처리하는 인터셉터
    // 반환이 false라면 controller로 요청을 안함
    // 매개변수 Object는 핸들러 정보를 의미한다. ( RequestMapping , DefaultServletHandler )
    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) throws Exception {

        try {

            String uri = httpServletRequest.getRequestURI();

            Cookie[] cookies = httpServletRequest.getCookies();

            if (uri.contains("/en")) {
                for (Cookie cookie : cookies) {
                    if ("lang".equalsIgnoreCase(cookie.getName())) {
                        cookie.setPath("/");
                        cookie.setValue("en");
                        httpServletResponse.addCookie(cookie);
                    }
                }
            } else if (uri.contains("/kr")) {
                for (Cookie cookie : cookies) {
                    if ("lang".equalsIgnoreCase(cookie.getName())) {
                        cookie.setPath("/");
                        cookie.setValue("ko");
                        httpServletResponse.addCookie(cookie);
                    }
                }

            }
            return true;
        } catch (Exception e) {
            return true;
        }
    }

    // controller의 handler가 끝나면 처리됨
    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    // view까지 처리가 끝난 후에 처리됨
    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
