package console.maum.ai.common.interceptor;

import console.maum.ai.member.constant.UserStatus;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.member.service.MemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * created by yejoon3117@mindslab.ai - 2021. 05. 17
 * Desc : Cloud API 페이지 제한 설정을 위한 interceptor
 * */
public class CloudPageInterceptor implements HandlerInterceptor {

    @Autowired
    MemberService memberService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {

        HttpSession session = request.getSession();
        MemberVo memberVo = (MemberVo) session.getAttribute("accessUser");
        int userStatus = memberVo.getStatus();
        int privacyAgree = memberService.getMemberDetail(memberVo.getEmail()).getPrivacyAgree();

        if(userStatus == UserStatus.FREE || userStatus == UserStatus.SUBSCRIBED || userStatus == UserStatus.UNSUBSCRIBING
            || (userStatus==UserStatus.INIT && privacyAgree == 1)) {
                    // 접속 가능 회원
        } else {    // 접속 불가능 회원 --> 결제 페이지로 redirection
            response.sendRedirect(request.getContextPath() + "/login/krPayment");
            return false;
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
