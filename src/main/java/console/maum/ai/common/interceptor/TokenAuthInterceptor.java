package console.maum.ai.common.interceptor;

import com.google.gson.JsonElement;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.oauth.model.AuthToken;
import console.maum.ai.oauth.service.OauthService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TokenAuthInterceptor extends HandlerInterceptorAdapter {

    private static final Logger logger = LoggerFactory.getLogger(TokenAuthInterceptor.class);

    @Autowired
    private OauthService oauthService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler){
        try{
            HttpSession session = request.getSession();
            MemberVo memberVo = (MemberVo) session.getAttribute("accessUser");
            AuthToken token = (AuthToken)session.getAttribute("token");

            if ( token == null ){
                return true;
            }

            // access_token expire time 확인
            if (isExpired(token.getAccessTokenExpTime())){
                if(isExpired(token.getRefreshTokenExpTime())){
                    logger.debug("{} :: refresh_token도 만료", memberVo.getEmail());
                    response.sendRedirect(request.getContextPath() + "/");
                    return false;
                }
                else{
                    // refreshToken으로 AccessToken 재발급
                    JsonElement newToken = oauthService.getAccessTokenWithRefresh(token.getRefreshToken());

                    if(newToken == null){
                        return true;
                    }
                    // Token 세팅
                    token.setAccessToken(newToken.getAsJsonObject().get("access_token").getAsString());
                    token.setAccessTokenExpTime(newToken.getAsJsonObject().get("access_expire_time").getAsString());
                    token.setRefreshToken(newToken.getAsJsonObject().get("refresh_token").getAsString());
                    token.setRefreshTokenExpTime(newToken.getAsJsonObject().get("refresh_expire_time").getAsString());

                    // 세션 세팅
                    session.setAttribute("token", token);
                }
            }
        }catch (Exception e){
            logger.error(e.toString());
        }

        return true;
    }


    // expire된 경우 true
    private boolean isExpired(String expTime){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date expDate;
        Date curDate;
        try {
            expDate = sdf.parse(expTime);
            curDate = sdf.parse(sdf.format(Calendar.getInstance().getTime()));
            return expDate.getTime() < curDate.getTime() ;
        } catch (ParseException e) {
            e.printStackTrace();
            return false;
        }

    }

}
