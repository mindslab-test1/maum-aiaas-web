package console.maum.ai.common.dao;

import java.util.List;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import console.maum.ai.common.model.MasterCodeDto;
import console.maum.ai.common.model.MasterCodeForm;

@Repository
public class CommonDao {


	@Resource(name = "sqlSession")
	private SqlSession sqlSession;

	private static final String NAMESPACE = "console.maum.ai.model.commonMapper";

	/**국가 목록 조회 */
	public List<MasterCodeDto> getNationSelectList(MasterCodeForm masterCodeForm) throws Exception {

		return sqlSession.selectList(NAMESPACE + ".getNationSelectList", masterCodeForm);
	}	
}