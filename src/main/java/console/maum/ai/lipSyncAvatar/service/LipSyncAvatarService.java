package console.maum.ai.lipSyncAvatar.service;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Optional;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})

@Service
public class LipSyncAvatarService {

    private static final Logger logger = LoggerFactory.getLogger(LipSyncAvatarService.class);

    @Value("${api.url}")
    private String mUrl_ApiServer;

    public ResponseEntity<String> lipSyncAvatar(String apiId, String apiKey, String uploadPath,
                                                Optional<MultipartFile> imageFile, String text, String model, boolean transparent, String width, String height) {

        String url = mUrl_ApiServer + "/lipsync/v2/upload";

        String logMsg = "\n===========================================================================\n";
        logMsg += "LipSyncAvatar API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "backgroundImage", imageFile.isPresent() ? imageFile.get().getOriginalFilename() : "");
        logMsg += String.format(":: %-10s = %s%n", "text", text);
        logMsg += String.format(":: %-10s = %s%n", "model", model);
        logMsg += String.format(":: %-10s = %s%n", "transparent", transparent);
        logMsg += String.format(":: %-10s = %s%n", "resolution.width", width);
        logMsg += String.format(":: %-10s = %s%n", "resolution.height", height);
        logMsg += "===========================================================================";
        logger.info(logMsg);


        try (CloseableHttpClient client = HttpClients.createDefault()){

            HttpPost post = new HttpPost(url);


            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));

            File imageVarFile = new File(uploadPath);
            if(imageFile.isPresent()){
                if (!imageVarFile.exists())
                    imageVarFile.mkdirs();

                imageVarFile = new File((uploadPath + apiId + "_" + imageFile.get().getOriginalFilename().substring(imageFile.get().getOriginalFilename().lastIndexOf("\\") + 1)));
                imageFile.get().transferTo(imageVarFile);

                FileBody imageBody = new FileBody(imageVarFile);

                builder.addPart("backgroundImage", imageBody);
            }
            builder.addPart("text", new StringBody(text, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("model", new StringBody(model, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("transparent", new StringBody(String.valueOf(transparent), ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("resolution.width", new StringBody(width, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("resolution.height", new StringBody(height, ContentType.TEXT_PLAIN.withCharset("UTF-8")));

            HttpEntity entity = builder.build();
            post.setEntity(entity);

            HttpResponse response = client.execute(post);
            HttpEntity responseEntity = response.getEntity();
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("Avatar responseCode = {}" , responseCode);

            // Response ERROR
            if (responseCode != 200) {
                imageVarFile.delete();
                String responseString = "";
                if(responseEntity!=null) {
                    responseString = EntityUtils.toString(responseEntity, "UTF-8");
                }
                logger.error("LipSyncAvatar fail : \n{}", responseString);
                return null;
            }

            // Response OK
            String resultMsg = EntityUtils.toString(responseEntity, "UTF-8");
            logger.debug(resultMsg);
            return new ResponseEntity<>(resultMsg, HttpStatus.OK);

        } catch (Exception e) {
            logger.error(" @ LipSyncAvatar Service Exception ! ==> " + e);
        }
        return null;
    }
}
