package console.maum.ai.lipSyncAvatar.controller;

import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.lipSyncAvatar.service.LipSyncAvatarService;
import console.maum.ai.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Optional;

@Controller
public class LipSyncAvatarApiController {

    @Autowired
    private LipSyncAvatarService service;

    @RequestMapping(value = "/api/lipSyncAvatar")
    public ResponseEntity<String> lipSyncAvatar(@RequestParam(value = "image") Optional<MultipartFile> image,
                                                @RequestParam(value = "text") String text,
                                                @RequestParam(value = "model") String model,
                                                @RequestParam(value = "transparent") boolean transparent,
                                                @RequestParam(value = "width") String width,
                                                @RequestParam(value = "height") String height,
                                                HttpServletRequest request)
    {
        HttpSession httpSession = request.getSession(true);

        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        return service.lipSyncAvatar(apiId, apiKey, PropertyUtil.getUploadPath() + "/lipSyncAvatar/", image, text, model, transparent, width, height);
    }
}
