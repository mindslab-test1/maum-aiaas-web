package console.maum.ai.lipSyncAvatar.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/lipSyncAvatar")
public class LipSyncAvatarController {

    private static final Logger logger = LoggerFactory.getLogger(LipSyncAvatarController.class);

    @Value("${api.url}")
    private String apiUrl;

    /**
     * Lip Sync Avatar Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/lipSyncAvatarMain")
    public String lipSyncAvatarMain(HttpServletRequest request, Model model) {
        logger.info("Welcome lipSyncAvatarMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "lipSyncAvatarMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "Lip Sync Avatar");
        httpSession.setAttribute("menuGrpName", "시각");

        httpSession.setAttribute("apiUrl", apiUrl);

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/lipSyncAvatar/lipSyncAvatarMain.pg";

        return returnUri;
    }

    /**
     * Lip Sync Avatar Main (En)
     * @param model
     * @return
     */
    @RequestMapping(value = "/enLipSyncAvatarMain")
    public String enLipSyncAvatarMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enLipSyncAvatarMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "lipSyncAvatarMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Lip Sync Avatar");
        httpSession.setAttribute("menuGrpName", "Vision");

        httpSession.setAttribute("apiUrl", apiUrl);

        model.addAttribute("lang", "en");
        String returnUri = "/en/lipSyncAvatar/lipSyncAvatarMain.pg";

        return returnUri;
    }


}
