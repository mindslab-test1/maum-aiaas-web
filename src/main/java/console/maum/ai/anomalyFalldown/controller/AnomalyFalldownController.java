package console.maum.ai.anomalyFalldown.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/anomalyFalldown")
public class AnomalyFalldownController {

    private static final Logger logger = LoggerFactory.getLogger(AnomalyFalldownController.class);

    /**
     * anomalyFalldown Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/krAnomalyFalldownMain")
    public String krAnomalyFalldownMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krAnomalyFalldownMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "anomalyFalldownMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "실신 감지");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/anomalyFalldown/anomalyFalldownMain.pg";

        return returnUri;
    }

    @RequestMapping(value = "/enAnomalyFalldownMain")
    public String enAnomalyFalldownMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enAnomalyFalldownMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "anomalyFalldownMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Fall Down Detection");
        httpSession.setAttribute("menuGrpName", "Vision");

        model.addAttribute("lang", "en");
        String returnUri = "/en/anomalyFalldown/anomalyFalldownMain.pg";

        return returnUri;
    }


}