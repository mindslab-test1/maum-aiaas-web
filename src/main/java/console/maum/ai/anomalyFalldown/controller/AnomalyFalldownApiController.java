package console.maum.ai.anomalyFalldown.controller;


import console.maum.ai.anomalyFalldown.service.AnomalyFalldownService;
import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
public class AnomalyFalldownApiController {

    @Autowired
    private AnomalyFalldownService AnomalyFalldownService;


    @RequestMapping(value="/api/anomalyFalldown")
    @ResponseBody
    public ResponseEntity<byte[]> apiAnomalyFalldown(@RequestParam MultipartFile video, @RequestParam String roiList, HttpServletRequest request){

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        return AnomalyFalldownService.apiAnomalyFalldown(apiId, apiKey, video, roiList, PropertyUtil.getUploadPath() + "/falldown/");
    }


}