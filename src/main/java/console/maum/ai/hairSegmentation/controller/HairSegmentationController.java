package console.maum.ai.hairSegmentation.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;



/*
 * 화면 Controller
 */

@Controller
@RequestMapping(value = "/cloudApi/hairSegmentation")


public class HairSegmentationController {
    private static final Logger logger = LoggerFactory.getLogger(HairSegmentationController.class);


    /**
     * HairSegmentation Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/hairSegmentationMain")
    public String krHairSegmentation(HttpServletRequest request, Model model) {
        logger.info("Welcome krHairSegmentationMain");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "hairSegmentationMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "헤어 컬러 인식");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/hairSegmentation/hairSegmentationMain.pg";

        return returnUri;

    }

    @RequestMapping(value = "/enHairSegmentationMain")
    public String enHairSegmentation(HttpServletRequest request, Model model) {
        logger.info("Welcome enHairSegmentationMain");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "hairSegmentationMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Hair Segmentation");
        httpSession.setAttribute("menuGrpName", "Vision");

        model.addAttribute("lang", "en");
        String returnUri = "/en/hairSegmentation/hairSegmentationMain.pg";

        return returnUri;

    }
}
