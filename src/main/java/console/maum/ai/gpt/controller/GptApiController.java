package console.maum.ai.gpt.controller;

import java.io.IOException;

import console.maum.ai.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import console.maum.ai.gpt.service.GptApiService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/*
 * API Controller
 */

@Controller
public class GptApiController {
	
	@Autowired
	private GptApiService gptApiService;
	
	@RequestMapping(value="/api/gpt", produces = "application/text; charset=utf8")
	@ResponseBody
	public String getApiGpt(
			@RequestParam(value = "context") String context
    		,@RequestParam(value = "lang") String lang
			, HttpServletRequest request) throws IOException {

		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		String apiId = memberVo.getApiId();
		String apiKey = memberVo.getApiKey();


		String result = gptApiService.getApiGpt(apiId, apiKey, context, lang);
		return result;
	}
}
