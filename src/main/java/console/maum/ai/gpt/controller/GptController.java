package console.maum.ai.gpt.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/cloudApi/gpt")
public class GptController {

	private static final Logger logger = LoggerFactory.getLogger(GptController.class);

	/**
	 * gpt Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krGptMain")
	public String krGptMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krGptMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "gptMain");
		httpSession.setAttribute("menuName", "언어");
		httpSession.setAttribute("subMenuName", "문장 생성");
		httpSession.setAttribute("menuGrpName", "언어");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/gpt/gptMain.pg";	
		
		return returnUri;
	}
	
	/**
	 * gpt Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enGptMain")
	public String enGptMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enGptMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "gptMain");
		httpSession.setAttribute("menuName", "Languages");
		httpSession.setAttribute("subMenuName", "GPT-2");
		httpSession.setAttribute("menuGrpName", "Languages");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/gpt/gptMain_Tmp.pg";
		
		return returnUri;
	}	

}