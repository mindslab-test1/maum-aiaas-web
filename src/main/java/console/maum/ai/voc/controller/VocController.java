package console.maum.ai.voc.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/voc")
public class VocController {

	private static final Logger logger = LoggerFactory.getLogger(VocController.class);
	
	@RequestMapping(value = "/krVocMain")
	public String krVocMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krVocMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "VOC 분석");		
		httpSession.setAttribute("subMenuName", "VOC 분석");
		httpSession.setAttribute("menuName", "Application Services");
		httpSession.setAttribute("menuGrpName", "");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/voc/vocMain.pg";
		
		return returnUri;
	}
	
	@RequestMapping(value = "/enVocMain")
	public String enVocMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enVocMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "maum VOC");		
		httpSession.setAttribute("subMenuName", "maum VOC");
		httpSession.setAttribute("menuName", "Application Services");
		httpSession.setAttribute("menuGrpName", "");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/voc/vocMain.pg";
		
		return returnUri;
	}	
	
}
