package console.maum.ai.vr.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/vr")
public class VrController {

    private static final Logger logger = LoggerFactory.getLogger(VrController.class);

    @RequestMapping(value = "/krVoiceRecogMain")
    public String krVoiceRecogMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krVoiceRecogMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "voiceRecogMain");
        httpSession.setAttribute("menuName", "음성");
        httpSession.setAttribute("subMenuName", "Voice Recognition");
        httpSession.setAttribute("menuGrpName", "음성");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/vr/voiceRecogMain.pg";

        return returnUri;
    }

    @RequestMapping(value = "/enVoiceRecogMain")
    public String enVoiceRecogMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enVoiceRecogMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "voiceRecogMain");
        httpSession.setAttribute("menuName", "Voice");
        httpSession.setAttribute("subMenuName", "Voice Recognition");
        httpSession.setAttribute("menuGrpName", "Speech");

        model.addAttribute("lang", "en");
        String returnUri = "/en/vr/voiceRecogMain.pg";

        return returnUri;
    }
}
