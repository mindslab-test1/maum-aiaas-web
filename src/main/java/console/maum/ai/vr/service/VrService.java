package console.maum.ai.vr.service;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.client.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})

@Service
public class VrService {

    private static final Logger logger = LoggerFactory.getLogger(VrService.class);

    @Value("${api.url}")
    private String mUrl_ApiServer;


    public void setVoice(String apiId, String apiKey, String dbId, String voiceId, MultipartFile file, String uploadPath, String saveId) throws Exception {


        String url = mUrl_ApiServer + "/dap/app/setVoice";

        String logMsg = "\n===========================================================================\n";
        logMsg += "VoiceRecog(setVoice) API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "file", file.getOriginalFilename());
        logMsg += String.format(":: %-10s = %s%n", "dbId", dbId);
        logMsg += String.format(":: %-10s = %s%n", "voiceId", voiceId);
        logMsg += "===========================================================================";
        logger.info(logMsg);

        HttpClient client = HttpClients.createDefault();
        HttpPut put = new HttpPut(url);
        File voiceRecogFile = new File(uploadPath);

        if(!voiceRecogFile.exists()) {
            logger.info("create Dir : {}", voiceRecogFile.getPath());
            voiceRecogFile.mkdirs();
        }

        voiceRecogFile = new File( (uploadPath + "/" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1) ) );
        logger.info("Dest File Name = {}", voiceRecogFile.getAbsolutePath());
        file.transferTo(voiceRecogFile);
        FileBody vrFileBody = new FileBody(voiceRecogFile);

        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
        builder.addPart("file", vrFileBody);
        builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
        builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));
        builder.addPart("voiceId", new StringBody(voiceId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
        builder.addPart("dbId", new StringBody(dbId, ContentType.MULTIPART_FORM_DATA));

        HttpEntity entity = builder.build();
        put.setEntity(entity);
        HttpResponse response = client.execute(put);

        int responseCode = response.getStatusLine().getStatusCode();
        logger.info("responseCode = {}" , responseCode);

        if(responseCode != 200) {
            throw new Exception("setVoice api fail");
        }
    }

    public void deleteVoice(String apiId, String apiKey, String voiceId, String dbId) {
        String url = mUrl_ApiServer + "/dap/app/deleteVoice";
        String lineEnd = "\r\n";
        String twoHyphens = "--";
        String boundary = "3FS4PKI7YH9S";

        String logMsg = "\n===========================================================================\n";
        logMsg += "VoiceRecog(deleteVoice) API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "dbId", dbId);
        logMsg += String.format(":: %-10s = %s%n", "voiceId", voiceId);
        logMsg += "===========================================================================";
        logger.info(logMsg);

        try{
            URL connectURL = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) connectURL.openConnection();
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setUseCaches(false);
            conn.setRequestMethod("DELETE");
            conn.setRequestProperty("Connection", "Keep-Alive");    // connection을 위함
            conn.setRequestProperty("Content-type", "multipart/form-data;boundary=" + boundary);
            conn.setConnectTimeout(15000);

            DataOutputStream dos = new DataOutputStream(conn.getOutputStream());

            StringBuffer sb = new StringBuffer();

            sb.append(twoHyphens+boundary+lineEnd);
            sb.append("Content-Disposition: form-data; name=\"apiId\"" + lineEnd);
            sb.append(lineEnd);
            sb.append(apiId+lineEnd);

            sb.append(twoHyphens+boundary+lineEnd);
            sb.append("Content-Disposition: form-data; name=\"apiKey\"" + lineEnd);
            sb.append(lineEnd);
            sb.append(apiKey+lineEnd);

            sb.append(twoHyphens+boundary+lineEnd);
            sb.append("Content-Disposition: form-data; name=\"voiceId\"" + lineEnd);
            sb.append(lineEnd);
            sb.append(voiceId+lineEnd);

            sb.append(twoHyphens+boundary+lineEnd);
            sb.append("Content-Disposition: form-data; name=\"dbId\"" + lineEnd);
            sb.append(lineEnd);
            sb.append(dbId+lineEnd);

            sb.append(twoHyphens+boundary+twoHyphens+lineEnd);

            dos.writeUTF(sb.toString());
            dos.flush();

            int responseCode = conn.getResponseCode();

            if(responseCode != 200){
                throw new Exception("deleteVoice api fail");
            }
        } catch (Exception e) {
            logger.info(" @ voiceRecognition.deleteVoice Error : {}", e.toString());
        }
    }

    public ResponseEntity<String> recogVoice(String apiId, String apiKey,MultipartFile file, String dbId, String uploadPath, String saveId) {
        String url = mUrl_ApiServer + "/dap/app/recogVoice";

        String logMsg = "\n===========================================================================\n";
        logMsg += "VoiceRecog(recogVoice) API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "file", file.getOriginalFilename());
        logMsg += String.format(":: %-10s = %s%n", "dbId", dbId);
        logMsg += "===========================================================================";
        logger.info(logMsg);

        try{
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File recogVoiceFile = new File(uploadPath);

            if(!recogVoiceFile.exists()) {
                recogVoiceFile.mkdirs();
            }

            recogVoiceFile = new File((uploadPath + "/" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));

            logger.info("Dest File Name = " + recogVoiceFile.getAbsolutePath());

            file.transferTo(recogVoiceFile);

            FileBody vrFileBody = new FileBody(recogVoiceFile);

            MultipartEntityBuilder builder = MultipartEntityBuilder.create();

            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("file", vrFileBody);
            builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));
            builder.addPart("dbId", new StringBody(dbId, ContentType.MULTIPART_FORM_DATA));

            HttpEntity entity = builder.build();

            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            int responseCode = response.getStatusLine().getStatusCode();
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Type", "application/json;charset=UTF-8");

            if(responseCode == 200){
                BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));

                StringBuffer result = new StringBuffer();
                String line = "";
                while((line = rd.readLine()) != null){
                    result.append(line);
                }

                ResponseEntity<String> resultEntity = new ResponseEntity<String>(result.toString(), headers, HttpStatus.OK);

                logger.info("recogVoice response ======= success ======= ");
                return resultEntity;
            } else {
                logger.info("recogVoice response ======= fail ======= ");
                throw new Exception("recogVoice api response fail");
            }
        } catch (Exception e) {
            logger.info(" @ voiceRecognition.recogVoice Error : " + e);
        }

        return null;
    }
}
