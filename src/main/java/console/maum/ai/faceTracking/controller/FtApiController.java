package console.maum.ai.faceTracking.controller;

import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.faceTracking.service.FtService;
import console.maum.ai.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class FtApiController {

    @Autowired
    private FtService ftService;

    @RequestMapping(value = "/api/faceTracking")
    public ResponseEntity<byte[]> multiFaceTracking(@RequestParam MultipartFile file, HttpServletRequest request) {

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getId();

        return ftService.getApiFt(apiId, apiKey, file, PropertyUtil.getUploadPath(), savedId);
    }
}
