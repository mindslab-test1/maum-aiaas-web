package console.maum.ai.faceTracking.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/faceTracking")
public class FtController {

    private static final Logger logger = LoggerFactory.getLogger(FtController.class);

    /**
     * faceTracking Main
     * @param model
     * @return page_url
     */
    @RequestMapping(value = "/krFaceTrackingMain")
    public String krFaceTracking(HttpServletRequest request, Model model) {
        logger.info("Welcome krFaceTrackingMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "faceTrackingMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "얼굴 추적");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/faceTracking/faceTrackingMain.pg";

        return returnUri;
    }
    @RequestMapping(value = "/enFaceTrackingMain")
    public String enFaceTrackingMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enFaceTrackingMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "faceTrackingMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Face Tracking");
        httpSession.setAttribute("menuGrpName", "Vision");

        model.addAttribute("lang", "en");
        String returnUri = "/en/faceTracking/faceTrackingMain.pg";

        return returnUri;
    }
}