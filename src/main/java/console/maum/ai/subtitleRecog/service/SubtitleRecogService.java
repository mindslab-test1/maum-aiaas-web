package console.maum.ai.subtitleRecog.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})

@Slf4j
@Service
public class SubtitleRecogService {
    private static final Logger logger = LoggerFactory.getLogger(SubtitleRecogService.class);
    @Value("${api.url}")
    private String mUrl_ApiServer;

/*
    @Value("admin.api.id")
    private String mAdmin_ApiId;

    @Value("admin.api.pw")
    private String mAdmin_APiKey;
*/

    public  ResponseEntity<byte[]> recog(String apiId, String apiKey, MultipartFile file, String uploadPath, String saveId){

        String url = mUrl_ApiServer + "/Vca/VCASubtitleExtract";

        String logMsg = "\n===========================================================================\n";
        logMsg += "SubtitleRecog API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "scene_img", file.getOriginalFilename());
        logMsg += "===========================================================================";
        logger.info(logMsg);

        try{
            HttpClient client = HttpClients.createDefault();
            HttpPost post = new HttpPost(url);
            File varFile = new File(uploadPath);

            if(!varFile.exists()) {
                varFile.mkdirs();
            }

            varFile = new File((uploadPath + "/" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));
            file.transferTo(varFile);
            FileBody fileBody = new FileBody(varFile);

            log.debug("Dest File Name = {}", varFile.getAbsolutePath());
            log.info(" @ FaceTracking uploadPath : {}", uploadPath);


            MultipartEntityBuilder builder = MultipartEntityBuilder.create();
            builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            builder.addPart("apiId", new StringBody(apiId, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("apiKey",new StringBody(apiKey, ContentType.TEXT_PLAIN.withCharset("UTF-8")));
            builder.addPart("scene_img", fileBody);

            HttpEntity entity = builder.build();
            post.setEntity(entity);
            HttpResponse response = client.execute(post);
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("responseCode = {}" , responseCode);

            if (responseCode != 200) {
                varFile.delete();
                logger.error("API fail.");
                throw new RuntimeException(" @ FaceTracking ErrCode : " + response);
            }

            HttpEntity responseEntity = response.getEntity();
            HttpHeaders headers = new HttpHeaders();
            InputStream in = responseEntity.getContent();
            byte[] imageArray = IOUtils.toByteArray(in);

            ResponseEntity<byte[]> resultEntity = new ResponseEntity<>(imageArray, headers, HttpStatus.OK);

            varFile.delete();

            return resultEntity;

        } catch (Exception e) {
            logger.error("API exception : {}", e.toString());
            e.printStackTrace();
        }

        return null;
    }
}
