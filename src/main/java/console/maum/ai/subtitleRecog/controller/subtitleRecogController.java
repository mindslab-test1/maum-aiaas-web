package console.maum.ai.subtitleRecog.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;



/*
 * 화면 Controller
 */

@Controller
@RequestMapping(value = "/cloudApi/subtitleRecog")


public class subtitleRecogController {
    private static final Logger logger = LoggerFactory.getLogger(subtitleRecogController.class);


    /**
     * subtitleRecog Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/krSubtitleRecogMain")
    public String krSubtitleRecogMain(HttpServletRequest request, Model model) {
        logger.info("Welcome krSubtitleRecogMain");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "subtitleRecogMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "이미지 자막 인식");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/subtitleRecog/subtitleRecogMain.pg";

        return returnUri;

    }

    @RequestMapping(value = "/enSubtitleRecogMain")
    public String enSubtitleRecogMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enSubtitleRecogMain");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "subtitleRecogMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Subtitle Recognition");
        httpSession.setAttribute("menuGrpName", "Vision");

        model.addAttribute("lang", "en");
        String returnUri = "/en/subtitleRecog/subtitleRecogMain.pg";

        return returnUri;

    }
}
