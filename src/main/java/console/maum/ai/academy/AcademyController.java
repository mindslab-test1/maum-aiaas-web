package console.maum.ai.academy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@Controller
@RequestMapping(value = "/academy")
public class AcademyController {

    private static final Logger logger = LoggerFactory.getLogger(AcademyController.class);


    /**
     * Academy Main
     * @param model
     * @return
     */

    @RequestMapping(value = "/krAcademyMain")
    public String krAcademyMain(HttpServletRequest request, Model model) {


        logger.info("Welcome krAcademyMain!");

        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("menuName", "Academy");
        httpSession.setAttribute("menuGrpName", "");
        httpSession.setAttribute("selectMenu", "academyMain");
        model.addAttribute("lang", "ko");
        String returnUri = "/kr/academy/academyMain.pg";

        return returnUri;
    }

}
