package console.maum.ai.academy.service;


import console.maum.ai.academy.dao.AcademyListDao;
import console.maum.ai.academy.model.AcademyListVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AcademyService {
    private static final Logger logger = LoggerFactory.getLogger(AcademyService.class);

    @Autowired
    private AcademyListDao academyListDao;


    /* 아카데미 리스트 맵 형식으 가져오기 */
    public Map<String, List<AcademyListVo>> getAcademyList() throws Exception {

        logger.info(" @ Hello AcademyService.getAcademyMap ! ");

        List<AcademyListVo> academyListVo = academyListDao.getAcademyList();

        Map<String, List<AcademyListVo>> resultMap = new HashMap<>();

        for (int i=0; i< academyListVo.size(); i++){
            String key = academyListVo.get(i).getModule_name();
//            logger.info("key : {}", key);

            if(resultMap.get(key) == null ){
                List<AcademyListVo> list = new ArrayList<>();
                list.add(academyListVo.get(i));

                resultMap.put(key, list);
            }else{
                List<AcademyListVo> list = resultMap.get(key);
                list.add(academyListVo.get(i));
                resultMap.replace(key,list);
            }
        }
//        logger.info(academyListVo.toString());
        return resultMap;
    }

    /* 아카데미 리스트 전체 가져오기*/
    public List<AcademyListVo> getAcademyAllList() {
        logger.info(" @ Hello AcademyService.getAcademyAllList ! ");
        List<AcademyListVo> academyList = academyListDao.getAcademyAllList();

//        logger.info (academyList.toString());
        return academyList;

    }

    /* 아카데미 모듈 리스 가져오기 */

    public List<String> getAcademyModuleList() throws Exception {

        logger.info(" @ Hello AcademyService.getAcademyModuleList ! ");

        List<String> moduleNameList = academyListDao.getAcademyModuleList();
        logger.info(moduleNameList.toString());
        return moduleNameList;


    }

}
