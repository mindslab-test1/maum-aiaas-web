package console.maum.ai.academy.dao;


import console.maum.ai.academy.model.AcademyListVo;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class AcademyListDao {

    @Resource(name = "sqlSession")
    private SqlSession sqlSession;

    private static final String NAMESPACE = "console.maum.ai.model.academyMapper";


    public List<AcademyListVo> getAcademyList() throws Exception{
        return sqlSession.selectList(NAMESPACE+".getAcademyList");
    }
    public List<AcademyListVo> getAcademyAllList(){
        return sqlSession.selectList(NAMESPACE+".getAcademyAllList");
    }
    public List<String> getAcademyModuleList() {
        return sqlSession.selectList(NAMESPACE+".getAcademyModuleList");

    }

}
