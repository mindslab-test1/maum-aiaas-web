package console.maum.ai.academy.controller;

import console.maum.ai.academy.AcademyController;
import console.maum.ai.academy.model.AcademyListVo;
import console.maum.ai.academy.service.AcademyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/academy")
public class AcademyApiController {

    private static final Logger logger = LoggerFactory.getLogger(AcademyController.class);

    @Autowired
    private AcademyService service;

    @GetMapping("/getAcademyInfo")
    public Map<String, List<AcademyListVo>> getAcademyInfo(HttpServletRequest request) throws Exception {
        logger.info(" @ Hello getAcademyInfo controller ! ");

        return service.getAcademyList();
    }
}
