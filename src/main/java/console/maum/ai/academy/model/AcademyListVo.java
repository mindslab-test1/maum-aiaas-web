package console.maum.ai.academy.model;

import lombok.Data;

@Data
public class AcademyListVo {



        int id;
        String module_name;
        String contents_name;
        String course_target;
        String difficulty;
        String description;
        String file_format;
        String file_link;


}
