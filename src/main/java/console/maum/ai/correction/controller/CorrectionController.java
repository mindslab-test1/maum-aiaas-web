package console.maum.ai.correction.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/correction")
public class CorrectionController {

    private static final Logger logger = LoggerFactory.getLogger(CorrectionController.class);

    /**
     * correction Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/correctionMain")
    public String correctionMain(HttpServletRequest request, Model model) {
        logger.info("Welcome correctionMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "correctionMain");
        httpSession.setAttribute("menuName", "언어");
        httpSession.setAttribute("subMenuName", "문장 교정");
        httpSession.setAttribute("menuGrpName", "언어");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/correction/correctionMain.pg";

        return returnUri;
    }
    /**
     * correction Main (EN)
     * @param model
     * @return
     */
    @RequestMapping(value = "/enCorrectionMain")
    public String enCorrectionMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enCorrectionMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "correctionMain");
        httpSession.setAttribute("menuName", "Languages");
        httpSession.setAttribute("subMenuName", "Bert Correction");
        httpSession.setAttribute("menuGrpName", "Languages");

        model.addAttribute("lang", "en");
        String returnUri = "/en/correction/correctionMain.pg";

        return returnUri;
    }
}
