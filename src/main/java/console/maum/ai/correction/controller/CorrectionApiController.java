package console.maum.ai.correction.controller;


import console.maum.ai.correction.service.CorrectionService;
import console.maum.ai.member.model.MemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@RestController
public class CorrectionApiController {

    private final static Logger logger = LoggerFactory.getLogger(CorrectionApiController.class);
    @Autowired
    private CorrectionService correctionService;


    @RequestMapping(value="/api/correct/sentence", produces = "application/text; charset=utf8")
    @ResponseBody
    public String correctionApi(@RequestParam(value = "context") String context, HttpServletRequest request) throws IOException {
        logger.info("===== Correction ApiController =====");

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        String result = correctionService.correctionApi(apiId, apiKey, context);

        return result;
    }
}
