package console.maum.ai.correction.service;


import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;



@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})

@Service
public class CorrectionService {
    @Value("${api.url}")
    private String mUrl_ApiServer;

    private static final Logger logger = LoggerFactory.getLogger(CorrectionService.class);
    public String correctionApi(String apiId, String apiKey, String context) {

        try {
            String url = mUrl_ApiServer + "/correct/sentence";

            JSONObject json = new JSONObject();
            json.put("apiId", apiId);
            json.put("apiKey", apiKey);
            json.put("sentence", context);

            logger.info("Correction Api json :::::: " + json.toString());

            CloseableHttpClient client = HttpClientBuilder.create().build();

            HttpPost post = new HttpPost(url);
            post.setHeader("Content-Type", "application/json");
            StringEntity entity = new StringEntity(json.toString(), "UTF-8");

            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            String resData = EntityUtils.toString(response.getEntity(), "UTF-8");
            logger.info("Sending 'POST' request to URL : " + url);
            logger.info("Post parameters : " + post.getEntity());
            logger.info("Response Code : " + response.getStatusLine().getStatusCode());
            logger.info("Response Data : " + resData);

            return resData;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
