package console.maum.ai.chatbot.service;

import console.maum.ai.hmd.service.HmdService;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class ChatbotService {

    private static final Logger logger = LoggerFactory.getLogger(ChatbotService.class);

    @Value("${api.url}")
    private String mUrl_ApiServer;

    public ResponseEntity<String> getSimpleQA(String apiId, String apiKey,
                                              String question, int ntop, String domain){

        String url = mUrl_ApiServer + "/qa/simpleQa";

        String logMsg = "\n===========================================================================\n";
        logMsg += "Chat simpleQA API @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
        logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
        logMsg += String.format(":: %-10s = %s%n", "question", question);
        logMsg += String.format(":: %-10s = %s%n", "ntop", ntop);
        logMsg += String.format(":: %-10s = %s%n", "domain", domain);
        logMsg += "===========================================================================";
        logger.info(logMsg);


        try (CloseableHttpClient client = HttpClients.createDefault()){

            HttpPost post = new HttpPost(url);

            JSONObject json = new JSONObject();
            json.put("apiId", apiId);
            json.put("apiKey", apiKey);
            json.put("question", question);
            json.put("ntop", ntop);
            json.put("domain", domain);

            post.setHeader("Content-Type", "application/json");
            StringEntity entity = new StringEntity(json.toString(), "UTF-8");
            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            HttpEntity responseEntity = response.getEntity();
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("Chat_SimpleQA responseCode = {}" , responseCode);

            // Response ERROR
            if (responseCode != 200) {

                String responseString = "";
                if(responseEntity!=null) {
                    responseString = EntityUtils.toString(responseEntity, "UTF-8");
                }
                logger.error("Chat_SimpleQA fail : \n{}", responseString);
                return null;
            }

            // Response OK
            String resultMsg = EntityUtils.toString(responseEntity, "UTF-8");
            logger.debug(resultMsg);
            return new ResponseEntity<>(resultMsg, HttpStatus.OK);

        } catch (Exception e) {
            logger.error(" @ Chat_SimpleQA Service Exception ! ==> " + e);
        }
        return null;

    }



    public ResponseEntity<String> chatSignIn(){

        String url = "https://aicc-prd1.maum.ai:9980/api/v3/auth/signIn";
        String userKey = "admin";
        String passphrase = "1234";

        String logMsg = "\n===========================================================================\n";
        logMsg += "WeatherBot sign in @ PARAMS \n";
        logMsg += String.format(":: %-10s = %s%n", "URL", url);
        logMsg += String.format(":: %-10s = %s%n", "userKey", userKey);
        logMsg += String.format(":: %-10s = %s%n", "passphrase", passphrase);
        logMsg += "===========================================================================";
        logger.info(logMsg);


        try (CloseableHttpClient client = HttpClients.createDefault()){

            HttpPost post = new HttpPost(url);

            JSONObject json = new JSONObject();
            json.put("userKey", userKey);
            json.put("passphrase", passphrase);

            post.setHeader("Content-Type", "application/json");
            post.setHeader("m2u-auth-internal", "m2u-auth-internal");
            StringEntity entity = new StringEntity(json.toString(), "UTF-8");
            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            HttpEntity responseEntity = response.getEntity();
            int responseCode = response.getStatusLine().getStatusCode();
            logger.info("WeatherBot sign in - responseCode : {}" , responseCode);

            // Response ERROR
            if (responseCode != 200) {

                String responseString = "";
                if(responseEntity!=null) {
                    responseString = EntityUtils.toString(responseEntity, "UTF-8");
                }
                logger.error("WeatherBot sign in - fail : \n{}", responseString);
                return null;
            }

            // Response OK
            String resultMsg = EntityUtils.toString(responseEntity, "UTF-8");
            logger.debug(resultMsg);
            return new ResponseEntity<>(resultMsg, HttpStatus.OK);

        } catch (Exception e) {
            logger.error(" @ WeatherBot sign in - Service Exception ! ==> " + e);
        }
        return null;

    }

    public ResponseEntity<String> kbqaApi(String apiId, String apiKey, String lang, String question) {
        try {
            String url = mUrl_ApiServer + "/kbqa/answer";

            String logMsg = "\n===========================================================================\n";
            logMsg += "KBQA API @ PARAMS \n";
            logMsg += String.format(":: %-10s = %s%n", "URL", url);
            logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
            logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
            logMsg += String.format(":: %-10s = %s%n", "lang", lang);
            logMsg += String.format(":: %-10s = %s%n", "text", question);
            logMsg += "===========================================================================";
            logger.info(logMsg);


            JSONObject json = new JSONObject();
            json.put("apiId", apiId);
            json.put("apiKey", apiKey);
            json.put("lang", lang);
            json.put("question", question);

            logger.info("kbqaApi json :::::: " + json.toString());

            CloseableHttpClient client = HttpClientBuilder.create().build();

            HttpPost post = new HttpPost(url);
            post.setHeader("Content-Type", "application/json");
            StringEntity entity = new StringEntity(json.toString(), "UTF-8");

            post.setEntity(entity);

            HttpResponse response = client.execute(post);

            String resData = EntityUtils.toString(response.getEntity(), "UTF-8");
            logger.info("Response Code : " + response.getStatusLine().getStatusCode());
            logger.info("Response Data : " + resData);

            return new ResponseEntity<>(resData, HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

}