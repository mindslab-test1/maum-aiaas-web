package console.maum.ai.chatbot.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/chatbot")
public class ChatbotController {

	private static final Logger logger = LoggerFactory.getLogger(ChatbotController.class);
	
	/**
	 * chatbot Main
	 * @param request, model
	 * @return string
	 */
	@RequestMapping(value = "/krChatbotMain")
	public String krChatbotMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krChatbotMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "chatbotMain");
		httpSession.setAttribute("menuName", "대화");
		httpSession.setAttribute("subMenuName", "MRC 봇"); // · 날씨봇
		httpSession.setAttribute("menuGrpName", "대화");
		
		model.addAttribute("lang", "ko");

		return "/kr/chatbot/chatbotMain.pg";
	}

	/**
	 * enChatbot Main
	 * @param request, model
	 * @return string
	 */
	@RequestMapping(value = "/enChatbotMain")
	public String enChatbotMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enChatbotMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "chatbotMain");
		httpSession.setAttribute("menuName", "Conversation");
		httpSession.setAttribute("subMenuName", "MRC Bots");
		httpSession.setAttribute("menuGrpName", "Conversation");
		
		model.addAttribute("lang", "en");

		return "/en/chatbot/chatbotMain.pg";

	}

	/**
	 * chatbot Main
	 * @param request, model
	 * @return string
	 */
	@RequestMapping(value = "/krKbqaMain")
	public String krKbqaMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krKbqaMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "kbqaBotMain");
		httpSession.setAttribute("menuName", "대화");
		httpSession.setAttribute("subMenuName", "KBQA 봇"); // KBQA 봇
		httpSession.setAttribute("menuGrpName", "대화");

		model.addAttribute("lang", "ko");

		return "/kr/chatbot/kbqaMain.pg";
	}

	/**
	 * chatbot Main
	 * @param request, model
	 * @return string
	 */
	@RequestMapping(value = "/enKbqaMain")
	public String enKbqaMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enKbqaMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "kbqaBotMain");
		httpSession.setAttribute("menuName", "Conversation");
		httpSession.setAttribute("subMenuName", "KBQA Bot"); // KBQA 봇
		httpSession.setAttribute("menuGrpName", "Conversation");

		model.addAttribute("lang", "en");

		return "/en/chatbot/kbqaMain.pg";
	}
}
