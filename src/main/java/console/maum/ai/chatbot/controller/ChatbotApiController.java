package console.maum.ai.chatbot.controller;

import console.maum.ai.avatar.controller.AvatarController;
import console.maum.ai.chatbot.service.ChatbotService;
import console.maum.ai.member.model.MemberVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/api/chat")
public class ChatbotApiController {

    @Autowired
    private ChatbotService chatbotService;

    private static final Logger logger = LoggerFactory.getLogger(ChatbotApiController.class);


    @RequestMapping(value = "/simpleQA", produces = "application/json; charset=utf8" )
    @ResponseBody
    public ResponseEntity<String> chatSimpleQA(@RequestParam(value = "question") String question,
                                               @RequestParam(value = "ntop") int ntop,
                                               @RequestParam(value = "domain") String domain,
                                               HttpServletRequest request){

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        return chatbotService.getSimpleQA(apiId, apiKey, question, ntop, domain);
    }


    // WeatherBot chat SignIn
    @RequestMapping(value = "/chatSignIn", produces = "application/json; charset=utf8" )
    @ResponseBody
    public ResponseEntity<String> chatSignIn(){
        return chatbotService.chatSignIn();
    }


    @RequestMapping(value = "/kbqa", produces = "application/json; charset=utf8")
    @ResponseBody
    public ResponseEntity<String> kbqaApi(@RequestParam(value = "lang") String lang,
                                          @RequestParam(value = "question") String question,
                                          HttpServletRequest request) {
        logger.info("Welcome kbqaApi controller!");

        HttpSession httpSession = request.getSession(true);

        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        ResponseEntity<String> responseEntity = chatbotService.kbqaApi(apiId, apiKey, lang, question);

        return responseEntity;
    }
}
