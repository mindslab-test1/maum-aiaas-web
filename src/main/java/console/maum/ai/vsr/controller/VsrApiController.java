package console.maum.ai.vsr.controller;


import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.vsr.service.VsrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class VsrApiController {

    @Autowired
    private VsrService vsrService;

    @RequestMapping(value="/api/vsr")
    public ResponseEntity<byte[]> apiVsr(@RequestParam MultipartFile file, HttpServletRequest request){

        HttpSession httpSession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();

        return vsrService.getApiVsr(apiId, apiKey, file, PropertyUtil.getUploadPath() + "/vsr/");

    }

}
