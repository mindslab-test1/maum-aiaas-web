package console.maum.ai.vsr.controller;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/cloudApi/vsr")
public class VsrController {

    private static final Logger logger = LoggerFactory.getLogger(VsrController.class);

    /**
     * Vsr Main
     * @param model
     * @return
     */
    @RequestMapping(value = "/vsrMain")
    public String vsrMain(HttpServletRequest request, Model model) {
        logger.info("Welcome vsrMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "vsrMain");
        httpSession.setAttribute("menuName", "시각");
        httpSession.setAttribute("subMenuName", "Video Super Resolution");
        httpSession.setAttribute("menuGrpName", "시각");

        model.addAttribute("lang", "ko");
        String returnUri = "/kr/vsr/vsrMain.pg";

        return returnUri;
    }
    /**
     * Vsr Main (EN)
     * @param model
     * @return
     */
    @RequestMapping(value = "/enVsrMain")
    public String enVsrMain(HttpServletRequest request, Model model) {
        logger.info("Welcome enVsrMain!");
        HttpSession httpSession = request.getSession(true);
        httpSession.setAttribute("selectMenu", "vsrMain");
        httpSession.setAttribute("menuName", "Vision");
        httpSession.setAttribute("subMenuName", "Video Super Resolution");
        httpSession.setAttribute("menuGrpName", "Vision");

        model.addAttribute("lang", "en");
        String returnUri = "/en/vsr/vsrMain.pg";

        return returnUri;
    }
}
