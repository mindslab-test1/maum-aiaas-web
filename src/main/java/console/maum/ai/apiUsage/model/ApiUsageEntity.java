package console.maum.ai.apiUsage.model;

import console.maum.ai.member.model.ApiUsageDto;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@EqualsAndHashCode
public class ApiUsageEntity extends ApiUsageDto {

    private String engineGrp;                   // 엔진 그룹
    private Long usageAmount = (long)0;         // 사용자의 엔진 사용 양

    private String service;
    private String apiId;
    private long usage;
    private String date;
}