package console.maum.ai.apiUsage.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@EqualsAndHashCode
public class ApiUsageSttVo {
    private int service_no;
    private String client_id;
    private String update_date;
    private Long stt_duration;
    private int stt_length;
    private String stt_data;
}
