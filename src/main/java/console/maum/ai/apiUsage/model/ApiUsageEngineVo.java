package console.maum.ai.apiUsage.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * created by yejoon3117@mindslab.ai
 * 2020. 12. 24
 * */

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class ApiUsageEngineVo {
    private String service;         // api 서버에서 받아오는 엔진 정보
    private int groupId;            // 엔진의 카테고리 (음성, 시각, 언어, 대화, 영어교육)
    private String engine_name;      // 엔진 이름
    private Long subscribe_limit;     // 구독 고객이 사용할 수 있는 엔진 당 제한 양
    private Long base_limit;          // 기준 금액 당 사용할 수 있는 엔진 당 제한 양 (1만 원 기준)
    private String unit_name;        // 제한 양의 단위명
}
