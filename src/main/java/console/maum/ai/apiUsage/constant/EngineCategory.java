package console.maum.ai.apiUsage.constant;

public class EngineCategory {
    public static final String ENGINE_CATEGORY_1 = "음성";
    public static final String ENGINE_CATEGORY_2 = "시각";
    public static final String ENGINE_CATEGORY_3 = "언어";
    public static final String ENGINE_CATEGORY_4 = "대화";
    public static final String ENGINE_CATEGORY_5 = "영어교육";
}
