package console.maum.ai.member.service;

import console.maum.ai.common.model.ResultDto;
import console.maum.ai.member.dao.ApiUsageDao;
import console.maum.ai.member.model.*;
import console.maum.ai.payment.dao.PaymentDao;
import lombok.extern.slf4j.Slf4j;
import org.mortbay.log.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Slf4j
@Service
public class ApiUsageService {
    @Autowired
    PaymentDao paymentDao;

    @Autowired
    ApiUsageDao apiUsageDao;

    public ApiUsageResultDto getApiUsageList(MemberVo member) throws Exception {
		ResultDto resultDto = new ResultDto();

		/* 결제일을 Date형으로 변환 */
        Calendar cal = Calendar.getInstance();
        String datestr = paymentDao.getPaymentDate(member.getUserno());
        Log.info("========================================> pay date = {}", datestr);
        SimpleDateFormat transFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date paydate = transFormat.parse(datestr);

        /* 시작날짜를 결정 */
        Date today = new Date();
        String todaystr = transFormat.format(today);
        today = transFormat.parse(todaystr);
        String begin_date = "";
        String end_date = "";
        if(today.compareTo(paydate) <= 0) {
            Log.info("결제일이 아직 남았네요.!!!!!!!!!!!!!!!1");

            cal.setTime(paydate);
            cal.add(Calendar.MONTH, -1);

            begin_date = transFormat.format(cal.getTime());
            end_date = transFormat.format(paydate);
        }
        else {
            Log.info("결제일이 지났어요.!!!!!!!!!!!!!!!2");
            cal.get(Calendar.DAY_OF_MONTH);

            cal.setTime(paydate);
            begin_date = transFormat.format(cal.getTime());
            end_date = transFormat.format(today);
        }


        ApiUsageRequestDto param = new ApiUsageRequestDto();
        param.setApi_id(member.getApiId());
        param.setBegin_date(begin_date + " 00:00:00.000");
        param.setEnd_date(end_date + " 23:59:59.999");

        List<ApiUsageDto> usage_list = new ArrayList<ApiUsageDto>();

        /* ApiServer로부터 사용량 조회 (Dnn Stt 제외) */
        List<ApiUsageDto> usage_list_api_server = apiUsageDao.getApiUsageList_ApiServer(param);
        if(usage_list_api_server == null) usage_list_api_server = new ArrayList<ApiUsageDto>();
        for(int xx = 0; xx < usage_list_api_server.size(); xx++) {
            /* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            ** 현재 API-Server의 CNN STT는 건수로만 기록되어서 기능 업데이트 되기 전까지 적용하지 않는다.
            ** !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
            */
            if(usage_list_api_server.get(xx).getService().equalsIgnoreCase("stt")) {
                usage_list_api_server.remove(xx);
                xx--;
            }
        }
        usage_list.addAll(usage_list_api_server);

        /* 구 ApiServer로부터 DNN Stt 사용량 조회 (단위가 usec) */
        List<ApiUsageDto> usage_list_api_server_legacy = apiUsageDao.getApiUsageList_ApiServerLegacy(param);
        if(usage_list_api_server_legacy == null) usage_list_api_server_legacy = new ArrayList<ApiUsageDto>();
        usage_list.addAll(usage_list_api_server_legacy);

        /* 반환 포맷으로 변환 */
        ApiUsageResultDto result = new ApiUsageResultDto();
        result.setPeriod(begin_date + " ~ " + end_date);
        ApiUsageDisplayDto total = new ApiUsageDisplayDto();
        total.setService("합계");
        result.getUsages().add(total);
        for(ApiUsageDto usage : usage_list) {
            ApiUsageDisplayDto item = calcApiUsage(usage);
            if(item != null) {
                result.getUsages().add(item);
                result.getUsages().get(0).setRate_val( result.getUsages().get(0).getRate_val() + item.getRate_val() );
            }
        }
        result.getUsages().get(0).setRate(String.format("%.1f%s", result.getUsages().get(0).getRate_val(), "%"));

		return result;
	}

	/*
	** 현재는 시간은 usec으로 저장(구 API 서버 기준)되는 것으로 함.
	*/
	ApiUsageDisplayDto calcApiUsage(ApiUsageDto usage) {
        ApiUsageLimitDto limit_info = apiUsageDao.getApiUsageLimit(usage.getService());
        if(limit_info == null) {
            Log.info("@ API USAGE: not registered service- {}", usage.getService());
            return null;
        }

        double rate = 0;
        ApiUsageDisplayDto result = new ApiUsageDisplayDto();
        result.setService(limit_info.getDesc());

        if(limit_info.getUnit().equals("글자수")) {
            rate = usage.getUsage()*100/(double)limit_info.getUsageLimit();
            result.setUsage(String.format("%,d자 / %,d자", usage.getUsage(), limit_info.getUsageLimit()));
        }
        else if(limit_info.getUnit().equals("분")) {
            rate = (usage.getUsage()/1000)*100/(double)(limit_info.getUsageLimit()*60*1000);
            result.setUsage(String.format("%,d분 / %,d분", usage.getUsage()/1000000/60, limit_info.getUsageLimit()));
        }
        else if(limit_info.getUnit().equals("초")) {
            rate = (usage.getUsage()/1000)*100/(double)(limit_info.getUsageLimit()*1000);
            result.setUsage(String.format("%,d초 / %,d초", usage.getUsage()/1000000, limit_info.getUsageLimit()));
        }
        else if(limit_info.getUnit().equals("건")) {
            rate = usage.getUsage()*100/(double)limit_info.getUsageLimit();
            result.setUsage(String.format("%,d건 / %,d건", usage.getUsage(), limit_info.getUsageLimit()));
        }
        else return null;
        result.setRate(String.format("%.1f%s", rate, "%"));
        result.setRate_val(rate);

        return result;
    }
}
