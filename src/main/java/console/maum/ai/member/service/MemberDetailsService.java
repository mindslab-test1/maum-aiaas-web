package console.maum.ai.member.service;

import com.google.gson.Gson;
import console.maum.ai.login.controller.LoginController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import console.maum.ai.member.dao.MemberDao;
import console.maum.ai.member.model.MemberForm;
import console.maum.ai.member.model.MemberSecurityVo;

public class MemberDetailsService implements UserDetailsService {
	private static final Logger logger = LoggerFactory.getLogger(MemberDetailsService.class);
    @Autowired
    private MemberDao memberDao;
    
	@Autowired
	private MemberService memberService;     
    
    @SuppressWarnings("unused")
	public UserDetails loadUserByUsername(MemberForm memberForm) throws UsernameNotFoundException {
    	MemberSecurityVo user = memberDao.getUserById(memberForm.getEmail());
		Gson gson = new Gson();
		logger.info("=====유저 조회 : {}",gson.toJson(user));
        if(user==null) {
			logger.info("=====새 유저=====");
			try {
				memberService.insertMember(memberForm);
				user = memberDao.getUserById(memberForm.getEmail());
				logger.info("=====새 유저 : {}",gson.toJson(user));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw new UsernameNotFoundException(memberForm.getEmail());
			}
        }else{
			try {
				logger.info("=====기존 유저=====");
				memberService.updateCreateFlagMember(memberForm);
				user = memberDao.getUserById(memberForm.getEmail());
				logger.info("=====기존 유저 : {}",gson.toJson(user));
			}catch (Exception e){
				e.printStackTrace();
			}
		}
        return user;
    }


    @Override
	public UserDetails loadUserByUsername(String email){

    	MemberSecurityVo user = memberDao.getUserById(email);
    	logger.info("USER : {}", user.getEmail());

    	if (user == null){
			throw new UsernameNotFoundException(email);
		}else{
    		return user;
		}
	}

//	@Override
//	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//		// TODO Auto-generated method stub
//		return null;
//	}
 
}