package console.maum.ai.member.service;

import java.util.*;

import console.maum.ai.admin.apikey.dao.ApiKeyDao;
import console.maum.ai.admin.apikey.model.ApiKeyVo;
import console.maum.ai.apiUsage.constant.EngineCategory;
import console.maum.ai.apiUsage.model.ApiUsageEngineVo;
import console.maum.ai.apiUsage.model.ApiUsageEntity;
import console.maum.ai.apiUsage.model.ApiUsageSttVo;
import console.maum.ai.common.util.MailSender;
import console.maum.ai.login.service.LoginService;
import console.maum.ai.member.constant.UserStatus;
import console.maum.ai.member.dao.ApiUsageDao;
import console.maum.ai.member.model.*;
import console.maum.ai.payment.model.PaymentVo;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import console.maum.ai.common.model.CommonDto;
import console.maum.ai.common.model.CommonForm;
import console.maum.ai.common.model.ResultDto;
import console.maum.ai.common.util.PagingUtil;
import console.maum.ai.member.dao.MemberDao;

@Service
public class MemberService {

    private final static Logger logger = LoggerFactory.getLogger(MemberService.class);

	@Autowired
	private MemberDao memberDao;

	@Autowired
	private ApiUsageDao apiUsageDao;

	@Autowired
	private MailSender mailSender;

	@Autowired
	private LoginService loginService;

	@Autowired
	private ApiKeyDao apiDao;

	@Value("${api.url}")
	private String apiServerUrl;

	@Value("${admin.api.id}")
	private String apiAdminId;

	@Value("${admin.api.pw}")
	private String apiAdminKey;

	public MemberDto getLogInMemberDetail(String id) throws Exception {

		MemberDto memberDto = new MemberDto();
		
		memberDto = memberDao.getLogInMemberDetail(id);

		return memberDto;
	}

	// 자체 로그인 사용자 정보 조회
	public MemberDto getMaumLogInMemberDetail(String id) throws Exception {

		MemberDto memberDto = new MemberDto();

		memberDto = memberDao.getMaumLogInMemberDetail(id);

		return memberDto;
	}
	
	/** 계정 - 등록 */
	public MemberDto insertMember(MemberForm memberForm) throws Exception {

		MemberDto memberDto = new MemberDto();
		MemberVo memberVo = new MemberVo();
		
		String id = memberForm.getEmail().substring(0,memberForm.getEmail().indexOf("@"));
		
		memberForm.setId(id);
		memberForm.setActive(1);
		memberForm.setPrivacyAgree(0);
		memberForm.setPhone("");
		if(memberForm.getEmail().contains("@mindslab.ai")) {
			memberForm.setAuthority("ROLE_INTERNAL");
		} else {
			memberForm.setAuthority("ROLE_USER");
		}
		memberForm.setEnabled(1);
		memberForm.setCreateFlag(1);
		memberForm.setCompanyEmail("");
		memberForm.setStatus(UserStatus.INIT);

		int insertCnt = 0;
		
//		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		memberForm.setPassword("$2a$10$kabdYP0/bdFaIrcdSBAu2uSZ9TIfYqhKI1i93neeeRZnGde4S.EO6");//임시 BCrypt 비번
		
		insertCnt = memberDao.insertMember(memberForm);

		if (insertCnt > 0) {
			memberDto.setResult("SUCCESS");
			/* 가입 환영 메일(예정) - 2019. 11. 15 LYJ */
			mailSender.sendWelcomeMail(memberForm.getEmail());
		} else {
			memberDto.setResult("FAIL");
		}

		memberVo.setEmail(memberForm.getEmail());
		memberVo.setId(memberForm.getId());
		memberVo.setName(memberForm.getName());

		ApiKeyVo api = apiDao.getUserApi(memberForm.getEmail());
		if(api.getApiId() == null && api.getApiKey() == null){   //apiId 정보가 없는 경우
			loginService.createOrResumeApiAccount(memberVo); // API 계정 create or resume
		}

		return memberDto;
	}
	
	/** 이용내역 - 목록 조회 */
	public ResultDto getApiAccountList(MemberForm memberForm) throws Exception {

		ResultDto resultDto = new ResultDto();

		CommonDto commonDto = new CommonDto();

		int totalCount = memberDao.getApiAccountCnt(memberForm);
		if (totalCount != 0) {
			CommonForm commonForm = new CommonForm();
			commonForm.setFunction_name(memberForm.getFunction_name());
			commonForm.setCurrent_page_no(memberForm.getCurrent_page_no());
			commonForm.setCount_per_page(10);
			commonForm.setCount_per_list(10);
			commonForm.setTatal_list_count(totalCount);
			commonDto = PagingUtil.setPageUtil(commonForm);
		}

		memberForm.setLimit(commonDto.getLimit());
		memberForm.setOffset(commonDto.getOffset());

		List<MemberDto> list = memberDao.getApiAccountList(memberForm);
		
		Gson gson = new Gson();
		logger.info("MemberDto list ======= " + gson.toJson(list));

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", list);
		resultMap.put("totalCount", totalCount);
		resultMap.put("pagination", commonDto.getPagination());

		resultDto.setData(resultMap);
		resultDto.setState("SUCCESS");

		return resultDto;

	}

	/** 잔여 사용량 조회 */
	public HashMap<String, String> selectUsage(int userNo) throws Exception {
		List<UsageVo> usageList = memberDao.selectUsage(userNo);
		HashMap<String, String> usageMap = new HashMap <>();

		for(UsageVo vo : usageList) {
			usageMap.put(vo.getProductName(), vo.getUsage());
		}

		return usageMap;
	}

	/** 결제 내역 조회 2019. 5. 17 YBU */
	public List<PaymentVo> selectPaymentInfo(int userNo) throws Exception {
		List<PaymentVo> paymentList = memberDao.selectPaymentInfo(userNo);

		return paymentList;
	}

	/** 특정 결제 내역 조회 2019. 5. 17 YBU */
	public PaymentVo selectOnePaymentInfo(String pano) throws Exception{
		PaymentVo paymentVo = memberDao.selectOnePaymentInfo(pano);

		return paymentVo;
	}

	/** 현재 결제 내용 조회 2019. 5. 20 YBU */
	public PaymentVo selectBillingInfo(int userNo) throws Exception {
		PaymentVo paymentVo = memberDao.selectBillingInfo(userNo);

		return paymentVo;
	}
	/**사용 가능여부 조회에 필요한 productNo, user_t에서 조회 2019. 05. 21 JWS */
	public int selectProductNoInUser(int userNo) throws Exception{
		int productNo = memberDao.getProductNoInUser(userNo);
		return productNo;
	}
	/**사용 가능여부 조회에 필요한 productId, productDetail_T에서 조회 2019. 05. 21 JWS */
	public int selectProductIdInProduct(UseAmountVo useAmountVo) throws Exception{
		int productId = memberDao.getProductIdInProduct(useAmountVo);
		return productId;
	}
	/**사용 가능여부 조회 2019. 05. 21 JWS */
	public int getUsableCheck (UseAmountVo useAmountVo) throws Exception{
		int usable = memberDao.getUsable(useAmountVo);
		return usable ;
	}
	/**사용량 차감 2019. 05. 21 YGE */
	public void deductUsageAmount(UseAmountVo useAmountVo) throws Exception{
		memberDao.deductUsageAmount(useAmountVo);
	}
	public ResultDto getUseLog(UseServiceVo useServiceVo) throws Exception{

		ResultDto resultDto = new ResultDto();

		List<UseServiceVo> useList = memberDao.getUseLog(useServiceVo);
		Gson gson = new Gson();
		logger.info("UseService list ======= " + gson.toJson(useList));

		HashMap<String, Object> resultMap = new HashMap<String, Object>();
		resultMap.put("list", useList);

		resultDto.setData(resultMap);
		resultDto.setState("SUCCESS");

		return resultDto;
	}

	public int updatePrivacyAgreeMember(MemberForm memberForm) throws Exception {

		return memberDao.updatePrivacyAgreeMember(memberForm);
	}

	public int rejectMarketingAgree(String email) throws Exception {

		return memberDao.rejectMarketingAgree(email);
	}

	/** createFlag update -2019.07.11 YGE */
	public int updateCreateFlagMember(MemberForm memberForm) throws Exception {

		return memberDao.updateCreateFlagMember(memberForm);
	}

	/** update sign up user info - 2019.07.12 YGE  */
	public MemberDto updateSignUpMemberInfo(MemberForm memberForm) throws Exception {

		MemberDto memberDto = new MemberDto();

		int updateCnt = memberDao.updateSignUpMemberInfo(memberForm);

		if (updateCnt > 0) {
			memberDto.setResult("SUCCESS");
		} else {
			memberDto.setResult("FAIL");
		}

		return memberDto;
	}


	/** 사용자 프로필 조회 */
	public MemberDto getMemberDetail(String email) throws Exception {

		MemberDto memberDto;

		memberDto = memberDao.getMemberDetail(email);
		Gson gson = new Gson();
		logger.info("result Dto ::: {}  ", gson.toJson(memberDto));
		return memberDto;
	}

	/** 사용자 프로필 수정 */
	public MemberDto setUpdate(MemberForm memberForm) throws Exception {

		MemberDto memberDto = new MemberDto();

		int updateCnt = memberDao.setUpdate(memberForm);

		if (updateCnt > 0) {
			memberDto.setResult("SUCCESS");
		} else {
			memberDto.setResult("FAIL");
		}

		return memberDto;
	}

    public PaymentVo selectNextBillingInfo(int userno) {
		return memberDao.getNextBillingInfo(userno);
    }

    /** 구독 취소 고객 리스트 조회 19.09.23 LYJ */
    public List<MemberVo> getCancelReqUser() throws Exception {

    	List<MemberVo> result = memberDao.getCancelReqUser();
    	return result;
	}

	/** 사용자 Status 변경 19.09.25 LYJ */
	public int updateUserStatus(MemberForm memberForm) throws Exception {

	    int updateResult = memberDao.updateUserStatus(memberForm);
	    int result;

        if(updateResult > 0) {
            //Success
            result = 1;
        } else {
            // Fail
            result = 0;
        }
        return result;
    }

	/** 사용자 Status 조회 19.09.30 LYJ */
	public int getUserStatus(MemberForm memberForm) throws Exception {

	    int result = memberDao.getUserStatus(memberForm);
	    return result;
    }
	/** 사용자 CreateDate 조회 20.02.19 SMR */
	public String getUserCreateDate(int userNo) {

		String result = memberDao.getUserCreateDate(userNo);
		return result;
	}
    /** 사용자의 특정 기간 API 사용량 조회 20.12.29 LYJ */
    public List<ApiUsageEntity> getApiUsageList(ApiUsageRequestDto apiUsageRequestDto) {
		String req_id = apiUsageRequestDto.getApi_id();
		String begin_date = apiUsageRequestDto.getBegin_date();
		String end_date = apiUsageRequestDto.getEnd_date();

		String url = apiServerUrl + "/admin/selectUsage";

		JSONObject json = new JSONObject();
		json.put("apiId", apiAdminId);
		json.put("apiKey", apiAdminKey);
		json.put("reqId", req_id);
		json.put("startDate", begin_date);
		json.put("endDate", end_date);

		try {
			CloseableHttpClient client = HttpClientBuilder.create().build();

			HttpPost post = new HttpPost(url);
			post.setHeader("Content-Type", "application/json");
			StringEntity entity = new StringEntity(json.toString(), "UTF-8");

			post.setEntity(entity);

			HttpResponse response = client.execute(post);

			String resultMsg = EntityUtils.toString(response.getEntity(), "UTF-8");

			JSONArray jsonArray = new JSONArray(resultMsg);
			JSONObject jsonObject = null;

			ArrayList<ApiUsageEntity> jsonObjList = new ArrayList<>();
			for(int idx = 0; idx < jsonArray.length(); idx++){
				jsonObject = (JSONObject) jsonArray.get(idx);

				ApiUsageEntity usageData = new ApiUsageEntity();
				usageData.setService(jsonObject.optString("service"));
				usageData.setApiId(jsonObject.optString("apiId"));
				usageData.setUsage(jsonObject.optInt("usage"));
				usageData.setDate(jsonObject.optString("date"));

				jsonObjList.add(usageData);
			}

			if(response.getStatusLine().getStatusCode() == 200) {
				return jsonObjList;
			} else {
				return null;
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	/** 사용자의 특정 기간 STT API 사용량 조회 21.01.13 LYJ */
	public List<ApiUsageSttVo> getSttUsageList(ApiUsageRequestDto apiUsageRequestDto) {
		 List<ApiUsageSttVo> result = apiUsageDao.getSttUsageList(apiUsageRequestDto);
		 return result;
	}

	/** 엔진의 그룹(카테고리) 조회 20.12.29 LYJ */
	public int getEngineGrp(String engineName) {
		int result = memberDao.getEngineGrp(engineName);
		return result;
	}

	/** 엔진별 카테고리 한글화 21.01.07 LYJ */
	public List<ApiUsageEntity> convertEngineNameKo(List<ApiUsageEntity> list) {
		for(ApiUsageEntity apiUsageEntity : list) {
			logger.info(apiUsageEntity.toString());
			int engineGrp = getEngineGrp(apiUsageEntity.getService());
			logger.info(String.valueOf(engineGrp));

			if(engineGrp == 1) {            // 음성
				apiUsageEntity.setEngineGrp(EngineCategory.ENGINE_CATEGORY_1);
			} else if (engineGrp == 2) {    // 시각
				apiUsageEntity.setEngineGrp(EngineCategory.ENGINE_CATEGORY_2);
			} else if (engineGrp == 3) {    // 언어
				apiUsageEntity.setEngineGrp(EngineCategory.ENGINE_CATEGORY_3);
			} else if (engineGrp == 4) {    // 대화
				apiUsageEntity.setEngineGrp(EngineCategory.ENGINE_CATEGORY_4);
			} else if (engineGrp == 5) {    // 영어교육
				apiUsageEntity.setEngineGrp(EngineCategory.ENGINE_CATEGORY_5);
			}
		}

		logger.debug(list.toString());
		return list;
	}

	/** 날짜 순으로 사용내역 정렬 */
	public List<ApiUsageEntity> sortListByDate(List<ApiUsageEntity> apiUsageEntityList) {
		Collections.sort(apiUsageEntityList, new Comparator<ApiUsageEntity>() {
			public int compare(ApiUsageEntity o1, ApiUsageEntity o2) {
				if (o1.getDate() == null || o2.getDate() == null)
					return 0;
				return o1.getDate().compareTo(o2.getDate());
			}
		});
		Collections.reverse(apiUsageEntityList);
		return apiUsageEntityList;
	}

	/** 각 엔진의 사용량 계산 20.12.31 LYJ */
	public List<ApiUsageEntity> engineUsageList(String apiId, List<ApiUsageEntity> apiUsageDtos) {
		List<ApiUsageEntity> result = new ArrayList<>();
		ArrayList<Integer> engineIndex = new ArrayList<>();		// 엔진 인덱스를 통해 합산된 사용량이 기록된 곳을

		apiUsageDtos.get(0).setUsageAmount( apiUsageDtos.get(0).getUsage() );
		for(int i=1 ; i<apiUsageDtos.size() ; i++) {
			for (int j=0; j < apiUsageDtos.size(); j++) {

				if ((i > j) && apiUsageDtos.get(i).getService().equalsIgnoreCase(apiUsageDtos.get(j).getService())) {
					Long usageSum = apiUsageDtos.get(i).getUsage() + apiUsageDtos.get(j).getUsageAmount();
					apiUsageDtos.get(j).setUsageAmount(usageSum);
					engineIndex.add(j);
					//System.out.println("i = " + i + " , j = " + j);
					break;
				} else if((j == apiUsageDtos.size()-1) ) {
					apiUsageDtos.get(i).setUsageAmount( apiUsageDtos.get(i).getUsage() );
					engineIndex.add(i);
					break;
				}
			}
		}

		// 엔진 인덱스 중복 제거
		ArrayList<Integer> engineIndexList = new ArrayList<>();
		engineIndexList.add(0);
		for (int i = 0; i < engineIndex.size(); i++) {
			if (!engineIndexList.contains(engineIndex.get(i))) {
				engineIndexList.add(engineIndex.get(i));
			}
		}

		// 엔진 인덱스에 기록된 엔진 목록만 추출
		for(int i=0 ; i<engineIndexList.size() ; i++) {
			result.add(apiUsageDtos.get( engineIndexList.get(i) ));
		}

		// 사용자의 결제 방식 조회 (Card, Cash, Etc...)
		String paymentMethod = memberDao.getPaymentMethodUsingApiId(apiId);

		// Cash 고객의 경우, 계약 건마다 제한이 다르기 때문에 일단은 구독 고객과 동일하게 설정
		if("Card".equalsIgnoreCase(paymentMethod) || "PayPal".equalsIgnoreCase(paymentMethod) || "Cash".equalsIgnoreCase(paymentMethod)) {
			for(int i=0 ; i<result.size() ; i++) {

				// 필요없는 값 초기화
				ApiUsageEntity apiUsageEntity = new ApiUsageEntity();
				apiUsageEntity.setService( result.get(i).getService() );
				apiUsageEntity.setEngineGrp( result.get(i).getEngineGrp() );
				apiUsageEntity.setUsageAmount( result.get(i).getUsageAmount() );

				ApiUsageEngineVo apiUsageEngineVo = memberDao.getEngineInfo(result.get(i).getService());
				apiUsageEntity.setLimitAmount(apiUsageEngineVo.getSubscribe_limit());	// 카드 고객의 경우 제한이 구독 제한 양과 일치
				apiUsageEntity.setUnitName(apiUsageEngineVo.getUnit_name());

				double usageRate = (double)apiUsageEntity.getUsageAmount() / (double)apiUsageEntity.getLimitAmount() * 100;	// 사용 비율
				double rate = Double.parseDouble(String.format("%.5f",usageRate));	// 소수점 최대 다섯 번째 자리까지만 표현
				apiUsageEntity.setRate(rate);

				result.set(i, apiUsageEntity);
			}
		} else if ("Etc".equalsIgnoreCase(paymentMethod)) {
			// TBD
		} else {
			// TBD
		}
		//System.out.println(result);
		return result;
	}

	/** 카테고리별 사용량 계산 - 2021.01.07 LYJ */
	public List<ApiUsageEntity> totalUsageList(String apiId, List<ApiUsageEntity> engineUsageList) {
		List<ApiUsageEntity> result = new ArrayList<>();

		ApiUsageEntity category1 = new ApiUsageEntity();		// 음성
		ApiUsageEntity category2 = new ApiUsageEntity();		// 시각
		ApiUsageEntity category3 = new ApiUsageEntity();		// 언어
		ApiUsageEntity category4 = new ApiUsageEntity();		// 대화
		ApiUsageEntity category5 = new ApiUsageEntity();		// 영어교육
		ApiUsageEntity category6 = new ApiUsageEntity();		// 남은 사용량
		
		category1.setEngineGrp(EngineCategory.ENGINE_CATEGORY_1);
		category2.setEngineGrp(EngineCategory.ENGINE_CATEGORY_2);
		category3.setEngineGrp(EngineCategory.ENGINE_CATEGORY_3);
		category4.setEngineGrp(EngineCategory.ENGINE_CATEGORY_4);
		category5.setEngineGrp(EngineCategory.ENGINE_CATEGORY_5);
		category6.setEngineGrp("남은 사용량");

		Long usageAmount1 = (long)0;
		Long usageAmount2 = (long)0;
		Long usageAmount3 = (long)0;
		Long usageAmount4 = (long)0;
		Long usageAmount5 = (long)0;

		Long maxLimit = null;

		// 사용자의 결제 방식 조회 (Card, Cash, Etc...)
		String paymentMethod = memberDao.getPaymentMethodUsingApiId(apiId);

		// Cash 고객의 경우, 계약 건마다 제한이 다르기 때문에 일단은 구독 고객과 동일하게 설정
		if("Card".equalsIgnoreCase(paymentMethod) || "PayPal".equalsIgnoreCase(paymentMethod) || "Cash".equalsIgnoreCase(paymentMethod)) {
			//카테고리에 속한 엔진 중 최대 제한값 return
			maxLimit = memberDao.getMaxSubscribeLimitVal();

			for(int i=0 ; i<engineUsageList.size() ; i++) {

				// 각 엔진의 사용량을 최대값으로 연산
				Long engineLimit = engineUsageList.get(i).getLimitAmount();
				Long x = maxLimit / engineLimit;		// 연산 비율
				Long convertUsageAmount = x * engineUsageList.get(i).getUsageAmount();
				
				// 카테고리별 연산
				String engineGrp = engineUsageList.get(i).getEngineGrp();
				if(engineGrp == EngineCategory.ENGINE_CATEGORY_1) {            // 음성
					usageAmount1 = usageAmount1 + convertUsageAmount;
				} else if (engineGrp == EngineCategory.ENGINE_CATEGORY_2) {    // 시각
					usageAmount2 = usageAmount2 + convertUsageAmount;
				} else if (engineGrp == EngineCategory.ENGINE_CATEGORY_2) {    // 언어
					usageAmount3 = usageAmount3 + convertUsageAmount;
				} else if (engineGrp == EngineCategory.ENGINE_CATEGORY_2) {    // 대화
					usageAmount4 = usageAmount4 + convertUsageAmount;
				} else if (engineGrp == EngineCategory.ENGINE_CATEGORY_2) {    // 영어교육
					usageAmount5 = usageAmount5 + convertUsageAmount;
				}
			}
		} else if ("Etc".equalsIgnoreCase(paymentMethod)) {
			// TBD
		} else {
			// TBD
		}

		category1.setUsageAmount(usageAmount1);
		category2.setUsageAmount(usageAmount2);
		category3.setUsageAmount(usageAmount3);
		category4.setUsageAmount(usageAmount4);
		category5.setUsageAmount(usageAmount5);
		
		category1.setRate((double)usageAmount1/(double)maxLimit * 100);
		category2.setRate((double)usageAmount2/(double)maxLimit * 100);
		category3.setRate((double)usageAmount3/(double)maxLimit * 100);
		category4.setRate((double)usageAmount4/(double)maxLimit * 100);
		category5.setRate((double)usageAmount5/(double)maxLimit * 100);

		// 남은 사용량 비율 계산
		double category6Rate = 100 - ((double)(usageAmount1+usageAmount2+usageAmount3+usageAmount4+usageAmount5)/(double)maxLimit * 100);
		if(category6Rate < 0)
			category6Rate = 0;
		category6.setRate(category6Rate);

		result.add(category1);
		result.add(category2);
		result.add(category3);
		result.add(category4);
		result.add(category5);
		result.add(category6);
		return result;
	}
}