package console.maum.ai.member.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.fasterxml.jackson.databind.ObjectMapper;
import console.maum.ai.login.controller.LoginController;
import console.maum.ai.login.model.RedirectStateVo;
import console.maum.ai.login.service.LoginService;
import console.maum.ai.member.constant.UserStatus;
import console.maum.ai.member.model.*;
import console.maum.ai.member.service.MemberDetailsService;
import console.maum.ai.payment.dao.PaymentDao;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;

import console.maum.ai.common.model.ResultDto;
import console.maum.ai.member.service.MemberService;
import console.maum.ai.payment.model.PaymentVo;
import console.maum.ai.payment.service.PaymentService;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",
		"classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@Controller
@RequestMapping(value="/member")
public class MemberController {
	@Value("${api.url}")
	private String mUrl_ApiServer;

	@Value("${admin.api.id}")
	private String mAdmin_ApiId;

	@Value("${admin.api.pw}")
	private String mAdmin_ApiKey;

	@Autowired
	private MemberService memberService;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private PaymentDao paymentDao;

	@Autowired
	private MemberDetailsService memberDetailsService;

	@Autowired
	private LoginService loginService;

	@Autowired
	private LoginController loginController;

	@Value("${hq.URL}")
	private String sso_url;

	@Value("${hq.client_id}")
	private String client_id;

	@Value("${hq.redirect_uri}")
	private String redirect_uri;


	private static final Logger logger = LoggerFactory.getLogger(MemberController.class);


	/* SSO에서 signup 요청시 SignupForm 뿌려줌 국/영문 분기 처리 -- 2020.04.08 YGE */
	@RequestMapping(value="/signupForm")
	public String SSOsignupForm(@RequestParam(value="email") String email,
							   @RequestParam(value="callback_uri") String callbackUri,
								@RequestParam(value="state") String state,
								HttpServletRequest request,
								HttpServletResponse response,
								Model model) {
		logger.info("================= SSO 회원등록 시작!! ================");

		String lang = loginService.setLang(request, response);
		logger.debug(lang);


		HttpSession session;
		MemberDto memberDto;
		MemberVo memberVo;

		try {
			memberDto = memberService.getMemberDetail(email);
			memberVo = memberDto;

			// DB에 user가 없는 경우
			if(memberDto == null) {
				MemberForm memberForm = new MemberForm();
				memberForm.setEmail(email);
				memberForm.setName(email.substring(0,email.indexOf("@")));
				MemberSecurityVo user = (MemberSecurityVo) memberDetailsService.loadUserByUsername(memberForm);
				logger.info("새 user 생성 완료!! -- {}", user.getEmail());

				memberDto = memberService.getMemberDetail(user.getEmail());
				memberVo = memberDto;
			}

			session = request.getSession();
			session.setAttribute("accessUser", memberVo);
			session.setAttribute("callback_uri", callbackUri);

		} catch (Exception e) {
			logger.error("Signup user 정보 세팅 Exception : " + e);
			return "redirect:/error/noti";
		}

		Gson gson = new Gson();
		logger.info("사용자 detail ::: {}  ", gson.toJson(memberDto));

		if(memberVo != null && memberVo.getStatus() == UserStatus.UNSUBSCRIBED) {
			logger.info("status == UNSUBSCRIBED -> return /login/krPayment ! ");
			return "redirect:/login/krPayment";
		}

		model.addAttribute("createFlag", memberDto.getCreateFlag());

		return "noLayout/" + lang + "/login/signupForm.pg";
	}




	@RequestMapping(value = "/krApiAccount")
	public String krApiAccount(HttpServletRequest request, Model model) throws Exception{
		logger.info("Welcome krApiAccount!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "member");
		httpSession.setAttribute("menuName", "계정");
		httpSession.setAttribute("subMenuName", "API 정보");
		httpSession.setAttribute("menuGrpName", "");

		model.addAttribute("lang", "ko");
		String returnUri = "/kr/member/apiAccount.pg";
		MemberVo member = (MemberVo) httpSession.getAttribute("accessUser");
		MemberDto memberDto = memberService.getMemberDetail(member.getEmail());
		/*this is apiTest*/
		StringBuffer outResult = new StringBuffer();

		String usageUrl = mUrl_ApiServer + "/admin/selectUsage";
		String adminId = mAdmin_ApiId;
		String adminPw = mAdmin_ApiKey;

		Map<String, String> usageApiMap = new HashMap<>();	//사용량 api에 보낼 값들
		usageApiMap.put("apiId", adminId);
		usageApiMap.put("apiKey", adminPw);
		usageApiMap.put("reqId", memberDto.getApiId());

		String paymentDate = paymentDao.selectPayDate(46);

		/* api를 통해 각 엔진 사용량 전체를 가져옴 */
		try{
			ObjectMapper mapper = new ObjectMapper();
			URL url = new URL(usageUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accetp-Charset", "UTF-8");

			OutputStream os = conn.getOutputStream();
			os.write(mapper.writeValueAsString(usageApiMap).getBytes("UTF-8"));
			os.flush();

			int responseCode = conn.getResponseCode();

			if(responseCode == HttpURLConnection.HTTP_OK){
				logger.debug("httpConnection success");
				BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
				String inputLine;
				while((inputLine = br.readLine()) != null){
					try{
						outResult.append(new String(URLDecoder.decode(inputLine, "UTF-8")));
					}catch(Exception e){
						outResult.append(inputLine);
					}
				}
				logger.debug("this is apiResult : "+outResult.toString());
				br.close();
			}else{
				throw new Exception();
			}
			conn.disconnect();
		}catch(Exception e){
			logger.debug(e.getMessage());
		}

		/* 결과 값을 list<Map>에 넣은 후 Session에 담음 */
		try{
			JSONArray apiArray = new JSONArray(outResult.toString());
			List<Map<String, String>> usageList = new ArrayList<>();
			Map<String, String> used_engine = new HashMap<>();	//서비스 중복되지 않도록

			/* JsonArray의 각 JsonObject를 list의 담음  */
			for(int i=0;i<apiArray.length();i++){
				Map<String, String> map = new HashMap<>();
				JSONObject object = apiArray.getJSONObject(i);
				Iterator<String> keysItr = object.keySet().iterator();

				/* JsonObject안에 값들을 Map에 담음 */
				if(used_engine.get(object.getString("service")) == null) {  /* engine이 들어있지 않은 경우*/
					while (keysItr.hasNext()) {
						String key = keysItr.next();
						String value = object.get(key).toString();
						map.put(key, value);

					}
					map.put("paymentDate", paymentDate.replace(paymentDate.split("-")[1], String.valueOf(Integer.parseInt(paymentDate.split("-")[1])-1))+ " ~ " +paymentDate); //사용기간
					usageList.add(map);
					used_engine.put(map.get("service"), map.get("service"));
				}else{	/* 동일 엔진이 이미 들어있는 경우 */
					for(int j=0;j<usageList.size();j++){
						if(usageList.get(j).get("service").equals(object.get("service").toString()))
						{
							usageList.get(j).put("usage", Integer.toString(Integer.parseInt(usageList.get(j).get("usage")) + Integer.parseInt(object.get("usage").toString())));
						}
					}
				}
			}

			for(Map<String, String> engine : usageList){
				String usage = engine.get("usage");
				if(engine.get("service").equals("TTS"))
				{
					engine.put("usage", usage+"/400,000자");
					engine.put("percentage", String.format("%.1f", (Integer.parseInt(usage)/(double)400000)));
				}
				else if(engine.get("service").equals("STT"))
				{
					engine.put("usage", usage+"500/분");
					engine.put("percentage", String.format("%.1f", (Integer.parseInt(usage)/(double)500)));
				}
				else if(engine.get("service").equals("TTI"))
				{

				}
				else if(engine.get("service").equals("DIARL"))
				{
					engine.put("usage", usage+"/8,000건");
					engine.put("percentage", String.format("%.1f", Integer.parseInt(usage)/(double)800));
				}
				else if(engine.get("service").equals("NLU"))
				{
					engine.put("usage", usage+"/2,000단위");
					engine.put("percentage", String.format("%.1f", Integer.parseInt(usage)/(double)2000));
				}
				else if(engine.get("service").equals("MRC"))
				{
					engine.put("usage", usage+"/200,000건");
					engine.put("percentage", String.format("%.1f", Integer.parseInt(usage)/(double)200000));
					//engine.put("percentage", Double.toString(Double.parseDouble(usage)/(double)200000));
				}
				else if(engine.get("service").equals("XDC"))
				{
					engine.put("usage", usage+"/2,000건");
					engine.put("percentage", String.format("%.1f", Integer.parseInt(usage)/(double)2000));
					//engine.put("percentage", Double.toString(Double.parseDouble(usage)/(double)2000));
				}
				else if(engine.get("service").equals("HMD"))
				{
					engine.put("usage", usage+"/2,000건");
					engine.put("percentage", String.format("%.1f", Integer.parseInt(usage)/(double)2000));
				}
				else if(engine.get("service").equals("GPT"))
				{
					engine.put("usage", usage+"/2,000건");
					engine.put("percentage", String.format("%.1f", Integer.parseInt(usage)/(double)20000));
				}
				else if(engine.get("service").equals("ITF"))
				{

				}
				else if(engine.get("service").equals("IRQA"))
				{
					engine.put("usage", usage+"/2,000건");
					engine.put("percentage", String.format("%.1f", Integer.parseInt(usage)/(double)2000));
				}
			}

			logger.debug("this is json list : "+usageList.toString());
			httpSession.setAttribute("usageList", usageList);
		}catch(Exception e){
			logger.debug(e.getMessage());
		}

		/* APIKEY 전달 */
		//MemberVo member = (MemberVo) httpSession.getAttribute("accessUser");
		try {
			//MemberDto memberDto = memberService.getMemberDetail(member.getEmail());
			httpSession.setAttribute("apiId", memberDto.getApiId());
			httpSession.setAttribute("apikey", memberDto.getApikey());
		} catch (Exception e) {
			httpSession.setAttribute("apiId", null);
			httpSession.setAttribute("apikey", null);
			e.printStackTrace();
		}

		return returnUri;
	}
	
	@RequestMapping(value = "/enApiAccount")
	public String apiAccount(HttpServletRequest request, Model model) {
		logger.info("Welcome member!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "member");
		httpSession.setAttribute("menuName", "ACCOUNT");
		httpSession.setAttribute("subMenuName", "API ACCOUNT");	
		httpSession.setAttribute("menuGrpName", "");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/member/apiAccount.pg";

		/* APIKEY 전달 */
		MemberVo member = (MemberVo) httpSession.getAttribute("accessUser");
		try {
			MemberDto memberDto = memberService.getMemberDetail(member.getEmail());
			httpSession.setAttribute("apiId", memberDto.getApiId());
			httpSession.setAttribute("apikey", memberDto.getApikey());
		} catch (Exception e) {
			httpSession.setAttribute("apiId", null);
			httpSession.setAttribute("apikey", null);
			e.printStackTrace();
		}
		
		return returnUri;
	}	


	
	/** 사용자 프로필 조회 */
	@RequestMapping(value = "/getDetail")
	@ResponseBody
	public MemberDto getMemberDetail(HttpServletRequest request, HttpServletResponse response, MemberVo memberVo) throws Exception {
		MemberDto memberDto;
		
		Gson gson = new Gson();
		
		logger.info("{} .getDetail()", gson.toJson(memberVo));
		System.out.println("getDetail() ======= " + gson.toJson(memberVo));
		
		memberDto = memberService.getMemberDetail(memberVo.getEmail());

		return memberDto;
	}
	
	/** 프로필 수정 */
	@RequestMapping(value = "/setUpdate")
	@ResponseBody
	public MemberDto setUpdate(HttpServletRequest request, HttpServletResponse response, MemberForm memberForm) throws Exception {

		MemberDto memberDto = new MemberDto();
		
		Gson gson = new Gson();

		logger.info("{} .setUpdate()", gson.toJson(memberForm));
		System.out.println("setUpdate() ======= " + gson.toJson(memberForm));	
		
		memberDto = memberService.setUpdate(memberForm);
		
		if("SUCCESS".equals(memberDto.getResult())) {
			MemberVo ssMemberVo = new MemberVo();
			HttpSession session = request.getSession();	
			ssMemberVo = (MemberVo)session.getAttribute("accessUser");
			
			ssMemberVo.setName(memberForm.getName());
			session.setAttribute("accessUser", ssMemberVo);			
		}

		return memberDto;
	}	

	/** 결제 내역 조회 */
	@RequestMapping(value="/krPaymentInfo")
	public String krPaymentInfo(HttpServletRequest request, Model model) {
		logger.info("welcom krPaymentInfo.");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "member");
		httpSession.setAttribute("menuName", "계정");
		httpSession.setAttribute("subMenuName", "결제정보");
		httpSession.setAttribute("menuGrpName", "");

		try {
			Gson gson = new Gson();
			MemberVo member = (MemberVo) httpSession.getAttribute("accessUser");
			List <PaymentVo> paymentList = memberService.selectPaymentInfo(member.getUserno());

			for (PaymentVo vo : paymentList) {
				vo.setGoodName(paymentService.selectProductName(vo.getGoodCode()));
				vo.setIssuerName(paymentService.selectIssuerName(Integer.parseInt(vo.getIssuer())));
			}

			logger.debug(paymentList.toString());

//			logger.debug(gson.toJson(memberService.selectNextBillingInfo(member.getUserno())) );
			model.addAttribute("paymentList", gson.toJson(paymentList));
			model.addAttribute("billingInfo", gson.toJson(memberService.selectBillingInfo(member.getUserno())));
			model.addAttribute("nextBillingInfo", gson.toJson(memberService.selectNextBillingInfo(member.getUserno())));
		} catch (Exception e) {
			e.printStackTrace();
		}

		model.addAttribute("lang", "ko");
		String returnUri = "/kr/member/paymentInfo.pg";	
		
		return returnUri;
	}
	
	/** 결제 내역 조회 */
	@RequestMapping(value="/enPaymentInfo")
	public String enPaymentInfo(HttpServletRequest request, Model model) {
		logger.info("welcom enPaymentInfo.");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "member");
		httpSession.setAttribute("menuName", "ACCOUNT");
		httpSession.setAttribute("subMenuName", "PAYMENT");
		httpSession.setAttribute("menuGrpName", "");

		try {
			Gson gson = new Gson();
			MemberVo member = (MemberVo) httpSession.getAttribute("accessUser");
			List <PaymentVo> paymentList = memberService.selectPaymentInfo(member.getUserno());

			for (PaymentVo vo : paymentList) {
				vo.setGoodName(paymentService.selectProductName(vo.getGoodCode()));
				vo.setIssuerName(paymentService.selectIssuerName(Integer.parseInt(vo.getIssuer())));
			}

			logger.debug(paymentList.toString());

//			logger.debug(gson.toJson(memberService.selectNextBillingInfo(member.getUserno())) );
			model.addAttribute("paymentList", gson.toJson(paymentList));
			model.addAttribute("billingInfo", gson.toJson(memberService.selectBillingInfo(member.getUserno())));
			model.addAttribute("nextBillingInfo", gson.toJson(memberService.selectNextBillingInfo(member.getUserno())));
		} catch (Exception e) {
			e.printStackTrace();
		}

		model.addAttribute("lang", "en");
		String returnUri = "/en/member/paymentInfo.pg";
		
		return returnUri;
	}	

	/** 영수증 조회 */
	@RequestMapping(value="/payinfo",  produces="application/text; charset=utf8")
	@ResponseBody
	public String payinfo(String pano) {
		String result = "";
		Gson gson = new Gson();
		PaymentVo payment = new PaymentVo();

		try {
			payment = memberService.selectOnePaymentInfo(pano);
			payment.setGoodName(paymentService.selectProductName(payment.getGoodCode()));
			payment.setIssuerName(paymentService.selectIssuerName(Integer.parseInt(payment.getIssuer())));
			logger.debug(payment.toString());

			result = gson.toJson(payment);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}
	
	@RequestMapping(value="/krProfile")
	public String krProfile(HttpServletRequest request, Model model) {
		logger.info("welcome krProfile.");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "member");
		httpSession.setAttribute("menuName", "계정");
		httpSession.setAttribute("subMenuName", "프로필");
		httpSession.setAttribute("menuGrpName", "");
		
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/member/profile.pg";
		
		return returnUri;
	}
	
	@RequestMapping(value="/enProfile")
	public String enProfile(HttpServletRequest request, Model model) {
		logger.info("welcome enProfile.");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "member");
		httpSession.setAttribute("menuName", "ACCOUNT");
		httpSession.setAttribute("subMenuName", "PROFILE");
		httpSession.setAttribute("menuGrpName", "");
		
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/member/profile.pg";
		
		return returnUri;
	}	
	
	/** 다음번 결제 시, 요금제 변경 */
	@RequestMapping(value="/changePlanNextBilling")
	@ResponseBody
	public void planChange(HttpServletRequest request , String goodName) {

	    logger.debug("goodname : " + goodName);

		HttpSession session = request.getSession();
		MemberVo member = (MemberVo) session.getAttribute("accessUser");

		int goodCode = paymentService.selectProductId(goodName);

		PaymentVo paymentVo = new PaymentVo();
		paymentVo.setUserNo(member.getUserno());
		paymentVo.setGoodCode(goodCode);


		paymentService.planChange(paymentVo);
	}
	@RequestMapping(value = "/getUseLog")
	@ResponseBody
	public ResultDto getUseLog(HttpServletRequest request, HttpServletResponse response, UseServiceVo useServiceVo) throws Exception {

		logger.info("welcome getUseLog !!");

		HttpSession session = request.getSession();
		MemberVo memberVo = (MemberVo) session.getAttribute("accessUser");

		String serviceId = request.getParameter("serviceName");
		System.out.println("serviceID!!!!!!!!!!!!!!!!!!!!+"+serviceId);
		int userno = memberVo.getUserno();

		useServiceVo.setService_Id(serviceId);
		useServiceVo.setUserNo(userno);
		logger.info("SERVICEID : "+serviceId+" USERNO " +userno);

		Gson gson = new Gson();

		logger.info("getUseLog ===== useServiceVo ======= " + gson.toJson(useServiceVo));
		ResultDto resultDto = memberService.getUseLog(useServiceVo);
		logger.info("result ======= " + resultDto);
		return resultDto;
	}


	@RequestMapping(value="/krPrivacyPolicyAgreement")
	public String krPrivacyPolicyAgreement(HttpServletRequest request, HttpServletResponse response, Model model) {
		logger.info("welcome krPrivacyPolicyAgreement.");
		String paramLang = request.getParameter("lang");

		model.addAttribute("google_url", loginController.loginBtnUrl);
		model.addAttribute("sso_url", sso_url);
		model.addAttribute("client_id", client_id);
		model.addAttribute("redirect_uri", redirect_uri);

		if(!"".equals(paramLang) && paramLang != null) {
			response.addCookie(new Cookie("lang",paramLang));
		}else {
			Cookie[] getCookie = request.getCookies();
			if(getCookie != null){
				for(int i=0; i<getCookie.length; i++){
					Cookie c = getCookie[i];
					String name = c.getName(); // 쿠키 이름 가져오기
					String value = c.getValue(); // 쿠키 값 가져오기

					if("lang".equals(name) && !"".equals(value) ) {
						paramLang = c.getValue();
					}
				}
			}
		}

		String returnUri = "noLayout/kr/member/PrivacyPolicyAgreement.pg";

		return returnUri;
	}

	/** 계정 Status 조회 2019. 10. 01 - LYJ */
	@RequestMapping(value="/getUserStatus", produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public int getUserStatus(HttpServletRequest request) {
		logger.info("welcome getUserStatus.");

		HttpSession httpSession = request.getSession(true);

		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		int userno = memberVo.getUserno();

		MemberForm memberForm = new MemberForm();
		memberForm.setUserNo(userno);

        int result = 0;
        try {
            result = memberService.getUserStatus(memberForm);
        } catch (Exception e) {
            logger.info("Error in getUserStatus : " + e);
        }
        return result;
	}

	/** 계정 Status update 2019. 10. 01 - LYJ */
    @RequestMapping(value="/updateUserStatus")
    @ResponseBody
    public int updateUserStatus(HttpServletRequest request, int status) throws Exception{
        logger.info("welcome updateUserStatus.");

        HttpSession httpsession = request.getSession(true);
        MemberVo memberVo = (MemberVo) httpsession.getAttribute("accessUser");
        int userno = memberVo.getUserno();

        MemberForm memberForm = new MemberForm();

        memberForm.setUserNo(userno);
        memberForm.setStatus(status);
        int result = memberService.updateUserStatus(memberForm);

        return result;
    }

}
