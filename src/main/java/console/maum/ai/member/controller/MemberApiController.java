package console.maum.ai.member.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import console.maum.ai.apiUsage.model.ApiUsageEntity;
import console.maum.ai.apiUsage.model.ApiUsageSttVo;
import console.maum.ai.member.model.*;
import console.maum.ai.member.service.ApiUsageService;
import console.maum.ai.member.service.MemberService;
import org.json.JSONArray;
import org.json.JSONObject;
import org.mortbay.log.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLDecoder;
import java.util.*;

/*
 * API Controller
 */

@PropertySource(value = {
        "classpath:/META-INF/spring/property/config.properties",
        "classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@RestController
public class MemberApiController {

    @Value("${api.url}")
    private String mUrl_ApiServer;

    @Value("${admin.api.id}")
    private String mAdmin_ApiId;

    @Value("${admin.api.pw}")
    private String mAdmin_ApiKey;

    private final static Logger logger = LoggerFactory.getLogger(MemberApiController.class);

    @Autowired
    private MemberService memberService;

    @Autowired
    private ApiUsageService apiUsageService;

    /*
    ** 사용자의 요청에 의한 마케팅동의 거절
    */

    @PostMapping(value= "/member/rejectMarketingAgree")
    public String rejectMarketingAgree(@RequestParam(value="email") String email) throws UnsupportedEncodingException {
        logger.info("===== rejectMarketingAgree =====");

        String result = "succ";
        try {
            int count = memberService.rejectMarketingAgree(email);

            if(count == 0) result = "등록된 계정의 이메일이 아닙니다.";
        } catch (Exception e) {
            e.printStackTrace();
            result = "서버와 연결이 되지 않습니다. 잠시 후, 다시 시도하세요.";
        }

        return result;
    }

    @PostMapping(value="/member/getApiUsageList")
    public List<Map<String, String>> getApiUSageList(HttpServletRequest request, @RequestParam(value="service") String service) throws Exception{

        System.out.println("this is getApiUsageList");
        HttpSession session = request.getSession(true);
        MemberVo memberVo = (MemberVo) session.getAttribute("accessUser");
        MemberDto memberDto = memberService.getMemberDetail(memberVo.getEmail());

        String usageUrl = mUrl_ApiServer + "/admin/selectUsage";
        StringBuffer outResult = new StringBuffer();

        List<Map<String, String>> apiList = new ArrayList<>();
        Map<String, String> sendMap = new HashMap<>();
        sendMap.put("apiId", mAdmin_ApiId);
        sendMap.put("apiKey", mAdmin_ApiKey);
        sendMap.put("reqId", memberDto.getApiId());
        sendMap.put("service", service);

        try{
            ObjectMapper mapper = new ObjectMapper();
            URL url = new URL(usageUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("Accetp-Charset", "UTF-8");

            OutputStream os = conn.getOutputStream();
            os.write(mapper.writeValueAsString(sendMap).getBytes("UTF-8"));
            os.flush();

            int responseCode = conn.getResponseCode();

            if(responseCode == HttpURLConnection.HTTP_OK){
                logger.debug("httpConnection success");
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"), 8);
                String inputLine;
                while((inputLine = br.readLine()) != null){
                    try{
                        outResult.append(new String(URLDecoder.decode(inputLine, "UTF-8")));
                    }catch(Exception e){
                        outResult.append(inputLine);
                    }
                }
                logger.debug("this is apiResult : "+outResult.toString());
                br.close();
            }else{
                throw new Exception();
            }
            conn.disconnect();
        }catch (Exception e){
            logger.debug(e.getMessage());
        }

        logger.debug("this is result : "+outResult.toString());

        try{
            JSONArray apiJsonArray = new JSONArray(outResult.toString());

            for(int i=0;i<apiJsonArray.length();i++){
                Map<String, String> engineMap = new HashMap<>();
                JSONObject engineJObject = apiJsonArray.getJSONObject(i);
                Iterator<String> iterator = engineJObject.keySet().iterator();

                while(iterator.hasNext()){
                    String key = iterator.next();
                    String value = engineJObject.get(key).toString();
                    engineMap.put(key, value);
                }
                apiList.add(engineMap);
            }
        }catch(Exception e){
            logger.debug(e.getMessage());
        }

        logger.debug("this is apiList : "+apiList.toString());

        return apiList;
    }


    /** 이용내역 - 목록 조회 */
    @RequestMapping(value = "/member/getApiAccountList")
    @ResponseBody
    public ApiUsageResultDto getApiAccountList(HttpServletRequest request, HttpServletResponse response, UseServiceVo useServiceVo) throws Exception {
        logger.info("welcome getApiAccountList !!");
        HttpSession session = request.getSession();
        MemberVo memberVo = (MemberVo) session.getAttribute("accessUser");

        ApiUsageResultDto result = null;
        try {
            result = apiUsageService.getApiUsageList(memberVo);
        } catch(Exception e) {
            Log.info("Exception = {}", e.getMessage());
            e.printStackTrace();
        }

        Log.info("==========> {}", result.toString());
        return result;
    }

    /** 특정 기간의 API 사용량 조회 */
    @RequestMapping(value = "/member/apiUsageList")
    @ResponseBody
    public HashMap<String,Object> getApiUsageList(@RequestParam(value="apiId") String apiId,
                                          @RequestParam(value="dateFrom") String dateFrom,
                                          @RequestParam(value="dateTo") String dateTo,
                                          HttpServletRequest request, HttpServletResponse response) {
        logger.info("welcome getApiUsageList !!");

        HashMap<String, Object> result = new HashMap<>();

        ApiUsageRequestDto apiUsageRequestDto = new ApiUsageRequestDto();
        apiUsageRequestDto.setApi_id(apiId);
        apiUsageRequestDto.setBegin_date(dateFrom);
        apiUsageRequestDto.setEnd_date(dateTo);

        logger.info("requestDto => " + apiUsageRequestDto);

        List<ApiUsageEntity> usageList = null;
        try {
            List<ApiUsageEntity> tempList = memberService.getApiUsageList(apiUsageRequestDto);

            List<ApiUsageSttVo> apiUsageSttVoList = memberService.getSttUsageList(apiUsageRequestDto);

            if(apiUsageSttVoList != null) {
                for (ApiUsageSttVo apiUsageSttVo : apiUsageSttVoList) {

                    ApiUsageEntity apiUsageDto = new ApiUsageEntity();
                    apiUsageDto.setService("STT");
                    apiUsageDto.setUsage(apiUsageSttVo.getStt_length());
                    apiUsageDto.setApiId(apiUsageSttVo.getClient_id());
                    apiUsageDto.setDate(apiUsageSttVo.getUpdate_date());
                    apiUsageDto.setUserIp(null);

                    tempList.add(apiUsageDto);
                }
                // STT 사용 기록이 있으면 날짜 순으로 sorting
                tempList = memberService.sortListByDate(tempList);
            }

            usageList = memberService.convertEngineNameKo(tempList);    // 엔진별 카테고리 한글화
            result.put("usageList", usageList);
            result.put("usageListSize", usageList.size());

            // 서비스별 사용 비율 계산
            List<ApiUsageEntity> engineUsageList = memberService.engineUsageList(apiId, usageList);
            result.put("engineUsageList", engineUsageList);
            result.put("engineUsageListSize", engineUsageList.size());

            // API 전체 사용 비율 계산
            List<ApiUsageEntity> totalUsageList = memberService.totalUsageList(apiId, engineUsageList);
            result.put("totalUsageList", totalUsageList);
            result.put("totalUsageListSize", totalUsageList.size());

        } catch (Exception e) {
            logger.info("Exception = {}", e.getMessage());
        }
        return result;
    }
}
