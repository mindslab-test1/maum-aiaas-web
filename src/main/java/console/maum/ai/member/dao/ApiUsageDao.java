package console.maum.ai.member.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import console.maum.ai.apiUsage.model.ApiUsageSttVo;
import console.maum.ai.member.model.*;
import console.maum.ai.member.service.MemberService;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;

@Repository
public class ApiUsageDao {

	@Resource(name = "sqlSession")
	private SqlSession sqlSession;

	@Resource(name = "sqlSession_ApiServer")
	private SqlSession sqlSession_ApiServer;

	@Resource(name = "sqlSession_ApiServerLegacy")
	private SqlSession sqlSession_ApiServerLegacy;

	private static final String NAMESPACE = "console.maum.ai.model.apiUsageMapper";
	private static final String NAMESPACE_API_SERVER = "console.maum.ai.model.api_server.apiUsageMapper";
	private static final String NAMESPACE_API_SERVER_LEGACY = "console.maum.ai.model.api_server_legacy.apiUsageMapper";

	/*
	** DNN STT를 제외한 API 사용량 조회
	*/
	public List<ApiUsageDto> getApiUsageList_ApiServer(ApiUsageRequestDto param) {
		return sqlSession_ApiServer.selectList(NAMESPACE_API_SERVER + ".getApiUsageList", param);
	}

	/*
	** DNN STT에 대한 API 사용량 조회
	*/
	public List<ApiUsageDto> getApiUsageList_ApiServerLegacy(ApiUsageRequestDto param) {
		return sqlSession_ApiServerLegacy.selectList(NAMESPACE_API_SERVER_LEGACY + ".getApiUsageList", param);
	}

	/*
	** API 엔진의 한도 정보 조회
	*/
	public ApiUsageLimitDto getApiUsageLimit(String engine_id) {
		return sqlSession.selectOne(NAMESPACE + ".getApiUsageLimit", engine_id);
	}

	/**
	 * API 서버에서 apiId를 통해 사용량 직접 조회
	 * */
	public List<ApiUsageDto> getApiUsageList(ApiUsageRequestDto apiUsageRequestDto) {
		return sqlSession_ApiServer.selectList(NAMESPACE_API_SERVER + ".getUsageList", apiUsageRequestDto);
	}

	/**
	 * api.maum.ai 서버에서 apiId를 통해 사용량 직접 조회
	 * */
	public List<ApiUsageSttVo> getSttUsageList(ApiUsageRequestDto apiUsageRequestDto) {
		return sqlSession_ApiServerLegacy.selectList(NAMESPACE_API_SERVER_LEGACY + ".getUsageList", apiUsageRequestDto);
	}

	/**
	 * API 서버에서 사용량 조회
	 * */
	public List<ApiUsageVo> getApiUsage(ApiUsageVo apiUsageVo) {
		return sqlSession_ApiServer.selectList(NAMESPACE_API_SERVER + ".getApiUsage", apiUsageVo);
	}

	/**
	 * api.maum.ai 서버에서 STT 사용량 조회
	 * */
	public List<ApiUsageSttVo> getLastDateSttApiUsage(ApiUsageSttVo apiUsageSttVo) {
		return sqlSession_ApiServerLegacy.selectList(NAMESPACE_API_SERVER_LEGACY + ".getLastDateSttApiUsage", apiUsageSttVo);
	}
}