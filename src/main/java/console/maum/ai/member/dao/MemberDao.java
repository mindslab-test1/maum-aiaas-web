package console.maum.ai.member.dao;

import java.util.List;

import javax.annotation.Resource;

import console.maum.ai.apiUsage.model.ApiUsageEngineVo;
import console.maum.ai.member.model.*;
import console.maum.ai.member.service.MemberService;
import console.maum.ai.payment.model.BillingVo;
import console.maum.ai.payment.model.PaymentVo;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

@Repository
public class MemberDao {


	private final static Logger logger = LoggerFactory.getLogger(MemberService.class);
	@Resource(name = "sqlSession")
	private SqlSession sqlSession;

	private static final String NAMESPACE = "console.maum.ai.model.memberMapper";


	public MemberSecurityVo getUserById(String username) {
		return sqlSession.selectOne(NAMESPACE + ".getUserById", username);
	}

	/**
	 * 이미 등록된 user의 createFlag update - 2019. 07. 11 YGE
	 */
	public int updateCreateFlagMember(MemberForm memberForm) throws Exception {
		return sqlSession.update(NAMESPACE + ".updateCreateFlagMember", memberForm);
	}

	/**
	 * getLogInMemberDetail (session info)
	 */
	public MemberDto getLogInMemberDetail(String id) throws Exception {

		return sqlSession.selectOne(NAMESPACE + ".getLogInMemberDetail", id);
	}

	/**
	 * getMaumLogInMemberDetail (session info) - 2020. 10. 05 LYJ
	 */
	public MemberDto getMaumLogInMemberDetail(String id) throws Exception {

		return sqlSession.selectOne(NAMESPACE + ".getMaumLogInMemberDetail", id);
	}

	/**
	 * 계정 - 등록
	 */
	public int insertMember(MemberForm memberForm) throws Exception {
		return sqlSession.insert(NAMESPACE + ".insertMember", memberForm);
	}

	/**
	 * 이용내역 - 목록 수
	 */
	public int getApiAccountCnt(MemberForm memberForm) throws Exception {

		return sqlSession.selectOne(NAMESPACE + ".getApiAccountCnt", memberForm);
	}

	/**
	 * 이용내역 - 목록 조회
	 */
	public List<MemberDto> getApiAccountList(MemberForm memberForm) throws Exception {

		return sqlSession.selectList(NAMESPACE + ".getApiAccountList", memberForm);
	}

	/**
	 * 잔여 사용량 조회
	 */
	public List<UsageVo> selectUsage(int userNo) throws Exception {

		return sqlSession.selectList(NAMESPACE + ".selectUsage", userNo);
	}

	/**
	 * 결제 내역 조회 2019. 5. 17 YBU
	 */
	public List<PaymentVo> selectPaymentInfo(int userNo) throws Exception {

		return sqlSession.selectList(NAMESPACE + ".selectPaymentInfo", userNo);
	}

	/**
	 * 특정 결제 내역 조회 2019. 5. 17 YBU
	 */
	public PaymentVo selectOnePaymentInfo(String pano) throws Exception {

		return sqlSession.selectOne(NAMESPACE + ".selectOnePaymentInfo", pano);
	}

	/**
	 * 현재 결제 내용 조회 2019. 5. 20 YBU
	 */
	public PaymentVo selectBillingInfo(int userNo) throws Exception {

		return sqlSession.selectOne(NAMESPACE + ".selectBillingInfo", userNo);
	}

	/**
	 * Product(Basic or Business) 조회 2019. 5. 20 YGE
	 */
	public int getProductNoInUser(int userNo) throws Exception {
		return sqlSession.selectOne(NAMESPACE + ".getProductNo", userNo);
	}

	/**
	 * ProdcutId 조회 2019. 05. 21 JWS
	 */
	public int getProductIdInProduct(UseAmountVo useAmountVo) throws Exception {
		return sqlSession.selectOne(NAMESPACE + ".getProductId", useAmountVo);
	}

	/**
	 * 사용가능여부 조회 2019. 05. 21 JWS
	 */
	public int getUsable(UseAmountVo useAmountVo) throws Exception {
		return sqlSession.selectOne(NAMESPACE + ".getUsable", useAmountVo);
	}

	/**
	 * 사용량 차감 2019. 05. 21 YGE
	 */
	public void deductUsageAmount(UseAmountVo useAmountVo) throws Exception {
		sqlSession.update(NAMESPACE + ".deductUsageAmount", useAmountVo);
	}

	/**
	 * 사용 log 조회 JWS
	 */
	public List<UseServiceVo> getUseLog(UseServiceVo useServiceVo) throws Exception {
		return sqlSession.selectList(NAMESPACE + ".selectUseLog", useServiceVo);
	}
	public List<UseServiceVo> getApiUseList(UseServiceVo useServiceVo) throws Exception {
		return sqlSession.selectList(NAMESPACE + ".getApiUseList", useServiceVo);
	}

	/**
	 * 이용내역 - 목록 수
	 */
	public int getApiUseCnt(UseServiceVo useServiceVo) throws Exception {

		return sqlSession.selectOne(NAMESPACE + ".getApiUseCnt", useServiceVo);
	}

	/**
	 * 개인정보 동의 업데이트
	 */
	public int updatePrivacyAgreeMember(MemberForm memberForm) throws Exception {

		return sqlSession.update(NAMESPACE + ".updatePrivacyAgreeMember", memberForm);
	}

	/**
	 * 마켓딩 동의 거절
	 */
	public int rejectMarketingAgree(String email) throws Exception {

		return sqlSession.update(NAMESPACE + ".rejectMarketingAgree", email);
	}

    /** signUp user 정보 업데이트 - 2019. 07. 12 YGE  */
    public int updateSignUpMemberInfo(MemberForm memberForm) throws Exception {

        return sqlSession.update(NAMESPACE + ".updateSignUpMemberInfo", memberForm);
    }

	/** 프로필 조회 */
	public MemberDto getMemberDetail(String email) throws Exception {

		return sqlSession.selectOne(NAMESPACE + ".getMemberDetail", email);
	}

	/** 프로필 수정 */
	public int setUpdate(MemberForm memberForm) throws Exception {

		return sqlSession.update(NAMESPACE + ".setUpdate", memberForm);
	}
	/**
	 * 다음 결제 정보 조회
	 * 2019-06-26 유병욱
	 */
    public PaymentVo getNextBillingInfo(int userno) {
		return sqlSession.selectOne(NAMESPACE + ".getNextBillingInfo", userno);
    }

	/**
	 * 해지 사유 등록
	 */
	public int insertUnsubsFeedback(UnsubsFeedbackVo feedback) {
		return sqlSession.insert(NAMESPACE + ".insertUnsubsFeedback", feedback);
	}

	/**
	 * 계정별 status update
	 * 2019.09.19 LYJ
	 */
	public int updateUserStatus(MemberForm memberForm) {
		return sqlSession.update(NAMESPACE + ".updateUserStatus", memberForm);
	}

	/**
	 * 구독 취소 신청 고객 조회
	 * 2019.09.23 LYJ
	 */
	public List<MemberVo> getCancelReqUser() {
		return sqlSession.selectList(NAMESPACE + ".getCancelReqUser");
	}

	/**
     * 계정별 status 조회
     * 2019.09.30 LYJ
     */
	public int getUserStatus(MemberForm memberForm) {
	    return sqlSession.selectOne(NAMESPACE + ".getUserStatus", memberForm);
    }

    /**
     * userNo를 통한 Email 조회
     * 2019.09.30 LYJ
     */
    public MemberForm getUserInfoByNo(MemberForm memberForm) {
        return sqlSession.selectOne(NAMESPACE + ".getUserInfoByNo", memberForm);
    }
	/**
	 * userNo를 통한 CreateDate 조회
	 * 2021.02.19 SMR
	 */
	public String getUserCreateDate(int userNo) {
		return sqlSession.selectOne(NAMESPACE + ".getUserCreateDate", userNo);
	}
    /**
	 * 정기 결제 성공 후 user_t updateDate update
	 *  2019.11.12 LYJ
	 */
    public int updateUpdateDate(int userNo) {
    	return sqlSession.update(NAMESPACE + ".updateUpdateDate", userNo);
	}

	/**
	 * userNo을 통한 payCount 조회
	 * 2020. 01. 20 LYJ
	 */
	public int getUserPayCount(int userNo) {
		return sqlSession.selectOne(NAMESPACE + ".getUserPayCount", userNo);
	}

	/**
	 * userNo을 통한 payCount increase
	 * 2020. 01. 20 LYJ
	 */
	public int increaseUserPayCount(int userNo) {
		return sqlSession.update(NAMESPACE + ".increaseUserPayCount" , userNo);
	}

	/**
	 * userNo을 통한 payCount decrease
	 * 2020. 01. 21 LYJ
	 * */
	public int decreaseUserPayCount(int userNo) {
		return sqlSession.update(NAMESPACE + ".decreaseUserPayCount", userNo);
	}

	/**
	 * userNo를 통한 Email 조회
	 * 2020. 05. 07 LYJ
	 * */
	public String getUserEmail(int userNo) {
		return sqlSession.selectOne(NAMESPACE + ".getUserEmail", userNo);
	}

	/**
	 * apiId를 통해 특정 기간의 API 사용내역 조회
	 * 2020. 12. 29 LYJ
	 * */
	public List<ApiUsageDto> getApiUsageList(ApiUsageRequestDto apiUsageRequestDto) {
		return sqlSession.selectList(NAMESPACE + ".getApiUsageList", apiUsageRequestDto);
	}

	/**
	 * 사용자가 사용한 엔진의 그룹 조회
	 * 2020. 12. 29 LYJ
	 * */
	public int getEngineGrp(String engineName) {
		Integer cnt = sqlSession.selectOne(NAMESPACE + ".getEngineGrp", engineName);
		return cnt == null ? 0 : Integer.parseInt(cnt.toString());
	}

	/**
	 * apiId를 통한 사용자의 결제 방식 조회
	 * 2021. 01. 04 LYJ
	 * */
	public String getPaymentMethodUsingApiId(String apiId) {
		return sqlSession.selectOne(NAMESPACE + ".getPaymentMethodUsingApiId", apiId);
	}

	/**
	 * 엔진 정보 조회
	 * 2021. 01. 05 LYJ
	 * */
	public ApiUsageEngineVo getEngineInfo(String service) {
		return sqlSession.selectOne(NAMESPACE + ".getEngineInfo", service);
	}

	/**
	 * 엔진 구독 제한양 중 최대값 조회
	 * 2021. 01. 07 LYJ
	 * */
	public Long getMaxSubscribeLimitVal() {
		return sqlSession.selectOne(NAMESPACE + ".getMaxSubscribeLimitVal");
	}
}