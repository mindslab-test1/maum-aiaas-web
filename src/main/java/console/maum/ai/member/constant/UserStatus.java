package console.maum.ai.member.constant;

public class UserStatus {
    public static final int INIT = 0;
    public static final int FREE = 1;
    public static final int SUBSCRIBED = 2;
    public static final int UNSUBSCRIBING = 3;
    public static final int UNSUBSCRIBED = 4;
}
