package console.maum.ai.member.model;

import lombok.Data;

@Data
public class UnsubsFeedbackVo {
	private int UserNo;
	private String CreateDate;
	private int FeedbackUsability;
	private int FeedbackDemo;
	private String FeedbackEtc;
}