package console.maum.ai.member.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/*
** DB에 저장된 API 건별 사용량 정보
*/
@Setter
@Getter
@ToString
@EqualsAndHashCode
public class ApiUsageDto extends ApiUsageVo implements Comparable<ApiUsageDto>{
    private int num;
    private String engineGrp;                   // 엔진 그룹

    private Long limitAmount;                   // 사용자의 엔진 제한 양 (엔진 정보는 ApiUsageVo에 포함)
    private Long usageAmount = (long)0;         // 사용자의 엔진 사용 양

    private Long subscribeLimit;                // 구독 서비스 이용 고객의 엔진 제한 양
    private Long baseLimit;                     // 기준 금액 당 사용할 수 있는 엔진 당 제한 양
    private String unitName;                    // 제한 양의 단위명

    private double rate;                        // 엔진별, 카테고리별 사용 비율


    @Override
    public int compareTo(ApiUsageDto o) {
        if (getDate() == null || o.getDate() == null)
            return 0;
        return getDate().compareTo(o.getDate());
    }
}
