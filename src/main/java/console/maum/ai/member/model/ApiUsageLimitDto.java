package console.maum.ai.member.model;

import lombok.Data;

/*
** API별 사용 한도량 정보
*/
@Data
public class ApiUsageLimitDto {
    private String EngineId;            // API 종류
    private String Unit;                // 글자수, 분, 건, 초
    private int UsageLimit;
    private String Desc;
}
