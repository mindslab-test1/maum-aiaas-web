package console.maum.ai.member.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/*
** DB에 저장된 API 건별 사용량 정보
*/
@Data
public class ApiUsageResultDto {
    private String period = "";
    private List<ApiUsageDisplayDto> usages = new ArrayList<ApiUsageDisplayDto>();
}
