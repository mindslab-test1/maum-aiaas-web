package console.maum.ai.member.model;

public class UsageVo {

    private String productName;
    private String usage;

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    @Override
    public String toString() {
        return "UsageVo{" +
                "productName='" + productName + '\'' +
                ", usage='" + usage + '\'' +
                '}';
    }
}
