package console.maum.ai.member.model;

public class UseAmountVo {

    private int service_id;
    private int use_amount;
    private int userNo;
    //3개가 사용가능여부 확인 파라미터

    private int product_no;
    private String service_name;

    public int getUserNo() {
        return userNo;
    }

    public void setUserNo(int userNo) {
        this.userNo = userNo;
    }

    public int getService_id() {
        return service_id;
    }

    public void setService_id(int service_id) {
        this.service_id = service_id;
    }

    public int getUse_amount() {
        return use_amount;
    }

    public void setUse_amount(int use_amount) {
        this.use_amount = use_amount;
    }

    public int getProduct_no() {
        return product_no;
    }

    public void setProduct_no(int product_no) {
        this.product_no = product_no;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    @Override
    public String toString() {
        return "UseAmountVo{" +
                "userNo=" + userNo +
                ", service_id=" + service_id +
                ", use_amount=" + use_amount +
                ", product_no=" + product_no +
                ", service_name='" + service_name + '\'' +
                '}';
    }
}

