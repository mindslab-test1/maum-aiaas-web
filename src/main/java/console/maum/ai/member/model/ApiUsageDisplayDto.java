package console.maum.ai.member.model;

import lombok.Data;

/*
** DB에 저장된 API 건별 사용량 정보
*/
@Data
public class ApiUsageDisplayDto {
    private String service = "";         // API 종류
    private String usage = "";           // 사용한 양 / 제공량
    private String rate = "";            // 비율
    private double rate_val = 0;    // 비율 합계 계산을 위해 필요.
}
