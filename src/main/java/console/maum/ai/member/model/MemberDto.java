package console.maum.ai.member.model;

public class MemberDto extends MemberVo {

	private String result;

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
