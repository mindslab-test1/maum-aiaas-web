package console.maum.ai.member.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@EqualsAndHashCode
public class ApiUsageVo{
    public String service;         // API 종류
    public long usage;
    public String apiId;
    public String date;
    public String userIp;
    public String regDate;
}
