package console.maum.ai.member.model;

import console.maum.ai.common.model.CommonDto;

public class MemberVo extends CommonDto {
	
	private String id;
	private String password;
	private String name;
	private String authority;
	private int enabled;
	private String email;
	private String phone;
	private int point = 0;
	private int voiceagree;
	private int userno ;
	private int type ;
	private int product ;
	private String nationcd;
	private String company;
	private String job;
	private int privacyAgree;
	private int createFlag;
	private String companyEmail;
	private String registerPath;
	private int marketingAgree;
	private String apikey;
	private String apiId;
	private int status;

	public int getUserno() {
		return userno;
	}
	public void setUserno(int userno) {
		this.userno = userno;
	}
	@Override
	public String toString() {
		return "MemberVo [id=" + id + ", password=" + password + ", name=" + name + ", authority=" + authority
				+ ", enabled=" + enabled + ", email=" + email + ", phone=" + phone + ", point=" + point
				+ ", voiceagree=" + voiceagree + ", userno=" + userno + ", status=" + status + "]";
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	public int getEnabled() {
		return enabled;
	}
	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getPoint() {
		return point;
	}
	public void setPoint(int point) {
		this.point = point;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getVoiceagree() {
		return voiceagree;
	}
	public void setVoiceagree(int voiceagree) {
		this.voiceagree = voiceagree;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getProduct() {
		return product;
	}
	public void setProduct(int product) {
		this.product = product;
	}
	public String getNationcd() {
		return nationcd;
	}
	public void setNationcd(String nationcd) {
		this.nationcd = nationcd;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public int getPrivacyAgree() {
		return privacyAgree;
	}
	public void setPrivacyAgree(int privacyAgree) {
		this.privacyAgree = privacyAgree;
	}
	public int getCreateFlag() {
		return createFlag;
	}
	public void setCreateFlag(int createFlag) {
		this.createFlag = createFlag;
	}
	public String getCompanyEmail() {
		return companyEmail;
	}
	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	public String getApikey() {
		return apikey;
	}
	public void setApikey(String apikey) {
		this.apikey = apikey;
	}
	public String getApiId() { return apiId; }
	public String getApiKey() { return apikey; }
	public void setApiId(String apiId) { this.apiId = apiId; }
	public String getRegisterPath() { return registerPath; }
	public void setRegisterPath(String registerPath) { this.registerPath = registerPath; }
	public int getMarketingAgree() { return marketingAgree; }
	public void setMarketingAgree(int marketingAgree) { this.marketingAgree = marketingAgree; }
	public int getStatus() { return status; }
	public void setStatus(int status) { this.status = status; }
}