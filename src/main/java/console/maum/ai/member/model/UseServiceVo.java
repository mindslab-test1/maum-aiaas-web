package console.maum.ai.member.model;

import console.maum.ai.common.model.CommonForm;

public class UseServiceVo extends CommonForm {
    private int id;
    private int userNo;

    private String service_Id;
    private String usage;
    private String createDate;
    private int createUser;
    private String kii;

    private int no;
    private String service;
    private String alimit;
    private String date;
    private String etc;



    public String getKii() {
        return kii;
    }

    public void setKii(String kii) {
        this.kii = kii;
    }

    public String getService_Id() {
        return service_Id;
    }

    public void setService_Id(String service_Id) {
        this.service_Id = service_Id;
    }
    public String getAlimit() {
        return alimit;
    }

    public void setAlimit(String alimit) {
        this.alimit = alimit;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEtc() {
        return etc;
    }

    public void setEtc(String etc) {
        this.etc = etc;
    }

    public int getNo() {
        return no;
    }

    public void setNo(int no) {
        this.no = no;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserNo() {
        return userNo;
    }

    public void setUserNo(int userNo) {
        this.userNo = userNo;
    }

    public String getUsage() {
        return usage;
    }

    public void setUsage(String usage) {
        this.usage = usage;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public int getCreateUser() {
        return createUser;
    }

    public void setCreateUser(int createUser) {
        this.createUser = createUser;
    }

    @Override
    public String toString() {
        return "UseServiceVo{" +
                "id=" + id +
                ", userNo=" + userNo +
                ", service_Id=" + service_Id +
                ", usage=" + usage +
                ", createDate='" + createDate + '\'' +
                ", createUser=" + createUser +
                '}';
    }
}
