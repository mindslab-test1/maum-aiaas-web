package console.maum.ai.member.model;

import console.maum.ai.common.model.CommonForm;

public class MemberForm extends CommonForm {

	private int userNo;
	private String name;
	private String id;
	private String password;
	private String email;
	private String phone;
	private int grade;
	private int type;
	private String account;
	private int privacyAgree;
	private String privacyDate;
	private int djangoid;
	private int active;
	private String authority;
	private int enabled;
	private String nationcd;
	private String company;	
	private int createFlag;
	private String companyEmail;
	private String registerPath;
	private int marketingAgree;
	private String marketingDate;
	private String job;
	private int status;
	private String apiId;


	@Override
	public String toString() {
		return "MemberForm{" +
				"userNo=" + userNo +
				", name='" + name + '\'' +
				", email='" + email + '\'' +
				", apiId='" + apiId + '\'' +
				", status=" + status + '\n' +
				", phone='" + phone + '\'' +
				", company='" + company + '\'' +
				", account='" + account + '\'' +
				", privacyAgree=" + privacyAgree +
				", privacyDate='" + privacyDate + '\'' +
				", createFlag=" + createFlag +
				", marketingAgree=" + marketingAgree +
				'}';
	}

	public int getUserNo() {
		return userNo;
	}
	public void setUserNo(int userNo) {
		this.userNo = userNo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public int getGrade() {
		return grade;
	}
	public void setGrade(int grade) {
		this.grade = grade;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public int getPrivacyAgree() {
		return privacyAgree;
	}
	public void setPrivacyAgree(int privacyAgree) {
		this.privacyAgree = privacyAgree;
	}
	public String getPrivacyDate() {
		return privacyDate;
	}
	public void setPrivacyDate(String privacyDate) {
		this.privacyDate = privacyDate;
	}
	public int getDjangoid() {
		return djangoid;
	}
	public void setDjangoid(int djangoid) {
		this.djangoid = djangoid;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	public String getAuthority() {
		return authority;
	}
	public void setAuthority(String authority) {
		this.authority = authority;
	}
	public int getEnabled() {
		return enabled;
	}
	public void setEnabled(int enabled) {
		this.enabled = enabled;
	}
	public String getCompanyEmail() {
		return companyEmail;
	}
	public void setCompanyEmail(String companyEmail) {
		this.companyEmail = companyEmail;
	}
	public int getCreateFlag() {
		return createFlag;
	}
	public void setCreateFlag(int createFlag) {
		this.createFlag = createFlag;
	}
	public String getNationcd() {
		return nationcd;
	}
	public void setNationcd(String nationcd) {
		this.nationcd = nationcd;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getJob() {
		return job;
	}
	public void setJob(String job) {
		this.job = job;
	}
	public String getRegisterPath() {
		return registerPath;
	}
	public void setRegisterPath(String registerPath) {
		this.registerPath = registerPath;
	}
	public int getMarketingAgree() {
		return marketingAgree;
	}
	public void setMarketingAgree(int marketingAgree) {
		this.marketingAgree = marketingAgree;
	}
	public String getMarketingDate() {
		return marketingDate;
	}
	public void setMarketingDate(String marketingDate) {
		this.marketingDate = marketingDate;
	}
	public int getStatus() { return status; }
	public void setStatus(int status) { this.status = status; }
	public String getApiId() { return apiId; }
	public void setApiId(String apiId) { this.apiId = apiId; }
}
