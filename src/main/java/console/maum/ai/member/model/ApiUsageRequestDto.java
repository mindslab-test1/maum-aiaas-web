package console.maum.ai.member.model;

import lombok.Data;

/*
** 월간 API 사용량 조회 요청 파라메타
*/
@Data
public class ApiUsageRequestDto {
    private String api_id;
    private String begin_date;
    private String end_date;
}
