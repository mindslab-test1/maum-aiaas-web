package console.maum.ai.stt.controller;

import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.member.model.MemberVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import console.maum.ai.stt.service.SttApiService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@RestController
public class SttApiController {
	
	@Autowired
	private SttApiService sttApiService;

	@PostMapping("/api/stt")
	public String getApiStt(@RequestParam("file") MultipartFile file, @RequestParam(value = "lang") String lang,
			@RequestParam(value = "level") String level, @RequestParam(value = "sampling") String sampling, HttpServletRequest request) {
		HttpSession httpSession = request.getSession(true);

		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		String apiId = memberVo.getApiId();
		String apiKey = memberVo.getApiKey();

		String result = sttApiService.getApiStt(apiId, apiKey,file,lang,level,sampling, PropertyUtil.getUploadPath() +"/stt");
		
		return result;

	}

	@PostMapping("/api/stt/cnnSttSimple")
	public ResponseEntity<String> getApiCnnStt(@RequestParam("file") MultipartFile file, @RequestParam(value = "lang") String lang, @RequestParam(value = "model") String model,
									   HttpServletRequest request, HttpServletResponse response) throws IOException {
		HttpSession httpSession = request.getSession(true);

		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		String apiId = memberVo.getApiId();
		String apiKey = memberVo.getApiKey();
		String userName = memberVo.getEmail().split("@")[0];

		String result = sttApiService.getApiCnnStt(apiId, apiKey, userName, file, lang, model, PropertyUtil.getUploadPath() +"/stt/" + userName);
		System.out.println("result=" + result);

		HttpHeaders responseHeaders = new HttpHeaders(); responseHeaders.add("Content-Type", "text/html; charset=utf-8");

		return new ResponseEntity<String>(result, responseHeaders, HttpStatus.OK);
	}

}
