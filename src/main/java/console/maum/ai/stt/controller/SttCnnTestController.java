package console.maum.ai.stt.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@Controller
@RequestMapping(value = "/stt")
public class SttCnnTestController {

	private static final Logger logger = LoggerFactory.getLogger(SttCnnTestController.class);

	/**
	 * stt Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krSttCnnTestMain")
	public String krSttMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krSttCnnTestMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "sttMain");
		httpSession.setAttribute("menuName", "API Services");		
		httpSession.setAttribute("subMenuName", "음성인식(STT) Cnn Test");
		httpSession.setAttribute("menuGrpName", "음성");
		
		model.addAttribute("lang", "ko");		
		String returnUri = "/kr/stt/sttCnnTestMain.pg";

		return returnUri;
	}
	


}