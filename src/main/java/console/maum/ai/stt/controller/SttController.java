package console.maum.ai.stt.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/cloudApi/stt")
public class SttController {

	private static final Logger logger = LoggerFactory.getLogger(SttController.class);

	/**
	 * stt Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krSttMain")
	public String krSttMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krSttMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "sttMain");
		httpSession.setAttribute("menuName", "음성");
		httpSession.setAttribute("subMenuName", "음성인식(STT)");	
		httpSession.setAttribute("menuGrpName", "음성");
		
		model.addAttribute("lang", "ko");		
		String returnUri = "/kr/stt/sttMain.pg";

		return returnUri;
	}
	
	/**
	 * stt Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enSttMain")
	public String enSttMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enSttMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "sttMain");
		httpSession.setAttribute("menuName", "Voice");
		httpSession.setAttribute("subMenuName", "Speech Recognition");
		httpSession.setAttribute("menuGrpName", "Speech");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/stt/sttMain.pg";
		
		return returnUri;
	}	

}