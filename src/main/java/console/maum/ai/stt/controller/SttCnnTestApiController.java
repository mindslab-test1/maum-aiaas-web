package console.maum.ai.stt.controller;

import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.stt.service.SttApiService;
import console.maum.ai.stt.service.SttCnnTestApiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

@RestController
public class SttCnnTestApiController {
	
	@Autowired
	private SttCnnTestApiService sttApiService;

	@PostMapping("/api/sttCnnTest")
	public String getApiStt(@RequestParam("file") MultipartFile file, @RequestParam(value = "lang") String lang,
			@RequestParam(value = "level") String level, @RequestParam(value = "sampling") String sampling, HttpServletRequest request) {
		HttpSession httpSession = request.getSession(true);
		MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
		String apiId = memberVo.getApiId();
		String apiKey = memberVo.getApiKey();
		String result = sttApiService.getApiSttCnnTest(apiId, apiKey,file,lang,level,sampling, PropertyUtil.getUploadPath() +"/stt");
		
		return result;

	}

}
