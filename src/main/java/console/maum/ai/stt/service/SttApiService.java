package console.maum.ai.stt.service;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",
		"classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@Service
public class SttApiService {
	private static final Logger logger = LoggerFactory.getLogger(SttApiService.class);

	@Value("${api.url}")
	private String mUrl_ApiServer;

	public String getApiStt(String apiId, String apiKey, MultipartFile file, String lang, String level, String sampling, String uploadPath) {

		if (file == null) {
			throw new RuntimeException("You must select the a file for uploading");
		}

		//String url = mUrl_ApiServer + "/api/stt/";
		String url = "https://api.maum.ai" + "/api/stt/";

		String logMsg = "\n===========================================================================\n";
		logMsg += "STT API @ PARAMS \n";
		logMsg += String.format(":: %-10s = %s%n", "URL", url);
		logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
		logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
		logMsg += String.format(":: %-10s = %s%n", "lang", lang);
		logMsg += String.format(":: %-10s = %s%n", "level", level);
		logMsg += String.format(":: %-10s = %s%n", "sampling", sampling);
		logMsg += String.format(":: %-10s = %s%n", "file", file.getOriginalFilename());
		logMsg += "===========================================================================";
		logger.info(logMsg);

		try {

			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(url);

			File varfile = new File(uploadPath);

			// 디렉토리가 없으면 생성
			if (!varfile.exists()) {
				logger.info("create Dir : {}", varfile.getPath());
				varfile.mkdirs();
			}

			varfile = new File((uploadPath + "/" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)));//.replace(".wav",".mp3"));
			file.transferTo(varfile);

			System.out.println("======varfile.getPath======:: " + varfile.getPath());

			FileBody fileBody = new FileBody(varfile);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addPart("file", fileBody);
			builder.addPart("lang", new StringBody(lang, ContentType.MULTIPART_FORM_DATA));
			builder.addPart("level", new StringBody(level, ContentType.MULTIPART_FORM_DATA));
			builder.addPart("sampling", new StringBody(sampling, ContentType.MULTIPART_FORM_DATA));

			builder.addPart("ID", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
			builder.addPart("key", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));
			builder.addPart("cmd", new StringBody("runFileStt", ContentType.MULTIPART_FORM_DATA));

			HttpEntity entity = builder.build();

			post.setEntity(entity);
			HttpResponse response = client.execute(post);

			int responseCode = response.getStatusLine().getStatusCode();
			logger.info("responseCode = {}", responseCode);

			if (responseCode == 200) {
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "utf-8"));

				StringBuffer result = new StringBuffer();
				String line = "";
				while ((line = rd.readLine()) != null) {
					// result.append(URLDecoder.decode(line, "UTF-8"));
					result.append(line);
				}
				System.out.println("Response Result : " + result.toString());

				// String jsonString = "{\"name\":\"moogii\",\"favorites\":{\"color\":\"white\",
				// \"foods\":[{\"id\":\"chicken\", \"priority\":1},{\"id\":\"pizza\",
				// \"priority\":2},{\"id\":\"pasta\", \"priority\":3}]}}";

				//STT 의 텍스트 결과값만 추출
				JsonParser jsonParser = new JsonParser();
				JsonElement jsonElement = jsonParser.parse(result.toString());
				String datastring = jsonElement.getAsJsonObject().get("data").toString();
				System.out.println("Response Result 2 : " + datastring);
				// return result.toString().replace('+',' ');


				return result.toString();
			} else {
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

				StringBuffer result = new StringBuffer();
				String line = "";
				while ((line = rd.readLine()) != null) {
					result.append(line);
				}

				System.out.println("Response Result : " + result.toString().replace('+', ' '));
				System.out.println("API 호출 에러 발생 : 에러코드=" + responseCode);
				return "{ \"status\": \"error\" }";

			}
		} catch (Exception e) {
			System.out.println(e);
			return "{ \"status\": \"error\" }";
		}
	}


	public String getApiCnnStt(String apiId, String apiKey, String userName, MultipartFile file, String lang, String model, String uploadPath) throws IOException {

		if (file == null) {
			throw new RuntimeException("You must select the a file for uploading");
		}

		String url = "https://api.maum.ai" + "/stt/cnnSttSimple/";

		String logMsg = "\n===========================================================================\n";
		logMsg += "STT API @ PARAMS \n";
		logMsg += String.format(":: %-10s = %s%n", "URL", url);
		logMsg += String.format(":: %-10s = %s%n", "apiId", apiId);
		logMsg += String.format(":: %-10s = %s%n", "apiKey", apiKey);
		logMsg += String.format(":: %-10s = %s%n", "userName", userName);
		logMsg += String.format(":: %-10s = %s%n", "lang", lang);
		logMsg += String.format(":: %-10s = %s%n", "model", model);
		logMsg += String.format(":: %-10s = %s%n", "file", file.getOriginalFilename());
		logMsg += "===========================================================================";
		logger.info(logMsg);

		File varfile = new File(uploadPath);

		// 디렉토리가 없으면 생성
		if (!varfile.exists()) {
			logger.info("create Dir : {}", varfile.getPath());
			varfile.mkdirs();
		}

		varfile = new File((uploadPath + "/" + file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1))); //.replace(".wav",".mp3")
		file.transferTo(varfile);

		try {

			Calendar cal = Calendar.getInstance();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
			String datestr = sdf.format(cal.getTime());

			String newFileName = "_"+datestr+".wav";
			String command = "ffmpeg -i " + varfile.getAbsolutePath() + " -ac 1 -ab 16000 -ar 8000 " + varfile.getAbsolutePath().replace(".wav", newFileName);

			String processLine;
			Process p = Runtime.getRuntime().exec(command);
			BufferedReader in = new BufferedReader(
					new InputStreamReader(p.getErrorStream()));
			while ((processLine = in.readLine()) != null) {
				System.out.println(processLine);
			}
			in.close();

			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(url);

			System.out.println("======varfile.getPath======:: " + varfile.getPath());

			FileBody fileBody = new FileBody(new File(varfile.getAbsolutePath().replace(".wav", newFileName)));

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
			builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));
			builder.addPart("file", fileBody);
			builder.addPart("model", new StringBody(model, ContentType.MULTIPART_FORM_DATA));

			HttpEntity entity = builder.build();

			post.setEntity(entity);
			HttpResponse response = client.execute(post);

			int responseCode = response.getStatusLine().getStatusCode();
			logger.info("responseCode = {}", responseCode);

			if (responseCode == 200) {
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));

				StringBuffer result = new StringBuffer();
				String line = "";
				while ((line = rd.readLine()) != null) {
					// result.append(URLDecoder.decode(line, "UTF-8"));
					result.append(line);
				}
				System.out.println("Response Result : " + result.toString());

				//STT 의 텍스트 결과값만 추출
				JsonParser jsonParser = new JsonParser();
				JsonElement jsonElement = jsonParser.parse(result.toString());
				String datastring = jsonElement.getAsJsonObject().get("result").toString();
				System.out.println("Response Result 2 : " + datastring);

				return result.toString();
			} else {
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

				StringBuffer result = new StringBuffer();
				String line = "";
				while ((line = rd.readLine()) != null) {
					result.append(line);
				}

				System.out.println("Response Result : " + result.toString().replace('+', ' '));
				System.out.println("API 호출 에러 발생 : 에러코드=" + responseCode);
				return "{ \"status\": \"error\" }";

			}
		} catch (Exception e) {
			System.out.println(e);
			return "{ \"status\": \"error\" }";
		}
	}


/*
	public String downSampling(File file, String fileName, String uploadPath, String apiId, String apiKey, String model, String url) throws IOException {

		logger.info("fileName = " + fileName);
		logger.info("uploadPath = " + uploadPath);

		RunProcessFunction func = new RunProcessFunction();
		func.setWorkingDirectory(uploadPath + "/");
		final FFmpeg ffmpeg = new FFmpeg("/usr/bin/ffmpeg");
		final FFprobe ffprobe = new FFprobe("/usr/bin/ffprobe");
		logger.info(" downSampling -------------------------------------------------------------- ready ! ");

		final FFmpegBuilder builder = new FFmpegBuilder()
				.setInput(file.getAbsolutePath())
				.overrideOutputFiles(true)
				.addOutput(file.getAbsolutePath())
				.setAudioChannels(FFmpeg.AUDIO_MONO)
				.setAudioBitRate(16000L)
				.setAudioSampleRate(FFmpeg.AUDIO_SAMPLE_8000)
				.done();

		logger.info(fileName + " written ready ! ");
		final FFmpegExecutor executor = new FFmpegExecutor(ffmpeg, ffprobe);

		FFmpegJob job =  executor.createJob(builder, new ProgressListener() {

			String apiResult="";
			@Override
			public void progress(Progress progress) {

				logger.error(progress.status.toString());
				//if(progress.status.toString().equalsIgnoreCase("end")) {
				if(progress.isEnd()) {
					logger.error(" ! in progress ------------------------------------------ 1");
					apiResult = cnnSttApiCall(file, apiId, apiKey, model, url);
					testVal(apiResult);
					setFlag(true);
				}
			}
		});

		job.run();

//		int timer = 0;
//		while(!getFlag()) {
//			try {
//				Thread.sleep(500);
//				timer++;
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//			if(timer > 10) {
//				break;
//			}
//		}
		logger.error("====================== " + returnStr);
		return returnStr;
	}

	public boolean flag = false;
	public String returnStr = "";
	public void setFlag(boolean val) {
		logger.error(" ! in progress ------------------------------------------ setFlag 1");
		flag = val;
		logger.error(" ! in progress ------------------------------------------ setFlag 2 ==> " + flag );
	}
	public boolean getFlag() {
		return flag;
	}
	public void testVal(String apiResult) {
		logger.error(" ! in progress ------------------------------------------ testVal 1");
		returnStr = apiResult;
		logger.error(" ! in progress ------------------------------------------ testVal 2");
	}

	public String cnnSttApiCall(File varfile, String apiId, String apiKey, String model, String url) {

		try {
			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(url);

			System.out.println("======varfile.getPath======:: " + varfile.getPath());

			FileBody fileBody = new FileBody(varfile);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
			builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));
			builder.addPart("file", fileBody);
			builder.addPart("model", new StringBody(model, ContentType.MULTIPART_FORM_DATA));

			HttpEntity entity = builder.build();

			post.setEntity(entity);
			HttpResponse response = client.execute(post);

			int responseCode = response.getStatusLine().getStatusCode();
			logger.info("responseCode = {}", responseCode);

			if (responseCode == 200) {
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "UTF-8"));

				StringBuffer result = new StringBuffer();
				String line = "";
				while ((line = rd.readLine()) != null) {
					// result.append(URLDecoder.decode(line, "UTF-8"));
					result.append(line);
				}
				System.out.println("Response Result : " + result.toString());

				//STT 의 텍스트 결과값만 추출
				JsonParser jsonParser = new JsonParser();
				JsonElement jsonElement = jsonParser.parse(result.toString());
				String datastring = jsonElement.getAsJsonObject().get("result").toString();
				System.out.println("Response Result 2 : " + datastring);

				return result.toString();
			} else {
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

				StringBuffer result = new StringBuffer();
				String line = "";
				while ((line = rd.readLine()) != null) {
					result.append(line);
				}

				System.out.println("Response Result : " + result.toString().replace('+', ' '));
				System.out.println("API 호출 에러 발생 : 에러코드=" + responseCode);
				return "{ \"status\": \"error\" }";

			}
		} catch (Exception e) {
			System.out.println(e);
			return "{ \"status\": \"error\" }";
		}
	}   */
}