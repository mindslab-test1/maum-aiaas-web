package console.maum.ai.stt.service;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

@PropertySource(value = {
		"classpath:/META-INF/spring/property/config.properties",
		"classpath:/META-INF/spring/property/config-${spring.profiles.active}.properties"
})
@Service
public class SttCnnTestApiService {
	@Value("${api.url}")
	private String mUrl_ApiServer;
		
	public String getApiSttCnnTest(String apiId, String apiKey,MultipartFile file, String lang, String level, String sampling, String uploadPath) {

		if (file == null) {
			throw new RuntimeException("You must select the a file for uploading");
		}

		System.out.println("======lang======:: " + lang);
		System.out.println("======level======:: " + level);
		System.out.println("======sampling======:: " + sampling);
		System.out.println("======file.getOriginalFilename()======:: " + file.getOriginalFilename());
		System.out.println(
				"======file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf(\"\\\\\")+1)======:: "
						+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1));
//    	String tmp = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\")+1);
//    	System.out.println("======file.getOriginalFilename()======:: "+tmp.substring(tmp.lastIndexOf("\\")+1));

		try {
			String url = "https://api.maum.ai/stt/cnnStt";

			HttpClient client = HttpClients.createDefault();
			HttpPost post = new HttpPost(url);

			File varfile = new File(uploadPath);

			// 디렉토리가 없으면 생성
			if (varfile.exists() == false) {
				varfile.mkdirs();
			}			

			System.out.println("======uploadPath====== ::  " + uploadPath);
//			File varfile = convertMultiPartToFile(file); // new File(file.getOriginalFilename());


			varfile = new File((uploadPath +"/"+ file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("\\") + 1)).replace(".pcm",".wav"));
//			file.transferTo(varfile);


            PCMtoFile(varfile.getAbsolutePath(), file.getBytes());


            System.out.println("======varfile.length======:: " + varfile.length());
			System.out.println("======varfile.getPath======:: " + varfile.getPath());
			System.out.println("======varfile.getName======:: " + varfile.getName());

			FileBody fileBody = new FileBody(varfile);
			
			System.out.println("======fileBody.getFilename======:: " + fileBody.getFilename());
			//
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.addPart("file", fileBody);
			builder.addPart("apiId", new StringBody(apiId, ContentType.MULTIPART_FORM_DATA));
			builder.addPart("apiKey", new StringBody(apiKey, ContentType.MULTIPART_FORM_DATA));

			HttpEntity entity = builder.build();
			//
			post.setEntity(entity);
			HttpResponse response = client.execute(post);

			int responseCode = response.getStatusLine().getStatusCode();
			System.out.println("\nSending 'POST' request to URL : " + url);
			System.out.println("Post parameters : " + post.getEntity());
			System.out.println("Response Code : " + response.getStatusLine().getStatusCode());

			if (responseCode == 200) {
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

				StringBuffer result = new StringBuffer();
				String line = "";
				while ((line = rd.readLine()) != null) {
					// result.append(URLDecoder.decode(line, "UTF-8"));
					result.append(line);
				}
				System.out.println("Response Result : " + result.toString());

				// String jsonString = "{\"name\":\"moogii\",\"favorites\":{\"color\":\"white\",
				// \"foods\":[{\"id\":\"chicken\", \"priority\":1},{\"id\":\"pizza\",
				// \"priority\":2},{\"id\":\"pasta\", \"priority\":3}]}}";

                //STT 의 텍스트 결과값만 추출
				JsonParser jsonParser = new JsonParser();
				JsonElement jsonElement = jsonParser.parse(result.toString());
				String datastring = jsonElement.getAsJsonObject().get("result").toString();
				System.out.println("Response Result 2 : " + datastring);
				// return result.toString().replace('+',' ');


				return result.toString();
			} else {
				BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));

				StringBuffer result = new StringBuffer();
				String line = "";
				while ((line = rd.readLine()) != null) {
					result.append(line);
				}

				System.out.println("Response Result : " + result.toString().replace('+', ' '));
				System.out.println("API 호출 에러 발생 : 에러코드=" + responseCode);
				return "{ \"status\": \"error\" }";

			}
		} catch (Exception e) {
			System.out.println(e);
			return "{ \"status\": \"error\" }";
		}
	}


    public void PCMtoFile(String filepath, byte[] data) throws IOException {
        final short NUM_CHANNEL = 1;
        final int BITS_PER_SAMPLE = 16;
        final int SAMPLE_RATE = 16000;

        FileOutputStream mFileOutStream = new FileOutputStream(filepath);
        DataOutputStream mOutStream = new DataOutputStream(mFileOutStream);

        mOutStream.writeBytes("RIFF"); // 00 - RIFF
        mOutStream.write(intToByteArray(32 + data.length), 0, 4); // 04 - how big is the rest of this file? ==> Update 필요
        mOutStream.writeBytes("WAVE"); // 08 - WAVE
        mOutStream.writeBytes("fmt "); // 12 - fmt
        mOutStream.write(intToByteArray(16), 0, 4); // 16 - size of this chunk
        mOutStream.write(shortToByteArray((short) 1), 0, 2); // 20 - what is the audio format? 1 for PCM = Pulse Code M
        mOutStream.write(shortToByteArray(NUM_CHANNEL), 0, 2); // 22 - mono or stereo? 1 or 2? (or 5 or ???)
        mOutStream.write(intToByteArray(SAMPLE_RATE), 0, 4); // 24 - samples per second (numbers per second)
        mOutStream.write(intToByteArray((BITS_PER_SAMPLE / 8) * SAMPLE_RATE * NUM_CHANNEL), 0, 4); // 28 - bytes per second
        mOutStream.write(shortToByteArray((short) ((BITS_PER_SAMPLE / 8) * NUM_CHANNEL)), 0, 2); // 32 - # of bytes in one sample, for all channels
        mOutStream.write(shortToByteArray((short) BITS_PER_SAMPLE), 0, 2); // 34 - how many bits in a sample(number)? usually 16 or 24
        mOutStream.writeBytes("data"); // 36 - data
        mOutStream.write(intToByteArray(data.length), 0, 4); // 40 - how big is this data chunk ==> Update 필요
        mOutStream.write(data);
        mOutStream.close();
        mFileOutStream.close();
    }

    /*
     ** INT -> BYTE []
     */
    private byte[] intToByteArray(int i) {
        byte[] b = new byte[4];
        b[0] = (byte) (i & 0x00FF);
        b[1] = (byte) ((i >> 8) & 0x000000FF);
        b[2] = (byte) ((i >> 16) & 0x000000FF);
        b[3] = (byte) ((i >> 24) & 0x000000FF);
        return b;
    }

    /*
     ** SHORT -> BYTE []
     */
    private byte[] shortToByteArray(short data) {
        return new byte[] { (byte) (data & 0xff), (byte) ((data >>> 8) & 0xff) };
    }
}