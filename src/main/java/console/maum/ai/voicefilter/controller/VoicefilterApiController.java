package console.maum.ai.voicefilter.controller;

import console.maum.ai.common.util.PropertyUtil;
import console.maum.ai.member.model.MemberVo;

import console.maum.ai.voicefilter.service.VoicefilterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/*
 * API Controller
 */


@RestController
public class VoicefilterApiController {

    @Autowired
    private VoicefilterService voicefilterService;

    @PostMapping("/api/dap/voicefilter")
    @ResponseBody
    public ResponseEntity<byte[]> getApiVoiceFilter(@RequestParam("mixedVoice") MultipartFile mixedVoice,
                                                    @RequestParam("reqVoice")MultipartFile reqVoice ,
                                                    HttpServletRequest request){

        HttpSession httpSession = request.getSession(true);

        MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
        String apiId = memberVo.getApiId();
        String apiKey = memberVo.getApiKey();
        String savedId = memberVo.getId();

        return voicefilterService.getApiVoiceFilter(apiId, apiKey, mixedVoice,reqVoice,PropertyUtil.getUploadPath()+"/voiceFilter/", savedId);
    }
	
}
