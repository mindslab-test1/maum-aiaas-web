package console.maum.ai.voicefilter.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/cloudApi/voicefilter")
public class VoicefilterController {

	private static final Logger logger = LoggerFactory.getLogger(VoicefilterController.class);

	/**
	 * Voicefilter Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/krVoicefilterMain")
	public String krVoicefilterMain(HttpServletRequest request, Model model) {
		logger.info("Welcome krVoicefilterMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "voicefilterMain");
		httpSession.setAttribute("menuName", "음성");
		httpSession.setAttribute("subMenuName", "Voice Filter");
		httpSession.setAttribute("menuGrpName", "음성");
		
		model.addAttribute("lang", "ko");
		String returnUri = "/kr/voicefilter/voicefilterMain.pg";	
		
		return returnUri;
	}

	/**
	 * mrc Main
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/enVoicefilterMain")
	public String enVoicefilterMain(HttpServletRequest request, Model model) {
		logger.info("Welcome enVoicefilterMain!");
		HttpSession httpSession = request.getSession(true);
		httpSession.setAttribute("selectMenu", "voicefilterMain");
		httpSession.setAttribute("menuName", "Voice");
		httpSession.setAttribute("subMenuName", "Voice Filter");
		httpSession.setAttribute("menuGrpName", "Speech");
		
		model.addAttribute("lang", "en");
		String returnUri = "/en/voicefilter/voicefilterMain.pg";	
		
		return returnUri;
	}	
}