package console.maum.ai.kiosk;


import com.google.gson.Gson;
import console.maum.ai.member.model.MemberForm;
import console.maum.ai.member.model.MemberSecurityVo;
import console.maum.ai.member.model.MemberVo;
import console.maum.ai.member.service.MemberDetailsService;
import console.maum.ai.member.service.MemberService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.social.connect.Connection;
import org.springframework.social.google.api.Google;
import org.springframework.social.google.api.impl.GoogleTemplate;
import org.springframework.social.google.api.plus.Person;
import org.springframework.social.google.api.plus.PlusOperations;
import org.springframework.social.google.connect.GoogleConnectionFactory;
import org.springframework.social.oauth2.AccessGrant;
import org.springframework.social.oauth2.GrantType;
import org.springframework.social.oauth2.OAuth2Operations;
import org.springframework.social.oauth2.OAuth2Parameters;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Map;


@Controller
@RequestMapping(value = "/kiosk")
public class KioskController {

    /* GoogleLogin */
    @Autowired
    private GoogleConnectionFactory googleConnectionFactory;

    @Autowired
    private MemberDetailsService memberDetailsService;

    @Autowired
    private MemberService memberService;


    @Value("${url.domain}")
    private String mDomain;


    private static final Logger logger = LoggerFactory.getLogger(KioskController.class);

    Gson gson = new Gson();
    /**
     * kiosk Login Main
     * @param model
     * @return
     */

    @RequestMapping(value = "/krKioskLoginMain")
    public String kioskLoginMain(String error, HttpServletRequest request, Model model){
        logger.info("Welcome kioskLoginMain!");


        /* 구글code 발행 */
        OAuth2Parameters googleOAuth2Parameters = new OAuth2Parameters();
        googleOAuth2Parameters.setRedirectUri(mDomain + "/kiosk/login/oauth2callback.do");
        googleOAuth2Parameters.setScope("https://www.googleapis.com/auth/userinfo.email");

        OAuth2Operations oauthOperations = googleConnectionFactory.getOAuthOperations();
        String url = oauthOperations.buildAuthorizeUrl(GrantType.AUTHORIZATION_CODE, googleOAuth2Parameters);


        model.addAttribute("google_url", url);
        model.addAttribute("error", error);
        model.addAttribute("errormsg", request.getParameter("errormsg"));

        String returnPage ="";
        HttpSession httpSession = request.getSession(true);

        if(httpSession != null){
            MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
            if(memberVo != null) {
                String insUserId = memberVo.getId();
                if("".equals(insUserId)){
                    returnPage = "noLayout/kr/kiosk/krKioskLoginMain.pg";
                }else {
                    returnPage = "noLayout/kr/kiosk/krKioskMain.pg";
                }

            }else {
                returnPage = "noLayout/kr/kiosk/krKioskLoginMain.pg";
            }
        }else {
            returnPage = "noLayout/kr/kiosk/krKioskLoginMain.pg";
        }


        return returnPage;
    }

    /**
     * kiosk Main
     * @return
     */

    @RequestMapping(value = "/krKioskMain")
    public String kioskMain(HttpSession session, HttpServletRequest request){
        logger.info("Welcome kioskMain!");

        session.setMaxInactiveInterval(-1);// 0이나 -1로 주면 유효기간 무한

        String returnPage ="";
        HttpSession httpSession = request.getSession(true);

        if(httpSession != null){
            MemberVo memberVo = (MemberVo) httpSession.getAttribute("accessUser");
            if(memberVo != null) {
                String insUserId = memberVo.getId();
                if("".equals(insUserId)){
                    returnPage = "redirect:/kiosk/krKioskLoginMain";
                }else {
                    returnPage = "noLayout/kr/kiosk/krKioskMain.pg";
                }

            }else {
                returnPage = "redirect:/kiosk/krKioskLoginMain";
            }
        }else {
            returnPage = "redirect:/kiosk/krKioskLoginMain";
        }


        return returnPage;

    }

    // 구글 Callback호출 메소드
    @RequestMapping(value = "/login/oauth2callback.do", method = { RequestMethod.GET, RequestMethod.POST })
    public String googleCallback(HttpServletRequest request, Model model, @RequestParam String code) throws IOException, Exception {
        logger.info("googleCallback");
        Gson gson = new Gson();

        //권한 체크
        OAuth2Operations oauthOperations = googleConnectionFactory.getOAuthOperations();
        AccessGrant accessGrant = null;
        try {
            OAuth2Parameters googleOAuth2Parameters = new OAuth2Parameters();
            googleOAuth2Parameters.setRedirectUri(mDomain + "/kiosk/login/oauth2callback.do");
            googleOAuth2Parameters.setScope("https://www.googleapis.com/auth/userinfo.email");

            accessGrant = oauthOperations.exchangeForAccess(code, googleOAuth2Parameters.getRedirectUri(), null);

        }catch(HttpClientErrorException e) {

            logger.info("HttpClientErrorException  : {}", e.getMessage());

            throw new UsernameNotFoundException("");
        }

        //토큰 받고
        String accessToken = accessGrant.getAccessToken();
        Long expireTime = accessGrant.getExpireTime();

        logger.info("accessToken  : {}", accessToken);
        logger.info("expireTime  : {}", expireTime);

        if (expireTime != null && expireTime < System.currentTimeMillis()) {
            accessToken = accessGrant.getRefreshToken();
            logger.info("accessToken is expired. refresh token = {}", accessToken);
        }

        //커넥션 맺기
        Connection<Google> connection = googleConnectionFactory.createConnection(accessGrant);
        Google google = connection == null ? new GoogleTemplate(accessToken) : connection.getApi();

        //이메일 구하고
        PlusOperations plusOperations = google.plusOperations();
        Person person = plusOperations.getGoogleProfile();

        String email = person.getAccountEmail();
        if(email == null){
            for (Map.Entry<String, String> entry : person.getEmails().entrySet()) {
                if (entry.getValue().equals("account") || entry.getValue().equals("ACCOUNT")) {
                    email=  entry.getKey();
                    logger.info("email : " +email);
                }
            }
        }


        MemberForm memberForm = new MemberForm();

        memberForm.setEmail(email);
        if(person.getDisplayName() != null && !"".equals(person.getDisplayName().replaceAll(" ", ""))) {
            memberForm.setName(person.getDisplayName().replaceAll(" ", ""));
        }else{
            //display이름이 없는 경우 이메일 주소  id로 설정!! (결제시 사용자 이름 필수) 2019-06-21
            memberForm.setName(email.substring(0,email.indexOf("@")));
        }

        MemberSecurityVo user = (MemberSecurityVo) memberDetailsService.loadUserByUsername(memberForm);
        logger.info("user.getAuthorities()  : {}", user.getAuthorities());
        logger.info("user.getPrivacyAgree()  : {}", user.getPrivacyAgree());

        //특정 페이지 이동처리 (쿠키)
        String redirectUri = "";
        String loginMenuUri = "";
        String agree = "";
        String paramLang = "";
        Cookie[] getCookie = request.getCookies();

        if(getCookie != null){

            for(int i=0; i<getCookie.length; i++){

                Cookie c = getCookie[i];

                String name = c.getName(); // 쿠키 이름 가져오기
                String value = c.getValue(); // 쿠키 값 가져오기

                logger.info("name : {}", name);
                logger.info("value : {}", value);

                if("loginmenu".equals(name) && !"".equals(value) ) {
                    loginMenuUri = "redirect:"+c.getValue();
                }

                if("agree".equals(name) && !"".equals(value) ) {
                    agree = c.getValue();
                }

                if("lang".equals(name) && !"".equals(value) ) {
                    paramLang = c.getValue();
                }
            }
        }

        String lang = request.getLocale().toString();
        if(paramLang != null && !"".equals(paramLang)) {
            lang = paramLang;
        }

        HttpSession session = null;
        MemberVo memberVo = new MemberVo();

        try{
            memberVo = memberService.getLogInMemberDetail(email);
            logger.info("memberVo  : {}", gson.toJson(memberVo));
            //세션 값 세팅
            session = request.getSession();
            session.setAttribute("accessUser", memberVo);
        }catch (Exception e){
            e.printStackTrace();
        }

        //시큐리티를 통과 시켜주자
        SecurityContextHolder.getContext().setAuthentication(new UsernamePasswordAuthenticationToken(email, "", user.getAuthorities()));



        redirectUri = "redirect:/kiosk/krKioskMain";
        return redirectUri;
    }
}
