jQuery.event.add(window,"load",function(){
	$(document).ready(function (){		
		
		//mrc
		// Layer (네이버 기사 검색)
		$('#btn_lyr_naver').on('click',function(){
			$('.lyr_wrap').fadeIn(300);
			$('#lyr_naver').fadeIn(300);
		});
		
		// Layer (예문선택)
		$('#btn_lyr_sample').on('click',function(){
			$('.lyr_wrap').fadeIn(300);
			$('#lyr_sample').fadeIn(300);
		});
		
		// Layer (질문선택)
		$('#btn_lyr_question').on('click',function(){
			$('.lyr_wrap').fadeIn(300);
			$('#lyr_question').fadeIn(300);
		});
		
		// Layer (리스트 선택)
		$('.lyr_wrap .lyr_lst li a').on('click',function(){
			$('.lyr_wrap .lyr_lst li a').removeClass('active');
			$(this).addClass('active');
		});
		
		// Layer (close)
		$('.lyr_wrap .btrnBox .btnType01').on('click',function(){
			$('.lyr_wrap').fadeOut(300);
			$(this).parent().parent().parent().fadeOut(300);
		});
		
		// Layer (close)
		$('.lyr_bg').on('click',function(){
			$('.lyr_wrap').fadeOut(300);
			$('.lyr_box').fadeOut(300);
		});
		
		
		// product layer popup     
		$('.close, .lyr_bg').on('click', function () {
			$('.demobox_mrc .lyr_wrap').fadeOut(300);			
			$('body').css({
				'overflow': '',
			});
		});	
		
		
		// step01 > step02
		$('.demobox_mrc .mrc_box .step02').show();
//		$('.mrc_box .step01 textarea').on('input keyup paste', function() {
//			var txtValLth = $(this).val().length;
//			if ( txtValLth > 0) {
//				$('.demobox_mrc .mrc_box .step02').show();
//				$('.progress li:nth-child(2)').addClass('active');
//
//			} else {
//				$('.demobox_mrc .mrc_box .step02').hide();
//				$('.demobox_mrc .mrc_box .step03').hide();
//				$('.demobox_mrc .mrc_box .step02 .btnBox').show();
//				$('.progress li:nth-child(2)').removeClass('active');
//			}
//		});
		
		// step02 button active
		$('.mrc_box .step02 textarea').on('input keyup paste', function() {			
			var txtValLth = $(this).val().length;
			
			if ( txtValLth > 0) {
				$('.mrc_box .step02 .btn_area button').removeClass('disable');	
				$('.mrc_box .step02 .btn_area button').removeAttr('disabled');	
				$('.mrc_box .step02 .btn_area .disBox').remove();
			} else {
//				$('.mrc_box .step02 .btn_area button').addClass('disable');	
				$('.mrc_box .step02 .btn_area button').attr('disabled');
				$('.mrc_box .step03').hide();
				$('.mrc_box .step02 .btn_area').append('<span class="disBox"></span>');
			}
		});
		
		// step02 > step03
/*		$('.mrc_box .step02 .btn_search').on('click',function(){	
//			$('.mrc_box .step01').hide();
//			$('.mrc_box .step02').hide();
			$('.mrc_box .step03').fadeIn();
            $('.tooltiptext').hide();
		});*/
		
		// step03 > step01
		$('.mrc_box .step03 .btn_reset').on('click',function(){
			$('.mrc_box .text_area textarea').val('');
			$('.mrc_box .step01').show();
			$('.mrc_box .step02').show();
			$('.mrc_box .step03').hide();
			$('.mrc_box .step02 .btn_area').show();
//			$('.mrc_box .step02 .btn_area button').addClass('disable');
			$('.mrc_box .step02 .btn_area button').attr('disabled');
//			$('.mrc_box .step02 .btn_area').append('<span class="disBox"></span>');

		});
        $('.mrc_box .step03 .btn_another').on('click',function(){
			$('.mrc_box .text_area textarea.example_area').val('');
			$('.mrc_box .step01').show();
			$('.mrc_box .step02').show();
			$('.mrc_box .step03').hide();
			$('.mrc_box .step02 .btn_area').show();
//			$('.mrc_box .step02 .btn_area button').addClass('disable');	
			$('.mrc_box .step02 .btn_area button').attr('disabled');
			$('.mrc_box .step02 .btn_area').append('<span class="disBox"></span>');

		});
		//mrc
	});
});

//API 탭  	
function openTap(evt, menu) {
  var i, demobox, tablinks;
  demobox = document.getElementsByClassName("demobox");
  for (i = 0; i < demobox.length; i++) {
    demobox[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(menu).style.display = "block";
  evt.currentTarget.className += " active";
}	
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();	

//mrc Demo tab
function clickTap(evt, tapName) {
 var i, tabcontent, tablinks;
 tabcontent = document.getElementsByClassName("tabcontent");
 for (i = 0; i < tabcontent.length; i++) {
   tabcontent[i].style.display = "none";
 }
 tablinks = document.getElementsByClassName("tablink");
 for (i = 0; i < tablinks.length; i++) {
   tablinks[i].className = tablinks[i].className.replace(" active", "");
 }
 document.getElementById(tapName).style.display = "block";
 evt.currentTarget.className += " active";
}
document.getElementById("defaultOpen2").click();
