import * as THREE from './three.module.js';
import {TrackballControls} from './jsm/controls/TrackballControls.js';
import {PCDLoader} from './jsm/loaders/PCDLoader.js';

let container;
let camera, controls, scene, renderer;

export function init(pcdUrl) {

    scene = new THREE.Scene();
    scene.background = new THREE.Color(0xffffff);

    camera = new THREE.PerspectiveCamera( 18, window.innerWidth / window.innerHeight, 0.01, 40 );
    camera.position.x = 0.1;
    camera.position.y = 0.8;
    camera.position.z = 0.6;
    camera.up.set( 0, 0, 1 );

    scene.add(camera);

    renderer = new THREE.WebGLRenderer({antialias: true});
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(600, 360);
    document.body.appendChild(renderer.domElement);

    const loader = new PCDLoader();
    loader.load(pcdUrl, function (points) {

        scene.add(points);
        const center = points.geometry.boundingSphere.center;
        controls.target.set(center.x, center.y, center.z);
        controls.update();

    });

    container = document.getElementById('pcdBox');
    container.appendChild(renderer.domElement);

    controls = new TrackballControls(camera, renderer.domElement);

    controls.rotateSpeed = 2.0;
    controls.zoomSpeed = 0.8;
    controls.panSpeed = 0.2;

    controls.staticMoving = true;

    controls.minDistance = 30;
    controls.maxDistance = 0.4 * 100;

}

export function animate() {

    requestAnimationFrame(animate);
    controls.update();
    renderer.render(scene, camera);

}