var fileTarget = $('.filebox .upload-hidden');
fileTarget.on('change', function(){
	if(window.FileReader){
		var filename = $(this)[0].files[0].name;    // 파일명 추출
	} else {
		var filename = $(this).val().split('/').pop().split('\\').pop();    // Old IE 파일명 추출
	};
	$(this).siblings('.upload-name').val(filename);
});

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
	if (this.readyState == 4 && this.status == 200) {
		console.log("OK");
	}
}
xhttp.open("GET", "https://dev-common.maum.ai/", true);     //
xhttp.setRequestHeader("Content-type", "application/json");
xhttp.send();
console.log("OK");
//20211202


function toByteArray(rawData) {
	var bytes = "";
	for (let i = 0; i < rawData.length; i++) {
		bytes += String.fromCharCode(rawData.charCodeAt(i) & 0xff);
	}
	return bytes;
}
function toBase64(rawData) {
	return btoa(toByteArray(rawData));
}
function toUint8Array(rawData) {
	var bytes = new Uint8Array(rawData.length);
	for (let i = 0; i < rawData.length; i++) {
		bytes[i] = rawData.charCodeAt(i);
	}
	return bytes.buffer;
}
function toText(rawData) {
	return new TextDecoder("utf8").decode(toUint8Array(rawData));
}

function getLoading() {
	var loading = document.getElementById('loading');
	loading.style.display = "inline-block"
}

function closeLoading() {
	var loading = document.getElementById('loading');
	loading.style.display = "none"
}

var aiURL, lifecycleName, apiId, apiKey, width, height, action, speaker, speaker_id, transparent, resolution, model, catalogInstanceName;


$("#enter").click(function () {

	var form = $('#sgFrm')[0];

	var inputText = $("#inputText").val();
	var file = $("#inputFile")[0].files[0];

	var data = new FormData(form);

	if(inputText.length > 0){
		console.log(inputText);

			getLoading()
			console.log(file);
			aiURL = "http://serengeti.maum.ai/api.app/app/v2/handle/catalog/instance/lifecycle/executes";
			lifecycleName = "Lipsync-only-Lifecycle";
			apiId = "ryu";
			apiKey = "d0cad9547b9c4a65a5cdfe50072b1588";
			width = "640";
			height = "360";
			action = "all_around";
			speaker = "0";
			speaker_id = "smyoo";
			transparent = "false";
			resolution = "HD";
			model = "smyoo";
			catalogInstanceName = "Lipsync-only-Catalog";


		data.append("lifecycleName", lifecycleName);
		data.append("target", "SoftwareCatalogInstance");
		data.append("async", false);
		data.append("payload", JSON.stringify({
			'apiId': apiId,
			'apiKey': apiKey,
			'width' : width,
			'height' : height,
			'action' : action,
			'speaker' : speaker,
			'speaker_id' : speaker_id,
			'transparent' : transparent,
			'resolution' : resolution,
			'text': inputText,
			'model' : model,
		}));
		data.append("catalogInstanceName", catalogInstanceName);
		data.append("file", file, file.name);
	} else {
		alert("Text를 입력해주세요!")
	}




	$.ajax({
		url: "http://10.122.64.84/api.app/app/v2/handle/catalog/instance/lifecycle/executes",
		headers: {
			"AccessKey": "SerengetiAdministrationAccessKey",
			"SecretKey": "SerengetiAdministrationSecretKey",
			"accept": "application/json",
			"LoginId": "maum-orchestra-com"
		},
		contentType: false,
		processData: false,
		data: data,
		mimeType: "multipart/form-data",
		method: "POST",
		beforeSend: function (xhr, opts) {
			console.log("beforesend");
			xhr.overrideMimeType('text/plain; charset=x-user-defined');
		},
		success: function (response, textStatus, jqXHR) {
			closeLoading()
			console.log("success");


			let data = toBase64(jqXHR.responseText);
			$("#video").attr("src", "data:video/mp4;base64," + data);
			console.log(data);


		},
		error: function (error) {
			console.log(error);
		}
	})
})