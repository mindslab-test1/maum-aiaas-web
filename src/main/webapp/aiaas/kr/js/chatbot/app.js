
var deviceId = '';

function chatSingIn() {

	let formData = new FormData();
	formData.append($("#key").val(), $("#value").val());

	let request = new XMLHttpRequest();
	request.onreadystatechange = function(){
		if (request.readyState === 4 && request.status !== 0){
			let response = JSON.parse(request.response);
			// console.log(response);

			if(response === null || response === ""){
				console.log("%cWeatherBot chatSignIn fail! ",'color:red' , response);
				return;
			}

			document.getElementById('AUTH_ID').value = response.directive.payload.authSuccess.authToken;
			console.log('login success!')
		}
	};

	request.open('POST', '/api/chat/chatSignIn');
	request.send(formData);
	request.timeout = 20000;
	request.ontimeout = function(err) {
		console.log("chatbot Login timeout! ", err);
	};

}

function chatOpen(chatbot) {
	deviceId = 'MAUM_'+ this.randomString();
	$.ajax({
		url: 'https://aicc-prd1.maum.ai:9980/api/v3/dialog/open',
		async: false,
		type: 'POST',
		headers: {
			"Content-Type": "application/json",
			"m2u-auth-internal": "m2u-auth-internal"
		},
		data: JSON.stringify({
			"payload": {
				"utter": "UTTER1",
				"chatbot": chatbot,
				"skill": "SKILL1"
			},
			"device": {
				"id": deviceId ,
				"type": "WEB",
				"version": "0.1",
				"channel": "ADMINWEB"
			},
			"location": {"latitude": 10.3, "longitude": 20.5, "location": "mindslab"},
			"authToken": document.getElementById('AUTH_ID').value
		}),
		dataType: 'json',
		success: function(data) {
//			console.log(data);

			console.log('open success!')
			// openDialogService(chatbot);
		}, error: function(err) {
			console.log("chatbot Login error! ", err);
		}
	});
}

function  randomString() {
	const chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz';
	const string_length = 15;
	let randomstring = '';
	for (let i = 0; i < string_length; i++) {
		let rnum = Math.floor(Math.random() * chars.length);
		randomstring += chars.substring(rnum, rnum + 1);
	}
	return randomstring;
}


function sendTalk(talkMessage) {

    talkMessage = talkMessage.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, " ");

	$.ajax({
		url: 'https://aicc-prd1.maum.ai:9980/api/v3/dialog/textToTextTalk',
		async: true,
		type: 'POST',
		headers: {
			"Content-Type": "application/json",
			"m2u-auth-internal": "m2u-auth-internal"
			//"Authorization": document.getElementById('AUTH_ID').value
		},
		data: JSON.stringify({
              "payload": {"utter": talkMessage, "lang": "ko_KR"},
              "device": {
                "id": deviceId,
                "type": "WEB",
                "version": "0.1",
                "channel": "ADMINWEB"
              },
              "location": {"latitude": 10.3, "longitude": 20.5, "location": "mindslab"},
              "authToken": document.getElementById('AUTH_ID').value
         }),
		dataType: 'json',
		success: function(data) {
//			 console.log(data);

			// textArea disabled 해제 & 로딩 UI 제거
			endTalkLoadingUI();

			if(data.directive === undefined){
				console.log("chatbot SendTalk error! ", data);
				alert("응답을 받아오지 못했습니다. 다시 시도해 주세요.\n(Failed to get response from server. Please try again)");
				return;
			}

            let result = data.directive.payload.response.speech.utter;
            console.log(result);

            appendAnswerTalk(result);

            // 이미지
            if (result.image) {
                $('.chatbot_box .chat_mid .talkLst .bot:last-child .cont').append(result.image);
            }
            // 썸네일
            if (result.thumbnail) {
                $('.chatbot_box .chat_mid .talkLst .bot:last-child .cont').append(result.thumbnail);
            }
            // 세로버튼
            if (result.v_btn) {
                $('.chatbot_box .chat_mid .talkLst .bot:last-child .cont').append(result.v_btn);
            }
            // 가로버튼
            if (result.h_btn) {
                $('.chatbot_box .chat_mid .talkLst .bot:last-child .cont').append(result.h_btn);
            }

			appendTalkTime();

            // 내용있을 시 스크롤 최하단
            $('#'+chatName+' .chat_mid').scrollTop($('#'+chatName+' .chat_mid')[0].scrollHeight);

		}, error: function(err) {
			endTalkLoadingUI();
			console.log("chatbot SendTalk error! ", err);
			alert("응답을 받아오지 못했습니다. 다시 시도해 주세요.\n(Failed to get response from server. Please try again)");
		}
	});
}



/*function closeDialogService() {
console.log("close");
console.log("close deviceId: "+deviceId);

	$.ajax({
		url: 'https://aicc-prd1.maum.ai:9980/api/v3/dialog/close',
		async: true,
		type: 'POST',
		headers: {
			"Content-Type": "application/json",
			"m2u-auth-internal": "m2u-auth-internal"
		},
		data: JSON.stringify({
            "device": {
                "id": deviceId,
                "type": "WEB",
                "version": "0.1",
                "channel": "ADMINWEB"
            },
            "location": {"latitude": 10.3, "longitude": 20.5, "location": "mindslab"},
            "authToken": document.getElementById('AUTH_ID').value
		}),
		dataType: 'json',
		success: function(data) {
			console.log(data);
		}, error: function(err) {
			console.log("chatbot Close Dialog error! ", err);
		}
	});

}*/
