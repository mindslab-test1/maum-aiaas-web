var fileTarget = $('.filebox .upload-hidden');
fileTarget.on('change', function(){
	if(window.FileReader){
		var filename = $(this)[0].files[0].name;    // 파일명 추출
	} else {
		var filename = $(this).val().split('/').pop().split('\\').pop();    // Old IE 파일명 추출
	};
	$(this).siblings('.upload-name').val(filename);
});

var xhttp = new XMLHttpRequest();
xhttp.onreadystatechange = function() {
	if (this.readyState == 4 && this.status == 200) {
		console.log("OK");
	}
}
xhttp.open("GET", "https://maum.ai/", true);     //
xhttp.setRequestHeader("Content-type", "application/json");
xhttp.send();
console.log("OK");
//20211202


function toByteArray(rawData) {
	var bytes = "";
	for (let i = 0; i < rawData.length; i++) {
		bytes += String.fromCharCode(rawData.charCodeAt(i) & 0xff);
	}
	return bytes;
}
function toBase64(rawData) {
	return btoa(toByteArray(rawData));
}
function toUint8Array(rawData) {
	var bytes = new Uint8Array(rawData.length);
	for (let i = 0; i < rawData.length; i++) {
		bytes[i] = rawData.charCodeAt(i);
	}
	return bytes.buffer;
}
function toText(rawData) {
	return new TextDecoder("utf8").decode(toUint8Array(rawData));
}

function getLoading() {
	var loading = document.getElementById('loading');
	loading.style.display = "inline-block"
}

function closeLoading() {
	var loading = document.getElementById('loading');
	loading.style.display = "none"
}

var aiURL, lifecycleName, apiId, apiKey, smodel, catalogInstanceName;

$("#enter").click(function () {
	var form = $('#sgFrm')[0];

	var language = $("#language_select option:selected").val();
	var file = $("#inputFile")[0].files[0];

	// var voice = $("#voice_select option:selected").val();
	// var inputText = $("#inputText").val();

	$("#audio, .img_area img").attr("src", "");
	$(".box").html('');

	var data = new FormData(form);

	if (language == "korean") {
		getLoading()
		console.log(file);
		aiURL = "http://serengeti2.maum.ai/api.app/app/v2/handle/catalog/instance/lifecycle/executes";
		lifecycleName = "STT-only-Lifecycle";
		apiId = "hoho105e032bc05d138";
		apiKey = "563e1097eeb64c5897990c43d391203d";
		smodel = "baseline_kor_8k_default";
		catalogInstanceName = "STT-only-Catalog";

	} else if (language == "english") {
		getLoading()
		console.log(file);
		aiURL = "http://serengeti2.maum.ai/api.app/app/v2/handle/catalog/instance/lifecycle/executes";
		lifecycleName = "STT-only-Lifecycle";
		apiId = "hoho105e032bc05d138";
		apiKey = "563e1097eeb64c5897990c43d391203d";
		smodel = "baseline_eng_8k_default";
		catalogInstanceName = "STT-only-Catalog";

	} else if (language =="") {
		alert("해당 언어를 선택해주세요!")
	}

	data.append("lifecycleName", lifecycleName);
	data.append("target", "SoftwareCatalogInstance");
	data.append("async", false);
	data.append("payload", JSON.stringify({
		'apiId': apiId,
		'apiKey': apiKey,
		'smodel': smodel,
	}));
	data.append("catalogInstanceName", catalogInstanceName);
	data.append("file", file, file.name);


	$.ajax({
		url: "http://serengeti2.maum.ai/api.app/app/v2/handle/catalog/instance/lifecycle/executes",
		headers: {
			"AccessKey": "SerengetiAdministrationAccessKey",
			"SecretKey": "SerengetiAdministrationSecretKey",
			"accept": "application/json",
			"LoginId": "maum-orchestra-com"
		},
		contentType: false,
		processData: false,
		data: data,
		mimeType: "multipart/form-data",
		method: "POST",
		beforeSend: function (xhr, opts) {
			console.log("beforesend");
			xhr.overrideMimeType('text/plain; charset=x-user-defined');
		},
		success: function (response, textStatus, jqXHR) {
			closeLoading()
			console.log("success");

			let data = jqXHR.responseText;
			if (data.includes("WAV")) {
				let data = toBase64(jqXHR.responseText);
				// console.log(data);
				$("#audio").attr("src", "data:audio/wav;base64," + data);
			} else if (data.includes("mp4")) {
				let data = toBase64(jqXHR.responseText);
				// console.log(data);
				$("#video").attr("src", "data:video/mp4;base64," + data);
			} else {
				// let data = jqXHR.responseText;
				let data = toText(jqXHR.responseText);
				console.log(response);
				$('.box').html(data);
			}

			// let data = toBase64(jqXHR.responseText);
			// console.log(btoa(data));
			// console.log(response);

			// $("#audioSource").attr("src", "data:audio/wav;base64," + data).detach().appendTo("#audio");
			/* if( aiHmID == "AI-elderlycare" ){
                $(".text").html(response);
            } else {
                jqXHR.overrideMimeType('application/x-www-form-urlencoded;charset=euc-kr');
                $("#audio").attr("src", "data:audio/wav;base64," + data);
                var decodedString = atob(data);
                console.log(decodedString);
            } */
		},
		error: function (error) {
			console.log(error);
		}
	})
})