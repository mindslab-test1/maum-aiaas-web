<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>
<%
String lang = request.getLocale().toString();
String paramLang = (String)request.getAttribute("lang");
//System.out.println("footer paramLang : " + paramLang);

if(paramLang != null && !"".equals(paramLang)) {
	lang = paramLang;
}	
%>
<!DOCTYPE html>
<html>
<head>
<%
if("ko_KR".equals(lang) || "ko".equals(lang)){
%>
<script>
	window.location.href = "/main/krMainHome"
</script>
<%
}else{
%>
<script>
	window.location.href = "/main/enMainHome"
</script>
<%
}
%>
	<title>Home</title>
</head>
<body>
<h1>
	Hello world!  
</h1>

<P>  The time on the server is ${serverTime}. </P>
</body>
</html>
