<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String lang = request.getLocale().toString();
String paramLang = (String)request.getAttribute("lang");
//System.out.println("nolayout paramLang : " + paramLang);

if(paramLang != null && !"".equals(paramLang)) {
	lang = paramLang;
}	
%>
<%
if("ko_KR".equals(lang) || "ko".equals(lang)){
%>
	<%@include file="noLayout_ko.jsp"%>
<%
}else{
%>
	<%@include file="noLayout_en.jsp"%>
<%
}
%>
