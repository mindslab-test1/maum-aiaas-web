<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<%--  CSS/JS 파일 캐싱 방지 --%>
<%
	Date lastModifiedStyle = new Date();
	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>



<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/menu.css?ver=<%=fmt.format(lastModifiedStyle)%>" />

		<!-- .snb 한글 -->
		<div class="snb">
			<!--API Services 리스트-->
<%--			<p class="sub_tit">API Services</p>--%>
			<ul class="api_list">
				<!--음성-->
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) eq '음성'}">
				<li class="collapsible list1 dropdwn active">
</c:if>
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) ne '음성'}">
				<li class="collapsible list1 dropdwn">
</c:if>
					<span>음성</span>
					<div class="ico_menu">
						<span title="음성생성(TTS)"><a href="/cloudApi/tts/krTtsMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_1_fold.svg" alt="음성생성(TTS)"></a></span>
						<span title="음성인식(STT)"><a href="/cloudApi/stt/krSttMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_2_fold.svg" alt="음성인식(STT)"></a></span>
						<span title="음성 정제"><a href="/cloudApi/cnnoise/krCnnoiseMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_3_fold.svg" alt="음성 정제"></a></span>
<%--						<span title="화자 분리"><a href="/diarize/krDiarizeMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_4_fold.svg" alt="화자 분리"></a></span>--%>
						<span title="Voice Filter"><a href="/cloudApi/voicefilter/krVoicefilterMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_5_fold.svg" alt="Voice Filter"></a></span>
<%--						<span title="Voice Recognition"><a href="/cloudApi/vr/krVoiceRecogMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_6_fold_.svg" alt="Voice Recognition" style="width:20px;"></a></span>--%>
					</div>
				</li>
				<li class="sublist list1_sub">
<!-- [API Services/음성 생성] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'ttsMain'}">
	<a class="twodepth active" href="/cloudApi/tts/krTtsMain">음성생성(TTS)</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'ttsMain'}">
	<a class="twodepth" href="/cloudApi/tts/krTtsMain">음성생성(TTS)</a>
</c:if>

<!-- [API Services/음성 인식] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'sttMain'}">
	<a class="twodepth active" href="/cloudApi/stt/krSttMain">음성인식(STT)</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'sttMain'}">
	<a class="twodepth" href="/cloudApi/stt/krSttMain">음성인식(STT)</a>
</c:if>

<!-- [API Services/음성 정제] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'cnnoiseMain'}">
	<a class="twodepth active" href="/cloudApi/cnnoise/krCnnoiseMain">음성 정제</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'cnnoiseMain'}">
	<a class="twodepth" href="/cloudApi/cnnoise/krCnnoiseMain">음성 정제</a>
</c:if>

<!-- [API Services/화자 분리] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'diarizeMain'}">--%>
<%--					<a class="twodepth active" href="/diarize/krDiarizeMain">화자 분리</a>--%>
<%--</c:if>--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'diarizeMain'}">--%>
<%--					<a class="twodepth" href="/diarize/krDiarizeMain">화자 분리</a>--%>
<%--</c:if>--%>

<!-- [API Services/음성 분리] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'voicefilterMain'}">
	<a class="twodepth active" href="/cloudApi/voicefilter/krVoicefilterMain">Voice Filter</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'voicefilterMain'}">
	<a class="twodepth" href="/cloudApi/voicefilter/krVoicefilterMain">Voice Filter</a>
</c:if>

<!-- [API Services/음성 분리] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'voiceRecogMain'}">
	<a class="twodepth active" href="/cloudApi/vr/krVoiceRecogMain">Voice Recognition</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'voiceRecogMain'}">
	<a class="twodepth" href="/cloudApi/vr/krVoiceRecogMain">Voice Recognition</a>
</c:if>--%>


				</li>
				<!--시각-->
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) eq '시각'}">
				<li class="collapsible list2 dropdwn active">
</c:if>
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) ne '시각'}">
				<li class="collapsible list2 dropdwn">
</c:if>
					<span>시각</span>
					<div class="ico_menu">
						<span title="Lip Sync Avatar"><a href="/cloudApi/lipSyncAvatar/lipSyncAvatarMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_lipSyncAvatar.svg" alt="Lip Sync Avatar" style="width:17px;"></a></span>
						<span title="Face-to-Face Avatar"><a href="/cloudApi/avatar/avatarMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_avatar.svg" alt="Face-to-Face Avatar" style="width:17px;"></a></span>
<%--						<span title="Face Recognition "><a href="/cloudApi/fr/krFaceRecogMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_6_fold_.svg" alt="Face Recognition " style="width:19px;"></a></span>--%>
						<span title="Enhanced Super Resolution"><a href="/cloudApi/superResolution/krSuperResolutionMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_5_fold.svg" alt="Enhanced Super Resolution " style="width:23px;"></a></span>
<%--						<span title="Video Super Resolution"><a href="/cloudApi/vsr/vsrMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_vsr.svg" alt="Video Super Resolution" style="width:19px;"></a></span>--%>
<%--						<span title="3D 인테리어"><a href="/cloudApi/holistic3d/krHolistic3dMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_hlst3D.svg" alt="3D 인테리어" style="width:24px;"></a></span>--%>
						<span title="AI 스타일링"><a href="/cloudApi/tti/krTtiMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_2_fold.svg" alt="AI 스타일링"></a></span>
<%--						<span title="텍스트 제거"><a href="/cloudApi/textRemoval/krTextRemovalMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_1_fold.svg" alt="텍스트 제거"></a></span>--%>
						<span title="이미지 자막 인식"><a href="/cloudApi/faceTracking/krFaceTrackingMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_10_fold_.svg" alt="이미지 자막 인식" style="width:19px;"></a></span>
<%--						<span title="문서 이미지 분석"><a href="/idr/krIdrMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_3_fold.svg" alt="문서 이미지 분석" style="width:15px;"></a></span>--%>
						<span title="인물 포즈 인식"><a href="/cloudApi/poseRecog/krPoseRecogMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_7_fold_.svg" alt="인물 포즈 인식" style="width:19px;"></a></span>
						<span title="얼굴 추적"><a href="/cloudApi/faceTracking/krFaceTrackingMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_8_fold.svg" alt="얼굴 추적" style="width:19px;"></a></span>
						<span title="얼굴 마스킹 "><a href="/cloudApi/avr/krFaceDetectionMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_face.svg" alt="얼굴 마스킹 " style="width:19px;"></a></span>
<%--						<span title="얼굴 애니메이션 필터 "><a href="/cloudApi/animeFace/krAnimeFaceMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_animeFace.svg" alt="얼굴 애니메이션 필터" style="width:19px;"></a></span>--%>
						<span title="헤어 컬러 인식"><a href="/cloudApi/hairSegmentation/hairSegmentationMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_hair.svg" alt="헤어 컬러 인식" style="width:18px;"></a></span>
						<span title="의상 특징 인식"><a href="/cloudApi/clothingDetection/clothingDetectionMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_clothes.svg" alt="의상 특징 인식" style="width:20px;"></a></span>
						<span title="스타일 코멘트"><a href="/cloudApi/styleComment/krstyleCommentMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_styleComment.svg" alt="스타일 코멘트" style="width:21px;"></a></span>
						<span title="고령자 소지품 검출"><a href="/cloudApi/airDetector/krairDetectorMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_airDetector.svg" alt="고령자 소지품 검출" style="width:21px;"></a></span>
						<span title="차량 번호판 의식"><a href="/cloudApi/avr/krPlateRecogMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_4_fold.svg" alt="차량 번호판 의식 " style="width:26px;"></a></span>
						<span title="차량 유리창 마스킹"><a href="/cloudApi/avr/krAvrMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_windshield.svg" alt="차량 유리창 마스킹 " style="width:23px;"></a></span>
						<span title="역주행 감지"><a href="/cloudApi/anomalyReverse/krAnomalyReverseMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_9_fold_.svg" alt="역주행 감지" style="width: 16px;margin: 0 0 0 7px;"></a></span>
						<span title="배회 감지"><a href="/cloudApi/anomalyLoitering/krAnomalyLoiteringMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_loiteringDetect.svg" alt="배회 감지" style="width:18px;"></a></span>
						<span title="실신 감지"><a href="/cloudApi/anomalyFalldown/krAnomalyFalldownMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_fallDownDetect.svg" alt="실신 감지" style="width:18px;"></a></span>
						<span title="침입 감지"><a href="/cloudApi/anomalyIntrusion/krAnomalyIntrusionMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_IntrusionDetect.svg" alt="침입 감지" style="width:18px;"></a></span>
<%--						<span title="치아 교정기 포지셔닝"><a href="/cloudApi/positioning/positioningMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_positioning.svg" alt="치아 교정기 포지셔닝" style="width:18px;"></a></span>--%>
					</div>
				</li>
				<li class="sublist list2_sub">
<!-- [API Services/Lip Sync Avatar ] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'lipSyncAvatarMain'}">
	<a class="twodepth active" href="/cloudApi/lipSyncAvatar/lipSyncAvatarMain">Lip Sync Avatar</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'lipSyncAvatarMain'}">
	<a class="twodepth" href="/cloudApi/lipSyncAvatar/lipSyncAvatarMain">Lip Sync Avatar</a>
</c:if>

<!-- [API Services/Face-to-Face Avatar ] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'avatarMain'}">
	<a class="twodepth active" href="/cloudApi/avatar/avatarMain">Face-to-Face Avatar</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'avatarMain'}">
	<a class="twodepth" href="/cloudApi/avatar/avatarMain">Face-to-Face Avatar</a>
</c:if>

<!-- [API Services/Face Recognition] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'faceRecogMain'}">
	<a class="twodepth active" href="/cloudApi/fr/krFaceRecogMain">Face Recognition</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'faceRecogMain'}">
	<a class="twodepth" href="/cloudApi/fr/krFaceRecogMain">Face Recognition</a>
</c:if>--%>

<!-- [API Services/Super Resolution] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'superResolutionMain'}">
	<a class="twodepth active" href="/cloudApi/superResolution/krSuperResolutionMain">Enhanced Super Resolution</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'superResolutionMain'}">
	<a class="twodepth" href="/cloudApi/superResolution/krSuperResolutionMain">Enhanced Super Resolution</a>
</c:if>

<!-- [API Services/vsr ] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'vsrMain'}">
	<a class="twodepth active" href="/cloudApi/vsr/vsrMain">Video Super Resolution</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'vsrMain'}">
	<a class="twodepth" href="/cloudApi/vsr/vsrMain">Video Super Resolution</a>
</c:if>--%>

<!-- [API Services/3D 인테리어] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'holistic3dMain'}">
	<a class="twodepth active" href="/cloudApi/holistic3d/krHolistic3dMain">3D 인테리어</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'holistic3dMain'}">
	<a class="twodepth" href="/cloudApi/holistic3d/krHolistic3dMain">3D 인테리어</a>
</c:if>--%>

<!-- [API Services/이미지 생성] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'ttiMain'}">
	<a class="twodepth active" href="/cloudApi/tti/krTtiMain">AI 스타일링</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'ttiMain'}">
	<a class="twodepth" href="/cloudApi/tti/krTtiMain">AI 스타일링</a>
</c:if>

<!-- [API Services/텍스트 제거] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'textRemovalMain'}">
	<a class="twodepth active" href="/cloudApi/textRemoval/krTextRemovalMain">텍스트 제거</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'textRemovalMain'}">
	<a class="twodepth" href="/cloudApi/textRemoval/krTextRemovalMain">텍스트 제거</a>
</c:if>--%>

<!-- [API Services/이미지 자막 인식 ] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'subtitleRecogMain'}">
	<a class="twodepth active" href="/cloudApi/subtitleRecog/krSubtitleRecogMain">이미지 자막 인식</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'subtitleRecogMain'}">
	<a class="twodepth" href="/cloudApi/subtitleRecog/krSubtitleRecogMain">이미지 자막 인식</a>
</c:if>
<!-- [API Services/화폐 · 고지서 인식] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'idrMain'}">--%>
<%--					<a class="twodepth active" href="/idr/krIdrMain">문서 이미지 분석</a>--%>
<%--</c:if>--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'idrMain'}">--%>
<%--					<a class="twodepth" href="/idr/krIdrMain">문서 이미지 분석</a>--%>
<%--</c:if>--%>

<!-- [API Services/인물 포즈 인식] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'poseRecogMain'}">
	<a class="twodepth active" href="/cloudApi/poseRecog/krPoseRecogMain">인물 포즈 인식</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'poseRecogMain'}">
	<a class="twodepth" href="/cloudApi/poseRecog/krPoseRecogMain">인물 포즈 인식</a>
</c:if>

<!-- [API Services/얼굴 추적] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'faceTrackingMain'}">
	<a class="twodepth active" href="/cloudApi/faceTracking/krFaceTrackingMain">얼굴 추적</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'faceTrackingMain'}">
	<a class="twodepth" href="/cloudApi/faceTracking/krFaceTrackingMain">얼굴 추적</a>
</c:if>


<!-- [API Services/물체/얼굴 검출] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'faceDetactionMain'}">
	<a class="twodepth active" href="/cloudApi/avr/krFaceDetectionMain">얼굴 마스킹</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'faceDetactionMain'}">
	<a class="twodepth" href="/cloudApi/avr/krFaceDetectionMain">얼굴 마스킹</a>
</c:if>

<!-- [API Services/ 얼굴 애니메이션 필터] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'animeFaceMain'}">
	<a class="twodepth active" href="/cloudApi/animeFace/krAnimeFaceMain">얼굴 애니메이션 필터</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'animeFaceMain'}">
	<a class="twodepth" href="/cloudApi/animeFace/krAnimeFaceMain">얼굴 애니메이션 필터</a>
</c:if>--%>

<!-- [API Services/ 헤어 컬러 인식 ] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'hairSegmentationMain'}">
	<a class="twodepth active" href="/cloudApi/hairSegmentation/hairSegmentationMain">헤어 컬러 인식</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'hairSegmentationMain'}">
	<a class="twodepth" href="/cloudApi/hairSegmentation/hairSegmentationMain">헤어 컬러 인식</a>
</c:if>

<!-- [API Services/의상 특징 인식] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'clothingDetectionMain'}">
	<a class="twodepth active" href="/cloudApi/clothingDetection/clothingDetectionMain">의상 특징 인식</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'clothingDetectionMain'}">
	<a class="twodepth" href="/cloudApi/clothingDetection/clothingDetectionMain">의상 특징 인식</a>
</c:if>
					<!-- [API Services/스타일 코멘트] -->
					<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'styleCommentMain'}">
						<a class="twodepth active" href="/cloudApi/styleComment/krstyleCommentMain">스타일 코멘트</a>
					</c:if>
					<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'styleCommentMain'}">
						<a class="twodepth" href="/cloudApi/styleComment/krstyleCommentMain">스타일 코멘트</a>
					</c:if>
<!-- [API Services/고령자 소지품 검출] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'airDetectorMain'}">
	<a class="twodepth active" href="/cloudApi/airDetector/krairDetectorMain">고령자 소지품 검출</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'airDetectorMain'}">
	<a class="twodepth" href="/cloudApi/airDetector/krairDetectorMain">고령자 소지품 검출</a>
</c:if>
<!-- [API Services/물체/얼굴 검출] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'plateRecogMain'}">
	<a class="twodepth active" href="/cloudApi/avr/krPlateRecogMain">차량 번호판 인식</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'plateRecogMain'}">
	<a class="twodepth" href="/cloudApi/avr/krPlateRecogMain">차량 번호판 인식</a>
</c:if>
<!-- [API Services/물체/얼굴 검출] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'avrMain'}">
	<a class="twodepth active" href="/cloudApi/avr/krAvrMain">차량 유리창 마스킹</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'avrMain'}">
	<a class="twodepth" href="/cloudApi/avr/krAvrMain">차량 유리창 마스킹</a>
</c:if>

<!-- [API Services/역주행 감지] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'anomalyReverseMain'}">
	<a class="twodepth active" href="/cloudApi/anomalyReverse/krAnomalyReverseMain">역주행 감지</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'anomalyReverseMain'}">
	<a class="twodepth" href="/cloudApi/anomalyReverse/krAnomalyReverseMain">역주행 감지</a>
</c:if>

<!-- [API Services/배회 감지] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'anomalyLoiteringMain'}">
	<a class="twodepth active" href="/cloudApi/anomalyLoitering/krAnomalyLoiteringMain">배회 감지</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'anomalyLoiteringMain'}">
	<a class="twodepth" href="/cloudApi/anomalyLoitering/krAnomalyLoiteringMain">배회 감지</a>
</c:if>

<!-- [API Services/실신 감지] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'anomalyFalldownMain'}">
	<a class="twodepth active" href="/cloudApi/anomalyFalldown/krAnomalyFalldownMain">실신 감지</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'anomalyFalldownMain'}">
	<a class="twodepth" href="/cloudApi/anomalyFalldown/krAnomalyFalldownMain">실신 감지</a>
</c:if>

<!-- [API Services/침입 감지] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'anomalyIntrusionMain'}">
	<a class="twodepth active" href="/cloudApi/anomalyIntrusion/krAnomalyIntrusionMain">침입 감지</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'anomalyIntrusionMain'}">
	<a class="twodepth" href="/cloudApi/anomalyIntrusion/krAnomalyIntrusionMain">침입 감지</a>
</c:if>

<!-- [API Services/치아 교정기 포지셔닝 ] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'positioningMain'}">
	<a class="twodepth active" href="/cloudApi/positioning/positioningMain">치아 교정기 포지셔닝</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'positioningMain'}">
	<a class="twodepth" href="/cloudApi/positioning/positioningMain">치아 교정기 포지셔닝</a>
</c:if>--%>


				</li>

				<!--언어-->
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) eq '언어'}">
				<li class="collapsible list3 dropdwn active">
</c:if>
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) ne '언어'}">
				<li class="collapsible list3 dropdwn">
</c:if>

					<span>언어</span>
					<div class="ico_menu">
						<span title="문장 교정"><a href="/cloudApi/correction/correctionMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_correct.svg" alt="문장 교정"></a></span>
						<span title="한글 변환"><a href="/cloudApi/conversion/conversionMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_convers.svg" alt="한글 변환"></a></span>
						<span title="자연어 이해"><a href="/cloudApi/nlu/krNluMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_1_fold.svg" alt="자연어 이해"></a></span>
						<span title="AI 독해"><a href="/cloudApi/mrc/krMrcMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_2_fold.svg" alt="AI 독해"></a></span>
						<span title="텍스트 분류"><a href="/cloudApi/xdc/krXdcMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_3_fold.svg" alt="텍스트 분류"></a></span>
						<span title="문장 생성"><a href="/cloudApi/gpt/krGptMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_5_fold.svg" alt="문장 생성"></a></span>
						<span title="패턴 분류"><a href="/cloudApi/hmd/krHmdMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_6_fold.svg" alt="패턴 분류"></a></span>
						<span title="의도 분류"><a href="/cloudApi/itf/krItfMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_itf.svg" alt="의도 분류"></a></span>
						<span title="스타일 변환"><a href="/cloudApi/styleTransfer/krStyleTransferMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_txtStyTrans.svg" alt="스타일 변환"></a></span>
					</div>
				</li>					
				<li class="sublist list3_sub">
<!-- [API Services/문장 교정] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'correctionMain'}">
	<a class="twodepth active" href="/cloudApi/correction/correctionMain">문장 교정</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'correctionMain'}">
	<a class="twodepth" href="/cloudApi/correction/correctionMain">문장 교정 </a>
</c:if>

<!-- [API Services/한글 변환] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'conversionMain'}">
	<a class="twodepth active" href="/cloudApi/conversion/conversionMain">한글 변환</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'conversionMain'}">
	<a class="twodepth" href="/cloudApi/conversion/conversionMain">한글 변환</a>
</c:if>

<!-- [API Services/자연어 이해] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'nluMain'}">
					<a class="twodepth active" href="/cloudApi/nlu/krNluMain">자연어 이해</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'nluMain'}">
					<a class="twodepth" href="/cloudApi/nlu/krNluMain">자연어 이해</a>
</c:if>				
	
<!-- [API Services/AI 독해] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'mrcMain'}">
					<a class="twodepth active" href="/cloudApi/mrc/krMrcMain">AI 독해</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'mrcMain'}">
					<a class="twodepth" href="/cloudApi/mrc/krMrcMain">AI 독해</a>
</c:if>

<!-- [API Services/텍스트 분류] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'xdcMain'}">
					<a class="twodepth active" href="/cloudApi/xdc/krXdcMain">텍스트 분류</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'xdcMain'}">
					<a class="twodepth" href="/cloudApi/xdc/krXdcMain">텍스트 분류</a>
</c:if>

<!-- [API Services/AI 작문] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'gptMain'}">
					<a class="twodepth active" href="/cloudApi/gpt/krGptMain">문장 생성</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'gptMain'}">
					<a class="twodepth" href="/cloudApi/gpt/krGptMain">문장 생성</a>
</c:if>

<!-- [API Services/패턴 분류] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'hmdMain'}">
					<a class="twodepth active" href="/cloudApi/hmd/krHmdMain">패턴 분류</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'hmdMain'}">
					<a class="twodepth" href="/cloudApi/hmd/krHmdMain">패턴 분류</a>
</c:if>

<!-- [API Services/의도 분류] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'itfMain'}">
					<a class="twodepth active" href="/cloudApi/itf/krItfMain">의도 분류</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'itfMain'}">
					<a class="twodepth" href="/cloudApi/itf/krItfMain">의도 분류</a>
</c:if>

<!-- [API Services/스타일 변환] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'styleTransfer'}">
	<a class="twodepth active" href="/cloudApi/styleTransfer/krStyleTransferMain">스타일 변환</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'styleTransfer'}">
	<a class="twodepth" href="/cloudApi/styleTransfer/krStyleTransferMain">스타일 변환</a>
</c:if>

				</li>
<!--분석-->
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) eq '분석'}">
	<li class="collapsible list6 dropdwn active">
</c:if>
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) ne '분석'}">
	<li class="collapsible list6 dropdwn">
</c:if>
			<span>분석</span>
			<div class="ico_menu">
				<span title="데이터 상관분석"><a href="/cloudApi/dataAnalysis/krDataAnalysis"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_data_analysis.svg" alt="데이터 상관분석" style="width:19px;"></a></span>
			</div>
	</li>
	<li class="sublist list6_sub">
				<!-- [Data/데이터 관계분석] -->
		<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'dataAnalysis'}">
			<a class="twodepth active" href="/cloudApi/dataAnalysis/krDataAnalysis">데이터 상관분석</a>
		</c:if>
		<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'dataAnalysis'}">
			<a class="twodepth" href="/cloudApi/dataAnalysis/krDataAnalysis">데이터 상관분석</a>
		</c:if>

	</li>
				<!--대화-->
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) eq '대화'}">
				<li class="collapsible list4 dropdwn active">
</c:if>
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) ne '대화'}">
				<li class="collapsible list4 dropdwn">
</c:if>					
					<span>대화</span>
					<div class="ico_menu">
<%--						<span title="NQA 봇"><a href="/cloudApi/nqa/krNqaMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_1_fold.svg" alt="NQA 봇"></a></span>--%>
						<span title="MRC 봇"><a href="/cloudApi/chatbot/krChatbotMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_2_fold.svg" alt="MRC 봇"></a></span>
<%--						<span title="KBQA 봇"><a href="/cloudApi/chatbot/krKbqaMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_2_fold.svg" alt="KBQA 봇"></a></span>--%>
						<span title="호텔 컨시어지 챗봇"><a href="/cloudApi/hotelbot/krHotelBot"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_3_fold.svg" alt="호텔 컨시어지 챗봇" style="width: 18px;"></a></span>
					</div>
				</li>					
				<li class="sublist list4_sub">
<!-- [API Services/NQA 봇] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'nqaMain'}">
					<a class="twodepth active" href="/cloudApi/nqa/krNqaMain">NQA 봇</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'nqaMain'}">
					<a class="twodepth" href="/cloudApi/nqa/krNqaMain">NQA 봇</a>
</c:if>--%>

<!-- [API Services/MRC봇(위키봇 · 뉴스봇) · 날씨봇] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'chatbotMain'}">
					<a class="twodepth active" href="/cloudApi/chatbot/krChatbotMain">MRC 봇</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'chatbotMain'}">
					<a class="twodepth" href="/cloudApi/chatbot/krChatbotMain">MRC 봇</a>
</c:if>

<!-- [API Services/KBQA봇] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'kbqaBotMain'}">
	<a class="twodepth active" href="/cloudApi/chatbot/krKbqaMain">KBQA 봇</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'kbqaBotMain'}">
	<a class="twodepth" href="/cloudApi/chatbot/krKbqaMain">KBQA 봇</a>
</c:if>--%>

<!-- [API Services/호텔 컨시어지 챗봇] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'hotelbotMain'}">
	<a class="twodepth active" href="/cloudApi/hotelbot/krHotelBot">호텔 컨시어지 챗봇</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'hotelbotMain'}">
	<a class="twodepth" href="/cloudApi/hotelbot/krHotelBot">호텔 컨시어지 챗봇</a>
</c:if>
				
				</li>
				<!--영어 교육-->
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) eq '영어교육'}">
				<li class="collapsible list5 dropdwn active">
</c:if>
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) ne '영어교육'}">
				<li class="collapsible list5 dropdwn">
</c:if>					
					<span>영어 교육</span>
					<div class="ico_menu">
						<span title="교육용 STT"><a href="/cloudApi/engedu/krEngeduSttMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_eng_1.svg" alt="교육용 STT"></a></span>
						<span title="문장 발음 평가"><a href="/cloudApi/engedu/krEngeduPronMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_eng_2.svg" alt="문장 발음 평가"></a></span>
						<span title="파닉스 평가"><a href="/cloudApi/engedu/krEngeduPhonicsMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_eng_3.svg" alt="파닉스 평가"></a></span>
					</div>			
				</li>					
				<li class="sublist list5_sub">
<!-- [API Services/영어교육 STT] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'engeduSttMain'}">
					<a class="twodepth active" href="/cloudApi/engedu/krEngeduSttMain">교육용 STT</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'engeduSttMain'}">
					<a class="twodepth" href="/cloudApi/engedu/krEngeduSttMain">교육용 STT</a>
</c:if>

<!-- [API Services/영어교육 발음평가] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'engeduPronMain'}">
					<a class="twodepth active" href="/cloudApi/engedu/krEngeduPronMain">문장 발음 평가</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'engeduPronMain'}">
					<a class="twodepth" href="/cloudApi/engedu/krEngeduPronMain">문장 발음 평가</a>
</c:if>

<!-- [API Services/영어교육 파닉스] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'engeduPhonicsMain'}">
					<a class="twodepth active" href="/cloudApi/engedu/krEngeduPhonicsMain">파닉스 평가</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'engeduPhonicsMain'}">
					<a class="twodepth" href="/cloudApi/engedu/krEngeduPhonicsMain">파닉스 평가</a>
</c:if>				
				</li>
			</ul>
			<!--//API Services 리스트-->
			
			<!--Application Services 리스트-->
<%--			<p class="sub_tit sub_list">Application Services</p>--%>
<%--			<ul>--%>
<%--				<li class="app_manu">--%>
<%--<!-- [AI Builder] -->--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'aiBuilder'}">--%>
<%--	<a class="twodepth active" href="/aiBuilder/krAiBuilderMain">AI Builder</a>--%>
<%--</c:if>--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'aiBuilder'}">--%>
<%--	<a class="twodepth" href="/aiBuilder/krAiBuilderMain">AI Builder</a>--%>
<%--</c:if>--%>


<%--<!-- [Application/내 목소리로 AI 음성 만들기] -->--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'aiVoiceFontMain'}">--%>
<%--					<a class="twodepth active" href="/tts_train/krAiVoiceFontMain">AI 보이스 폰트 제작</a>--%>
<%--</c:if>--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'aiVoiceFontMain'}">--%>
<%--					<a class="twodepth" href="/tts_train/krAiVoiceFontMain">AI 보이스 폰트 제작</a>--%>
<%--</c:if>--%>


<%--<!-- [Application/maum 회의록] -->--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'minutesMain'}">--%>
<%--	<a class="twodepth active" href="/minutes/krMinutesMain">maum 회의록</a>--%>
<%--</c:if>--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'minutesMain'}">--%>
<%--	<a class="twodepth" href="/minutes/krMinutesMain">maum 회의록</a>--%>
<%--</c:if>--%>



<%--<!-- [Application/음성봇] -->--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'Voice 봇'}">--%>
<%--					<a class="twodepth active" href="/voicebot/krVoiceBotMain">Voice bot</a>--%>
<%--</c:if>	--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'Voice 봇'}">--%>
<%--					<a class="twodepth" href="/voicebot/krVoiceBotMain">Voice bot</a>--%>
<%--</c:if>--%>

<%--				</li>--%>
<%--			</ul>--%>
			<!--//Application Services 리스트-->

<!--마음 아카데미-->
<%--<p class="sub_tit sub_list">maum Academy</p>--%>
<%--<ul>--%>
<%--	<li class="edu_manu">--%>
<%--		<!-- [Application/내 목소리로 AI 음성 만들기] -->--%>
<%--		<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'academyMain'}">--%>
<%--			<a class="twodepth active" href="/academy/krAcademyMain">사용자 교육</a>--%>
<%--		</c:if>--%>
<%--		<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'academyMain'}">--%>
<%--			<a class="twodepth" href="/academy/krAcademyMain">사용자 교육</a>--%>
<%--		</c:if>--%>


<%--	</li>--%>
<%--</ul>--%>
<!--//마음 아카데미-->

			<!--data 리스트-->

<%--			<p class="sub_tit sub_list">Data</p>--%>
<%--			<ul>--%>
<%--				<li class="data_manu">--%>
<%--<!-- [Data/판매 데이터] -->			--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'sellDataMain'}">--%>
<%--					<a class="twodepth active" href="/data/krSellDataMain">판매 데이터</a>--%>
<%--</c:if>	--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'sellDataMain'}">--%>
<%--					<a class="twodepth" href="/data/krSellDataMain">판매 데이터</a>--%>
<%--</c:if>					--%>
<%--					--%>
<%--<!-- [Data/데이터 정제 서비스] -->			--%>
<%--&lt;%&ndash;<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'dataServiceMain'}">&ndash;%&gt;--%>
<%--&lt;%&ndash;					<a class="twodepth active" href="/data/krDataServiceMain">데이터 정제 서비스</a>&ndash;%&gt;--%>
<%--&lt;%&ndash;</c:if>	&ndash;%&gt;--%>
<%--&lt;%&ndash;<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'dataServiceMain'}">&ndash;%&gt;--%>
<%--&lt;%&ndash;					<a class="twodepth" href="/data/krDataServiceMain">데이터 정제 서비스</a>&ndash;%&gt;--%>
<%--&lt;%&ndash;</c:if>&ndash;%&gt;--%>

<%--<!-- [Data/마음 데이터] -->--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'maumDataMain'}">--%>
<%--	<a class="twodepth active" href="/data/krMaumDataMain">데이터 정제 서비스</a>--%>
<%--</c:if>--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'maumDataMain'}">--%>
<%--	<a class="twodepth" href="/data/krMaumDataMain">데이터 정제 서비스</a>--%>
<%--</c:if>--%>

<%--				</li>--%>
<%--			</ul>--%>
<%--			<!--//data 리스트-->--%>
		</div>
		<!-- //.snb 한글 -->




		
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/menu.js"></script>		