<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%--<c:if test="${empty fn:escapeXml(sessionScope.accessUser)}">--%>
<%--	<script>--%>
<%--		window.location.href = "/?lang=ko";--%>
<%--	</script>--%>
<%--</c:if>--%>


<sec:authorize access="isAnonymous()">
		<script>
			window.location.href = "/?lang=ko";
		</script>
</sec:authorize>

<form id="logout-form" action='/logout' method="POST">
   <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>

        <h1><a href="/main/krMainHome"><img src="${pageContext.request.contextPath}/aiaas/common/images/logo_cloudAPI.svg" alt="cloudAPI"></a></h1>
		<div class="sta">			
			<!-- .titArea -->
			<div class="titArea">               
                <div class="path">
                    <span><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_path_home_bk.png" alt="HOME"></span>
                    <span>${fn:escapeXml(sessionScope.menuName)}</span>
<c:if test="${fn:escapeXml(sessionScope.subMenuName) ne ''}">                    
                    <span>${fn:escapeXml(sessionScope.subMenuName)}</span>
</c:if>
                </div>
<%--				<div class="header_link">--%>
<%--					<a href="/support/krPricing" >가격정책</a>--%>
<%--				</div>--%>
            </div>
            <!-- //.titArea -->
<%--			<div class="academy_link_box">--%>
<%--				<a href="/academy/krAcademyMain" class="academy_link">마음 아카데미</a>--%>
<%--			</div>--%>
			<!-- .etcmenu -->


			<div class="etcmenu">
<%--				<div class="inner_box">--%>
<%--					<a href="/main/krEmployeesMain" class="go_employees" id="Button_Internal" hidden>직원용</a>--%>
<%--				</div>--%>
<%-- 				<span><em>P</em><em>${fn:escapeXml(sessionScope.accessUser.point)}</em> point</span> --%>
				<div class="userBox">
					<dl>
						<dt class="ico_user"><em class="far fa-user"></em></dt>
						<dd>
							<a target="_self" href="#none">${fn:escapeXml(sessionScope.accessUser.email)}<em class="fas fa-chevron-down"></em></a>
						</dd>
						<ul class="lst">
 							<li class="ico_profile"><a target="_blank" href="/user/profileMain">프로필</a></li>
 							<li class="ico_account"><a target="_blank" href="/user/apiAccountMain">API 정보</a></li>
							<li class="ico_payment"><a target="_blank" href="/user/paymentInfoMain">결제정보</a></li>
							<li class="ico_logout"><a href="#" onclick="document.getElementById('logout-form').submit();">로그아웃</a></li>
						</ul>
					</dl>
				</div>
			</div>
			<!-- //.etcmenu -->

			<div class="lang_box">
				<span>한국어</span>
				<span><a href="/main/enMainHome" target="_self">English</a></span>
			</div>
		</div>

