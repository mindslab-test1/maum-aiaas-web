		<!-- #footer -->
		<div id="footer">
			<div class="lot_c">
				<div class="cyrt">
					<p>
						<a href="https://mindslab.ai:8080/en" target="_blank"><strong>Minds Lab </strong> </a>
						601 Dasan Tower, 49 Daewangpangyo-ro 644beon-gil, Bundang-gu, Seongnam-si, Gyeonggi-do, Republic of Korea &#124;
						<span>CEO Taejoon Yoo &#124; Company Registration Number 314-86-55446</span>
						<strong class="footer_link"><%--<a href="/support/enPricing">Pricing</a>--%><a href="/support/enTerms">Terms &amp; Conditions</a><a href="/support/enContactUs">Contact us</a></strong>
					</p>

				</div>
			</div>
		</div>
		<!-- //#footer -->