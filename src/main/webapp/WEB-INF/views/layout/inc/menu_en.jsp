<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<%--  CSS/JS 파일 캐싱 방지 --%>
<%
	Date lastModifiedStyle = new Date();
	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/menu.css?ver=<%=fmt.format(lastModifiedStyle)%>" />

		<!-- .snb EN -->
		<div class="snb">
			<!--API Services 리스트-->
<%--			<p class="sub_tit">API Services</p>	--%>
			<ul class="api_list">	
				<!--Speech-->
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) eq 'Speech'}">
				<li class="collapsible list1 dropdwn active">			
</c:if>
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) ne 'Speech'}">
				<li class="collapsible list1 dropdwn">		
</c:if>
					<span>Voice</span>
					<div class="ico_menu">
						<span title="Speech Generation"><a href="/cloudApi/tts/enTtsMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_spe_1_fold.svg" alt="Speech Generation"></a></span>
						<span title="Speech Recognition"><a href="/cloudApi/stt/enSttMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_spe_2_fold.svg" alt="Speech Recognition"></a></span>
						<span title="Denoise"><a href="/cloudApi/cnnoise/enCnnoiseMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_spe_3_fold.svg" alt="Denoise"></a></span>
<%--						<span title="Diarization"><a href="/diarize/enDiarizeMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_spe_4_fold.svg" alt="Diarization"></a></span>--%>
						<span title="Voice Filter"><a href="/cloudApi/voicefilter/enVoicefilterMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_spe_5_fold.svg" alt="Voice Filter"></a></span>
<%--						<span title="Voice Recognition"><a href="/cloudApi/vr/enVoiceRecogMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_6_fold_.svg" alt="Voice Recognition" style="width:20px;"></a></span>--%>

					</div>
				</li>					
				<li class="sublist list1_sub">
<!-- [API Services/음성 생성] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'ttsMain'}">
					<a class="twodepth active" href="/cloudApi/tts/enTtsMain">Speech Generation</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'ttsMain'}">
					<a class="twodepth" href="/cloudApi/tts/enTtsMain">Speech Generation</a>
</c:if>

<!-- [API Services/음성 인식] -->				
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'sttMain'}">
					<a class="twodepth active" href="/cloudApi/stt/enSttMain">Speech Recognition</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'sttMain'}">
					<a class="twodepth" href="/cloudApi/stt/enSttMain">Speech Recognition</a>
</c:if>	

<!-- [API Services/Denoise] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'cnnoiseMain'}">
					<a class="twodepth active" href="/cloudApi/cnnoise/enCnnoiseMain">Denoise</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'cnnoiseMain'}">
					<a class="twodepth" href="/cloudApi/cnnoise/enCnnoiseMain">Denoise</a>
</c:if>

<!-- [API Services/Diarization] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'diarizeMain'}">--%>
<%--					<a class="twodepth active" href="/diarize/enDiarizeMain">Diarization</a>--%>
<%--</c:if>--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'diarizeMain'}">--%>
<%--					<a class="twodepth" href="/diarize/enDiarizeMain">Diarization</a>--%>
<%--</c:if>--%>
	
<!-- [API Services/음성 분리] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'voicefilterMain'}">
					<a class="twodepth active" href="/cloudApi/voicefilter/enVoicefilterMain">Voice Filter</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'voicefilterMain'}">
					<a class="twodepth" href="/cloudApi/voicefilter/enVoicefilterMain">Voice Filter</a>
</c:if>

<!-- [API Services/화자 인증] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'voiceRecogMain'}">
	<a class="twodepth active" href="/cloudApi/vr/enVoiceRecogMain">Voice Recognition</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'voiceRecogMain'}">
	<a class="twodepth" href="/cloudApi/vr/enVoiceRecogMain">Voice Recognition</a>
</c:if>--%>

				</li>
				<!--Vision-->
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) eq 'Vision'}">
				<li class="collapsible list2 dropdwn active">			
</c:if>
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) ne 'Vision'}">
				<li class="collapsible list2 dropdwn">		
</c:if>				
					<span>Vision</span>
					<div class="ico_menu">
						<span title="Lip Sync Avatar"><a href="/cloudApi/lipSyncAvatar/enLipSyncAvatarMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_lipSyncAvatar.svg" alt="Lip Sync Avatar" style="width:17px;"></a></span>
						<span title="Face-to-Face Avatar"><a href="/cloudApi/avatar/enAvatarMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_avatar.svg" alt="Face-to-Face Avatar" style="width:17px;"></a></span>
<%--						<span title="Face Recognition "><a href="/cloudApi/fr/enFaceRecogMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_vis_6_fold.svg" alt="Face Recognition " style="width:19px;"></a></span>--%>
						<span title="Enhanced Super Resolution"><a href="/cloudApi/superResolution/enSuperResolutionMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_vis_5_fold.svg" alt="Enhanced Super Resolution " style="width:23px;"></a></span>
<%--						<span title="Video Super Resolution"><a href="/cloudApi/vsr/enVsrMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_vsr.svg" alt="Video Super Resolution" style="width:19px;"></a></span>--%>
<%--						<span title="Holistic 3D"><a href="/cloudApi/holistic3d/enHolistic3dMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_hlst3D.svg" alt="Holistic 3D" style="width:24px;"></a></span>--%>
						<span title="AI Styling"><a href="/cloudApi/tti/enTtiMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_vis_2_fold.svg" alt="AI Styling"></a></span>
<%--						<span title="Text Removal"><a href="/cloudApi/textRemoval/enTextRemovalMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_vis_1_fold.svg" alt="Text Removal"></a></span>--%>
						<span title="Subtitles Recognition"><a href="/cloudApi/subtitleRecog/enSubtitleRecogMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_10_fold_.svg" alt="Subtitiles Recognition" style="width:19px;"></a></span>
<%--						<span title="AI Vehicle Recognition"><a href="/avr/enAvrMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_vis_4_fold.svg" alt="AI Vehicle Recognition " style="width:26px;"></a></span>--%>
<%--						<span title="Document Recognition"><a href="/idr/enIdrMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_vis_3_fold.svg" alt="Document Recognition" style="width:15px;"></a></span>--%>
						<span title="Pose Recognition"><a href="/cloudApi/poseRecog/enPoseRecogMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_7_fold_.svg" alt="Pose Recognition" style="width:19px;"></a></span>
						<span title="Face Tracking"><a href="/cloudApi/faceTracking/enFaceTrackingMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_8_fold.svg" alt="Face Tracking" style="width:19px;"></a></span>
						<span title="Face Detection "><a href="/cloudApi/avr/enFaceDetectionMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_face.svg" alt="Face Detection " style="width:19px;"></a></span>
<%--						<span title="Anime Face "><a href="/cloudApi/animeFace/enAnimeFaceMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_vis_animeFace.svg" alt="Anime Face" style="width:19px;"></a></span>--%>
						<span title="Hair Segmentation"><a href="/cloudApi/hairSegmentation/enhairSegmentationMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_hair.svg" alt="Hair Segmentation" style="width:18px;"></a></span>
						<span title="Clothing Multi-Attributes Detection"><a href="/cloudApi/clothingDetection/enclothingDetectionMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_clothes.svg" alt="의상 특징 인식" style="width:20px;"></a></span>
						<span title="License Plate Recognition"><a href="/cloudApi/avr/enPlateRecogMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_4_fold.svg" alt="License Plate Recognition " style="width:26px;"></a></span>
						<span title="Windshield Detection"><a href="/cloudApi/avr/enAvrMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_windshield.svg" alt="Windshield Detection" style="width:23px;"></a></span>

						<span title="Reverse Direction Detection"><a href="/cloudApi/anomalyReverse/enAnomalyReverseMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_9_fold_.svg" alt="Reverse Direction Detection" style="width: 16px;margin: 0 0 0 7px;"></a></span>
						<span title="Loitering Detection"><a href="/cloudApi/anomalyLoitering/enAnomalyLoiteringMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_vis_loiteringDetect.svg" alt="Loitering Detection" style="width:18px;"></a></span>
						<span title="Fall Down Detection"><a href="/cloudApi/anomalyFalldown/enAnomalyFalldownMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_vis_fallDownDetect.svg" alt="Fall Down Detection" style="width:18px;"></a></span>
						<span title="Intrusion Detection"><a href="/cloudApi/anomalyIntrusion/enAnomalyIntrusionMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_vis_IntrusionDetect.svg" alt="Intrusion Detection" style="width:18px;"></a></span>
<%--						<span title="Bracket Positioning"><a href="/cloudApi/positioning/positioningMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_positioning.svg" alt="Bracket Positioning" style="width:18px;"></a></span>--%>
					</div>
				</li>					
				<li class="sublist list2_sub">
<!-- [API Services/Lip Sync Avatar ] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'lipSyncAvatarMain'}">
	<a class="twodepth active" href="/cloudApi/lipSyncAvatar/enLipSyncAvatarMain">Lip Sync Avatar</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'lipSyncAvatarMain'}">
	<a class="twodepth" href="/cloudApi/lipSyncAvatar/enLipSyncAvatarMain">Lip Sync Avatar</a>
</c:if>

<!-- [API Services/Face-to-Face Avatar ] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'avatarMain'}">
	<a class="twodepth active" href="/cloudApi/avatar/enAvatarMain">Face-to-Face Avatar</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'avatarMain'}">
	<a class="twodepth" href="/cloudApi/avatar/enAvatarMain">Face-to-Face Avatar</a>
</c:if>

<!-- [API Services/Face Recognition] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'faceRecogMain'}">
	<a class="twodepth active" href="/cloudApi/fr/enFaceRecogMain">Face Recognition</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'faceRecogMain'}">
	<a class="twodepth" href="/cloudApi/fr/enFaceRecogMain">Face Recognition</a>
</c:if>--%>

<!-- [API Services/Super Resolution] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'superResolutionMain'}">
	<a class="twodepth active" href="/cloudApi/superResolution/enSuperResolutionMain">Enhanced Super Resolution</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'superResolutionMain'}">
	<a class="twodepth" href="/cloudApi/superResolution/enSuperResolutionMain">Enhanced Super Resolution</a>
</c:if>

<!-- [API Services/vsr ] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'vsrMain'}">
	<a class="twodepth active" href="/cloudApi/vsr/enVsrMain">Video Super Resolution</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'vsrMain'}">
	<a class="twodepth" href="/cloudApi/vsr/enVsrMain">Video Super Resolution</a>
</c:if>--%>

<!-- [API Services/Holistic 3D] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'holistic3dMain'}">
	<a class="twodepth active" href="/cloudApi/holistic3d/enHolistic3dMain">Holistic 3D</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'holistic3dMain'}">
	<a class="twodepth" href="/cloudApi/holistic3d/enHolistic3dMain">Holistic 3D</a>
</c:if>--%>

<!-- [API Services/이미지 생성] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'ttiMain'}">
	<a class="twodepth active" href="/cloudApi/tti/enTtiMain">AI Styling</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'ttiMain'}">
	<a class="twodepth" href="/cloudApi/tti/enTtiMain">AI Styling</a>
</c:if>

<!-- [API Services/TextRemoval] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'textRemovalMain'}">
	<a class="twodepth active" href="/cloudApi/textRemoval/enTextRemovalMain">Text Removal</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'textRemovalMain'}">
	<a class="twodepth" href="/cloudApi/textRemoval/enTextRemovalMain">Text Removal</a>
</c:if>--%>

<!-- [API Services/물체/얼굴 검출] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'avrMain'}">--%>
<%--	<a class="twodepth active" href="/avr/enAvrMain">AI Vehicle Recognition</a>--%>
<%--</c:if>--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'avrMain'}">--%>
<%--	<a class="twodepth" href="/avr/enAvrMain">AI Vehicle Recognition</a>--%>
<%--</c:if>--%>

<!-- [API Services/이미지 자막 인식 ] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'subtitleRecogMain'}">
	<a class="twodepth active" href="/cloudApi/subtitleRecog/enSubtitleRecogMain">Subtitles Recognition</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'subtitleRecogMain'}">
	<a class="twodepth" href="/cloudApi/subtitleRecog/enSubtitleRecogMain">Subtitles Recognition</a>
</c:if>

<!-- [API Services/Document Recognition] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'idrMain'}">--%>
<%--					<a class="twodepth active" href="/idr/enIdrMain">Document Recognition</a>--%>
<%--</c:if>--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'idrMain'}">--%>
<%--					<a class="twodepth" href="/idr/enIdrMain">Document Recognition</a>--%>
<%--</c:if>--%>

<!-- [API Services/인물 포즈 인식] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'poseRecogMain'}">
	<a class="twodepth active" href="/cloudApi/poseRecog/enPoseRecogMain">Pose Recognition</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'poseRecogMain'}">
	<a class="twodepth" href="/cloudApi/poseRecog/enPoseRecogMain">Pose Recognition</a>
</c:if>

<!-- [API Services/얼굴 추적] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'faceTrackingMain'}">
	<a class="twodepth active" href="/cloudApi/faceTracking/enFaceTrackingMain">Face Tracking</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'faceTrackingMain'}">
	<a class="twodepth" href="/cloudApi/faceTracking/enFaceTrackingMain">Face Tracking</a>
</c:if>


					<!-- [API Services/물체/얼굴 검출] -->
					<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'faceDetactionMain'}">
						<a class="twodepth active" href="/cloudApi/avr/enFaceDetectionMain">Face Detection</a>
					</c:if>
					<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'faceDetactionMain'}">
						<a class="twodepth" href="/cloudApi/avr/enFaceDetectionMain">Face Detection</a>
					</c:if>

					<!-- [API Services/ 얼굴 애니메이션 필터] -->
					<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'animeFaceMain'}">
						<a class="twodepth active" href="/cloudApi/animeFace/enAnimeFaceMain">Anime Face</a>
					</c:if>
					<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'animeFaceMain'}">
						<a class="twodepth" href="/cloudApi/animeFace/enAnimeFaceMain">Anime Face</a>
					</c:if>--%>

					<!-- [API Services/ 헤어 컬러 인식 ] -->
					<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'hairSegmentationMain'}">
						<a class="twodepth active" href="/cloudApi/hairSegmentation/enHairSegmentationMain">Hair Segmentation</a>
					</c:if>
					<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'hairSegmentationMain'}">
						<a class="twodepth" href="/cloudApi/hairSegmentation/enHairSegmentationMain">Hair Segmentation</a>
					</c:if>

					<!-- [API Services/의상 특징 인식] -->
					<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'clothingDetectionMain'}">
						<a class="twodepth active" href="/cloudApi/clothingDetection/enclothingDetectionMain">Clothing Detection</a>
					</c:if>
					<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'clothingDetectionMain'}">
						<a class="twodepth" href="/cloudApi/clothingDetection/enclothingDetectionMain">Clothing Detection</a>
					</c:if>

					<!-- [API Services/물체/얼굴 검출] -->
					<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'plateRecogMain'}">
						<a class="twodepth active" href="/cloudApi/avr/enPlateRecogMain">License Plate Recognition</a>
					</c:if>
					<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'plateRecogMain'}">
						<a class="twodepth" href="/cloudApi/avr/enPlateRecogMain">License Plate Recognition</a>
					</c:if>
					<!-- [API Services/물체/얼굴 검출] -->
					<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'avrMain'}">
						<a class="twodepth active" href="/cloudApi/avr/enAvrMain">Windshield Detection</a>
					</c:if>
					<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'avrMain'}">
						<a class="twodepth" href="/cloudApi/avr/enAvrMain">Windshield Detection</a>
					</c:if>

<!-- [API Services/역주행 감지] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'anomalyReverseMain'}">
	<a class="twodepth active" href="/cloudApi/anomalyReverse/enAnomalyReverseMain">Reverse Direction Detection</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'anomalyReverseMain'}">
	<a class="twodepth" href="/cloudApi/anomalyReverse/enAnomalyReverseMain">Reverse Direction Detection</a>
</c:if>

<!-- [API Services/배회 감지] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'anomalyLoiteringMain'}">
	<a class="twodepth active" href="/cloudApi/anomalyLoitering/enAnomalyLoiteringMain">Loitering Detection</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'anomalyLoiteringMain'}">
	<a class="twodepth" href="/cloudApi/anomalyLoitering/enAnomalyLoiteringMain">Loitering Detection</a>
</c:if>

<!-- [API Services/실신 감지] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'anomalyFalldownMain'}">
	<a class="twodepth active" href="/cloudApi/anomalyFalldown/enAnomalyFalldownMain">Fall Down Detection</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'anomalyFalldownMain'}">
	<a class="twodepth" href="/cloudApi/anomalyFalldown/enAnomalyFalldownMain">Fall Down Detection</a>
</c:if>

<!-- [API Services/침입 감지] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'anomalyIntrusionMain'}">
	<a class="twodepth active" href="/cloudApi/anomalyIntrusion/enAnomalyIntrusionMain">Intrusion Detection</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'anomalyIntrusionMain'}">
	<a class="twodepth" href="/cloudApi/anomalyIntrusion/enAnomalyIntrusionMain">Intrusion Detection</a>
</c:if>

<!-- [API Services/치아 교정기 포지셔닝 ] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'positioningMain'}">
	<a class="twodepth active" href="/cloudApi/positioning/enPositioningMain">Bracket Positioning</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'positioningMain'}">
	<a class="twodepth" href="/cloudApi/positioning/enPositioningMain">Bracket Positioning</a>
</c:if>--%>


				</li>
				<!--언어-->
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) eq 'Languages'}">
				<li class="collapsible list3 dropdwn active">			
</c:if>
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) ne 'Languages'}">
				<li class="collapsible list3 dropdwn">		
</c:if>

					<span>Languages</span>
					<div class="ico_menu">
						<span title="Bert Correction"><a href="/cloudApi/correction/enCorrectionMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_correct.svg" alt="Bert Correction"></a></span>
						<span title="Konglish"><a href="/cloudApi/conversion/enConversionMain"><img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_convers.svg" alt="Konglish"></a></span>
						<span title="NLU"><a href="/cloudApi/nlu/enNluMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_lan_1_fold.svg" alt="NLU"></a></span>
						<span title="MRC"><a href="/cloudApi/mrc/enMrcMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_lan_2_fold.svg" alt="MRC"></a></span>
						<span title="XDC"><a href="/cloudApi/xdc/enXdcMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_lan_3_fold.svg" alt="XDC"></a></span>
						<span title="GPT-2"><a href="/cloudApi/gpt/enGptMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_lan_5_fold.svg" alt="GPT-2"></a></span>
						<span title="HMD"><a href="/cloudApi/hmd/enHmdMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_lan_6_fold.svg" alt="HMD"></a></span>
						<span title="ITF"><a href="/cloudApi/itf/enItfMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_lan_itf.svg" alt="ITF"></a></span>
						<span title="Style Transfer"><a href="/cloudApi/styleTransfer/enStyleTransferMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_lan_txtStyTrans.svg" alt="Style Transfer"></a></span>
					</div>
				</li>					
				<li class="sublist list3_sub">
<!-- [API Services/문장 교정] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'correctionMain'}">
	<a class="twodepth active" href="/cloudApi/correction/enCorrectionMain">Bert Correction</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'correctionMain'}">
	<a class="twodepth" href="/cloudApi/correction/enCorrectionMain">Bert Correction </a>
</c:if>

<!-- [API Services/한글 변환] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'conversionMain'}">
	<a class="twodepth active" href="/cloudApi/conversion/enConversionMain">Konglish</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'conversionMain'}">
	<a class="twodepth" href="/cloudApi/conversion/enConversionMain">Konglish</a>
</c:if>

<!-- [API Services/NLU] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'nluMain'}">
					<a class="twodepth active" href="/cloudApi/nlu/enNluMain">NLU</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'nluMain'}">
					<a class="twodepth" href="/cloudApi/nlu/enNluMain">NLU</a>
</c:if>				
	
<!-- [API Services/MRC] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'mrcMain'}">
					<a class="twodepth active" href="/cloudApi/mrc/enMrcMain">MRC</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'mrcMain'}">
					<a class="twodepth" href="/cloudApi/mrc/enMrcMain">MRC</a>
</c:if>

<!-- [API Services/Text Classifier] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'xdcMain'}">
					<a class="twodepth active" href="/cloudApi/xdc/enXdcMain">XDC</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'xdcMain'}">
					<a class="twodepth" href="/cloudApi/xdc/enXdcMain">XDC</a>
</c:if>

<!-- [API Services/AI 작문] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'gptMain'}">
					<a class="twodepth active" href="/cloudApi/gpt/enGptMain">GPT-2</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'gptMain'}">
					<a class="twodepth" href="/cloudApi/gpt/enGptMain">GPT-2</a>
</c:if>

<!-- [API Services/Pattern Recognition] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'hmdMain'}">
					<a class="twodepth active" href="/cloudApi/hmd/enHmdMain">HMD</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'hmdMain'}">
					<a class="twodepth" href="/cloudApi/hmd/enHmdMain">HMD</a>
</c:if>

<!-- [API Services/Intent Finder] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'itfMain'}">
					<a class="twodepth active" href="/cloudApi/itf/enItfMain">ITF</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'itfMain'}">
					<a class="twodepth" href="/cloudApi/itf/enItfMain">ITF</a>
</c:if>

<!-- [API Services/스타일 변환] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'styleTransfer'}">
	<a class="twodepth active" href="/cloudApi/styleTransfer/enStyleTransferMain">Style Transfer</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'styleTransfer'}">
	<a class="twodepth" href="/cloudApi/styleTransfer/enStyleTransferMain">Style Transfer</a>
</c:if>
				</li>

				<!--분석-->
				<c:if test="${fn:escapeXml(sessionScope.menuGrpName) eq 'Analysis'}">
				<li class="collapsible list6 dropdwn active">
					</c:if>
					<c:if test="${fn:escapeXml(sessionScope.menuGrpName) ne 'Analysis'}">
				<li class="collapsible list6 dropdwn">
					</c:if>
					<span>Analysis</span>
					<div class="ico_menu">
						<span title="Correlation analysis"><a href="/cloudApi/dataAnalysis/enDataAnalysis"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_data_analysis.svg" alt="Correlation analysis" style="width:19px;"></a></span>
					</div>
				</li>
				<li class="sublist list6_sub">
					<!-- [Data/데이터 관계분석] -->
					<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'dataAnalysis'}">
						<a class="twodepth active" href="/cloudApi/dataAnalysis/enDataAnalysis">Correlation analysis</a>
					</c:if>
					<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'dataAnalysis'}">
						<a class="twodepth" href="/cloudApi/dataAnalysis/enDataAnalysis">Correlation analysis</a>
					</c:if>

				</li>

				<!--대화-->
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) eq 'Conversation'}">
				<li class="collapsible list4 dropdwn active">			
</c:if>
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) ne 'Conversation'}">
				<li class="collapsible list4 dropdwn">		
</c:if>					
					<span>Conversation</span>
					<div class="ico_menu">
<%--						<span title="NQA Bot"><a href="/cloudApi/nqa/enNqaMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_bot_1_fold.svg" alt="NQA Bot"></a></span>--%>
						<span title="MRC Bots"><a href="/cloudApi/chatbot/enChatbotMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_bot_2_fold.svg" alt="MRC Bots"></a></span>
<%--						<span title="KBQA Bot"><a href="/cloudApi/chatbot/enKbqaMain"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_bot_2_fold.svg" alt="KBQA Bot"></a></span>--%>
						<span title="Hotel Concierge Chatbot"><a href="/cloudApi/hotelbot/enHotelBot"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_bot_3_fold.svg" alt="Hotel Concierge Chatbot" style="width: 18px;"></a></span>
					</div>
				</li>					
				<li class="sublist list4_sub">

<!-- [API Services/NQA Bot] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'nqaMain'}">
					<a class="twodepth active" href="/cloudApi/nqa/enNqaMain">NQA Bot</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'nqaMain'}">
					<a class="twodepth" href="/cloudApi/nqa/enNqaMain">NQA Bot</a>
</c:if>--%>

<!-- [API Services/MRC Bots] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'chatbotMain'}">
					<a class="twodepth active" href="/cloudApi/chatbot/enChatbotMain">MRC Bots</a>
</c:if>		
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'chatbotMain'}">
					<a class="twodepth" href="/cloudApi/chatbot/enChatbotMain">MRC Bots</a>
</c:if>

<!-- [API Services/KBQA봇] -->
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'kbqaBotMain'}">
<a class="twodepth active" href="/cloudApi/chatbot/enKbqaMain">KBQA Bot</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'kbqaBotMain'}">
<a class="twodepth" href="/cloudApi/chatbot/enKbqaMain">KBQA Bot</a>
</c:if>--%>
<!-- [API Services/호텔 컨시어지 챗봇] -->
<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'hotelbotMain'}">
	<a class="twodepth active" href="/cloudApi/hotelbot/enHotelBot">Hotel Concierge Chatbot</a>
</c:if>
<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'hotelbotMain'}">
	<a class="twodepth" href="/cloudApi/hotelbot/enHotelBot">Hotel Concierge Chatbot</a>
</c:if>
<!--English Education-->

<c:if test="${fn:escapeXml(sessionScope.menuGrpName) eq 'English Education'}">
				<li class="collapsible list5 dropdwn active">
</c:if>
<c:if test="${fn:escapeXml(sessionScope.menuGrpName) ne 'English Education'}">
				<li class="collapsible list5 dropdwn">
</c:if>
<span>English Education</span>
<div class="ico_menu">
	<span title="STT for English Education"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_eng_1.svg" alt="STT for English Education"></span>
	<span title="Pronunciation Scoring"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_eng_2.svg" alt="Pronunciation Scoring"></span>
	<span title="Phonics Assessment"><img src="${pageContext.request.contextPath}/aiaas/en/images/menu/ico_eng_3.svg" alt="Phonics Assessment"></span>
</div>
                                    </li>
                                    <li class="sublist list5_sub">
                    <!-- [API Services/영어교육 STT] -->
                    <c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'engeduSttMain'}">
                                        <a class="twodepth active" href="/cloudApi/engedu/enEngeduSttMain">STT for English Education</a>
                    </c:if>
                    <c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'engeduSttMain'}">
                                        <a class="twodepth" href="/cloudApi/engedu/enEngeduSttMain">STT for English Education</a>
                    </c:if>

                    <!-- [API Services/영어교육 발음평가] -->
                    <c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'engeduPronMain'}">
                                        <a class="twodepth active" href="/cloudApi/engedu/enEngeduPronMain">Pronunciation Scoring</a>
                    </c:if>
                    <c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'engeduPronMain'}">
                                        <a class="twodepth" href="/cloudApi/engedu/enEngeduPronMain">Pronunciation Scoring</a>
                    </c:if>

                    <!-- [API Services/영어교육 파닉스] -->
                    <c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'engeduPhonicsMain'}">
                                        <a class="twodepth active" href="/cloudApi/engedu/enEngeduPhonicsMain">Phonics Assessment</a>
                    </c:if>
                    <c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'engeduPhonicsMain'}">
                                        <a class="twodepth" href="/cloudApi/engedu/enEngeduPhonicsMain">Phonics Assessment</a>
                    </c:if>
                                    </li>

			</ul>
			<!--//API Services 리스트-->
			
			<!--Application Services 리스트-->
<%--			<p class="sub_tit sub_list">Application Services</p>--%>
<%--			<ul>--%>
<%--				<li class="app_manu">--%>
<%--<!-- [Application/My AI Voice] -->			--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'aiVoiceFontMain'}">--%>
<%--					<a class="twodepth active" href="/tts_train/enAiVoiceFontMain">My AI Voice</a>--%>
<%--</c:if>	--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'aiVoiceFontMain'}">--%>
<%--					<a class="twodepth" href="/tts_train/enAiVoiceFontMain">My AI Voice</a>--%>
<%--</c:if>--%>

<%--<!-- [Application/maum 회의록] -->--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'minutesMain'}">--%>
<%--	<a class="twodepth active" href="/minutes/enMinutesMain">maum Minutes</a>--%>
<%--</c:if>--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'minutesMain'}">--%>
<%--	<a class="twodepth" href="/minutes/enMinutesMain">maum Minutes</a>--%>
<%--</c:if>--%>


<%--					<!-- [Application/음성봇] -->--%>
<%--&lt;%&ndash; 		--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'Voice 봇'}">--%>
<%--					<a class="twodepth active" href="/voicebot/enVoiceBotMain">Voice bot</a>--%>
<%--</c:if>	--%>
<%--<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'Voice 봇'}">--%>
<%--					<a class="twodepth" href="/voicebot/enVoiceBotMain">Voice bot</a>--%>
<%--</c:if> &ndash;%&gt;--%>

<%--				</li>--%>
<%--			</ul>--%>
			<!--//Application Services 리스트-->

			<!--data 리스트-->

<%--			<p class="sub_tit sub_list">Data</p>--%>
<%--			<ul>--%>
<%--				<li class="data_manu">--%>
<%--					<!-- [Data/판매 데이터] -->--%>
<%--					<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'sellDataMain'}">--%>
<%--						<a class="twodepth active" href="/data/enSellDataMain">Data available for purchase </a>--%>
<%--					</c:if>--%>
<%--					<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'sellDataMain'}">--%>
<%--						<a class="twodepth" href="/data/enSellDataMain">Data available for purchase </a>--%>
<%--					</c:if>--%>

<%--					<!-- [Data/데이터 정제 서비스] -->--%>
<%--					<c:if test="${fn:escapeXml(sessionScope.selectMenu) eq 'dataServiceMain'}">--%>
<%--						<a class="twodepth active" href="/data/enDataServiceMain">Data Refinery Services</a>--%>
<%--					</c:if>--%>
<%--					<c:if test="${fn:escapeXml(sessionScope.selectMenu) ne 'dataServiceMain'}">--%>
<%--						<a class="twodepth" href="/data/enDataServiceMain">Data Refinery Services</a>--%>
<%--					</c:if>--%>
<%--				</li>--%>
<%--			</ul>--%>
			<!--//data 리스트-->
		</div>
		<!-- //.snb EN -->


<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/menu.js"></script>		