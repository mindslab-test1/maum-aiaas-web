<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%--<c:if test="${empty fn:escapeXml(sessionScope.accessUser)}">--%>
<%--	<script>--%>
<%--		window.location.href = "/?lang=en";--%>
<%--	</script>--%>
<%--</c:if>--%>


<sec:authorize access="isAnonymous()">
	<script>
		window.location.href = "/?lang=en";
	</script>
</sec:authorize>

<form id="logout-form" action='/logout' method="POST">
   <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>

        <h1><a href="/main/enMainHome"><img src="${pageContext.request.contextPath}/aiaas/common/images/logo_cloudAPI.svg" alt="cloudAPI"></a></h1>
		<!-- .sta -->
		<div class="sta">
			
			<!-- .titArea -->
			<div class="titArea">               
                <div class="path">
                    <span><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_path_home_bk.png" alt="HOME"></span>
                    <span>${fn:escapeXml(sessionScope.menuName)}</span>
<c:if test="${fn:escapeXml(sessionScope.subMenuName) ne ''}">                    
                    <span>${fn:escapeXml(sessionScope.subMenuName)}</span>
</c:if>
                </div>
            </div>
            <!-- //.titArea -->	
			
			<!-- .etcmenu -->
			<div class="etcmenu">
<!--			<span><em>P</em><em>1,000</em> point</span>-->
				<div class="userBox">
					<dl>
						<dt class="ico_user"><em class="far fa-user"></em></dt>
						<dd>
							<a target="_self" href="#none">${fn:escapeXml(sessionScope.accessUser.email)}<em class="fas fa-chevron-down"></em></a>
						</dd>
						<ul class="lst">
 							<li class="ico_profile"><a target="_blank"  href="/user/profileMain">PROFILE</a></li>
 							 <li class="ico_account"><a target="_blank" href="/user/apiAccountMain">API ACCOUNT</a></li>
							<li class="ico_payment"><a target="_blank"  href="/user/paymentInfoMain">PAYMENT</a></li>
							<li class="ico_logout"><a href="#" onclick="document.getElementById('logout-form').submit();">LOGOUT</a></li>
						</ul>
					</dl>
				</div>
			</div>
			<!-- //.etcmenu -->
			<div class="lang_box">
				<span>English</span>
				<span><a href="/main/krMainHome" target="_self">한국어</a></span>
			</div>
		</div>
		<!-- //.sta -->