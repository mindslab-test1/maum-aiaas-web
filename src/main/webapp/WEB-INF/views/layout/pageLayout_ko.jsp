<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="t" uri="http://tiles.apache.org/tags-tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%
 response.setHeader("Cache-Control","no-cache");
 response.setHeader("Pragma","no-cache");
 response.setDateHeader("Expires",0);
%>

<%--  CSS/JS 파일 캐싱 방지 --%>
<%
	Date lastModifiedStyle = new Date();
	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>

<!DOCTYPE html> 
<html lang="ko">
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">

<!-- Open Graph Tag -->
<meta property="og:title"            content="maum.ai"/>
<meta property="og:type"             content="website"/><!-- 웹 페이지 타입 -->
<meta property="og:url"              content="https://maum.ai"/>
<meta property="og:image"            content="${pageContext.request.contextPath}/aiaas/common/images/maum.ai_web.png"/>
<meta property="og:description"      content="웹인공지능이 필요할땐 마음AI"/>

<!-- icon_favicon -->
<link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
<link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">

<!-- resources -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/reset.css?ver=<%=fmt.format(lastModifiedStyle)%>">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/font.css?ver=<%=fmt.format(lastModifiedStyle)%>">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/pop_common.css?ver=<%=fmt.format(lastModifiedStyle)%>">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/css_kiosk/kiosk.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/common.css?ver=<%=fmt.format(lastModifiedStyle)%>">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/common/css/all.css">
<%--<link rel="stylesheet" href="https://sds.maum.ai/css?host=mindslab"/>--%>

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-1.11.0.min.js"></script>	
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/common.js?ver=<%=fmt.format(lastModifiedStyle)%>"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jsrender.min.js"></script>


<title>maum.ai platform</title>
<%--<script src="https://sds.maum.ai/js/mindslab"></script>--%>
<%--<script>--%>
<%--	window.GitpleConfig = {"appCode":"ubFQIWPWwu4xGxC4kMsPhe2mei2533"};--%>
<%--	!function(){function e(){function e(){var e=t.contentDocument,a=e.createElement("script");a.type="text/javascript",a.async=!0,a.src=window[n]&&window[n].url?window[n].url+"/inapp-web/gitple-loader.js":"https://app.gitple.io/inapp-web/gitple-loader.js",a.charset="UTF-8",e.head&&e.head.appendChild(a)}var t=document.getElementById(a);t||((t=document.createElement("iframe")).id=a,t.style.display="none",t.style.width="0",t.style.height="0",t.addEventListener?t.addEventListener("load",e,!1):t.attachEvent?t.attachEvent("onload",e):t.onload=e,document.body.appendChild(t))}var t=window,n="GitpleConfig",a="gitple-loader-frame";if(!window.Gitple){document;var i=function(){i.ex&&i.ex(arguments)};i.q=[],i.ex=function(e){i.processApi?i.processApi.apply(void 0,e):i.q&&i.q.push(e)},window.Gitple=i,t.attachEvent?t.attachEvent("onload",e):t.addEventListener("load",e,!1)}}();--%>
<%--	Gitple('boot');--%>
<%--</script>--%>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-122649087-1"></script>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag(){dataLayer.push(arguments);}
	gtag('js', new Date());
	gtag('config', 'UA-122649087-1');
</script>

<!-- 럭키오렌지 tag -->
<script async defer src="https://tools.luckyorange.com/core/lo.js?site-id=dddcda82"></script>

</head>
<body>
<!-- .page loading -->
<div id="pageldg">
    <span class="out_bg">
        <em>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <b>&nbsp;</b>
        </em>
    </span>
</div>
<!-- //.page loading -->
	<div id="wrap">
		<!-- #header -->
		<header id="header" class="header">
			<t:insertAttribute name="incHeader" />
		</header>
		<t:insertAttribute name="incLeftMenu" />		
		<div id="container">
			<t:insertAttribute name="incContent" />
			<t:insertAttribute name="incFooter" />
		</div>
  	</div>
</body>
</html>
