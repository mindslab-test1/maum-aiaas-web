<%--
  Created by IntelliJ IDEA.
  User: YeJun Lee
  Date: 2021/05/13
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <!-- Cache reset -->
    <meta http-equiv="Expires" content="Mon, 06 Jan 2016 00:00:01 GMT">
    <meta http-equiv="Expires" content="-1">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <!-- Open Graph Tag -->
    <meta property="og:title"         content="AI サクセスストーリー">
    <meta property="og:type"          content="website"> <!-- 웹 페이지 타입 -->
    <meta property="og:url"           content="">
    <meta property="og:image"         content="../resources/images/maumBook/img_svc_prev.png">
    <meta property="og:description"   content="AI プラットフォーム・maum AI がそのお悩みを解決いたします。">
    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
    <!---------------------- General Resources ---------------------->
    <!-- General CSS -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/maumBookEvent.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/common/css/all.css">
    <!-- General Script -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-migrate-3.3.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/swiper.min.js"></script>

    <title>Event || Maum Book</title>

    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            }
            ;
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '6347251735300507');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=6347251735300507&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body>

<div id="wrap" class="event ja_ver" >

    <!-- #contents -->
    <div id="contents">
        <div class="stn intro">
            <div class="contBox">
                <div class="msg_lead">
                    <p>&ldquo;今の会社にも AI を導入できるだろうか?&rdquo;</p>
                    <p>&ldquo;そもそも、AI ってどうやって導入するの?&rdquo;</p>
                    <p>&ldquo;自社開発するしかないのかな&rdquo;</p>
                    <%--<div class="msg_audio_play">
                        <button type="button" class="btn_play" title="오디오 재생 버튼">
                            <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
                                <defs>
                                    <clipPath id="0v71adoxsa">
                                        <path fill="none" d="M0 0H40V40H0z"/>
                                    </clipPath>
                                </defs>
                                <g clip-path="url(#0v71adoxsa)" class="ico_audio">
                                    <path fill="#8ba7b3" d="M14.368 19.706a.64.64 0 0 1 .328-.841 9.638 9.638 0 0 0 0-17.641.639.639 0 0 1 .511-1.17 10.918 10.918 0 0 1 0 19.982.637.637 0 0 1-.839-.329zm-3.882-1.281l-4.944-3.638H.636A.637.637 0 0 1 0 14.148V5.941A.637.637 0 0 1 .636 5.3h4.906l4.944-3.638a.636.636 0 0 1 1.012.515V17.91a.637.637 0 0 1-.636.639.632.632 0 0 1-.377-.124zM1.273 13.51h4.476a.63.63 0 0 1 .376.124l4.1 3.017V3.439l-4.1 3.017a.633.633 0 0 1-.376.123H1.273zm13.2 1.549a.639.639 0 0 1 .051-.9 5.53 5.53 0 0 0 0-8.224.637.637 0 1 1 .849-.952 6.809 6.809 0 0 1 0 10.127.635.635 0 0 1-.9-.05z" transform="translate(9.31 10.085)"/>
                                </g>
                            </svg>
                        </button>
                        <audio src="${pageContext.request.contextPath}/aiaas/kr/audio/maumBook_intro.wav" id="ava_audio"></audio>
                    </div>--%>
                </div>

                <div class="svc_start">
                    <p class="msg_key">ご心配なく。</p>
                    <p class="msg_key">AI プラットフォーム・maum AI がそのお悩みを解決いたします。</p>

                    <img src="${pageContext.request.contextPath}/aiaas/en/images/logo_maumAi_en.png" alt="maum AI logo" class="maum_logo">
                    <p class="msg_start">AI を導入する、最も確実な方法、maum AI</p>
                    <p class="msg_start">maum AI を試してみませんか?</p>

                    <div class="btnBox">
                        <a href="https://maum.ai" title="maum AI">いいえ</a>
                        <a href="#none" class="btn_go_event">はい</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="stn success_cases">
            <div class="contBox">
                <p class="sub_tit">多くの企業が、maum AI と共に AI の導入に成功しています。</p>
                <strong class="main_tit">AI サクセスストーリー</strong>

                <!-- .scs_cases_box -->
                <div class="scs_cases_box swiper-container">
                    <ul class="cases_lst swiper-wrapper">
                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>航空</span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_incheonAirport.png" alt="Incheon Airport logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name">仁川空港管制センター・テキスト分析システム</p>
                                    <p class="svc_desc">maum AI のテキスト分析技術で、管制通信を見える化した事例です。</p>
                                    <div class="viewBox dualItem">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_ic_aprt01.png" alt="Incheon Airport success case 01">
                                            <p>管制官とパイロットの通信をリアルタイムモニタリング</p>
                                        </div>
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_ic_aprt02.png" alt="Incheon Airport success case 02">
                                            <p>電波障害等状況のリアルタイムモニタリング</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>

                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>製鋼</span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_posco.svg" alt="POSCO logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name">POSCO スマートファクトリー</p>
                                    <p class="svc_desc">リアルタイムモニタリング技術により、製造工程の効率を大幅アップ</p>
                                    <div class="viewBox">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_posco.png" alt="POSCO success case">
                                            <p>生産性向上・コストカットのための AI スマートファクトリーの導入</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>
                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>金融</span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_hanaBank.svg" alt="hanabank logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name svc_label">Hana Bank HAI Banking <span><strong>5</strong> 年間運営中</span></p>
                                    <p class="svc_desc">テキスト分析・画像解析等最新の AI 技術を用いた、<br> AI インターネットバンキングサービス・HAI をローンチしました。</p>
                                    <div class="viewBox">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_hanaBank.png" alt="hanabank success case">
                                            <p>海外送金や料金払込など、全部チャットだけで。<br> AI による、全く新しい金融サービスを開発</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>
                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>金融</span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_hyundai.svg" alt="Hyundai Marine logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name svc_label">現代海上 AI 音声 bot <span><strong>4</strong> 年間運営中</span></p>
                                    <p class="svc_desc">リアルタイム音声認識技術と、<br> Chatbot 技術を用いた AI コールセンターを構築しました。</p>
                                    <div class="viewBox">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_hyundai.png" alt="Hyundai Marine success case">
                                            <p>AI 音声 bot による自動化サービスで、顧客・コールスタッフの満足度向上</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>
                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>地方自治体</span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_seoul.svg" alt="Seoul Metropolitan City logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name">ソウル特別市 Deep Learning 車両認識</p>
                                    <p class="svc_desc">maum AI の車両認識ソリューションと映像解析による開発</p>
                                    <div class="viewBox">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_seoul.png" alt="Seoul Metropolitan City success case">
                                            <p>老朽化したディーゼル車の通行量 30% 減</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>
                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>地方自治体</span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_suwon.svg" alt="Suwon City logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name">水原市 監視カメラによる異常行動の検出</p>
                                    <p class="svc_desc">AI による異常行動検出・分析システムを開発</p>
                                    <div class="viewBox">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_suwon.png" alt="Suwon City success case">
                                            <p>監視カメラによる異常行動検出システムにより、検挙率向上</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>
                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>地方自治体</span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_kyeongnam.svg" alt="Gyeongnam logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name">可搬型 AI 議事録システム</p>
                                    <p class="svc_desc">多重接続が可能な無線マイクに、弊社の高性能 AI 議事録システムを用いた開発</p>
                                    <div class="viewBox">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_kyeongnam.png" alt="Gyeongnam success case">
                                            <p>AI 議事録システムによる業務効率の向上</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>
                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>地方自治体 </span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_ulju.png" alt="Ulsan Metropolitan City logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name">蔚山広域市 蔚州郡・可搬型 On Device AI 議事録システム</p>
                                    <p class="svc_desc">スマートマイクを用い、携帯性に優れた会議システムを実現</p>
                                    <div class="viewBox">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_ulju.png" alt="Ulsan Metropolitan City success case">
                                            <p>リアルタイムに文字化され、編集が容易</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>
                    </ul>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>
                </div>
                <!-- //.scs_cases_box -->
            </div>
        </div>

        <div class="stn strength">
            <div class="contBox">
                <p class="sub_tit">AI 導入に関するお悩みを解決する</p>
                <strong class="main_tit">maum AI の３つのイノベーション</strong>
                <ul class="strt_lst">
                    <li>
                        <div class="lst_cont">
                            <span>30 種類以上の AI 基盤技術を組み合</br>わせた様々なサービスを実現</span>
                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/ico_strt01.svg" alt="strength icon 01">
                        </div>
                    </li>
                    <li>
                        <div class="lst_cont">
                            <span>国内外の多数のクライアント社で検</br>証された、高性能・安定性</span>
                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/ico_strt02.svg" alt="strength icon 02">
                        </div>
                    </li>
                    <li>
                        <div class="lst_cont">
                            <span>設計から導入まで、無料コンサルティング</span>
                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/ico_strt03.svg" alt="strength icon 03">
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="stn eventBanner">
            <div class="contBox">
                <p class="sub_tit">全てのサクセスストーリーをご確認いただける</p>
                <strong class="main_tit">maum AI book</strong>
                <div class="imgBox">
                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_maumAi_book.png" alt="maum AI book">
                </div>
                <p class="desc_txt">maum AI ブックは、イノベーションを必要とする顧客企業様が、<br> AI を導入することでどのようにイノベーションを成し得ることができたか、<br> そのサクセスストーリーを含んでいます。</p>
            </div>
        </div>

        <div class="stn contact">
            <div class="contBox">
                <p class="sub_tit">多くの企業様が、既にmaum AI の導入で AI 技術の導入に成功しています。</p>
                <strong class="main_tit">maum AI の無料資料請求には、下記フォームをご記入ください</strong>
                <div class="contactform">
                    <dl>
                        <dt>受領方法</dt>
                        <dd>
                            <div class="radioBox">
                                <input type="radio" name="contact_type" id="eBook" value="eBook" checked>
                                <label for="eBook">E-book</label>
                            </div>
                            <%--<div class="radioBox">
                                <input type="radio" name="contact_type" id="delivery" value="우편물">
                                <label for="delivery">우편물 발송</label>
                            </div>--%>
                        </dd>
                    </dl>
                    <dl>
                        <dt>氏名<span>(必須)</span></dt>
                        <dd><input type="text" id="userName" class="ipt_txt" autocomplete="off" placeholder="お名前をご入力ください"></dd>
                    </dl>
                    <dl>
                        <dt>E-mail<span>(必須)</span></dt>
                        <dd><input type="text" id="userTel" class="ipt_tel" placeholder="ご連絡先をご入力ください"></dd>
                    </dl>
                    <dl>
                        <dt>お問い合わせ<em>(選択)</em></dt>
                        <dd>
                            <textarea id="usertxt" placeholder="お問い合わせ内容をご記入ください"></textarea>
                        </dd>
                    </dl>
                    <dl>
                        <dt>個人情報取得同意</dt>
                        <dd>
                            <div class="assentBox">
                                <div class="checkBox">
                                    <input type="checkbox" name="assent" id="assent">
                                    <label for="assent">同意します。</label>
                                </div>
                                <p>取得する個人情報：氏名・連絡先
                                    <br>
                                    ご記入いただいた個人情報は、お問い合わせへの対応等のため、１年間保管されます。
                                    <br>
                                    個人情報の取得を拒否した場合、お問い合わせを受け付け兼ねます。
                                    <br>
                                    <br>
                                    <span class="ft_point01">弊社のコンサルタントより、３営業日以内にご連絡を差し上げております。</span>
                                </p>
                            </div>
                        </dd>
                    </dl>
                    <!-- [D] 문의하기 버튼 클릭 시 성공적으로 문의접수 된 경우 alert 등장 (문의가 완료되었습니다. 버튼:확인 -> 확인버튼 클릭 시에는 maum.ai 홈페이지로 이동) -->
                    <button type="button" id="inquireBtn" disabled="true">送信</button>
                </div>
<%--                <p class="slogan">마음AI는 언제나 당신의 마음까지 생각합니다.</p>--%>
                <img src="${pageContext.request.contextPath}/aiaas/en/images/logo_maumAi_en.png" alt="maum AI logo">
            </div>
        </div>
    </div>
    <!-- //#contents -->

    <!-- 신청 바로가기 버튼 -->
    <button type="button" class="btn_go_contact">
        <span>無料見積依頼</span>
    </button>
    <!-- //신청하기 버튼 -->
</div>

<!---------------------- Local resources ---------------------->
<!-- page Landing -->
<script type="text/javascript">
    $(window).load(function() {
        //page loading delete
        $('#pageldg').addClass('pageldg_hide').delay(300).queue(function() { $(this).remove(); });
    });
</script>

<script>
jQuery.event.add(window,"load",function() {
    $(document).ready(function () {
        // 동영상 레이어 팝업
        $('.btn_video_play').on('click', function () {
            $('body').css('overflow', 'hidden');

            $('body').append(
                '<div class="lyr_bg_dim"></div>\
                <div id="lyr_video_player" class="lyrPlayBox">\
                    <div class="lyr_ct">\
                        <video controls autoplay>\
                            <source src="${pageContext.request.contextPath}/aiaas/common/video/kbs_success_case.mp4" type="video/mp4">\
                        </video>\
                    </div>\
                    <button type="button" class="btn_lyr_close">\
                        <em class="fas fa-times" title="닫기버튼"></em>\
                    </button>\
                </div>');

            $('.btn_lyr_close, .lyr_bg_dim').on('click', function () {
                $('body').css('overflow', '');
                $('.lyr_bg_dim').remove();
                $('#lyr_video_player').remove();
            });
        });

        //Layer popup open
        $('.btn_lyr_open').on('click', function () {
            var winHeight = $(window).height() * 0.7,
                hrefId = $(this).attr('href');

            $('body').css('overflow', 'hidden');
            $('body').find(hrefId).wrap('<div class="lyrWrap"></div>');
            $('body').find(hrefId).before('<div class="lyr_bg"></div>');

            //Layer popup close
            $('.btn_lyr_close, .lyr_bg').on('click', function () {
                $('body').css('overflow', '');
                $('body').find(hrefId).unwrap();
                $('.lyr_bg').remove();
            });
        });

        //Layer popup close
        var assentClicked = false;
        $('#assent').on('click', function () {
            if (!assentClicked) {
                $('#btn_request').prop('disabled', '');
                assentClicked = true;
            } else {
                $('#btn_request').prop('disabled', 'disabled');
                assentClicked = false
            }
        });

        // intro에서 오디오 재생 버튼
        var audio = document.getElementById('ava_audio');
        $('.btn_play').on('click', function () {
            audio.play();
        });

        // intro에서 이벤트 페이지 이동
        $('.btnBox .btn_go_event').on('click', function () {
            $('html').animate({scrollTop: 0,}, 500);
            $('.intro').hide();
            $('.stn:not(.intro)').fadeIn(300);
            if ($(window).width() <= 768) {
                $('.btn_go_contact').fadeIn();
            } else {
                $('.btn_go_contact').fadeIn();
            }
        });

        // swiper
        new Swiper('.scs_cases_box', {
            slidesPerGroup : 1,
            slidesPerView: 1,
            loop: true,
            observer: true,
            observeParents: true,
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },
            navigation: {
                prevEl: '.swiper-button-prev',
                nextEl: '.swiper-button-next',
            },
            autoplay: {
                delay: 6000,
                disableOnInteraction: false,
            },
            breakpoints: {
                320: {
                    spaceBetween: 20,
                    autoHeight : true,
                },
            },
        });

        // 문의 영역으로 이동
        $('.btn_go_contact').on('click', function () {
            var contactOffset = $('.contact').offset();
            $('html').animate({scrollTop: contactOffset.top}, 300);
        });

        // 문의 영역 이동 시 버튼 제거
        $(window).scroll(function () {
            var contactOffset = $('.contact').offset();
            var contactOffsetTop = contactOffset.top;
            var scrollTop = $('html').scrollTop() + $('.contact').height();
            if ($(window).width() > 768 && scrollTop >= contactOffsetTop) {
                $('.btn_go_contact').fadeOut();
            } else if ($(window).width() > 768 && scrollTop < contactOffsetTop) {
                $('.btn_go_contact').fadeIn();
            } else if ($(window).width() <= 768 && scrollTop >= contactOffsetTop) {
                $('.btn_go_contact').fadeOut();
            } else if ($(window).width() <= 768 && scrollTop < contactOffsetTop) {
                $('.btn_go_contact').fadeIn();
            }
        });

        // 문의하기 연락처 숫자입력 제한
/*        $('#userTel').on('keyup', function () {
            $(this).val($(this).val().replace(/[^0-9]/g, ''));
        });*/

        // 문의하기 필수 내용 입력, 동의체크 시 버튼 컬러 활성화
        $('.ipt_txt').on('input', checkInput);
        $('.ipt_tel').on('input', checkInput);
        $('#assent').on('click', checkInput);

        function checkInput() {
            var userName = $('.ipt_txt').val();
            var userTel = $('.ipt_tel').val();
            var assentCheck = $('#assent')
            var inquireButton = $('#inquireBtn');

            if (userName == '' || userTel == '' || assentCheck.is(':checked') == false) {
                inquireButton.prop('disabled', true);
            } else {
                inquireButton.prop('disabled', false);
            }
        }

        //문의하기 메일 발송 2021.03.11 SMR
        $('#inquireBtn').on('click', function () {
            var type = $('input[type=radio]:checked').val();
            var agreeCheck = $('input[type=checkbox]:checked').is(':checked');
            var checkText;

            agreeCheck ? checkText = '동의': checkText = '비동의'

            let title = "[마음북 - 일본] " + $("#userName").val() + "님의 문의 사항입니다.";
            let msg = "이름 : " + $("#userName").val()
                + "<br>이메일 : " + $("#userTel").val()
                + "<br>수령 방법 : " + type
                + "<br>문의 내용 : " + $("#usertxt").val()
                + "<br>개인 정보 수신 동의 : " + checkText;

            let formData = new FormData();

            formData.append('fromaddr', 'maumBook');
            formData.append('toaddr', 'hello@mindslab.ai');
            formData.append('subject', title);
            formData.append('message', msg);

            $.ajax({
                type: 'POST',
                async: true,
                url: '/support/sendContactMail',
                dataType: 'text', //서버가 응답할 때 보내줄 데이터 타입
                data: formData,
                processData: false, //query string false
                contentType: false, // multipart/form-data 설정
                success: function () {
                    $("#userName").val('');
                    $("#userTel").val('');
                    $("#usertxt").val('');
                    $("#eBook").prop('checked', true);
                    $("#assent").prop('checked', false);

                    alert('お問い合わせを受け付けました。')
                    window.location.href='/';
                },

                error: function (xhr, status, error) {
                    console.log("error", error);
                    alert("お問い合わせの受付に失敗しました。しばらく後に再度お試しください。");
                }
            });
        });
    });
});
</script>
</body>
</html>