<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-06-23
  Time: 14:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="../common/common_header.jsp" %>

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->
<!-- 2 .pop_simple -->
<div class="pop_simple dataset_pop" id="pop_2">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap">
        <div class="pop_hd">
            <em class="fas fa-times ico_close"></em>
            <!--클릭한 제목 읽어오기-->
            <h6 class="cont_name">Maum.ai 비즈니스</h6>
        </div>

        <!-- .pop_bd -->
        <div class="pop_bd">
            <table>
                <caption class="hide"></caption>

                <colgroup>
                    <col width="95px;"><col>
                </colgroup>

                <tbody>
                <tr>
                    <th>수강 대상 &colon;</th>
                    <td class="target"></td>
                </tr>
                <tr>
                    <th>난이도 &colon;</th>
                    <td class="difficulty"></td>
                </tr>
                <tr>
                    <th>형식 &colon;</th>
                    <td class="format">pdf</td>
                </tr>
                <tr>
                    <th colspan="2">Description &colon;</th>
                </tr>
                <tr>
                    <td colspan="2">
                        <textarea disabled class="description"></textarea>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="download" href="" download target="_blank">학습 자료 보기</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- wrap -->
<div id="wrap" class="maumUI">
    <div class="loginWrap dataset">
        <%@ include file="../common/header.jsp" %>

        <!-- .content -->
        <div class="content">
            <!-- dataset_box -->
            <div class="dataset_box">
                <h3 class="all_list"><img src="${pageContext.request.contextPath}/aiaas/common/images/academy.png" alt="maum Academy 로고"/></h3>

<%--                <div class="srch_box">--%>
<%--                    <form action="">--%>
<%--                        <input type="text">--%>
<%--                        <button type="submit"><span class="fas fa-search"></span>Search</button>--%>
<%--                    </form>--%>
<%--                </div>--%>

                <!-- .stn -->
                <div class="stn">
                    <ul>


                        <li>
                            <p class="title">maum.ai 안내서</p>
                            <div class="keyword_box" id="box_1">
                            </div>
                        </li>
                        <li>
                            <p class="title">MBA(Maum.ai Business in Action</p>
                            <div class="keyword_box" id="box_2">
                            </div>
                        </li>
                        <li>
                            <p class="title">엔진 가이드</p>
                            <div class="keyword_box" id="box_3">
                            </div>
                        </li>
                        <li>
                            <p class="title">maum.ai 기술</p>
                            <div class="keyword_box" id="box_4">
                            </div>
                        </li>
                        <li>
                            <p class="title">읽을 거리</p>
                            <div class="keyword_box" id="box_5">
                            </div>
                        </li>
                        <li>
                            <p class="title">Certificate</p>
                            <div class="keyword_box">
                                <a href="#none" class="blank_txt">추후 업로드 예정</a>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- //.stn -->
            </div>
            <!-- //.dataset_box -->

            <!-- .dataset_box -->
            <div class="dataset_box">
                <div class="dataset_lst">
                    <table summary="corpus type, corpus name, language, data type, application, format으로 구성">
                        <caption class="hide">데이터셋 리스트</caption>

                        <colgroup>
                            <col width="30%"><col width="40%"><col width="10%"><col width="10%"><col width="10%">
                        </colgroup>

                        <thead>
                        <tr>
                            <th>모듈명</th>
                            <th>콘텐츠명</th>
                            <th>수강 대상</th>
                            <th>난이도</th>
                            <th>형식</th>
                        </tr>
                        </thead>

                        <tbody class="tbody">
                            <c:forEach items="${academy_list}" var="list" varStatus="index">
                                <tr data-value="<c:out value="${index.count}"/>">
                                    <td id="module_name"><c:out value="${list.module_name}"/></td>
                                    <td id="cont_name"><c:out value="${list.contents_name}"/></td>
                                    <td id="target"><c:out value="${list.course_target}"/></td>
                                    <td id="difficulty"><c:out value="${list.difficulty}"/></td>
                                    <td id="file_format"><c:out value="${list.file_format}"/></td>
                                    <td hidden id="description"><c:out value="${list.description}"/></td>
                                    <td hidden id="download"><c:out value="${list.file_link}"/></td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>

<%--                <div class="btm_info">--%>
<%--                    <div class="paging">--%>
<%--                        <a class="btn_paging_prev" href="#none" ><em class="fas fa-angle-left"></em></a>--%>
<%--                        <span class="list">--%>
<%--                            <a href="#none" class="on">1</a>--%>
<%--                            <a href="#none">2</a>--%>
<%--                            <a href="#none">3</a>--%>
<%--                            <a href="#none">4</a>--%>
<%--                            <a href="#none">5</a>--%>
<%--                        </span>--%>
<%--                        <a class="btn_paging_next" href="#none"><em class="fas fa-angle-right"></em></a>--%>
<%--                    </div>--%>
<%--                </div>--%>
            </div>
            <!-- //.dataset_box -->
        </div>
        <!-- //.content -->

        <!-- #footer -->
        <%@ include file="../common/footer.jsp" %>
    </div>
</div>

    <script type="text/javascript">

        function go_academy(){
            // window.location.href="/main/krMainHome";
            window.location.href = "${google_url}" + "?targetUrl=/home/krAcademyMain";
        }

        $(window).load(function() {
            $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
                $(this).remove();
            });
        });
    </script>

    <script type="text/javascript">
        jQuery.event.add(window,"load",function(){

            function loadAcademyInfo() {

                $.ajax({
                    url		:"/academy/getAcademyInfo",
                    cache   : false,
                    async   : true,
                    type	:"GET",
                    dataType: "JSON",
                    success : function(result) {
                        var keys = Object.keys(result);

                        keys.forEach(value =>{

                            console.dir(value);  //모듈 이름
                            var module_name = value;

                            var arr_result = result[value]; //모듈 리스트
                            console.log(arr_result);

                            arr_result.forEach(val =>{

                                if (module_name == "maum.ai 안내서"){
                                    var box_1 = val['contents_name'];
                                    var id = val['id'];
                                    $('#box_1').append("<a href=\"#none\" class=\"item_btn \" name="+id+">"+box_1+"</a>");
                                }else if (module_name == "MBA (Maum.ai Business in Action)"){
                                    var box_2 = val['contents_name'];
                                    var id = val['id'];
                                    $('#box_2').append("<a href=\"#none\" class=\"item_btn\" name="+id+">"+box_2+"</a>");
                                }else if (module_name == "엔진 가이드"){
                                    var box_3 = val['contents_name'];
                                    var id = val['id'];
                                    $('#box_3').append("<a href=\"#none\" class=\"item_btn\" name="+id+">"+box_3+"</a>");
                                }else if (module_name == "maum.ai 기술"){
                                    var box_4 = val['contents_name'];
                                    var id = val['id'];
                                    $('#box_4').append("<a href=\"#none\" class=\"item_btn\" name="+id+">"+box_4+"</a>");
                                }else if (module_name == "읽을 거리"){
                                    var box_5 = val['contents_name'];
                                    var id = val['id'];
                                    $('#box_5').append("<a href=\"#none\" class=\"item_btn\" name="+id+">"+box_5+"</a>");
                                }

                            });
                        });
                        $(".item_btn").on('click', function(){
                            var item_= $(this).attr('name'); // 콘텐츠 ID 값
                            $('.stn ul li').removeClass('active');
                            $(".item_btn").removeClass('active');
                            $(this).addClass('active');
                            $(this).parent().parent('li').addClass('active');
                            $('.tbody tr').hide();
                            $('.tbody tr[data-value="'+item_ +'"').show();

                        });

                        $('.all_list').on('click', function(){
                            $('.tbody tr').show();
                        });
                    },
                    error 	: function(xhr, status, error) {
                        console.log(xhr);
                        console.log(status);
                        console.log(error);
                    }
                });
            }

            $(window).ready(function() {

               loadAcademyInfo();

                $('.dataset_lst table tbody tr').on('click',function(){

                    var value = $(this).attr('data-value');
                    var module_name = $(this).children('#module_name').text();
                    var cont_name = $(this).children('#cont_name').text();
                    var target = $(this).children('#target').text();
                    var difficulty = $(this).children('#difficulty').text();
                    var file_format = $(this).children('#file_format').text();
                    var description = $(this).children('#description').text();
                    var download = $(this).children('#download').text();

                    $('.dataset_pop').fadeIn();
                    $('.dataset_pop .cont_name').text(cont_name);
                    $('.dataset_pop .target').text(target);
                    $('.dataset_pop .difficulty').text(difficulty);
                    $('.dataset_pop .description').text(description);
                    $('.dataset_pop .download').attr('href',download);
                });
                $('.ico_close, .btn .cancel, .pop_bg').on('click',function(){
                    $('.dataset_pop').fadeOut(300);
                });


                $('.keyword_box a').on('click', function () {

                })

            })
        });

    </script>
