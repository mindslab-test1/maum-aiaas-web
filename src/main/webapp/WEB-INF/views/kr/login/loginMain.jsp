<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020/08/18
  Time: 10:10 오전
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>
<html>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
    <title>maum.ai platform</title>
    <!-- resources -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/reset.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/font.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/login.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- //resources -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-1.11.0.min.js"></script>

</head>
<body>
<div id="wrap">
    <header>
        <a href="https://maum.ai/" title="마음에이아이" target="_self"><img src="${pageContext.request.contextPath}/aiaas/common/images/maumai.svg" alt="maum.ai logo" /></a>
    </header>
    <section id="container">
        <div class="content">
            <form class="loginWrap" method="get" action="">
                <fieldset>
                    <legend>Login</legend>
                    <div class="loginBox">
                        <span>
                            <input type="text" name="ipt_id" id="ipt_id" class="ipt_txt" title="User Name" placeholder="이메일" >
                        </span>
                        <span>
                            <input type="password" name="ipt_pw" id="ipt_pw" class="ipt_txt" title="Password" placeholder="비밀번호" >
                        </span>
                        <span class="checks">
                            <input type="checkbox" name="ipt_check" id="ipt_check" class="ipt_check">
                            <label for="ipt_check"><em class="far fa-check-circle"></em> 로그인 상태 유지</label>
                            <!-- [D] 에러일 경우 .txt_error 스크립트로 구현 -->
                            <span class="txt_error">가입하지 않은 이메일이거나, 잘못된 비밀번호 입니다.</span>
                        </span>
                        <span>
                            <a href="javascript:login();" title="로그인" class="homebtn"> 로그인</a>
                        </span>
                    </div>
                    <span class="or">또는</span>
                </fieldset>
            </form>
            <div class="btn_box">
                <a href="javascript:login_google();" title="구글로그인" class="google_login"> 구글 로그인</a>
                <a href="javascript:login_facebook();" title="페이스북로그인" class="facebook_login">페이스북 로그인</a>
            </div>
            <div class="btn">
                <a href="/home/agree" class="btn_signupt">회원가입 </a><span>&nbsp;|&nbsp;</span>
                <a href="/home/password"  class="btn_forgot">비밀번호 찾기</a>
            </div>
        </div>
    </section>
    <footer>
        <div class="footer">
            <a href="#none">이용약관 </a>  ㅣ
            <a href="#none"> 개인정보처리방침 </a>  ㅣ
            <a href="#none"> 고객센터 </a> ㅣ
            <div class="lang">
                <span class="lang_select">한국어 <em class="fas fa-chevron-up"></em></span>
                <ul>
                    <li class="active">한국어</li>
                    <li><a href="#none">English</a></li>
                </ul>
            </div>
        </div>
        <p>Copyright © 2020 주식회사 마인즈랩. All rights reserved.</p>
    </footer>
</div>


<script>
    function login(){
        location.href = "https://maum.ai/?lang=ko";
    }
    function login_google(){
        var path= window.location.pathname;
        location.href = "${google_url}" +"?targetUrl="+path;
    }
    function login_facebook(){
        // 페이스북 로그인 구현
    }
    jQuery.event.add(window,"load",function() {
        $(document).ready(function () {
            // input 초기화
            $('.ipt_txt').each(function () {
                $(this).val('');
                $('#ipt_id').focus();
            });

            // 입력값 체크 (버튼 활성화)
            // $('.ipt_txt').on('change keyup paste click', function (e) {
            //     var idValLth = $('#ipt_id').val().length;
            //     var pwValLth = $('#ipt_pw').val().length;
            //
            //     if (idValLth > 0 && pwValLth > 0) {
            //         $('.btn_login').removeClass('disabled');
            //         $('.btn_login').removeAttr('disabled');
            //         $('.disbBox').remove();
            //     } else {
            //         $('.btn_login').addClass('disabled');
            //         $('.btn_login').attr('disabled');
            //     }
            // });

            //footer 언어 체크
            $('.lang_select').on('click', function(){
                $(this).next().toggleClass('active');
            })


        });
    });
</script>
</body>
</html>
