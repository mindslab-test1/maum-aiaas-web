<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020/08/18
  Time: 1:54 오후
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>
<html>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
    <title>maum.ai platform</title>
    <!-- resources -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/reset.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/font.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/login.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- //resources -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-1.11.0.min.js"></script>

</head>
<body>
<div id="wrap">
    <header>
        <a href="https://maum.ai/" title="마음에이아이" target="_self"><img src="${pageContext.request.contextPath}/aiaas/common/images/maumai.svg" alt="maum.ai logo" /></a>
    </header>
    <section id="container">
        <div class="content">
            <div class="agreeWrap">
                <div class="agreeBox">
                     <span class="checks">
                            <input type="checkbox" name="ipt_check" id="check1" class="ipt_check">
                            <label for="check1"><em class="far fa-check-circle"></em> 이용약관 동의 <strong> (필수)</strong></label>
                     </span>
                    <div class="scroll_box">
                        제 1조 (목적)<br>
                        본 약관은 주식회사 마인즈랩(이하 “회사”라고 합니다.)이 제공하는 마음에이아이(maum AI) 서비스(이하 “서비스”라 합니다)의 이용조건 및 절차, 요금에 관한 사항과 기타 이용에 필요한 제반 사항 규정을 목적으로 합니다.<br>
                        <br>
                        제2조 (약관의 효력 및 변경)<br>
                        ① 본 약관은 서비스의 홈페이지(maum.ai)에 게시하여 공지하며, 이에 동의한 고객과 회사간에 본 약관 제5조에 따라 서비스 이용계약을 체결함으로써 효력이 발생합니다.<br>
                        ② 회사는 필요하다고 인정하는 경우에는 본 약관을 변경할 수 있으며, 또한 법률 또는 정부의 명령•지시•권고나 법원의 판결 또는 회사 및 별도 사업자의 관련 정책 변경 등으로 인한 필요에 따라 서비스의 전부 또는 일부를 변경 또는 중지할 수 있습니다. 이 경우 회사는 적용일자 7일 전부터 본 조 제1항의 방법으로 공지해야 하며, 이용자에게 불리하게 약관을 변경하는 경우 30일 이상의 유예기간을 두고 공지하도록 합니다.<br>
                        ③ 회사가 전항에 따라 개정약관을 공지하면서 고객에게 적용예정일까지 회사에게 거부의사를 표시하지 않으면 동의의 의사표시가 표명된 것으로 본다는 것을 명확하게 공지하였음에도 고객이 명시적으로 거부의 의사표시를 하지 아니한 경우 고객이 개정약관에 동의한 것으로 봅니다.<br>
                        ④ 고객은 변경된 약관 사항에 동의하지 않는 경우, 서비스 이용계약을 해지할 수 있습니다.<br>
                        <br>
                        제3조 (약관 외 준칙)<br>
                        본 약관에 명시되지 않은 사항에 대해서는 관계법령 및 회사가 제공하는 부가서비스에 관한 별도의 약관, 이용규정 또는 세부 이용지침 등의 규정에 따릅니다.
                    </div>
                    <span class="checks">
                            <input type="checkbox" name="ipt_check" id="check2" class="ipt_check">
                            <label for="check2"><em class="far fa-check-circle"></em> 개인정보 수집 및 이용에 대한 안내 <strong> (필수)</strong></label>
                     </span>
                    <div class="scroll_box">
                        제 1조 (목적)<br>
                        본 약관은 주식회사 마인즈랩(이하 “회사”라고 합니다.)이 제공하는 마음에이아이(maum AI) 서비스(이하 “서비스”라 합니다)의 이용조건 및 절차, 요금에 관한 사항과 기타 이용에 필요한 제반 사항 규정을 목적으로 합니다.<br>
                        <br>
                        제2조 (약관의 효력 및 변경)<br>
                        ① 본 약관은 서비스의 홈페이지(maum.ai)에 게시하여 공지하며, 이에 동의한 고객과 회사간에 본 약관 제5조에 따라 서비스 이용계약을 체결함으로써 효력이 발생합니다.<br>
                        ② 회사는 필요하다고 인정하는 경우에는 본 약관을 변경할 수 있으며, 또한 법률 또는 정부의 명령•지시•권고나 법원의 판결 또는 회사 및 별도 사업자의 관련 정책 변경 등으로 인한 필요에 따라 서비스의 전부 또는 일부를 변경 또는 중지할 수 있습니다. 이 경우 회사는 적용일자 7일 전부터 본 조 제1항의 방법으로 공지해야 하며, 이용자에게 불리하게 약관을 변경하는 경우 30일 이상의 유예기간을 두고 공지하도록 합니다.<br>
                        ③ 회사가 전항에 따라 개정약관을 공지하면서 고객에게 적용예정일까지 회사에게 거부의사를 표시하지 않으면 동의의 의사표시가 표명된 것으로 본다는 것을 명확하게 공지하였음에도 고객이 명시적으로 거부의 의사표시를 하지 아니한 경우 고객이 개정약관에 동의한 것으로 봅니다.<br>
                        ④ 고객은 변경된 약관 사항에 동의하지 않는 경우, 서비스 이용계약을 해지할 수 있습니다.<br>
                        <br>
                        제3조 (약관 외 준칙)<br>
                        본 약관에 명시되지 않은 사항에 대해서는 관계법령 및 회사가 제공하는 부가서비스에 관한 별도의 약관, 이용규정 또는 세부 이용지침 등의 규정에 따릅니다.
                    </div>
                    <span class="checks">
                            <input type="checkbox" name="ipt_check" id="check3" class="ipt_check">
                            <label for="check3"><em class="far fa-check-circle"></em> 이벤트 등 프로모션 알림 메일 수신 (선택)</label>
                     </span>
                    <a href="/home/login" title="취소" target="_self">취소</a>
                    <a href="#none" title="확인" target="_self" class="agree_btn" >확인</a>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="footer">
            <a href="#none">이용약관 </a>  ㅣ
            <a href="#none"> 개인정보처리방침 </a>  ㅣ
            <a href="#none"> 고객센터 </a>
            <span>ㅣ</span>
            <div class="lang">
                <span class="lang_select">한국어 <em class="fas fa-chevron-up"></em></span>
                <ul>
                    <li class="active">한국어</li>
                    <li><a href="#none">English</a></li>
                </ul>
            </div>
        </div>
        <p>Copyright © 2020 주식회사 마인즈랩. All rights reserved.</p>
        <div class="m_lang">한국어 <a href="" title="English">English</a></div>
    </footer>
</div>


<script>

    jQuery.event.add(window,"load",function() {
        $(document).ready(function () {

            $('.agree_btn').on('click', function(){

                if($('#check1').is(":checked")== false || $('#check2').is(":checked")== false ){
                    alert('필수 사항은 모두 동의해 주세요.')
                }else{
                    window.location.href="/home/signUp"
                }
            });

            //footer 언어 체크
            $('.lang_select').on('click', function(){
                $(this).next().toggleClass('active');
            });

        });
    });
</script>
</body>
</html>

