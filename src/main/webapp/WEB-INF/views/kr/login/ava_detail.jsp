<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ include file="../common/common_header.jsp" %>

<!-- .page loading -->
<div id="pageldg">
    <span class="out_bg">
        <em>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <b>&nbsp;</b>
        </em>
    </span>
</div>
<!-- //.page loading -->
<%--</div>--%>
<div id="wrap" class="maumUI">
	<%@ include file="../common/header.jsp" %>

	<!-- .loginWrap -->
	<div class="loginWrap">
		<!-- .login_box -->
		<div class="login_box">
			<div class="stn_event_detail">
				<div class="imgBox">
					<img class="web" src="${pageContext.request.contextPath}/aiaas/kr/images/img_ava_contest.png" alt="AVA 유튜브 콘테스트 안내">
					<img class="mob" src="${pageContext.request.contextPath}/aiaas/kr/images/img_ava_contest_m.png" alt="AVA 유튜브 콘테스트 안내">
				</div>

				<div class="btnBox">
					<a target="_blank" href="https://ava.maum.ai" class="btn_go_ava">
						<img class="web" src="${pageContext.request.contextPath}/aiaas/kr/images/img_btn_goAva_p.png" alt="AVA 바로가기">
						<img class="mob" src="${pageContext.request.contextPath}/aiaas/kr/images/img_btn_goAva_pm.png" alt="AVA 바로가기">
					</a>
					<a target="_blank" href="https://forms.gle/DTDJmUWY9F25gPXY8" class="btn_video_rgst">
						<img class="web" src="${pageContext.request.contextPath}/aiaas/kr/images/img_btn_videoRgst.png" alt="영상 접수하기">
						<img class="mob" src="${pageContext.request.contextPath}/aiaas/kr/images/img_btn_videoRgst_m.png" alt="영상 접수하기">
					</a>
				</div>
			</div>
		</div>
		<!-- //.login_box -->
		<%@ include file="../common/footer.jsp" %>
	</div>
	<!-- //.loginWrap -->
</div>
<!-- //wrap -->
