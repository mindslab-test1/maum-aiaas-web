<%--
  Created by IntelliJ IDEA.
  User: YGE
  Date: 2019-07-12
  Time: 오후 8:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="../common/common_login_header.jsp" %>

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<!-- wrap -->
<div id="wrap" class="maumUI">
    <%@ include file="../common/login_header.jsp" %>



    <!-- #container -->
    <section id="container">
        <!-- .content -->
        <div class="content registrationContent">
            <div class="signUpCompleteWrap">
                <div class="paymentCompleteBox">
                    <h1>결제수단 등록이 완료 되었습니다.</h1>
                    <p id="register_noti">지금부터 <em class="ft_point_orange">한달 동안 무료</em>로 마음 AI 서비스를<br>자유롭게 이용 가능합니다.</p>
                    <ul>
                        <li>
                            <span>
                                <em class="fas fa-chevron-right"></em>결제수단
                            </span>
<%--                            <span>신용카드</span>--%>
                            <span>${payMethod }</span>
                        </li>
                        <li>
                            <span>
                                <em class="fas fa-chevron-right"></em>다음 결제일
                            </span>
                            <span>${payDate }</span>
                        </li>
                    </ul>
                </div>

                <div class="btnBox repaymentCompleteBox">
                    <a id="complete_btn" href="#none" title="메인으로 이동" target="_self" >서비스 시작하기</a>
                </div>

                <ul class="detailList">
                    <li>구독이 취소되더라도 구독 결제를 소급하여 환불하지 않으며 이전에 청구된 구독 요금은 취소일에 비례하여 계산되지 않습니다.<br>중도 해지시 환불은 없고, 다음 카드 결제일 전날까지 서비스 이용이 가능합니다.</li>
                    <li>첫 1달 Free 사용자의 경우 가입 해지 시 모든 서비스가 즉시 중지되니 유의해주세요.</li>
                    <li>구독 취소는 화면 상단의 내 이메일 계정 &gt; 결제내역 &gt; 구독해지 에서 바로 하실 수 있습니다.</li>
                </ul>
            </div>

        </div>
        <!-- //.content -->
    </section>
    <!-- //#container -->
<%--    <div class="loginWrap">--%>
<%--        <div class="login_box signup_box">--%>
<%--            <div class="txt_box">--%>

<%--                <span class="signup_desc animated fadeInDown delay-1 signup_complete">--%>
<%--					<strong>등록이 완료되었습니다!</strong>--%>
<%--                    <em style="font-weight:normal; font-size:18px;" id="register_noti">지금부터 1달 동안 무료 서비스를 이용하실 수 있으며,--%>
<%--                        <br>첫 결제일은 ${payDate } 입니다.</em>--%>
<%--				</span>--%>

<%--                <button type="button" id="complete_btn" class="input_done  animated flipInY delay-1" style="width:166px;">서비스 시작하기</button>--%>
<%--            </div>--%>

<%--            <div class="enroll_noti animated fadeIn delay-2">--%>
<%--                <img class="surprise_face" src="${pageContext.request.contextPath}/aiaas/kr/images/ico_alarm_after_enroll_2x.png" alt="구독해지안내 아이콘"/>--%>
<%--                <ul>--%>
<%--                    <li>구독이 취소되더라도 구독 결제를 소급하여 환불하지 않으며 이전에 청구된 구독 요금은 취소일에 비례하여 <br>계산되지 않습니다. 중도 해지시 환불은 없고, 다음 카드 결제일 전날까지 서비스 이용이 가능합니다.</li>--%>
<%--                    <li>첫 1달 Free 사용자의 경우 가입 해지 시 모든 서비스가 즉시 중지되니 유의해주세요.</li>--%>
<%--                    <li>구독 취소는 화면 상단의 내 이메일 계정 &gt; 결제내역 &gt; 구독 해지 에서 바로 하실 수 있습니다.</li>--%>
<%--                </ul>--%>
<%--            </div>--%>
<%--        </div>--%>

<%--    </div>--%>
    <!-- footer -->
    <%@ include file="../common/login_footer.jsp" %>
    <!-- //footer -->

</div>
<!-- //wrap -->

<script type="text/javascript">
    $(window).load(function() {
        $("#complete_btn").on('click',function(){
            // window.location.href="/login/krLoginMain";
            window.location.href="${btnUri}";
        });
    });

    /**
     * 2019. 09. 20  LYJ
     * 계정의 무료 체험 여부 조회
     * */
    var freeFlag = "${freeFlag }";

    if(freeFlag == 0) {
        $('#register_noti').html("마음 AI 서비스로 돌아오신 것을 환영합니다!<br>서비스를 마음껏 이용하세요.");
    }
</script>

