<%--
  Created by IntelliJ IDEA.
  User: YGE
  Date: 2019-07-12
  Time: 오후 8:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="../common/common_login_header.jsp" %>

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<!-- wrap -->
<div id="wrap" class="maumUI">
    <%@ include file="../common/login_header.jsp" %>

    <!-- #container -->
    <section id="container">
        <!-- .content -->
        <div class="content registrationContent">
            <div class="signUpCompleteWrap">
                <div class="paymentCompleteBox repaymentCompleteBox">
                    <h1>결제수단 등록이 완료 되었습니다.</h1>

                    <ul>
                        <li>
                            <span>
                                <em class="fas fa-chevron-right"></em>결제수단
                            </span>
                            <span>${payMethod }</span>
                        </li>
                        <li>
                            <span>
                                <em class="fas fa-chevron-right"></em>다음 결제일
                            </span>
                            <span>${payDate }</span>
                        </li>
                    </ul>
                </div>

                <div class="btn">
                    <a href="/user/paymentInfoMain">결제 정보 바로가기</a>
                    <a HREF="/">홈으로</a>
                </div>

                <ul class="detailList">
                    <li>구독이 취소되더라도 구독 결제를 소급하여 환불하지 않으며 이전에 청구된 구독 요금은 취소일에 비례하여 계산되지 않습니다.<br>중도 해지시 환불은 없고, 다음 카드 결제일 전날까지 서비스 이용이 가능합니다.</li>
                    <li>첫 1달 Free 사용자의 경우 가입 해지 시 모든 서비스가 즉시 중지되니 유의해주세요.</li>
                    <li>구독 취소는 화면 상단의 내 이메일 계정 &gt; 결제내역 &gt; 구독해지 에서 바로 하실 수 있습니다.</li>
                </ul>
            </div>

        </div>
        <!-- //.content -->
    </section>
    <!-- //#container -->

    <!-- footer -->
    <%@ include file="../common/login_footer.jsp" %>
    <!-- //footer -->

</div>
<!-- //wrap -->

<script type="text/javascript">

</script>

