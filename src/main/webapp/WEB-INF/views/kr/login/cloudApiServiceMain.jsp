<%--
  Created by IntelliJ IDEA.
  User: JHS
  Date: 2021-11-08
  Time: 오후 4:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ include file="../common/common_cloudapi_header.jsp" %>

<div id="pageldg">
    <span class="out_bg">
        <em>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <b>&nbsp;</b>
        </em>
    </span>
</div>

<!-- #wrap -->
<div id="wrap" class="maumUI visual_header">
    <%@ include file="../common/header.jsp" %>
    <!-- #container -->
    <div id="container">
        <!-- .intro -->
        <div class="stn intro">
            <div class="content">
                <div class="svc_txt">
                    <p class="tit">CLOUD API<br class="mobile"> SERVICE</p>
                    <p class="desc">음성, 언어, 시각 등 40여개의<br class="mobile"> 최첨단 AI 기술을 마음AI에서<br class="mobile"> 마음껏
                        활용해보세요.</p>
                    <a href="https://maum.ai/main/krMainHome" target="_blank" class="btn_primary">시작하기</a>
                    <a href="/home/orcttsMain" target="_blank" class="btn_primary">AI Builder(Beta)</a>
                </div>
            </div>
        </div>

        <!-- //.intro -->

        <!-- .service -->
        <div class="stn service">
            <div class="content">
                <ul class="service_list">
                    <li>
                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/ico_cloudApi_svc01.svg"
                             alt="icon">
                        <p>최첨단 인공지능 기술을 활용한<br class="pc"> <strong>다양한 서비스 구현</strong></p>
                    </li>
                    <li>
                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/ico_cloudApi_svc02.svg"
                             alt="icon">
                        <p>국내외의 기업에서 검증받은<br class="pc"> <strong>제품의 성능과 안정성</strong></p>
                    </li>
                    <li>
                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/ico_cloudApi_svc03.svg"
                             alt="icon">
                        <p>설계부터 도입까지<br class="pc"> <strong>무료 컨설팅 지원</strong></p>
                    </li>
                </ul>
            </div>
        </div>
        <!-- //.service -->

        <!-- .success -->
        <div class="stn success">
            <div class="content">
                <div class="titArea">
                    <p class="tit">Customer Success Stories</p>
                    <p class="desc">이미 수많은 기업이 마음AI와 함께 성공사례를 만들어가고 있습니다.</p>
                </div>


                <div class="success_list_wrap swiper-container">
                    <ul class="success_list swiper-wrapper">
                        <li class="swiper-slide">
                            <a href="#none">
                                <div class="logo">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/logo_incheonAirport.svg"
                                         alt="Incheon Airport logo">
                                </div>
                                <p class="tit">인천공항 관제센터 문자화 시스템</p>
                                <p class="desc">시각화 기술과 텍스트 분석 기술을 통해 관제사와 조종사 간 교신을 실시간으로 모니터링하고 전파 장애 지역에 대한 항공기
                                    안전
                                    모니터링을 실시간으로 지원합니다.</p>
                            </a>
                        </li>
                        <li class="swiper-slide">
                            <a href="#none">
                                <div class="logo">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/logo_hanaBank.svg"
                                         alt="Hana Bank logo">
                                </div>
                                <p class="tit">하나은행 하이(HAI) 뱅킹</p>
                                <p class="desc">언어와 시각의 최신 AI 기술을 활용해 말 한마디로 해외송금부터 공과금 납부까지 가능한 AI 금융 서비스 하이뱅킹을
                                    구축하였습니다.</p>
                            </a>
                        </li>
                        <li class="swiper-slide">
                            <a href="#none">
                                <div class="logo">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/logo_hyundaiMarine.svg"
                                         alt="Hyundai Marine logo">
                                </div>
                                <p class="tit">현대해상 AI 음성봇</p>
                                <p class="desc">실시간 STT 기술과 챗봇 기술을 접목해 AI 음성봇의 자동화 서비스를 통한 AI 상담원 서비스를 구현하였습니다.</p>
                            </a>
                        </li>
                        <li class="swiper-slide">
                            <a href="#none">
                                <div class="logo">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/logo_kbs.png"
                                         alt="KBS logo">
                                </div>
                                <p class="tit">KBS 시청자칼럼 방송</p>
                                <p class="desc">고품질의 TTS 음성생성 기술로 실제 사람과 같이 자연스럽고 매끄러운 AI 보이스를 만들어내어 방송을 진행합니다.</p>
                            </a>
                        </li>
                        <li class="swiper-slide">
                            <a href="#none">
                                <div class="logo">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/logo_posco.svg"
                                         alt="POSCO logo">
                                </div>
                                <p class="tit">포스코 스마트 팩토리</p>
                                <p class="desc">생산성 및 비용 절감을 위해 AI 스마트팩토리의 실시간 공장 모니터링 기술을 도입해 제조공정의 경쟁력을
                                    향상시켰습니다.</p>
                            </a>
                        </li>
                        <li class="swiper-slide">
                            <a href="#none">
                                <div class="logo">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/logo_seoul.svg"
                                         alt="Seoul logo">
                                </div>
                                <p class="tit">서울시 딥러닝 차량 인식</p>
                                <p class="desc">차량 인식 솔루션과 영상 분석 시스템을 통해 서울시의 노후 경유차 통행량을 30% 감소시켰습니다.</p>
                            </a>
                        </li>
                        <li class="swiper-slide">
                            <a href="#none">
                                <div class="logo">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/logo_suwon.svg"
                                         alt="Suwon logo">
                                </div>
                                <p class="tit">수원시 이상행동 CCTV</p>
                                <p class="desc">AI 이상행동 자동 검출 분석 시스템을 도입해 도시의 안전과 범죄 검거율을 향상시켰습니다.</p>
                            </a>
                        </li>
                        <li class="swiper-slide">
                            <a href="#none">
                                <div class="logo">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/logo_gyeongnam.svg"
                                         alt="Gyeongnam logo">
                                </div>
                                <p class="tit">경상남도청 이동형 AI 회의록</p>
                                <p class="desc">업무 효율성을 높이기 위해 공용량 / 다접속이 가능한 무선마이크에 고성능 AI 회의록을 적용하였습니다.</p>
                            </a>
                        </li>
                        <li class="swiper-slide">
                            <a href="#none">
                                <div class="logo">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/logo_ulsan.png"
                                         alt="Ulsan logo">
                                </div>
                                <p class="tit">울주군 이동형 On Device AI 회의록</p>
                                <p class="desc">스마트 마이크로 구성되어 휴대성이 높고 실시간으로 문서화되어 손쉽게 회의록 작업이 가능한 AI 회의록을
                                    구현하였습니다.</p>
                            </a>
                        </li>
                        <li class="shortcut swiper-slide">
                            <a href="https://play.google.com/store/apps/details?id=com.linkhous.homebutton"
                               target="_blank" title="새창으로 열기">
                                <div class="logo">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/logo_homebutton.png"
                                         alt="Homebutton logo">
                                </div>
                                <p class="tit">홈버튼챗 민원관리 챗봇</p>
                                <p class="desc">Bert 모델을 활용하여 4개국어로 구성된 임대차인 민원관리 챗봇 서비스를 제공합니다.</p>
                                <span class="svc_link">서비스 바로가기 <em class="fas fa-angle-double-right"></em></span>
                            </a>
                        </li>
                        <li class="shortcut swiper-slide">
                            <a href="https://play.google.com/store/apps/details?id=com.dkbooktalk.dkbooktalk_app"
                               target="_blank" title="새창으로 열기">
                                <div class="logo">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/logo_daekyo.png"
                                         alt="Daekyo logo">
                                </div>
                                <p class="tit">대교 상상키즈 모바일 APP</p>
                                <p class="desc">유아용 상상 놀이터 APP 서비스에서 배우 김강훈이 읽어주는 AI 소리동화를 제공합니다.</p>
                                <span class="svc_link">서비스 바로가기 <em class="fas fa-angle-double-right"></em></span>
                            </a>
                        </li>
                        <li class="shortcut swiper-slide">
                            <a href="https://www.samsungenglish.com/" target="_blank" title="새창으로 열기">
                                <div class="logo">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/logo_samsungPublishingCo.png"
                                         alt="Samsung Publishing Co. logo">
                                </div>
                                <p class="tit">삼성출판사 Selena 인공지능 선생님</p>
                                <p class="desc">원어민과 1:1 수업 효과를 내는 인공지능 원어민 선생님 모델을 제공합니다.</p>
                                <span class="svc_link">서비스 바로가기 <em class="fas fa-angle-double-right"></em></span>
                            </a>
                        </li>
                        <li class="shortcut swiper-slide">
                            <a href="https://twip.kr/" target="_blank" title="새창으로 열기">
                                <div class="logo">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/logo_ejn.png"
                                         alt="EJN logo" style="height:20px;">
                                </div>
                                <p class="tit">TWIP 음성 합성 후원 플랫폼</p>
                                <p class="desc">인공지능 음성 합성 후원 플랫폼 TWIP에 개인방송 플랫폼 유저와 스트리머 간의 소통을 위한 TTS 서비스를
                                    제공합니다.</p>
                                <span class="svc_link">서비스 바로가기 <em class="fas fa-angle-double-right"></em></span>
                            </a>
                        </li>
                        <li class="shortcut swiper-slide">
                            <a href="https://play.google.com/store/apps/details?id=com.Archipin.Epin.sba&hl=ko"
                               target="_blank" title="새창으로 열기">
                                <div class="logo">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/logo_archipin.png"
                                         alt="Archipin logo">
                                </div>
                                <p class="tit">스피킹 버스 어린이 영어 교육 게임</p>
                                <p class="desc">영어 게임 내 캐릭터가 영어로 말할 수 있도록 TTS 서비스를 제공합니다.</p>
                                <span class="svc_link">서비스 바로가기 <em class="fas fa-angle-double-right"></em></span>
                            </a>
                        </li>
                        <li class="shortcut swiper-slide">
                            <a href="https://mayfield.co.kr/2017/kor/html/index/" target="_blank" title="새창으로 열기">
                                <div class="logo">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/logo_redTie.png"
                                         alt="RedTie logo" style="height:20px;">
                                </div>
                                <p class="tit">레드타이 AI 다국어 호텔챗봇</p>
                                <p class="desc">인공지능 기반의 4개국어가 가능한 호텔 챗봇 서비스를 제공합니다.</p>
                                <span class="svc_link">서비스 바로가기 <em class="fas fa-angle-double-right"></em></span>
                            </a>
                        </li>
                    </ul>
                    <div class="swiper-pagination"></div>
                </div>
            </div>
        </div>
        <!-- //.success -->

        <!-- .engines -->
        <div class="stn engines">
            <div class="content">
                <div class="titArea">
                    <p class="tit">cloud api engines</p>
                    <p class="desc">음성, 시각, 언어 등 40여개의 인공지능 엔진을 제공하는 구독형 API 서비스입니다.<br> 최신 AI 기술을 가장 신속하게 제품화하여
                        비즈니스에
                        연동 가능하며, 사용량을 실시간으로 체크할 수 있습니다.(API 메뉴얼 제공)</p>
                </div>

                <div class="engines_list_wrap swiper-container swiper-container-initialized swiper-container-horizontal swiper-container-multirow">
                    <ul class="engines_list swiper-wrapper"
                        style="width: 2496px; transform: translate3d(0px, 0px, 0px);">
                        <li class="vis swiper-slide swiper-slide-active" data-swiper-column="0" data-swiper-row="0"
                            style="width: 624px;">
                            <span class="imgBox img_vis_lsa">상품이미지</span>
                            <span class="eg_info">
								<em class="ico">
									<img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_lipSyncAvatar_w.svg"
                                         alt="아이콘">
								</em>
								<em class="name">
									<strong>Lip Sync Avatar</strong>
									Lip Sync Avatar
								</em>
								<em class="desc">입력한 텍스트에 따라 자연스럽게 말하는 아바타가 생성됩니다.</em>
							</span>
                        </li>
                        <li class="spe swiper-slide swiper-slide-next" data-swiper-column="1" data-swiper-row="0"
                            style="width: 624px;">
							<span class="point">
								<em class="best">BEST</em>
							</span>
                            <span class="imgBox img_spe_tts">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img
                                        src="${pageContext.request.contextPath}/aiaas/kr/images/ico_spe_1_fold_w.svg"
                                        alt="아이콘"></em>
								<em class="name">
									<strong>음성생성</strong>
									Text-to-Speech
								</em>
								<em class="desc">실제 그 사람의 목소리 그대로 자연스럽게, 세계 최고 수준의 음질과 실시간 합성 속도를 제공합니다.</em>
							</span>
                        </li>
                        <li class="spe swiper-slide" data-swiper-column="2" data-swiper-row="0"
                            style="width: 624px;">
							<span class="point">
								<em class="best">BEST</em>
							</span>
                            <span class="imgBox img_spe_stt">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img
                                        src="${pageContext.request.contextPath}/aiaas/kr/images/ico_spe_2_fold_w.svg"
                                        alt="아이콘"></em>
								<em class="name">
									<strong>음성인식</strong>
									Speech-to-Text
								</em>
								<em class="desc">음성을 텍스트로 변환하는 엔진으로, 다양한 학습모델을 활용할 수 있고 높은 인식률과 빠른 처리 속도를 제공합니다.</em>
							</span>
                        </li>
                        <li class="vis swiper-slide" data-swiper-column="3" data-swiper-row="0"
                            style="width: 624px;">
							<span class="point">
								<em>NEW</em>
							</span>
                            <span class="imgBox img_vis_litrDtc">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img
                                        src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_loiteringDetect_w.svg"
                                        alt="아이콘"></em>
								<em class="name">
									<strong>배회 감지</strong>
									Loitering Detection
								</em>
								<em class="desc">영역을 지정하여 해당 영역을 누군가가 장시간 배회할 시 알려줍니다.</em>
							</span>
                        </li>
                        <li class="vis swiper-slide" data-swiper-column="0" data-swiper-row="1"
                            style="margin-top: 0px; width: 624px;">
							<span class="point">
								<em>NEW</em>
							</span>
                            <span class="imgBox img_vis_fdDtc">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img
                                        src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_fallDownDetect_w.svg"
                                        alt="아이콘"></em>
								<em class="name">
									<strong>실신 감지</strong>
									Fall Down Detection
								</em>
								<em class="desc">특정 지역에서 실신하는 사람을 인식합니다.</em>
							</span>
                        </li>
                        <li class="vis swiper-slide" data-swiper-column="1" data-swiper-row="1"
                            style="margin-top: 0px; width: 624px;">
							<span class="point">
								<em>NEW</em>
							</span>
                            <span class="imgBox img_vis_itrsDtc">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img
                                        src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_IntrusionDetect_w.svg"
                                        alt="아이콘"></em>
								<em class="name">
									<strong>침입 감지</strong>
									Intrusion Detection
								</em>
								<em class="desc">영역을 지정하여 해당 영역을 누군가가 침입할 시 알려줍니다.</em>
							</span>
                        </li>
                        <li class="lang swiper-slide" data-swiper-column="2" data-swiper-row="1"
                            style="margin-top: 0px; width: 624px;">
							<span class="point">
								<em>NEW</em>
							</span>
                            <span class="imgBox img_lang_sts">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img
                                        src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_txtStyTrans_w.svg"
                                        alt="아이콘"></em>
								<em class="name">
									<strong>스타일 변환</strong>
									Text Style Transfer
								</em>
								<em class="desc">문장을 원하는 스타일로 변경하는 동시에 의미는 일관성 있게 전달해줍니다.</em>
							</span>
                        </li>
                        <li class="lang swiper-slide" data-swiper-column="3" data-swiper-row="1"
                            style="margin-top: 0px; width: 624px;">
                            <span class="imgBox img_lang_itf">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img
                                        src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_itf_w.svg"
                                        alt="아이콘"></em>
								<em class="name">
									<strong>의도 분류</strong>
									ITF(Intent Finder)
								</em>
								<em class="desc">입력된 질문의 의도를 파악하여 알려줍니다.</em>
							</span>
                        </li>
                    </ul>
                    <div class="swiper-pagination swiper-pagination-clickable swiper-pagination-bullets"><span
                            class="swiper-pagination-bullet swiper-pagination-bullet-active" tabindex="0"
                            role="button"
                            aria-label="Go to slide 1"></span><span class="swiper-pagination-bullet" tabindex="0"
                                                                    role="button"
                                                                    aria-label="Go to slide 2"></span><span
                            class="swiper-pagination-bullet" tabindex="0" role="button"
                            aria-label="Go to slide 3"></span><span class="swiper-pagination-bullet" tabindex="0"
                                                                    role="button" aria-label="Go to slide 4"></span>
                    </div>
                    <span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span></div>
            </div>
        </div>
        <!-- //.engines -->

        <!-- .human_builder -->
        <div class="stn human_builder">
            <div class="content">
                <div class="titArea fr">
                    <p class="tit">AI Human Builder</p>
                    <p class="desc">인공지능 엔진들을 내 마음대로 연결하고 테스트해 볼 수 있는 AI Human Builder입니다.<br> maum.ai 플랫폼 내 엔진들을
                        자유롭게
                        조합하여 고객이 원하는 서비스에 바로, 쉽게 적용할 수 있습니다.</p>
                </div>
                <div class="imgBox fl">
                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/cloudApi_landing/img_humanBuilder_cloud.svg"
                         alt="AI HUMAN BUILDER image">
                </div>
                <a href="https://builder.maum.ai/landing" target="_blank" title="새창으로 열기"
                   class="btn_primary blue">시작하기</a>
            </div>
        </div>
        <!-- //.human_builder -->

        <!-- .contact -->
        <div class="stn contact">
            <div class="content">
                <div class="titArea">
                    <p class="tit">Contact us</p>
                    <p class="desc">이미 많은 기업들이 마음AI를 통해 인공지능을 성공적으로 도입했습니다.<br> 상담 문의를 남겨주시면 담당 컨설턴트가 빠른 시일 내로 연락을
                        드리겠습니다.</p>
                </div>

                <div class="contactform">
                    <div class="required_group">
                        <dl>
                            <dt>이름<span>(필수)</span></dt>
                            <dd><input type="text" id="userName" class="ipt_txt" autocomplete="off"></dd>
                        </dl>
                        <dl>
                            <dt>연락처<span>(필수)</span></dt>
                            <dd><input type="tel" id="userTel" class="ipt_tel" pattern="[0-9]{3}-[0-9]{4}-[0-9]{4}">
                            </dd>
                        </dl>
                        <dl>
                            <dt>이메일<span>(필수)</span></dt>
                            <dd><input type="text" id="userEmail" class="ipt_email"></dd>
                        </dl>
                    </div>
                    <dl>
                        <dt>문의<span>(선택)</span></dt>
                        <dd><textarea class="textarea" id="usertxt"></textarea></dd>
                    </dl>
                    <dl>
                        <dt class="hide">개인정보 수집동의</dt>
                        <dd>
                            <div class="assentBox">
                                <div class="checkBox">
                                    <input type="checkbox" name="assent" id="assent">
                                    <label for="assent">개인 정보 수집 동의</label>
                                </div>
                                <p>
                                    수집하는 개인정보 항목: 이름, 연락처<br>
                                    작성해주시는 개인 정보는 문의 접수 및 고객 불만 해결을 위해 1년간 보관됩니다.<br>
                                    본 동의를 거부할 수 있으나, 미동의 시 문의 접수가 불가능합니다.
                                </p>
                            </div>
                            <p>전문 상담사가 영업일 기준 3일 이내 개별적으로 연락드립니다.</p>
                        </dd>
                    </dl>
                    <button type="button" id="inquireBtn" class="btn_primary">문의하기</button>
                </div>
            </div>
        </div>
        <!-- //.contact -->
    </div>
    <!-- //#container -->
    <!-- footer -->
        <%@ include file="../common/footerLanding.jsp" %>
    <!-- //footer -->
</div>
    <!-- //#wrap -->
    <!---------------------- Local Resources ---------------------->
    <!-- Local CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/swiper-4.5.1.min.css"/>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/cloudApi_service_main.css">

    <!-- Local Script Library -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/swiper-4.5.1.min.js"></script>

    <script type="text/javascript">

        function linkExtService(site_url) {
            var newWindow = window.open('about:blank');
            newWindow.location.href = "${pageContext.request.contextPath}/login/linkExtService?serviceUrl=" + site_url;
        }

    </script>


    <!-- AMR 공통으로 적용되는 script -->
    <script type="text/javascript">
        // swiper
        var swiperSuccess = new Swiper('.success_list_wrap', {
            initialSlide: 0,
            slidesPerGroup: 1,
            slidesPerView: 4,
            slidesPerColumn: 4,
            slidesPerColumnFill: 'row',
            // spaceBetween: 0,
            allowSlideNext: false,
            allowSlidePrev: false,
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },
            breakpoints: {
                768: {
                    slidesPerView: 1,
                    slidesPerColumn: 2,
                    // spaceBetween: 0,
                    allowSlideNext: true,
                    allowSlidePrev: true,
                }
            }
        });

        var swiperEngines = new Swiper('.engines_list_wrap', {
            slidesPerGroup: 1,
            slidesPerView: 4,
            slidesPerColumn: 2,
            slidesPerColumnFill: 'row',
            allowSlideNext: false,
            allowSlidePrev: false,
            pagination: {
                el: '.swiper-pagination',
                clickable: true
            },
            breakpoints: {
                768: {
                    slidesPerView: 1,
                    allowSlideNext: true,
                    allowSlidePrev: true,
                }
            }
        });

        $(window).resize(function () {
            swiperSuccess.update();
            swiperEngines.update();
        });

        // fixed된 header에 가로스크롤을 적용하기 위한 코드
        $(window).scroll(function () {
            $('.maumUI.transform .maum_sta').css('left', 0 - $(this).scrollLeft());
        });

        // #aside_btns 페이지 위로 버튼 display 이벤트
        $(window).scroll(function () {
            var scrollLocate = $(window).scrollTop();
            if (scrollLocate > 50) {
                $('#header').addClass('transform');
                $('#aside_btns .page_top').css('display', 'block');
            }
            if (scrollLocate < 50) {
                $('#header').removeClass('transform');
                $('#aside_btns .page_top').css('display', 'none');
            }
        });

        // 최상단 배너 닫기 버튼 클릭 이벤트
        $('.btn_banner_close').on('click', function () {
            var $banner = $(this).parents('.top_banner_area');
            $banner.remove();
            $('#wrap').removeClass('top_banner');
        });

        // page top 버튼 클릭 이벤트
        $('#aside_btns .page_top').on('click', function () {
            console.log('top click')

            $('html, body').animate({
                scrollTop: 0
            }, 500);
        });
    </script>
    <!-- AMR //공통으로 적용되는 script -->
    <script>
        // 문의하기 연락처 숫자입력 제한
        $('#userTel').on('keyup', function () {
            $(this).val($(this).val().replace(/[^0-9]/g, ''));
        });

        // // 문의하기 필수 내용 입력, 동의체크 시 버튼 컬러 활성화
        // $('.ipt_txt').on('input', checkInput);
        // $('.ipt_tel').on('input', checkInput);
        // $('.ipt_email').on('input', checkInput);
        // $('#assent').on('click', checkInput);
        //
        // function checkInput() {
        //     var userName = $('.ipt_txt').val();
        //     var userTel = $('.ipt_tel').val();
        //     var userEmail = $('.ipt_email').val();
        //     var assentCheck = $('#assent')
        //     var inquireButton = $('#inquireBtn');
        //
        //     if (userName == '' || userTel == '' || assentCheck.is(':checked') == false) {
        //         inquireButton.prop('disabled', true);
        //     } else {
        //         inquireButton.prop('disabled', false);
        //     }
        // }

        //문의하기 메일 발송 2021.03.11 SMR
        $('#inquireBtn').on('click', function () {
            var type = $('input[type=radio]:checked').val();
            var agreeCheck = $('input[type=checkbox]:checked').is(':checked');
            var checkText;

            agreeCheck ? checkText = '동의' : checkText = '비동의'

            let title = "[Cloud Api] " + $("#userName").val() + "님의 문의 사항입니다.";

            let msg = "이름 : " + $("#userName").val()
                + "<br>연락처 : " + $("#userTel").val()
                + "<br>이메일 : " + $("#userEmail").val()
                + "<br>문의 내용 : " + $("#usertxt").val()
                + "<br>개인 정보 수신 동의 : " + checkText;

            let formData = new FormData();

            formData.append('fromaddr', 'CloudApiService');
            formData.append('toaddr', 'hello@mindslab.ai');
            formData.append('subject', title);
            formData.append('message', msg);

            $.ajax({
                type: 'POST',
                async: true,
                url: '/support/sendContactMail',
                dataType: 'text', //서버가 응답할 때 보내줄 데이터 타입
                data: formData,
                processData: false, //query string false
                contentType: false, // multipart/form-data 설정
                success: function () {
                    $("#userName").val('');
                    $("#userTel").val('');
                    $("#userEmail").val('');
                    $("#usertxt").val('');

                    alert('문의가 완료되었습니다.')
                    window.location.href = '../../../..';
                },

                error: function (xhr, status, error) {
                    console.log("error", error);
                    alert("문의 요청이 실패되었습니다. 잠시 후에 다시 시도해 주세요.");
                }
            });
        });
    </script>
