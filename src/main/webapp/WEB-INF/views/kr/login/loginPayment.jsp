<%--
  Created by IntelliJ IDEA.
  User: YGE
  Date: 2019-07-12
  Time: 오후 8:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="../common/common_login_header.jsp" %>

<c:set var="flag" value="${requestScope['checkFlag']}"/>
<c:choose>
    <c:when test="${fn:containsIgnoreCase(flag, 'Pay')}">
        <c:import url="/payment/billingForm?method=billingPay"/>
    </c:when>
    <c:otherwise>
        <c:import url="/payment/billingForm?method=billingFree"/>
    </c:otherwise>
</c:choose>


<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_notice">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-exclamation-triangle" style="color:#9cafd2;"></em>
            <h5 style="color: #33516b;;font-size:20px;font-weight: 400;margin: 10px;">서비스 이용 중지 안내</h5>
            <p>이용 기간이 <strong>만료</strong>되어 <strong>서비스 사용이 중지</strong>되었습니다.<br>
                <strong>카드를 재등록</strong>하여 maum.ai 서비스를 이용해주세요. </p>
        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <button class="btn_blue">확인</button>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->


<div id="wrap">
    <!-- header -->
    <%@ include file="../common/login_header.jsp" %>
    <!-- //header -->
    <!-- #container -->
    <section id="container">
        <!-- .content -->
        <div class="content registrationContent">
            <div class="signUpCompleteWrap">
                <div class="signUpConfirmBox">
                    <p>회원가입이 완료 되었습니다.</p>
                    <ul>
                        <li><span><em class="fas fa-chevron-right"></em>이름</span><span id="userName" class="user">${fn:escapeXml(sessionScope.accessUser.name)}</span></li>
                        <li><span><em class="fas fa-chevron-right"></em>이메일</span><span id="userEmail" class="user">${fn:escapeXml(sessionScope.accessUser.email)}</span></li>
                    </ul>
                </div>

                <div class="txt" id="txt">마음 AI 서비스 이용을 위해서는 결제 수단을 등록 하셔야 서비스 이용이 가능합니다.<br>
                    체험을 위해서 모든 고객은 <strong>첫달 무료</strong>이며, 결제수단을 등록해도 1달 동안 요금이 청구되지 않습니다.<br>
                    결제수단을 등록하여 더욱 편리하게 서비스를 이용해보세요.
                </div>

                <div class="signUpCompleteBox">
                    <h1 id="title">첫 1달 무료!</h1>
                    <p id="payment_noti">결제 수단을 등록해도 1달 동안 요금이 청구되지 않습니다.</p>
                    <a id="businessBilling" class="registration" href="#none" title="결제정보 등록하기" style="margin-bottom:0;">결제수단 등록하기</a>
                    <p class="note">*해외 카드 등록 시, <a class="ft_point_orange" href="/login/enPayment">여기를 눌러</a> 결제를 진행해주세요.</p>
                </div>
                <span id="payment_noti2">해지 후, 재등록 시 바로 요금이 결제됩니다.</span>
                <div class="btnBox">
                    <a class="btn_sign" href="#none" title="로그인"> 로그인</a>
                    <a id="serviceMainBtn" href="/" title="메인으로 이동" target="_self">메인으로 이동</a>
                </div>
            </div>

        </div>
        <!-- //.content -->
    </section>
    <!-- //#container -->
    <!-- footer -->
    <%@ include file="../common/login_footer.jsp" %>
    <!-- //footer -->
</div>
<!-- //#wrap -->
<script>
    $(window).load(function() {
        // 팝업창 닫기
        $('.pop_close, .pop_bg, .btn button').on('click', function () {
            $('.pop_simple').fadeOut(300);
            $('body').css({
                'overflow': ''
            });
        });
    });
    /**
     * 2019. 09. 19  LYJ
     * 계정의 무료 체험 여부 조회
     * */
    $(document).ready(function(){
        let $ClientId = "${client_id}";
        let $RedirectUri = "${redirect_uri}";
        let $SsoUrl = "${sso_url}";
        //해더 로그인 버튼
        $('.btn_sign').on('click',function(){
            var stateVal = uuidv4();
            location.href = $SsoUrl+"/maum/oauthLoginMain" + "?response_type=code&client_id=" + $ClientId + "&redirect_uri=" + encodeURIComponent($RedirectUri);
        });
        function uuidv4() {
            return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
                (c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
            );
        }
        var freeFlag = "${freeFlag}";

        if (freeFlag == null) {
            alert("서버와의 통신이 원활하지 않습니다.");
        } else {
            if (freeFlag >= 1) {
                $('.signUpConfirmBox').hide();
                $('#txt').hide();
                $('#title').text('결제 등록 하기');
                $('.btn_sign').hide();
                $('#payment_noti').html("마음 AI 서비스로 돌아오신 것을 환영합니다.<br>결제수단을 등록하시어 더욱 편리하게 서비스를 이용해보세요.");
                $('#payment_noti2').text("해지 후, 재등록 시 바로 요금이 결제됩니다.");
            }
        }

    });

    function setParam(price, goodName, sign, callback) {
        $("#billingPrice").val(price);
        $("#billingGoodName").val(goodName);
        $("#billingSignature").val(sign);
        console.dir($("#billingForm"));
        // planConfirm(goodName, callback);
        callback();
    }
    function payFunc() {
        INIStdPay.pay('billingForm');
    }

    $("#businessBilling").click(function() {
        if(isMobile()) {
            // alert('모바일 결재');
            location.href = "/payment/mobilePay?product=3&goodname=BUSINESS&price=99000&username=";
        }
        else setParam("99000", "BUSINESS", $("#businessSignature").val(), payFunc);

    });

    function isMobile() {
        var filter = "win16|win32|win64|mac|macintel";
        if ( navigator.platform ) {
            if ( filter.indexOf( navigator.platform.toLowerCase() ) < 0 ) {
                return true;
            } else {
                return false;
            }
        }
    }
</script>

