<%--
  Created by IntelliJ IDEA.
  User: bliss
  Date: 2020-08-20
  Time: 오후 4:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>
<html>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
    <title>maum.ai platform</title>
    <!-- resources -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/reset.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/font.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/login.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- //resources -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-1.11.0.min.js"></script>

</head>
<body>
<div id="wrap">
    <header>
        <a href="https://maum.ai/" title="마음에이아이" target="_self"><img src="${pageContext.request.contextPath}/aiaas/common/images/maumai.svg" alt="maum.ai logo" /></a>
    </header>
    <section id="container">
        <div class="content registrationContent">
            <div class="signUpCompleteWrap">
                <p class="regist_noti">마음 AI 서비스 이용을 위해서는 결제 수단을 등록 하셔야 서비스 이용이 가능합니다.<br>
                    체험을 위해서 모든 고객은 <strong>첫달 무료</strong>이며, 결제수단을 등록해도 1달 동안 요금이 청구되지 않습니다.<br>
                결제수단을 등록하여 더욱 편리하게 서비스를 이용해보세요.</p>
                <div class="signUpCompleteBox">
                    <h1>첫 1달 무료!</h1>
                    <p>결제 수단을 등록해도 1달 동안 요금이 청구되지 않습니다.</p>
                    <a class="registration" href="#none" title="결제정보 등록하기">결제수단 등록하기</a>
                </div>
            </div>
            <div class="paymentConfirmWrap ">
                <div class="paymentConfirmBox">
                    <h5>결제수단 등록이 완료 되었습니다.</h5>
                    <p>마음 AI의 모든 서비스를 마음껏 이용하세요.</p>
                </div>
                <div class="btn">
                    <a href="/home/login">로그인</a>
                    <a HREF="/">메인으로 이동</a>
                </div>
            </div>
        </div>
    </section>
    <footer>
        <div class="footer">
            <a href="#none">이용약관 </a>  ㅣ
            <a href="#none"> 개인정보처리방침 </a>  ㅣ
            <a href="#none"> 고객센터 </a> \
            <span>ㅣ</span>
            <div class="lang">
                <span class="lang_select">한국어 <em class="fas fa-chevron-up"></em></span>
                <ul>
                    <li class="active">한국어</li>
                    <li><a href="#none">English</a></li>
                </ul>
            </div>
        </div>
        <p>Copyright © 2020 주식회사 마인즈랩. All rights reserved.</p>
        <div class="m_lang">한국어 <a href="" title="English">English</a></div>
    </footer>
</div>
<script>

    jQuery.event.add(window,"load",function() {
        $(document).ready(function () {

            //결제 카드 등록하기 버튼
            $('.registration').on('click', function(){
                $('.signUpCompleteWrap').hide();
                $('.paymentConfirmWrap').fadeIn();
            });

            //footer 언어 체크
            $('.lang_select').on('click', function(){
                $(this).next().toggleClass('active');
            })

        });
    });
</script>
</body>
</html>
