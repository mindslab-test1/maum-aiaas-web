<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-06-02
  Time: 10:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<%@ include file="../common/common_header.jsp" %>


<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<!-- .lyr_info -->
<div class="lyr_info">
    <div class="lyr_plan_bg"></div>
    <!-- .productWrap -->
    <div class="lyrWrap">
        <button class="btn_lyrWrap_close" type="button">닫기</button>
        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <em class="far fa-address-card"></em>
            <p>고객 정보 입력</p>
            <ul>
                <li>
                    <label for="info_name">*이름</label>
                    <input id="info_name" type="text" value="" placeholder="필수 입력 사항">
                </li>
                <li>
                    <label for="info_phone">*연락처</label>
                    <input id="info_phone" type="tel" value="" placeholder="필수 입력 사항" pattern="[0-9]{3}-[0-9]{4}-[0-9]{4}">
                </li>
                <li>
                    <label for="info_email">*이메일</label>
                    <input id="info_email" type="email" value="" placeholder="필수 입력 사항">
                </li>
                <li>
                    <label for="info_company">*회사명/직급</label>
                    <input id="info_company" type="text" value="" placeholder="필수 입력 사항">
                </li>
            </ul>
            <button class="btn_blue" id="downloadPDF">다운로드</button>
        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.productWrap -->
</div>
<!-- //.lyr_info -->

<!-- wrap -->
<div id="wrap" class="maumUI">
    <%@ include file="../common/header.jsp" %>
    <div class="loginWrap">

        <div class="academy_top ecomins_top">
            <div class="academy_top_area ">
                <h1>함께 만드는 인공지능 생태계</h1>
                <img src="${pageContext.request.contextPath}/aiaas/common/images/logo_ecominds.svg" alt="에코마인즈 로고" />
                <div class="tab_menu ">
                    <ul class="tab_list">
<%--                        <li class="tab_tit active"><button type="button" class="ecominds">에코마인즈란?</button></li>--%>
                        <li class="tab_tit active"><button type="button" class="familyCo">성공 사례</button></li>
                        <li class="tab_tit"><button type="button" class="benefits">파트너쉽 혜택</button></li>
                    </ul>
                </div>
            </div>
        </div>

<%--        <div class="academy_main ecominds_main" id="ecominds">--%>
<%--            <div class="ecominds_box">--%>
<%--                <p>AI 알고리즘과 엔진 등 기술 전반에서부터 AI 응용서비스까지 AI 유관 영역마다 여러 스타트업 및 연구기관과 협업하는 파트너십 프로젝트입니다. </p>--%>
<%--                <a href="#none" class="down_link" title="소개자료 다운로드">소개자료 다운로드</a>--%>

<%--                <div class="consultant">--%>
<%--                    <h4>주요 컨설팅 사업 분야</h4>--%>
<%--                    <span>AI Consultant</span>--%>
<%--                    <ul>--%>
<%--                        <li>--%>
<%--                            <h5>SaaS 챗봇 사업<span>비즈니스에 활용 가능한 챗봇 엔진</span></h5>--%>
<%--                            <p>비즈니스에 활용 가능한 챗봇 엔진 및 어플리케이션 개발<br>--%>
<%--                                고객사 비즈니스를 위한 특화된 챗봇 플랫폼 개발</p>--%>
<%--                        </li>--%>
<%--                        <li>--%>
<%--                            <h5>FAST 대화형 AI 사업<span>음성봇을 통한 상담업무 효율화</span></h5>--%>
<%--                            <p>고객별 비즈니스 모델에 따른 맞춤형 컨설팅 진행<br>--%>
<%--                                다양한 AI엔진을 접목한 음성봇 어플리케이션 개발</p>--%>
<%--                        </li>--%>
<%--                        <li>--%>
<%--                            <h5>AI 알고리즘/어플리케이션 사업<span>인공지능을 활용한 비즈니스 모델 확립</span></h5>--%>
<%--                            <p>고객의 비즈니스 모델 컨설팅부터 알고리즘,<br>--%>
<%--                                서비스 개발까지 모든 비즈니스 파트너십 서비스 제공--%>
<%--                            </p>--%>
<%--                        </li>--%>
<%--                        <li>--%>
<%--                            <h5>클라우드 회의록 사업<span>사내/국제 회의록 자동 작성</span></h5>--%>
<%--                            <p>공공기관, 의료기관, 상담센터 등의 회의록 및<br>--%>
<%--                                조사/상담 내용은 녹취하여 보고서 형태로 보관하고,<br>--%>
<%--                                수정/편집이 가능한 시스템을 납품</p>--%>
<%--                        </li>--%>
<%--                        <li>--%>
<%--                            <h5>데이터 사업<span>AI 학습을 위한 정제된 데이터</span></h5>--%>
<%--                            <p>AI 인공지능 엔진에 특화 된 학습용 데이터 에딧툴<br>--%>
<%--                                개발 / 오픈 데이터 포탈 운영</p>--%>
<%--                        </li>--%>
<%--                        <li>--%>
<%--                            <h5>공공 사업<span>자동 회의록 작성 시스템</span></h5>--%>
<%--                            <p>공공사업분야 maum 회의록 파트너 발굴 및 영업</p>--%>
<%--                        </li>--%>
<%--                    </ul>--%>
<%--&lt;%&ndash;                    <a href="#inquiry" title="가입 문의하기">ecoMINDs 가입 문의하기</a>&ndash;%&gt;--%>
<%--                </div>--%>

<%--            </div>--%>
<%--        </div>--%>

        <div class="academy_main ecominds_main" id="benefits">
            <div class="benefits_box">
                <p>AI 알고리즘과 엔진 등 기술 전반에서부터 AI 응용서비스까지 AI 유관 영역마다 여러 스타트업 및 연구기관과 협업하는 파트너십 프로젝트입니다.<br>
                    마인즈랩과 함께 인공지능 생태계, ecoMINDs 안에서 Win-Win할 스타트업 파트너사를 모집합니다.</p>
                <div class="benefit_list">
                    <div>
                        <h4>파트너사에게 드리는 혜택</h4>
                        <ul>
                            <li><strong>기술지원</strong>
                                최신 인공지능 기술 트렌드 공유 및 공동 프로젝트 수행
                            </li>
                            <li><strong>경영지원</strong>
                                경영지원 업무 및 영업, 홍보 마케팅 등 지원
                            </li>
                            <li><strong>교육 프로그램 지원</strong>
                                인공지능 활용 방법 및 알고리즘, 데이터 교육 세션
                            </li>
                            <li><strong>오피스 지원</strong>
                                판교테크노벨리 내 코워킹 스페이스 활용(임차료 발생)
                            </li>
                            <li><strong>투자 지원</strong>
                                자금 확보 방안 제시 및 팁스(Tips) 등 투자유치 지원
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="scope">
                    <h4>maum.ai 기술지원 범위</h4>
                    <dl>
                        <dt><em class="fas fa-chevron-right"></em>사전 학습된 엔진</dt>
                        <dd>maum.ai에 공개된 최신 30여 개의 인공지능 엔진 제공</dd>
                    </dl>
                    <div class="stn_landing_cont">
                        <div class="item_set bg_blue">
                            <div class="item_cont">
                                <ul class="item_lst">
                                    <li>
                                        <div class="">
                                            <dl class="blue">
                                                <dt>
                                                    <em>음성</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico-spe-1-fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>음성 생성</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_2_fold.svg" class="img_m" alt="백그라운드">
                                                            </div>
                                                            <em>음성 인식</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_3_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>음성 정제</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_5_fold.svg"  alt="백그라운드">
                                                            </div>
                                                            <em>Voice Filter</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_6_fold_.svg"  alt="백그라운드">
                                                            </div>
                                                            <em>화자 인증</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="inner_box">
                                            <dl class="blue">
                                                <dt>
                                                    <em>시각</em>
                                                </dt>
                                                <dd class="vis_box">
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_lipSyncAvatar.svg" class="img_xs" alt="백그라운드">
                                                            </div>
                                                            <em>Lip Sync Avata</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_avatar.svg" class="img_xs" alt="백그라운드">
                                                            </div>
                                                            <em>Face-to-Face Avatar</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_6_fold_.svg" class="img_m" alt="백그라운드">
                                                            </div>
                                                            <em>얼굴 인증</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_5_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>슈퍼 레졸루션</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_vsr.svg" class="img_xs" alt="백그라운드">
                                                            </div>
                                                            <em>비디오 슈퍼 레졸루션</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_2_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>AI 스타일링</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_1_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>텍스트 제거</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_10_fold_.svg" alt="백그라운드">
                                                            </div>
                                                            <em>이미지 자막인식</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_7_fold_.svg"  class="img_m" alt="백그라운드">
                                                            </div>
                                                            <em>인물포즈인식</em>
                                                        </li>


                                                    </ul>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_8_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>얼굴추적</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_face.svg" alt="백그라운드">
                                                            </div>
                                                            <em>얼굴 마스킹</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_hair.svg" alt="백그라운드">
                                                            </div>
                                                            <em>헤어 컬러 인식</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_clothes.svg" alt="백그라운드">
                                                            </div>
                                                            <em>의상 특징 인식</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_4_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>차량 번호판인식</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_windshield.svg"  class="img_m" alt="백그라운드">
                                                            </div>
                                                            <em>차량 유리창 마스킹</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_9_fold_.svg" class="img_s" alt="백그라운드">
                                                            </div>
                                                            <em>이상행동 감지</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_positioning.svg" class="img_xs" alt="백그라운드">
                                                            </div>
                                                            <em>치아 교정기 포지셔닝</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="inner_box">
                                            <dl class="blue">
                                                <dt>
                                                    <em>언어</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_correct.svg" class="img_s" alt="백그라운드">
                                                            </div>
                                                            <em>문장 교정</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_convers.svg" class="img_s" alt="백그라운드">
                                                            </div>
                                                            <em>한글 변환</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_1_fold.svg" class="img_s" alt="백그라운드">
                                                            </div>
                                                            <em>자연어 이해</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_2_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>AI 독해</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_3_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>텍스트 분류</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_5_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>문장 생성</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_6_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>패턴분류</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_itf.svg" alt="백그라운드">
                                                            </div>
                                                            <em>의도 분류</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="inner_box">
                                            <dl class="blue">
                                                <dt>
                                                    <em>대화</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_1_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>NQA 봇</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_2_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>위키봇&#47;뉴스봇</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_3_fold.svg" alt="백그라운드">
                                                            </div>
                                                            <em>호텔컨시어지봇</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="last_box">
                                        <div class="inner_box">
                                            <dl class="short_box">
                                                <dt>
                                                    <em>분석</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_data_analysis.svg" alt="백그라운드">
                                                            </div>
                                                            <em>데이터 상관 분석</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                            <dl class="long_box">
                                                <dt>
                                                    <em>영어교육</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_eng_1.svg" alt="백그라운드">
                                                            </div>
                                                            <em>교육용 STT</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_eng_2.svg" alt="백그라운드">
                                                            </div>
                                                            <em>문장 발음 평가</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_eng_3.svg" alt="백그라운드">
                                                            </div>
                                                            <em>파닉스 평가</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <!-- .mobile 추가 -->
                    <div class="stn_landing_m mobile">
                        <div class="stn_landing_cont">
                            <div class="item_set bg_blue">
                                <div class="swiper-container ecomins_engine">
                                    <ul class="item_lst swiper-wrapper">
                                        <li class="swiper-slide">
                                            <div class="inner_box">
                                                <dl class="">
                                                    <dt>
                                                        <em>음성</em>
                                                    </dt>
                                                    <dd>
                                                        <ul>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico-spe-1-fold.svg" alt="백그라운드">
                                                                </div>
                                                                <em>음성 생성</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_2_fold.svg" class="img_m" alt="백그라운드">
                                                                </div>
                                                                <em>음성 인식</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_3_fold.svg" alt="백그라운드">
                                                                </div>
                                                                <em>음성 정제</em>
                                                            </li>

                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_5_fold.svg"  alt="백그라운드">
                                                                </div>
                                                                <em>Voice Filter</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_spe_6_fold_.svg"  alt="백그라운드">
                                                                </div>
                                                                <em>화자 인증</em>
                                                            </li>
                                                        </ul>
                                                    </dd>
                                                </dl>
                                            </div>
                                        </li>
                                        <li class="swiper-slide">
                                            <div class="inner_box">
                                                <dl class="inner_box">
                                                    <dt>
                                                        <em>시각</em>
                                                    </dt>
                                                    <dd>
                                                        <ul>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_lipSyncAvatar.svg" class="img_xs" alt="백그라운드">
                                                                </div>
                                                                <em>Lip Sync Avata</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_avatar.svg" class="img_xs" alt="백그라운드">
                                                                </div>
                                                                <em>Face-to-Face Avatar</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_6_fold_.svg" class="img_m" alt="백그라운드">
                                                                </div>
                                                                <em>얼굴 인증</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_5_fold.svg" alt="백그라운드">
                                                                </div>
                                                                <em>슈퍼 레졸루션</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_vsr.svg" class="img_xs" alt="백그라운드">
                                                                </div>
                                                                <em>비디오 슈퍼 레졸루션</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_2_fold.svg" alt="백그라운드">
                                                                </div>
                                                                <em>AI 스타일링</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_1_fold.svg" alt="백그라운드">
                                                                </div>
                                                                <em>텍스트 제거</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_10_fold_.svg" alt="백그라운드">
                                                                </div>
                                                                <em>이미지 자막인식</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_7_fold_.svg"  class="img_m" alt="백그라운드">
                                                                </div>
                                                                <em>인물포즈인식</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_8_fold.svg" alt="백그라운드">
                                                                </div>
                                                                <em>얼굴추적</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_face.svg" alt="백그라운드">
                                                                </div>
                                                                <em>얼굴 마스킹</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_hair.svg" alt="백그라운드">
                                                                </div>
                                                                <em>헤어 컬러 인식</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_clothes.svg" alt="백그라운드">
                                                                </div>
                                                                <em>의상 특징 인식</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_4_fold.svg" alt="백그라운드">
                                                                </div>
                                                                <em>차량 번호판인식</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_windshield.svg"  class="img_m" alt="백그라운드">
                                                                </div>
                                                                <em>차량 유리창 마스킹</em>
                                                            </li>

                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_9_fold_.svg" class="img_s" alt="백그라운드">
                                                                </div>
                                                                <em>이상행동 감지</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_positioning.svg" class="img_xs" alt="백그라운드">
                                                                </div>
                                                                <em>치아 교정기 포지셔닝</em>
                                                            </li>
                                                        </ul>
                                                    </dd>
                                                </dl>
                                            </div>
                                        </li>
                                        <li class="swiper-slide">
                                            <div class="inner_box">
                                                <dl class="">
                                                    <dt>
                                                        <em>언어</em>
                                                    </dt>
                                                    <dd>
                                                        <ul>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_correct.svg" class="img_s" alt="백그라운드">
                                                                </div>
                                                                <em>문장 교정</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_convers.svg" class="img_s" alt="백그라운드">
                                                                </div>
                                                                <em>한글 변환</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_1_fold.svg" class="img_s" alt="백그라운드">
                                                                </div>
                                                                <em>자연어 이해</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_2_fold.svg" alt="백그라운드">
                                                                </div>
                                                                <em>AI 독해</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_3_fold.svg" alt="백그라운드">
                                                                </div>
                                                                <em>텍스트 분류</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_5_fold.svg" alt="백그라운드">
                                                                </div>
                                                                <em>문장 생성</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_6_fold.svg" alt="백그라운드">
                                                                </div>
                                                                <em>패턴분류</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_lan_itf.svg" alt="백그라운드">
                                                                </div>
                                                                <em>의도 분류</em>
                                                            </li>
                                                        </ul>
                                                    </dd>
                                                </dl>
                                            </div>
                                        </li>
                                        <li class="swiper-slide">
                                            <div class="inner_box">
                                                <dl class="">
                                                    <dt>
                                                        <em>대화</em>
                                                    </dt>
                                                    <dd>
                                                        <ul>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_1_fold.svg" alt="백그라운드">
                                                                </div>
                                                                <em>NQA 봇</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_2_fold.svg" alt="백그라운드">
                                                                </div>
                                                                <em>위키봇&#47;뉴스봇</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_bot_3_fold.svg" alt="백그라운드">
                                                                </div>
                                                                <em>호텔컨시어지봇</em>
                                                            </li>
                                                        </ul>
                                                    </dd>
                                                </dl>
                                            </div>
                                        </li>
                                        <li class="swiper-slide">
                                            <div class="inner_box" >
                                                <dl>
                                                    <dt>
                                                        <em>분석</em>
                                                    </dt>
                                                    <dd>
                                                        <ul>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_data_analysis.svg" alt="백그라운드">
                                                                </div>
                                                                <em>데이터<br>상관 분석</em>
                                                            </li>
                                                        </ul>
                                                    </dd>
                                                </dl>
                                            </div>
                                        </li>
                                        <li class="swiper-slide">
                                            <div class="inner_box" >
                                                <dl>
                                                    <dt>
                                                        <em>영어교육</em>
                                                    </dt>
                                                    <dd>
                                                        <ul>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_eng_1.svg" alt="백그라운드">
                                                                </div>
                                                                <em>교육용 STT</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_eng_2.svg" alt="백그라운드">
                                                                </div>
                                                                <em>문장 발음 평가</em>
                                                            </li>
                                                            <li class="landing_ico">
                                                                <div class="img">
                                                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_eng_3.svg" alt="백그라운드">
                                                                </div>
                                                                <em>파닉스 평가</em>
                                                            </li>
                                                        </ul>
                                                    </dd>
                                                </dl>
                                            </div>
                                        </li>
                                    </ul>
                                    <div class="swiper-pagination"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <dl>
                        <dt><em class="fas fa-chevron-right"></em>데이터 커스터마이징</dt>
                        <dd>maum.ai 플랫폼을 활용해 사용자가 보유하고 있는 데이터로 고유한 학습 모델 생성
                            <small>* 사용자가 제공한 데이터는 사용자의 엔진 개발 목적으로만 사용되고, 이후의 소유권은 사용자에게 있습니다.</small>
                        </dd>
                    </dl>
                    <dl>
                        <dt><em class="fas fa-chevron-right"></em>엔진 개발</dt>
                        <dd>특정 산업과 도메인에 특화된 고유한 엔진 공동 개발</dd>
                    </dl>
<%--                    <a href="#inquiry" title="가입 문의하기">ecoMINDs 가입 문의하기</a>--%>
                </div>
            </div>
        </div>

        <div class="academy_main ecominds_main on" id="familyCo">
            <div class="familyCo_box">
                <p>현재 많은 기업들이 maum.ai의 인공지능을 도입하여 사용하고 있습니다.  </p>
                <div class="bg">
                    <div class="intro_box dekyo_box">
                        <div class="fl">
                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_dekyo.png" alt="대교">
                            <h5> TTS를 활용한 북튜버의 특별한 콘텐츠! 대교 디지털 동화,
                                <strong>상상 Kids</strong>
                            </h5>
                            <div class="link">
                                <a href="https://play.google.com/store/apps/details?id=com.dkbooktalk.dkbooktalk_app" title="안드로이드 앱" target="_blank">안드로이드 앱</a>
                                <a href="https://apps.apple.com/kr/app/%EC%83%81%EC%83%81kids/id1523250313" title="아이폰 앱" target="_blank">아이폰 앱</a>
                            </div>
                        </div>
                        <div class="intro_img">
                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/img_dekyokids.png" alt="대교" />
                        </div>
                    </div>
                </div>

                <div class="intro_box twip_box">
                    <div class="intro_img">
                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/img_twip.png" alt="Twip"/>
                    </div>
                    <div class="fr">
                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_twip_text.png" alt="Twip" >
                        <h5>스트리머의 라이브 방송을 보다 재미있고 아름답게 만들어 주는
                            <strong>Twip</strong>
                        </h5>
                        <div class="link">
                            <a href="https://youtu.be/O36dVJUCPRg" title="YouTube  영상" target="_blank">YouTube  영상</a>
                            <a href="https://twip.kr/" title="홈페이지" target="_blank">홈페이지</a>
                        </div>
                    </div>
                </div>
                <div class="bg">
                    <div class="intro_box redtie_box">
                        <div class="fl">
                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_retiebutler.png" alt="레드타이">
                        </div>
                        <div class="fr">
                            <h5>인공지능 기반 다국어 호텔 컨시어지 챗봇 플랫폼,<strong>레드타이버틀러</strong></h5>
                        </div>
                        <div class="logos_box swiper-container hotels_logo">
                            <ul class="swiper-wrapper" id="swiper-wrapper">
                                <li class="swiper-slide">
                                    <a href="https://goldentulip-haeundae.com/" title="골든튤립해운대호텔앤스위트" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_1.png" alt="골든튤립해운대호텔앤스위트"/>
                                    </a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="https://mayfield.co.kr/2017/kor/html/index/" title="메이필드호텔" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_2.png" alt="메이필드호텔"/>
                                    </a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="http://rosanahotel.co.kr/" title="로사나부띠크호텔" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_4.png" alt="로사나부띠크호텔"/>
                                    </a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="http://westerngracehotel.net/" title="웨스턴그레이스호텔" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_5.png" alt="웨스턴그레이스호텔"/>
                                    </a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="http://www.suites.co.kr/Naksan/ko/" title="스위트호텔낙산" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_7.png" alt="스위트호텔낙산"/>
                                    </a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="http://www.hotelsienna.com/" title="호텔시에나" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_8.png" alt="호텔시에나"/>
                                    </a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="http://www.suites.co.kr/Gyeongju/ko/" title="스위트호텔경주" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_9.png" alt="스위트호텔경주"/>
                                    </a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="http://www.suites.co.kr/Namwon/ko/" title="스위트호텔남원" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_10.png" alt="스위트호텔남원"/>
                                    </a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="http://www.suites.co.kr/Jeju/ko" title="스위트호텔제주" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_11.png" alt="스위트호텔제주"/>
                                    </a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="http://royalemporium.co.kr/" title="로얄엠포리움호텔" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_12.png" alt="로얄엠포리움호텔"/>
                                    </a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="http://lavitahotelseoul.com/" title="라비타호텔" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_13.png" alt="라비타호텔"/>
                                    </a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="http://www.royal.co.kr/" title="로얄호텔서울" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_15.png" alt="로얄호텔서울"/>
                                    </a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="http://www.brownsuites.com/index.php" title="브라운스위트서울" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_16.png" alt="브라운스위트서울"/>
                                    </a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="http://hello-peace.com/" title="헬로피스펜션" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_17.png" alt="헬로피스펜션"/>
                                    </a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="https://www.patio7.co.kr/" title="파티오세븐호텔" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_18.png" alt="파티오세븐호텔"/>
                                    </a>
                                </li>
                                <li class="swiper-slide">
                                    <a href="https://www.seanhotelgroup.com/hotels/ramada-namdaemun/ko" title="라마다호텔앤스위트서울남대문" target="_blank">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_hotel_19.png" alt="라마다호텔앤스위트서울남대문"/>
                                    </a>
                                </li>
                            </ul>
<%--                            <div class="swiper-pagination"></div>--%>
                        </div>
                        <p>로고를 클릭하시면, 해당 호텔 홈페이지에서 챗봇을 체험할 수 있습니다.</p>
                        <div class="swiper-button-prev"><em class="fas fa-chevron-left"></em></div>
                        <div class="swiper-button-next"><em class="fas fa-chevron-right"></em></div>
                    </div>
                </div>

                <div class="intro_box fanmeet_box">
                    <div class="intro_img">
                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/img_fanmeet.png" alt="FanMeet"/>
                    </div>
                    <div class="fr">
                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_fanmeet.png" alt="FanMeet">
                        <h5>팬과 셀럽이 즐거운 추억을 쌓아가는 곳,
                            <strong>팬밋 FanMeet</strong>
                        </h5>
                        <div class="link">
                            <a href="https://play.google.com/store/apps/details?id=ai.maum.rf_tap_fanseem" title="안드로이드 앱" target="_blank">안드로이드 앱</a>
                        </div>
                    </div>
                </div>

                <div class="bg">
                    <div class="intro_box neo_box">
                        <div class="fl">
                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_logo_neocomix.png" alt="NEOCOMIX">
                            <h5>인공지능 더빙 서비스와 오디오북 플랫폼,<strong>네오코믹스</strong></h5>
<%--                            <div class="link">--%>
<%--                                <a href="http://neocomix.com/" title="홈페이지" target="_blank">홈페이지</a>--%>
<%--                            </div>--%>
                        </div>
                        <div class="intro_img">
                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_neocomix.png" alt="네오코믹스"/>
                        </div>
                    </div>
                </div>

                <div class="last_box">
                    <h5>더 많은 eco MINDs 고객사</h5>
                    <div class="family_logo">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_daekyo.png" alt="대교">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_kbs.png" alt="KBS">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_hanhwa.png" alt="한화생명">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/logo_samsungen.png" alt="삼성영어">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_logo_nlrc.png" alt="중앙노동위원회">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_logo_law.png" alt="대한민국법원">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/logo_orot.png" alt="오롯">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/logo_sunjin.png" alt="sunjin">
                    </div>
<%--                    <a href="#inquiry" title="가입 문의하기">ecoMINDs 가입 문의하기</a>--%>
                </div>
            </div>
        </div>
        <!-- #footer -->
        <%@ include file="../common/footer.jsp" %>

    </div>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/swiper.min.js"></script>
<script type="text/javascript">

    $(window).load(function() {

        $(document).ready(function (){

            $('.tab_list li').each(function(index){

                $(this).click(function(){
                    $('.tab_list li').removeClass('active');
                    $(this).addClass('active');
                    $('.academy_main ').hide();

                    if ($(this).hasClass('active')){
                        var cont = $(this).children().attr('class');
                        $("#"+cont).fadeIn();

                    }

                });
            });

            function validateEmail(sEmail) {
                var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
                if (filter.test(sEmail)) {
                    return true;
                }
                else {
                    return false;
                }
            }
            // 소개서 다운받기 버튼
            $('.down_link').on('click', function (e) {
                $('.lyr_info').fadeIn();
            });
            $('.btn_blue').on('click',function(){
                var name = $('#info_name').val();
                var company = $('#info_company').val();
                var email = $('#info_email').val();
                var phone = $('#info_phone').val();

                if(name !== "" && company !== "" && validateEmail( email ) == true && phone !== ""){

                    var title = $("#info_name").val() + " 님께서 자료를 다운로드 받았습니다.";
                    var mailContents = "이름 : " + $("#info_name").val() + "<br>"
                        + "회사(소속) : " + $("#info_company").val() + "<br>"
                        + "Email : " + $("#info_email").val() + "<br>"
                        + "연락처 : " + $("#info_phone").val() + "<br>"
                        + "다운로드 받은 소개서 : ecoMINDs ";

                    var formData = new FormData();
                    formData.append('fromaddr', $("#info_email").val());
                    formData.append('toaddr', 'hello@mindslab.ai');
                    formData.append('subject', title);
                    formData.append('message', mailContents);
                    formData.append($("#key").val(), $("#value").val());

                    $.ajax({
                        type 	: 'POST',
                        async   : true,
                        url  	: '/support/sendContactMail',
                        dataType : 'text',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (result) {
                            $('#info_name').val("");
                            $('#info_company').val("");
                            $('#info_email').val("");
                            $('#info_phone').val("");
                            console.log(result.toString());
                            console.log("sending info success");

                            var route = " maum.ai : 소개서 자료 다운로드 ";
                            var a = document.createElement('a');


                            a.href = "${pageContext.request.contextPath}/aiaas/common/files/eco%20MINDs%20컨설턴트%20소개서.pdf";
                            a.download = "[maum.ai] eco MINDs 컨설턴트 소개서.pdf";
                            a.style.display = 'none';
                            document.body.appendChild(a);
                            a.click();
                            delete a;

                            $('.lyr_info').hide();

                        }, error: function (err) {
                            console.log("parse HTML error! ", err);
                        }
                    });
                }else{
                    var alertMsg = "필수 입력사항을 모두 알맞게 입력해 주세요.";
                    alert( alertMsg );
                }
            });

            // var idx=1;
            // $('.swiper-next').on('click',function(){
            //     $('#swiper-wrapper').animate({
            //         "left":-958*idx+"px"
            //     },300);
            //     idx ++;
            // })
            // var idx2=1;
            // $('.swiper-prev').on('click',function(){
            //     $('#swiper-wrapper').animate({
            //         "left":+958*idx2+"px"
            //     },300);
            //     idx2 ++;
            // })
            applySwiper('.hotels_logo', {
                freeMode: true,
                speed : 300,
                slidesPerView: 'auto',
                spaceBetween: 15,
                slidesPerGroup : 6,
                loop : true,
                // loopFillGroupWithBlank : true,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev'
                }
            });

            applySwiper('.ecomins_engine', {
                slidesPerView: 1.5,
                spaceBetween: 20,
                slidesPerGroup : 1,
                loopFillGroupWithBlank : true,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev'
                }
            });
            function applySwiper(selector, option) {
                var defaultOption = {
                    speed : 200,
                    centeredSlides: false,
                    pagination: {
                        el: '.swiper-pagination',
                        clickable: true
                    }
                };
                return new Swiper(selector, $.extend(defaultOption, option))
            }
        });
    });
</script>
