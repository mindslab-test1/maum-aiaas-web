<%--
  Created by IntelliJ IDEA.
  User: bliss
  Date: 2020-08-21
  Time: 오전 11:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>
<html>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
    <title>maum.ai platform</title>
    <!-- resources -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/reset.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/font.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/login.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- //resources -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-1.11.0.min.js"></script>

</head>
<body>
<div id="wrap">
    <header>
        <a href="https://maum.ai/" title="마음에이아이" target="_self"><img src="${pageContext.request.contextPath}/aiaas/common/images/maumai.svg" alt="maum.ai logo" /></a>
    </header>
    <section id="container">
        <div class="content">
            <div class="passwordWrap">
                <div class="passwordBox">
                    <h5 class="password_tit">비밀번호 찾기</h5>
                    <p>가입한 이메일 주소를 입력해 주세요.</p>
                    <div class="email_type">
                        <label for="email"></label>
                        <input type="email" id="email" value="">
                        <a class="email_send" href="#none" title="결제정보 등록하기">보내기</a>
                    </div>
                    <div class="code_box">
                        <label for="code" style="display: none"></label>
                        <input type="number" name="code" id="code" class="" value="" placeholder="인증 코드 입력">
                        <em class="count">02:30</em>
                        <button class="email_code" type="button" id="email_code">확인</button>
                        <span class="email" id="code_noti">인증이 성공했습니다.</span>
                    </div>
                    <div class="login_btn">
                        <a href="/home/login" title="로그인">로그인</a>
                    </div>
                    <div class="passwordChangeBox">
                        <h5>비밀번호 변경</h5>
                        <label for="password">새 비밀번호</label>
                        <input type="password" id="password" value="">
                        <span class="noti">*8~12자리 영문,숫자 포함</span>
                        <label for="passwordCheck">새 비밀번호 재확인</label>
                        <input type="password" id="passwordCheck" value="">
                        <span class="error password">비밀번호가 일치하지 않습니다.</span>
                        <a class="passwordChange" href="#none" title="비밀번호 변경">비밀번호 변경</a>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <footer>
        <div class="footer">
            <a href="#none">이용약관 </a>  ㅣ
            <a href="#none"> 개인정보처리방침 </a>  ㅣ
            <a href="#none"> 고객센터 </a>
            <span>ㅣ</span>
            <div class="lang">
                <span class="lang_select">한국어 <em class="fas fa-chevron-up"></em></span>
                <ul>
                    <li class="active">한국어</li>
                    <li><a href="#none">English</a></li>
                </ul>
            </div>
        </div>
        <p>Copyright © 2020 주식회사 마인즈랩. All rights reserved.</p>
        <div class="m_lang">한국어 <a href="" title="English">English</a></div>
    </footer>
</div>
<script>

    jQuery.event.add(window,"load",function() {
        $(document).ready(function () {

            //비밀번호 변경 이메일 보내기 버튼
            $('.email_send').on('click', function(){
                $('.email_type').hide();
                $(".password_tit").text('비밀번호 변경 인증 코드');
                $(".passwordBox p").text('입력하신 메일 주소로 발송한 인증코드를 입력해주세요.');
                $('.code_box').fadeIn();
            })

            //비밀번호 이메일 코드 입력
            $(".email_code").on('click', function(){
                $('#code_noti').fadeIn();
                $('.passwordChangeBox').fadeIn();
                //만약 코드가 맞지 않은 경우
                //$('#code_noti').fadeIn().addClass('error').text('코드 번호를 다시 확인해주세요.');
            })

            //비밀번호 체크
            $('#password').keyup(function(){
                $(this).removeClass('error');
                $('.noti').removeClass('error');
                $('.password').hide();
            });

            //비밀번호 재확인 체크
            $('#passwordCheck').on('input keyup paste', function(){
                var password = $('#password').val();
                var password_check = $('#passwordCheck').val();
                if(password_check.length > 0){
                    if (password !== password_check) {
                        $('.password').css('display','block').addClass('error');
                    }else{
                        $('.password').hide();
                    }
                }else {
                    $('.password').hide();
                }
            });


            //최종 비밀번호 변경 버튼
            $(".passwordChange").on('click', function(){

                //비밀번호 유효성 체크
                var pw = $("#password").val();
                var pw_input = $("#password");
                var num = pw.search(/[0-9]/g);
                var eng = pw.search(/[a-z]/ig);
                if(pw.length < 8 || pw.length > 12){
                    alert("비밀번호는 8자리 ~ 12자리 이내로 입력해주세요.");
                    $('.noti').addClass('error');
                    $('#passwordCheck').val('');
                    pw_input.addClass('error');
                    return false;
                }else if(pw.search(/\s/) != -1){
                    alert("비밀번호는 공백 없이 입력해주세요.");
                    $('.noti').addClass('error');
                    $('#passwordCheck').val('');
                    pw_input.addClass('error');
                    return false;
                }else if(num < 0 || eng < 0  ){
                    alert("영문,숫자를 혼합하여 입력해주세요.");
                    $('.noti').addClass('error');
                    $('#passwordCheck').val('');
                    pw_input.addClass('error');
                    return false;
                }else {
                    $(".password_tit").text('비밀번호 변경 완료');
                    $(".passwordBox p").text('비빌번호 변경이 완료 되었습니다.');
                    $('.code_box').hide();
                    $('.passwordChangeBox').hide();
                    $('.login_btn').fadeIn();
                    return true;
                }

            })

            //footer 언어 체크
            $('.lang_select').on('click', function(){
                $(this).next().toggleClass('active');
            })

        });
    });
</script>
</body>
</html>