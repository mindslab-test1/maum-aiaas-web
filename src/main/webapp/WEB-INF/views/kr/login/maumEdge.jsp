<%--
  Created by IntelliJ IDEA.
  User: SMR
  Date: 2019-09-16
  Time: 오후 4:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="../common/common_header.jsp" %>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/maumEdgeAI.css?ver=<%=fmt.format(lastModifiedStyle)%>">


<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<!-- wrap -->
<div id="wrap" class="maumUI maumEdge">
    <%@ include file="../common/header.jsp" %>

        <!-- #container -->
        <div id="container">
            <!-- .content -->
            <div class="content">
                <!-- svc_disb -->
                <div id="stn_overview" class="svc_disb">
                    <div class="disbBox">
                        <p><span>Edge AI Platform</span>은<br><span>Edge AI Device</span>와 <span>maum AI Cloud</span>를 연동한 신개념 <span>AI Platform</span>입니다.</p>

                        <ul class="svc_card_list">
                            <li>
                                <dt>Edge AI Device</dt>
                                <dd>특징 추출 및 실시간 제어</dd>
                            </li>
                            <li>
                                <dt>AI Computing Cloud</dt>
                                <dd>데이터 분석</dd>
                            </li>
                        </ul>

                        <small>&ast; 엣지 컴퓨팅&lpar;Edge Computing&rpar;은 중앙 서버가 모든 데이터를 처리하는 클라우드 컴퓨팅과는 다른 방식으로 분산된 소형 서버를 통해 실시간으로 데이터를 처리하는 기술입니다.</small>
                    </div>
                </div>
                <!-- //svc_disb -->

                <!-- bg_whiteBox -->
                <div id="stn_edgeDevice" class="bg_whiteBox">
                    <dl>
                        <dt>Edge AI Device</dt>
                        <dd>
                            <ul class="dvc_card_list">
                                <li>
                                    <span class="tit">Device 1</span>

                                    <div class="dvc_detail">
<%--                                        <ul class="specify_list">--%>
<%--                                            <li>GSP &colon; <span>16 core (16 TOPS)</span></li>--%>
<%--                                            <li>CPU &colon; <span>ARM A53 (1GHz)</span></li>--%>
<%--                                            <li>RAM &colon; <span>DDR4 2/4/8 GB</span></li>--%>
<%--                                            <li>Power &colon; <span>7W</span></li>--%>
<%--                                            <li>Camera &colon; <span>MIPI CSI2</span></li>--%>
<%--                                            <li>Display &colon; <span>MIPI DSI</span></li>--%>
<%--                                            <li>Ethernet, USB</li>--%>
<%--                                        </ul>--%>

                                        <div class="radioBox">
                                            <!-- [D] input[type="radio"]의 id와 label의 for값 동일하게 주어야함 -->
                                            <input type="radio" name="device" id="device1" value="Device 1" checked>
                                            <label for="device1">
                                                <!-- <img src="resources/images/img_device01.png" alt="edge device 이미지"> -->
                                            </label>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="tit">Device 2</span>

                                    <div class="dvc_detail">
<%--                                        <ul class="specify_list">--%>
<%--                                            <li>GSP &colon; <span>17 core (16 TOPS)</span></li>--%>
<%--                                            <li>CPU &colon; <span>ARM A53 (1GHz)</span></li>--%>
<%--                                            <li>RAM &colon; <span>DDR4 2/4/8 GB</span></li>--%>
<%--                                            <li>Power &colon; <span>7W</span></li>--%>
<%--                                            <li>Camera &colon; <span>MIPI CSI2</span></li>--%>
<%--                                            <li>Display &colon; <span>MIPI DSI</span></li>--%>
<%--                                            <li>Ethernet, USB</li>--%>
<%--                                        </ul>--%>

                                        <div class="radioBox">
                                            <!-- [D] input[type="radio"]의 id와 label의 for값 동일하게 주어야함 -->
                                            <input type="radio" name="device" id="device2" value="Device 2">
                                            <label for="device2">
                                                <!-- <img src="resources/images/img_device02.png" alt="edge device 이미지"> -->
                                            </label>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <span class="tit">Device 3</span>

                                    <div class="dvc_detail">
<%--                                        <ul class="specify_list">--%>
<%--                                            <li>GSP &colon; <span>18 core (16 TOPS)</span></li>--%>
<%--                                            <li>CPU &colon; <span>ARM A53 (1GHz)</span></li>--%>
<%--                                            <li>RAM &colon; <span>DDR4 2/4/8 GB</span></li>--%>
<%--                                            <li>Power &colon; <span>7W</span></li>--%>
<%--                                            <li>Camera &colon; <span>MIPI CSI2</span></li>--%>
<%--                                            <li>Display &colon; <span>MIPI DSI</span></li>--%>
<%--                                            <li>Ethernet, USB</li>--%>
<%--                                        </ul>--%>

                                        <div class="radioBox">
                                            <!-- [D] input[type="radio"]의 id와 label의 for값 동일하게 주어야함 -->
                                            <input type="radio" name="device" id="device3" value="Device 3">
                                            <label for="device3">
                                                <!-- <img src="resources/images/img_device03.png" alt="edge device 이미지"> -->
                                            </label>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </dd>
                    </dl>

                    <div class="btnBox">
                        <a class="btn_lyr_open" id="inquiry_estimate" href="#lyr_inquiry_estimate">견적요청하기</a>
                    </div>
                </div>
                <!-- //bg_whiteBox -->

                <!-- svc_disb -->
                <div id="stn_edgeCloud" class="svc_disb">
                    <div class="disbBox">
                        <dl>
                            <dt>AI Computing Cloud</dt>
                            <dd>
                                <ul class="cloud_list fl">
                                    <li>
                                        <dl>
                                            <dt>Edge Road</dt>
                                            <dd>
                                                <ul>
                                                    <li>차량 통행 도로 AI surveillance</li>
                                                    <li>차량 종류 및 번호 인식</li>
                                                    <li>차선위반, 속도 위반 등 검출</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <span class="ico"></span>
                                    </li>
                                    <li>
                                        <dl>
                                            <dt>Edge Street</dt>
                                            <dd>
                                                <ul>
                                                    <li>거리 통행 행인 AI surveillance</li>
                                                    <li>
                                                        이상행동 탐지 및 통보<br>
                                                        &lpar;ex&colon; 폭행, 납치, 실신 등&rpar;
                                                    </li>
                                                    <li>특정인 수색 및 추적</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <span class="ico"></span>
                                    </li>
                                    <li>
                                        <dl>
                                            <dt>Edge Factory</dt>
                                            <dd>
                                                <ul>
                                                    <li>
                                                        고가의 GPU를 대체하는<br>
                                                        실용적인 솔루션 제공
                                                    </li>
                                                    <li>AI 기반의 실시간 분석 및 제어</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <span class="ico"></span>
                                    </li>
                                </ul>

                                <span class="img">
                                <img src="${pageContext.request.contextPath}/aiaas/kr/images/img_edgeCloud_main.png" alt="Edge Cloud 이미지">
                            </span>

                                <ul class="cloud_list fr">
                                    <li>
                                        <span class="ico"></span>
                                        <dl>
                                            <dt>Edge Farm</dt>
                                            <dd>
                                                <ul>
                                                    <li>스마트 농장 AI surveillance</li>
                                                    <li>스마트 농장 실시간 제어</li>
                                                    <li>농장 침입자 및 도둑 탐지</li>
                                                    <li>작황 및 생육 상황 분석</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </li>
                                    <li>
                                        <span class="ico"></span>
                                        <dl>
                                            <dt>Edge Home</dt>
                                            <dd>
                                                <ul>
                                                    <li>
                                                        개인정보 비식별화가 필요한<br>
                                                        사적&sol;공적 공간 AI surveillance
                                                    </li>
                                                    <li>이상행동 검출 및 통보</li>
                                                    <li>행동 패턴 분석</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </li>
                                    <li>
                                        <span class="ico"></span>
                                        <dl>
                                            <dt>Edge Drone</dt>
                                            <dd>
                                                <ul>
                                                    <li>
                                                        강력한 AI 계산 기능을 갖춘<br>
                                                        소형의 저전력 드론 솔루션 제공
                                                    </li>
                                                    <li>물체 및 인물 인식 및 추적 기능</li>
                                                    <li>드론 자율 비행을 위한 메타 데이터 추출</li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </li>
                                </ul>
                            </dd>
                        </dl>
                        <div class="btnBox">
                            <a href="#none" onclick="alert('준비중입니다.')" >시작하기</a>
                        </div>
                    </div>
                </div>
                <!-- //svc_disb -->

                <!-- bg_navyBox -->
                <div id="stn_svcCase" class="bg_navyBox">
                    <dl>
                        <dt>고객사례</dt>
                        <dd>
                            <ul class="svc_case_list">
                                <li>
                                    <span class="tit">노후경유 차량 검출 프로젝트</span>
                                    <a class="img" href="https://m.post.naver.com/viewer/postView.nhn?volumeNo=29675697&memberNo=457" target="_blank">
                                        <span>자세히보기<em class="fas fa-chevron-right"></em></span>
                                    </a>
                                </li>
                                <li>
                                    <span class="tit">이상행동 CCTV</span>
                                    <a class="img" href="https://m.post.naver.com/viewer/postView.nhn?volumeNo=29675697&memberNo=4" target="_blank">
                                        <span>자세히보기<em class="fas fa-chevron-right"></em></span>
                                    </a>
                                </li>
                                <li>
                                    <span class="tit">독거노인 모니터링 기능</span>
                                    <a class="img" href="https://m.post.naver.com/viewer/postView.nhn?volumeNo=29737645&memberNo=45" target="_blank">
                                        <span>자세히보기<em class="fas fa-chevron-right"></em></span>
                                    </a>
                                </li>
                            </ul>
                        </dd>
                    </dl>
                </div>
                <!-- //bg_navyBox -->

                <!-- svc_disb -->
                <div class="svc_disb">
                    <div class="disbBox">
                        <div class="img">
                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/img_inquiry.png" alt="문의하기 이미지">
                        </div>
                        <div class="txt">
                            <span>문의하기</span>
                            <p>Edge AI Platform에 관한 문의 사항이나 도움이 필요하시면<br>아래 고객센터로 전화해 주시거나 이메일로 문의해 주시면 바로 답변 드리겠습니다.</p>
                        </div>
                    </div>
                </div>
                <!-- //svc_disb -->
            </div>
            <!-- //.content -->
        </div>
        <!-- //#container -->
        <!-- #footer -->
        <%@ include file="../common/footer.jsp" %>

</div>
<!-- //wrap -->

<!-- 견적요청하기 -->
<div id="lyr_inquiry_estimate" class="lyrBox contactBox">
    <div class="estimate_tit">
        <h3>견적요청하기</h3>

        <div class="estimate_dvc">
            <span id="deviceNum">Device 3</span>
            <img id="deviceImg" src="${pageContext.request.contextPath}/aiaas/kr/images/img_device01.png" alt="Edge Device 이미지">
        </div>
    </div>

    <div class="estimate_form">
        <div class="contact_item">
            <span>이름</span>
            <input id="edge_name" type="text" class="ipt_txt" autocomplete="off" placeholder="필수">
        </div>
        <div class="contact_item">
            <span>회사</span>
            <input id="edge_company" type="text" class="ipt_txt" autocomplete="off" placeholder="필수">
        </div>
        <div class="contact_item">
            <span>직급</span>
            <input id="edge_rank" type="text" class="ipt_txt" autocomplete="off" placeholder="필수">
        </div>
        <div class="contact_item">
            <span>연락처</span>
            <input id="edge_phone" type="text" class="ipt_txt" autocomplete="off" placeholder="필수">
        </div>
        <div class="contact_item">
            <span>이메일</span>
            <input id="edge_mail" type="text" class="ipt_txt" autocomplete="off" placeholder="필수">
        </div>
        <div class="contact_item">
            <span>메모</span>
            <textarea id="noti" name="name" class="ipt_txt"></textarea>
        </div>
    </div>

    <div class="estimate_btn">
        <button class="btn_estimate">견적요청하기</button>
        <button class="btn_lyr_close">닫기</button>
    </div>
</div>
<!-- //견적요청하기 -->
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/maumEdgeAI.js?ver=<%=fmt.format(lastModifiedStyle)%>"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/viewScrollTrans.js?ver=<%=fmt.format(lastModifiedStyle)%>"></script>

<script type="text/javascript">
    function validateEmail(sEmail) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            return false;
        }
    }
    jQuery.event.add(window,"load",function(){
        $(document).ready(function(){
            $('html').scrollTop(0);

            $('.nav li a').on('click', function(){
                // 선택 메뉴 활성 효과
                $('.nav li a').removeClass('active');
                $(this).addClass('active');

                // 선택 메뉴 영역으로 스크롤 이동
                var scrollPosition = $($(this).attr('href')).offset().top;

                if( $('#wrap').is('.trans') ){
                    $('html').animate({
                        scrollTop: scrollPosition - 96,
                    }, 500);
                }else{
                    $('html').animate({
                        scrollTop: scrollPosition - 466,
                    }, 500);
                }
            });

            // Edge AI Device 영역 이미지 마우스오버 시 각 성능 리스트 보여주기
            var $deviceImage = $('.dvc_detail');

            $deviceImage.on('mousemove', function(e){
                e.stopPropagation();
                offsetX = e.offsetX;
                offsetY = e.offsetY;

                $(this).find('.specify_list').css({
                    display: 'inline-block',
                    top: offsetY + 20,
                    left: offsetX + 15
                });
            });
            $deviceImage.on('mouseleave', function(){
                $(this).find('.specify_list').css({
                    display: 'none',
                });
            });

            $('.btn_estimate').on('click', function () {

                if ($('#edge_name').val() === '' || $('#edge_rank').val() === ''|| $('#edge_phone').val() === ''|| $('#edge_mail').val() === ''|| $('#edge_company').val() === '' ) {
                    alert("필수 입력 사항을 입력해 주세요!");
                }else if(validateEmail( $('#edge_mail').val() ) == false){
                    alert("이메일 형식을 확인해 주세요!");
                }else {
                    var noti = "maum Edge AI Platform에서 견적요청하기";
                    var checkd = $('input[name="device"]:checked').val();
                    let title = "[maum.ai] "+$("#edge_name").val() + "님의 문의 사항입니다.";
                    let msg = "이름 : " + $("#edge_name").val() + "<br>회사 : " + $("#edge_company").val() + "<br>직급 : " + $("#edge_rank").val() + "<br>연락처 : " + $("#edge_phone").val() + "<br>이메일 : " + $('#edge_mail').val() +  "<br>메모 : " + $('#noti').val() +"<br>문의 상품 : " + checkd
                        + "<br><br><br>* "+noti+" 에서 보낸 메일입니다.";

                    let formData = new FormData();

                    formData.append('fromaddr', $('#edge_mail').val());
                    formData.append('toaddr', 'hello@mindslab.ai');
                    formData.append('subject', title);
                    formData.append('message', msg);

                    $.ajax({
                        type: 'POST',
                        async: true,
                        url: '/support/sendContactMail',
                        dataType: 'text',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (obj) {
                            $("#edge_name").val('');
                            $("#edge_company").val('');
                            $("#edge_rank").val('');
                            $("#edge_phone").val('');
                            $("#edge_mail").val('');
                            $("#noti").val('');
                            $('body').css('overflow','');
                            $('body').find('#lyr_inquiry_estimate').unwrap();
                            $('#mail_success').fadeIn();
                        },
                        error: function (xhr, status, error) {
                            console.log("error");
                            alert("Contact Us 메일발송 요청 실패하였습니다.");
                            window.location.href = "/";
                        }
                    });
                }
            });

        });
    });
</script>
