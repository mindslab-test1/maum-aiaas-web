<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-06-30
  Time: 10:32
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="../common/common_header.jsp" %>
<!-- .lyr_info -->
<div class="lyr_info">
    <div class="lyr_plan_bg"></div>
    <!-- .productWrap -->
    <div class="lyrWrap">
        <button class="btn_lyrWrap_close" type="button">닫기</button>
        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <em class="far fa-address-card"></em>
            <p>고객 정보 입력</p>
            <ul>
                <li>
                    <span>*이름</span>
                    <input id="info_name" type="text" value="" placeholder="필수 입력 사항">
                </li>
                <li>
                    <span>*회사(소속)</span>
                    <input id="info_company" type="text" value="" placeholder="필수 입력 사항">
                </li>
                <li>
                    <span>*이메일</span>
                    <input id="info_email" type="email" value="" placeholder="필수 입력 사항">
                </li>
                <li>
                    <span>휴대폰 정보</span>
                    <input id="info_phone" type="text" value="">
                </li>
            </ul>
            <button class="btn_blue" id="downloadPDF">다운로드</button>
        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.productWrap -->
</div>
<!-- //.lyr_info -->

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<!-- wrap -->
<div id="wrap" class="maumUI">
    <%@ include file="../common/header.jsp" %>
    <div class="loginWrap">

        <div class="stn_seminar">
            <h5>2020 마음AI 세미나 발표자료</h5>
            <div class="seminar_box">
                <div class="download_box part_1">
                    <img src="${pageContext.request.contextPath}/aiaas/common/images/img_seminar_part_1.jpg" alt="마음AI 세미나 1부" />
                    <span class="dwn_1">
                         <a class="" href="https://youtu.be/AWM1bi6Ja4c" target="_blank" title="인공지능이 필요할 땐, 마음 AI-AI Builder"><em class="fab fa-youtube"></em> <strong>영상보기</strong></a>
                         <a class="down_link" id="part1" href="#none" title="인공지능이 필요할 땐, 마음 AI-AI Builder"><em class="fas fa-download"></em> <strong>발표자료</strong></a>
                    </span>
                    <span class="dwn_2">
                         <a class="" href="https://youtu.be/evmX4WIwbOE" target="_blank" title="초고품질 AI 데이터의 중요성과 구축 방안"><em class="fab fa-youtube"></em> <strong>영상보기</strong></a>
                         <a class="down_link" id="part2" href="#none" title="초고품질 AI 데이터의 중요성과 구축 방안"><em class="fas fa-download"></em> <strong>다운로드</strong></a>
                    </span>
                    <span class="dwn_3">
                         <a class="" href="https://youtu.be/75HHyhAv5U0" target="_blank" title="사람을 살리는 데이터, 국내 최대 규모 COVID-19 데이터셋 구축 사례"><em class="fab fa-youtube"></em> <strong>영상보기</strong></a>
                         <a class="down_link" id="part3" href="#none" title="사람을 살리는 데이터, 국내 최대 규모 COVID-19 데이터셋 구축 사례"><em class="fas fa-download"></em> <strong>다운로드</strong></a>
                    </span>
                    <span class="dwn_4">
                         <a class="" href="https://youtu.be/9I-T6x9uIyg" target="_blank" title="AI Paas기반 인공지능 운영 및 학습 자동화"><em class="fab fa-youtube"></em> <strong>영상보기</strong></a>
                         <a class="down_link" id="part4" href="#none" title="AI Paas기반 인공지능 운영 및 학습 자동화"><em class="fas fa-download"></em> <strong>다운로드</strong></a>
                    </span>
                    <span class="dwn_5">
                         <a class="" href="https://youtu.be/wKz5dv_X0mw" target="_blank" title="세계 최고 인공지능 알고리즘을 누구나 쉽게, cloud AIP"><em class="fab fa-youtube"></em> <strong>영상보기</strong></a>
                         <a class="down_link" id="part5" href="#none" title="세계 최고 인공지능 알고리즘을 누구나 쉽게, cloud AIP"><em class="fas fa-download"></em> <strong>다운로드</strong></a>
                    </span>
                    <span class="dwn_6">
                         <a class="" href="https://youtu.be/7V_TuTUihFU" target="_blank" title="내 손안의 인공지능, Edge Computing 기반 인공지능 알고리즘"><em class="fab fa-youtube"></em> <strong>영상보기</strong></a>
                         <a class="down_link" id="part6" href="#none" title="내 손안의 인공지능, Edge Computing 기반 인공지능 알고리즘"><em class="fas fa-download"></em> <strong>다운로드</strong></a>
                    </span>
                </div>
                <div class="download_box part_2">
                    <img src="${pageContext.request.contextPath}/aiaas/common/images/img_seminar_part_2.jpg" alt="마음AI 세미나 2부" />
                    <span class="dwn_1">
                          <a class="" href="https://youtu.be/SpIdtPZ8-QQ" target="_blank" title="당신의 비즈니스를 AI Transform 하라, ecoMINDs 파트너십"><em class="fab fa-youtube"></em> <strong>영상보기</strong></a>
                          <a class="down_link" id="part7" href="#none" title="당신의 비즈니스를 AI Transform 하라, ecoMINDs 파트너십"><em class="fas fa-download"></em> <strong>다운로드</strong></a>
                    </span>
                    <span class="dwn_2">
                          <a class="" href="https://youtu.be/wNesqbLxYs8" target="_blank" title="ecoMINDs 파트너십 사례 - Heystars"><em class="fab fa-youtube"></em> <strong>영상보기</strong></a>
                          <a class="down_link" id="part8" href="#none" title="ecoMINDs 파트너십 사례 - Heystars"><em class="fas fa-download"></em> <strong>다운로드</strong></a>
                    </span>
                    <span class="dwn_3">
                          <a class="" href="https://youtu.be/bWxdzlewEH0" target="_blank" title="내 손 안의 인공지능, Edge Computing 기반 인공지능 혁신 사례"><em class="fab fa-youtube"></em> <strong>영상보기</strong></a>
                          <a class="down_link" id="part9" href="#none" title="내 손 안의 인공지능, Edge Computing 기반 인공지능 혁신 사례"><em class="fas fa-download"></em> <strong>다운로드</strong></a>
                    </span>
                    <span class="dwn_4">
                          <a class="" href="https://youtu.be/uHu5xEMohuk" target="_blank" title="기술이 사람을 잇다. 대화형 AI 기반 고객 상담 서비스"><em class="fab fa-youtube"></em> <strong>영상보기</strong></a>
                          <a class="down_link" id="part10" href="#none" title="기술이 사람을 잇다. 대화형 AI 기반 고객 상담 서비스"><em class="fas fa-download"></em> <strong>다운로드</strong></a>
                    </span>
<%--                    <a class="dwn_5" href="" download title="AI가 만드는 Insuretech, 현대해상 음성봇 운영 사례">다운로드</a>--%>
                    <span class="dwn_6">
                          <a class="" href="https://youtu.be/Gq6GciS9bLc" target="_blank" title="비즈니스의 일상을 바꾸는 AI, 'MAUM MINUTES'"><em class="fab fa-youtube"></em> <strong>영상보기</strong></a>
                          <a class="down_link" id="part11" href="#none" title="비즈니스의 일상을 바꾸는 AI, 'MAUM MINUTES'"><em class="fas fa-download"></em> <strong>다운로드</strong></a>
                    </span>
                </div>

            </div>
        </div>
        <!-- #footer -->
        <%@ include file="../common/footer.jsp" %>

    </div>
</div>
<!-- //wrap -->

<script type="text/javascript">
    $(window).load(function() {
        $(document).ready(function (){


            //팝업 창
            $('#seminar').on('click', function(){
                $('.seminar').fadeIn();
            });
            $('#membership').on('click', function(){
                $('.membership').fadeIn();
            });
            $('#curriculum').on('click', function(){
                $('.curriculum').fadeIn();
            });
            // 공통 팝업창 닫기
            $('.ico_close, .pop_bg').on('click', function () {
                $('.membership').fadeOut(300);
                $('.seminar').fadeOut(300);
                $('.curriculum').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });




            // 소개서 다운받기 버튼
            $('.down_link').on('click', function (e) {
                $('.lyr_info').fadeIn();
                downloadFlag = $(this).attr('id');
            });
            $('.btn_blue').on('click',function(){
                var name = $('#info_name').val();
                var company = $('#info_company').val();
                var email = $('#info_email').val();
                var phone = $('#info_phone').val();

                if(name !== "" && company !== "" && validateEmail( email ) == true){

                    var title = $("#info_name").val() + " 님께서 세미나 자료를 다운로드 받았습니다.";
                    var mailContents = "이름 : " + $("#info_name").val() + "<br>"
                        + "회사(소속) : " + $("#info_company").val() + "<br>"
                        + "Email : " + $("#info_email").val() + "<br>"
                        + "연락처 : " + $("#info_phone").val() + "<br>"
                        + "다운로드 받은 소개서 : " + downloadFlag;

                    var formData = new FormData();
                    formData.append('fromaddr', $("#info_email").val());
                    formData.append('toaddr', 'marketing@mindslab.ai');
                    formData.append('subject', title);
                    formData.append('message', mailContents);
                    formData.append($("#key").val(), $("#value").val());

                    $.ajax({
                        type 	: 'POST',
                        async   : true,
                        url  	: '/support/sendContactMail',
                        dataType : 'text',
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function (result) {
                            $('#info_name').val("");
                            $('#info_company').val("");
                            $('#info_email').val("");
                            $('#info_phone').val("");
                            console.log(result.toString());
                            console.log("sending info success");

                            var route = " maum.ai 세미나 자료 다운로드 ";
                            var a = document.createElement('a');

                            if( downloadFlag == 'part1' ) {
                                a.href = "https://drive.google.com/file/d/1xi-W6oR6OsfNePcY6qSt1IFSsBRNQO3H/view?usp=sharing";
                                a.download = "인공지능이 필요할 땐, 마음 AI-AI Builder";
                                a.target = "_blank";
                            }
                            else if ( downloadFlag == 'part2' ) {
                                a.href = "https://drive.google.com/file/d/1IUxc8P_5KwDG7qHrm1J_AFgp03jx6YIn/view?usp=sharing";
                                a.download = "초고품질 AI 데이터의 중요성과 구축 방안";
                                a.target = "_blank";
                            }
                            else if( downloadFlag == 'part3') {
                                a.href = "https://drive.google.com/file/d/1-Py_Gh71wFQD2I8LZegAqcACCMgz1oiQ/view?usp=sharing";
                                a.download = "사람을 살리는 데이터, 국내 최대 규모 COVID-19 데이터셋 구축 사례";
                                a.target = "_blank";
                            }
                            else if( downloadFlag == 'part4' ) {
                                a.href = "https://drive.google.com/file/d/19N6RzMNFpLSXfIJEc5xUHO2if19YGKe_/view?usp=sharing";
                                a.download = "AI Paas기반 인공지능 운영 및 학습 자동화";
                                a.target = "_blank";
                            }
                            else if( downloadFlag == 'part5' ) {

                                a.href = "https://drive.google.com/file/d/1UwA8_cUOBuA8d2BWkFRQyn74mJmVpI_k/view?usp=sharing";
                                a.download = "세계 최고 인공지능 알고리즘을 누구나 쉽게, cloud AIP";
                                a.target = "_blank";
                            }
                            else if( downloadFlag == 'part6' ) {
                                a.href = "https://drive.google.com/file/d/1QuU1a7OGRe0vrs7gxJIReuJCxQqJ01Ho/view?usp=sharing";
                                a.download = "내 손안의 인공지능, Edge Computing 기반 인공지능 알고리즘";
                                a.target = "_blank";
                            }
                            else if( downloadFlag == 'part7' ) {
                                a.href = "https://drive.google.com/file/d/1vAmVdI3dv0nrT0rAqqB0fo2WXODkvP-z/view?usp=sharing";
                                a.download = "당신의 비즈니스를 AI Transform 하라, ecoMINDs 파트너십";
                                a.target = "_blank";
                            }
                            else if( downloadFlag == 'part8' ) {
                                a.href = "https://drive.google.com/file/d/1MXM41P8zH_5e0q5FIF8BFdFa0AgMakDs/view?usp=sharing";
                                a.download = "ecoMINDs 파트너십 사례 - Heystars";
                                a.target = "_blank";
                            }
                            else if( downloadFlag == 'part9' ) {
                                a.href = "https://drive.google.com/file/d/1FmnY3pXVsvAiQt3DowcnKdk3VOXsEaoZ/view?usp=sharing";
                                a.download = "내 손 안의 인공지능, Edge Computing 기반 인공지능 혁신 사례";
                                a.target = "_blank";
                            }
                            else if( downloadFlag == 'part10' ) {
                                a.href = "https://drive.google.com/file/d/1ErC5w1ZSlLJfL8VOK5PYwmYCyd9nC-J2/view?usp=sharing";
                                a.download = "기술이 사람을 잇다. 대화형 AI 기반 고객 상담 서비스";
                                a.target = "_blank";
                            }
                            else if( downloadFlag == 'part11' ) {
                                a.href = "https://drive.google.com/file/d/1aNJG5cKW3JR5_A3_LSNcMCHgGyf_pD7b/view?usp=sharing";
                                a.download = "비즈니스의 일상을 바꾸는 AI, 'MAUM MINUTES'";
                                a.target = "_blank";
                            }
                            a.style.display = 'none';
                            document.body.appendChild(a);
                            a.click();
                            delete a;

                            $('.lyr_info').hide();

                        }, error: function (err) {
                            console.log("parse HTML error! ", err);
                        }
                    });
                }else{
                    var alertMsg = "필수 입력사항을 모두 알맞게 입력해 주세요.";
                    alert( alertMsg );
                }
            });

        });
    });
</script>

<script type="text/javascript">


</script>