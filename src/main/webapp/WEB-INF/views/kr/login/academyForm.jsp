<%--
  Created by IntelliJ IDEA.
  User: SMR
  Date: 2019-09-16
  Time: 오후 4:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="../common/common_header.jsp" %>


<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->


<!-- 5 .pop_simple -->
<%--<div class="pop_simple membership">--%>
<%--    <div class="pop_bg"></div>--%>
<%--    <!-- .popwrap -->--%>
<%--    <div class="popwrap">--%>
<%--        <em class="fas fa-times ico_close"></em>--%>
<%--        <!-- .pop_bd -->--%>
<%--        <div class="pop_bd">--%>
<%--            <h1><strong>에코마인즈</strong> 멤버십 혜택</h1>--%>
<%--            <p>에코마인즈란, AI 알고리즘과 엔진 등 기술전반에서부터 AI 응용 서비스, AI 기반 스마트 머신,<br>--%>
<%--                AI 유관 영역마다 여러 스타트업 및 연구 기관과 협업하는 파트너십 프로젝트입니다.</p>--%>
<%--            <div class="list_desc">--%>
<%--                <ul>--%>
<%--                    <li><span>에코마인즈</span>--%>
<%--                        프로젝트 지원범위--%>
<%--                    </li>--%>
<%--                    <li><h5>기술 지원</h5>--%>
<%--                        기존 서비스 아이디어에 마인즈랩의<br>--%>
<%--                        AI 기술을 더하여 공동프로젝트 수행--%>
<%--                    </li>--%>
<%--                    <li><h5>Sales/Marketing 지원</h5>--%>
<%--                        AI 전문 컨설턴트의 영업지원부터<br>--%>
<%--                        홍보마케팅까지 한 번에 해결 가능--%>
<%--                    </li>--%>
<%--                    <li><h5>BackOffice 지원</h5>--%>
<%--                        AI 기술지원부터 이를 통한 수익배분까지 경영지원 업무 서포트--%>
<%--                    </li>--%>
<%--                    <li><h5>투자 지원</h5>--%>
<%--                        팁스(TIPs) 등 자금투자 방안 제시 및--%>
<%--                        투자유치 가이드를 통한 자금난 해결--%>
<%--                    </li>--%>
<%--                    <li><h5>Space 지원</h5>--%>
<%--                        판교테크노벨리 내 Co-Working<br>--%>
<%--                        공간 활용--%>
<%--                    </li>--%>
<%--                </ul>--%>
<%--            </div>--%>
<%--        </div>--%>
<%--        <!-- //.pop_bd -->--%>

<%--    </div>--%>
<%--    <!-- //.popwrap -->--%>
<%--</div>--%>
<!-- //.pop_simple -->

<!-- 5 .pop_simple -->
<div class="pop_simple seminar">
    <div class="pop_bg"></div>
    <!-- .popwrap -->
    <div class="popwrap">
        <em class="fas fa-times ico_close"></em>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <h1><strong>맞춤형</strong> 인공지능 세미나</h1>
            <p>maum.ai 고객 각자에 맞는 맞춤 세미나를 열어드립니다. 인공지능 기초 교육은 물론, 실제 비즈니스 모델에 적용<br>할 수 있게 마음 아카데미가 함께 합니다. 더 늦기 전에 최신 인공지능을 경험해보세요.
            </p>
            <div class="list_desc">
                <ul>
                    <li><h5>공공기관/ 제조업, 행정 사무</h5>
                        Smart City<br>
                        Smart Factory<br>
                        Smart Work / Document
                    </li>
                    <li><h5>보험</h5>
                        인공지능 Overview<br>
                        완전판매모니터링<br>
                        보험계약대출<br>
                        회의록 자동 기록 시스템
                    </li>
                    <li><h5>교육</h5>
                        인공지능 Overview<br>
                        음성지능 적용 말하기 연습<br>
                        시각지능 적용 방안<br>
                        하이브리드 교육 센터
                    </li>
                    <li><h5>고객센터</h5>
                        인공지능 Overview<br>
                        음성봇과 휴먼 상담사의 만남<br>
                        주문봇<br>
                        감성케어
                    </li>
                    <li><h5>금융</h5>
                        인공지능 Overview<br>
                        대직원용, 대고객용 챗봇<br>
                        음성지능 적용<br>
                        시각지능 적용
                    </li>
                    <li><h5>대학 / 교육기관</h5>
                        인공지능 개요<br>
                        인공지능 플랫폼<br>
                        인공지능 엔진 사용법
                    </li>
                    <li><h5>콘텐츠, 미디어</h5>
                        인공지능 Overview<br>
                        TTS 만들기 과정<br>
                        인공지능을 활용한 콘텐츠 제작 과정 소개
                    </li>
                    <li><h5>스타트업</h5>
                        인공지능 Overview<br>
                        Success Stories를 통해 본비즈니스 적용<br>
                        EcoMinds
                    </li>
                </ul>
            </div>
        </div>
        <!-- //.pop_bd -->

    </div>
    <!-- //.popwrap -->
</div>
<!-- //.pop_simple -->

<!-- 5 .pop_simple -->
<div class="pop_simple curriculum">
    <div class="pop_bg"></div>
    <!-- .popwrap -->
    <div class="popwrap">
        <em class="fas fa-times ico_close"></em>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <h1><strong>대학교</strong> 인공지능 커리큘럼(입문과정) 예시</h1>
            <div class="curri_table">
                <table>
                    <colgroup>
                        <col width="130"><col width="170"><col width="250"><col width="120">
                    </colgroup>
                    <tr>
                        <td rowspan="2" class="">
                            <p>Part 1.<br>
                                오버뷰 과정</p>
                        </td>
                        <td rowspan="2" align="left" class="vertical">인공지능의 개괄적 이해</td>
                        <td align="left" class="">1. 인공지능Overview<br>
                            -인공지능이 세상을 어떻게 바꾸는가?<br>
                            -Use cases</td>
                        <td rowspan="6" class="">일반 대학생<br>
                            (교양/개론)</td>

                    </tr>
                    <tr>
                        <td align="left" class="">2. 인공지능 개론<br>
                            -인공지능과 4차산업혁명<br>
                            -딥러닝 및 자연어처리의 개괄적 이해</td>
                    </tr>
                    <tr>
                        <td rowspan="4" class="">
                            <p>Part 2.<br>
                                이론 심화과정</p>
                        </td>
                        <td align="left" class="vertical">인공지능 플랫폼 사용실습</td>
                        <td align="left" class="">1 인공지능 플랫폼 실습<br>
                            -maum.ai 사용실습</td>
                    </tr>
                    <tr>
                        <td rowspan="3" align="left" class="vertical">인공지능의 이해 (심화)</td>
                        <td align="left" class="">1. 음성지능의 이해<br>
                            -STT, TTS 이론 및 Use Cases<br>
                            -STT 데이터 수집 (과제) </td>
                    </tr>
                    <tr>
                        <td align="left" class=""> 2. 시각지능의 이해<br>
                            -DIARL, TTI 이론 및 Use Cases </td>
                    </tr>
                    <tr>
                        <td align="left" class="">3. 언어지능의 이해<br>
                            -NLP, Bert<br>
                            -Data Check </td>
                    </tr>
                    <tr>
                        <td class="">
                            <p>Part 3.<br>
                                개발 적용 과정</p>
                        </td>
                        <td align="left" >인공지능 API 활용<br>
                            앱/웹 개발 가이드</td>
                        <td align="left">1 인공지능 기술 활용한 서비스 제작<br>
                            -서비스 개발 사례, API 연결법, 연결 실습</td>
                        <td class="">기초 개발자<br>
                            및 컴공전공자 </td>

                    </tr>
                    <tr>
                        <td rowspan="2" class="">
                            <p>Part 4.<br>
                                챗봇 과정</p>
                        </td>

                        <td rowspan="2" align="left" class="vertical">챗봇의 이해<br>
                            및 실습</td>
                        <td align="left" >1. 챗봇의 이해<br>
                            -현황 및 기본 이해<br>
                            2. Spoken Dialogue System<br>
                            -SDS 활용하여 챗봇 만들기<br>
                            -챗봇 만들기 실습 </td>

                        <td rowspan="2" class="">일반 대학생</td>
                    </tr>
                    <tr>
                        <td align="left" class="">3. 검색기반 QA 시스템<br>
                            -QA 기술 및 시맨틱 QA의 이해<br>
                            -실습 </td>
                    </tr>
                </table>
            </div>

        </div>
        <!-- //.pop_bd -->
    </div>
    <!-- //.popwrap -->
</div>
<!-- //.pop_simple -->

<!-- 5 .pop_simple -->
<div class="pop_simple schedule">
    <div class="pop_bg"></div>
    <!-- .popwrap -->
    <div class="popwrap">
        <em class="fas fa-times ico_close"></em>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <h1>상세 <strong>교육 일정</strong></h1>
            <div class="tbl_box">
                <div class="tbl_area">
                    <div class="top">
                        <strong>월</strong>
                        <strong>화</strong>
                        <strong>수</strong>
                        <strong>목</strong>
                        <strong>금</strong>
                        <div>
                            <span>AI TransformationM</span>
                            <span>maum.ai</span>
                            <span>AI Transformation</span>
                        </div>
                    </div>
                    <ul class="week">
                        <li>1주차</li>
                        <li>2주차</li>
                        <li>3주차</li>
                        <li>4주차</li>
                    </ul>
                    <div class="tbl">
                        <table>
                            <colgroup>
                                <col width="153"><col><col><col><col width="153">
                            </colgroup>
                            <tr>
                                <td>Overview</td>
                                <td colspan="3">
                                    인공지능 생태계
                                    <ul>
                                        <li>스타트업 동향</li>
                                        <li>maum.ai</li>
                                        <li>Success story</li>
                                    </ul>
                                </td>
                                <td>#1 기업 조사</td>
                            </tr>
                            <tr>
                                <td>Business<br>
                                    Communication</td>
                                <td colspan="3">
                                    Cloud API
                                    <ul>
                                        <li>Audio</li>
                                        <li>Vision</li>
                                        <li>NLP</li>
                                    </ul>
                                </td>
                                <td>#2 인공지능<br>
                                    정의하기</td>
                            </tr>
                            <tr>
                                <td>#2 인공지능<br>
                                    정의하기</td>
                                <td colspan="3">
                                    maum.ai 실습
                                    <ul>
                                        <li>AI Builder</li>
                                    </ul>
                                </td>
                                <td>#3 인공지능<br>
                                    계획 수립</td>
                            </tr>
                            <tr>
                                <td>#3 인공지능<br>
                                    계획 수립</td>
                                <td colspan="3">
                                    AI Consultant
                                    <ul>
                                        <li>AI Consultant</li>
                                        <li>MAU 파트너십</li>
                                        <li>정부 사업</li>
                                    </ul>
                                </td>
                                <td>#4 인공지능<br>
                                    사업계획</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- //.pop_bd -->

    </div>
    <!-- //.popwrap -->
</div>
<!-- //.pop_simple -->


<!-- wrap -->
<div id="wrap" class="maumUI">
    <%@ include file="../common/header.jsp" %>

    <div class="loginWrap">

        <div class="academy_top">
            <div class="academy_top_area">
                <h1>인공지능 교육 플랫폼</h1>
                <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_academy.svg" alt="아카데미 로고" />
<%--                <div class="tab_menu">--%>
<%--                    <ul class="tab_list">--%>
<%--                        <li class="tab_tit active"><button type="button" class="aca_industry">마음 아카데미란?</button></li>--%>
<%--                        <li class="tab_tit"><button type="button" class="aca_guide">엔진 가이드</button></li>--%>
<%--                        <li class="tab_tit"><button type="button" class="aca_curri">교육 커리큘럼</button></li>--%>
<%--&lt;%&ndash;                        <li class="tab_tit"><button type="button" class="aca_apply">수강신청</button></li>&ndash;%&gt;--%>
<%--                    </ul>--%>
<%--                </div>--%>
            </div>
        </div>
<%--        <div class="academy_main" id="aca_guide">--%>
<%--            <div class="curriculum_table">--%>
<%--                <h1>엔진 교육 커리큘럼</h1>--%>
<%--                <img src="${pageContext.request.contextPath}/aiaas/common/images/img_academyTable_new.png" alt="커리큘럼 테이블" />--%>
<%--                <div class="disc_box">--%>
<%--                    <div class="disc_desc">--%>
<%--                        <h4>엔진 교육</h4>--%>
<%--                        <ul>--%>
<%--                            <li><em class="fas fa-check"></em>인공지능 플랫폼 maum.ai에서 제공 중인 다양한 엔진에 대해 배울 수 있습니다.</li>--%>
<%--                            <li><em class="fas fa-check"></em>각 엔진을 쉽게 활용할 수 있도록 이론 및 실습교육을 제공합니다. </li>--%>
<%--                            <li><em class="fas fa-check"></em>엔진을 활용한 서비스 제작 사례, Use Cases를 살펴볼 수 있습니다. </li>--%>
<%--                        </ul>--%>

<%--<!-- 로그인 X인 경우 -->--%>
<%--<sec:authorize access="isAnonymous()">--%>
<%--    <a href="javascript:go_academy()" class="apply_btn">교육 플랫폼 <small>바로가기</small> <em class="fas fa-chevron-right"></em></a>--%>
<%--</sec:authorize>--%>
<%--<!-- 로그인 O인 경우 -->--%>
<%--<sec:authorize access="isAuthenticated()">--%>
<%--    <a href="https://www.notion.so/maum-Academy-9b6c0a54b996410f85b06d1395965301" class="apply_btn loginCheck" target="_blank">교육 플랫폼 <small>바로가기</small> <em class="fas fa-chevron-right"></em></a>--%>
<%--</sec:authorize>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--&lt;%&ndash;                <div class="step_box">&ndash;%&gt;--%>
<%--&lt;%&ndash;                    <div class="step">&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <h5><strong>Level 1.</strong> Overview 교육 <small>How to use AI Engines</small></h5>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <ul>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <li><span>대상</span>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                인공지능을 비즈니스에 적용하고자 하는&ndash;%&gt;--%>
<%--&lt;%&ndash;                                    기업 실무자 및 스타트업</li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <li><span>목표</span>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                인공지능 플랫폼 maum.ai 사용법을 익히고&ndash;%&gt;--%>
<%--&lt;%&ndash;                                    비즈니스에 활용할 수 있다.</li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        </ul>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <dl>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dt>인공지능<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                플랫폼의 이해</dt>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dd>- 인공지능이 세상을 어떻게<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                바꾸는가?&ndash;%&gt;--%>
<%--&lt;%&ndash;                            </dd>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        </dl>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <dl>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dt>인공지능의<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                비즈니스 적용</dt>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dd>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                - 각 산업 별 인공지능의 적용<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                - Success Stories&ndash;%&gt;--%>
<%--&lt;%&ndash;                            </dd>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        </dl>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <dl>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dt>인공지능 플랫폼의<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                이해 및 실습</dt>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dd>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                - maum.ai 인공지능 엔진<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                사용법<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                - maum.ai 실습&ndash;%&gt;--%>
<%--&lt;%&ndash;                            </dd>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        </dl>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <dl>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dt>MVP 만들기</dt>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dd>- MVP 방법론<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                - MVP Maker툴을 활용하여<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                인공지능 엔진 조합하는 법<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                - Make a thon&ndash;%&gt;--%>
<%--&lt;%&ndash;                            </dd>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        </dl>&ndash;%&gt;--%>
<%--&lt;%&ndash;                    </div>&ndash;%&gt;--%>
<%--&lt;%&ndash;                    <div class="step">&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <h5><strong>Level 2.</strong> AI 엔진교육<small>How to customize AI Engines</small></h5>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <ul>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <li><span>대상</span>인공지능 엔진을 커스터마이징 하여 모델을&ndash;%&gt;--%>
<%--&lt;%&ndash;                                구축하고자 하는 사용자&ndash;%&gt;--%>
<%--&lt;%&ndash;                            </li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <li><span>목표</span>인공지능 엔진을 학습, 커스터마이징&ndash;%&gt;--%>
<%--&lt;%&ndash;                                할 수 있다. </li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        </ul>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <dl>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dt>머신러닝</dt>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dd>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                - 머신러닝/딥러닝 학습<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                - 특정 모델 구축&ndash;%&gt;--%>
<%--&lt;%&ndash;                            </dd>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        </dl>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <dl>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dt>데이터 분석</dt>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dd>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                - 데이터 수집과 처리<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                - 데이터 분석과 머신러닝 적용&ndash;%&gt;--%>
<%--&lt;%&ndash;                            </dd>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        </dl>&ndash;%&gt;--%>

<%--&lt;%&ndash;                    </div>&ndash;%&gt;--%>
<%--&lt;%&ndash;                    <div class="step">&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <h5><strong>Level 3.</strong> 알고리즘 교육<small>How to develop AI algorithm</small></h5>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <ul>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <li><span>대상</span>인공지능 알고리즘과 엔진을 개발하고자&ndash;%&gt;--%>
<%--&lt;%&ndash;                                하는 전문가&ndash;%&gt;--%>
<%--&lt;%&ndash;                            </li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <li><span>목표</span>인공지능 알고리즘을 이해하고 새로운&ndash;%&gt;--%>
<%--&lt;%&ndash;                                알고리즘을 만들 수 있다. </li>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        </ul>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <dl>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dt>인공지능의<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                알고리즘 분석</dt>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dd>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                - 인공지능을 위한 수학/함수<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                - 파이썬 프로그래밍<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                - 언어지능, 시각지능, 음성지능&ndash;%&gt;--%>
<%--&lt;%&ndash;                            </dd>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        </dl>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <dl>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dt>API 패키징</dt>&ndash;%&gt;--%>
<%--&lt;%&ndash;                            <dd>- 인공지능 엔진의 API 패키징<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                방법 및 실습<br>&ndash;%&gt;--%>
<%--&lt;%&ndash;                                - CUDA Optimization&ndash;%&gt;--%>
<%--&lt;%&ndash;                            </dd>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        </dl>&ndash;%&gt;--%>
<%--&lt;%&ndash;                    </div>&ndash;%&gt;--%>

<%--&lt;%&ndash;                </div>&ndash;%&gt;--%>
<%--            </div>--%>

<%--        </div>--%>
<%--        <div class="academy_main" id="aca_curri">--%>
<%--            <div class="edu_curriculum">--%>
<%--                <h1>교육 커리큘럼</h1>--%>
<%--                <p>maum Academy에서는 주기적인 교육 업데이트가 진행되고 있습니다. <br>--%>
<%--                    maum.ai 로그인 후 인공지능에 관한 더욱더 다양한 교육을 만나보실 수 있습니다.</p>--%>
<%--                <ul>--%>
<%--                    <li>--%>
<%--                        <h5>소양 교육</h5>--%>
<%--                        <p>인공지능 기초지식 이곳에서 쌓자!<small>maum.ai 기초 &nbsp;|&nbsp; AIaaS 기초 &nbsp;|&nbsp; 비전공자로 인공지능 시대 살아남기&nbsp; |&nbsp; AIaaS 심화</small> </p>--%>
<%--                    </li>--%>
<%--                    <li>--%>
<%--                        <h5>실무 교육</h5>--%>
<%--                        <p>인공지능 전문가가 되고 싶다면? <small>STT &nbsp;|&nbsp; TTS &nbsp;| &nbsp;인물 포즈인식 &nbsp;|&nbsp; Maum회의록 &nbsp;|&nbsp; AI Builder &nbsp;|&nbsp; maumSDS</small></p>--%>
<%--                    </li>--%>
<%--                </ul>--%>

<%--<!-- 로그인 X인 경우 -->--%>
<%--<sec:authorize access="isAnonymous()">--%>
<%--                <a href="javascript:go_academy()">교육 플랫폼 <small>바로가기</small> <em class="fas fa-chevron-right"></em></a>--%>
<%--</sec:authorize>--%>

<%--<!-- 로그인 O인 경우 -->--%>
<%--<sec:authorize access="isAuthenticated()">--%>
<%--    <a href="https://www.notion.so/maum-Academy-9b6c0a54b996410f85b06d1395965301" class="loginCheck" target="_blank">교육 플랫폼 <small>바로가기</small> <em class="fas fa-chevron-right"></em></a>--%>
<%--</sec:authorize>--%>

<%--            </div>--%>
<%--        </div>--%>
<%--        <div class="academy_main" id="aca_apply">--%>
<%--            <div class="apply_desc_area">--%>
<%--                <div class="apply_desc">--%>
<%--                    <div class="btn_apply">--%>
<%--                        <a href="https://docs.google.com/forms/d/1fQ1byg5AE5mFiRBQXrci5r2hqlC-WY7RKrBkUdClky4/edit" target="_blank" class="apply_btn">수강신청</a>--%>
<%--                        <a href="https://docs.google.com/forms/d/1geXLuzS4ueF0M8XuasV6NPy1Ksu3CDyxRb2Se97E-rk/edit" target="_blank" class="apply_change">수강변경 및 취소</a>--%>
<%--                    </div>--%>
<%--                    <p>--%>
<%--                        <strong>장소</strong>마인즈랩 AI 교육센터 (판교 오피스 6층 강의실)<br>--%>
<%--                        <strong>수강료</strong>maum.ai 가입 고객 무료 교육<br>--%>
<%--                        <strong>문의</strong>hello@mindslab.ai<br>--%>
<%--                    </p>--%>
<%--                </div>--%>

<%--            </div>--%>

<%--            <div class="month_box">--%>
<%--                <h3>12월</h3>--%>
<%--                <table>--%>
<%--                    <colgroup>--%>
<%--                        <col><col width="140"><col width="140"><col><col>--%>
<%--                    </colgroup>--%>
<%--                    <tr>--%>
<%--                        <td rowspan="5" class="td_date">--%>
<%--                            <em><strong>19일</strong> (목)</em>--%>
<%--                            <span>Level 2. <br>--%>
<%--                                    <small style="font-size:12px;line-height: 19px; display: block;">How to customize <br>AI engines.<br>--%>
<%--                                 (Part 1. TTS)</small> </span>--%>
<%--                        </td>--%>
<%--                        <td class="td_txt"><strong>9:30-10:30</strong></td>--%>
<%--                        <td class="td_txt">데이터 구축 및 정제</td>--%>
<%--                        <td class="td_txt">- TTS 모델 구축을 위한 데이터는 어떻게 구축하는가?</td>--%>

<%--                    </tr>--%>
<%--                    <tr>--%>
<%--                        <td class="td_txt"><strong>10:30-11:30</strong></td>--%>
<%--                        <td class="td_txt">학습 방법</td>--%>
<%--                        <td class="td_txt">- TTS를 커스터마이징 하는 방법 <br>- AI 엔진 학습 실습</td>--%>
<%--                    </tr>--%>
<%--&lt;%&ndash;                    <tr>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <td class="last_txt"><strong>11:30-12:30</strong></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <td class="last_txt">인공지능 플랫폼의 <span class="br_span">이해 및 실습</span></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <td class="last_txt">- maum.ai 및 언어지능, 시각지능, 음성지능 등 엔진 소개 <br>- maum.ai 실습</td>&ndash;%&gt;--%>
<%--&lt;%&ndash;                    </tr>&ndash;%&gt;--%>
<%--                </table>--%>
<%--            </div>--%>
<%--            <div class="month_box" style="margin:0 auto 200px;">--%>
<%--&lt;%&ndash;                <h3>12월</h3>&ndash;%&gt;--%>
<%--                <table>--%>
<%--                    <colgroup>--%>
<%--                        <col><col width="140"><col width="140"><col><col>--%>
<%--                    </colgroup>--%>
<%--                    <tr>--%>
<%--                        <td rowspan="5" class="td_date">--%>
<%--                            <em><strong>26일</strong> (목)</em>--%>
<%--                            <span>Level 2. <br>--%>
<%--                                    <small style="font-size:12px;line-height: 19px; display: block;">How to customize <br>AI engines.<br>--%>
<%--                                    (Part 2. STT)</small>--%>
<%--                                </span>--%>
<%--                        </td>--%>
<%--                        <td class="td_txt"><strong>9:30-10:30</strong></td>--%>
<%--                        <td class="td_txt">데이터 구축 및 정제</td>--%>
<%--                        <td class="td_txt">- STT 모델 구축을 위한 데이터는 어떻게 구축하는가?</td>--%>
<%--                    </tr>--%>


<%--                    <tr>--%>
<%--                        <td class="td_txt last_txt"><strong>10:30-11:30</strong></td>--%>
<%--                        <td class="td_txt last_txt">학습 방법</td>--%>
<%--                        <td class="td_txt last_txt">- STT를 커스터마이징 하는 방법 <br>- AI 엔진 학습 실습</td>--%>
<%--                    </tr>--%>
<%--&lt;%&ndash;                    <tr>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <td class="last_txt"><strong>11:30-12:30</strong></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <td class="last_txt">인공지능 플랫폼의 <span class="br_span">이해 및 실습</span></td>&ndash;%&gt;--%>
<%--&lt;%&ndash;                        <td class="last_txt">- maum.ai 및 언어지능, 시각지능, 음성지능 등 엔진 소개 <br>- maum.ai 실습</td>&ndash;%&gt;--%>
<%--&lt;%&ndash;                    </tr>&ndash;%&gt;--%>
<%--                </table>--%>
<%--            </div>--%>
<%--        </div>--%>
        <div class="academy_main on" id="aca_industry">
            <div class="link_box">
                <div>
<%--                    <p>마인즈랩의 마음 아카데미는 인공지능에 관심이 있는 개인이나 기업을 위한 AI 교육 프로그램으로, maum.ai 유료 고객에게 마음 아카데미 교육을 제공합니다.--%>
<%--                        <span>마음 아카데미 커리큘럼은 인공지능 기술에 대해 전반적으로 알고자 하는 일반인을 위한 <em>소양 교육</em>과 </span>인공지능을 실제 비즈니스에 적용하고자 하는 실무자를 위한--%>
<%--                        <em>실무 교육</em>으로 구성됩니다.--%>

<%--                        어렵고 복잡한 인공지능 비즈니스를 마음 아카데미에서 쉽게 배워보세요!--%>
<%--                    </p>--%>
<%--                    <ul>--%>
<%--                        <li class="main_link">--%>
<%--                            <h5>소양 교육</h5>--%>
<%--                            <p>인공지능 기초지식 이곳에서 쌓자! <small>#인공지능이란  #인공지능시대살아남기</small></p>--%>
<%--                        </li>--%>
<%--                        <li class="main_link">--%>
<%--                            <h5>실무 교육</h5>--%>
<%--                            <p>인공지능 전문가가 되고 싶다면? <small>#TTS  #STT  #SDS</small></p>--%>
<%--                        </li>--%>
<%--                    </ul>--%>

<!-- 로그인 X인 경우 -->
<sec:authorize access="isAnonymous()">
    <a href="javascript:go_academy()">마음 아카데미 <small>바로가기</small> <em class="fas fa-chevron-right"></em></a>
</sec:authorize>

<!-- 로그인 O인 경우 -->
<sec:authorize access="isAuthenticated()">
    <a href="https://www.notion.so/maum-Academy-9b6c0a54b996410f85b06d1395965301" class="loginCheck" target="_blank">마음 아카데미 <small>바로가기</small> <em class="fas fa-chevron-right"></em></a>
</sec:authorize>

                </div>
            </div>
            <div class="overview">
                <h4>맞춤형 직무역량 강화 교육</h4>
                <span>AI Platform Overview</span>
                <ul class="overview_lst">
                    <li><h5>기업</h5><p>인공지능으로 비즈니스 경쟁력을 높이고 싶은 기업들을 위한 맞춤형 교육과 1:1 컨설팅</p>
                        <button id="seminar">맞춤형 인공지능 세미나</button>
                    </li>
                    <li><h5>대학 및 교육 기관</h5><p>인공지능 시대의 핵심 인재가 되기 위한 전문 교육. AI 플랫폼, 비즈니스 활용, 알고리즘 전문 교육 제공</p>
                        <button id="curriculum">대학교 인공지능 커리큘럼 예시</button>
                    </li>
                    <li><h5>AI 컨설턴트</h5><p>AI Transformation을 수행하고자 하는<br>
                        AI 컨설턴트를 위한 직무 전문성 교육 제공
                    </p>
                        <button id="schedule">AI 컨설턴트 교육 일정</button>
                    </li>

                </ul>
<%--                <a href="#" class="aca_apply">마음아카데미 신청하기</a>--%>

                <div class="aca_curriculum">
                    <h4>마음 아카데미 대표 커리큘럼</h4>
                    <div class="curriculum_box">
                        <table>
                            <colgroup>
                                <col><col><col><col>
                            </colgroup>
                            <tr>
                                <th class="">
                                목표
                                </th>
                                <td colspan="3" style="font-weight:500">AI Transformation을 이해하고 비즈니스에 인공지능을 도입할 수 있는 인공지능 인재 육성</td>
                            </tr>
                            <tr>
                                <th class="" rowspan="6">
                                  내용
                                </th>
                                <td colspan="2" class="green"><span>이론 과정</span></td>
                                <td class="blue"><span>실무 과정</span></td>
                            </tr>
                            <tr>
                                <td><em>maum.ai</em></td>
                                <td><em>AI Transformation</em></td>
                                <td><em class="blue">실무</em></td>
                            </tr>
                            <tr>
                                <td>인공지능 생태계</td>
                                <td>기업 조사 </td>
                                <td>현직자 멘토링</td>
                            </tr>
                            <tr>
                                <td>Cloud API</td>
                                <td>인공지능 정의</td>
                                <td>비즈니스 실무</td>
                            </tr>
                            <tr>
                                <td>maum.ai 실습</td>
                                <td>인공지능 계획</td>
                                <td></td>
                            </tr>
                            <tr class="last">
                                <td>AI Consultant</td>
                                <td>인공지능 도입</td>
                                <td></td>
                            </tr>
                            <tr>
                                <th class="">
                                    평가
                                </th>
                                <td colspan="2" style="color:#27b9ba;font-weight:500;font-size:15px;">인공지능 사업계획 발표</td>
                                <td style="color:#2cace5;font-weight:500;font-size:15px;">MAU 달성</td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="aca_consultant">
                    <div class="consultant_box">
                        <h4>AI 컨설턴트 모집</h4>
                        <p>마음AI와 함께할 AI 컨설턴트를 상시로 모집중에 있습니다. </p>
                        <a href="javascript:openConsultantPop()" class="">AI 컨설턴트 <small>자세히 보기</small> <em class="fas fa-chevron-right"></em></a>
                    </div>
                </div>
<%--                <div class="disc_box">--%>
<%--&lt;%&ndash;                    <img src="${pageContext.request.contextPath}/aiaas/common/images/bg_aca.png" alt="아카데미 이미지">&ndash;%&gt;--%>
<%--                    <div class="disc_desc">--%>
<%--                        <h4>마음아카데미만의 차별점</h4>--%>
<%--                        <p>우리 회사에 딱맞는 AI 직무 역량 강화교육,<br>--%>
<%--                            마인즈랩의 maum Academy와 함께 하세요. 맞춤형 교육 프로그램을 제공해드립니다.</p>--%>
<%--                        <ul>--%>
<%--                            <li><em class="fas fa-check"></em>언어지능에서부터 음성지능, 시각지능 까지 다양한 최신 인공지능 교육을 한번에!</li>--%>
<%--                            <li><em class="fas fa-check"></em>인공지능 플랫폼 실습 중심의 교육, 이수 후 자유롭게 인공지능 사용할 수 있을 때 까지 밀착 지원</li>--%>
<%--                            <li><em class="fas fa-check"></em>인공지능 활용한 비즈니스 전략 수립, 실행 가능한 아이템 프로토타입 제작 지원</li>--%>
<%--                        </ul>--%>

<%--&lt;%&ndash;                        <a href="https://docs.google.com/forms/d/1fQ1byg5AE5mFiRBQXrci5r2hqlC-WY7RKrBkUdClky4/edit" target="_blank" class="apply_btn">수강신청</a>&ndash;%&gt;--%>

<%--                    </div>--%>
                </div>
            </div>
        </div>
        <!-- #footer -->
        <%@ include file="../common/footer.jsp" %>
    </div>
</div>
<!-- //wrap -->

<script type="text/javascript">
    $(window).load(function() {
        $(document).ready(function (){

            $('.tab_list li').each(function(index){

                $(this).click(function(){
                    $('.tab_list li').removeClass('active');
                    $(this).addClass('active');
                    $('.academy_main ').hide();

                    if ($(this).hasClass('active')){
                        var cont = $(this).children().attr('class');
                        $("#"+cont).fadeIn();

                    }
                });
            });


            //팝업 창
            $('#seminar').on('click', function(){
                $('.seminar').fadeIn();
            });
            // $('#membership').on('click', function(){
            //     $('.membership').fadeIn();
            // });
            $('#schedule').on('click', function(){
                $('.schedule  ').fadeIn();
            });
            $('#curriculum').on('click', function(){
                $('.curriculum').fadeIn();
            });
            // 공통 팝업창 닫기
            $('.ico_close, .pop_bg').on('click', function () {
                $('.membership').fadeOut(300);
                $('.seminar').fadeOut(300);
                $('.curriculum').fadeOut(300);
                $('.schedule').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });

            openAcademy();
            setTimeout(function() {
                history.replaceState({}, null, location.pathname);
            }, 2000);



        });
    });
</script>

<script type="text/javascript">

    function go_academy(){
        window.location.href = "${google_url}" + "?targetUrl=http://maumacademy.maum.ai/";
    }

    function getParam(sname) {
        var params = location.search.substr(location.search.indexOf("?") + 1);
        var sval = "";
        params = params.split("&");
        for (var i = 0; i < params.length; i++) {
            temp = params[i].split("=");
            if ([temp[0]] == sname) { sval = temp[1]; }
    }
    return sval;
    }

    function openAcademy() {
        if("1" === getParam("openPopUp")) {
            window.open("https://www.notion.so/maum-Academy-9b6c0a54b996410f85b06d1395965301");
        }
    }

</script>