<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ include file="../common/common_header.jsp" %>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/swiper-6.2.0.css?ver=<%=fmt.format(lastModifiedStyle)%>">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/maum_landing.css?ver=<%=fmt.format(lastModifiedStyle)%>">
<!-- .page loading -->
<div id="pageldg">
    <span class="out_bg">
        <em>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <b>&nbsp;</b>
        </em>
    </span>
</div>
<!-- //.page loading -->

<!-- 로그인 X인 경우 -->
<%--<sec:authorize access="isAnonymous()">--%>
<%--	<!-- .lyr_event -->--%>
<%--	<div class="lyr_event">--%>
<%--		<div class="lyr_event_bg"></div>--%>
<%--		<!-- .lyrWrap -->--%>
<%--		<div class="lyrWrap">--%>
<%--			<em class="fas fa-times btn_lyrWrap_close"></em>--%>
<%--			<!-- .lyr_bd -->--%>
<%--			<div class="lyr_bd">--%>
<%--				<span>마음 AI 모든 서비스</span>--%>
<%--				<h1>무료 사용 Event</h1>--%>
<%--				<ul>--%>
<%--					<li><em>기간</em> 2020년 6월 1일 ~ 6월 10일</li>--%>
<%--					<li><em>대상</em>모든 신규 가입자</li>--%>
<%--					<li><em>혜택</em>카드 등록 및 결제 절차 없이,</li>--%>
<%--					<li><em></em>마음AI 모든 서비스 이용 가능</li>--%>
<%--				</ul>--%>
<%--				<div class="checkBox">--%>
<%--					<div>--%>
<%--						<input type="checkbox" id="check_stop" onclick="javascript:todaycloseWin();">--%>
<%--						<label for="check_stop" >오늘 하루 그만 보기</label>--%>
<%--					</div>--%>
<%--				</div>--%>
<%--				<div class="bottom border">--%>
<%--					<a class="cloudapi" href="/main/krMainHome" target="_blank" title="Cloud Api 바로가기">Cloud API <span>바로가기</span></a>--%>
<%--				</div>--%>
<%--				<div class="bottom">--%>
<%--					<a class="aibuilder" href="${mvp_maker_url}" target="_blank" title="AI Builder 바로가기">AI Builder<span>바로가기</span></a>--%>
<%--				</div>--%>

<%--			</div>--%>
<%--			<!-- //.lyr_bd -->--%>
<%--		</div>--%>
<%--		<!-- //.lyrWrap -->--%>
<%--	</div>--%>
<%--	<!-- //.lyr_event -->--%>
<%--</sec:authorize>--%>

<!-- .lyr_info -->
<div class="lyr_info">
	<div class="lyr_plan_bg"></div>
	<!-- .productWrap -->
	<div class="lyrWrap">
		<button class="btn_lyrWrap_close" type="button">닫기</button>
		<!-- .lyr_bd -->
		<div class="lyr_bd">
			<em class="far fa-address-card"></em>
			<p>고객 정보 입력</p>
			<ul>
				<li>
					<label for="info_name">*이름</label>
					<input id="info_name" type="text" value="" placeholder="필수 입력 사항">
				</li>
				<li>
					<label for="info_phone">*연락처</label>
					<input id="info_phone" type="tel" value="" placeholder="필수 입력 사항" pattern="[0-9]{3}-[0-9]{4}-[0-9]{4}">
				</li>
				<li>
					<label for="info_email">*이메일</label>
					<input id="info_email" type="email" value="" placeholder="필수 입력 사항">
				</li>
				<li>
					<label for="info_company">*회사명/직급</label>
					<input id="info_company" type="text" value="" placeholder="필수 입력 사항">
				</li>
			</ul>
			<button class="btn_blue" id="downloadPDF">다운로드</button>
		</div>
		<!-- //.lyr_bd -->
	</div>
	<!-- //.productWrap -->
</div>
<!-- //.lyr_info -->



<!--
AMR #wrap.maumUI 랜딩 '메인'페이지 헤더
랜딩 '메인'페이지에만 #wrap.maumUI 에 .visual_header 클래스가 같이 적용되어야 합니다.
-->
<div id="wrap" class="maumUI top_banner visual_header">
	<!-- .top_banner_area -->
	<div class="top_banner_area">
		<div class="banner_inner">
			<img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/img_kiosk.png" alt="kiosk image">

			<div class="text_box">
				<span>AI Human X 키오스크</span>
				<span>세상에 없던 <em>AI Human KIOSK</em>의 등장</span>
			</div>
			<a href="/event/kiosk" target="_blank" class="btn_go_event"><span>바로가기</span></a>

			<button type="button" class="btn_banner_close" title="닫기"><span class="hide">닫기</span></button>
		</div>
	</div>
	<!-- //.top_banner_area -->

	<div id="aside_btns">
		<a href="/inquiry" style="padding-top: 8px;" title="문의하기">
			<div class="btn_effect">
				<img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/ico_email_wh.svg" alt="문의하기">
			</div>
		</a>
		<%--
                <a href="#none" style="padding-top: 2px;" title="챗봇 상담하기">
                    <div class="btn_effect btn_chatbot">
                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/ico_chatbot_wh.svg" alt="챗봇 상담하기">
                    </div>
                </a>
        --%>
		<!-- AMR .page_top 적용되는 script : 875 line, 895 line  -->
		<a href="#none" style="padding-top: 10px;" class="page_top" title="위로가기">
			<div class="btn_effect">
				<i class="fas fa-chevron-up"></i>
			</div>
			<span>TOP</span>
		</a>
	</div>
	<!-- //우측 하단 aside, #aside_btns -->

	<%@ include file="../common/header.jsp" %>

	<!-- #container -->
	<div id="container">
		<!-- visual -->
		<div class="stn visual" name="AI Human">
			<div class="content">
				<div class="svc_txt">
					<div class="topBox">
						<h2 class="main_tit">AI Human</h2>
						<p class="sub_tit">artificial intelligence human</p>
					</div>
					<div class="btmBox">
						<p class="main_desc">인공인간</p>
						<p class="sub_desc">디지털 세계에서 사람과 상호 작용하며 소통하는 AI Human 서비스</p>

						<div class="btnBox">
							<a href="https://human.maum.ai/event/" target="_blank" class="btn_primary">서비스 소개</a>
							<a href="/home/estimate/krAhb2" target="_blank" class="btn_primary">AI Human 견적문의</a>
						</div>
					</div>
				</div>
			</div>
			<div class="videoBox">
				<video autoplay muted loop playsinline>
					<source src="${pageContext.request.contextPath}/aiaas/common/video/aiHuman_main.mp4" type="video/mp4">
				</video>
			</div>
		</div>
		<!-- //visual -->

		<!-- .aicc -->
		<div class="stn aicc" name="AICC">
			<div class="content">
				<div class="titArea fl">
					<p class="main_tit">AICC</p>
					<p class="sub_tit">인공지능 콜 센터</p>
					<p class="desc">운영 비용 부담을 덜고, 고객상담 커버율을 높여주는 서비스</p>

					<div class="btnBox">
						<a href="https://fast-aicc.maum.ai/login" target="_blank" title="새창으로 열기" class="btn_primary point_color">바로가기</a>
                        <a href="https://fast-aicc.maum.ai/intro" target="_blank" title="새창으로 열기" class="btn_primary">서비스 소개</a>
					</div>
				</div>

				<div class="figureArea fr">
					<div class="figureBox">
						<dl class="item dual">
							<dt>음성봇 성공 콜수</dt>
							<dd>
								<span>연간</span>
								<strong class="figure aicc_fgr01">768,000</strong>
								<span>콜</span>
							</dd>
						</dl>
						<dl class="item dual">
							<dt>은행 챗봇 거래 처리수</dt>
							<dd>
								<span>연간</span>
								<strong class="figure aicc_fgr02">3,600,000</strong>
								<span>건</span>
							</dd>
						</dl>
						<dl class="item">
							<dt>고객의 소리(VOC) 분석 콜수</dt>
							<dd>
								<div>
									<img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/img_kor.svg" alt="Korean flag">
									<span>연간</span>
									<strong class="figure aicc_fgr03">1억</strong>
									<span>콜</span>
								</div>
								<div>
									<img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/img_eng.svg" alt="American flag">
									<span>연간</span>
									<strong class="figure aicc_fgr04">1천만</strong>
									<span>콜</span>
								</div>
							</dd>
						</dl>
						<dl class="item">
							<dt>마케팅 콜 검사(TMQA) 계약수</dt>
							<dd>
								<span>연간</span>
								<strong class="figure aicc_fgr05">2,300,000</strong>
								<span>건</span>
							</dd>
						</dl>
					</div>
				</div>
			</div>
		</div>
		<!-- //.aicc -->

		<!-- .smartX -->
		<div class="stn smartX" name="SMART X">
			<div class="content">
				<div class="titArea fr">
					<p class="main_tit">SMART X</p>
					<p class="sub_tit">Smart Factory, City, Video, Edge AI</p>
					<p class="desc">인공지능 기반 영상 분석 기술이 융합된 Edge AI 서비스</p>

					<div class="btnBox">
						<a href="https://edge.maum.ai/home" target="_blank" title="새창으로 열기" class="btn_primary point_color">바로가기</a>
						<a href="https://edge.maum.ai/" target="_blank" title="새창으로 열기" class="btn_primary">서비스 소개</a>
					</div>
				</div>

				<div class="figureArea fl">
					<div class="figureBox">
						<dl class="item dual">
							<dt>Smart Factory</dt>
							<dd>
								<span>센서</span>
								<strong class="figure smartX_fgr01">수천개</strong>
								<span>데이터 기반으로<br> 예측 및 최적화</span>
								<img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/img_smartX_fgr01.svg" alt="Smart Factory">
							</dd>
						</dl>
						<dl class="item dual">
							<dt>Smart City</dt>
							<dd>
								<span>하루</span>
								<strong class="figure smartX_fgr02">백만대</strong>
								<span>서울시<br> 차량 번호판 인식 처리</span>
								<img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/img_smartX_fgr02.svg" alt="Smart City">
							</dd>
						</dl>
						<dl class="item">
							<dt>Smart 영상분석</dt>
							<dd>
								<span>하루</span>
								<strong class="figure smartX_fgr03">2만여개</strong>
								<span>광고 분석<br class="mobile"> 서비스 제공</span>
								<img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/img_smartX_fgr03.svg" alt="Smart 영상분석">
							</dd>
						</dl>
						<dl class="item">
							<dt>Edge AI 이상행동</dt>
							<dd>
								<span>하루</span>
								<strong class="figure smartX_fgr04">2,000</strong>
								<span>건 이상징후 탐지<br> 실신/침입/배회 이상행동 판독</span>
								<img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/img_smartX_fgr04.svg" alt="Edge AI 이상행동">
							</dd>
						</dl>
					</div>
				</div>
			</div>
		</div>
		<!-- //.smartX -->

		<!-- .minutes -->
		<div class="stn minutes" name="maum minutes">
			<div class="content">
				<div class="titArea fl">
					<p class="main_tit">maum <span>회의록</span></p>
					<p class="sub_tit">인공지능 회의록</p>
					<p class="desc">실시간으로 회의 내용을 정리해 주는 AI 회의록 서비스</p>

					<div class="btnBox">
						<a href="https://minutes.maum.ai" target="_blank" title="새창으로 열기" class="btn_primary point_color">바로가기</a>
						<a href="https://minutes.maum.ai/event" target="_blank" title="새창으로 열기" class="btn_primary">서비스 소개</a>
					</div>
				</div>

				<div class="figureArea fr">
					<div class="figureBox">
						<dl class="item dual">
							<dt>클라우드형<br class="pc"> 회의록</dt>
							<dd>
								<span>연간</span>
								<strong class="figure minutes_fgr01">400,000</strong>
								<span>분 사용</span>
							</dd>
						</dl>
						<dl class="item dual">
							<dt>구축형<br class="pc"> 회의록 시스템</dt>
							<dd>
								<span>연간</span>
								<strong class="figure minutes_fgr02">276,000</strong>
								<span>시간 사용</span>
							</dd>
						</dl>
						<dl class="item">
							<dt>온디바이스형 회의록</dt>
							<dd>
								<span>연간</span>
								<strong class="figure minutes_fgr03">29,000</strong>
								<span>시간 사용</span>
							</dd>
						</dl>
					</div>
				</div>
			</div>
		</div>
		<!-- //.minutes -->

		<!-- .cloudApi -->
		<div class="stn cloudApi" name="Cloud API">
			<div class="content">
				<div class="titArea fr">
					<p class="main_tit">Cloud API</p>
					<p class="sub_tit">인공지능 API 서비스</p>
					<p class="desc">음성, 시각, 언어 등 40여 개의 인공지능 엔진을 제공하는 구독형 API</p>

					<div class="btnBox">
						<a href="/main/krMainHome" target="_blank" title="새창으로 열기" class="btn_primary point_color">바로가기</a>
<%--						<a href="https://maum.ai/event/maumBook" target="_blank" title="새창으로 열기" class="btn_primary">서비스 소개</a> --%>
						<a href="/home/krcloudApiServiceMain" target="_blank" title="새창으로 열기" class="btn_primary">서비스 소개</a>
					</div>
				</div>

				<div class="figureArea fl">
					<div class="figureBox">
						<dl class="item dual">
							<dt>대교 소리동화 컨텐츠</dt>
							<dd>
								<strong class="figure cloudApi_fgr01">10,000</strong>
								<span>회 제공</span>
							</dd>
						</dl>
						<dl class="item dual">
							<dt>TTS 사용량</dt>
							<dd>
								<span>EJN, 아키핀, 투스라이프, 알에프탭, KBS</span>
								<span>연간</span>
								<strong class="figure cloudApi_fgr02">수십만</strong>
								<span>건</span>
							</dd>
						</dl>
						<dl class="item dual">
							<dt>영어발음/회화 평가 제공량</dt>
							<dd>
								<strong class="figure cloudApi_fgr03">4,500,000</strong>
								<span>회</span>
							</dd>
						</dl>
						<dl class="item dual">
							<dt>챗봇 대화량</dt>
							<dd>
								<span>연간</span>
								<strong class="figure cloudApi_fgr04">3,600,000</strong>
								<span>건</span>
							</dd>
						</dl>
						<dl class="item">
							<dt>AI 엔진 API 구독자 수</dt>
							<dd>
								<strong class="figure cloudApi_fgr05">1,000</strong>
								<span>명</span>
							</dd>
						</dl>
					</div>
				</div>
			</div>
		</div>
		<!-- //.cloudApi -->

		<!-- .maumData -->
		<div class="stn maumData" name="maum DATA">
			<div class="content">
				<div class="titArea fl">
					<p class="main_tit">maum DATA</p>
					<p class="sub_tit">인공지능 데이터 서비스</p>
					<p class="desc">AI 데이터 가공 서비스와 맞춤형 데이터 가공 툴 개발 서비스 </p>

					<div class="btnBox">
						<a href="https://data.maum.ai/builder" target="_blank" title="새창으로 열기" class="btn_primary point_color">바로가기</a>
						<a href="https://data.maum.ai" target="_blank" title="새창으로 열기" class="btn_primary">서비스 소개</a>
					</div>
				</div>

				<div class="figureArea fr">
					<div class="figureBox">
						<dl class="item dual">
							<dt>AI 데이터 라벨링<br class="pc"> 작업건수</dt>
							<dd>
								<span>연간</span>
								<strong class="figure maumData_fgr01">4,700,000</strong>
								<span>건</span>
							</dd>
						</dl>
						<dl class="item dual">
							<dt>AI 데이터 라벨링<br class="pc"> 프로젝트 건수</dt>
							<dd>
								<span>연간</span>
								<strong class="figure maumData_fgr02">1,787</strong>
								<span>건</span>
							</dd>
						</dl>
						<dl class="item">
							<dt>AI 데이터 라벨링<br class="pc"> 클라우드 작업자 수</dt>
							<dd>
								<span>연간</span>
								<strong class="figure maumData_fgr03">1,356</strong>
								<span>명</span>
							</dd>
						</dl>
					</div>
				</div>
			</div>
		</div>
		<!-- //.maumData -->
	</div>
	<!-- //#container -->

	<%@ include file="../common/footerLanding.jsp" %>
</div>
</div>
<!-- //wrap -->

<!-- 2 .pop_confirm -->
<div class="pop_confirm">
	<div class="pop_bg"></div>
	<!-- .popWrap -->
	<div class="popWrap" style="min-width:460px">
		<button class="pop_close" type="button">닫기</button>
		<!-- .pop_bd -->
		<div class="pop_bd">
			<!--아이콘 부분-->
			<em class="fas fa-exclamation-triangle"></em>
			<!--제목 부분-->
			<p>권장 브라우저 안내</p>
			<!--내용 부분-->
			<span>마음 AI는 <strong>크롬(Chrome) 브라우저</strong>에 최적화 되어있습니다.
				<span style="display:block;margin-top:4px;">크롬 브라우저에서 다양한 마음 AI의 엔진을 만나보세요!</span>
			</span>
		</div>
		<!-- //.pop_bd -->
		<!--창닫기 버튼 -->
		<div class="btn">
			<button class="btn_close" >창닫기</button>
		</div>
		<!--창닫기 버튼 -->
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_confirm -->


<script type="text/javascript">
	window.onload = function(){
		var browse = navigator.userAgent.toLowerCase();

		if( (navigator.appName == 'Netscape' && browse.indexOf('trident') != -1) || (browse.indexOf("msie") != -1)) {
			$('.pop_confirm').fadeIn();
		}
		// 공통 팝업창 닫기
		$('.pop_close, .pop_bg, .btn a, .btn button, .btn_close').on('click', function () {
			$('.pop_confirm').fadeOut(300);
		});
	};
</script>

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/swiper-6.2.0.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/text_animation.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/common/js/chatbot.js"></script>
<%--<script>
	window.addEventListener('message', fromChild);

	function fromChild(e) {
		console.log('#### from child iframe:', e.data);

		if(e.data.message == 'openChat') {
			$('#chatbotAvatar').css({'right':'32px','bottom':'24px', 'width':'320px','height':'568px'});
		} else if (e.data.message == 'closeChat') {
			$('#chatbotAvatar').css({'position':'fixed', 'right':'24px', 'bottom':'24px', 'width':'180px', 'height':'180px', 'z-index':'9999'});
		} else if (e.data.message == 'fullScreen') {
			$('#chatbotAvatar').css({'right':'0', 'bottom':'0', 'width':'100%', 'height':'100%'});
		}

	}
</script>--%>


<script type="text/javascript">

	var downloadFlag = "";

	$(window).load(function() {
		var errormsg = "${errormsg}";
		if(errormsg != '' && errormsg != null){
			alert(errormsg);
		}
		$('#pageldg').addClass('pageldg_hide2').delay(300).queue(function() { $(this).remove(); });
	});

	$(document).ready(function() {

		let $ClientId = "${client_id}";
		let $RedirectUri = "${redirect_uri}";
		let $SsoUrl = "${sso_url}";

		/* '무료 시작하기' 로그인 연동 */
		$('#freeStartBtn').on('click', function() {
			location.href = $SsoUrl+"/maum/oauthLoginMain" + "?response_type=code&client_id=" + $ClientId + "&redirect_uri=" + encodeURIComponent($RedirectUri);

		});

		function validateEmail(sEmail) {
			var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
			if (filter.test(sEmail)) {
				return true;
			}
			else {
				return false;
			}
		}

		// 소개서 다운받기 버튼
		$('.down_link').on('click', function (e) {
			$('.lyr_info').fadeIn();
			downloadFlag = $(this).attr('id');
		});
		$('.btn_blue').on('click',function(){
			var name = $('#info_name').val();
			var company = $('#info_company').val();
			var email = $('#info_email').val();
			var phone = $('#info_phone').val();

			if(name !== "" && company !== "" && validateEmail( email ) == true && phone !== ""){

				var title = $("#info_name").val() + " 님께서 자료를 다운로드 받았습니다.(KR)";
				var mailContents = "이름 : " + $("#info_name").val() + "<br>"
						+ "회사(소속) : " + $("#info_company").val() + "<br>"
						+ "Email : " + $("#info_email").val() + "<br>"
						+ "연락처 : " + $("#info_phone").val() + "<br>"
						+ "다운로드 받은 소개서 : " + downloadFlag;

				var formData = new FormData();
				formData.append('fromaddr', $("#info_email").val());
				formData.append('toaddr', 'hello@mindslab.ai');
				formData.append('subject', title);
				formData.append('message', mailContents);
				formData.append($("#key").val(), $("#value").val());

				$.ajax({
					type 	: 'POST',
					async   : true,
					url  	: '/support/sendContactMail',
					dataType : 'text',
					data: formData,
					processData: false,
					contentType: false,
					success: function (result) {
						$('#info_name').val("");
						$('#info_company').val("");
						$('#info_email').val("");
						$('#info_phone').val("");
						console.log(result.toString());
						console.log("sending info success");

						var route = " maum.ai : 소개서 자료 다운로드 ";
						var a = document.createElement('a');

						// 만약에 소개서 다운받기 버튼의 아이디값에 따라 다운로드 (maumCloud_dwn/ mvpmaker_dwn/ minutes_dwn/ fastaicc_dwn/ voicealbum_dwn/ ecominds_dwn)
						<%--클라우드API id=""--%>
						if( downloadFlag == 'maumCloud_dwn' ) {
							a.href = "${pageContext.request.contextPath}/aiaas/common/files/engine_Leaflet_ko.pdf";
							a.download = "[maum.ai] Cloud API 서비스 소개서.pdf";
						}
								<%--MVP메이커  id="mvpmaker_dwn"--%>
						else if ( downloadFlag == 'mvpmaker_dwn' ) {
							a.href = "${pageContext.request.contextPath}/aiaas/common/files/%5Bmaum.ai%5D%20AI%20Builder%20소개서_2020.pdf";
							a.download = "[maum.ai] AI Builder 서비스 소개서.pdf";
						}
								<%--maum회의록 id="minutes_dwn"--%>
						else if( downloadFlag == 'minutes_dwn') {
							a.href = "${pageContext.request.contextPath}/aiaas/common/files/maum%20회의록%20서비스%20소개서.pdf";
							a.download = "[maum.ai] maum 회의록 서비스 소개서.pdf";
						}
								<%--Fast AICC id="fastaicc_dwn"--%>
						else if( downloadFlag == 'fastaicc_dwn' ) {
							a.href = "${pageContext.request.contextPath}/aiaas/common/files/%5Bmaum.ai%5D%20FAST%20대화형%20AI%20서비스%20소개서.pdf";
							a.download = "[maum.ai] FAST 대화형 AI 서비스 소개서.pdf";
						}
								<%--보이스앨범  id="voicealbum_dwn"---%>
						else if( downloadFlag == 'ecominds_dwn' ) {
							a.href = "${pageContext.request.contextPath}/aiaas/common/files/eco%20MINDs%20컨설턴트%20소개서.pdf";
							a.download = "[maum.ai] eco MINDs 컨설턴트 소개서.pdf";
						}
								<%--에코마인즈 id="ecominds_dwn"--%>
						else if( downloadFlag == 'ecominds_dwn' ) {
							a.href = "${pageContext.request.contextPath}/aiaas/common/files/eco%20MINDs%20컨설턴트%20소개서.pdf";
							a.download = "[maum.ai] eco MINDs 컨설턴트 소개서.pdf";
						}
						a.style.display = 'none';
						document.body.appendChild(a);
						a.click();
						delete a;

						$('.lyr_info').hide();

					}, error: function (err) {
						console.log("parse HTML error! ", err);
					}
				});
			}else{
				var alertMsg = "필수 입력사항을 모두 알맞게 입력해 주세요.";
				alert( alertMsg );
			}
		});

		// 팝업 닫기
		$('.btn_lyrWrap_close').on('click', function () {
			$('.lyr_event').hide();
			$('body').css({
				'overflow': ''
			});
		});

		// // 팝업 닫기
		// $('.btn_lyrWrap_close').on('click', function () {
		// 	$(this).parent('.lyr_cont').remove();
		//
		// 	if( $('.lyr_cont').length == 0 ) {
		// 		$('.lyr_event').remove();
		// 		$('body').css({
		// 			'overflow': ''
		// 		});
		// 		clicked = true;
		// 	}
		// });
		// 팝업 닫기
		$('.lyr_event_bg').on('click', function () {
			$(this).parent().remove();
		});

		// swiper
		var swiper = new Swiper('.main_swiper', {
			autoResize: true,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			},
			loop: true,
			initialSlide: 0,
			spaceBetween: 300,
			simulateTouch: false,
			autoplay: true,
			effect: 'fade',
			autoplay: {
				// css animation-duration 에 deley와 동일한 값을 넣어야 합니다. ( 3000 = 3s )
				// css선택자 : .swiper-pagination-bullet.swiper-pagination-bullet-active:before {animation-duration: 3s;}
				delay: 10000,
				disableOnInteraction: false,
			},
		});


		// 팝업창 쿠키 적용시 주석 풀어서 사용하기 - 1
		/*
		cookiedata = document.cookie;
		if ( cookiedata.indexOf("ncookie=done") < 0 ){
			$('#wrap').addClass('top_banner'); //  팝업창 아이디
		} else {
			$('#wrap').removeClass('top_banner '); // 팝업창 아이디
		}
		*/
	});

	// 팝업창 쿠키 적용시 주석 풀어서 사용하기 - 2
	/*function setCookie( name, value, expiredays ) {
		var todayDate = new Date();
		todayDate.setDate( todayDate.getDate() + expiredays );
		document.cookie = name + "=" + escape( value ) + "; path=/; expires=" + todayDate.toGMTString() + ";"
	}

	function todaycloseWin() {
		setCookie( "ncookie", "done" , 1 );     // 저장될 쿠키명 , 쿠키 value값 , 기간( ex. 1은 하루, 7은 일주일)
		$('.btn_lyrWrap_close').click();
	}*/

</script>
<script type="text/javascript">

	function linkExtService(site_url) {
		var newWindow = window.open('about:blank');
		newWindow.location.href = "${pageContext.request.contextPath}/login/linkExtService?serviceUrl=" + site_url;
	}

</script>
<!-- AMR 공통으로 적용되는 script -->
<script type="text/javascript">
	// AMR fixed된 header에 가로스크롤을 적용하기 위한 코드
	$(window).scroll(function () {
		$('.maumUI.transform .maum_sta').css('left', 0 - $(this).scrollLeft());
	});

	// AMR #aside_btns 페이지 위로 버튼 display 이벤트
	$(window).scroll(function(){
		var scrollLocate = $(window).scrollTop();
		if (scrollLocate > 50) {
			$('#header').addClass('transform');
			$('#aside_btns .page_top').css('display', 'block');
		}
		if (scrollLocate < 50) {
			$('#header').removeClass('transform');
			$('#aside_btns .page_top').css('display', 'none');
		}
	});

	// AMR 최상단 배너 닫기 버튼 클릭 이벤트
	$('.btn_banner_close').on('click', function(){
		var $banner = $(this).parents('.top_banner_area');
		$banner.remove();
		$('#wrap').removeClass('top_banner');
	});

	// AMR page top 버튼 클릭 이벤트
	$('#aside_btns .page_top').on('click', function(){
		console.log('top click')

		$('html, body').animate({
			scrollTop: 0
		}, 500);
	});
</script>
<!-- AMR //공통으로 적용되는 script -->

<%-- 회의록 관련 스크립트 --%>
<%--<script type="text/javascript">--%>
<%--	$('.Button_Minutes').click(function () {--%>
<%--		// if(is_internal) {--%>
<%--		var newWindow = window.open('about:blank');--%>
<%--		newWindow.location.href = 'https://minutes.maum.ai/';--%>
<%--		// }--%>
<%--		// else {--%>
<%--		// 	alert('곧 만나요!\n\nmaum 회의록은 업그레이드 작업중입니다.\n구축 및 문의사항은 대표 이메일(hello@mindslab.ai)로 보내주세요.');--%>
<%--		// }--%>

<%--	});--%>
<%--</script>--%>