<%--
  Created by IntelliJ IDEA.
  User: YGE
  Date: 2019-07-13
  Time: 오전 11:40
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="../common/common_login_header.jsp" %>

<!--.wrap-->
<div id="wrap">
    <%@ include file="../common/login_header.jsp" %>
    <!-- #container -->
    <section id="container">
        <!-- .content -->
        <div class="content registrationContent">
            <div class="signUpCompleteWrap">
                <div class="paymentCompleteBox failed">
                    <h1>
                        <em class="fas fa-exclamation-circle"></em> 결제 정보 등록에 실패하였습니다.
                    </h1>
                    <p>실패사유를 확인하신 후 재시도를 하시거나, 계속 실패되는 경우<br>고객센터로 문의주시기 바랍니다.</p>
                    <ul>
                        <li>
                            <span>
                                <em class="fas fa-chevron-right"></em>실패사유
                            </span>
                            <span>${reason }</span>
                        </li>
                        <li>
                            <span>
                                <em class="fas fa-chevron-right"></em>문의
                            </span>
                            <span>support@mindslab.ai</span>
                        </li>
                    </ul>
                </div>

                <div class="btnBox">
                    <a href="/login/krPayment">돌아가기</a>
                </div>
            </div>

        </div>
        <!-- //.content -->
    </section>
    <!-- //#container -->

    <!-- #footer -->
    <%@ include file="../common/login_footer.jsp" %>
    <!-- //#footer -->

</div>
