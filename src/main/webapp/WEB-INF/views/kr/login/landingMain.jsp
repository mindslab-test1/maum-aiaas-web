<%--
  Created by IntelliJ IDEA.
  User: bliss
  Date: 2020-03-30
  Time: 오전 11:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<%--  CSS/JS 파일 캐싱 방지 --%>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>


<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
<%--    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">--%>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">

    <!-- Open Graph Tag -->
    <meta property="og:title"            content="maum.ai"/>
    <meta property="og:type"             content="website"/><!-- 웹 페이지 타입 -->
    <meta property="og:url"              content="https://maum.ai"/>
    <meta property="og:image"            content="${pageContext.request.contextPath}/aiaas/common/images/maum.ai_web.png"/>
    <meta property="og:description"      content="웹인공지능이 필요할땐 마음AI"/>

    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/common/css/all.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/reset.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/font.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/login_renew.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/landing.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-1.11.2.min.js"></script>

    <title>maum.ai</title>
</head>
<body>
<!-- .page loading -->
<div id="pageldg">
    <span class="out_bg">
        <em>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <b>&nbsp;</b>
        </em>
    </span>
</div>
<!-- //.page loading -->
<%--<header class="common_header">--%>
<%--    <div class="header_box">--%>
<%--        <h1><a href="/login/loginForm?lang=ko"><img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_title_navy.svg" alt="maum.ai logo"></a>--%>
<%--            <span>인공지능이 필요할 땐 마음AI</span>--%>
<%--        </h1>--%>
<%--        <!--.sta-->--%>
<%--        <div class="sta">--%>
<%--            <a href="/home/pricingPage?lang=ko" class="go_price">가격정책 </a>--%>
<%--            <a href="/home/academyForm" class="go_academy">마음 아카데미</a>--%>
<%--            <a class="btn_sign" href="javascript:login()">마음AI 로그인</a>--%>
<%--            <!--.etcMenu-->--%>
<%--            <div class="etcMenu">--%>
<%--                <ul>--%>
<%--                    <li class="lang">--%>
<%--                        <p class="lang_select">한국어<em class="fas fa-chevron-down"></em></p>--%>
<%--                        <ul class="lst">--%>
<%--                            <li><a href="/login/loginForm?lang=en" target="_self"><em>English</em></a></li>--%>
<%--                        </ul>--%>
<%--                    </li>--%>
<%--                </ul>--%>
<%--            </div>--%>
<%--            <!--//.etcMenu-->--%>
<%--        </div>--%>
<%--        <!--//.sta-->--%>
<%--    </div>--%>
<%--</header>--%>

<!-- pop_mic 팝업 -->
<div class="pop_simple" id="pop_mic">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap">
        <em class="fas fa-times pop_close"></em>
        <!-- .pop_bd -->
        <div class="pop_bd">
               <div class="circle">
                  <span class="circle__btn">
                    <em class="pause fas fa-pause" name="pause"></em>
                    <em class="play fas fa-play" name="play"></em>
                  </span>
                   <span class="circle__back-1"></span>
                   <span class="circle__back-2"></span>
               </div>
                <p>음성을 듣고 있습니다.</p>
        </div>
        <!-- //.pop_bd -->
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_mic -->

<!-- #container -->
<div id="container">
    <!-- .contents -->
    <div class="contents">
        <!-- .stn stn_landing -->
       <div class="stn stn_avatar">
           <div class="con_top">
               <div class="home_btn">
                   <a>
                       <em class="fas fa-home"></em>
                       처음으로
                   </a>
               </div>
               <h1><img src="${pageContext.request.contextPath}/aiaas/common/images/logo_new.svg" alt="maum.ai logo"></h1>
           </div>

           <div class="demo_box main_engin_area">
               <div class="con_fl">
                    <div class="chat_box">
                        <div class="chat_top">
                            <div class="bg_box"></div>
                            <h4> <em class="fas fa-comment-dots"></em>maum Assistant</h4>
                        </div>
                        <ul class="chat_area">
                            <li class="bot_text">maum.ai 에 대해 궁금하신가요?</li>
                            <li class="bot_tag">
                                <ul>
                                    <li><a class="tag" href="#none" data-display="서비스 소개" data-intent="서비스 소개">서비스 소개</a></li>
                                    <li><a class="tag" href="#none" data-display="회사 소개" data-intent="회사 소개">회사 소개</a></li>
                                    <li><a class="tag" href="#none" data-display="가격" data-intent="가격">가격</a></li>
                                    <li><a class="tag" href="#none" data-display="사용법" data-intent="사용법">사용법</a></li>
                                    <li><a class="tag" href="#none" data-display="마음에이아이가 뭐예요?" data-intent="마음에이아이가 뭐에요?">마음에이아이가 뭐에요?</a></li>
                                </ul>
                            </li>
                            <li class="custom_text">maum.ai가 뭐예요?</li>
                            <li class="bot_text">maum.ai는 마인즈랩의 다양한 엔진들을 경험해보고 사용이
                                가능한 인공지능 플랫폼입니다.</li>
                        </ul>
                        <div class="chat_input">
                            <button type="button" class="pop_mic"><img style="width:19px;" src="${pageContext.request.contextPath}/aiaas/common/images/engine/mic.svg"></button>
                            <input type="text" placeholder="직접 입력해 주세요.">
                        </div>
                    </div>
               </div>

               <div class="con_fr">
                   <div class="desc_box">
                       <h5>Description</h5>
                       <p>마인즈랩의 제품 및 서비스에 설명을 음성 으로 도와주는 <br>아바타입니다.</p>
                       <ul>
                           <li>
                               <span>Data</span>
                               <div class="graph_box">
                                   <div class="graph-circle">
                                       <small>학습데이터</small>
                                       <span class="graph-circle__count" data-graph-perc="10">10</span>
                                       <svg width="100" height="100">
                                           <circle class="graph-circle__backg" r="45" cx="50" cy="50" stroke-width="1" fill="none"></circle>
                                           <circle class="graph" r="42" cx="50" cy="50" stroke-width="3" stroke="#fff" fill="none"></circle>
                                           <circle class="graph-circle__backg" r="40" cx="50" cy="50" stroke-width="1"  fill="none"></circle>

                                           <linearGradient id="grad1">
                                               <stop offset="5%" stop-color="#7d71d1" />
                                               <stop offset="95%" stop-color="#ffebf2" />
                                           </linearGradient>

                                           <circle stroke="url(#grad1)" class="graph-circle__bar" id="purple" r="43" cx="150" cy="50" stroke-dasharray="200" stroke-dashoffset="100" stroke-width="6" fill="none" transform="rotate(-90 100 100)"></circle>
                                       </svg>
                                   </div>
                               </div>
                           </li>
                           <li>
                               <span>Accuracy</span>
                               <div class="graph_box">
                                   <div class="graph-circle">
                                       <small>정확도</small>
                                       <span class="graph-circle__count accuracy" data-graph-perc="65">65</span>
                                       <svg width="100" height="100">
                                           <circle class="graph-circle__backg" r="45" cx="50" cy="50" stroke-width="1" fill="none"></circle>
                                           <circle class="graph" r="43" cx="50" cy="50" stroke-width="3" stroke="#fff" fill="none"></circle>
                                           <circle class="graph-circle__backg" r="41.5" cx="50" cy="50" stroke-width="1"  fill="none"></circle>

                                           <linearGradient id="grad2">
                                               <stop offset="5%" stop-color="#27b9ba" />
                                               <stop offset="95%" stop-color="#cbdffd" />
                                           </linearGradient>
                                           <circle stroke="url(#grad2)" class="graph-circle__bar" id="blue" r="43" cx="150" cy="50" stroke-dasharray="200" stroke-dashoffset="100" stroke-width="6" fill="none" transform="rotate(-90 100 100)"></circle>
                                       </svg>
                                   </div>
                               </div>
                           </li>
                       </ul>
                   </div>
                   <div class="engin_bot">
                       <h5>Progression of engines</h5>
                       <div class="cont_box clearfix" id="bot_area">
<%--                           <div class="api_set clearfix ">--%>
<%--                               <div class="input_show" id="FlowNode_Input_0">--%>
<%--                                   <div class="input audiobox 1 "><span>입력값</span>--%>
<%--                                       <p>음성</p>--%>
<%--                                   </div>--%>
<%--                               </div>--%>
<%--                               <div class="api_show" id="FlowNode_Engine_0">--%>
<%--                                   <div class="api_bot" id="2">--%>
<%--                                       <span id=""> STT </span>--%>
<%--                                       <img src="${pageContext.request.contextPath}/aiaas/common/images/engine/ico_api_stt.png" title="STT: 음성을 텍스트로 변환">--%>
<%--                                       <div class="spinner"><strong class="fas fa-spinner"></strong></div>--%>
<%--                                   </div>--%>
<%--                               </div>--%>
<%--                               <div class="output_show active" id="FlowNode_Output_0">--%>
<%--                                   <div class="output textbox 1"><span>결과값</span>--%>
<%--                                       <p>텍스트</p>--%>
<%--                                   </div>--%>
<%--                               </div>--%>
<%--                           </div>--%>
<%--                           <div class="api_set clearfix ">--%>
<%--                               <div class="api_show" id="FlowNode_Engine_1">--%>
<%--                                   <div class="api_bot" id="38">--%>
<%--                                       <span id=""> 챗봇 </span>--%>
<%--                                       <img src="${pageContext.request.contextPath}/aiaas/common/images/engine/ico_api_weather.png" title="챗봇: 대화엔진">--%>
<%--                                       <div class="spinner"><strong class="fas fa-spinner"></strong></div>--%>
<%--                                   </div>--%>
<%--                               </div>--%>
<%--                               <div class="output_show" id="FlowNode_Output_1">--%>
<%--                                   <div class="output textbox blue"><span>결과값</span>--%>
<%--                                       <p>텍스트</p>--%>
<%--                                   </div>--%>
<%--                               </div>--%>
<%--                           </div>--%>
<%--                           <div class="api_set clearfix">--%>
<%--                               <div class="api_show" id="FlowNode_Engine_2">--%>
<%--                                   <div class="api_bot" id="1">--%>
<%--                                       <span id=""> TTS </span>--%>
<%--                                       <img src="${pageContext.request.contextPath}/aiaas/common/images/engine/ico_api_tts.png" title="TTS: 텍스트를 음성으로 변환">--%>
<%--                                       <div class="spinner"><strong class="fas fa-spinner"></strong></div>--%>
<%--                                   </div>--%>
<%--                                   <em class="fas fa-times api_close"></em></div>--%>
<%--                               <div class="output_show" id="FlowNode_Output_2">--%>
<%--                                   <div class="output audiobox 1"><span>결과값</span>--%>
<%--                                       <p>음성</p>--%>
<%--                                   </div>--%>
<%--                               </div>--%>
<%--                           </div>--%>
                       </div>
                   </div>
               </div>
           </div>


           <div class="demo_box main_select_area" >
               <ul>
                   <li>
                       <a href="#none" class="assistant_btn">
                           <em class="fas fa-building"></em>
                           maum.ai
                           <span>Assistant </span>
                       </a>
                   </li>
                   <li>
                       <a href="#none">
                           <em class="fas fa-cloud"></em>
                           Cloud API
                           <span>Assistant </span>
                       </a>
                   </li>
                   <li>
                       <a href="#none">
                           <em class="fas fa-project-diagram"></em>
                            AI Builder
                           <span>Assistant </span>
                       </a>
                   </li>
                   <li>
                       <a href="#none">
                           <em class="fas fa-file-audio"></em>
                           maum 회의록
                           <span>Assistant </span>
                       </a>
                   </li>
                   <li>
                       <a href="#none">
                           <em class="fas fa-headset"></em>
                           FAST 대화형 AI
                           <span>Assistant </span>
                       </a>
                   </li>
                   <li>
                       <a href="#none">
                           <em class="fas fa-compact-disc"></em>
                           Voice Album
                           <span>Assistant </span>
                       </a>
                   </li>
               </ul>
           </div>
           <div class="info_box">
<%--               <em class="fas fa-volume-up"></em>--%>
               <p> <span>maum.ai</span>에 오신걸 환영합니다. 저는 maum.ai 비서 Samantha 입니다.<br>
                   <strong>회사 소개, 서비스, 사용법</strong> 및 <strong>가격</strong> 등 무엇이든 물어보세요.</p>
           </div>
       </div>
        <!-- //.stn -->
    </div>
    <!-- //.contents -->
</div>
<!-- //#container -->

<script type="text/javascript">
    $(window).load(function() {
        $('#pageldg').addClass('pageldg_hide2').delay(300).queue(function() { $(this).remove(); });

    });

    $(document).ready(function() {

        $('.assistant_btn').on('click',function(){
            $('.main_select_area').hide();
            $('.main_engin_area').fadeIn();
            $('.home_btn').fadeIn();
            $('.info_box').html('maum.ai는 마인즈랩의 다양한 엔진들을 경험해보고 사용이<br> 가능한 인공지능 플랫폼입니다.\n')

        });

        $('.home_btn').on('click',function(){
            $('.main_select_area').fadeIn();
            $('.main_engin_area').hide();
            $('.home_btn').hide();
            $('.info_box').html(' <p> <span>maum.ai</span>에 오신걸 환영합니다. 저는 maum.ai 비서 Samantha 입니다.<br>\n' +
                '                   <strong>회사 소개, 서비스, 사용법</strong> 및 <strong>가격</strong> 등 무엇이든 물어보세요.</p>')
        });

        $('.pop_mic').on('click',function(){
            $('#pop_mic').fadeIn();
        });
        // 팝업창 닫기
        $('.pop_close').on('click', function () {
            $('#pop_mic').fadeOut(300);
            $('body').css({
                'overflow': ''
            });

        });

    });


    /*  play button */
    const play = document.querySelector('.play');
    const pause = document.querySelector('.pause');
    const playBtn = document.querySelector('.circle__btn');
    const wave1 = document.querySelector('.circle__back-1');
    const wave2 = document.querySelector('.circle__back-2');

    playBtn.addEventListener('click', function(e) {
        e.preventDefault();
        pause.classList.toggle('visibility');
        play.classList.toggle('visibility');
        playBtn.classList.toggle('shadow');
        wave1.classList.toggle('paused');
        wave2.classList.toggle('paused');
    });



    var path = $('.graph-circle__bar');
    $(path).animate({
        "strokeDashoffset" : 0
    }, 2000);

    if($(document).height()>1080){
        $('#container').css({'height':($(document).height())+'px'});

        $('.demo_box').css({
            'height':'calc(100% - 278px)',
        });
        $('.main_select_area').css({
            'height':'calc(100% - 315px)',
        });

        $('.con_fl .chat_box ul.chat_area').css({
            'height':'calc(100% - 160px)',
            'max-height':'unset',
        });
    }


    //engine setting
    function first_api_set(){
        $('#bot_area').append("<div class=\"api_set clearfix \">\n" +
    "                               <div class=\"input_show\" id=\"FlowNode_Input_0\">\n" +
    "                                   <div class=\"input audiobox 1 \"><span>입력값</span>\n" +
    "                                       <p>음성</p>\n" +
    "                                   </div>\n" +
    "                               </div>\n" +
    "                               <div class=\"api_show\" id=\"FlowNode_Engine_0\">\n" +
    "                                   <div class=\"api_bot\" id=\"2\">\n" +
    "                                       <span id=\"\"> STT </span>\n" +
    "                                       <img src=\"${pageContext.request.contextPath}/aiaas/common/images/engine/ico_api_stt.png\" title=\"STT: 음성을 텍스트로 변환\">\n" +
    "                                       <div class=\"spinner\"><strong class=\"fas fa-spinner\"></strong></div>\n" +
    "                                   </div>\n" +
    "                               </div>\n" +
    "                               <div class=\"output_show active\" id=\"FlowNode_Output_0\">\n" +
    "                                   <div class=\"output textbox 1\"><span>결과값</span>\n" +
    "                                       <p>텍스트</p>\n" +
    "                                   </div>\n" +
    "                               </div>\n" +
    "                           </div>");

    };

    function add_api_set(){
        $('#bot_area').append("<div class=\"api_set clearfix \">\n" +
    "                               <div class=\"api_show\" id=\"FlowNode_Engine_1\">\n" +
    "                                   <div class=\"api_bot\" id=\"38\">\n" +
    "                                       <span id=\"\"> 챗봇 </span>\n" +
    "                                       <img src=\"${pageContext.request.contextPath}/aiaas/common/images/engine/ico_api_weather.png\" title=\"챗봇: 대화엔진\">\n" +
    "                                       <div class=\"spinner\"><strong class=\"fas fa-spinner\"></strong></div>\n" +
    "                                   </div>\n" +
    "                               </div>\n" +
    "                               <div class=\"output_show\" id=\"FlowNode_Output_1\">\n" +
    "                                   <div class=\"output textbox blue\"><span>결과값</span>\n" +
    "                                       <p>텍스트</p>\n" +
    "                                   </div>\n" +
    "                               </div>\n" +
    "                           </div>");

    }

    first_api_set();
    add_api_set();
    add_api_set();



</script>

</body>
</html>
