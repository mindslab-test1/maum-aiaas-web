<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020/08/18
  Time: 1:54 오후
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>
<html>
<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
    <title>maum.ai platform</title>
    <!-- resources -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/reset.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/font.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/login.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- //resources -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-1.11.0.min.js"></script>

</head>
<body>
<div id="wrap">
    <header>
        <a href="https://maum.ai/" title="마음에이아이" target="_self"><img src="${pageContext.request.contextPath}/aiaas/common/images/maumai.svg" alt="maum.ai logo" /></a>
    </header>
    <section id="container">
        <div class="content">
            <div class="signUpWrap">
                <div class="signUpBox">
                    <form id="memberForm" name="memberForm" method="post">
                        <fieldset>
                            <legend>sign up form</legend>
                            <div class="input_box">
                                <div class="input_txt">
                                    <div class="email_box">
                                        <label for="email">이메일</label>
                                        <input type="email" name="Email" id="email" class="" value="">
                                        <button type="button" id="email_auth">이메일 인증</button>
                                        <span class="email" id="email_noti">인증 코드가 메일로 발송 되었습니다.</span>
                                    </div>
                                    <div class="code_box">
                                        <label for="code" style="display: none"></label>
                                        <input type="number" name="code" id="code" class="" value="" placeholder="인증 코드 입력">
                                        <em class="count">02:30</em>
                                        <button type="button" id="email_code">확인</button>
                                        <span class="email" id="code_noti">인증이 성공했습니다.</span>
                                    </div>
                                </div>
                                <div class="input_txt">
                                    <label for="password">비밀번호</label>
                                    <input type="password" name="password" id="password" class="" value="">
                                    <span class="noti">* 8~12자 영문, 숫자 포함</span>
                                </div>
                                <div class="input_txt">
                                    <label for="passwordCheck">비밀번호 재확인</label>
                                    <input type="password" name="passwordCheck" id="passwordCheck"  class="" value="">
                                    <span class="error password">비밀번호가 일치하지 않습니다.</span>
                                </div>
                                <div class="input_txt">
                                    <label for="name">이름</label>
                                    <input type="text" name="name" id="name" class="" value="">
                                </div>
                                <div class="input_txt">
                                    <label for="phone">연락처</label>
                                    <input type="tel" name="companyEmail" id="phone" class="" value="">
                                </div>
                                <div class="input_txt">
                                    <label for="companyEmail">회사/소속</label>
                                    <input type="text" name="companyEmail" id="companyEmail" class="" value="">
                                </div>

                            </div>
                            <div class="select_form">
                                <span>가입 경로</span>
                                <div class="selectarea">
                                      <span class="checks">
                                            <input type="checkbox" name="registerPath" id="1" class="ipt_check" value="포털 검색" >
                                            <label for="1"><em class="far fa-check-circle"></em> 포털 검색</label>
                                     </span>
                                    <span class="checks">
                                            <input type="checkbox" name="registerPath" id="2" class="ipt_check" value="세미나, 전시회" >
                                            <label for="2"><em class="far fa-check-circle"></em> 세미나, 전시회</label>
                                     </span>
                                    <span class="checks">
                                            <input type="checkbox" name="registerPath" id="3" class="ipt_check" value="영업사원" >
                                            <label for="3"><em class="far fa-check-circle"></em> 영업사원</label>
                                     </span>
                                </div>
                                <div class="selectarea">
                                    <span class="checks">
                                            <input type="checkbox" name="registerPath" id="4" class="ipt_check" value="광고, 홍보물" >
                                            <label for="4"><em class="far fa-check-circle"></em> 광고, 홍보물</label>
                                     </span>
                                    <span class="checks">
                                            <input type="checkbox" name="registerPath" id="5" class="ipt_check" value="관련 기사" >
                                            <label for="5"><em class="far fa-check-circle"></em> 관련 기사</label>
                                     </span>
                                </div>
                            </div>
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

                        </fieldset>
                    </form>
                    <a href="#none" class="signupBtn" title="가입하기" target="_self">가입하기</a>
                </div>
            </div>

            <div class="signUpCompleteWrap">
                <div class="signUpConfirmBox">
                    <p>회원가입이 완료 되었습니다.</p>
                    <ul>
                        <li><span><em class="fas fa-chevron-right"></em>이름</span>마인즈랩</li>
                        <li><span><em class="fas fa-chevron-right"></em>이메일</span>mindslab@mindslab.ai</li>
                    </ul>
                </div>

                <div class="txt">마음 AI 서비스 이용을 위해서는 결제 수단을 등록 하셔야 서비스 이용이 가능합니다.<br>
                    체험을 위해서 모든 고객은 <strong>첫달 무료</strong>이며, 결제수단을 등록해도 1달 동안 요금이 청구되지 않습니다.<br>
                    결제수단을 등록하여 더욱 편리하게 서비스를 이용해보세요.</div>

                <div class="signUpCompleteBox">
                    <h1>첫 1달 무료!</h1>
                    <p>결제 정보를 등록해도 1달 동안 요금이 청구되지 않습니다.</p>
                    <a class="registration" href="#none" title="결제정보 등록하기">결제정보 등록하기</a>
                </div>
                <span>해지 후, 재가입 시 바로 요금이 결제됩니다.</span>
                <div class="btn">
                    <a href="/home/login" title="로그인"> 로그인</a>
                    <a href="/"  title="메인으로 이동">메인으로 이동</a>
                </div>
            </div>

            <div class="paymentConfirmWrap ">
                <div class="paymentConfirmBox">
                    <h5>결제수단 등록이 완료 되었습니다.</h5>
                    <p>마음 AI의 모든 서비스를 마음껏 이용하세요.</p>
                </div>
                <div class="btn">
                    <a href="/home/login" title="로그인"> 로그인</a>
                    <a href="/"  title="메인으로 이동">메인으로 이동</a>
                </div>
            </div>

        </div>
    </section>
    <footer>
        <div class="footer">
            <a href="#none">이용약관 </a>  ㅣ
            <a href="#none"> 개인정보처리방침 </a>  ㅣ
            <a href="#none"> 고객센터 </a>
            <span>ㅣ</span>
            <div class="lang">
                <span class="lang_select">한국어 <em class="fas fa-chevron-up"></em></span>
                <ul>
                    <li class="active">한국어</li>
                    <li><a href="#none">English</a></li>
                </ul>
            </div>
        </div>
        <p>Copyright © 2020 주식회사 마인즈랩. All rights reserved.</p>
        <div class="m_lang">한국어 <a href="" title="English">English</a></div>
    </footer>
</div>

<script>
    // var SetTime = 5;		// 최초 설정 시간(기본 : 초)
    // function msg_time() {	// 1초씩 카운트
    //     m = Math.floor(SetTime / 60) + "분 " + (SetTime % 60) + "초";	// 남은 시간 계산
    //     var msg = m;
    //     SetTime--;					// 1초씩 감소
    //     if (SetTime < 0) {			// 시간이 종료 되었으면..
    //         clearInterval(tid);		// 타이머 해제
    //         alert("종료");
    //     }
    // }
    $('footer').css('position','relative');

    jQuery.event.add(window,"load",function() {
        $(document).ready(function () {

            //이메일 인증
            $('#email_auth').on('click',function(){
                $("#email_noti").fadeIn().css('display','block');
                $('.code_box').fadeIn();
                // msg_time();
            });
            //코드 인증
            $('#email_code').on('click', function(){
                $("#code_noti").fadeIn().css('display','block');
            });

            //비밀번호 체크
            $('#password').keyup(function(){
                $(this).removeClass('error');
                $('.password').hide();
            });

            //비밀번호 재확인 체크
            $('#passwordCheck').on('input keyup paste', function(){
                var password = $('#password').val();
                var password_check = $('#passwordCheck').val();
                if(password_check.length > 0){
                    if (password !== password_check) {
                        $('.password').css('display','block').addClass('error');
                    }else{
                        $('.password').hide();
                    }
                }else {
                    $('.password').hide();
                }
            });

            //가입하기 버튼
            $(".signupBtn").on('click', function(){
                //비밀번호 유효성 체크
                var pw = $("#password").val();
                var pw_input = $("#password");
                var num = pw.search(/[0-9]/g);
                var eng = pw.search(/[a-z]/ig);
                if(pw.length < 8 || pw.length > 12){
                    alert("비밀번호는 8자리 ~ 12자리 이내로 입력해주세요.");
                    $('.noti').addClass('error');
                    $('#passwordCheck').val('');
                    pw_input.addClass('error');
                    return false;
                }else if(pw.search(/\s/) != -1){
                    alert("비밀번호는 공백 없이 입력해주세요.");
                    $('.noti').addClass('error');
                    $('#passwordCheck').val('');
                    pw_input.addClass('error');
                    return false;
                }else if(num < 0 || eng < 0  ){
                    alert("영문,숫자를 혼합하여 입력해주세요.");
                    $('.noti').addClass('error');
                    $('#passwordCheck').val('');
                    pw_input.addClass('error');
                    return false;
                }else {
                    $('.signUpWrap').hide();
                    $('.signUpCompleteWrap').fadeIn();
                    return true;
                }
            });

            //결제 카드 등록하기 버튼
            $('.registration').on('click', function(){
                $('.signUpCompleteWrap').hide();
                $('.paymentConfirmWrap').fadeIn();
            });


            //footer 언어 체크
            $('.lang_select').on('click', function(){
                $(this).next().toggleClass('active');
            })

        });
    });
</script>
</body>
</html>

