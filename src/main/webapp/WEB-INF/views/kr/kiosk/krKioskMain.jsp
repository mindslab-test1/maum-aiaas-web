<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2019-10-14
  Time: 15:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <!-- Cache reset -->
    <meta http-equiv="Expires" content="Mon, 06 Jan 2016 00:00:01 GMT">
    <meta http-equiv="Expires" content="-1">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <!-- icon_favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/images_kiosk/ico_favicon_60x60.png"><!-- 윈도우즈 사이트 아이콘, HiDPI/Retina 에서 Safari 나중에 읽기 사이드바 -->
    <!-- css -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/css_kiosk/font.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/css_kiosk/reset.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/css_kiosk/kiosk.css">

    <title>maumAI KIOSK</title>
</head>

<body id="maumAI_kiosk">
<!-- .page loading -->
<div id="pageldg">
    <span class="out_bg">
        <em>
            <strong></strong>
            <strong></strong>
            <strong></strong>
            <b></b>
        </em>
    </span>
</div>
<!-- //.page loading -->

<!-- .kiosk_wrap -->
<div class="kiosk_wrap" >
    <h1><img src="${pageContext.request.contextPath}/aiaas/kr/images/images_kiosk/logo_maumAI.png" alt="maumAI"></h1>

    <div class="kiosk_container">
        <ul class="nav_kiosk">
            <li><a href="/cloudApi/tts/krTtsMain"><span>음성생성</span></a></li>
            <li><a href="/cloudApi/stt/krSttMain"><span>음성인식</span></a></li>
            <li><a href="/cloudApi/cnnoise/krCnnoiseMain"><span>음성 정제</span></a></li>
            <li><a href="/cloudApi/diarize/krDiarizeMain"><span>화자분리</span></a></li>
            <li><a href="/cloudApi/voicefilter/krVoicefilterMain"><span>Voice Filter</span></a></li>

            <li><a href="/cloudApi/textRemoval/krTextRemovalMain"><span>텍스트 제거</span></a></li>
            <li><a href="/cloudApi/tti/krTtiMain"><span>AI 스타일링</span></a></li>
            <li><a href="/cloudApi/idr/krIdrMain"><span>화폐&middot;고지서<br>인식</span></a></li>
            <li><a href="/cloudApi/avr/krAvrMain"><span>도로상의 객체<br>인식</span></a></li>
            <li><a href="/cloudApi/superResolution/krSuperResolutionMain"><span>Enhanced Super<br>Resolution</span></a></li>

            <li><a href="/cloudApi/nlu/krNluMain"><span>자연어 이해</span></a></li>
            <li><a href="/cloudApi/mrc/krMrcMain"><span>AI 독해</span></a></li>
            <li><a href="/cloudApi/xdc/krXdcMain"><span>텍스트 분류</span></a></li>
            <li><a href="/cloudApi/gpt/krGptMain"><span>문장 생성</span></a></li>
            <li><a href="/cloudApi/hmd/krHmdMain"><span>패턴 분류</span></a></li>
            <li><a href="/cloudApi/itf/krItfMain"><span>의도 분류</span></a></li>

            <li><a href="/cloudApi/nqa/krNqaMain"><span>금융 업무<br>상담 봇</span></a></li>
            <li><a href="/cloudApi/cloudApi/chatbot/krChatbotMain"><span>날씨봇, 위키봇</span></a></li>

            <li><a href="/cloudApi/engedu/krEngeduSttMain"><span>영어 교육용 STT</span></a></li>
            <li><a href="/cloudApi/engedu/krEngeduPronMain"><span>영어 문장<br>발음 평가</span></a></li>
            <li><a href="/cloudApi/engedu/krEngeduPhonicsMain"><span>파닉스 평가</span></a></li>

            <li><a href="/tts_train/krAiVoiceFontMain"><span>AI 보이스 폰트</span></a></li>
            <li><a href="/voicebot/krVoiceBotMain"><span>Voice 봇</span></a></li>
            <li><a href="/data/krSellDataMain"><span>판매 데이터</span></a></li>
            <li><a href="/data/krDataServiceMain"><span>데이터 정제<br>서비스</span></a></li>
            
            <li><a href="/cloudApi/vr/krVoiceRecogMain"><span>화자인증</span></a></li>
            <li><a href="/cloudApi/fr/krFaceRecogMain"><span>얼굴 인증</span></a></li>
            <li><strong><span>Coming<br>Soon</span></strong></li>
        </ul>
    </div>
    
    <div class="m_kiosk_container">
        <div class="visualBox">
            <p class="txt">인공지능이 필요할 땐, 마음AI</p>
            <div class="img_aniBox">
                <img src="${pageContext.request.contextPath}/aiaas/kr/images/images_kiosk/ico_maumAI_cloud.png" alt="마음AI 클라우드">
            </div>
        </div>
        <div class="m_lstBox">
            <dl>
                <dt><span>음성</span></dt>
                <dd>
                    <ul>
                        <li><a href="/cloudApi/tts/krTtsMain"><span>음성생성</span></a></li>
                        <li><a href="/cloudApi/voicefilter/krVoicefilterMain"><span>Voice Filter</span></a></li>
                        <li><a href="/cloudApi/stt/krSttMain"><span>음성인식</span></a></li>
                        <li><a href="/cloudApi/cnnoise/krCnnoiseMain"><span>음성 정제</span></a></li>
                        <li><a href="/cloudApi/diarize/krDiarizeMain"><span>화자분리</span></a></li>
                        <li><a href="/cloudApi/vr/krVoiceRecogMain"><span>화자인증</span></a></li>
                    </ul>
                </dd>
            </dl>
            <dl>
                <dt><span>언어</span></dt>
                <dd>
                    <ul>
                        <li><a href="/cloudApi/mrc/krMrcMain"><span>AI 독해</span></a></li>
                        <li><a href="/cloudApi/xdc/krXdcMain"><span>텍스트 분류</span></a></li>
                        <li><a href="/cloudApi/nlu/krNluMain"><span>자연어 이해</span></a></li>
                        <li><a href="/cloudApi/hmd/krHmdMain"><span>패턴 분류</span></a></li>
                        <li><a href="/cloudApi/gpt/krGptMain"><span>문장 생성</span></a></li>
                        <li><a href="/cloudApi/itf/krItfMain"><span>의도 분류</span></a></li>
                    </ul>
                </dd>
            </dl>
            <dl>
                <dt><span>시각</span></dt>
                <dd>
                    <ul>
                        <li><a href="/cloudApi/tti/krTtiMain"><span>AI 스타일링</span></a></li>
                        <li><a href="/cloudApi/idr/krIdrMain"><span>화폐&middot;고지서 인식</span></a></li>
                        <li><a href="/cloudApi/textRemoval/krTextRemovalMain"><span>텍스트 제거</span></a></li>
                        <li><a href="/cloudApi/avr/krAvrMain"><span>도로상의 객체인식</span></a></li>
                        <li><a href="/cloudApi/superResolution/krSuperResolutionMain"><span>ESR</span></a></li>
                        <li><a href="/cloudApi/fr/krFaceRecogMain"><span>얼굴 인증</span></a></li>
                    </ul>
                </dd>
            </dl>
            <dl>
                <dt><span>영어교육</span></dt>
                <dd>
                    <ul>
                        <li><a href="/cloudApi/engedu/krEngeduSttMain"><span>영어교육용 STT</span></a></li>
                        <li><a href="/cloudApi/engedu/krEngeduPronMain"><span>영어문장 발음 평가</span></a></li>
                        <li><a href="/cloudApi/engedu/krEngeduPhonicsMain"><span>파닉스 평가</span></a></li>
                    </ul>
                </dd>
            </dl>
        </div>
    </div>
</div>
<!-- //.kiosk_wrap -->

<!-- 공통 script -->
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-1.11.2.min.js"></script>
<!-- page Landing -->
<script type="text/javascript">
$(window).load(function() { 
	$(document).ready(function (){    
        //page loading delete  
        $('#pageldg').addClass('pageldg_hide').delay(300).queue(function() { $(this).remove(); });
        
        //UI transform
        $('body').each(function(){
            var winWidth = $(window).width();
            var winHeight = $(window).height();

            if ( winWidth > winHeight) {        
                $('.kiosk_container').css({
                    display: 'table-cell',
                });
                $('.m_kiosk_container').css({
                    display: 'none',
                });
            } else {
                $('.kiosk_container').css({
                    display: 'none',
                });
                $('.m_kiosk_container').css({
                    display: 'table-cell',
                });  
            }
        }); 
        
        //layer popup
        $('ul.nav_kiosk li a').on('click',function(event){
            event.preventDefault();
            $('.lyrWrap').remove();
            var iframeTit = $(this).text();
            var iframeLink= $(this).attr('href');
            console.log(iframeLink);
            var lyrWidth = $(window).width() * 90 / 100;
            var lyrHeight = $(window).height() * 90 / 100;

            var lyrWidthHalf =  lyrWidth / 2;
            var lyrHeightHalf = lyrHeight / 2;

            //Layer popup 생성
            $('body').append(' \
                <div class="lyrWrap"> \
                    <div class="lyr_bg"></div> \
                    <div class="lyrBox" style="margin:-'+lyrHeightHalf+'px 0 0 -'+lyrWidthHalf+'px"> \
                        <div class="lyr_top"> \
                            <h3>'+iframeTit+'</h3> \
                            <button class="btn_lyr_close">닫기</button> \
                         </div> \
                        <div class="lyr_mid"> \
                            <div id="pageldg"> \
                                <span class="out_bg"> \
                                    <em> \
                                        <strong></strong> \
                                        <strong></strong> \
                                        <strong></strong> \
                                        <b></b> \
                                    </em> \
                                </span> \
                            </div> \
                            <iframe frameborder="0" src='+iframeLink+'></iframe> \
                        </div> \
                    </div> \
                </div>'
            );
            //Layer popup 삭제
            $('.btn_lyr_close, .lyr_bg').on('click',function(){
                $(this).closest('.lyrWrap').addClass('lyr_hide').delay(500).queue(function() { $(this).remove(); });
            });

            $(this).contents().find('#wrap').addClass('kiosk');
            //iframe 접근
            $('iframe').load(function(){    // iframe이 모두 load된후 제어
                $(this).parent().find('#pageldg').addClass('pageldg_hide').delay(300).queue(function() { $(this).remove(); });
                $(this).contents().find('#wrap').addClass('kiosk');
                $(this).contents().find('#container').css({
                    "height": "auto",
                });
            });
        });
        //layer popup
        $('.m_kiosk_container .m_lstBox dl dd ul li a').on('click',function(event){
            event.preventDefault();
            $('.lyrWrap').remove();
            var miframeTit = $(this).children('span').text();
            var miframeLink= $(this).attr('href');
            console.log(miframeLink);
            var mlyrWidth = $(window).width() * 90 / 100;
            var mlyrHeight = $(window).height() * 90 / 100;

            var mlyrWidthHalf =  mlyrWidth / 2;
            var mlyrHeightHalf = mlyrHeight / 2;

            //Layer popup 생성
            $('body').append(' \
                <div class="lyrWrap"> \
                    <div class="lyr_bg"></div> \
                    <div class="lyrBox" style="margin:-'+mlyrHeightHalf+'px 0 0 -'+mlyrWidthHalf+'px"> \
                        <div class="lyr_top"> \
                            <h3>'+miframeTit+'</h3> \
                            <button class="btn_lyr_close">닫기</button> \
                         </div> \
                        <div class="lyr_mid"> \
                            <div id="pageldg"> \
                                <span class="out_bg"> \
                                    <em> \
                                        <strong></strong> \
                                        <strong></strong> \
                                        <strong></strong> \
                                        <b></b> \
                                    </em> \
                                </span> \
                            </div> \
                            <iframe frameborder="0" src='+miframeLink+'></iframe> \
                        </div> \
                    </div> \
                </div>'
            );
            //Layer popup 삭제
            $('.btn_lyr_close, .lyr_bg').on('click',function(){
                $(this).closest('.lyrWrap').addClass('lyr_hide').delay(500).queue(function() { $(this).remove(); });
            });

            $(this).contents().find('#wrap').addClass('kiosk');
            //iframe 접근
            $('iframe').load(function(){    // iframe이 모두 load된후 제어
                $(this).parent().find('#pageldg').addClass('pageldg_hide').delay(300).queue(function() { $(this).remove(); });
                $(this).contents().find('#wrap').addClass('kiosk');
                $(this).contents().find('#container').css({
                    "height": "auto",
                });
            });
        });
    });
    $(window).resize(function() {
        //UI transform
        $('body').each(function(){
            var winWidth = $(window).width();
            var winHeight = $(window).height();

            if ( winWidth > winHeight) {        
                $('.kiosk_container').css({
                    display: 'table-cell',
                });
                $('.m_kiosk_container').css({
                    display: 'none',
                });
            } else {
                $('.kiosk_container').css({
                    display: 'none',
                });
                $('.m_kiosk_container').css({
                    display: 'table-cell',
                });  
            }
        });    
    });
});
</script>


</body>
</html>
