<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2019-10-14
  Time: 15:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <!-- Cache reset -->
    <meta http-equiv="Expires" content="Mon, 06 Jan 2016 00:00:01 GMT">
    <meta http-equiv="Expires" content="-1">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <!-- icon_favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/images_kiosk/ico_favicon_60x60.png"><!-- 윈도우즈 사이트 아이콘, HiDPI/Retina 에서 Safari 나중에 읽기 사이드바 -->
    <!-- css -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/css_kiosk/font.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/css_kiosk/reset.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/css_kiosk/kiosk.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/pop_common.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/common/css/all.css">
    <title>maumAI KIOSK</title>
</head>

<body id="maumAI_kiosk">
<!-- .page loading -->
<div id="pageldg">
    <span class="out_bg">
        <em>
            <strong></strong>
            <strong></strong>
            <strong></strong>
            <b></b>
        </em>
    </span>
</div>
<!-- //.page loading -->


<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap centerBox">
        <button class="pop_close" type="button"> 닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="far fa-smile-wink"></em>
            <p>로그인 후 이용해 주세요.</p>
        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="hoverEffect"  href="javascript:login()">로그인 하기</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .kiosk_wrap -->
<div class="kiosk_wrap" >
    <h1><img src="${pageContext.request.contextPath}/aiaas/kr/images/images_kiosk/logo_maumAI.png" alt="maumAI"></h1>
    <a class="btn_login_kiosk" href="javascript:login()" >로그인</a>
    <div class="kiosk_container">
        <ul class="nav_kiosk">
            <li><a href="#none"><span>음성생성</span></a></li>
            <li><a href="#none"><span>음성인식</span></a></li>
            <li><a href="#none"><span>음성 정제</span></a></li>
            <li><a href="#none"><span>화자분리</span></a></li>
            <li><a href="#none"><span>Voice Filter</span></a></li>

            <li><a href="#none"><span>텍스트 제거</span></a></li>
            <li><a href="#none"><span>AI 스타일링</span></a></li>
            <li><a href="#none"><span>화폐&middot;고지서<br>인식</span></a></li>
            <li><a href="#none"><span>도로상의 객체<br>인식</span></a></li>
            <li><a href="#none"><span>Enhanced Super<br>Resolution</span></a></li>

            <li><a href="#none"><span>자연어 이해</span></a></li>
            <li><a href="#none"><span>AI 독해</span></a></li>
            <li><a href="#none"><span>텍스트 분류</span></a></li>
            <li><a href="#none"><span>문장 생성</span></a></li>
            <li><a href="#none"><span>패턴 분류</span></a></li>
            <li><a href="#none"><span>의도 분류</span></a></li>

            <li><a href="#none"><span>금융 업무<br>상담 봇</span></a></li>
            <li><a href="#none"><span>날씨봇, 위키봇</span></a></li>

            <li><a href="#none"><span>영어 교육용 STT</span></a></li>
            <li><a href="#none"><span>영어 문장<br>발음 평가</span></a></li>
            <li><a href="#none"><span>파닉스 평가</span></a></li>

            <li><a href="#none"><span>AI 보이스 폰트</span></a></li>
            <li><a href="#none"><span>Voice 봇</span></a></li>
            <li><a href="#none"><span>판매 데이터</span></a></li>
            <li><a href="#none"><span>데이터 정제<br>서비스</span></a></li>
            
            <li><a href="#none"><span>화자인증</span></a></li>
            <li><a href="#none"><span>얼굴 인증</span></a></li>
            <li><strong><span>Coming<br>Soon</span></strong></li>
        </ul>
    </div>
    <div class="m_kiosk_container">
        <div class="visualBox">
            <p class="txt">인공지능이 필요할 땐, 마음AI</p>
            <div class="img_aniBox">
                <img src="${pageContext.request.contextPath}/aiaas/kr/images/images_kiosk/ico_maumAI_cloud.png" alt="마음AI 클라우드">
            </div>
        </div>
        <div class="m_lstBox">
            <dl>
                <dt><span>음성</span></dt>
                <dd>
                    <ul>
                        <li><a href="#none"><span>음성생성</span></a></li>
                        <li><a href="#none"><span>Voice Filter</span></a></li>
                        <li><a href="#none"><span>음성인식</span></a></li>
                        <li><a href="#none"><span>음성 정제</span></a></li>
                        <li><a href="#none"><span>화자분리</span></a></li>
                        <li><a href="#none"><span>화자인증</span></a></li>
                    </ul>
                </dd>
            </dl>
            <dl>
                <dt><span>언어</span></dt>
                <dd>
                    <ul>
                        <li><a href="#none"><span>AI 독해</span></a></li>
                        <li><a href="#none"><span>텍스트 분류</span></a></li>
                        <li><a href="#none"><span>자연어 이해</span></a></li>
                        <li><a href="#none"><span>패턴 분류</span></a></li>                        
                        <li><a href="#none"><span>문장 생성</span></a></li>                        
                        <li><a href="#none"><span>의도 분류</span></a></li>
                    </ul>
                </dd>
            </dl>
            <dl>
                <dt><span>시각</span></dt>
                <dd>
                    <ul>
                        <li><a href="#none"><span>AI 스타일링</span></a></li>
                        <li><a href="#none"><span>화폐&middot;고지서 인식</span></a></li>
                        <li><a href="#none"><span>텍스트 제거</span></a></li>
                        <li><a href="#none"><span>도로상의 객체인식</span></a></li>
                        <li><a href="#none"><span>ESR</span></a></li>
                        <li><a href="#none"><span>얼굴 인증</span></a></li>
                    </ul>
                </dd>
            </dl>
            <dl>
                <dt><span>영어교육</span></dt>
                <dd>
                    <ul>
                        <li><a href="#none"><span>영어교육용 STT</span></a></li>
                        <li><a href="#none"><span>영어문장 발음 평가</span></a></li>
                        <li><a href="#none"><span>파닉스 평가</span></a></li>
                    </ul>
                </dd>
            </dl>
        </div>
    </div>
</div>
<!-- //.kiosk_wrap -->

<!-- 공통 script -->
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-1.11.2.min.js"></script>
<!-- page Landing -->
<script type="text/javascript">
$(window).load(function() {
    $(document).ready(function (){ 
        //page loading delete
        $('#pageldg').addClass('pageldg_hide').delay(300).queue(function() { $(this).remove(); });

        //UI transform
        $('body').each(function(){
            var winWidth = $(window).width();
            var winHeight = $(window).height();

            if ( winWidth > winHeight) {        
                $('.kiosk_container').css({
                    display: 'table-cell',
                });
                $('.m_kiosk_container').css({
                    display: 'none',
                });
            } else {
                $('.kiosk_container').css({
                    display: 'none',
                });
                $('.m_kiosk_container').css({
                    display: 'table-cell',
                });  
            }
        }); 
        
        $('ul.nav_kiosk li a').on('click',function(){
            $('.pop_simple').fadeIn(300);
        });
        
        $('.m_kiosk_container .m_lstBox dl dd ul li a').on('click',function(){
            $('.pop_simple').fadeIn(300);
        });        
        
        //팝업창 닫기
        $('.pop_close, .pop_bg, .btn a').on('click', function () {
            $('.pop_simple').fadeOut(300);
            $('body').css({
                'overflow': '',
            });
        });
    });
    $(window).resize(function() {
        //UI transform
        $('body').each(function(){
            var winWidth = $(window).width();
            var winHeight = $(window).height();

            if ( winWidth > winHeight) {        
                $('.kiosk_container').css({
                    display: 'table-cell',
                });
                $('.m_kiosk_container').css({
                    display: 'none',
                });
            } else {
                $('.kiosk_container').css({
                    display: 'none',
                });
                $('.m_kiosk_container').css({
                    display: 'table-cell',
                });  
            }
        });    
    });
});

</script>

<script type="text/javascript">
    function login(){
        location.href = "${google_url}" + "?targetUrl=/?lang=ko";
    }
</script>

</body>
</html>
