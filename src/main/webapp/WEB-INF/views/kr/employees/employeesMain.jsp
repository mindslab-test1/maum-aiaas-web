<%--
  Created by IntelliJ IDEA.
  User: bliss
  Date: 2020-03-05
  Time: 오후 2:10
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<%--  CSS/JS 파일 캐싱 방지 --%>
<%

    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>


<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">

    <!-- Open Graph Tag -->
    <meta property="og:title"            content="maum.ai"/>
    <meta property="og:type"             content="website"/><!-- 웹 페이지 타입 -->
    <meta property="og:url"              content="https://maum.ai"/>
    <meta property="og:image"            content="${pageContext.request.contextPath}/aiaas/common/images/maum.ai_web.png"/>
    <meta property="og:description"      content="웹인공지능이 필요할땐 마음AI"/>

    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/common/css/all.css">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/reset.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/font.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/login_renew.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/maum_common.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/employees.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/common.js?ver=<%=fmt.format(lastModifiedStyle)%>"></script>


    <title>직원용 페이지</title>
</head>
<body>
<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<div id="wrap" class="maumUI">

<%@ include file="../common/header.jsp" %>
<!-- #container -->
<div id="container">
    <!-- .contents -->
    <div class="contents">
        <!-- .stn stn_list -->
        <div class="stn">
            <!-- .cont_box -->
            <div class="cont_box">
                <div class="list_column">
                    <h3>Project &<br>
                        Human Resource</h3>
                    <ul class="project">
                        <li>
                            <a href="https://time.maum.ai:5443" title="Time Report" target="_blank">
                                <span>Time <br>Report</span>
                                <em class="far fa-clock"></em>
                            </a>
                        </li>
                        <li>
                            <a href="https://docs.google.com/spreadsheets/d/1c41lQDX7E1-Jh68rd7z9-BbAraUw7hcmuTg4ItvhDzs/edit?ts=5d679ca8#gid=0" title="주간 Project Assignment" target="_blank">
                                <span>Weekly <br>Project<br>Assignment</span>
                                <em class="far fa-calendar-check"></em>
                            </a>
                        </li>
                        <li>
                            <a href="https://docs.google.com/spreadsheets/d/1P4D5IXIJyIe_SDwCG6X07Dr4wcMkjTLTV5l0iPUqPqY/edit#gid=1661068797" title="Weekly Report 2020" target="_blank">
                                <span>Weekly <br>Report<br>2020</span>
                                <em class="far fa-calendar-alt"></em>
                            </a>
                        </li>
                        <li>
                            <a href="https://docs.google.com/spreadsheets/d/1g1cNJ8pKCe1pTMitu-13A6tb8-FlVW8h3DZCuTJDGBQ/edit?usp=sharing" title="서비스 품질 현황" target="_blank">
                                <span>maum.ai<br>서비스<br>품질현황</span>
                                <em class="far fa-chart-bar"></em>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.notion.so/MINDsLab-Job-Description-b42019a67d8a4ab0bd77c2f12101729d" title="Job Description" target="_blank">
                                <span>Job <br>Description</span>
                                <em class="far fa-file"></em>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="list_column">
                    <h3>Task</h3>
                    <ul class="task">
                        <li>
                            <a href="https://pms.maum.ai/jira" title="JIRA" target="_blank">
                                <span>JIRA</span>
                                <img src="${pageContext.request.contextPath}/aiaas/common/images/ico_jira.png" alt="jira icon">
                            </a>
                        </li>
                        <li>
                            <a href="https://pms.maum.ai/confluence" title="Confluence" target="_blank">
                                <span>Confluence</span>
                                <img src="${pageContext.request.contextPath}/aiaas/common/images/ico_jira.png" alt="conflouence icon">
                            </a>
                        </li>
                        <li>
                            <a href="https://pms.maum.ai/bitbucket" title="Bitbucket" target="_blank">
                                <span>Bitbucket</span>
                                <img src="${pageContext.request.contextPath}/aiaas/common/images/ico_bitbucket.png" alt="bitbutcket icon">
                            </a>
                        </li>
                        <li>
                            <a href="https://github.com/mindslab-ai" title="Github" target="_blank">
                                <span>Github</span>
                                <img src="${pageContext.request.contextPath}/aiaas/common/images/ico_github.png" alt="github icon">
                            </a>
                        </li>
                        <li>
                            <a href="https://docs.google.com/spreadsheets/d/1xXjqmxkpGDZxxueuSrGmkFlUaB3AlROyktkhK8OzSRs/edit?pli=1#gid=1290337715" title="프로젝트 관리" target="_blank">
                                <span>프로젝트 관리</span>
                                <em class="fas fa-project-diagram"></em>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="list_column">
                    <h3>공유사항</h3>
                    <ul class="common">
                        <li>
                            <a href="https://www.notion.so/On-boarding-Manual-b49a70066067461c81d9e148cce727c1" title="온보딩 매뉴얼" target="_blank">
                                <span>온보딩 매뉴얼</span>
                                <em class="fas fa-list-ol"></em>
                            </a>
                        </li>
                        <li>
                            <a href="https://drive.google.com/file/d/1cWOl0hG-oij6mvcjMXR1aJjrnqh4MANx/view?usp=sharing" title="process" target="_blank">
                                <span>업무 프로세스</span>
                                <em class="fas fa-braille"></em>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.tigrison.com/" title="Docswave" target="_blank">
                                <span>티그리스</span>
                                <em class="far fa-id-card"></em>
                            </a>
                        </li>
                        <li>
                            <a href="https://docs.google.com/spreadsheets/d/1J31mZQuPe_j6Q727aEoz-3MMqo2qWg9tQ0o1_gvcYLg/edit#gid=1234626334" title="과제비 현황" target="_blank">
                                <span>과제비 현황</span>
                                <em class="fas fa-hand-holding-usd"></em>
                            </a>
                        </li>
                        <li>
                            <a href="https://drive.google.com/open?id=1-HJ24tNyiYwjbzcyzziX1G68viRrulEI" title="Marketing Material" target="_blank">
                                <span>Marketing Material</span>
                                <em class="far fa-lightbulb"></em>
                            </a>
                        </li>

<%--                        <li>--%>
<%--                            <a href="" title="" target="_blank">--%>
<%--                                <span>기타 사항</span>--%>
<%--                                <em class="far fa-sticky-note"></em>--%>
<%--                            </a>--%>
<%--                        </li>--%>
                    </ul>
                </div>

            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn -->
    </div>
    <!-- //.contents -->
</div>
<!-- //#container -->
</div>
<script type="text/javascript">
    $(window).load(function() {
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
    });

    $(document).ready(function() {

        $('.more').on('click', function () {
            console.log('dkdkdkdk');
            if ($(this).parent().children().children('ul').is(':visible')){
                $(this).removeClass('fa-sort-up');
                $(this).addClass('fa-sort-down');
                $(this).parent().children().children('ul').hide();
            }else{
                $(this).removeClass('fa-sort-down');
                $(this).addClass('fa-sort-up');
                $(this).parent().children().children('ul').fadeIn(300);
            }

        })
    });

    $('#container').css({'height':($(document).height()-80)+'px'});
    $(window).resize(function(){
        $('#container').css({'height':($(document).height()-80)+'px'});
    });


</script>
<script type="text/javascript">

    <%--function login(){--%>
    <%--    location.href = "${google_url}" + "?targetUrl=/?lang=ko";--%>

    <%--}--%>

</script>

</body>
</html>
