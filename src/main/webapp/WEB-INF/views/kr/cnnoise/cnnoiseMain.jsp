<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">


<!-- 5 .pop_simple -->
<div class="pop_simple" id="api_upload_fail_popup">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>파일 업로드 실패</h5>
            <p>파일 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
                업로드 되지 않았습니다.</p>
            <span>
                * 지원가능 파일 확장자: .wav<br>
                * channels: mono<br>
                * 파일 용량 5MB 이하<br>
                * 음성 길이 30분 이하
            </span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="#">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- 5 .pop_simple -->
<div class="pop_simple " id="api_fail_popup">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap" style="position: inherit">
        <button class="pop_close" type="button">팝업 닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-exclamation-triangle"></em>
            <p>Timeout error</p>
            <p style="font-size: small"><strong>업로드한 파일을 확인해주세요</strong></p>
            <p style="font-size: small">* 지원가능 파일 확장자: .wav<br>
                * channels: mono<br>
                * 파일 용량 5MB 이하<br>
                * 음성 길이 30분 이하</p>

        </div>
        <!-- //.pop_bd -->
        <div class="btn" id="close_api_fail_popup">
            <a class="">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents">
    <!-- .content -->
    <div class="content api_content">
        <h1 class="api_tit">음성정제</h1>
        <ul class="menu_lst voice_menulst">
            <li class="tablinks" onclick="openTap(event, 'dndemo')" id="defaultOpen">
                <button type="button">엔진</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'dnexample')">
                <button type="button">적용사례</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'dnmenu')">
                <button type="button">매뉴얼</button>
            </li>
<%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <!-- .demobox -->
        <div class="demobox" id="dndemo">
            <p>어떤 환경에서도 또렷한 <span>음성 정제</span> <small>(Denoise)</small></p>
            <span class="sub">배경 음악 소리가 크던, 목소리의 울림이 심하던, 목소리가 가장 잘 들리게끔 음성을 정제해줍니다.</span>
            <!--dn_box-->
            <div class="demo_layout dn_box">
                <!--dn_1-->
                <div class="dn_1">
                    <div class="fl_box">
                        <p><em class="far fa-file-audio"></em><strong>샘플 파일</strong>로 테스트 하기</p>
                        <div class="sample_box">
                            <div class="sample first_sample">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="option" value="샘플1" checked>
                                    <label for="sample1" class="">샘플1</label>
                                </div>
                                <!--player-->
                                <div class="player">
                                    <div class="button-items">
                                        <audio id="music" class="music" preload="auto" onended="audioEnded($(this))">
                                            <source src="${pageContext.request.contextPath}/aiaas/kr/audio/denoise/msl-seo.wav"
                                                    type="audio/wav">
                                            <p>Alas, your browser doesn't support html5 audio.</p>
                                        </audio>
                                        <div id="slider" class="slider">
                                            <div id="elapsed" class="elapsed"></div>
                                        </div>
                                        <p id="timer" class="timer">0:00</p>
                                        <p class="timer_fr">0:00</p>
                                        <div class="controls">
                                            <div id="play" class="play sample_play">
                                            </div>
                                            <div id="pause" class="pause">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="sample">
                                <div class="radio">
                                    <input type="radio" id="sample2" name="option" value="샘플2">
                                    <label for="sample2" class="">샘플2</label>
                                </div>
                                <!--player-->
                                <div class="player">
                                    <div class="button-items">
                                        <audio id="music2" class="music" preload="auto" onended="audioEnded($(this))">
                                            <source src="${pageContext.request.contextPath}/aiaas/kr/audio/denoise/trump-cut.wav"
                                                    type="audio/wav">
                                            <p>Alas, your browser doesn't support html5 audio.</p>
                                        </audio>
                                        <div class="slider">
                                            <div class="elapsed"></div>
                                        </div>
                                        <p class="timer">0:00</p>
                                        <p class="timer_fr">0:00</p>
                                        <div class="controls">
                                            <div class="play sample_play">
                                            </div>
                                            <div class="pause">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--player-->
                            </div>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-audio"></em><strong>내 파일</strong>로 해보기</p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-image hidden"></em>
                                <label for="demoFile" class="demolabel">.wav 파일 업로드</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".wav">
                            </div>
                            <ul>
                                <li>* 지원가능 파일 확장자: .wav</li>
                                <li>* channels: mono</li>
                                <li>* 파일 용량 5MB 이하</li>
                                <li>* 음성 길이 30분 이하</li>

<%--                                <li>* 지원가능 파일 확장자: .wav</li>--%>
<%--                                <li>* 30분 길이 이하의 mono channel 음성을 이용해주세요.</li>--%>
<%--                                <li>* 파일명에 특수문자나 공백은 없애주세요.</li>--%>
<%--                                <li>* 2MB 이하의 음성 파일을 이용해주세요.</li>--%>
                            </ul>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_start" id="change_txt">음성 <span>정제하기</span></button>
                    </div>

                </div>
                <!--dn_1-->
                <!--dn_2-->
                <div class="dn_2">
                    <p><em class="far fa-file-audio"></em>음성 정제중</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px"
                             viewBox="0 0 128 16" xml:space="preserve"><path fill="#c9c3ec" fill-opacity="0.42"
                                                                             d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/>
                            <g>
                                <path fill="#7e71d1" fill-opacity="1"
                                      d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/>
                                <animatetransform attributeName="transform" type="translate"
                                                  values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0"
                                                  calcMode="discrete" dur="1820ms" repeatCount="indefinite"/>
                            </g></svg>

                        <p>약간의 시간이 소요 됩니다.</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
                    </div>

                </div>
                <!--dn_2-->
                <!--dn_3-->
                <div class="dn_3">
                    <div class="origin_file">
                        <p><em class="far fa-file-audio"></em> 원본 파일</p>
                        <div class="origin">
                            <!--player-->
                            <div class="player">
                                <div class="button-items">
                                    <audio id="music3" class="music" preload="auto" onended="audioEnded($(this))">
                                        <source src="${pageContext.request.contextPath}/aiaas/kr/audio/denoise.wav"
                                                type="audio/wav">
                                        <p>Alas, your browser doesn't support html5 audio.</p>
                                    </audio>
                                    <div class="slider">
                                        <div class="elapsed"></div>
                                    </div>
                                    <p class="timer">0:00</p>
                                    <p class="timer_fr">0:00</p>
                                    <div class="controls">
                                        <div class="play">
                                        </div>
                                        <div class="pause">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--player-->
                        </div>
                    </div>
                    <div class="result_file">
                        <p><em class="far fa-file-audio"></em> 음성 정제 파일</p>
                        <div class="result">
                            <!--player-->
                            <div class="player">
                                <div class="button-items">
                                    <audio id="music4" class="music" preload="auto" onended="audioEnded($(this))">
                                        <source src="${pageContext.request.contextPath}/aiaas/kr/audio/denoise.wav"
                                                type="audio/wav">
                                        <p>Alas, your browser doesn't support html5 audio.</p>
                                    </audio>
                                    <div class="slider">
                                        <div class="elapsed"></div>
                                    </div>
                                    <p class="timer">0:00</p>
                                    <p class="timer_fr">0:00</p>
                                    <div class="controls">
                                        <div class="play">
                                        </div>
                                        <div class="pause">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--player-->
                            <a id="btn_dwn" class="btn_dwn"><em class="far fa-arrow-alt-circle-down"></em> 결과 파일
                                다운로드</a>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
                    </div>

                </div>
                <!--dn_3-->
            </div>
            <!--// dn_box-->
            <span class="remark">* 배경음에 가사가 섞여 있을 경우, 소리가 너무 큰 경우에는 성능이 다소 낮게 나올 수 있습니다.</span>
        </div>
        <!-- //.demobox -->


        <!--.dnmenu-->
        <div class="demobox voice_menu" id="dnmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        음성정제 <small>(Denoise)</small>
                    </div>
                    <p class="sub_txt"> 마인즈랩의 음성정제 API는 잡음 뿐만 아니라, 배경 음악 소리, 울림소리 등 화자 목소리를 제외한 소리들을 정제하여 음성의 질을
                        높여줍니다.</p>

                    <span class="sub_title">
								준비사항
							</span>
                    <p class="sub_txt">Input: 음성파일 </p>
                    <ul>
                        <li>확장자: .wav</li>
                        <li>Channels: mono</li>
                        <li>길이: 30분 미만의 음성 파일</li>
                    </ul>
                    <span class="sub_title">
								 실행 가이드
							</span>
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/denoise/stream</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId</td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey</td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>file</td>
                            <td>type:file (wav) 음성파일 (5분 길이 이하)</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
<pre>
curl -X POST \
  'https://api.maum.ai/denoise/stream \
  -H 'content-type: multipart/form-data; boundary=----
WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -F apiId=(*ID 요청 필요) \
  -F apiKey=(*key 요청 필요) \
  -F 'file=@sample.wav'
</pre>
                    </div>

                    <p class="sub_txt">④ Response 예제 </p>

                    <div class="code_box">
                        <audio src="${pageContext.request.contextPath}/aiaas/kr/audio/denoise.wav" preload="auto"
                               controls></audio>
                    </div>
                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.dnmenu-->
        <!--.dnexample-->
        <div class="demobox" id="dnexample">
            <p><em style="font-weight: 400;">적용사례</em> <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
            <!-- //화폐, 고지서 인식(DIARL) -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>음성파일 보관</span>
                            </dt>
                            <dd class="txt">기존에 가지고 있는 잡음이 많은 음성 파일을 필요한 음성만 깨끗하게 보관할 수 있습니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_den"><span>음성 정제</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>음성인식 성능 <strong>향상</strong></span>
                            </dt>
                            <dd class="txt">다양한 음성인식 서비스 기기들의 인식률 향상을 위해 실제 서비스 전에 음성을 먼저 정제할 수 있습니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_den"><span>음성 정제</span></li>
                                    <li class="ico_stt"><span>STT</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //음성정제(Denoise) -->
        </div>
        <!--//.dnexample-->

    </div>
    <!-- //.content -->
</div>
<!-- //.contents -->


<script type="text/javascript">

    var timelineWidth = $('#slider').get(0).offsetWidth;
    var sample1File;
    var sample2File;
    var sample1SrcUrl;
    var sample2SrcUrl;


    // Set audio duration
    $('.music').each(function () {
        $(this).on("canplay", function () {
            var $parent = $(this).parent();
            var dur = this.duration;
            var fl_dur = Math.floor(dur);
            var endTime = toTimeFormat(fl_dur + "");
            $parent.children('.timer_fr').text(endTime);
        });
    });

    $(document).ready(function () {
        loadSample1();
        loadSample2();


        // Time update event
        $('.music').on("timeupdate", function () {
            var curIdx = $('.music').index($(this));
            timeUpdate($(this), curIdx);
        });

        // Audio play
        $('.play').on('click', function (me) {
            var curIdx = $('.play').index(this);

            $('.play').each(function (idx, e) {
                if (e !== me.currentTarget) {
                    $('.pause').eq(idx).css("display", "none");
                    $('.play').eq(idx).css("display", "block");
                    $('.music').eq(idx).trigger("pause");
                }
            });

            $('.music').eq(curIdx).trigger("play");
            $(this).css("display", "none");
            $('.pause').eq(curIdx).css("display", "block");
        });

        // Audio pause
        $('.pause').on('click', function () {
            var curIdx = $('.pause').index(this);

            $('.music').eq(curIdx).trigger("pause");
            $(this).css("display", "none");
            $('.play').eq(curIdx).css("display", "block");
        });


        $('.sample .sample_play').on('click', function () {
            var sampleNum = $('.sample .sample_play').index(this);
            $('input[type="radio"]:eq(' + sampleNum + ')').trigger('click');
        });

        $('input[type="radio"]').on('click', function () {
            $('em.close').trigger('click');
        });

        $('.fl_box').on('click', function () {
            if ($('.fl_box').css("opacity") === "0.5") {
                $('.fl_box').css("opacity", "1");
            }
        });


        $('.pop_close, .pop_bg, .btn a').on('click', function () {
            $('.pop_simple').fadeOut(300);
            $('body').css({
                'overflow': '',
            });
        });


        // 파일 업로드 후 이벤트
        document.querySelector("#demoFile").addEventListener('change', function () {

            let file = this.files[0];

            if (file === undefined || file === null || file == "") { return; }

            let name = file.name;
            let fileSize = file.size;
            let maxSize = 1024 * 1024 * 5; //5MB
            //console.log(fileSize);
            //console.log(maxSize);

            if (!file.type.match(/audio\/wav/) ) {
                console.log("wav 파일이 아닙니다.");
                this.value = null;
                $('#api_upload_fail_popup').fadeIn(300);

            } else if(fileSize > maxSize) {
                console.log("5MB 이하의 파일을 업로드해 주세요.");
                this.value = null;
                $('#api_upload_fail_popup').fadeIn(300);

            } else {

                let audio = document.createElement('audio');
                audio.preload = 'metadata';
                audio.onloadedmetadata = function() {
                    window.URL.revokeObjectURL(audio.src);
                    let dur =  audio.duration;
                    console.log (dur);

                    if (dur > 1800) { // 30분 제한
                        console.log("30분 이상의 파일은 업로드 할 수 없습니다.");
                        $("#demoFile").val(null);
                        $('#api_upload_fail_popup').fadeIn(300);

                    } else {

                        let size = (fileSize / 1048576).toFixed(3); //size in mb
                        $('input[type="radio"]:checked').prop("checked", false);
                        $('.demolabel').text(name + ' (' + size + 'MB)');
                        $('#uploadFile').removeClass('btn').addClass('btn_change');
                        $('.fl_box').css("opacity", "0.5");
                    }
                };
                audio.src = URL.createObjectURL(file);
                const delete1 = audio.delete;
            }

        });

        // Remove uploaded file
        $('em.close').on('click', function () {
            $('#demoFile').val(null);
            $('.demolabel').text('.wav 파일 업로드');
            $(this).parent().removeClass("btn_change");
            $(this).parent().addClass("btn");
            $('.fl_box').css('opacity', '1');
        });


        $('#change_txt, .btn_back2, .demolabel, .tablinks').on('click', function () {
            $('.music').each(function () {
                $('.pause').trigger('click');
            });
        });


        $('#api_fail_popup .pop_close, #api_fail_popup .btn').on('click', function () {
            $('.btn_back2').click();
        });


        // 음성 정제하기
        $('#change_txt').on('click', function () {
            var $checked = $('input[type="radio"]:checked');
            var $demoFile = $("#demoFile");
            var inputFile;
            var url;

            //내 파일로
            if ($checked.length === 0) {
                if ($demoFile.val() === "" || $demoFile.val() === null) {
                    alert("샘플을 선택하거나 파일을 업로드해 주세요.");
                    return 0;
                }
                inputFile = $demoFile.get(0).files[0];
                url = URL.createObjectURL(inputFile);
            }
            //샘플로
            else {
                if ($checked.val() === "샘플1") {
                    url = sample1SrcUrl;
                    inputFile = sample1File;
                } else if ($checked.val() === "샘플2") {
                    url = sample2SrcUrl;
                    inputFile = sample2File;
                }
            }

            $('#music3').attr('src', url);

            $('.dn_1').hide();
            $('.dn_2').fadeIn(300);

            startRequest(inputFile);

        });


        // Result -> 처음으로
        $('.btn_back2').on('click', function () {
            $('.dn_2').hide();
            $('.dn_3').hide();
            $('.dn_1').fadeIn(300);

            // audio UI setting
            $('.music').each(function () {
                this.currentTime = 0;
            });
            $('.elapsed').css("width", "0px");
            $('.timer').text("0:00");
        });
    });


    //------------------ Functions --------------------------

    // Update current play time and player bar
    //      obj : current .music jquery object
    //      idx : currnet .music index (= audio player index)
    function timeUpdate(obj, idx) {
        var cur_music = obj.get(0);
        var playHead = $('.elapsed').eq(idx).get(0);
        var timer = $('.timer').eq(idx).get(0);

        var playPercent = timelineWidth * (cur_music.currentTime / cur_music.duration);
        playHead.style.width = playPercent + "px";

        var secondsIn = Math.floor(cur_music.currentTime);
        var curTime = toTimeFormat(secondsIn + "");
        timer.innerHTML = curTime;
    }


    function audioEnded(audio) {
        var idx = $('.music').index(audio);
        // audio UI setting
        $('.pause').eq(idx).trigger('click');
        $('.music').eq(idx).get(0).currentTime = 0;
        $('.elapsed').eq(idx).css("width", "0px");
        $('.timer').eq(idx).text("0:00");
    }

    function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/aiaas/kr/audio/denoise/msl-seo.wav");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function () {
            blob = xhr.response;//xhr.response is now a blob object
            sample1File = new File([blob], "msl-seo.wav");
            sample1SrcUrl = URL.createObjectURL(blob);
        };

        xhr.send();
    }

    function loadSample2() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/aiaas/kr/audio/denoise/trump-cut.wav");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function () {
            blob = xhr.response;//xhr.response is now a blob object
            sample2File = new File([blob], "trump-cut.wav");
            sample2SrcUrl = URL.createObjectURL(blob);
        };

        xhr.send();
    }

    function startRequest(inputFile){
        var formData = new FormData();
        formData.append('noiseFile',inputFile);
        formData.append($("#key").val(), $("#value").val());

        var request = new XMLHttpRequest();

        request.responseType ="blob";
        request.onreadystatechange = function(){
            if (request.readyState === 4 && request.status !== 0){
                //console.log(request.response);

                var blob = request.response;
                var audioSrcURL = window.URL.createObjectURL(blob);

                var link = document.getElementById("btn_dwn");
                link.href = audioSrcURL;
                link.download = "denoise.wav" ;

                $('#music4').attr('src',audioSrcURL);
                $('.dn_2').hide();
                $('.dn_3').fadeIn(300);
                $('.play').eq(3).trigger('click');
            }
        };

        request.open('POST', '/api/denoise/stream');
        request.send(formData);
        request.timeout = 90000;
        request.ontimeout = function() {
            $('#api_fail_popup').show();
        };

        request.addEventListener("abort", function(){ });

        // step2->step1
        $('.btn_back1').on('click', function () {
            if(request){ request.abort(); }

            // audio UI setting
            $('.music').each(function(){ this.currentTime = 0; });
            $('.elapsed').css("width", "0px");
            $('.timer').text("0:00");

            $('.dn_2').hide();
            $('.dn_1').fadeIn(300);
        });
    }

    function toTimeFormat(text) {
        var sec_num = parseInt(text, 10); // don't forget the second param
        var hours = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        // if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {
            minutes = minutes;
        }
        if (seconds < 10) {
            seconds = "0" + seconds;
        }

        return minutes + ':' + seconds;
    }


</script>
<script>
    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>
