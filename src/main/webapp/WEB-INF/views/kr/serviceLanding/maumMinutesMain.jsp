<%--
  Created by IntelliJ IDEA.
  User: bliss
  Date: 2020-03-04
  Time: 오후 3:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<%--  CSS/JS 파일 캐싱 방지 --%>
<%

    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>


<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">

    <!-- Open Graph Tag -->
    <meta property="og:title"            content="maum.ai"/>
    <meta property="og:type"             content="website"/><!-- 웹 페이지 타입 -->
    <meta property="og:url"              content="https://maum.ai"/>
    <meta property="og:image"            content="${pageContext.request.contextPath}/aiaas/common/images/maum.ai_web.png"/>
    <meta property="og:description"      content="웹인공지능이 필요할땐 마음AI"/>

    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/common/css/all.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/reset.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/font.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/login_renew.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/sublanding.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-1.11.2.min.js"></script>


    <title>maum 회의록</title>
</head>
<body>
<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<!-- #container -->
<div id="container">
    <!-- .contents -->
    <div class="contents">
        <h1>아직도 회의록을 직접 타이핑한다고!?</h1>
        <h5>이제, <em>maum 회의록</em>으로 회의하세요.</h5>
        <!-- .stn  -->
        <div class="stn ">
            <!-- .cont_box -->
            <div class="cont_box">
                <!-- .fl_box -->
                <div class="fl_box">
                    <div class="intro_box">
                        <p ><em class="far fa-play-circle"></em> 회의록시스템</p>
                        <ul>
                            <li><span>00:00:03</span> <strong>부장님 </strong>  미정님, 미팅 전에..마케팅 자료 다 준비됐나요? </li>
                            <li><span>00:00:04</span> <strong>미정 대리 </strong>  네, 미팅 전에 이메일로 보내놓았는데, 잠깐 브리핑하고 회의 시작 하겠습니다.</li>
                            <li><span>00:00:05</span> <strong>준수 과장 </strong>  대리님 보내주신 자료 잠깐 확인했는데, 지난 주 이야기했던 내용이 반영이 안된거 같아요. 업데이트 날짜 확인해주실 수 있어요?</li>
                            <li><span>00:00:07</span> <strong>미정 대리 </strong>  네 회의 마치고 저도 파일 체크 해볼께요.  지난주에 이어서 마케팅 리서치 결과와 방향 설정 보고를 시작하고 이야기를 시작  해볼까요.</li>
                            <li><span>00:00:07</span> <strong>부장님 </strong>  네. 우리팀이 지난번 회의에서 이야기했던 유튜브 영상작업이 기대가 되네요. 오늘 회의에서는 조금 더 구체적인 내용이 나오면 좋겠어요.</li>
                        </ul>
                    </div>
                </div>
                <!-- //.fl_box -->
                <!-- .fr_box -->
                <div class="fr_box">
                    <ul>
                        <li>첫 가입 1달 무료</li>
                        <li><span>1</span>회의 녹음</li>
                        <li><span>2</span>maum 회의록에 녹음 파일 업로드</li>
                        <li><span>3</span>작성된 파일 확인 & 다운로드</li>
                        <li><small>또는</small>실시간 녹음 후 파일 확인 & 다운로드</li>
                    </ul>
                    <a href="" title="maum 회의록 시작하기" target="_parent">
                        maum 회의록 시작하기
                        <span>Sign in with Google</span>
                    </a>
                </div>
                <!-- //.fr_box -->
            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn  -->
    </div>
    <!-- //.contents -->
    <div class="contents sub_footer">
        <!-- .stn -->
        <div class="stn ">
            <!-- .cont_box -->
            <div class="cont_box">
                <a href="" titil="마음에이아이 로고" target="_blank">
                    <img src="${pageContext.request.contextPath}/aiaas/common/images/logo_new_maum_footer.svg" alt="maum.ai" >
                </a>
                <div class="fl">
                    <div class="corp_info">
                        <p>Contact us</p>
                        <a href="mailto:hello@mindslab.ai">hello@mindslab.ai</a>
                        <a href="tel:+8216613222">1661 3222</a>
                    </div>
                    <ul>
                        <li><a href="https://mindslab.ai:8080/kr/company" target="_blank" title="마인즈랩 회사소개">회사소개</a></li>
                        <li><a href="" target="_blank" title="이용약관">이용약관</a></li>
                        <li><a href="" target="_blank" title="개인정보처리방침">개인정보처리방침</a></li>
                    </ul>
                </div>
                <div class="fr">
                    <ul>
                        <li>
                            <span>AI 서비스</span>
                            <a href="" target="_blank" title="FAST AICC">FAST AICC</a>
                            <a href="" target="_blank" title="maum 회의록">maum 회의록</a>
                            <a href="" target="_blank" title="AI builder"> AI Builder</a>
                            <a href="" target="_blank" title="Voice 앨범">Voice 앨범</a>
                        </li>
                        <li>
                            <span>엔진 API</span>
                            <a href="" target="_blank" title="클라우드 서비스">클라우드 서비스</a>
                        </li>
                        <li>
                            <span>Data</span>
                            <a href="" target="_blank" title="maum Data">maum Data</a>
                        </li>
                    </ul>
                </div>
                <address><em>© Copyright 2020 주식회사 마인즈랩</em>  경기도 성남시 분당구 대왕판교로644번길 49 다산타워 6층 ㅣ대표 유태준 ㅣ사업자 등록번호 314-86-55446 </address>

            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn -->
    </div>
</div>
<!-- //#container -->

<script type="text/javascript">
    $(window).load(function() {
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
            $('.intro_box ul li').addClass('chat_line');
        });

        $(document).ready(function() {

            // $('.intro_box ul li').addClass('chat_line');
            // $('.fr_box ul li').addClass('text_ani');

        });

    });


</script>
</body>
</html>
