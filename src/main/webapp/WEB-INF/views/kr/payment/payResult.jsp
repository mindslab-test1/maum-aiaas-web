﻿    <!-- .contents -->
    <div class="contents">

        <div class="content">
            <h1>Pricing</h1>

            <!--.demobox-->
            <div class="demobox">
                <!-- .failure_box -->
                <div class="failure_box payresult">
                    <h1><em class="fas fa-gift"></em> 결제가 완료되었습니다</h1>
                    <!--<a href="https://console.maum.ai/" target="_blank" title="새창열림" class="btn_try">무료 체험하기</a>-->
                    <p>마이페이지 / 결제정보를 통해서 세부 내역을 확인하실 수 있습니다.</p>
                    <a href="/member/krPaymentInfo" class="linkto">결제내역 확인 바로가기</a>
                    <a href="/main/krMainHome">확인</a>
                </div>
                <!-- //.failure_box -->

            </div>
            <!--//.demobox-->
        </div>
    </div>
    <!-- //.contents -->
