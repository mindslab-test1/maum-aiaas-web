<%@page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
	<!-- 이니시스 표준결제 js -->
	<!-- 가맹점 URL이 http일 경우 http처리, 실제 오픈시 가맹점 MID로 stdpay로 처리 -->	
<!-- 	<script language="javascript" type="text/javascript" src="https://stgstdpay.inicis.com/stdjs/INIStdPay.js" charset="UTF-8"></script> -->
<script language="javascript" type="text/javascript" src="HTTPS://stdpay.inicis.com/stdjs/INIStdPay.js" charset="UTF-8"></script> 
<form id="payForm" name="" method="POST" >

	<!-- ***** 필 수 ***** -->
	<input type="hidden"  name="version" value="1.0" >
	<input type="hidden"  name="mid" value="${mid }" >
	<input type="hidden"  name="goodname" value="${goodName}" >
	<input type="hidden"  name="oid" value="${oid }" >
	<input type="hidden"  name="price" value="${price}" >
	<input type="hidden"  name="currency" value="WON" >
	<input type="hidden"  name="buyername" value="${fn:escapeXml(sessionScope.accessUser.name)}" >
	<input type="hidden"  name="buyertel" value="010-0000-0000" >	<!-- phone 번호 확인 필요 -->
	<input type="hidden"  name="buyeremail" value="${fn:escapeXml(sessionScope.accessUser.email)}" >
	<input type="hidden"  name="timestamp" value="${timestamp }" >
	<input type="hidden"  id="billingSignature" name="signature" value="${signature }" >
	<input type="hidden"  name="returnUrl" value="${siteDomain}/payment/commonPay">
	<input type="hidden"  name="mKey" value="${mKey }" >

	<!-- ***** 기본옵션 ***** -->
	<input type="hidden"  name="gopaymethod" value="Card">
<%--	<input type="hidden"  name="offerPeriod" value="${dateTo }-${dateFrom}" >--%>
	<input type="hidden"  name="acceptmethod" value="CARDPOINT:HPP(1):no_receipt:va_receipt:vbanknoreg(0):below1000" >
<!-- 	<input   id="billPrint_msg" name="billPrint_msg" value="고객님의 매월 결제일은 24일 입니다." > -->

	<!-- ***** 표시옵션 ***** -->
	<input type="hidden"  name="languageView" value="ko" >
	<input type="hidden"  name="charset" value="UTF-8" >
	<input type="hidden"  name="payViewType" value="overlay" >
	<input type="hidden"  name="closeUrl" value="${siteDomain }/payment/close" >

	<!-- ***** 추가옵션 ***** -->
<!-- 	<input  name="merchantData" value="" > -->
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
</form>

