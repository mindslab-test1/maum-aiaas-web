﻿		<!-- .contents -->
		<div class="contents">
			<div class="content api_content bot_content">
				<h1 class="api_tit">금융봇</h1>
				<ul class="menu_lst bot_lst">
					<li class="tablinks" onclick="openTap(event, 'nqabox')" id="defaultOpen"><button type="button">엔진</button></li>
					<li class="tablinks" onclick="openTap(event, 'nqaexample')"><button type="button">적용사례</button></li>
					<li class="tablinks" onclick="openTap(event, 'nqamenu')"><button type="button">매뉴얼</button></li>
<%--					<li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
				</ul>
				<div class="demobox" id="nqabox">
					<p><span style="color:#2cace5;">NQA</span>(Natural QA)를 이용한 금융 업무 상담 봇</p>
						<span class="sub">펀드, 연금, CMA, 주식, 선물, 매뉴얼, 용어 사전 등 금융 관련 상담 업무를 지원해 줍니다.<br>
							금융봇은 NQA를 이용한 예시 챗봇입니다. NQA를 활용하여 다양한 컨텐츠를 구현할 수 있습니다. 맞춤형 챗봇을 만들어 지금 바로 비즈니스에 활용해 보세요.</span>

					<!-- chatbot_box -->
					<div class="chatbot_box" id="NQA">
						<!-- .chat_mid -->
						<div class="chat_mid">
							<div class="bot_infoBox">
								<div class="thumb"><img src="${pageContext.request.contextPath}/aiaas/kr/images/chat_nqa_big.png" alt="chatbot"></div>
								<div class="txt">금융봇에게 질문해보세요. </div>
								<ul class="info_btnBox">
									<li><button type="button">자동화기기 무통장 서비스 비번 바꾸려 하는데요</button></li>
									<li><button type="button">폰뱅 무통장 거래 내역 조회 업무 메뉴</button></li>
									<li><button type="button">주식 매매 수수료가 어떻게 되나요?</button></li>
								</ul>
							</div>
							<ul class="talkLst">
								<li class="newDate">
									<span><!-- 날짜는 스크립트가 정의--></span>
								</li>
<%--								<li class="user">--%>
<%--                                    <span class="cont">--%>
<%--                                    <em class="txt">사용자가 입력한 텍스트 사용자가 입력한 텍스트 사용자가 입력한 텍스트 사용자가 입력한 텍스트 사용자가 입력한 텍스트</em>--%>
<%--                                    </span>--%>
<%--								</li>--%>

<%--									<!-- bot 채팅+버튼 UI -->  --%>
<%--								<li class="bot">--%>
<%--									<span class="thumb"><img src="${pageContext.request.contextPath}/aiaas/kr/images/chat_nqa.png" alt="chatbot"></span>--%>
<%--									<!-- 봇 메세지 (평문) -->--%>
<%--									<span class="cont">--%>
<%--                                        <em class="txt">안오늘 서울 강남구 도곡동 날씨입니다.<br> 흐림, 현재 온도는 24 (최저: 17, 최고:25), 습도는 45% 입니다. </em>--%>
<%--                                    </span>--%>
<%--									<div class="hashbox">--%>
<%--										<ul>--%>
<%--											<li>반대매매</li>--%>
<%--											<li>주문유형</li>--%>
<%--											<li class="on">예약주문</li>--%>
<%--											<li>주문유형</li>--%>
<%--											<li>수수료</li>--%>
<%--										</ul>--%>
<%--									</div>--%>
<%--								</li>--%>

<%--									<!-- bot 로딩 UI -->  --%>
<%--								<li class="bot">--%>
<%--									<span class="thumb"><img src="${pageContext.request.contextPath}/aiaas/kr/images/chat_nqa.png" alt="sully"></span>--%>
<%--									<span class="cont">--%>
<%--                                        <!-- 로딩 -->--%>
<%--                                        <em class="txt">--%>
<%--                                            <span class="chatLoading">--%>
<%--                                                <b class="chatLoading_item01"></b>--%>
<%--                                                <b class="chatLoading_item02"></b>--%>
<%--                                                <b class="chatLoading_item03"></b>--%>
<%--                                            </span>--%>
<%--                                        </em>--%>
<%--                                    </span>--%>
<%--								</li>--%>
							</ul>
						</div>
						<!-- //.chat_mid -->
						<!-- .chat_btm -->
						<div class="chat_btm">
							<form method="post" action="" id="formChat" name="formChat">
								<textarea class="textArea" placeholder="메세지를 입력해 주세요"></textarea>
								<input type="button" name="btn_chat" id="btn_chat" class="btn_chat" title="전송" value="전송">
							</form>
						</div>
						<!-- //.chat_btm -->

					</div>
					<!-- //.chatbot_box -->
					
				</div>
				<!-- .demobox -->

				<!--.nqamenu-->
				<div class="demobox bot_menu" id="nqamenu">
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API 공통 가이드
							</div>
							<p class="sub_title">개발 환경 세팅</p>
							<p class="sub_txt">1) REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
							<p class="sub_txt">2) 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
							<p class="sub_title">키 발급</p>
							<p class="sub_txt">1) Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
							<p class="sub_txt">2) 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
							<p class="sub_txt">3) [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
							<p class="sub_txt">4) 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
							<p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
						</div>
						<div class="guide_group">
							<div class="title">
								Natural QA <small>(NQA)</small>
							</div>
							<p class="sub_txt">챗봇의 기본 QA 엔진으로써 미리 등록되어 있는 질의응답 데이터를 이용하여 질문에 대한 답변을 제공하는 서비스에 활용합니다.</p>

						</div>
					</div>
					<!--//.guide_box-->
				</div>
				<!--//nqamenu-->
				<!--.nqaexample-->
				<div class="demobox" id="nqaexample">
					<p><em style="color:#2cace5;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>
					<span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
					<%--챗봇(chatbot)--%>
					<div class="useCasesBox">
						<ul class="lst_useCases">
							<li>
								<dl>
									<dt>
										<em>CASE 01</em>
										<span>챗봇 제작</span>
									</dt>
									<dd class="txt">목적에 맞는 커스텀 챗봇을 제작할 수 있습니다.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_sds"><span>SDS</span></li>
											<li class="ico_bqa"><span>NQA</span></li>
											<li class="ico_nlu"><span>NLU</span></li>
											<li class="ico_irqa"><span>IRQA</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>지식 포털</span>
									</dt>
									<dd class="txt">특정 분야를 전문적으로 다루는 지식 포털을 구축하는 데 활용할 수 있습니다.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_sds"><span>SDS</span></li>
											<li class="ico_bqa"><span>NQA</span></li>
											<li class="ico_nlu"><span>NLU</span></li>
											<li class="ico_mrc"><span>MRC</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 03</em>
										<span>대화형 <strong>교육 서비스</strong></span>
									</dt>
									<dd class="txt">기보유 데이터를 활용하여 대화로 이루어지는 교육 서비스를 구현할 수 있습니다.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_sds"><span>SDS</span></li>
											<li class="ico_bqa"><span>NQA</span></li>
											<li class="ico_nlu"><span>NLU</span></li>
											<li class="ico_tts"><span>TTS</span></li>
										</ul>
									</dd>
								</dl>
							</li>
						</ul>
					</div>
					<!-- //챗봇(chatbot) -->

				</div>
				<!--//.nqaexample-->


			</div>

		</div>
		<!-- //.contents -->

<input type="hidden" id="AUTH_ID">
<input type="hidden" id="SESSION_ID">
<input type="hidden" id="thumb">
<!-- //wrap -->
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/common.js"></script>	
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/chatbot/nqa.js?v=20190902"></script>
<script type="text/javascript">   
$(window).load(function() {    
	$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() { 
		$(this).remove(); 
	});
});
chatSingIn();
function chatSingIn() {
	$.ajax({
		url: 'https://aicc-prd1.maum.ai:9980/api/v3/auth/signIn',
		async: false,
		type: 'POST',
		headers: {
			"Content-Type": "application/json",
			"m2u-auth-internal": "m2u-auth-internal"
		},
		data: JSON.stringify({
			"userKey": "admin",
			"passphrase": "1234"
		}),
		dataType: 'json',
		success: function(data) {
			console.log(data.directive.payload.authSuccess.authToken);

			document.getElementById('AUTH_ID').value = data.directive.payload.authSuccess.authToken;
			console.log(document.getElementById('AUTH_ID').value);
			console.log('login success!')

		}, error: function(err) {
			console.log("chatbot Login error! ", err);
		}
	});
}


</script>
<script type="text/javascript">
jQuery.event.add(window,"load",function(){
	$(document).ready(function (){
		var chatName = $('.chatbot_box').attr('id');
		document.getElementById('thumb').value='/aiaas/kr/images/chat_nqa.png';
		chatOpen(chatName);
		chatNm = chatName;
	});
});

//API 탭
function openTap(evt, menu) {
	var i, demobox, tablinks;
	demobox = document.getElementsByClassName("demobox");
	for (i = 0; i < demobox.length; i++) {
		demobox[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(menu).style.display = "block";
	evt.currentTarget.className += " active";
}
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


