<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-07-08
  Time: 09:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
    <div class="content api_content">
        <h1  class="api_tit">한글 변환</h1>
        <ul class="menu_lst">
            <li class="tablinks" onclick="openTap(event, 'conv_demo')" id="defaultOpen"><button type="button">엔진</button></li>
            <li class="tablinks" onclick="openTap(event, 'conv_example')"><button type="button">적용사례</button></li>
            <li class="tablinks" onclick="openTap(event, 'conv_menu')"><button type="button">매뉴얼</button></li>
            <%--			<li class="tablinks"><a href="/member/krApiAccount" >API ID, key 발급</a></li>--%>
        </ul>
        <!--.conv_demo-->
        <div class="demobox demobox_nlu" id="conv_demo">
            <p><span>한글 변환</span> <small>(Konglish)</small></p>
            <span class="sub">영어 단어 또는 한글과 영어가 혼용된 문장에서 영어 단어들을 외래어 표기법에 가까운 한글로 변환시켜줍니다.</span>

            <!--.itf_box-->
            <div class="itf_box correct_box conversion_box">
                <!--.step01-->
                <div class="step01">
                    <div class="select_box">
                        <span>유형</span>
                        <div class="radio">
                            <input type="radio" name="conversion" id="word" checked value="word"/>
                            <label for="word">단어</label>
                        </div>
                        <div class="radio">
                            <input type="radio" name="conversion" id="sentence" value="sentence"/>
                            <label for="sentence">문장</label>
                        </div>
                    </div>

                    <div class="demo_top">
                        <em class="far fa-list-alt"></em><span>샘플 선택</span>
                    </div>
                    <div class="ift_topbox">
                        <p>한글로 변환시킬 영어 <span class="change_txt">단어</span>를 선택해 주세요.</p>
                        <ul class="ift_lst word_lst">
                            <li>
                                <div class="radio">
                                    <input type="radio" id="radio" name="word" value="Disneyland" checked>
                                    <label for="radio">Disneyland</label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="radio2" name="word" value="Magic Carpet">
                                    <label for="radio2">Magic Carpet</label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="radio3" name="word" value="Library">
                                    <label for="radio3">Library</label>
                                </div>
                            </li>
                        </ul>
                        <ul class="ift_lst sentence_lst">
                            <li>
                                <div class="radio">
                                    <input type="radio" id="radio4" name="sentence" value="Java는 Simple하다.">
                                    <label for="radio4">Java는 Simple하다.</label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="radio5" name="sentence" value="이번 Project의 Leader는 홍길동 Manager입니다.">
                                    <label for="radio5">이번 Project의 Leader는 홍길동 Manager입니다.</label>
                                </div>
                            </li>

                        </ul>
                    </div>
                    <p class="txt_or">또는</p>
                    <div class="demo_top">
                        <em class="far fa-keyboard"></em><span><span class="change_txt">단어</span> 입력</span>
                    </div>
                    <div class="text_area">
                        <textarea id="text-contents" rows="10" placeholder="영어 단어를 입력해 주세요." maxlength="1000"></textarea>
                        <div class="text_info">
                            <span class=""><strong id="count">0</strong>/1000자</span>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_search" id="find_btn">한글 변환</button>
                    </div>
                </div>
                <!--//.step01-->

                <div class="loading_wrap">
                    <p><em class="far fa-file-alt"></em> 한글 변환중</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#c9c3ec" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#27c1c1" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

                        <p>약간의 시간이 소요 됩니다.</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1" ><em class="fas fa-redo"></em>처음으로</button>
                    </div>

                </div>


                <!--.step02-->
                <div class="step02">
                    <div class="demo_top">
                        <em class="far fa-file-alt"></em><span> 한글 변환 결과</span>
                    </div>
                    <div class="result_box">
                        <ul>
                            <li>
                                <span>입력</span>
                                <p id="input_sentence"></p>
                            </li>
                            <li>
                                <span>한글 변환</span>
                                <p id="result_sentence"></p>
                            </li>
                        </ul>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_reset" id="btn_reset"><em class="fas fa-redo-alt"></em>처음으로</button>
                    </div>
                </div>
                <!--//.step02-->
            </div>
            <!--//.itf_box-->

        </div>
        <!-- .conv_demo -->

        <!--.conv_menu-->
        <div class="demobox" id="conv_menu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>
                <div class="guide_group">
                    <div class="title">한글 변환 <small>(Konglish)</small></div>
                    <p class="sub_txt">영어 단어 또는 한글과 영어가 혼용된 문장에서 영어 단어들을 외래어 표기법에 가까운 한글로 변환시켜줍니다.</p>
                    <span class="sub_title">
							준비사항
						</span>
                    <p class="sub_txt">① Input: 영어 단어 또는 영어와 한글이 혼용되어 있는 문장</p>

                    <span class="sub_title">
							 실행 가이드 : 영어 단어
					</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL :  https://api.maum.ai/konglish/words</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId</td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>wordList</td>
                            <td>콩글리시로 변환할 영어 단어 </td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
							<pre>
curl -X POST \
http://api.maum.ai/konglish/words \
-H 'Content-Type: application/json' \
-H 'cache-control: no-cache' \
-d '{
        "apiId": {발급받은 API ID},
        "apiKey":{발급받은 API KEY},
        "wordList": [
                "apple","bee"
        ]
}
</pre>
                    </div>

                    <p class="sub_txt">④ Response 파라미터 설명 </p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>타입</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>API 동작여부</td>
                            <td>list</td>
                        </tr>
                        <tr>
                            <td>result</td>
                            <td>콩글리시로 변환된 영어 단어 목록</td>
                            <td>list</td>
                        </tr>
                    </table>
                    <span class="table_tit">message: API 동작여부</span>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>타입</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>요청 처리 상태를 설명하는 문자열 (Success/ Fail)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>요청 처리 상태에 대한 status code (0: Success)</td>
                            <td>number</td>
                        </tr>
                    </table>

                    <p class="sub_txt">⑤ Response 예제 </p>
                    <div class="code_box">
<pre>
{
        "message": {
                "message": "Success",
                "status": 0
        },
        "result": [
                "애플",
                "비"
        ]
}
</pre>

                    </div>


                    <span class="sub_title">
							 실행 가이드 : 영어 문장
					</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL :  https://api.maum.ai/api/konglish/sentence</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId</td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>sentence</td>
                            <td>콩글리시로 변환할 영어 문장</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
							<pre>
curl -X POST \
http://api.maum.ai/konglish/sentence \
-H 'Content-Type: application/json' \
-H 'cache-control: no-cache' \
-d '{
        "apiId": {발급받은 API ID},
        "apiKey": {발급받은 API KEY},
        "sentence": "항상 ladyfirst이다"
}'
</pre>
                    </div>

                    <p class="sub_txt">④ Response 파라미터 설명 </p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>타입</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>API 동작여부</td>
                            <td>list</td>
                        </tr>
                        <tr>
                            <td>result</td>
                            <td>콩글리시로 변환된 영어 문장 결과</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <span class="table_tit">message: API 동작여부</span>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>타입</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>요청 처리 상태를 설명하는 문자열 (Success/ Fail)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>요청 처리 상태에 대한 status code (0: Success)</td>
                            <td>number</td>
                        </tr>
                    </table>

                    <p class="sub_txt">⑤ Response 예제 </p>
                    <div class="code_box">
<pre>
{
        "message":{
                "message":"Success","status":0
        },
        "result": "항상 레이디퍼스트이다"
}
</pre>

                    </div>
                </div>
            </div>

        </div>
        <!--//conv_menu-->
        <!--.conv_example-->
        <div class="demobox" id="conv_example">
            <p><em style="color:#27c1c1;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
            <%--conversion--%>
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>TTS 고도화</span>
                            </dt>
                            <dd class="txt">한글과 영문이 혼용되어 있는 문장을 한국어 TTS 엔진을 이용하여 한번에 자연스럽게 읽을 수 있습니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_tts"><span>TTS</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>영어교육</span>
                            </dt>
                            <dd class="txt">영어 단어의 외래 표기법 및 발음하는 방법을 익혀 영어 교육에 쓰입니다. </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_engeduPron"><span>발음 평가</span></li>
                                    <li class="ico_engeduPhonics"><span>파닉스 평가 </span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>영문 오디오 북 청취</span>
                            </dt>
                            <dd class="txt">영어 단어 및 문장 표현의 발음이 어려운 경우, 한글 변환 후 TTS로 영문  오디오 북 청취가 가능합니다.  </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_tts"><span>TTS</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //conversion -->
        </div>
        <!--//.conv_example-->


    </div>
</div>
<!-- //.contents -->


<script type="text/javascript">
    $(window).load(function() {
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
    });
</script>

<script type="text/javascript">
        $(document).ready(function (){
            let ajaxXHR;
            let $textArea = $('#text-contents');

            //===== radioBox event =====
            $('input[name=conversion]').on('click', function(){
                $textArea.text("");
                $textArea.val("");

                let _id= $(this).attr('id');
                if(_id == "word"){
                    $('.change_txt').text('단어');
                    $('.sentence_lst').hide();
                    $('.word_lst').fadeIn();
                    $('#text-contents').attr('placeholder', '영어 단어를 입력해 주세요');
                    $('.ift_lst input[name=sentence]:checked').prop('checked', false);
                    $('.ift_lst #radio').prop('checked', true);
                    $('.text_info').hide();

                }else if(_id =="sentence"){
                    $('.change_txt').text('문장');
                    $('.word_lst').hide();
                    $('.sentence_lst').fadeIn();
                    $('#text-contents').attr('placeholder', '영어 문장을 입력해 주세요');
                    $('.ift_lst input[name=word]:checked').prop('checked', false);
                    $('.ift_lst #radio4').prop('checked', true);
                    $('.text_info').fadeIn();
                }


            });

            //===== textArea event =====
            $textArea.on('change paste input keyup', function(){
                $('.ift_lst input[type=radio]:checked').prop("checked", false);
                countTextAreaLength ()
            });

            //===== 한글 변환 =====
            $('#find_btn').on('click',function(){
                let $checkedTopRadio = $('input[type=radio][name=conversion]:checked').val();
                let $checkedInput = $('.ift_lst input[type=radio]:checked');
                let $checkedInputText = $checkedInput.val();
                let inputText = "";


                if( $textArea.val().trim() === ""){
                    if($checkedInput.length === 0){
                        alert("한글을 변환하고 싶은 예문을 선택하거나 텍스트를 입력해주세요.");
                        return;
                    }
                    console.log($checkedInputText);
                    inputText = $checkedInputText;
                }else{
                    inputText = $textArea.val();
                }
                $('.itf_box .step01').hide();
                $('.itf_box .loading_wrap').fadeIn();

                if($checkedTopRadio == "word"){
                    // 단어 선택일 때
                    ajaxXHR = $.ajax({
                        url: '/api/konglish/words',
                        async: true,
                        headers: {
                            "Content-Type": "application/json"
                        },
                        data: {
                            "word": inputText,
                            "${_csrf.parameterName}" : "${_csrf.token}"
                        },
                        error: function(error){
                            if (error.status === 0) {
                                return false;
                            }
                            console.log(error);
                        },
                        success: function(data){

                            console.log(unescape(data));
                            var responseText = JSON.parse(data);

                            $('#input_sentence').text(inputText);
                            $('#result_sentence').text(responseText.result);

                            // $('.itf_box .step01').hide();
                            $('.itf_box .step02').fadeIn();
                            $('.itf_box .loading_wrap').hide();
                        }
                    });
                }else if($checkedTopRadio == "sentence"){

                    //문장 선택일 때
                    ajaxXHR =$.ajax({
                        url: '/api/konglish/sentence',
                        async: true,
                        data: {
                            "sentence": inputText,
                            "${_csrf.parameterName}" : "${_csrf.token}"
                        },
                        error: function(error){
                            if (error.status === 0) {
                                return false;
                            }
                            console.log(error);

                        },
                        success: function(data){
                            var responseData = JSON.parse(data);

                            $('#input_sentence').text(inputText);
                            $('#result_sentence').text(responseData.result);

                            // $('.itf_box .step01').hide();
                            $('.itf_box .step02').fadeIn();
                            $('.itf_box .loading_wrap').hide();
                        }
                    });

                }

            });



            $('.btn_back1').on('click',function(){
                if(ajaxXHR){ajaxXHR.abort();}


                $('.itf_box .step02').hide();
                $('.itf_box .step01').fadeIn();
                $('.itf_box .loading_wrap').hide();
            });

            $('#btn_reset').on('click',function(){
                $('.itf_box .step02').hide();
                $('.itf_box .step01').fadeIn();
                $('.itf_box .loading_wrap').hide();
            });

            function countTextAreaLength (){
                let content = $('#text-contents').val();
                if (content.length >= 1000) {
                    $('#count').html(1000);
                    return;
                }
                $('#count').html(content.length);
            }

        });

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();



</script>
