<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-02-20
  Time: 09:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/aibuilder.css">

<!-- .contents -->
<div class="contents">
    <div class="content">
        <h1>AI Builder: 최적의 인공지능 툴 </h1>
        <div class="demobox data_demobox">
            <p><span>AI Builder: 최적의 인공지능 툴</span></p>
            <span class="sub">30+ maum.ai 만의 다양한 AI 엔진들을 지금 바로 조합하고 테스트해보세요. </span>
            <div class="data_box minutes_box aibuilder_box">

                <!-- .builder_box -->
                <div class="builder_box">
                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/img_aibuilder.png" alt="AI Builder 예시 이미지"/>
                </div>
                <!-- //.builder_box -->


                <ul>
                    <li>
                        <h3>1. 원하는 인공지능 선택하기! </h3>
                        <p>음성, 시각, 언어 등 다양한 분야의<br>인공지능을 선택하여, 해당 엔진의 <br>Input, Output, Parameter등을<br>확인해줍니다.
                        </p>
                    </li>
                    <li>
                        <h3>2. Application 조합하여 <br>MVP 실행!</h3>
                        <p>선택 가능한 엔진을 이어서 붙이거나, 그대로<br>MVP 실행을 눌러 테스트해봅니다.<br>입력 값을 넣었을 때, 어떤 값이 나오는지<br>확인해보세요.
                        </p>
                    </li>
                    <li>
                        <h3>3. 개발자 코드 이용하여 바로 <br>적용해보기!</h3>
                        <p>제공되는 개발자 코드를 이용하여 사용자 <br>입장에서 바로 적용해봅니다.<br>내가 만드는 AI Application을<br>지금 바로 사용하세요!
                        </p>
                    </li>

                </ul>
                <a href="https://builder.maum.ai/" class="minutes_btn" target="_blank" title="AI Builder 바로가기">AI Builder 바로가기 <em class="fas fa-angle-right"></em></a>
            </div>

        </div>

    </div>
    <!-- //.content -->
</div>
<!-- //.contents -->

<%-- 회의록 관련 스크립트 --%>
<script type="text/javascript">

</script>
