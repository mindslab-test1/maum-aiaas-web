<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
    <div class="content api_content">
        <h1>영어 교육용 STT</h1>

        <ul class="menu_lst eng_lst">
            <li class="tablinks" onclick="openTap(event, 'edustt_demo')" id="defaultOpen">
                <button type="button">엔진</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'edustt_example')">
                <button type="button">적용사례</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'edustt_menu')">
                <button type="button">매뉴얼</button>
            </li>
<%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <!-- [신규] 20190809 서비스개발팀 YMJ 영어교육용STT 콘텐츠 -->
        <div class="demobox en_demobox" id="edustt_demo">
            <p><span>영어 교육용 STT</span></p>
            <span class="sub">학습자의 영어 발화를 인식하여 정확하게 읽었는지 평가해 줍니다.</span>
            <!-- demoBox -->
            <div class="demoBox">
                <!-- commBox -->
                <div class="commBox">
                    <p class="commBox_txt">하단의 <img src="/aiaas/kr/images/img_mic_orange.png" alt="마이크"> 버튼을 눌러 마이크를 활성화
                        한 뒤 문장을 읽으세요.</p>
                    <!-- stepBox -->
                    <ul class="stepBox">
                        <li id="assStep">
                            <ul class="nav">
                                <li class="active">Step 1</li>
                                <li class="disabled">Step 2</li>
                                <li class="disabled">Step 3</li>
                            </ul>
                            <div class="questionBox">
                                <p class="sound">
                                    <!-- [D] 오디오파일 링크은 하단 javascript에 넣어주세요. -->
                                    <button type="button" id="oriAudio1" class="btn_oriPlay" title="파일 재생">파일 재생
                                    </button>
                                </p>
                                <p class="txt_en"><span>I went to school.</span></p>
                                <p class="txt_kr">나는 학교에 갔어요.</p>
                            </div>
                            <div class="answerBox">
                                <p class="txt">
                                    <span><!--스트리밍 결과값--><button type="button" id="recordAudio" class="btn_recordPlay"
                                                                 title="파일 재생">파일 재생</button></span>
                                </p>
                                <dl class="assResult">
                                    <dt>정확도</dt>
                                    <dd>00점</dd>
                                </dl>
                            </div>
                            <div class="btnBox">
                                <!-- 버튼_평가전 -->
                                <p class="assBefore"><!-- 평가 완료되면 display:none; 스타일 선언 -->
                                    <button type="button" class="btn_orange btn_ass"><span>평가하기</span></button>
                                </p>
                                <!-- //버튼_평가전 -->
                                <!-- 버튼_평가후 -->
                                <p class="assAfter"><!-- 평가 완료되면 display:block; 스타일 선언 -->
                                    <button type="button" class="btn_ass_again"><em class="fas fa-retweet"></em>다시하기
                                    </button>
                                    <button type="button" id="retry_btn" class="btn_orange btn_ass_next"><em
                                            class="fas fa-arrow-right"></em>다음문장
                                    </button>
                                </p>
                                <!-- //버튼_평가후 -->
                            </div>
                        </li>
                    </ul>
                    <!-- //stepBox -->
                </div>
                <!-- //commBox -->
            </div>
            <!-- //demoBox -->
        </div>
        <!-- //[신규] 20190809 서비스개발팀 YMJ 영어교육용STT 콘텐츠-->

        <!--.pron_menu-->
        <div class="demobox eng_menu" id="edustt_menu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        영어 교육용 STT
                    </div>
                    <p class="sub_txt">학습자의 영어 발화를 인식하여 정확하게 읽었는지 평가해 줍니다.</p>
                    <span class="sub_title">준비사항</span>
                    <p class="sub_txt">① Input: 음성파일 (.wav/.pcm) </p>
                    <ul>
                        <li>type:file (wav, pcm) 16K mono 음성파일</li>
                    </ul>
                    <p class="sub_txt">② Input: 정답 문장</p>
                    <span class="sub_title">실행 가이드</span>
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/edueng/stt</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId</td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey</td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>userId</td>
                            <td>사용자의 ID</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>model</td>
                            <td>사용할 모델 (baseline)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>answerText</td>
                            <td>정답 문장</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>file</td>
                            <td>type:file (wav, pcm) 16K mono 음성 파일</td>
                            <td>file</td>
                        </tr>

                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                        curl -X POST \<br>
                        https://api.maum.ai/edueng/stt \<br>
                        -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \<br>
                        -F apiId= (*ID 요청 필요) \<br>
                        -F apiKey= (*key 요청 필요) \<br>
                        -F userId= (사용자 ID) \<br>
                        -F model= (사용할 model) \<br>
                        -F answerText= (정답 문장) \<br>
                        -F 'file=@eng_16k_1.wav' \<br>
                    </div>

                    <p class="sub_txt">④ Response 파라미터 설명 </p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>타입</th>
                        </tr>
                        <tr>
                            <td>resultText</td>
                            <td>인식된 문장</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>resCode</td>
                            <td>API 응답 결과 (Success : 200)</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>answerText</td>
                            <td>정답 문장</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>recordUrl</td>
                            <td>인식한 파일 url</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>sentenceScore</td>
                            <td>정답 문장과 인식 문장을 비교한 점수</td>
                            <td>int</td>
                        </tr>
                    </table>
                    <p class="sub_txt">⑤ Response 예제 </p>
                    <div class="code_box">
                        <pre>
{
    "result": {
        "resultText": "what are you doing",87
        "resCode": 200,
        "answerText": "what are you doing?",
        "recordUrl": "http://maieng.maum.ai:7776/record/engedu.api/apiId/userId/202002/20200210_143106_60.mp3",
        "sentenceScore": 100
    }
}
                        </pre>
                    </div>
                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--//pron_menu-->
        <!--.pron_example-->
        <div class="demobox" id="edustt_example">
            <p><em style="font-weight: 400; color: #fb8923;">적용사례</em> <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
            <div class="useCasesBox">
                <p style="font-size:15px;">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>영어 따라 말하기</span>
                                <span style="font-size:16px">(Speaking 활동)</span>
                            </dt>
                            <dd class="txt">주어진 문장을 따라 말하고 STT 결과 비교를 통해 정확도를 파악합니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_engeduStt"><span>engedu STT</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>영어책 따라 읽기</span>
                                <span style="font-size:16px">(Read Aloud 활동)</span>
                            </dt>
                            <dd class="txt">영어 스토리를 듣고 따라 읽기로 활용할 수 있습니다. 이야기를 따라 읽으면 STT를 통해 읽었는지 피드백을 줄 수 있습니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_engeduStt"><span>engedu STT</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>영어로 대화하기</span>
                            </dt>
                            <dd class="txt">영어로 묻고 대답하는 활동에서 사용할 수 있습니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_engeduStt"><span>engedu STT</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 04</em>
                                <span>토익 스피킹, OPIc, <br>프레젠테이션 등 <br>스피치 연습</span>
                            </dt>
                            <dd class="txt">주어진 지문을 읽거나 말하기 연습으로 활용할 수 있습니다. 학습자의 발화가 텍스트로 변환되어 피드백을 받을 수 있습니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_engeduStt"><span>engedu STT</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
                </p>
            </div>

        </div>
        <%--pron_example--%>

    </div>
    <!-- //.content -->
</div>

<script type="text/javascript">

    // $('.category_1').trigger("click");

    $(window).load(function () {
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function () {
            $(this).remove();
        });

    });
</script>

<script src="https://${engedu_host}/front/resources/static/js/maiEng-v1${engedu_surfix}.min.js"></script>

<script type="text/javascript">

    var stepNum = 0; //현재 스텝이 어디인지 표시
    var toggleFlag = false; //min 상태 표시

    function onResult_Stt(result) {

        showResult(result.userText, result.recordUrl, result.score);

        $('.btnBox .assBefore').css('display', 'none');
        $('.btnBox .assAfter').css('display', 'block');

        $('.btn_ass').removeClass('playing');
        toggleFlag = false;
    }

    function onFail(reason) {

    }

    function showResult(answerText, recordUrl, score) {

        recordUrl = recordUrl.replace(".wav", ".mp3");
        audioTemp = new Audio(recordUrl);

        $('.answerBox .txt span').html(answerText + "&nbsp;&nbsp;"
            + "<button type='button' id='recordAudio_n' class='btn_recordPlay' title='파일 재생' >파일 재생</button>");

        $('#recordAudio_n').click(function () {
            audioTemp.play();
        });

        $('.answerBox .assResult dd').text(score + " 점");

        $('.answerBox span').css('display', 'table-cell');
        $('.btn_recordPlay').css('display', 'inline-block');

        $('.answerBox .assResult').css('display', 'block');
        $('.answerBox .assResult').show();
    }

    $(document).ready(function () {

        var sentence = ["I went to school.", "I went to school yesterday.", "I went to school yesterday with my brother."];
        var translation = ["나는 학교에 갔어요.", "나는 어제 학교를 갔어요.", "나는 어제 형이랑 학교를 갔어요."];

        if (!maiEng.isAvailableBrowser()) {
            alert("'getUserMedia()' is not supported on your browser.\n\nWe recommend using the latest version of Chrome, Firefox, Safari, or Microsoft Edge.");
        }

        function assStepColoring() {
            if (stepNum == 0) {
                $('#assStep .nav li:nth-child(1)').addClass("active");
                $('#assStep .nav li:nth-child(2)').addClass("disabled");
                $('#assStep .nav li:nth-child(3)').removeClass();
                $('#assStep .nav li:nth-child(3)').addClass("disabled");
            } else if (stepNum == 1) {
                $('#assStep .nav li:nth-child(1)').removeClass();
                $('#assStep .nav li:nth-child(2)').addClass("active");
                $('#assStep .nav li:nth-child(3)').addClass("disabled");
            } else {
                $('#assStep .nav li:nth-child(1)').removeClass();
                $('#assStep .nav li:nth-child(2)').removeClass();
                $('#assStep .nav li:nth-child(3)').addClass("active");
            }
        }

        //평가 시작
        $('.btn_ass').on('click', function () {

            if (toggleFlag) { // ON -> OFF 바뀌어야 함

                maiEng.cancel();

                toggleFlag = false;
                $(this).removeClass('playing');

            } else { // OFF -> ON 바뀌어야 함 (마이크를 사용하겠다고 방금 선언한 상태!!)

                if (!maiEng.isAvailableBrowser()) {
                    alert("'getUserMedia()' is not supported on your browser.\n\nWe recommend using the latest version of Chrome, Firefox, Safari, or Microsoft Edge.");
                    return;
                }

                $('#recordAudio').css('display', 'none');

                $(this).addClass('playing');
                $(this).parent().parent().parent().addClass('textOn');

                toggleFlag = true;

                maiEng.startStt('userId', null, sentence[stepNum], onResult_Stt, onFail);
            }
        });

        //평가 다시하기
        $('.btn_ass_again').on('click', function () {

            assStepColoring();

            $('.questionBox .txt_en span').html(sentence[stepNum]);
            $('.questionBox .txt_kr').text(translation[stepNum]);

            $('.answerBox .txt span').hide();
            $('.answerBox .assResult').hide();

            // assBefore block '평가하기' 버튼
            $('.btnBox .assBefore').css('display', 'block');
            $('.btnBox .assBefore').show();

            // assAfter none '다시하기, 다음문장' 버튼
            $('.btnBox .assAfter').css('display', 'none');
            $('.btnBox .assAfter').hide();

            // 결과창 초기화
            $('.answerBox .txt span').text("");
            // 점수 초기화
            $('.answerBox .assResult dd').text("");
        });

        // 다음 단계 넘어가기
        $('.btn_ass_next').on('click', function () {

            stepNum++;

            if (stepNum == 3) {
                stepNum = stepNum - 3;
            }

            assStepColoring();

            $('.questionBox .txt_en span').html(sentence[stepNum]);
            $('.questionBox .txt_kr').text(translation[stepNum]);

            $('.answerBox .txt span').hide();
            $('.answerBox .assResult').hide();

            // assBefore block '평가하기' 버튼
            $('.btnBox .assBefore').css('display', 'block');
            $('.btnBox .assBefore').show();

            // assAfter none '다시하기, 다음문장' 버튼
            $('.btnBox .assAfter').css('display', 'none');
            $('.btnBox .assAfter').hide();

            if (stepNum == 2) {
                $('#retry_btn').html('<em class="fas fa-redo"></em>처음으로');
            } else {
                $('#retry_btn').html('<em class="fas fa-arrow-right"></em>다음문장');
            }

            // 결과값 초기화
            $('.answerBox .txt span').text("");

        });

        //sound play
        var audioFileUrl = "/aiaas/kr/audio/engedu/";
        var oriAudio = ["engedu_stt_ori1.mp3", "engedu_stt_ori2.mp3", "engedu_stt_ori3.mp3"];

        //원본파일 재생
        $('#oriAudio1').click(function () {
            new Audio(audioFileUrl + oriAudio[stepNum]).play();
        });
    });

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>
<script src="https://${engedu_host}/front/resources/static/js/init-v1.js"></script>