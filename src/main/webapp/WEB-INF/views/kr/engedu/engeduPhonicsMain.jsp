<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">

    <div class="content api_content">
        <h1>파닉스 평가</h1>
        <ul class="menu_lst eng_lst">
            <li class="tablinks" onclick="openTap(event, 'phonics_demo')" id="defaultOpen"><button type="button">엔진</button></li>
            <li class="tablinks" onclick="openTap(event, 'phonics_example')"><button type="button">적용사례</button></li>
            <li class="tablinks" onclick="openTap(event, 'phonics_menu')"><button type="button">매뉴얼</button></li>
<%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <!-- [신규] 20190810 서비스개발 YMJ 파닉스 평가 -->
        <div class="demobox en_demobox" id="phonics_demo">
            <p><span>파닉스 평가</span></p>
            <span class="sub">영어 단어의 특정 음소를 지정하여 음소 단위의 발음 평가를 제공합니다.</span>
            <!-- demoBox -->
            <div class="demoBox">
                <!-- commBox -->
                <div class="commBox">
                    <p class="commBox_txt">하단의 <img src="/aiaas/kr/images/img_mic_orange.png" alt="마이크"> 버튼을 눌러 마이크를 활성화
                        한 뒤 문장을 읽으세요.</p>
                    <!-- stepBox -->
                    <ul class="stepBox">
                        <li id="assStep01">
                            <ul class="nav">
                                <li class="active">Step 1</li>
                                <li class="disabled">Step 2</li>
                                <li class="disabled">Step 3</li>
                            </ul>
                            <div class="questionBox">
                                <p class="photoTxt">
                                    <span class="img"><img src="/aiaas/kr/images/img_phonics_map.png"
                                                           alt="map"></span>
                                    <span class="txt"><em>M</em>ap <button type="button" id="oriAudio1"
                                                                           class="btn_oriPlay"
                                                                           title="파일 재생">파일 재생</button></span>
                                </p>
                                <p class="txt_kr"><strong>첫소리 m</strong> 발음에 유의하여 읽어보세요.</p>
                            </div>
                            <div class="answerBox03">
                                <div class="assResult">
                                    <div class="txtBox">
                                        <!-- [D] 평가 점수에 따라 하단 <p>안에 문구를 바꿔 주세요. -->
                                        <p id="phoResult0" class="txt">Excellent!</p>
                                        <!-- [D] Try again! 경우 피드백 메세지 도출
                                        <p class="txt">Try again!
                                            <span class="feedback">	윗니를 아랫입술에 살짝 대고 마찰시켜 발음해 보세요.</span>
                                        </p>
                                        -->
                                        <!-- [D] 평가 총 점수 참고
                                        90-100: Excellent!
                                        80-89:  Good!
                                        70-79:  Good try!
                                        60-69:  Not bad!
                                        ~59:    Try again!
                                        -->

                                    </div>
                                </div>
                            </div>
                            <div class="btnBox">
                                <!-- 버튼_평가전 -->
                                <p class="assBefore"><!-- 평가 완료되면 display:none; 스타일 선언 -->
                                    <button type="button" class="btn_orange btn_ass"><span>평가하기</span></button>
                                </p>
                                <!-- //버튼_평가전 -->
                                <!-- 버튼_평가후 -->
                                <p class="assAfter"><!-- 평가 완료되면 display:block; 스타일 선언 -->
                                    <button type="button" class="btn_ass_again"><em class="fas fa-retweet"></em>다시하기
                                    </button>
                                    <button type="button" class="btn_orange btn_ass_next"><em
                                            class="fas fa-arrow-right"></em>다음문장
                                    </button>
                                </p>
                                <!-- //버튼_평가후 -->
                            </div>
                        </li>
                        <li id="assStep02">
                            <ul class="nav">
                                <li>Step 1</li>
                                <li class="active">Step 2</li>
                                <li class="disabled">Step 3</li>
                            </ul>
                            <div class="questionBox">
                                <p class="photoTxt">
                                    <span class="img"><img src="/aiaas/kr/images/img_phonics_nine.png"
                                                           alt="nine"></span>
                                    <span class="txt">N<em>i</em>n<em>e</em> <button type="button" id="oriAudio2"
                                                                                     class="btn_oriPlay" title="파일 재생">파일 재생</button></span>
                                </p>
                                <p class="txt_kr"><strong>장모음 i_e</strong> 발음에 유의하여 읽어보세요.</p>
                            </div>
                            <div class="answerBox03">
                                <div class="assResult">
                                    <div class="txtBox">
                                        <!-- [D] 평가 점수에 따라 하단 <p>안에 문구를 바꿔 주세요. -->
                                        <p id="phoResult1" class="txt">Excellent!</p>
                                        <!-- [D] Try again! 경우 피드백 메세지 도출
                                        <p class="txt">Try again!
                                            <span class="feedback">장모음 i_e 는 우리말의 ‘아이’로 발음 됩니다. ‘브-아이-크’와 같이 발음해보세요.</span>
                                        </p>
                                        -->
                                        <!-- [D] 평가 총 점수 참고
                                        90-100: Excellent!
                                        80-89:  Good!
                                        70-79:  Good try!
                                        60-69:  Not bad!
                                        ~59:    Try again!
                                        -->

                                    </div>
                                </div>
                            </div>
                            <div class="btnBox">
                                <!-- 버튼_평가전 -->
                                <p class="assBefore">
                                    <button type="button" class="btn_orange btn_ass"><span>평가하기</span></button>
                                </p>
                                <!-- //버튼_평가전 -->
                                <!-- 버튼_평가후 -->
                                <p class="assAfter">
                                    <button type="button" class="btn_ass_again"><em class="fas fa-retweet"></em>다시하기
                                    </button>
                                    <button type="button" class="btn_orange btn_ass_next"><em
                                            class="fas fa-arrow-right"></em>다음문장
                                    </button>
                                </p>
                                <!-- //버튼_평가후 -->
                            </div>
                        </li>
                        <li id="assStep03">
                            <ul class="nav">
                                <li>Step 1</li>
                                <li>Step 2</li>
                                <li class="active">Step 3</li>
                            </ul>
                            <div class="questionBox">
                                <p class="photoTxt">
                                    <span class="img"><img src="/aiaas/kr/images/img_phonics_pool.png"
                                                           alt="pool"></span>
                                    <span class="txt">Poo<em>l</em> <button type="button" id="oriAudio3"
                                                                            class="btn_oriPlay"
                                                                            title="파일 재생">파일 재생</button></span>
                                </p>
                                <p class="txt_kr"><strong>끝소리 l</strong> 발음에 유의하여 읽어보세요.</p>
                            </div>
                            <div class="answerBox03">
                                <div class="assResult">
                                    <div class="txtBox">
                                        <!-- [D] 평가 점수에 따라 하단 <p>안에 문구를 바꿔 주세요. -->
                                        <p id="phoResult2" class="txt">Excellent!</p>
                                        <!-- [D] Try again! 경우 피드백 메세지 도출
                                        <p class="txt">Try again!
                                            <span class="feedback">모음 뒤에 우리말의 '을' 소리를 붙여 발음해 보세요.</span>
                                        </p>
                                        -->
                                        <!-- [D] 평가 총 점수 참고
                                        90-100: Excellent!
                                        80-89:  Good!
                                        70-79:  Good try!
                                        60-69:  Not bad!
                                        ~59:    Try again!
                                        -->

                                    </div>
                                </div>
                            </div>
                            <div class="btnBox">
                                <!-- 버튼_평가전 -->
                                <p class="assBefore">
                                    <button type="button" class="btn_orange btn_ass"><span>평가하기</span></button>
                                </p>
                                <!-- //버튼_평가전 -->
                                <!-- 버튼_평가후 -->
                                <p class="assAfter">
                                    <button type="button" class="btn_ass_again"><em class="fas fa-retweet"></em>다시하기
                                    </button>
                                    <button type="button" class="btn_orange btn_ass_reset"><em class="fas fa-redo"></em>처음으로
                                    </button>
                                </p>
                                <!-- //버튼_평가후 -->
                            </div>
                        </li>
                    </ul>

                    <!-- //stepBox -->
                </div>
                <!-- //commBox -->
            </div>
            <!-- //demoBox -->
        </div>
        <!-- //[신규] 20190809 서비스개발 YMJ 영어 문장 발음 평가 콘텐츠-->
        <!--.phonics_menu-->
        <div class="demobox eng_menu" id="phonics_menu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        파닉스 평가
                    </div>
                    <p class="sub_txt">영어 단어의 특정 음소를 지정하여 음소 단위의 발음 평가를 제공합니다.</p>
                    <span class="sub_title">준비사항</span>
                    <p class="sub_txt">① Input:  음성파일 (.wav/.pcm) </p>
                    <ul>
                        <li>type:file (wav, pcm) 16K mono 음성파일</li>
                    </ul>
                    <p class="sub_txt">② Input: 확인할 음소</p>
                    <span class="sub_title">실행 가이드</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/edueng/phonics</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId</td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey</td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>userId</td>
                            <td>사용자의 ID </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>model</td>
                            <td>사용할 모델 (baseline)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>answerText</td>
                            <td>정답 문장</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>chySym</td>
                            <td>확인할 음소</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>file</td>
                            <td>type:file (wav, pcm) 16K mono 음성 파일 </td>
                            <td>file</td>
                        </tr>

                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                        curl -X POST \<br>
                        https://api.maum.ai/edueng/phonics \<br>
                        -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \<br>
                        -F apiId= (*ID 요청 필요) \<br>
                        -F apiKey= (*key 요청 필요) \<br>
                        -F userId= (사용자 ID) \<br>
                        -F model= (사용할 model) \<br>
                        -F answerText= (정답 text) \<br>
                        -F chkSym= (확인할 음소) \<br>
                        -F 'file=@eng_16k_1.wav' \<br>
                    </div>

                    <p class="sub_txt">④ Response 파라미터 설명 </p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>타입</th>
                        </tr>
                        <tr>
                            <td>resultText</td>
                            <td>인식 문장</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>resCode</td>
                            <td>API 응답 결과 (Success : 200)</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>answerText</td>
                            <td>정답 문장</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>recordUrl</td>
                            <td>인식한 파일 url</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>userPron</td>
                            <td>인식된 음소</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>correctPron</td>
                            <td>정답 음소</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>sentenceScore</td>
                            <td>정답 문장과 인식 문장 비교 점수</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>음소의 정답 여부</td>
                            <td>int</td>
                        </tr>
                    </table>
                    <p class="sub_txt">⑤ Response 예제 </p>
                    <div class="code_box">
                        <pre>
{
    "result": {
        "resultText": "They are saying.",
        "userPron": "P AY V",
        "answerText": "Five",
        "recordUrl": "http://maieng.maum.ai:7776/record/engedu.api/apiId/userId/202002/20200210_165127_2.mp3",
        "resCode": 200,
        "correctPron": "F AY V",
        "sentenceScore": 0,
        "status": 1
    }
}
                        </pre>
                    </div>
                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--//phonics_menu-->
        <!--.phonics_example-->
        <div class="demobox" id="phonics_example">
            <p><em style="font-weight: 400; color: #fb8923;">적용사례</em>  <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
            <div class="useCasesBox">
                <p style="font-size:15px;">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>파닉스 연습</span>
                            </dt>
                            <dd class="txt">vase, base 등과 같이 쌍으로 작용하는 minimal-pairs 연습할 수 있습니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_engeduStt"><span>engedu STT</span></li>
                                    <li class="ico_engeduPhonics"><span>engedu Phonics</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>단어 발음 연습</span>
                            </dt>
                            <dd class="txt">한국인이 틀리기 쉬운 단어 중심의 발음 평가로 활용할 수 있습니다. </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_engeduStt"><span>engedu STT</span></li>
                                    <li class="ico_engeduPron"><span>engedu Pron</span></li>
                                    <li class="ico_engeduPhonics"><span>engedu Phonics</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
                </p>
            </div>

        </div>
        <%--phonics_example--%>
    </div>
    <!-- //.content -->
</div>
<!-- //.contents -->

<script type="text/javascript">

    // $('.category_1').trigger("click");

    $(window).load(function () {
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function () {
            $(this).remove();
        });

    });
</script>

<script src="https://${engedu_host}/front/resources/static/js/maiEng-v1${engedu_surfix}.min.js"></script>

<!-- [신규] 20190809 서비스개발 YMJ -->
<script type="text/javascript">

    var stepNum = 0;
    var toggleFlag = false;
    var phoWord = ["MAP", "NINE", "POOL"];
    var checkSymbol = ["M", "N", "L"];

    function onNoti_Stt(result) {
        // 파닉스 서버를 거치는 동안 STT의 결과값 사용 가능
    }

    function onResult_Phonics(result) {
        showResult(result.status, result.userPron, result.correctPron);

        $(".btnBox .assBefore").css('display', 'none');
        $(".btnBox .assAfter").css('display', 'block');

        $('.btn_ass').removeClass('playing');
        toggleFlag = false;
    }

    function onFail(reason) {

    }

    function showResult(status, userPron, correctPron) {

        var str = "#phoResult" + stepNum;

        if (status == 1) {

            console.log("status 1");

            $(str).text("Correct!");
        } else {
            $(str).text("Incorrect!");
        }

        $('.assResult').css('display', 'block');
    }

    $(document).ready(function () {

        //평가 시작
        $('.btn_ass').on('click', function () {

            if (toggleFlag) { // ON -> OFF 바뀌어야 함

                maiEng.cancel();

                toggleFlag = false;
                $(this).removeClass('playing');

            } else { // OFF -> ON 바뀌어야 함 (마이크를 사용하겠다고 방금 선언한 상태!!)

                if (!maiEng.isAvailableBrowser()) {
                    alert("'getUserMedia()' is not supported on your browser.\n\nWe recommend using the latest version of Chrome, Firefox, Safari, or Microsoft Edge.");
                    return;
                }

                $('#recordAudio').css('display', 'none');

                $(this).addClass('playing');
                $(this).parent().parent().parent().addClass('textOn');

                toggleFlag = true;

                maiEng.startPhonics('userid', null, phoWord[stepNum], checkSymbol[stepNum], onNoti_Stt, onResult_Phonics, onFail);

            }
        });

        //평가 다시하기
        $('.btn_ass_again').on('click', function () {
            $('.commBox .stepBox .answerBox03 .assResult').css({
                display: 'none',
            });

            // assBefore block '평가하기' 버튼
            $('.btnBox .assBefore').css('display', 'block');
            $('.btnBox .assBefore').show();

            // assAfter none '다시하기, 다음문장' 버튼
            $('.btnBox .assAfter').css('display', 'none');
            $('.btnBox .assAfter').hide();

            // 결과값 초기화
        });

        //1단계 -> 2단계
        $('#assStep01 .btn_ass_next').on('click', function () {
            $('ul.stepBox li').hide();
            $('#assStep02').show();
            $('#assStep02 .nav li').show();

            stepNum = 1;
            $('#phoResult1').text("");

            $('#assStep01 .btnBox .assAfter').hide();
            $('#assStep02 .btnBox .assBefore').show();
            $('#assStep02 .btnBox .assAfter').hide();
        });

        //2단계 -> 3단계
        $('#assStep02 .btn_ass_next').on('click', function () {
            $('ul.stepBox li').hide();
            $('#assStep03').show();
            $('#assStep03 .nav li').show();

            stepNum = 2;
            $('#phoResult2').text("");

            $('#assStep02 .btnBox .assAfter').hide();
            $('#assStep03 .btnBox .assBefore').show();
            $('#assStep03 .btnBox .assAfter').hide();
        });

        //3단계 -> 1단계
        $('#assStep03 .btn_ass_reset').on('click', function () {
            $('ul.stepBox li').hide();
            $('#assStep01').show();
            $('#assStep01 .nav li').show();

            stepNum = 0;
            $('#phoResult0').text("");

            $('#assStep03 .btnBox .assAfter').hide();
            $('#assStep01 .btnBox .assBefore').show();
            $('#assStep01 .btnBox .assAfter').hide();
        });

        //sound play
        var audioFileUrl = "/aiaas/kr/audio/engedu/";
        var oriAudio = ["engedu_phonics_1.mp3", "engedu_phonics_2.mp3", "engedu_phonics_3.mp3"];

        //원본파일 재생
        $('button.btn_oriPlay').click(function () {
            new Audio(audioFileUrl + oriAudio[stepNum]).play();
        });

    });


    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>
<script src="https://${engedu_host}/front/resources/static/js/init-v1.js"></script>