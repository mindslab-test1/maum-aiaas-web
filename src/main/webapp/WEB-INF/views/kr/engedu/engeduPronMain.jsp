<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>

<!-- .contents -->
<div class="contents">
    <div class="content api_content">
        <h1>영어 문장 발음 평가</h1>
        <ul class="menu_lst eng_lst">
            <li class="tablinks" onclick="openTap(event, 'pron_demo')" id="defaultOpen">
                <button type="button">엔진</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'pron_example')">
                <button type="button">적용사례</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'pron_menu')">
                <button type="button">매뉴얼</button>
            </li>
<%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <!-- [신규] 20190809 서비스개발 YMJ 영어 문장 발음 평가 콘텐츠 -->
        <div class="demobox en_demobox" id="pron_demo">
            <p><span>영어 문장 발음 평가</span></p>
            <span class="sub">학습자 영어 발화의 유창성을 원어민과 비교하여 평가해 줍니다.</span>
            <!-- demoBox -->
            <div class="demoBox">
                <!-- commBox -->
                <div class="commBox">
                    <p class="commBox_txt">하단의 <img src="/aiaas/kr/images/img_mic_orange.png" alt="마이크"> 버튼을 눌러 마이크를 활성화
                        한 뒤 문장을 읽으세요.</p>
                    <!-- stepBox -->
                    <ul class="stepBox">
                        <li id="assStep">
                            <ul class="nav">
                                <li class="active">Step 1</li>
                                <li class="disabled">Step 2</li>
                                <li class="disabled">Step 3</li>
                            </ul>
                            <div class="questionBox">
                                <p class="sound">
                                    <!-- [D] 오디오파일 링크은 하단 javascript에 넣어주세요. -->
                                    <button type="button" id="oriAudio2" class="btn_oriPlay" title="파일 재생">파일 재생
                                    </button>
                                </p>
                                <p class="txt_en"><span>I want to buy a ticket.</span></p>
                                <p class="txt_kr">나는 티켓을 사고 싶어요.</p>
                            </div>
                            <div class="answerBox2 ">
                                <div class="assResult">
                                    <div id="chart" class="gaugeBox">
                                        <dl>
                                            <dt>발음</dt>
                                            <dd id="pronResult1">
                                                <!-- [D] 평가 점수만큼 <span>에 width값을 넣어주세요. -->
                                                <span id="pron1gauge" class="gauge" style="width:80%">80%</span>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>인토네이션</dt>
                                            <dd id="pronResult2">
                                                <span id="pron2gauge" class="gauge" style="width:20%">20%</span>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>속도</dt>
                                            <dd id="pronResult3">
                                                <span id="pron3gauge" class="gauge" style="width:100%">100%</span>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>리듬</dt>
                                            <dd id="pronResult4">
                                                <span id="pron4gauge" class="gauge" style="width:50%">50%</span>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>분절</dt>
                                            <dd id="pronResult5">
                                                <span id="pron5gauge" class="gauge" style="width:50%">50%</span>
                                            </dd>
                                        </dl>
                                    </div>
                                    <div id="star_box" class="starBox">
                                        <p id="img_box" class="imgBox">
                                            <!-- [D] 평가 점수에 따라 <em> 추가 -->
                                            <em id="star1" class="fas fa-star"></em>
                                            <em id="star2" class="fas fa-star"></em>
                                            <em id="star3" class="fas fa-star"></em>
                                            <em id="star4" class="fas fa-star"></em>
                                            <em id="star5" class="fas fa-star"></em>
                                        </p>
                                        <!-- [D] 평가 점수에 따라 문구 변경 -->
                                        <p id="result_txt" class="txt">Excellent</p>
                                        <!-- [D] 평가 총 점수 참고
                                        90-100: 별:5개 Excellent!
                                        80-89:  별:4개 Good!
                                        70-79:  별:3개 Good try!
                                        60-69:  별:2개 Not bad!
                                        ~59:    별:1개 Try again!
                                        -->
                                    </div>
                                </div>
                            </div>
                            <div class="btnBox">
                                <!-- 버튼_평가전 -->
                                <p class="assBefore"><!-- 평가 완료되면 display:none; 스타일 선언 -->
                                    <button type="button" class="btn_orange btn_ass"><span>평가하기</span></button>
                                </p>
                                <!-- //버튼_평가전 -->
                                <!-- 버튼_평가후 -->
                                <p class="assAfter"><!-- 평가 완료되면 display:block; 스타일 선언 -->
                                    <button type="button" class="btn_ass_again"><em class="fas fa-retweet"></em>다시하기
                                    </button>
                                    <button type="button" id="retry_btn" class="btn_orange btn_ass_next"><em
                                            class="fas fa-arrow-right"></em>다음문장
                                    </button>
                                </p>
                                <!-- //버튼_평가후 -->
                            </div>
                        </li>
                    </ul>

                    <!-- //stepBox -->
                </div>
                <!-- //commBox -->
            </div>
            <!-- //demoBox -->
        </div>
        <!-- //[신규] 20190809 서비스개발 YMJ 영어 문장 발음 평가 콘텐츠-->
        <!--.pron_menu-->
        <div class="demobox eng_menu" id="pron_menu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        영어 문장 발음 평가
                    </div>
                    <p class="sub_txt">학습자 영어 발화의 유창성을 원어민과 비교하여 평가해 줍니다.</p>
                    <span class="sub_title">준비사항</span>
                    <p class="sub_txt">① Input: 음성파일 (.wav/.pcm) </p>
                    <ul>
                        <li>type:file (wav, pcm) 16K mono 음성파일</li>
                    </ul>
                    <p class="sub_txt">② Input: 정답 문장</p>
                    <span class="sub_title">실행 가이드</span>
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/edueng/pron</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId</td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey</td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>userId</td>
                            <td>사용자의 ID</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>model</td>
                            <td>사용할 모델 (baseline)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>answerText</td>
                            <td>정답 문장</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>file</td>
                            <td>type:file (wav, pcm) 16K mono 음성 파일</td>
                            <td>file</td>
                        </tr>

                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                        curl -X POST \<br>
                        https://api.maum.ai/edueng/pron \<br>
                        -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \<br>
                        -F apiId= (*ID 요청 필요) \<br>
                        -F apiKey= (*key 요청 필요) \<br>
                        -F userId= (사용자 ID) \<br>
                        -F model= (사용할 model) \<br>
                        -F answerText= (정답 문장) \<br>
                        -F 'file=@eng_16k_1.wav' \<br>
                    </div>

                    <p class="sub_txt">④ Response 파라미터 설명 </p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>타입</th>
                        </tr>
                        <tr>
                            <td>resultText</td>
                            <td>인식된 문장</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>resCode</td>
                            <td>API 응답 결과 (Success : 200)</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>answerText</td>
                            <td>정답 문장</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>recordUrl</td>
                            <td>인식한 파일 url</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>words</td>
                            <td>단어별 발음 점수</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>rhythmScore</td>
                            <td>Rhythm 점수</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>segmentalScore</td>
                            <td>Segmental 점수</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>totalScore</td>
                            <td>Total 점수</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>sentenceScore</td>
                            <td>Sentence 점수</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>pronScore</td>
                            <td>Pron 점수</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>Speed</td>
                            <td>Speed 점수</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>intonationScore</td>
                            <td>Intonation 점수</td>
                            <td>int</td>
                        </tr>
                    </table>
                    <p class="sub_txt">⑤ Response 예제 </p>
                    <div class="code_box">
                        <pre>
{
    "result": {
        "resultText": "Cold.",
        "answerText": "cool",
        "recordUrl": "http://maieng.maum.ai:7776/record/engedu.api/apiId/userId/202002/20200210_165127_2.mp3",
        "words": [
            {
                "word": "cold",
                "pronScore": 31
            }
        ],
        "rhythmScore": 56,
        "segmentalScore": 76,
        "totalScore": 68,
        "sentenceScore": 0,
        "pronScore": 23,
        "speedScore": 92,
        "resCode": 200,
        "intonationScore": 54
    }
}
                        </pre>
                    </div>
                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--//pron_menu-->
        <!--.pron_example-->
        <div class="demobox" id="pron_example">
            <p><em style="font-weight: 400; color: #fb8923;">적용사례</em> <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
            <div class="useCasesBox">
                <p style="font-size:15px;">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>영어 문장 평가</span>
                            </dt>
                            <dd class="txt">영어로 문장을 따라 말한 후 인토네이션, 강세, 리듬, 발음을 원어민과 가까운 정도에 따라 점수로 확인할 수 있습니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_engeduStt"><span>engedu STT</span></li>
                                    <li class="ico_engeduPron"><span>engedu Pron</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>영어 스피치 평가</span>
                            </dt>
                            <dd class="txt">영어 스피치를 하고 문장 단위의 평가를 적용할 수 있습니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_engeduStt"><span>engedu STT</span></li>
                                    <li class="ico_engeduPron"><span>engedu Pron</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
                </p>
            </div>

        </div>
        <%--pron_example--%>

    </div>
    <!-- //.content -->
</div>


<script type="text/javascript">

    // $('.category_1').trigger("click");

    $(window).load(function () {
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function () {
            $(this).remove();
        });

    });
</script>

<script src="https://${engedu_host}/front/resources/static/js/maiEng-v1${engedu_surfix}.min.js"></script>

<!-- [신규] 20190809 서비스개발 YMJ -->
<script type="text/javascript">

    var stepNum = 0; //현재 스텝 표시
    var toggleFlag = false; //min 상태 표시

    function onNoti_Stt(result) {
        // 발음 평가 서버를 거치는 동안 STT의 결과값 사용 가능
    }

    function onResult_Pron(result) {
        showResult(result.totalScore, result.pronScore, result.speedScore, result.rhythmScore, result.intonationScore, result.segmentalScore);
        console.log("" + result.totalScore);

        $(".btnBox .assBefore").css('display', 'none');
        $(".btnBox .assAfter").css('display', 'block');

        $('.btn_ass').removeClass('playing');
        toggleFlag = false;
    }

    function onFail(reason) {

    }

    function showResult(totalScore, pronScore, speedScore, rhythmScore, intoScore, segmentalScore) {

        // 점수 설정
        $('#pron1gauge').text(pronScore + '%');
        $('#pron1gauge').css('width', pronScore + '%');

        $('#pron2gauge').text(intoScore + '%');
        $('#pron2gauge').css('width', intoScore + '%');


        $('#pron3gauge').text(speedScore + '%');
        $('#pron3gauge').css('width', speedScore + '%');


        $('#pron4gauge').text(rhythmScore + '%');
        $('#pron4gauge').css('width', rhythmScore + '%');

        $('#pron5gauge').text(segmentalScore + '%');
        $('#pron5gauge').css('width', segmentalScore + '%');

        // starBox 설정
        $('#img_box em').css('display', 'inline-block');

        if (totalScore >= 90) {
            $('#result_txt').text("Excellent!");
        } else if (totalScore >= 80) {
            $('#star5').css('display', 'none');

            $('#result_txt').text("Good!");
        } else if (totalScore >= 70) {
            $('#star4').css('display', 'none');
            $('#star5').css('display', 'none');

            $('#result_txt').text("Good try!");
        } else if (totalScore >= 60) {
            $('#star3').css('display', 'none');
            $('#star4').css('display', 'none');
            $('#star5').css('display', 'none');

            $('#result_txt').text("Not bad!");
        } else {
            $('#star2').css('display', 'none');
            $('#star3').css('display', 'none');
            $('#star4').css('display', 'none');
            $('#star5').css('display', 'none');

            $('#result_txt').text("Try again!");
        }

        // 화면 출력
        $('.assResult').css('display', 'table');
        $('#chart').css('display', 'table-cell');
        $('#star_box').css('display', 'table-cell');
        $('#img_box').css('display', 'block');
    }

    function assStepColoring() {
        if (stepNum == 0) {
            $('#assStep .nav li:nth-child(1)').addClass("active");
            $('#assStep .nav li:nth-child(2)').addClass("disabled");
            $('#assStep .nav li:nth-child(3)').removeClass();
            $('#assStep .nav li:nth-child(3)').addClass("disabled");
        } else if (stepNum == 1) {
            $('#assStep .nav li:nth-child(1)').removeClass();
            $('#assStep .nav li:nth-child(2)').addClass("active");
            $('#assStep .nav li:nth-child(3)').addClass("disabled");
        } else {
            $('#assStep .nav li:nth-child(1)').removeClass();
            $('#assStep .nav li:nth-child(2)').removeClass();
            $('#assStep .nav li:nth-child(3)').addClass("active");
        }
    }

    $(document).ready(function () {

        var sentence = ["I want to buy a ticket.", "I would like to watch the movie.", "I need to take a rest."];
        var translation = ["나는 티켓을 사고 싶어요.", "나는 영화를 보고 싶어요.", "나는 휴식이 필요해요."];

        $('#chart').css('display', 'none');
        $('#img_box').css('display', 'none');
        $('#result_txt').css('display', 'block');
        $('#result_txt').text("");

        if (!maiEng.isAvailableBrowser()) {
            alert("'getUserMedia()' is not supported on your browser.\n\nWe recommend using the latest version of Chrome, Firefox, Safari, or Microsoft Edge.");
        }

        //평가 시작
        $('.btn_ass').on('click', function () {

            $('#result_txt').text("");
            $('#result_txt').css('display', 'block');

            if (toggleFlag) { // ON -> OFF 바뀌어야 함

                maiEng.cancel();

                toggleFlag = false;
                $(this).removeClass('playing');

            } else { // OFF -> ON 바뀌어야 함 (마이크를 사용하겠다고 방금 선언한 상태!!)

                if (!maiEng.isAvailableBrowser()) {
                    alert("'getUserMedia()' is not supported on your browser.\n\nWe recommend using the latest version of Chrome, Firefox, Safari, or Microsoft Edge.");
                    return;
                }

                $(this).addClass('playing');
                $(this).parent().parent().parent().addClass('textOn');

                toggleFlag = true;

                maiEng.startPron('userId', null, sentence[stepNum], onNoti_Stt, onResult_Pron, onFail);
            }
            //결과값을 받았다면
        });

        //평가 다시하기
        $('.btn_ass_again').on('click', function () {
            $('#chart').css('display', 'none');
            $('#img_box').css('display', 'none');
            $('#result_txt').text("");
            $('#result_txt').css('display', 'block');

            // assBefore block '평가하기' 버튼
            $('.btnBox .assBefore').css('display', 'block');
            $('.btnBox .assBefore').show();

            // assAfter none '다시하기, 다음문장' 버튼
            $('.btnBox .assAfter').css('display', 'none');
            $('.btnBox .assAfter').hide();

            /*
                        // 보이는 점수, 그래프 값 초기화
                        $('#pron1gauge').text('0%');
                        $('#pron1gauge').css('width', '0');
                        $('#pron2gauge').text('0%');
                        $('#pron2gauge').css('width', '0');
                        $('#pron3gauge').text('0%');
                        $('#pron3gauge').css('width', '0');
                        $('#pron4gauge').text('0%');
                        $('#pron4gauge').css('width', '0');

                        $('#star1').css('display', 'none');
                        $('#star2').css('display', 'none');
                        $('#star3').css('display', 'none');
                        $('#star4').css('display', 'none');
                        $('#star5').css('display', 'none');
            */
        });

        //다음 단계 넘어가기
        $('.btn_ass_next').on('click', function () {

            $('#chart').css('display', 'none');
            $('#img_box').css('display', 'none');
            $('#result_txt').text("");
            $('#result_txt').css('display', 'block');

            stepNum++;

            if (stepNum == 3) {
                stepNum = stepNum - 3;
            }

            assStepColoring();

            $('.questionBox .txt_en span').html(sentence[stepNum]);
            $('.questionBox .txt_kr').text(translation[stepNum]);

            // assBefore block '평가하기' 버튼
            $('.btnBox .assBefore').css('display', 'block');
            $('.btnBox .assBefore').show();

            // assAfter none '다시하기, 다음문장' 버튼
            $('.btnBox .assAfter').css('display', 'none');
            $('.btnBox .assAfter').hide();

            if (stepNum == 2) {
                $('#retry_btn').html('<em class="fas fa-redo"></em>처음으로');
            } else {
                $('#retry_btn').html('<em class="fas fa-arrow-right"></em>다음문장');
            }


            $('#star1').css('display', 'none');
            $('#star2').css('display', 'none');
            $('#star3').css('display', 'none');
            $('#star4').css('display', 'none');
            $('#star5').css('display', 'none');
        });

        //sound play
        var audioFileUrl = "/aiaas/kr/audio/engedu/";
        var oriAudio = ["engedu_pron_ori1.mp3", "engedu_pron_ori2.mp3", "engedu_pron_ori3.mp3"];

        //원본파일 재생
        $('#oriAudio2').click(function () {
            new Audio(audioFileUrl + oriAudio[stepNum]).play();
        });
    });

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>
<script src="https://${engedu_host}/front/resources/static/js/init-v1.js"></script>