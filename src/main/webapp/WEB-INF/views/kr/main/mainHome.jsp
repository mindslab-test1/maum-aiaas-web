<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-ui-1.10.3.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/pop_common.css">

<!-- 5 .pop_simple -->
<div class="pop_simple">
	<div class="pop_bg"></div>
	<!-- .popWrap -->
	<div class="popWrap" style="width:400px;margin: 200px auto;">
<%--		<button class="pop_close" type="button">닫기</button>--%>
		<!-- .pop_bd -->
		<div class="pop_bd">
			<em class="fas fa-exclamation-triangle" style="color:#9cafd2;"></em>
			<h5 style="color: #33516b;;font-size:20px;font-weight: 400;margin: 10px;">서비스 이용 중지 안내</h5>
			<p style="color:#33516b;line-height: 22px;font-size: 14px;">카드를 <strong>변경</strong>해 주셔야 서비스 이용이 가능합니다.<br>
				<strong>재등록</strong>을 진행해 주시고, <strong>maum.ai</strong>를 이용해 주세요 <em class="far fa-smile-wink" style="height: 15px; line-height: 0;font-size: 17px;"></em></p>
		</div>
		<!-- //.pop_bd -->
		<div class="btn">
			<a class="btn" href="/member/krPaymentInfo" style="background: #2575f9;color:#fff;">재등록하기</a>
		</div>
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_simple -->








<div class="contents">

	<div class="content main_content">
		<h1>최고의 인공지능 API</h1>

		<!-- egArea -->
		<div class="egArea">
			<ul class="eg_tab_nav">
				<li><a href="#eg_contents01">추천</a></li>
				<li><a href="#eg_contents02">음성</a></li>
				<li><a href="#eg_contents03">시각</a></li>
				<li><a href="#eg_contents04">언어</a></li>
				<li><a href="#eg_contents05">분석</a></li>
				<li><a href="#eg_contents06">대화</a></li>
				<li><a href="#eg_contents07">영어교육</a></li>
			</ul>
			<div class="eg_tab_container">
				<!-- 추천엔진 -->
                <div id="eg_contents01" class="eg_contents">
                    <ul class="lst">
                        <!-- 음성:spe, 시각:vis, 언어:lang, 분석:ana, 대화:talk, 영어교육:eng  -->
                        <li class="vis">
                            <span class="imgBox img_vis_lsa">상품이미지</span>
                            <span class="eg_info">
                                <em class="ico">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_lipSyncAvatar_w.svg" alt="아이콘">
                                </em>
                                <em class="name">
                                    <strong>Lip Sync Avatar</strong>
                                    Lip Sync Avatar
                                </em>
                                <em class="desc">입력한 텍스트에 따라 자연스럽게 말하는 아바타가 생성됩니다.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/lipSyncAvatar/lipSyncAvatarMain">엔진 사용하기</a>
                            </span>
                        </li>
                        <%--<li class="vis">
                            <span class="imgBox img_vis_aiAvatar">상품이미지</span>
                            <span class="eg_info">
                                <em class="ico">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_avatar_w.svg" alt="아이콘">
                                </em>
                                <em class="name">
                                    <strong>Face-to-Face Avatar</strong>
                                    Face-to-Face Avatar
                                </em>
                                <em class="desc">동영상 내 인물의 얼굴 움직임을 포착하여, 사진 속 특정 인물이 이를 따라 움직이는 영상을 만드는 엔진입니다.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/avatar/avatarMain">엔진 사용하기</a>
                                <a href="https://youtu.be/zWbY6Vl4Xaw" target="_blank">
                                    <em class="fab fa-youtube"></em> 소개 영상보기
                                </a>
                            </span>
                        </li>--%>
                        <li class="spe">
                                <span class="point">
                                    <em class="best">BEST</em>
                                </span>
                            <span class="imgBox img_spe_tts">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_spe_1_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>음성생성</strong>
                                        Text-to-Speech
                                    </em>
                                    <em class="desc">실제 그 사람의 목소리 그대로 자연스럽게, 세계 최고 수준의 음질과 실시간 합성 속도를 제공합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/tts/krTtsMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/3c0Lgs-_3Yo" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <li class="spe">
                                <span class="point">
                                    <em class="best">BEST</em>
                                </span>
                            <span class="imgBox img_spe_stt">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_spe_2_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>음성인식</strong>
                                        Speech-to-Text
                                    </em>
                                    <em class="desc">음성을 텍스트로 변환하는 엔진으로, 다양한 학습모델을 활용할 수 있고 높은 인식률과 빠른 처리 속도를 제공합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/stt/krSttMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/HL8qoqOPcFo" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <%--<li class="vis">
							<span class="point">
								<em>NEW</em>
							</span>
                            <span class="imgBox img_vis_anmFc">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_animeFace_w.svg" alt="아이콘"></em>
								<em class="name">
									<strong>얼굴 애니메이션 필터</strong>
									Anime Face
								</em>
								<em class="desc">나의 얼굴을 애니메이션으로 생성하고, style 값을 조정하여 애니메이션을 꾸밀 수 있습니다.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/animeFace/krAnimeFaceMain">엔진 사용하기</a>
							</span>
                        </li>--%>
                        <li class="vis">
							<span class="point">
								<em>NEW</em>
							</span>
                            <span class="imgBox img_vis_litrDtc">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_loiteringDetect_w.svg"
                                                     alt="아이콘"></em>
								<em class="name">
									<strong>배회 감지</strong>
									Loitering Detection
								</em>
								<em class="desc">영역을 지정하여 해당 영역을 누군가가 장시간 배회할 시 알려줍니다.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/anomalyLoitering/krAnomalyLoiteringMain">엔진 사용하기</a>
							</span>
                        </li>
                        <li class="vis">
							<span class="point">
								<em>NEW</em>
							</span>
                            <span class="imgBox img_vis_fdDtc">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_fallDownDetect_w.svg"
                                                     alt="아이콘"></em>
								<em class="name">
									<strong>실신 감지</strong>
									Fall Down Detection
								</em>
								<em class="desc">특정 지역에서 실신하는 사람을 인식합니다.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/anomalyFalldown/krAnomalyFalldownMain">엔진 사용하기</a>
							</span>
                        </li>
                        <li class="vis">
							<span class="point">
								<em>NEW</em>
							</span>
                            <span class="imgBox img_vis_itrsDtc">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_IntrusionDetect_w.svg"
                                                     alt="아이콘"></em>
								<em class="name">
									<strong>침입 감지</strong>
									Intrusion Detection
								</em>
								<em class="desc">영역을 지정하여 해당 영역을 누군가가 침입할 시 알려줍니다.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/anomalyIntrusion/krAnomalyIntrusionMain">엔진 사용하기</a>
							</span>
                        </li>
                        <li class="lang">
							<span class="point">
								<em>NEW</em>
							</span>
                            <span class="imgBox img_lang_sts">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_txtStyTrans_w.svg" alt="아이콘"></em>
								<em class="name">
									<strong>스타일 변환</strong>
									Text Style Transfer
								</em>
								<em class="desc">문장을 원하는 스타일로 변경하는 동시에 의미는 일관성 있게 전달해줍니다.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/styleTransfer/krStyleTransferMain">엔진 사용하기</a>
							</span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_itf">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_itf_w.svg" alt="아이콘"></em>
								<em class="name">
									<strong>의도 분류</strong>
									ITF(Intent Finder)
								</em>
								<em class="desc">입력된 질문의 의도를 파악하여 알려줍니다.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/itf/krItfMain">엔진 사용하기</a>
								<a href="https://youtu.be/TllWVMmRF58" target="_blank"><em class="fab fa-youtube"></em>
									소개 영상보기</a>
							</span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_xdc">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_3_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>텍스트 분류</strong>
                                        eXplainable Document Classifier
                                    </em>
                                    <em class="desc">뉴스 기사를 입력하면 기사의 주제를 정확하게 분류해 냅니다. 더불어 분류의 근거를 문장 단위와 단어 단위로 제공하는 '설명 가능한 AI'입니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/xdc/krXdcMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/dCzKeF79ofU" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_mrc">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_2_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>AI 독해</strong>
                                        Machine Reading Comprehension
                                    </em>
                                    <em class="desc">주어진 텍스트를 독해하여 문맥을 이해하고, 질문에 맞는 정답의 위치를 찾아내서 정답을 제공합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/mrc/krMrcMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/LKWa40r5RcI" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                    </ul>
                </div>
				<!-- 추천엔진 -->

				<!-- 음성엔진 -->
                <div id="eg_contents02" class="eg_contents">
                    <ul class="lst">
                        <!-- 음성:spe, 시각:vis, 언어:lang, 분석:ana, 대화:talk, 영어교육:eng  -->
                        <li class="spe">
                            <span class="point"><em class="best">BEST</em></span>
                            <span class="imgBox img_spe_tts">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_spe_1_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>음성생성</strong>
                                        Text-to-Speech
                                    </em>
                                    <em class="desc">실제 그 사람의 목소리 그대로 자연스럽게, 세계 최고 수준의 음질과 실시간 합성 속도를 제공합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/tts/krTtsMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/3c0Lgs-_3Yo" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <li class="spe">
                            <span class="point"><em class="best">BEST</em></span>
                            <span class="imgBox img_spe_stt">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_spe_2_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>음성인식</strong>
                                        Speech-to-Text
                                    </em>
                                    <em class="desc">음성을 텍스트로 변환하는 엔진으로, 다양한 학습모델을 활용할 수 있고 높은 인식률과 빠른 처리 속도를 제공합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/stt/krSttMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/HL8qoqOPcFo" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <li class="spe">
                            <span class="imgBox img_spe_den">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_spe_3_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>음성정제</strong>
                                        Denoise
                                    </em>
                                    <em class="desc">음성에 섞여있는 배경음과 같이, 음성 내의 다양한 잡음을 제거합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/cnnoise/krCnnoiseMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/siu3mfWznbE" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <li class="spe">
                            <span class="imgBox img_spe_vf">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_spe_5_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>Voice Filter</strong>
                                        Voice Filter
                                    </em>
                                    <em class="desc">내 목소리와 다른 사람의 목소리에서 내 목소리를 분리해냅니다. 마인즈랩이 구글에 이어 구현에 성공한 엔진입니다.(2019년 6월)</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/voicefilter/krVoicefilterMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/4xPpFKGu3ZM" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <%--<li class="spe">
                            <span class="imgBox img_spe_vr">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_spe_6_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>화자 인증</strong>
                                        Voice Recognition
                                    </em>
                                    <em class="desc">사람의 음성 데이터를 Vector화하고 그 값을 대조하여 목소리를 인식합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/vr/krVoiceRecogMain">엔진 사용하기</a>
                                </span>
                        </li>--%>
                    </ul>
                </div>
				<!-- 음성엔진 -->


                <!-- 시각엔진 -->
                <div id="eg_contents03" class="eg_contents">
                    <ul class="lst">
                        <!-- 음성:spe, 시각:vis, 언어:lang, 분석:ana, 대화:talk, 영어교육:eng  -->
                        <li class="vis">
                            <span class="imgBox img_vis_lsa">상품이미지</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_lipSyncAvatar_w.svg" alt="아이콘"></em>
                                <em class="name">
                                    <strong>Lip Sync Avatar</strong>
                                    Lip Sync Avatar
                                </em>
                                <em class="desc">입력한 텍스트에 따라 자연스럽게 말하는 아바타가 생성됩니다.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/lipSyncAvatar/lipSyncAvatarMain">엔진 사용하기</a>
                            </span>
                        </li>
                        <%--<li class="vis">
                            <span class="imgBox img_vis_aiAvatar">상품이미지</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_avatar_w.svg" alt="아이콘"></em>
                                <em class="name">
                                    <strong>Face-to-Face Avatar</strong>
                                    Face-to-Face Avatar
                                </em>
                                <em class="desc">동영상 내 인물의 얼굴 움직임을 포착하여, 사진 속 특정 인물이 이를 따라 움직이는 영상을 만드는 엔진입니다.</em>
                            </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/avatar/avatarMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/zWbY6Vl4Xaw" target="_blank">소개 영상보기</a>
                                </span>
                        </li>--%>
                        <%--<li class="vis">
                            <span class="imgBox img_vis_fr">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_6_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>Face Recognition</strong>
                                        Face Recognition
                                    </em>
                                    <em class="desc">사람의 얼굴 데이터를 Vector화하고 그 값을 대조하여 얼굴을 인식합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/fr/krFaceRecogMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/qx2fEuP21dA" target="_blank">소개 영상보기</a>
                                </span>
                        </li>--%>
                        <li class="vis">
                            <span class="imgBox img_vis_esr">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_5_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>ESR</strong>
                                        Enhanced Super Resolution
                                    </em>
                                    <em class="desc">작은 크기의 이미지를 손실률을 최소화하여 확대해 줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/superResolution/krSuperResolutionMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/FdrgqudL9rU" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <%--<li class="vis">
                            <span class="imgBox img_vis_vsr">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_vsr_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>VSR</strong>
                                        Video Super Resolution
                                    </em>
                                    <em class="desc">저화질 영상의 해상도를 2배 높여 줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/vsr/vsrMain">엔진 사용하기</a>
                                </span>
                        </li>--%>
                        <%--<li class="vis">
							<span class="point">
								<em>NEW</em>
							</span>
                            <span class="imgBox img_vis_hlst3D">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_hlst3D_w.svg" alt="아이콘"></em>
								<em class="name">
									<strong>3D 인테리어</strong>
									Holistic 3D
								</em>
								<em class="desc">방 안을 360도에서 촬영한 파노라마 이미지를 3D 모델로 생성합니다.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/holistic3d/krHolistic3dMain">엔진 사용하기</a>
							</span>
                        </li>--%>
                        <li class="vis">
                            <span class="imgBox img_vis_tti">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_2_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>AI 스타일링</strong>
                                        Text-to-Image for fashion
                                    </em>
                                    <em class="desc">패션에 대한 설명 텍스트를 입력하면 이를 이미지로 생성해 냅니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/tti/krTtiMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/7Q88bh2Qqro" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <%--<li class="vis">
                            <span class="imgBox img_vis_tr">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_1_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>텍스트 제거</strong>
                                        Text Removal
                                    </em>
                                    <em class="desc">이미지에 있는 텍스트를 찾아 내어 제거해줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/textRemoval/krTextRemovalMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/77rzEwM8cvo" target="_blank">소개 영상보기</a>
                                </span>
                        </li>--%>
                        <li class="vis">
                            <span class="imgBox img_vis_sr">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_10_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>이미지 자막 인식</strong>
                                        Subtitle Recognition
                                    </em>
                                    <em class="desc">이미지 내에 있는 자막을 인식하여 추출하는 인공지능 엔진입니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/subtitleRecog/krSubtitleRecogMain">엔진 사용하기</a>
                                </span>
                        </li>
                        <li class="vis">
                            <span class="imgBox img_vis_ipr">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_7_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>인물 포즈 인식</strong>
                                        Image Pose Recognition
                                    </em>
                                    <em class="desc">이미지 내에 있는 사람의 포즈를 인식하여 추출하고 시각화하는 엔진입니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/poseRecog/krPoseRecogMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/TCd-CwrJFW8" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <li class="vis">
                            <span class="imgBox img_vis_ft">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_8_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>얼굴 추적</strong>
                                        Face Tracking
                                    </em>
                                    <em class="desc">동영상 내에 있는 얼굴을 인식하여 추출하는 인공지능 엔진입니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/faceTracking/krFaceTrackingMain">엔진 사용하기</a>
                                </span>
                        </li>
                        <li class="vis">
                            <span class="imgBox img_vis_fd">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_face_white.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>얼굴 마스킹</strong>
                                        Face Detection
                                    </em>
                                    <em class="desc">사람의 얼굴을 인식하여 검출 후, 비식별화합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/avr/krFaceDetectionMain">엔진 사용하기</a>
                                </span>
                        </li>
                        <%--<li class="vis">
							<span class="point"><em>NEW</em></span>
                            <span class="imgBox img_vis_anmFc">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_animeFace_w.svg" alt="아이콘"></em>
								<em class="name">
									<strong>얼굴 애니메이션 필터</strong>
									Anime Face
								</em>
								<em class="desc">나의 얼굴을 애니메이션으로 생성하고, style 값을 조정하여 애니메이션을 꾸밀 수 있습니다.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/animeFace/krAnimeFaceMain">엔진 사용하기</a>
							</span>
                        </li>--%>
                        <li class="vis">
                            <span class="imgBox img_vis_hs">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_hair_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>헤어 컬러 인식</strong>
                                        Hair Segmentation
                                    </em>
                                    <em class="desc">이미지 속 머리카락 영역을 인식하여 표시해주고, 색상을 분류해줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/hairSegmentation/hairSegmentationMain">엔진 사용하기</a>
                                </span>
                        </li>
                        <li class="vis">
                            <span class="imgBox img_vis_cmad">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_cmad_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>의상 특징 인식</strong>
                                        Clothing Multi-Attributes Detection
                                    </em>
                                    <em class="desc">이미지 내 인물의 영역을 표시하고, 상하의의 속성 인식 결과를 나타내줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/clothingDetection/clothingDetectionMain">엔진 사용하기</a>
                                </span>
                        </li>
                        <li class="vis">
                            <span class="imgBox img_vis_lpr">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_4_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>차량 번호판 인식</strong>
                                        License Plate Recognition
                                    </em>
                                    <em class="desc">도로상에서 달리는 차량의 번호판 위치를 검출하고 번호를 인식해줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/avr/krPlateRecogMain">엔진 사용하기</a>
                                </span>
                        </li>
                        <li class="vis">
                            <span class="imgBox img_vis_wd">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_windshield_white.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>차량 유리창 마스킹</strong>
                                        Windshield Detection
                                    </em>
                                    <em class="desc">도로상에서 달리는 차량의 유리창 위치를 인식하여 비식별화합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/avr/krAvrMain">엔진 사용하기</a>
                                </span>
                        </li>
                        <li class="vis">
                            <span class="imgBox img_vis_ad">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_9_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>역주행 감지</strong>
                                        Reverse Direction Detection
                                    </em>
                                    <em class="desc">보행중인 사람의 역주행을 감지하여 방향전환의 시작 시점을 알려줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/anomalyReverse/krAnomalyReverseMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/GF_yP18EAIM" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <li class="vis">
                            <span class="point"><em>NEW</em></span>
                            <span class="imgBox img_vis_litrDtc">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_loiteringDetect_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>배회 감지</strong>
                                        Loitering Detection
                                    </em>
                                    <em class="desc">영역을 지정하여 해당 영역을 누군가가 장시간 배회할 시 알려줍니다.</em>
                                </span>
                            <span class="btnBox">
                                <a href="/cloudApi/anomalyLoitering/krAnomalyLoiteringMain">엔진 사용하기</a>
                            </span>
                        </li>
                        <li class="vis">
                            <span class="point"><em>NEW</em></span>
                            <span class="imgBox img_vis_fdDtc">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_fallDownDetect_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>실신 감지</strong>
                                        Fall Down Detection
                                    </em>
                                    <em class="desc">특정 지역에서 실신하는 사람을 인식합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/anomalyFalldown/krAnomalyFalldownMain">엔진 사용하기</a>
                                </span>
                        </li>
                        <li class="vis">
                            <span class="point"><em>NEW</em></span>
                            <span class="imgBox img_vis_itrsDtc">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_IntrusionDetect_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>침입 감지</strong>
                                        Intrusion Detection
                                    </em>
                                    <em class="desc">영역을 지정하여 해당 영역을 누군가가 침입할 시 알려줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/anomalyIntrusion/krAnomalyIntrusionMain">엔진 사용하기</a>
                                </span>
                        </li>
                        <%--<li class="vis">
                            <span class="imgBox img_vis_bp">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_positioning_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>치아 교정기 포지셔닝</strong>
                                        Bracket Positioning
                                    </em>
                                    <em class="desc">주어진 치아에 교정기를 부착할 위치를 선정합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/positioning/positioningMain">엔진 사용하기</a>
                                </span>
                        </li>--%>
                    </ul>
                </div>
                <!-- 시각엔진 -->


				<!-- 언어엔진 -->
                <div id="eg_contents04" class="eg_contents">
                    <ul class="lst">
                        <!-- 음성:spe, 시각:vis, 언어:lang, 분석:ana, 대화:talk, 영어교육:eng  -->
                        <li class="lang">
                            <span class="imgBox img_lang_cor">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_correct_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>문장 교정</strong>
                                        Bert Correction
                                    </em>
                                    <em class="desc">잘못된 한글 문장을 문맥에 맞게 교정해줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/correction/correctionMain">엔진 사용하기</a>
                                </span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_kon">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_convers_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>한글 변환</strong>
                                        Konglish
                                    </em>
                                    <em class="desc">영어 단어 또는 한글과 영어가 혼용된 문장에서 영어 단어들을 외래어 표기법에 가까운 한글로 변환시켜줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/conversion/conversionMain">엔진 사용하기</a>
                                </span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_nlu">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_1_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>자연어 이해</strong>
                                        Natural Language Understanding
                                    </em>
                                    <em class="desc">문장을 입력하면 형태소 분석과 개체명 인식 결과를 제공해 줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/nlu/krNluMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/4zgfHlAdNDA" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_mrc">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_2_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>AI 독해</strong>
                                        Machine Reading Comprehension
                                    </em>
                                    <em class="desc">주어진 텍스트를 독해하여 문맥을 이해하고, 질문에 맞는 정답의 위치를 찾아내서 정답을 제공합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/mrc/krMrcMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/LKWa40r5RcI" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_xdc">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_3_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>텍스트 분류</strong>
                                        eXplainable Document Classifier
                                    </em>
                                    <em class="desc">뉴스 기사를 입력하면 기사의 주제를 정확하게 분류해 냅니다. 더불어 분류의 근거를 문장 단위와 단어 단위로 제공하는 '설명 가능한 AI'입니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/xdc/krXdcMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/dCzKeF79ofU" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
<%-- 아래 clickbait 링크 넣어야됨!!!!--%>
                        <%--<li class="lang">
							<span class="point"><em>NEW</em></span>
                            <span class="imgBox img_lang_clbt">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_clickbait_w.svg" alt="아이콘"></em>
								<em class="name">
									<strong>낚시성 뉴스 분류</strong>
									Clickbait News
								</em>
								<em class="desc">뉴스의 제목과 본문의 내용이 다른 낚시성 뉴스 여부를 판별해줍니다.</em>
							</span>
                            <span class="btnBox">
								<a href="해당 페이지 링크 넣어주세요">엔진 사용하기</a>
							</span>
                        </li>--%>
                        <li class="lang">
                            <span class="imgBox img_lang_gpt">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_5_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>문장 생성</strong>
                                        GPT-2 (Generative Pre-Training)
                                    </em>
                                    <em class="desc">문장을 입력하면, 언어 모델 기반의 새로운 문장을 생성합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/gpt/krGptMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/RfjUCSrnImI" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_hmd">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_6_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>패턴 분류</strong>
                                        Hierarchical Multiple Dictionary
                                    </em>
                                    <em class="desc">정규 표현식으로 표현된 긍정, 부정 문장 패턴을 통해서 문장의 감정을 파악하고 분류합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/hmd/krHmdMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/W5HP0i3ZRgA" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_itf">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_itf_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>의도 분류</strong>
                                        ITF(Intent Finder)
                                    </em>
                                    <em class="desc">입력된 질문의 의도를 파악하여 알려줍니다.</em>
                                </span>
                            <span class="btnBox">
                                <a href="/cloudApi/itf/krItfMain">엔진 사용하기</a>
                                <a href="https://youtu.be/TllWVMmRF58" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                            </span>
                        </li>
                        <li class="lang">
							<span class="point"><em>NEW</em></span>
                            <span class="imgBox img_lang_sts">상품이미지</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_txtStyTrans_w.svg" alt="아이콘"></em>
								<em class="name">
									<strong>스타일 변환</strong>
									Text Style Transfer
								</em>
								<em class="desc">문장을 원하는 스타일로 변경하는 동시에 의미는 일관성 있게 전달해줍니다.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/styleTransfer/krStyleTransferMain">엔진 사용하기</a>
							</span>
                        </li>
                    </ul>
                </div>
				<!-- 언어엔진 -->


				<!-- 분석엔진 -->
                <div id="eg_contents05" class="eg_contents">
                    <ul class="lst">
                        <!-- 음성:spe, 시각:vis, 언어:lang, 분석:ana, 대화:talk, 영어교육:eng  -->
                        <li class="ana">
                            <span class="imgBox img_ana_data">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_ana_data_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>데이터 상관분석</strong>
                                        Data Analysis
                                    </em>
                                    <em class="desc">데이터의 연관 관계를 분석하는 다양한 알고리즘들을 제공합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/dataAnalysis/krDataAnalysis">엔진 사용하기</a>
                                </span>
                        </li>
                    </ul>
                </div>
				<!-- 분석엔진 -->
				<!-- 대화엔진 -->
                <div id="eg_contents06" class="eg_contents">
                    <ul class="lst">
                        <!-- 음성:spe, 시각:vis, 언어:lang, 분석:ana, 대화:talk, 영어교육:eng  -->
                        <%--<li class="talk">
                            <span class="imgBox img_talk_nqa">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_bot_1_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>NQA 봇</strong>
                                    </em>
                                    <em class="desc">펀드, 연금, CMA, 주식, 선물, 매뉴얼, 용어 사전 등 금융 관련 상담 업무를 지원해 줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/nqa/krNqaMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/5-STj9pO3WE" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>--%>
                        <li class="talk">
                            <span class="imgBox img_talk_chat">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_bot_2_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>MRC 봇<!--&middot;날씨봇--></strong>
                                    </em>
                                    <em class="desc">아무리 긴 텍스트도 읽고 검색하여 답변해줍니다. 현재는 위키와 뉴스 본문을 읽어주는 봇이 준비되어있습니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/chatbot/krChatbotMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/YTD5jFjmoJ4" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <%--<li class="talk">
                            <span class="imgBox img_talk_kbqa">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_bot_2_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>KBQA 봇<!--&middot;날씨봇--></strong>
                                    </em>
                                    <em class="desc">위키피디아의 인포박스를 KBQA 엔진을 이용하여 검색하여 답변해줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/chatbot/krKbqaMain">엔진 사용하기</a>
                            </span>
                        </li>--%>
                        <li class="talk">
                            <span class="imgBox img_talk_hotel">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_bot_3_fold_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>호텔 컨시어지 챗봇</strong>
                                    </em>
                                    <em class="desc">호텔 예약 전부터 퇴실까지, 호텔 관련 문의 및 컨시어지 서비스를 담당하는 챗봇입니다. 24시간 응대, 다국어 서비스를 통하여 고객 응대율을 높여줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/hotelbot/krHotelBot">엔진 사용하기</a>
                                    <a href="https://youtu.be/au9DV9jrDkQ" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                    </ul>
                </div>
				<!-- 대화엔진 -->
				<!-- 영어교육 -->
                <div id="eg_contents07" class="eg_contents">
                    <ul class="lst">
                        <!-- 음성:spe, 시각:vis, 언어:lang, 분석:ana, 대화:talk, 영어교육:eng  -->
                        <li class="eng">
                            <span class="imgBox img_eng_stt">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_eng_1_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>교육용 STT</strong>
                                        STT for English Education
                                    </em>
                                    <em class="desc">학습자의 영어 발화를 인식하여 정확하게 읽었는지 평가해 줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/engedu/krEngeduSttMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/0cWEV-NKsPQ" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <li class="eng">
                            <span class="imgBox img_eng_edu">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_eng_2_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>문장 발음 평가</strong>
                                        Automatic Pronunciation Scoring
                                    </em>
                                    <em class="desc">학습자 영어 발화의 유창성을 원어민과 비교하여 평가해 줍니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/engedu/krEngeduPronMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/LLjaoou4aW4" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                        <li class="eng">
                            <span class="imgBox img_eng_pa">상품이미지</span>
                            <span class="eg_info">
                                    <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_eng_3_w.svg" alt="아이콘"></em>
                                    <em class="name">
                                        <strong>파닉스 평가</strong>
                                        Phonics Assessment
                                    </em>
                                    <em class="desc">영어 단어의 특정 음소를 지정하여 음소 단위의 발음 평가를 제공합니다.</em>
                                </span>
                            <span class="btnBox">
                                    <a href="/cloudApi/engedu/krEngeduPhonicsMain">엔진 사용하기</a>
                                    <a href="https://youtu.be/Cdza6v-kKkc" target="_blank"><em class="fab fa-youtube"></em> 소개 영상보기</a>
                                </span>
                        </li>
                    </ul>
                </div>
				<!-- 영어교육 -->
			</div>
		</div>
		<!-- //egArea -->
	</div>

</div>

<!-- .page loading -->
<%--<div id="pageldg">--%>
<%--    <span class="out_bg">--%>
<%--        <em>--%>
<%--            <strong>&nbsp;</strong>--%>
<%--            <strong>&nbsp;</strong>--%>
<%--            <strong>&nbsp;</strong>--%>
<%--            <b>&nbsp;</b>--%>
<%--        </em>--%>
<%--    </span>--%>
<%--</div>--%>
<!-- //.page loading -->

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/main.js"></script>

<script type="text/javascript">   
$(window).load(function() {    
	$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() { 
		$(this).remove(); 
	});
}); 
</script>
<script type="text/javascript">
// jQuery(function(){
// 	jQuery("a.btn_movLayer").movLayer();
// });
	
jQuery.event.add(window,"load",function(){
	$(document).ready(function (){
		$('.list1').trigger('click');
		$('.list2').trigger('click');

	});	
});	
	

</script>
	
	
<script type="text/javascript">	

//	체크박스 한개만 선택
function oneCheckbox(chk){

   var obj = document.getElementsByName("select");

      for(var i=0; i<obj.length; i++){
            if(obj[i] != chk){
                  obj[i].checked = false;
                  }
            }
    }		
</script>