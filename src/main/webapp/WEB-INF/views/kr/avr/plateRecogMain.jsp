<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/avr/croppie.css" />


<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/avr/avr.js?ver=20210603"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/avr/croppie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/avr/filesaver.js"></script>
<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">


<!--사용자 가이드 -->
<div class="pop_simple" id="user_guide">
	<div class="pop_bg"></div>
	<!-- .popWrap -->
	<div class="popWrap">
		<div class="pop_hd">
			차량 번호판 인식 사용자 가이드
			<em class="fas fa-times close_layer"></em>
		</div>
		<!-- .pop_bd -->
		<div class="pop_bd">
			<div class="pop_body">

				<p>위 번호판 인식 모델은 실제 도로 위 CCTV 상황에 적용되어 있으며, 아래와 같은 사항을주의하여 이용해주시길 바랍니다.<br>
					- 인식에 해당되는 번호판은 아래 5가지 번호판입니다. (외교나 건설 등 특수 번호판은제외)<br>
					- 대한민국 번호판 규칙에 따른 번호판만 해당됩니다. 즉, 임의의 가짜 번호판은부적절합니다.<br>
					- 폰트 및 간격의 임의적인 변화에 따라 인식이 어려워지게 됩니다.<br>
				</p>
				<figure>
					<img class="img2" src="${pageContext.request.contextPath}/aiaas/common/images/img_smartX02.png" alt="인식 가능한 5가지 번호판 예시">
					<figcaption>&lt;인식 가능한 5가지 번호판 예시&gt;</figcaption>
				</figure>
				<figure>
					<img class="img3" src="${pageContext.request.contextPath}/aiaas/common/images/img_smartX03.png" alt="대한민국 번호판 규칙">
					<figcaption>&lt;대한민국 번호판 규칙&gt;</figcaption>
				</figure>

				<img class="img4" src="${pageContext.request.contextPath}/aiaas/common/images/img_smartX04.png" alt="대한민국 번호판 규칙">

				<figure>
					<img class="img5" src="${pageContext.request.contextPath}/aiaas/common/images/img_smartX05.png" alt="잘못된 번호판의 예">
					<figcaption>&lt;잘못된 번호판의 예&gt;</figcaption>
				</figure>

			</div>

		</div>
		<!-- //.pop_bd -->

	</div>
	<!-- //.popWrap -->
</div>
<!-- // \user_guide -->

<!-- .contents -->
<div class="contents">
	<div class="content api_content">
		<h1 class="api_tit">도로상의 객체 인식</h1>
		<ul class="menu_lst vision_lst">
			<li class="tablinks" onclick="openTap(event, 'avrdemo')" id="defaultOpen"><button type="button">엔진</button></li>
			<li class="tablinks" onclick="openTap(event, 'avrexample')"><button type="button">적용사례</button></li>
			<li class="tablinks" onclick="openTap(event, 'avrmenu')"><button type="button">매뉴얼</button></li>
<%--			<li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
		</ul>
		<div class="demobox" id="avrdemo">
			<p><span>차량 번호판 인식</span> <small>(License Plate Recognition)</small> </p>
			<span class="sub">도로상에서 달리는 차량의 번호판 위치를 검출하고 번호를 인식해줍니다.</span>
			<!--textremoval_box-->
			<div class="demo_layout avr_box">
				<!--avr_1-->
				<div class="avr_1">
					<div class="fl_box">
						<p><em class="far fa-file-image"></em><strong>샘플 파일</strong>로 테스트 하기</p>
						<div class="sample_box">
							<div class="sample_1">
								<div class="radio">
									<input type="radio" id="sample1" name="avr_option" value="plate" checked>
									<label for="sample1" class="female">
										<div class="img_area">
											<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_avr.png" alt="sample img 1" />
										</div>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="fr_box">
						<p><em class="far fa-file-image"></em><strong>내 파일</strong>로 해보기</p>
						<div class="uplode_box">
							<div class="btn" id="uploadFile">
								<em class="fas fa-times hidden close"></em>
								<em class="far fa-file-image hidden"></em>
								<label for="demoFile" class="demolabel">이미지 업로드</label>
								<input type="file" id="demoFile" class="demoFile" accept=".jpg, .png" >
							</div>
							<div class="user_guide"><em class="fas fa-info-circle"></em>&nbsp;&nbsp;사용자 가이드 보기&nbsp;&nbsp;<em class="fas fa-chevron-right"></em></div>
							<ul>
								<li>* 지원가능 파일 확장자: .jpg, .png</li>
								<li>* 이미지 용량 2MB 이하만 가능합니다.</li>
								<li>* CCTV 환경에 맞게 촬영된 사진이어야 합니다.</li>
								<li>* 차량이 잘리지 않고, 도로 위에 존재해야 합니다.</li>
								<li>* 샘플과 비슷한 각도 및 거리의 이미지를 사용해주세요.</li>
							</ul>
						</div>
					</div>
<%--					<div class="bottom_box">--%>
<%--						<p><em class="fas fa-expand"></em><strong>검출 조건 선택</strong></p>--%>
<%--						<div class="range_box">--%>
<%--							<div class="radio">--%>
<%--								<input type="radio" id="radio2" name="avr_option" value='window' checked="checked">--%>
<%--								<label for="radio2">차량 앞 유리창 검출 및<br>비식별화</label>--%>
<%--							</div>						--%>
<%--							<div class="radio">--%>
<%--								<input type="radio" id="radio3" name="avr_option" value='plate'>--%>
<%--								<label for="radio3">차량 번호판 검출 및 인식</label>--%>
<%--							</div>						--%>
<%--							<div class="radio">--%>
<%--								<input type="radio" id="radio" name="avr_option" value='face'>--%>
<%--								<label for="radio">얼굴검출 및 비식별화</label>--%>
<%--							</div>--%>
<%--						</div>--%>
<%--					</div>--%>
					<div class="btn_area">
						<button type="button" class="btn_start" id="change_txt">이미지 편집</button>
					</div>
				</div>
				<!--avr_1-->
				<!--edit_box-->
				<div class="edit_box">
					<p><em class="far fa-file-image"></em>이미지 편집</p>
					<div class="img_box">
						<em class="fas fa-minus minus"></em>
						<em class="fas fa-plus plus"></em>
						<!--불러온 이미지 들어갈 곳-->
						<img src="" alt="불러온 이미지" id="previewImg">
					</div>
					<p class="desc">사진의 하단 바를 조절하여 크기를, 마우스를 움직여서 상하좌우 위치를 적절히 편집해주세요.</p>
					<div class="btn_area">
						<a class="btn_start btn_cancel" href="">취소</a>
						<a class="btn_start" id="recogButton">결과보기</a>
					</div>
				</div>
				<!--//edit_box-->
				<!--avr_2-->
				<div class="avr_2">
					<p><em class="far fa-file-image"></em>이미지 처리중</p>
					<div class="loding_box ">
						<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#f7778a" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

						<p>약간의 시간이 소요 됩니다. (약 3초 내외)</p>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
					</div>
				</div>
				<!--avr_2-->
				<!--avr_3-->
				<div class="avr_3" >
					<p><em class="far fa-file-image"></em>결과 파일</p>
					<div class="result_file" >
						<div class="imgBox">
							<img src="" id="resultImg" alt="결과 이미지" />
						</div>
						<%--<div class="recogbox" id="testRecog" style="display:block;">--%>	<!-- 차량 번호판 인식일 경우에만 display : blcok  -->
						<div class="recogbox" id="recogbox" style="display:none">
							<span>검출 및 인식 결과</span>
							<div class="carnumber"><img src="" id="carnumber" alt="번호판 이미지"/></div>
							<div class="carnumber_txt">33호 5598</div>
							<div class="errortxt"><em class="fas fa-exclamation-triangle"></em>&nbsp; 사진의 크기와 위치를 다시 맞춰주세요. </div>
						</div>
						<a id="save" onclick="downloadResultImg();" class="btn_dwn"><em class="far fa-arrow-alt-circle-down"></em> 결과 파일 다운로드</a>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
					</div>
				</div>
				<!--avr_3-->
			</div>
			<!--// textremoval_box-->
		</div>
		<!-- .demobox -->

		<!--.avrmenu-->
		<div class="demobox vision_menu" id="avrmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API 공통 가이드
					</div>
					<p class="sub_title">개발 환경 세팅</p>
					<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
					<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
					<p class="sub_title">키 발급</p>
					<p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
					<p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
					<p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
					<p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
					<p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
				</div>
				<div class="guide_group">
					<div class="title">
						차량 번호판 인식 <small>(License Plate Recognition)</small>
					</div>
					<p class="sub_txt">도로상에서 달리는 차량의 번호판 위치를 검출하고 번호를 인식해줍니다.</p>
					<span class="sub_title">
								준비사항
					</span>
					<p class="sub_txt">- Input: CCTV에서 찍힌 차량 이미지 파일</p>
					<ul>
						<li>확장자: .jpg, .png.</li>
						<li>용량: 2MB 이하 </li>
					</ul>
					<span class="sub_title">
								 실행 가이드
							</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : http://api.maum.ai/smartXLoad/PlateRecog</li>
					</ul>
					<p class="sub_txt">② Request 파라미터 설명 </p>
					<table>
						<tr>
							<th>키</th>
							<th>설명</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId </td>
							<td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청 </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청 </td>
							<td>string</td>
						</tr>
						<tr>
							<td>car_img</td>
							<td>차량 이미지 파일type:file (.jpg,.png) </td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request 예제 </p>
					<div class="code_box">
<pre>
curl --location --request POST 'http://api.maum.ai/smartXLoad/PlateRecog' \
--header 'Content-Type: multipart/form-data' \
--form 'apiId= 발급받은 API ID' \
--form 'apiKey= 발급받은 API KEY' \
--form 'car_img= 번호판 이미지 파일'
</pre>
					</div>

					<p class="sub_txt">④ Response 예제 </p>

					<div class="code_box">
<pre>
{
   "plt_boxed_img": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAgIC...",
   "plt_img": "AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAgICA...",
   "plt_num": "41어0124"
}
</pre>
					</div>
				</div>
			</div>

		</div>
		<!--//avrmenu-->
		<!--.avrexample-->
		<div class="demobox" id="avrexample">
			<p><em style="color:#f7778a;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>
			<span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
			<!--도로상의 객체 인식(AVR) -->
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>노후 경유차량 <strong>인식</strong></span>
							</dt>
							<dd class="txt">차량 번호판 인식을 통해 노후 경유차량을 감지하고 벌금을 부과할 수 있는 자동 모니터링 시스템을 구축할 수 있습니다.
								<span><em class="fas fa-book-reader"></em> Reference: 서울시</span>
							</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_avr"><span>AVR</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>도난 및 신고 차량 검거</span>
							</dt>
							<dd class="txt">도난 또는 신고된 차량의 번호판을 이용하여 도로 CCTV 실시간 검거에 활용됩니다.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_avr"><span>AVR</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //도로상의 객체 인식(AVR) -->
		</div>
		<!--//.avrexample-->

	</div>
</div>
<!-- //.contents -->
<script>

var sampleImage1;
var data;


//파일명 변경
document.querySelector("#demoFile").addEventListener('change', function (ev) {
	document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
	var element = document.getElementById( 'uploadFile' );
	element.classList.remove( 'btn' );
	element.classList.add( 'btn_change' );
	$('.fl_box').css("opacity", "0.5");
});



jQuery.event.add(window,"load",function(){
	$(document).ready(function (){
		loadSample1();

		// step1->step2  (이미지 선택)
		$('.btn').on('click', function () {
//			$('.fl_box').css("opacity", "0.5");
			$('#change_txt').text('이미지 편집');

		});
		// step1->step2  (close button)
		$('em.close').on('click', function () {
			$(this).parent().removeClass("btn_change");
			$(this).parent().addClass("btn");
			$(this).parent().children('.demolabel').text('이미지 업로드');
			$('.fl_box').css('opacity', '1');
			//파일명 변경
			document.querySelector("#demoFile").addEventListener('change', function (ev) {
				document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
				var element = document.getElementById( 'uploadFile' );
				element.classList.remove( 'btn' );
				element.classList.add( 'btn_change' );
			});
		});
		// step1->step2
		$('#change_txt').on('click', function () {
			var $checked = $('input[name="avr_option"]:checked');
			var uploadfile = $('#demoFile');
			var demoFileTxt = uploadfile.val();
			if (demoFileTxt === "") {
				loadSample1();
				console.log("샘플로");

				if( $checked.val() === "plate"){
					console.log("옵션 2");

				}
				activateCroppie();
			}else{

				console.log("이미지 업로드");
				var url = URL.createObjectURL(uploadfile.get(0).files[0]);

				$('#previewImg').attr('src', url);
				activateCroppie();
			}
			$('.avr_1').hide();
			$('.edit_box').fadeIn(300);

		});


		// step2->step3
		$('.btn_back1').on('click', function () {
			// if(request){ request.abort(); }
			document.getElementById('recogbox').style.display = "block";
			window.location.reload();
		});

		// step3->step1
		$('.btn_back2').on('click', function () {
			window.location.reload();
		});


		//사용자 가이드 보기
		$('.user_guide').on('click', function(){
			$('#user_guide').fadeIn();
		});
		$('.close_layer').on('click', function(){
			$('#user_guide').fadeOut();
		});

	});
});


function loadSample1() {
	var blob = null;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "/aiaas/kr/images/img_avr.png");
	xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
	xhr.onload = function()
	{
		blob = xhr.response;//xhr.response is now a blob object
		sampleImage1 = new File([blob], "img_avr.jpg");

		var imgSrcURL = URL.createObjectURL(blob);
		var textr_output=document.getElementById('previewImg');
		textr_output.setAttribute("src",imgSrcURL);
	};

	xhr.send();
}

function downloadResultImg(){
	var img = document.getElementById('resultImg');
	var link = document.getElementById("save");
	link.href = img.src.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
	link.download = "AVR_IMAGE.JPG";
}
//API 탭
function openTap(evt, menu) {
var i, demobox, tablinks;
demobox = document.getElementsByClassName("demobox");
for (i = 0; i < demobox.length; i++) {
demobox[i].style.display = "none";
}
tablinks = document.getElementsByClassName("tablinks");
for (i = 0; i < tablinks.length; i++) {
tablinks[i].className = tablinks[i].className.replace(" active", "");
}
document.getElementById(menu).style.display = "block";
evt.currentTarget.className += " active";
}

document.getElementById("defaultOpen").click();

</script>