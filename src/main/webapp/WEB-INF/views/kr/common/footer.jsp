<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-05-22
  Time: 14:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>



<!-- .stn_inquiry -->
<div class="stn_inquiry main_footer" id="inquiry">
    <!-- .cont_box -->
    <div class="cont_box">
        <div class="info_box">
            <h4>Contact Us</h4>
            <ul>
                <li><em class="fas fa-headset"></em> 고객센터<a href="tel:+8216613222" class="info number">1661 - 3222</a></li>
                <li><em class="far fa-envelope"></em> 이메일 <a href="mailto:hello@mindslab.ai" class="info mail">hello@mindslab.ai</a></li>
            </ul>

            <div class="social_box">
                <a href="https://www.youtube.com/channel/UC8ocQaJFp-adzGtwcBuS3iQ?view_as=subscriber" target="_blank" title="유튜브"><em class="fab fa-youtube"></em></a>
                <a href="https://www.facebook.com/pg/mindsinsight" target="_blank" title="페이스북"><em class="fab fa-facebook-f"></em></a>
                <a href="https://post.naver.com/my.nhn?memberNo=45704243" target="_blank" title="네이버 블로그"><em class="fas fa-blog"></em></a>
            </div>
            <div class="info_desc">
                <em>© Copyright 2020 주식회사 마인즈랩</em>
                경기도 성남시 분당구 대왕판교로644번길 49 다산타워 5-6층 ㅣ 대표 유태준 ㅣ 사업자 등록번호 314-86-55446
            </div>
            <p>
                <a href="https://mindslab.ai:8080/kr/company" class="co_link "  target="_blank">회사소개</a> &nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="/home/krTermsMain" class="co_link " target="_blank">이용약관</a>&nbsp;&nbsp;|&nbsp;&nbsp;
                <a href="/home/krTermsMain#conditions" class="co_link " target="_blank">개인정보처리방침</a>
            </p>

        </div>

        <div class="inquiry_box">
            <div class="my_info">
                <input type="text" id="name">
                <label for="name">이름</label>
            </div>
            <div class="my_info">
                <input type="tel" pattern="[0-9]{3}-[0-9]{4}-[0-9]{4}" id="phone" >
                <label for="phone">연락처</label>
            </div>
            <div class="my_info email_info">
                <input type="email" id="mail">
                <label for="mail">이메일</label>
            </div>
            <div class="my_info ">
                <input type="text" id="company">
                <label for="company">회사명/직급</label>
            </div>
            <div class="my_txt">
                <textarea id="txt" ></textarea>
                <label for="txt">문의내용</label>
            </div>
            <button class="btn_send" id="sendMailToHello_landing">문의하기</button>
        </div>
    </div>
    <!-- //.cont_box -->
</div>
<!-- //.stn_inquiry -->


<!-- 2 .pop_confirm -->
<div class="pop_confirm" id="mail_success">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap">
        <!-- .pop_bd -->
        <div class="pop_bd">
            <!--내용 부분-->
            <p>문의내용이 접수되었습니다.<br>
                담당자가 빠른 시일 내에<br>
                답변 드리도록 하겠습니다.</p>
        </div>
        <!-- //.pop_bd -->
        <!--창닫기 버튼 -->
        <div class="btn">
            <button class="btn_close" >확인</button>
        </div>
        <!--창닫기 버튼 -->
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_confirm -->

<script>
    function validateEmail(sEmail) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            return false;
        }
    }
    $(document).ready(function() {
        $('#sendMailToHello_landing').on('click', function () {

            if(validateEmail( $('#mail').val() ) == false){
                alert("이메일 형식을 확인해 주세요!");
            }else if ($('#name').val() === '' || $('#phone').val() === '' || $('#mail').val() === '' || $('#txt').val() === '' || $('#company').val() === '') {

                alert("내용을 모두 입력해야 이메일을 전달할 수 있어요!");

            } else {
                var noti;

                var param = location.search;
                var host = window.location.hostname;
                var pathname = window.location.pathname;

                if (pathname == "/home/pricingPage"){
                    noti = "maum.ai Enterprise 가격 문의(KR)";
                }else if (pathname == "/home/krEcomindsMain"){
                    noti = "maum.ai ecoMINDs(KR)";
                }else if (pathname == "/home/academyForm"){
                    noti = "maum.ai 아카데미(KR)";
                }else {
                    noti = "maum.ai Landing Page(KR)"
                }


                let title = "[maum.ai] "+$("#name").val() + "님의 문의 사항입니다.";
                let msg = "이름 : " + $("#name").val() + "<br>이메일 : " + $('#mail').val() + "<br>연락처 : " + $("#phone").val() + "<br>회사명 : " + $("#company").val() + "<br>문의 내용 : " + $("#txt").val()
                + "<br><br><br>* "+noti+" 에서 보낸 메일입니다.";

                let formData = new FormData();

                formData.append('fromaddr', $('#mail').val());
                formData.append('toaddr', 'hello@mindslab.ai');
                formData.append('subject', title);
                formData.append('message', msg);
                formData.append($("#key").val(), $("#value").val());

                $.ajax({
                    type: 'POST',
                    async: true,
                    url: '/support/sendContactMail',
                    dataType: 'text',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (obj) {
                        $("#name").val('');
                        $("#office").val('');
                        $("#phone").val('');
                        $("#company").val('');
                        $("#txt").val('');
                        $("#mail").val('');
                        var placeholderLabel = $('.inquiry_box input, .inquiry_box textarea');
                        placeholderLabel.siblings('label').show();

                        $('#mail_success').fadeIn();

                    },
                    error: function (xhr, status, error) {
                        console.log("error");
                        alert("Contact Us 메일발송 요청 실패하였습니다.");
                        window.location.href = "/";
                    }
                });
            }
        });

        $('.btn_close').on('click', function () {
            $('#mail_success').fadeOut();
        });
        // 인풋 라벨
        var placeholderLabel = $('.inquiry_box input, .inquiry_box textarea');

        placeholderLabel.on('focus', function () {
            $(this).siblings('label').hide();
        });

        placeholderLabel.on('focusout', function () {
            if ($(this).val() === '') {
                $(this).siblings('label').show();
            }
        });


    });

</script>