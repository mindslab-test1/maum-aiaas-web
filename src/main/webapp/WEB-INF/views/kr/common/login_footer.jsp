<%--
  Created by IntelliJ IDEA.
  User: smr
  Date: 2020-11-17
  Time: 2:09 p.m.
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<!-- #footer -->
<div id="footer">
    <footer>
        <div class="footer">
            <a href="https://maum.ai/home/krTermsMain" target="_blank">이용약관 </a> ㅣ
            <a href="https://maum.ai/home/krTermsMain#conditions" target="_blank"> 개인정보처리방침 </a> ㅣ
            <a href="https://maum.ai/?lang=ko#inquiry" target="_blank"> 고객센터 </a>
            <span>ㅣ</span>
            <div class="lang">
                <span class="lang_select">한국어 <em class="fas fa-chevron-up"></em></span>
                <ul>
                    <li class="active">한국어</li>
                    <li><a href="https://maum.ai/?lang=en" target="_blank">English</a></li>
                </ul>
            </div>
        </div>
        <p>Copyright © 2020 주식회사 마인즈랩. All rights reserved.</p>
        <div class="m_lang">한국어 <a href="" title="English">English</a></div>
    </footer>

</div>
<!-- //#footer -->

<script>

    //footer 언어 체크
    $('.lang_select').on('click', function () {
        $(this).next().toggleClass('active');
        return false;
    });
    $(document).on('click', function (e) {
        var langOpen = $('footer .lang ul.active');
        if( langOpen.has(e.target).length === 0){
            langOpen.removeClass('active');
        }
    });
</script>