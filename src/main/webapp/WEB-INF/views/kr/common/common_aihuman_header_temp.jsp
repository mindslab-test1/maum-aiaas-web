<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
	String styleCss = application.getRealPath("${pageContext.request.contextPath}/aiaas/kr/css/login_renew.css");
	File style = new File(styleCss);
	Date lastModifiedStyle = new Date();

	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>
<%--<spring:eval expression="@config['floating.chat.url']" var="chatUrl"/>--%>

<!DOCTYPE html>
<html lang="ko">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="format-detection" content="telephone=no">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">

	<!-- Open Graph Tag -->
	<meta property="og:title"            content="maum.ai"/>
	<meta property="og:type"             content="website"/><!-- 웹 페이지 타입 -->
	<meta property="og:url"              content="https://maum.ai"/>
	<meta property="og:image"            content="${pageContext.request.contextPath}/aiaas/common/images/maum.ai_web.png"/>
	<meta property="og:description"      content="웹인공지능이 필요할땐 마음AI"/>
	<meta name="AdsBot-Google" content="index">
	<meta name="AdsBot-Google-Mobile" content="index">

	<!-- icon_favicon -->
	<link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
	<link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
	<title>maum.ai platform</title>
	<!-- resources -->
<%--	<link rel="stylesheet" type="text/css" href="${chatUrl}/css/chatbotAvatar.css"/>--%>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/pop_common.css?ver=<%=fmt.format(lastModifiedStyle)%>">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/maum_common.css?ver=<%=fmt.format(lastModifiedStyle)%>">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/login_renew.css?ver=<%=fmt.format(lastModifiedStyle)%>">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/user.css?ver=<%=fmt.format(lastModifiedStyle)%>">

	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/aibuilder/css/font.css"/>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/aibuilder/css/reset.css"/>
	<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/aibuilder/css/style.css"/>
	<%--<link rel="stylesheet" href="https://sds.maum.ai/css?host=mindslab"/>--%>
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/aibuilder/css/all.css">
	<!-- //resources -->

	<%--<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-1.11.0.min.js"></script>--%>

	<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-3.5.1.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-migrate-3.3.1.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/all-5.13.0.js" data-auto-replace-svg="nest"></script>

	<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/common.js?ver=<%=fmt.format(lastModifiedStyle)%>"></script>


<%--	<script type="text/javascript" src="${pageContext.request.contextPath}/aiass/aibuilder/js/jquery-1.11.2.min.js?ver=<%=fmt.format(lastModifiedStyle)%>"></script>--%>
<%--	<script type="text/javascript" src="${pageContext.request.contextPath}/aiass/aibuilder/js/jquery.easing.1.3.js?ver=<%=fmt.format(lastModifiedStyle)%>"></script>--%>
<%--	<script type="text/javascript" src="${pageContext.request.contextPath}/aiass/aibuilder/js/html5div.js?ver=<%=fmt.format(lastModifiedStyle)%>"></script>--%>
<%--	<script type="text/javascript" src="${pageContext.request.contextPath}/aiass/aibuilder/js/html5shiv.min.js?ver=<%=fmt.format(lastModifiedStyle)%>"></script>--%>

<%--	<script>--%>
<%--		var botUrl = "${chatUrl}/iframe/?projectId=-200";--%>
<%--	</script>--%>
<%--	<script src= "${chatUrl}/js/bot-export.js"></script>--%>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-122649087-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-122649087-1');
	</script>
	<meta name="google-site-verification" content="5xEMrR_G9l_kUaLCaQT2qr8rv2Neu66j8EEWErSwfzg" />

	<!-- 럭키오렌지 tag -->
	<script async defer src="https://tools.luckyorange.com/core/lo.js?site-id=dddcda82"></script>
</head>