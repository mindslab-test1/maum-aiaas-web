<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-05-22
  Time: 14:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<!-- footer -->
<div id="footerLanding">
    <div class="footer_inner">
        <div class="col">
            <div class="contact">
                <a href="tel:16613222">
                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/ico_headset_line.svg" alt="전화 걸기">
                    <span>1661-3222</span>
                </a>
                <a href="mailto:hello@mindslab.ai">
                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/ico_email_line.svg" alt="메일 보내기">
                    <span>hello@mindslab.ai</span>
                </a>
            </div>
            <div class="col_inner">
                <dl class="cs_info">
                    <dt>상담시간 | </dt>
                    <dd> <span>평일 09:00 ~ 18:00</span> <span>(점심시간 12:30 ~ 13:30),</span> <span>공휴일 휴무</span></dd>
                </dl>
                <div class="sns">
                    <a href="https://www.facebook.com/mindsinsight" target="_blank" title="페이스북 바로가기"><img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/ico_sns_facebook.svg" alt="페이스북 아이콘"></a>
                    <a href="https://www.youtube.com/channel/UC8ocQaJFp-adzGtwcBuS3iQ?view_as=subscriber" target="_blank" title="유튜브 바로가기"><img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/ico_sns_youtube.svg" alt="유튜브 아이콘"></a>
                    <a href="https://post.naver.com/my.nhn?memberNo=45704243" target="_blank" title="네이버 블로그 바로가기"><img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/ico_sns_naverblog.svg" alt="네이버블로그 아이콘"></a>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="links">
                <a href="https://mindslab.ai:8080/kr/company" target="_blank">회사소개</a>
                <a href="http://bit.ly/mindslab_recruit" target="_blank">채용공고</a>
                <a href="https://maum.ai/home/krTermsMain" target="_blank">이용약관</a>
                <a href="https://maum.ai/home/krTermsMain#conditions" target="_blank">개인정보처리방침</a>
            </div>
            <address>
                <span>(주) 마인즈랩 경기도 성남시 분당구 대왕판교로644번길 49 다산타워 6층</span><br>
                <span>대표 유태준 |</span>
                <span>사업자 등록번호 314-86-55446 |</span>
                <span>통신판매업신고번호 2019-대전유성-0094</span>
            </address>
            <p class="copyright">
                Copyright © 2021 MINDsLab. All rights reserved.
            </p>
        </div>
    </div>
</div>
<!-- //footer -->

<script>
    $(document).ready(function() {

    });

</script>