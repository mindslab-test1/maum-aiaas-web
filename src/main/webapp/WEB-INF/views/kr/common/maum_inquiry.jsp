<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="../common/common_header.jsp" %>

<link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/maum_landing.css?ver=<%=fmt.format(lastModifiedStyle)%>">
<!-- AMR 최상단 배너
  #wrap에 .top_banner class가 적용되면 최상단 배너가 보여집니다.
-->
<div id="wrap" class="maumUI transform">
    <%@ include file="../common/header.jsp" %>
    <!-- container -->
    <!-- AMR #container inline style
      '문의하기' 페이지는 #container에 inline으로 style background가 추가되어야 합니다.
    -->
    <div id="container" style="background: radial-gradient(circle at 101% 0%, rgba(28, 217, 255, 0.8), #0bd6ff, #2ddbff 6%, #8190ff 51%, #8190ff 60%, #005299 110%)">
        <div class="stn inquiry">
            <div class="content">
                <div class="content_tit">
                    <p class="tit">문의하기</p>
                    <p class="desc">서비스 의견 및 인공지능 도입에 대해<br>
                        궁금한 내용을 문의 주시면 자세히 안내해 드리겠습니다.</p>
                </div>

                <div class="inquiry_box">
                    <div class="inner">
                        <div class="dual">
                            <div class="my_info">
                                <label for="name">이름</label>
                                <input type="text" id="name" placeholder="이름(필수)" autocomplete="off">
                            </div>
                            <div class="my_info">
                                <label for="phone">연락처</label>
                                <input type="tel" pattern="[0-9]{3}-[0-9]{4}-[0-9]{4}" id="phone" placeholder="연락처(필수)" autocomplete="off">
                            </div>
                        </div>
                        <div class="dual">
                            <div class="my_info email_info">
                                <label for="mail">이메일</label>
                                <input type="email" id="mail" placeholder="이메일(필수)" autocomplete="off">
                            </div>
                            <div class="my_info">
                                <label for="company">회사명</label>
                                <input type="text" id="company" placeholder="회사명(필수)" autocomplete="off">
                            </div>
                        </div>
                        <div class="my_info my_txt">
                            <label for="txt">문의</label>
                            <textarea id="txt" placeholder="문의 내용을 입력해 주세요.(필수)"></textarea>
                        </div>
                        <div class="submit_agree">
                            <div class="input_box">
                                <input type="checkbox" id="personal_info">
                                <label for="personal_info">[필수]개인정보 수집 동의</label>
                            </div>
                            <div class="agree_contents">
                                <p>수집하는 개인정보 항목: 이름, 회사명, 연락처, 이메일 주소 작성해주시는 개인 정보는 문의 접수 및 고객 불만 해결을 위해 1년간 보관됩니다. 본 동의를 거부할 수 있으나, 미동의 시 문의 접수가 불가능합니다.</p>
                            </div>
                        </div>
                        <div class="btn_box">
                            <button class="btn_send btn_primary large" id="sendMailToHello_inquiry">문의하기</button>
                        </div>
                    </div>
                    <div class="contact">
                        <a href="tel:+8216613222">
                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/ico_headset_line.svg" alt="전화 걸기">
                            <span>1661-3222</span>
                        </a>
                        <a href="mailto:hello@mindslab.ai">
                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/landing/ico_email_line.svg" alt="메일 보내기">
                            <span>hello@mindslab.ai</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- //container -->

    <%@ include file="../common/footerLanding.jsp" %>

    <!-- (수정) 기존 헤더.jsp에 적용되어 있는 script -->
    <script type="text/javascript">
        //aside menu
        var clicked = false;
        var asideWidth = '100%'; //AMR asideWidth는 100%가 되어야 합니다
        $('.aside').show();

        $('a.btn_maum_ham').click(function(){
            if (!clicked) {
                $(this).addClass('active');
                $('.aside').animate({
                    width : asideWidth
                },{duration:200,queue:false});
                $('.btn_goTop').hide();

                $('.bg_aside').animate({
                    opacity : 0.7,
                    display : 'block'
                },{duration:200,queue:false});

                $('body').css({
                    overflow : 'hidden'
                });

                clicked=true;
            } else {
                $(this).removeClass('active');
                $('.aside').animate({
                    width: '0'
                },{duration:200,queue:false});
                $('.btn_goTop').show();

                $('.bg_aside').animate({
                    opacity : 0,
                    display : 'none'
                },{duration:150,queue:false});

                $('body').css({
                    overflow : ''
                });

                clicked=false;
            }
        });

        $('.bg_aside').on('click',function(){
            $('a.btn_maum_ham').removeClass('active');
            $('.aside').animate({
                width: '0'
            },{duration:200,queue:false});
            $('.btn_goTop').show();

            $('.bg_aside').animate({
                opacity : 0,
                display : 'none'
            },{duration:150,queue:false});

            $('body').css({
                overflow : ''
            });
            clicked=false;
        });
    </script>
    <!-- //(수정) 기존 헤더.jsp에 적용되어 있는 script -->

    <!-- AMR 공통으로 적용되는 script -->
    <script type="text/javascript">
        // AMR fixed된 header에 가로스크롤을 적용하기 위한 코드
        $(window).scroll(function () {
            $('.maumUI.transform .maum_sta').css('left', 0 - $(this).scrollLeft());
        });

        // AMR #aside_btns 페이지 위로 버튼 display 이벤트
        $(window).scroll(function(){
            var scrollLocate = $(window).scrollTop();
            if (scrollLocate > 50) {
                $('#header').addClass('transform');
                $('#aside_btns .page_top').css('display', 'block');
            }
            if (scrollLocate < 50) {
                $('#header').removeClass('transform');
                $('#aside_btns .page_top').css('display', 'none');
            }
        });

        // AMR 최상단 배너 닫기 버튼 클릭 이벤트
        $('.banner_close').on('click', function(){
            var $banner = $(this).parents('.top_banner_area');
            $banner.remove();
            $('#wrap').removeClass('top_banner');
        });

        // AMR page top 버튼 클릭 이벤트
        $('#aside_btns .page_top').on('click', function(){
            $('body').animate({
                scrollTop: 0
            }, 500);
        });
    </script>
    <!-- AMR //공통으로 적용되는 script -->
</div>


<script type="text/javascript">
    function validateEmail(sEmail) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            return false;
        }
    }

    $(document).ready(function() {
        $('#sendMailToHello_inquiry').on('click', function () {

            if (validateEmail($('#mail').val()) == false) {
                alert("이메일 형식을 확인해 주세요!");
            } else if ($('#name').val() === '' || $('#phone').val() === '' || $('#mail').val() === '' || $('#txt').val() === '' || $('#company').val() === '') {

                alert("내용을 모두 입력해야 이메일을 전달할 수 있어요!");

            } else if ($('#personal_info').prop('checked') == false) {

                alert("개인정보 수집 동의란에 체크해주셔야 문의하기가 완료됩니다.");

            } else {


                let title = "[maum.ai] " + $("#name").val() + "님의 문의 사항입니다.";
                let msg = "이름 : " + $("#name").val() + "<br>이메일 : " + $('#mail').val() + "<br>연락처 : " + $("#phone").val() + "<br>회사명 : " + $("#company").val() + "<br>문의 내용 : " + $("#txt").val()
                    + "<br><br><br>* " + " '문의하기' 페이지를 통해 발송낸 메일입니다.";

                let formData = new FormData();

                formData.append('fromaddr', $('#mail').val());
                formData.append('toaddr', 'hello@mindslab.ai');
                formData.append('subject', title);
                formData.append('message', msg);
                formData.append($("#key").val(), $("#value").val());

                $.ajax({
                    type: 'POST',
                    async: true,
                    url: '/support/sendContactMail',
                    dataType: 'text',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (obj) {

                        alert("문의 접수가 완료 되었습니다.");

                        $("#name").val('');
                        $("#office").val('');
                        $("#phone").val('');
                        $("#company").val('');
                        $("#txt").val('');
                        $("#mail").val('');
                        var placeholderLabel = $('.inquiry_box input, .inquiry_box textarea');
                        placeholderLabel.siblings('label').show();

                        $('#mail_success').fadeIn();

                    },
                    error: function (xhr, status, error) {
                        console.log("error");
                        alert("Contact Us 메일발송 요청 실패하였습니다.");
                        window.location.href = "/";
                    }
                });
            }
        });
    });

</script>