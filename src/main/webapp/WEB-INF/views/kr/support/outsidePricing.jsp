<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="../common/common_header.jsp" %>


<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
	<div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<!-- .price_detail -->
<div class="price_detail api_month">
	<div class="lyr_bg"></div>
	<!-- .lyrBox -->
	<div class="lyrBox"  >
		<!-- .lyr_top -->
		<div class="lyr_top">
			<h3>API 월 사용량 제한</h3>
			<em class="fas fa-times close"></em>
		</div>
		<!-- //.lyr_top -->

		<!-- .lyr_mid -->
		<div class="lyr_mid">
			<table class="tbl_view">
				<colgroup>
					<col><col><col><col>
				</colgroup>
				<thead>
				<tr>
					<th scope="col" style="width:66px;padding:0 0 0 13px;">카테고리</th>
					<th scope="col">Engine API</th>
					<th scope="col">유형</th>
					<th scope="col">월 사용량 제한</th>
				</tr>
				</thead>
				<tbody>
					<tr>
						<th scope="rowgroup" rowspan="7">음성</th>
						<td rowspan="2" class="col_1">TTS</td>
						<td class="col_2">한국어</td>
						<td>4,000,000자</td>
					</tr>
					<tr>
						<td>영어</td>
						<td>6,666,666자</td>
					</tr>
					<tr>
						<td>STT</td>
						<td>한국어/영어</td>
						<td>5,000분</td>
					</tr>
					<tr>
						<td>음성정제</td>
						<td>-</td>
						<td>50,000분</td>
					</tr>
					<tr>
						<td>화자분리</td>
						<td>-</td>
						<td>5,000분</td>
					</tr>
					<tr>
						<td>Voice Filter</td>
						<td>-</td>
						<td>25,000분</td>
					</tr>
					<tr>
						<td>Voice Recognition</td>
						<td>-</td>
						<td>25,000분</td>
					</tr>
				</tbody>
			</table>

			<table class="tbl_view">
				<colgroup>
					<col><col><col><col>
				</colgroup>
				<tbody>
					<tr>
						<th scope="rowgroup" rowspan="10" style="width:66px;padding:0 0 0 13px;">시각</th>
						<td class="col_1">텍스트제거</td>
						<td class="col_2">-</td>
						<td>80,000건</td>
					</tr>
					<tr>
						<td>TTI</td>
						<td>AI스타일링</td>
						<td>80,000건</td>
					</tr>
					<tr>
						<td>문서 이미지 분석</td>
						<td>문서인식</td>
						<td>80,000건</td>
					</tr>
					<tr>
						<td>도로상의 객체 인식</td>
						<td>객체인식</td>
						<td>80,000건</td>
					</tr>
					<tr>
						<td>Super Resolution</td>
						<td>화질 개선</td>
						<td>80,000건</td>
					</tr>
					<tr>
						<td>Face Recognition</td>
						<td>얼굴인식</td>
						<td>80,000건</td>
					</tr>
					<tr>
						<td>인물 포즈 인식</td>
						<td>-</td>
						<td>80,000건</td>
					</tr>
					<tr>
						<td>얼굴추적</td>
						<td>-</td>
						<td>50,000초</td>
					</tr>
					<tr>
						<td>이상행동 감지</td>
						<td>역주행</td>
						<td>50,000초</td>
					</tr>
					<tr>
						<td>이미지 자막인식</td>
						<td>한국어</td>
						<td>80,000건</td>
					</tr>
				</tbody>
			</table>

			<table class="tbl_view">
				<colgroup>
					<col><col><col><col>
				</colgroup>
				<tbody>
					<tr>
						<th scope="rowgroup" rowspan="6" style="width:66px;padding:0 0 0 13px;">언어</th>
						<td class="col_1">자연어 이해</td>
						<td class="col_2">한국어/영어</td>
						<td>20,000단위</td>
					</tr>
					<tr>
						<td>AI 독해(MRC)</td>
						<td>한국어/영어</td>
						<td>200,000건</td>
					</tr>
					<tr>
						<td>텍스트 분류(XDC)</td>
						<td>한국어/영어</td>
						<td>40,000건</td>
					</tr>
					<tr>
						<td>문장생성</td>
						<td>한국어/영어</td>
						<td>100,000건</td>
					</tr>
					<tr>
						<td>패턴분류 (HMD)</td>
						<td>한국어/영어</td>
						<td>400,000건</td>
					</tr>
					<tr>
						<td>의도분류 (ITF)</td>
						<td>한국어</td>
						<td>400,000건</td>
					</tr>
				</tbody>
			</table>

			<table class="tbl_view">
				<colgroup>
					<col><col><col><col>
				</colgroup>
				<tbody>
					<tr>
						<th scope="row" style="width:66px;padding:0 0 0 13px;">분석</th>
						<td class="col_1">데이터분석</td>
						<td class="col_2">-</td>
						<td>200,000건</td>
					</tr>
				</tbody>
			</table>
			<table class="tbl_view">
				<colgroup>
					<col><col><col><col>
				</colgroup>
				<tbody>
					<tr>
						<th scope="rowgroup" rowspan="3" style="width:66px;padding:0 0 0 13px;">챗봇</th>
						<td class="col_1">NQA봇</td>
						<td class="col_2">한국어</td>
						<td>40,000건</td>
					</tr>
					<tr>
						<td rowspan="2">바로쓰는 챗봇</td>
						<td>날씨봇(한국어)</td>
						<td>20,000건</td>
					</tr>
					<tr>
						<td>위키봇(한국어)</td>
						<td>20,000건</td>
					</tr>
				</tbody>
			</table>
			<table class="tbl_view">
				<colgroup>
					<col><col><col><col>
				</colgroup>
				<tbody>
					<tr>
						<th scope="rowgroup" rowspan="3" style="width:66px;padding:0 0 0 13px;">영어교육</th>
						<td class="col_1">영어교육용 STT</td>
						<td class="col_2">영어</td>
						<td>4,000분</td>
					</tr>
					<tr>
						<td>발음 평가</td>
						<td>영어</td>
						<td>20,000건</td>
					</tr>
					<tr>
						<td>파닉스 평가</td>
						<td>영어</td>
						<td>20,000건</td>
					</tr>
				</tbody>
			</table>
		</div>
		<!-- //.lyr_mid -->
	</div>
	<!-- //.lyrBox -->
</div>
<!-- //.price_detail -->

<!-- .price_detail -->
<div class="price_detail minutes_month">
	<div class="lyr_bg"></div>
	<!-- .lyrBox -->
	<div class="lyrBox" >
		<div class="lyr_top">
			<h3>maum 회의록 가격정책</h3>
			<em class="fas fa-times close"></em>
		</div>
		<div class="lyr_mid">
			<div class="mid_area">
				<div class="minibox month_user">
					<h5>maum.ai <strong>첫 달 무료</strong></h5>
					<div class="dlBox">
						<dl>
							<dt>가격</dt>
							<dd>월 99,000원/ 계정당</dd>
						</dl>
						<dl>
							<dt>계정</dt>
							<dd>단일 계정</dd>
						</dl>
						<dl>
							<dt>사용량 제한</dt>
							<dd>100 시간 / 월</dd>
						</dl>
						<dl>
							<dt>보관기간</dt>
							<dd>서비스 해지 시까지</dd>
						</dl>
					</div>
				</div>
				<h4>B2B Enterprise</h4>
				<div class="minibox enterprise">
					<h5>On-Device</h5>
					<p>이동형 회의록 시스템<br>
						4,990만원 / 10채널<br>
						<span>(노트북, 마이크 포함)</span>
					</p>
				</div>
				<div class="minibox enterprise">
					<h5>On-Site</h5>
					<p>협의<br>
						고객용 별도 모델 생성<br>
						(인식률 85% 보장)
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- //.lyrBox -->
</div>
<!-- //.price_detail -->
<!-- .lyr_info -->
<div class="price_detail price_contact">
	<div class="lyr_bg"></div>
	<!-- .lyrBox -->
	<div class="lyrBox" >
		<div class="lyr_top">
			<h3>문의하기</h3>
			<em class="fas fa-times close"></em>
		</div>
		<!-- .lyr_bd -->
		<div class="lyr_mid">
			<ul>
				<li class="half_box">
					<label for="info_name">*이름</label>
					<input id="info_name" type="text" value="" placeholder="이름">
				</li>
				<li class="half_box">
					<label for="info_number">*전화번호</label>
					<input id="info_number" type="number" value="" placeholder="전화번호">
				</li>
				<li>
					<label for="info_email">*이메일</label>
					<input id="info_email" type="email" value="" placeholder="이메일">
				</li>
				<li>
					<label for="info_memo">문의내용</label>
					<textarea id="info_memo" type="text" placeholder="문의내용"></textarea>
				</li>
			</ul>
			<button type="button" class="btn_blue" id="">보내기</button>
		</div>
		<!-- //.lyr_bd -->
	</div>
	<!-- //.lyrBox -->
</div>
<!-- //.lyr_info -->



<div class="pop_confirm" id="mail_success">
	<div class="pop_bg"></div>
	<!-- .popWrap -->
	<div class="popWrap">
		<!-- .pop_bd -->
		<div class="pop_bd">

			<!--내용 부분-->
			<p>문의내용이 접수되었습니다.<br>
				담당자가 빠른 시일 내에<br>
				답변 드리도록 하겠습니다.</p>
		</div>
		<!-- //.pop_bd -->
		<!--창닫기 버튼 -->
		<div class="btn">
			<button class="btn_close" >확인</button>
		</div>
		<!--창닫기 버튼 -->
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_confirm -->

<!-- wrap -->
<div id="wrap" class="pricingWrap maumUI" >
	<%@ include file="../common/header.jsp" %>
		<!-- .priceBox -->
		<div class="priceBox">
			<h1>가격 정책</h1>
			<span>쉽게 쓰는 마음 AI로 성공적인 비즈니스를 만드세요. <em>가입 시 첫 1달 무료!</em> </span>
			<!-- .price_stn -->
			<div class="price_stn">
				<div class="cardBox business">
					<h4>Business</h4>
					<div class="inner_area">
						<p> 99,000<small>원 / 월 </small> <strong>첫 1달 무료</strong></p>
						<div>
							<span>클라우드 API<br>AI Builder</span>
							<span>maum 회의록<br>FAST 대화형 AI</span>
						</div>
						<ul>
							<li><em class="fas fa-check"></em>30+ AI 알고리즘 활용법 안내
								<small>(음성, 시각, 언어, 분석, 대화 분야)</small>
							</li>
							<li><em class="fas fa-check"></em>REST API Id, Key 및 문서 제공</li>
							<li><em class="fas fa-check"></em>월별 사용량 측정</li>
							<li><em class="fas fa-check"></em>결과 파일 내려받기</li>
							<li><em class="fas fa-check"></em>파일 보관 서비스</li>
							<li><em class="fas fa-check"></em>기술 지원</li>
						</ul>
						<button type="button" id="api_month">API 월 사용량 제한 <em class="fas fa-angle-right"></em></button>
						<button type="button" id="minutes_month" class="minutes" >maum 회의록 가격정책 <em class="fas fa-angle-right"></em></button>
						<!-- 로그인 X인 경우 -->
						<sec:authorize access="isAnonymous()">
							<a href="javascript:login()" title="Business 시작하기">시작하기</a>
						</sec:authorize>
						<!-- 로그인 O인 경우 -->
						<sec:authorize access="isAuthenticated()">
							<a href="/?lang=ko/service_position" title="Business 시작하기">시작하기</a>
						</sec:authorize>
					</div>
				</div>
				<div class="cardBox enterprise">
					<h4>Enterprise</h4>
					<div class="inner_area">
						<p>기업 커스텀</p>
						<div>
							<span>전문 AI 컨설턴트 밀착 상담 및 지원</span>
						</div>
						<ul>
							<li><em class="fas fa-check"></em>30+ AI 알고리즘 활용법 안내
								<small>(음성, 시각, 언어, 분석, 대화 분야)</small>
							</li>
							<li><em class="fas fa-check"></em>REST API Id, Key 및 문서 제공</li>
							<li><em class="fas fa-check"></em>월별 사용량 측정</li>
							<li><em class="fas fa-check"></em>결과 파일 내려받기</li>
							<li><em class="fas fa-check"></em>파일 보관 서비스</li>
							<li><em class="fas fa-check"></em>기술 지원</li>
						</ul>
						<ul class="list">
							<li><em class="fas fa-check"></em>AI 보이스 폰트 제작</li>
							<li><em class="fas fa-check"></em>학습 데이터 판매 지원</li>
							<li><em class="fas fa-check"></em>맞춤형 모델 학습 지원</li>
							<li><em class="fas fa-check"></em>B2B형 개별 사이트 구축</li>
							<li><em class="fas fa-check"></em>다수 계정 연결</li>
						</ul>
						<a href="/inquiry" class="contact_btn" title="Enterprise 문의하기">문의하기</a>
					</div>
				</div>
			</div>
			<!-- //.price_stn -->
		</div>
		<!-- //.priceBox  -->

		<!-- #footer -->
	<%@ include file="../common/footerLanding.jsp" %>


</div>
<!-- //wrap -->

<%--<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/pricing_page.js"></script>--%>
<script type="text/javascript">


	<%--function login(){--%>
	<%--	location.href = "${google_url}" + "?targetUrl=/?lang=ko";--%>
	<%--}--%>

	$('#api_month').on('click', function(){
		$('.api_month').fadeIn();
		// $('body').css({
		// 	'overflow': 'hidden',
		// });
	});
	$('#minutes_month').on('click', function(){
		$('.minutes_month').fadeIn();
		// $('body').css({
		// 	'overflow': 'hidden',
		// });
	});


	$('.close, .btn_close').on('click',function(){
		$('.price_detail').fadeOut();
		$('.pop_confirm').fadeOut();
		$('body').css({
			'overflow': '',
		});
	});


</script>

	


