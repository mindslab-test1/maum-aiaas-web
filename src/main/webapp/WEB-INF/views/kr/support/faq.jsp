<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<body>

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
	<div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->
		<!-- .contents -->
		<div class="contents"> 
			<!--content-->
            <div class="content">
				<h1>FAQ</h1>				
				<!-- .notice -->
				<!-- 검색 -->
				<div class="searchType_wrap02">
					<a class="txtWrite" href="/support/faq_write">글쓰기</a>
					<input type="text" class="input_textType" title="" placeholder="검색어를 입력해 주세요.">
					<button type="button" class="btn_search">검색</button>
				</div>
				<ul class="qa_lst">
					<li class="tablinks" onclick="openCity(event, 'fqaBox')" id="defaultOpen">Getting started</li>
					<li class="tablinks" onclick="openCity(event, 'fqaBox2')">Pricing &amp; plans</li>
					<li class="tablinks" onclick="openCity(event, 'fqaBox3')">Usage Guide</li>
					<li class="tablinks" onclick="openCity(event, 'fqaBox4')">Sales question</li>
				</ul>
				
				<div id="fqaBox" class="tabcontent faqBox">	
					<div class="lst_stn">
						<button class="noticetit">Puchasing <strong>process</strong></button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p><strong>Lorem</strong> ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>	
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
					</div>
				</div>
				<!-- //.notice -->
				<div id="fqaBox2" class="tabcontent faqBox">	
					<div class="lst_stn">
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>	
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
					</div>
				</div>
				<!-- //.notice -->
				<div id="fqaBox3" class="tabcontent faqBox">	
					<div class="lst_stn">
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>	
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
					</div>
				</div>
				<!-- //.notice -->
				<div id="fqaBox4" class="tabcontent faqBox">						
					<div class="lst_stn">
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>	
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
					</div>
				</div>
				<!-- //.notice -->
				
			</div>	
			<!--content-->

		</div>
		<!-- //.contents -->


<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/bootstrap-datepicker.js"></script>	
<script type="text/javascript">   
$(window).load(function() {    
	$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() { 
		$(this).remove(); 
	});
}); 
</script>
<script type="text/javascript">

jQuery.event.add(window,"load",function(){
	$(document).ready(function (){		
		
		$('.btn_search').on('click', function() {
			$('.qa_lst').fadeOut(300);
			
		});
	});	
});	

	
//notice 리스트	
var coll = document.getElementsByClassName("noticetit");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var noticetxt = this.nextElementSibling;
    if (noticetxt.style.maxHeight){
      noticetxt.style.maxHeight = null;
    } else {
      noticetxt.style.maxHeight = noticetxt.scrollHeight + "px";
    } 
  });
}
	
//fqa 리스트	
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}	
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();	
	
	
</script>
	
</body>
</html>


