<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

		<div class="contents"> 
			<!--content-->
            <div class="content">
				<h1>Contact us</h1>
				<!-- .api_contact -->
				<div id="api_contact" class="api_contact">
					<ul class="contact_lst">
						<li class="btn_tel"><a href="tel:+82316254340">1661-3222</a></li>
						<li class="btn_mail"><a href="mailto:support@mindslab.ai">support@mindslab.ai</a></li>
					</ul>
					<!-- .contactBox -->
					<div class="contactBox">
<%--								<em class="fas fa-envelope-open" style="font-size:30px; text-align: center;display: block; margin: 0 0 20px 0; color: #2575f9;"></em>--%>
						<p>문의 사항은 아래 작성하여서 보내주세요. 저희 담당자가 빠른 시일 내에 답변 드리도록 하겠습니다.</p>
						<!-- .rightBox -->
						<div class="formBox">
							<form>
								<fieldset>
									<legend>문의하기</legend>
									<div class="contact">
										<div class="txtfield inline_field">
											<label for="fm_name">이름</label>
											<input type="text" name="fm_name" id="fm_name" class="input_txt">
										</div>
										<div class="txtfield inline_field">
											<label for="fm_company">회사명</label>
											<input type="text" name="fm_company" id="fm_company" class="input_txt">
										</div>
										<div class="txtfield inline_field">
											<label for="fm_email">이메일</label>
											<input type="email" name="fm_email" id="fm_email"  class="input_txt">
										</div>
										<div class="txtfield">
											<label for="fm_tel">전화번호</label>
											<input type="number" name="fm_tel" id="fm_tel"  class="input_txt" >
										</div>
<!-- 												<div class="txtfield"> -->
<!-- 													<label>Category</label> -->
<!-- 													<div class="selectbox input_txt">  -->
<!-- 														<label for="Category">- Category -</label>  -->
<%-- 														<select id="Category">  --%>
<!-- 															<option selected>- Category -</option>  -->
<!-- 															 <option value="1">1</option>  -->
<!-- 															<option value="2">2</option>                           -->
<%-- 														</select>  --%>
<!-- 													</div>													 -->
<!-- 												</div> -->
										<div class="txtfield">
											<label for="fm_txt">내용</label>
											<textarea name="fm_txt" id="fm_txt" class="textArea" rows="3"></textarea>
										</div>
<!-- 												<div class="txtfield file_box"> -->
<!-- 													<label>Attach files</label> -->
<!-- 													<div class="srchbox">						 -->
<!-- 														<input class="upload-name" value=""  disabled="disabled"> -->
<!-- 														<label for="ex_filename" class="file_label">Upload files</label>  -->
<!-- 														<input type="file" id="ex_filename" class="upload-hidden">  -->
<!-- 													</div> -->
<!-- 												</div> -->
										<ul class="btnBox">

											<li>
											<!-- <button type="button" id="send" class="btn_clor">보내기</button> -->
											<a class="btn_clor" onclick="sendMailApi();">보내기</a>
											</li>
										</ul>
									</div>
								</fieldset>
							</form>
						</div>
						<!-- //.rightBox -->
					</div>
					<!-- //.contactBox -->
					<div class="mail_result">
						<p>메일이 전송되었습니다.<br>
							빠른시일내에 답변 드리겠습니다. 감사합니다.</p>
						<ul class="btnBox">
							<li>
								<a class="btn_clor" id="btn_reset">확인</a>
							</li>
						</ul>
					</div>

				</div>
				<!-- //.api_contact -->
		
			</div>	
			<!--content-->
		</div>
		

		
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/bootstrap-datepicker.js"></script>	


<script type="text/javascript">

	function validateEmail(sEmail) {
		var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		if (filter.test(sEmail)) {
			return true;
		}
		else {
			return false;
		}
	}


	/** Contact Us 메일발송 요청 */
function sendMailApi(){

		var name = $('#fm_name').val();
		var company = $('#fm_company').val();
		var email = $('#fm_email').val();
		var phone = $('#fm_tel').val();
		var text = "Cloud API(KR)에서 보낸 메일입니다."

		if(name !== "" && company !== "" && phone !== "" && validateEmail( email ) == true){
			var yn = confirm("Contact Us 메일발송 요청 하겠습니까?");
			var contents = $("#fm_txt").val() + "<br><br><br>* Cloud API(KR)에서 보낸 메일입니다.";
			if(yn){
				$.ajax({
					type 	: 'post',
					url  	: '/support/insertSupportContact',
					data: {
						'name' : $("#fm_name").val(),
						'company' : $("#fm_company").val(),
						'mailAddr' : $("#fm_email").val(),
						'phone' : $("#fm_tel").val(),
						'content' : contents,
						'${_csrf.parameterName}' : '${_csrf.token}'
					},
					dataType: "JSON",
					cache   : false,
					async   : true,
					type	: "POST",
					success : function(obj) {
						sendMailApiCallback(obj);
					},
					error 	: function(xhr, status, error) {}
				});
			}
		}else{

			var alertMsg = "입력 사항을 모두 알맞게 입력해 주세요.";
			if(validateEmail( email ) == false){
				alert( "이메일 형식을 확인해 주세요." );
			}else{
				alert( alertMsg );
			}
		}
}	
	
	function sendMailApiCallback(obj){
		
		if(obj != null){		
			
			var state = obj.state;
			
			if(state == "SUCCESS"){			
				// alert("Contact Us 메일발송 요청하였습니다.");
				$('.contactBox').hide();
				$('.mail_result').fadeIn();
			} else {				
				alert("Contact Us 메일발송 요청 실패하였습니다.");	
				return;
			}
		}
	}
jQuery.event.add(window,"load",function(){
	$(document).ready(function (){

		$('#btn_reset').on('click', function(){
			$('.mail_result').hide();
			$('.contactBox').fadeIn();
			$('.txtfield input').val('');
			$('.txtfield textarea').val('');
		});

	});
});
</script>