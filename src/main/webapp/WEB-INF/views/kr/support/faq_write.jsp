<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
	<div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

		
		<!-- .contents -->
		<div class="contents"> 
			<!--content-->
            <div class="content">
				<h1>FAQ</h1>
				<!-- .notice -->
				<div id="noticeBox" class="writebox">	
					<div class="notice_write">					
						<div class="wide_box">
							<label for="title">제목</label>
							<input type="text" id="title" class="ipt_txt" placeholder="">   
						</div>
						<div class="short_box">
							<label for="data">유형</label>
							<div class="selectbox"> 
								<label for="select">선택</label> 
								<select id="select"> 
									<option selected>선택</option> 
									 <option value="1">1</option> 
									<option value="2">2</option>                          
								</select> 
							</div>
						</div>
						<div class="short_box">
							<label for="data">오픈일자</label>
							<input type="text" id="" class="ipt_txt ipt_short" placeholder="">  
						</div>
						<div class="short_box">
							<label for="data">공개여부</label>
							<div class="selectbox"> 
								<label for="">공개</label> 
								<select id=""> 
									<option selected>공개</option> 
									 <option value="1">1</option> 
									<option value="2">2</option>                          
								</select> 
							</div>
						</div>	
					</div>
					
					<div class="writeArea">					
					</div>
					<div class="btn_area">
						<a class="btn gray">취소</a>
						<a class="btn blue">저장</a>
					</div>
					
					
				</div>
				<!-- //.notice -->		
			</div>	
			<!--content-->

		</div>
		<!-- //.contents -->

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/bootstrap-datepicker.js"></script>	
<script type="text/javascript">   
$(window).load(function() {    
	$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() { 
		$(this).remove(); 
	});
}); 
</script>
<script type="text/javascript">

jQuery.event.add(window,"load",function(){
	$(document).ready(function (){

	});	
});	

	
//notice 리스트	
var coll = document.getElementsByClassName("noticetit");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var noticetxt = this.nextElementSibling;
    if (noticetxt.style.maxHeight){
      noticetxt.style.maxHeight = null;
    } else {
      noticetxt.style.maxHeight = noticetxt.scrollHeight + "px";
    } 
  });
}

	
// select design 
var selectTarget = $('.selectbox select'); 

selectTarget.change(function(){ 

var select_name = $(this).children('option:selected').text(); 
$(this).siblings('label').text(select_name); 
}); 

// select	
$('.selectbox select').on('focus',function(){
	$(this).parent().addClass('active');
});
$('.selectbox select').on('focusout',function(){
	$(this).parent().removeClass('active');
});	
		
</script>
	