<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/hospitalReceipt.css">

<%@ include file="../common/common_header.jsp" %>


<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
	<div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->


<!-- wrap -->
<div id="wrap" class="maumUI" >
	<%@ include file="../common/header.jsp" %>
	<div id="container">

		<!-- .pop_simple -->
		<div class="pop_simple">
			<div class="pop_bg"></div>
			<!-- .popWrap -->
			<div class="popWrap pop_sr_noti">
				<button class="pop_close" type="button">닫기</button>
				<!-- .pop_bd -->
				<div class="pop_bd">
					<em class="fas fa-sad-cry"></em>
					<h5>이미지 업로드 실패</h5>
					<p>
						이미지 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
						업로드 되지 않았습니다.
					</p>
					<span>
						* 지원가능 파일 확장자: .jpg, .png<br>
						* 이미지 용량 2MB 이하만 가능합니다.<br>
					</span>
				</div>
				<!-- //.pop_bd -->
				<div class="btn">
					<a class="">확인</a>
				</div>
			</div>
			<!-- //.popWrap -->
		</div>
		<!-- //.pop_simple -->


		<!-- 2 .pop_common -->
		<div class="pop_common samplebill_pop" id="hospital_pop">
			<!-- .pop_bg 팝업 바탕 어두운 색 -->
			<div class="pop_bg"></div>
			<!-- //.pop_bg 팝업 바탕 어두운 색 -->

			<!-- .popWrap -->
			<div class="popWrap">
				<!-- .pop_hd -->
				<div class="pop_hd">
					<h3 id="hospital_origin"><em class="far fa-file-image"></em>&nbsp; 진료비 영수증 샘플</h3>
					<em class="fas fa-times pop_sample_close"></em>
				</div>
				<!-- //.pop_hd -->
				<!-- .pop_bd -->
				<div class="pop_bd">
					<img id="samplem_edical" class="bill_img" src="${pageContext.request.contextPath}/aiaas/common/images/hospital-sample.jpg" alt="진료비 영수증 샘플" />
				</div>
				<!-- //.pop_bd -->
			</div>
			<!-- //.popWrap -->
		</div>
		<!-- //.pop_common -->


		<!-- .contents -->
		<div class="contents">
			<div class="content api_content">
				<h1 class="api_tit">문서 이미지 분석</h1>
				<ul class="menu_lst vision_lst">
					<li class="tablinks" onclick="openTap(event, 'recogdemo')" id="defaultOpen"><button type="button">엔진</button></li>
					<li class="tablinks" onclick="openTap(event, 'recogmenu')"><button type="button">매뉴얼</button></li>
				</ul>
				<div class="demobox" id="recogdemo">
					<p><span>병원 진료비 영수증 이미지 인식 API</span> <small>(Hospital Receipt Recognition)</small></p>
					<span class="sub">병원 진료비 영수증의 이미지를 활용하여 정보를 분석해줍니다.</span>
					<!--이미지인식box-->
					<div class="demo_layout pattern_box">
						<div class="pattern_1" >
							<div class="fl_box" id="opacity">
								<p><em class="far fa-file-image"></em><strong>샘플 파일</strong>로 테스트 하기</p>
								<div class="sample_box">
									<div class="sample_3">
										<div class="radio">
											<input type="radio" id="sample3" name="sample" value="진료비영수증" checked>
											<label for="sample3"  class="">
												<em class="img_area bill_area">
													<img id="sampleImg_3" class="bill_img" src="${pageContext.request.contextPath}/aiaas/common/images/hospital-sample.jpg" alt="진료비 영수증 샘플 이미지" />
													<strong class="hospital_detail"><em class="fas fa-search"></em> 자세히 보기</strong>
												</em>
												<span>병원<br>진료비<br>계산서·<br>영수증
                                        </span>
											</label>
											<span class="bill_desc">이미지 클릭시, 자세히 볼 수 있습니다.</span>
										</div>
									</div>
								</div>
							</div>
							<div class="fr_box">
								<p><em class="far fa-file-image"></em><strong>내 파일</strong>로 해보기</p>
								<div class="uplode_box">
									<div class="btn" id="uploadFile">
										<em class="fas fa-times hidden close"></em>
										<em class="far fa-file-image hidden"></em>
										<label for="demoFile" class="demolabel">이미지 업로드</label>
										<input type="file" id="demoFile" class="demoFile" accept=".jpg, .png, .bmp">
									</div>
									<p class="img_desc"><em class="fas fa-exclamation"></em> 이미지 업로드 시 유의사항</p>
									<div>
										<span><em>병원 진료비 계산서 ·영수증 </em> : 상급 종합병원에서 보편적으로 쓰이는 영수증 형식 권고</span>
									</div>

									<ul>
										<li>* 지원가능 파일 확장자: .jpg, .png</li>
										<li>* 이미지 용량 2MB 이하만 가능합니다.</li>
									</ul>
								</div>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_start editImgBtn" id="editImgBtn">분석하기</button>
							</div>
						</div>

						<!--loading_area-->
						<div class="loading_area">
							<p><em class="far fa-file-image"></em>이미지 인식 및 분석 처리중</p>
							<div class="loading_box ">
								<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#f7778a" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

								<p>잠시만(1분 내외) 기다려 주세요.</p>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_back1" id="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
							</div>
						</div>
						<!--loading_area-->
						<!--pattern_3-->
						<div class="pattern_3">
							<div class="result_hospital">
								<p>인식 결과</p>
								<div class="table_hospital">
									<div class="text_area">
										<textarea id="result_textArea"  rows="7" placeholder=" "></textarea>
									</div>
								</div>
								<a class="img_pop" href="#" alt="원본 이미지"><em class="far fa-file-image"></em> 원본 이미지 확인</a>
<%--								<a id="save" onclick="downloadResultImg();" class="dwn_link" ><em class="far fa-arrow-alt-circle-down"></em> 결과 파일(.csv) 다운로드</a>--%>
								<div class="btn_area">
									<button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
								</div>

							</div>


							<!--						인식하지 못했을 때 style="display: block;" 해두세용-->
							<div class="resultBox_fail" style="display: none;">
								<div class="no_recog">인식할 수 없는 이미지 입니다.</div>
								<div class="btn_area">
									<button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
								</div>
							</div>
						</div>
						<!--pattern_3-->
					</div>

				</div>
				<!-- .demobox -->

				<!--.recogmenu-->
				<div class="demobox vision_menu" id="recogmenu">
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API 공통 가이드
							</div>
							<p class="sub_title">개발 환경 세팅</p>
							<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
							<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
						</div>
						<div class="guide_group">
							<div class="title">
								병원 진료비 영수증 이미지 인식 Open API<small></small>
							</div>
							<p class="sub_txt">병원 진료비 영수증 내 지정된 진료 항목과 진료 금액을 인식한 후 해당 정보를 제공합니다.</p>

							<span class="sub_title">준비사항</span>

							<p class="sub_txt">- Input: 병원 진료비 영수증 이미지 파일 </p>
							<ul>
								<li>확장자: .jpg, .png</li>
								<li>용량: 2MB 이하 </li>
							</ul>
							<span class="sub_title">
								 실행 가이드
							</span>
							<p class="sub_txt">① Request  </p>
							<ul>
								<li>Method : POST</li>
								<li>URL : https://smartx-api.maum.ai:10001/TableRecog</li>
							</ul>
							<p class="sub_txt">② Request 파라미터 설명 </p>
							<table>
								<tr>
									<th>키</th>
									<th>설명</th>
									<th>type</th>
								</tr>
								<tr>
									<td>imageNo</td>
									<td>이미지 ID</td>
									<td>integer</td>
								</tr>
								<tr>
									<td>imageFile</td>
									<td>이미지 원본 파일 (.jpg,.png)</td>
									<td>string</td>
								</tr>
								<tr>
									<td>recognitions</td>
									<td>인식 유형 설정</td>
									<td>dictionary</td>
								</tr>
							</table>
							<p class="sub_txt">③ Request 예제 </p>
							<div class="code_box">
								url --location --request POST 'https://smartx-api.maum.ai:10001/TableRecog' \<br>
								--header 'Content-Type: application/x-www-form-urlencoded' \<br>
								--form 'imageNo= 병원 진료비 영수증이미지 ID (e.g. 10000000010)' \<br>
								--form 'imageFile= 병원 진료비 영수증이미지 파일 (e.g. sample01.jpg)' \<br>
								--form 'recognitions= 인식 유형 설정 정보 (e.g. [{\"rcgnTypeCd\":\"RFRTABLE\",\"rcgnSq\":4,\"rcgnNm\":\"테이블\"}])'<br>
							</div>

							<p class="sub_txt">④ Response 예제 </p>

							<div class="code_box">
<pre>
Content-Disposition: form-data; name="imageNo"
10000000010

Content-Disposition: form-data; name="recognitions"
[
    {
        "rcgnSq": "4",
        "rcgnTypeCd": "RFRTABLE",
        "rcgnNm": "테이블",
        "headers": ["1", "2", "3", "4", "5"],
        "titles": [
            "식대",
            "진찰료",
            "입원료",
            "식대",
            "투약조제료행위료",
            "투약조제료약품비",
            "주사료행위료",
            "주사료약품비",
            "마취료",
            "처치수술료",
            "검사료",
            "영상진단료",
            "방사선치료료",
            "치료재료대",
            "초음파진단료",
            "검사료",
            "제증명료",
            "콘택트렌즈료",
            "기타"
        ],
        "values": [
            ["", "", "", "", ""],
            ["4536", "6804", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["6859", "10289", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
            ["", "", "", "", ""],
        ]
    }
]
</pre>
							</div>


						</div>
					</div>

				</div>
				<!--//recogmenu-->

			</div>
		</div>
		<!-- //.contents -->
	</div>

		<!-- #footer -->
	<%@ include file="../common/footerLanding.jsp" %>


</div>
<!-- //wrap -->

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/common/js/multipart_parse.js"></script>
<script>

	var req = null;
	var sample3File;
	var sample3SrcUrl;


	jQuery.event.add(window,"load",function(){

		$(document).ready(function (){
			loadSample3();

			$('.radio input[type="radio"]').on('click', function(){
				$('em.close').click();
			});

			$('.hospital_detail').on('click', function () {
				$('#hospital_pop').fadeIn(300);
			});

			// 파일 업로드 후 이벤트
			document.querySelector("#demoFile").addEventListener('change', function () {

				let demoFileInput = document.getElementById('demoFile');
				let demoFile = demoFileInput.files[0];
				let demoFileSize = demoFile.size;
				let max_demoFileSize = 1024 * 1024 * 2; //1kb는 1024바이트
				//파일 용량, 확장자 체크
				if(demoFileSize > max_demoFileSize || (!demoFile.type.match(/image.png/) && !demoFile.type.match(/image.jp*/))){
					$('.pop_simple').show();
					$('#demoFile').val('');
				} else {
					$('#sample3').prop('checked', false);
					$('.demolabel').html(demoFile.name);
					$('#uploadFile').removeClass('btn');
					$('#uploadFile').addClass('btn_change');
					$('.fl_box').css("opacity", "0.5");
					$('#sample1').prop('checked', false);
				}

			});


			// 샘플 팝업창 닫기
			$('.pop_sample_close, .pop_bg').on('click', function () {
				$('.pop_common').fadeOut(300);
				$('body').css({'overflow': '',	});
			});

			// 파일 업로드 오류 팝업창 닫기
			$('.pop_close, .pop_bg, .btn a').on('click', function () {
				$('.pop_simple').fadeOut(300);
				$('body').css({ 'overflow': '', });
			});

			// Remove uploaded image
			$('em.close').on('click', function () {
				$('#demoFile').val("");
				$('#uploadFile label').text('이미지 업로드');
				$('#uploadFile').removeClass("btn_change");
				$('#uploadFile').addClass("btn");
				$('.fl_box').css('opacity', '1');
			});


			// 인식하기 버튼
			$('#editImgBtn').on('click', function () {
				let file;

				if($('#demoFile').val() !== "") {    // 사용자 이미지 업로드
					file =	$('#demoFile').get(0).files[0];
					$('#samplem_edical').attr('src', URL.createObjectURL(file));
				}else{
					file = sample3File;
					$('#samplem_edical').attr('src', sample3SrcUrl);
				}


				$('#hospital_origin').text("원본 이미지");
				$('.pattern_1').hide();
				$('.loading_area').fadeIn();

				sendReqHospitalReceipt(file);
			});

			// 변환 중 취소
			$('#btn_back1').on('click', function () {
				if(req) req.abort();
				$('.loading_area').hide();
				$('.pattern_1').fadeIn(300);
			});

			// 완료 후 처음으로
			$('.btn_back2').on('click', function () {

				$('#hospital_origin').text("진료비 영수증 샘플");
				$('.result_hospital').hide();
				$('.pattern_3').hide();
				$('.pattern_1').fadeIn(300);
			});

			$('.img_pop').on('click', function () {
				$('#hospital_pop').fadeIn();
			});

		});

		function sendReqHospitalReceipt(file){

			let formData = new FormData();
			formData.append('imageNo', '20210727');
			formData.append('imageFile', file);
			formData.append('recognitions', JSON.stringify([{rcgnTypeCd:"RFRTABLE",rcgnSq:4,rcgnNm:"테이블"}]));

			req = $.ajax({
				type : "POST",
				async: true,
				url : "https://smartx-api.maum.ai:10001/TableRecog",
				data : formData,
				processData : false,
				contentType : false,
				timeout : 420000,
				success : function(result, textStatus, request) {
					console.log(result);

					let parsedObj = MultiPart_parse(result, request.getResponseHeader('Content-Type'));

					$('.result_hospital .text_area textarea').html(JSON.stringify(JSON.parse(parsedObj.recognitions), undefined, 8));
					$('.loading_area').hide();
					$('.pattern_3').fadeIn(300);
					$('.result_hospital').fadeIn(300);

				},
				error : function(req, textStatus, errorThrown) {
					if (req.status === 0) {
						return false;
					}

					$('.result_hospital .text_area textarea').html("인식 오류 : 다시 한 번 시도해 주세요.");
					$('.loading_area').hide();
					$('.pattern_3').fadeIn(300);
					$('.result_hospital').fadeIn(300);

					console.log("Error : " + errorThrown);
					// window.location.reload();
				}
			});
		}

		function loadSample3() {
			let blob = null;
			let xhr = new XMLHttpRequest();
			xhr.open("GET", "/aiaas/common/images/hospital-sample.jpg");
			xhr.responseType = "blob";
			xhr.onload = function()
			{
				blob = xhr.response;
				if(!navigator.msSaveBlob){
					sample3File = new File([blob], "hospital-sample.jpg");
				}else{
					sample3File = new Blob([blob], "hospital-sample.jpg");
				}
				sample3SrcUrl = URL.createObjectURL(blob);
			};

			xhr.send();
		}

	});


	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
</script>
	


