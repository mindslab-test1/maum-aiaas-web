﻿﻿<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<c:import url="/payment/billingForm?method=billingPay"></c:import>

<!-- .price_detail -->
<div class="price_detail api_month">
	<div class="lyr_bg"></div>
	<!-- .lyrBox -->
	<div class="lyrBox"  >
		<!-- .lyr_top -->
		<div class="lyr_top">
			<h3>API 월 사용량 제한</h3>
			<em class="fas fa-times close"></em>
		</div>
		<!-- //.lyr_top -->

		<!-- .lyr_mid -->
		<div class="lyr_mid">
			<table class="tbl_view">
				<colgroup>
					<col><col><col><col>
				</colgroup>
				<thead>
				<tr>
					<th scope="col" style="width:66px;padding:0 0 0 13px;">카테고리</th>
					<th scope="col">Engine API</th>
					<th scope="col">유형</th>
					<th scope="col"  style="width:120px;">월 사용량 제한</th>
				</tr>
				</thead>
				<tbody>
				<tr>
					<th scope="rowgroup" rowspan="7">음성</th>
					<td rowspan="2" class="col_1">TTS</td>
					<td class="col_2">한국어</td>
					<td>4000000자</td>
				</tr>
				<tr>
					<td>영어</td>
					<td>6666666자</td>
				</tr>
				<tr>
					<td>STT</td>
					<td>한국어/영어</td>
					<td>5000분</td>
				</tr>
				<tr>
					<td>음성정제</td>
					<td>날씨봇(한국어)</td>
					<td>50000분</td>
				</tr>
				<tr>
					<td>화자분리</td>
					<td>-</td>
					<td>5000분</td>
				</tr>
				<tr>
					<td>Voice Filter</td>
					<td>-</td>
					<td>25000분</td>
				</tr>
				<tr>
					<td>Voice Recognition</td>
					<td>-</td>
					<td>25000분</td>
				</tr>
				</tbody>
			</table>

			<table class="tbl_view">
				<colgroup>
					<col><col><col><col>
				</colgroup>
				<tbody>
				<tr>
					<th scope="rowgroup" rowspan="10" style="width:66px;padding:0 0 0 13px;">시각</th>
					<td class="col_1">텍스트제거</td>
					<td class="col_2">-</td>
					<td>80000건</td>
				</tr>
				<tr>
					<td>TTI</td>
					<td>AI스타일링</td>
					<td>80000건</td>
				</tr>
				<tr>
					<td>문서 이미지 분석</td>
					<td>문서인식</td>
					<td>80000건</td>
				</tr>
				<tr>
					<td>도로상의 객체 인식</td>
					<td>객체인식</td>
					<td>80000건</td>
				</tr>
				<tr>
					<td>Super Resolution</td>
					<td>화질 개선</td>
					<td>80000건</td>
				</tr>
				<tr>
					<td>Face Recognition</td>
					<td>얼굴인식</td>
					<td>80000건</td>
				</tr>
				<tr>
					<td>인물 포즈 인식</td>
					<td>-</td>
					<td>80000건</td>
				</tr>
				<tr>
					<td>얼굴추적</td>
					<td>-</td>
					<td>50000초</td>
				</tr>
				<tr>
					<td>이상행동 감지</td>
					<td>역주행</td>
					<td>50000초</td>
				</tr>
				<tr>
					<td>이미지 자막인식</td>
					<td>한국어</td>
					<td>80000건</td>
				</tr>
				</tbody>
			</table>

			<table class="tbl_view">
				<colgroup>
					<col><col><col><col>
				</colgroup>
				<tbody>
				<tr>
					<th scope="rowgroup" rowspan="6" style="width:66px;padding:0 0 0 13px;">언어</th>
					<td class="col_1">자연어 이해</td>
					<td class="col_2">한국어/영어</td>
					<td>20000단위</td>
				</tr>
				<tr>
					<td>AI 독해(MRC)</td>
					<td>한국어/영어</td>
					<td>200000건</td>
				</tr>
				<tr>
					<td>텍스트 분류(XDC)</td>
					<td>한국어/영어</td>
					<td>40000건</td>
				</tr>
				<tr>
					<td>문장생성</td>
					<td>한국어/영어</td>
					<td>100000건</td>
				</tr>
				<tr>
					<td>패턴분류 (HMD)</td>
					<td>한국어/영어</td>
					<td>400000건</td>
				</tr>
				<tr>
					<td>의도분류 (ITF)</td>
					<td>한국어</td>
					<td>400000건</td>
				</tr>
				</tbody>
			</table>

			<table class="tbl_view">
				<colgroup>
					<col><col><col><col>
				</colgroup>
				<tbody>
				<tr>
					<th scope="row" style="width:66px;padding:0 0 0 13px;">분석</th>
					<td class="col_1">데이터분석</td>
					<td class="col_2">-</td>
					<td>200000건</td>
				</tr>
				</tbody>
			</table>
			<table class="tbl_view">
				<colgroup>
					<col><col><col><col>
				</colgroup>
				<tbody>
				<tr>
					<th scope="rowgroup" rowspan="3" style="width:66px;padding:0 0 0 13px;">챗봇</th>
					<td class="col_1">NQA봇</td>
					<td class="col_2">한국어</td>
					<td>40000건</td>
				</tr>
				<tr>
					<td rowspan="2">바로쓰는 챗봇</td>
					<td>날씨봇(한국어)</td>
					<td>20000건</td>
				</tr>
				<tr>
					<td>위키봇(한국어)</td>
					<td>20000건</td>
				</tr>
				</tbody>
			</table>
			<table class="tbl_view">
				<colgroup>
					<col><col><col><col>
				</colgroup>
				<tbody>
				<tr>
					<th scope="rowgroup" rowspan="3" style="width:66px;padding:0 0 0 13px;">영어교육</th>
					<td class="col_1">영어교육용 STT</td>
					<td class="col_2">영어</td>
					<td>4000분</td>
				</tr>
				<tr>
					<td>발음 평가</td>
					<td>영어</td>
					<td>20000건</td>
				</tr>
				<tr>
					<td>파닉스 평가</td>
					<td>영어</td>
					<td>20000건</td>
				</tr>
				</tbody>
			</table>
		</div>
		<!-- //.lyr_mid -->
	</div>
	<!-- //.lyrBox -->
</div>
<!-- //.price_detail -->

<!-- .price_detail -->
<div class="price_detail minutes_month">
	<div class="lyr_bg"></div>
	<!-- .lyrBox -->
	<div class="lyrBox" >
		<div class="lyr_top">
			<h3>maum 회의록 가격정책</h3>
			<em class="fas fa-times close"></em>
		</div>
		<div class="lyr_mid">
			<div class="mid_area">
				<div class="subbox">
					<ul>
						<li>가격</li>
						<li>계정</li>
						<li>사용량 제한</li>
						<li>보관 기간</li>
					</ul>
				</div>
				<div class="minibox month_user">
					<h5>maum.ai <strong>첫 달 무료</strong></h5>
					<ul>
						<li>월 99,000원/ 계정당</li>
						<li>단일 계정</li>
						<li>50시간 / 월</li>
						<li>서비스 해지 시까지</li>
					</ul>
				</div>
				<h4>B2B Enterprise</h4>
				<div class="minibox enterprise_user">
					<h5>Basic</h5>
					<ul>
						<li>월 300만원 / 기업당</li>
						<li>다수 계정</li>
						<li>1000시간 / 월</li>
						<li>6개월 +</li>
					</ul>
				</div>
				<div class="minibox enterprise_user">
					<h5>Pro</h5>
					<ul>
						<li>월 500만원/ 기업당</li>
						<li>다수 계정</li>
						<li>3000시간 / 월</li>
						<li>1년 +</li>
					</ul>
				</div>
				<div class="minibox enterprise_user premium">
					<h5>Premium</h5>
					<p>협의<br>
						고객용 별도 모델 생성<br>
						(인식률 85% 보장)
					</p>
				</div>
			</div>
		</div>
	</div>
	<!-- //.lyrBox -->
</div>
<!-- //.price_detail -->
<!-- .lyr_info -->
<div class="price_detail price_contact">
	<div class="lyr_bg"></div>
	<!-- .lyrBox -->
	<div class="lyrBox" >
		<div class="lyr_top">
			<h3>문의하기</h3>
			<em class="fas fa-times close"></em>
		</div>
		<!-- .lyr_bd -->
		<div class="lyr_mid">
			<ul>
				<li class="half_box">
					<label for="info_name">*이름</label>
					<input id="info_name" type="text" value="" placeholder="이름">
				</li>
				<li class="half_box">
					<label for="info_number">*회사(소속)</label>
					<input id="info_number" type="number" value="" placeholder="전화번호">
				</li>
				<li>
					<label for="info_email">*이메일</label>
					<input id="info_email" type="email" value="" placeholder="이메일">
				</li>
				<li>
					<label for="info_memo">문의내용</label>
					<textarea id="info_memo" type="text" placeholder="문의내용"></textarea>
				</li>
			</ul>
			<button type="button" class="btn_blue" id="">보내기</button>
		</div>
		<!-- //.lyr_bd -->
	</div>
	<!-- //.lyrBox -->
</div>
<!-- //.lyr_info -->

<div class="pop_confirm" id="mail_success">
	<div class="pop_bg"></div>
	<!-- .popWrap -->
	<div class="popWrap">
		<!-- .pop_bd -->
		<div class="pop_bd">

			<!--내용 부분-->
			<p>문의내용이 접수되었습니다.<br>
				담당자가 빠른 시일 내에<br>
				답변 드리도록 하겠습니다.</p>
		</div>
		<!-- //.pop_bd -->
		<!--창닫기 버튼 -->
		<div class="btn">
			<button class="btn_close" >확인</button>
		</div>
		<!--창닫기 버튼 -->
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_confirm -->

<!-- .priceBox -->
<div class="priceBox">
	<h1>가격 정책</h1>
	<span>쉽게 쓰는 마음 AI로 성공적인 비즈니스를 만드세요. <em>가입 시 첫 1달 무료!</em> </span>
	<!-- .price_stn -->
	<div class="price_stn">
		<div class="cardBox business">
			<h4>Business</h4>
			<div class="inner_area">
				<p> 99,000<small>원 / 월 </small> <strong>첫 1달 무료</strong></p>
				<div>
					<span>클라우드 API<br>AI Builder</span>
					<span>maum 회의록<br>FAST 대화형 AI</span>
				</div>
				<ul>
					<li><em class="fas fa-check"></em>30+ AI 알고리즘 활용법 안내
						<small>(음성, 시각, 언어, 분석, 대화 분야)</small>
					</li>
					<li><em class="fas fa-check"></em>REST API Id, Key 및 문서 제공</li>
					<li><em class="fas fa-check"></em>월별 사용량 측정</li>
					<li><em class="fas fa-check"></em>결과 파일 내려받기</li>
					<li><em class="fas fa-check"></em>파일 보관 서비스</li>
					<li><em class="fas fa-check"></em>기술 지원</li>
				</ul>
				<button type="button" id="api_month">API 월 사용량 제한 <em class="fas fa-angle-right"></em></button>
				<button type="button" id="minutes_month" class="minutes" >maum 회의록 가격정책 <em class="fas fa-angle-right"></em></button>
				<a href="javascript:login()" title="Business 시작하기">시작하기</a>
			</div>
		</div>
		<div class="cardBox enterprise">
			<h4>Enterprise</h4>
			<div class="inner_area">
				<p>기업 커스텀</p>
				<div>
					<span>전문 AI 컨설턴트 밀착 상담 및 지원</span>
				</div>
				<ul>
					<li><em class="fas fa-check"></em>30+ AI 알고리즘 활용법 안내
						<small>(음성, 시각, 언어, 분석, 대화 분야)</small>
					</li>
					<li><em class="fas fa-check"></em>REST API Id, Key 및 문서 제공</li>
					<li><em class="fas fa-check"></em>월별 사용량 측정</li>
					<li><em class="fas fa-check"></em>결과 파일 내려받기</li>
					<li><em class="fas fa-check"></em>파일 보관 서비스</li>
					<li><em class="fas fa-check"></em>기술 지원</li>
				</ul>
				<ul class="list">
					<li><em class="fas fa-check"></em>AI 보이스 폰트 제작</li>
					<li><em class="fas fa-check"></em>학습 데이터 판매 지원</li>
					<li><em class="fas fa-check"></em>맞춤형 모델 학습 지원</li>
					<li><em class="fas fa-check"></em>B2B형 개별 사이트 구축</li>
					<li><em class="fas fa-check"></em>다수 계정 연결</li>
				</ul>
				<a href="#" class="contact_btn" target="_blank" title="Enterprise 문의하기">문의하기</a>
			</div>
		</div>
	</div>
	<!-- //.price_stn -->
</div>
<!-- //.priceBox -->
<script type="text/javascript">   
$(window).load(function() {    
	$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() { 
		$(this).remove(); 
	});
}); 
</script>
<script type="text/javascript">
// jQuery(function(){
// 	jQuery("a.btn_movLayer").movLayer();
// });
	
jQuery.event.add(window,"load",function(){
	$(document).ready(function (){
		
		
		//	pricing 더 알아보기 버튼



		$('#api_month').on('click', function(){
			$('.api_month').fadeIn();
			$('body').css({
				'overflow': 'hidden',
			});
		});
		$('#minutes_month').on('click', function(){
			$('.minutes_month').fadeIn();
			$('body').css({
				'overflow': 'hidden',
			});
		});
		$('.contact_btn').on('click', function(e){
			e.preventDefault();
			$('.price_contact').fadeIn();
			$('body').css({
				'overflow': 'hidden',
			});
		});

		$('.close, .btn_blue, .btn_close').on('click',function(){
			$('.price_detail').fadeOut()
			$('.pop_confirm').fadeOut()
			$('body').css({
				'overflow': '',
			});
		});


		// pricing section move
		$('.tbl_lst .price_1 a[href^="#"]').on('click', function (e) {
			e.preventDefault();

			var target = this.hash;
			var $target = $(target);

			$('html, body').animate({
				'scrollTop': $target.offset().top
			}, 700, 'swing');
			
		});

		// $('.folder').click( function() {
		// 	if ( $('.folder em').hasClass('fa-chevron-up')) {
		// 		$('.folder em').removeClass('fa-chevron-up');
		// 		$('.folder em').addClass('fa-chevron-down');
		// 		$('#folder_box').fadeIn();
		// 		$('.one_row').css("border-bottom", "none");
		// 		$('.one_row td:nth-child(4)').css("background", "#eff6fb");
		// 	}else {
		// 		$('.folder em').addClass('fa-chevron-up');
		// 		$('.folder em').removeClass('fa-chevron-down');
		// 		$('#folder_box').hide();
		// 		$('.one_row td:nth-child(4)').css("background", "#fff");
		// 		$('.one_row').css("border-bottom", "2px solid #979797");
		// 	}
		//
		// });
	});	
});	
	

</script>
	
<script>

	function setParam(price, goodName, sign, callback) {
		$("#billingPrice").val(price);
		$("#billingGoodName").val(goodName);
		$("#billingSignature").val(sign);
		console.dir($("#billingForm"));
		planConfirm(goodName, callback);
	}

	function planConfirm(goodName, callback) {
		let param = {
			goodName: goodName,
			userNo: '${fn:escapeXml(sessionScope.accessUser.userno)}',
			${_csrf.parameterName} : '${_csrf.token}'
		};

		$.ajax({
			type: 'POST',
			url: "/payment/planConfirm",
			dataType: 'json',
			data: param,
			async:false,
			error: function(){
				alert('planConfirm Error');
			},
			success: function(data) {
				console.log(data);
				if (data == 1) {
					callback();
				} else {
					alert("Basic 요금제에서 Basic 재 결재는 불가능 합니다.\nBusiness 요금제를 이용해 주세요.");
				}
			}
		});

	}
	
	function payFunc() {
		INIStdPay.pay('billingForm');
	};

	$(".basicBilling").click(function() {
		let price = "29000";
		let goodName = "BASIC";


		setParam(price, goodName, $("#basicSignature").val(), payFunc);
	});
	
	$(".businessBilling").click(function() {
		setParam("99000", "BUSINESS", $("#businessSignature").val(), payFunc);
	});


	function validateEmail(sEmail) {
		var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		if (filter.test(sEmail)) {
			return true;
		}
		else {
			return false;
		}
	}


	$('.btn_blue').on('click',function(){
		var name = $('#info_name').val();
		var number = $('#info_number').val();
		var email = $('#info_email').val();
		var memo = $('#info_memo').val();

		if(name !== "" && validateEmail( email ) == true){

			var title = $("#info_name").val() + "이 보낸 메일입니다. [Enterprise 문의]";
			var mailContents = "이름 : " + $("#info_name").val() + "<br>"
					+ "전화번호 : " + $("#info_number").val() + "<br>"
					+ "Email : " + $("#info_email").val() + "<br>"
					+ "문의내용 : " + $("#info_memo").val() + "<br><br><br>"
					+ "(이 문의는 가격정책 Enterprise를 통해 보낸 내용입니다.)";

			var formData = new FormData();
			formData.append('fromaddr', $("#info_email").val());
			formData.append('toaddr', 'hello@mindslab.ai');
			formData.append('subject', title);
			formData.append('message', mailContents);
			formData.append($("#key").val(), $("#value").val());

			console.log(formData);

			$.ajax({
				type 	: 'POST',
				async   : true,
				url  	: '/support/sendContactMail',
				dataType : 'text',
				data: formData,
				processData: false,
				contentType: false,
				success: function (result) {
					$('#info_name').val("");
					$('#info_number').val("");
					$('#info_email').val("");
					$('#info_memo').val("");
					console.log(result.toString());
					console.log("sending info success");

					$('.price_contact').hide();
					$('#mail_success').fadeIn();

				}, error: function (err) {
					console.log("error! ", err);
				}
			});
		}else{
			var alertMsg = "입력사항을 모두 입력해 주세요.";
			alert( alertMsg );
		}
	});

</script>



