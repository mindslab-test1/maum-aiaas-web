<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<div class="contents"> 
			<!-- The Modal -->
			<div id="myModal" class="modal">
				<div class="modalImg">
				  <a href="#none" class="layer_close"></a>
				  <img class="modal-content" id="img01" alt="불러온 이미지">
				</div>
			</div>
			<div id="myModal2" class="modal">
				<div class="modalImg">
				  <a href="#none" class="layer_close"></a>
				  <img class="modal-content" id="img02" alt="불러온 이미지">
				</div>
			</div>
			<!-- //The Modal -->
			
			<!--content-->
            <div class="content">
				<h1>Requests</h1>
				<!-- .notice -->
				<div id="noticeBox" class="request">	
					<div class="chackboxarea">
						<div class="checkbox">
							<input type="checkbox" id="checkboxType01"><label for="checkboxType01">완료</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" id="checkboxType02" checked="checked"><label for="checkboxType02">미완료</label>
						</div>
					</div>
					<div class="lst_stn">
						<button class="noticetit">Puchasing process<div class="right_txt"><span>2019.03.09 17:25</span> <em class="incomplete">미완료</em></div></button>						
						<div class="noticetxt">
							<div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit"><span class="tag">&#91;음성생성&#93;</span><span class="tag">&#91;버그&#93;</span>Puchasing process<div class="right_txt"><span>2019.03.09 17:25</span> <em class="complete">완료</em></div></button>
						<div class="noticetxt">
						     <div class="txt_area">	
								 <em class="writer">작성자 : admin@gmail.com</em>
								 <p>안녕하세요,<br>시간외근로 수당 자동합계 오류 입니다.</p>
								 <div class="imgarea">
									<span class="img">
										<img id="myImg" src="${pageContext.request.contextPath}/aiaas/kr/images/capture.jpg" alt="불러온 이미지">										
									</span>	
									 <span class="img">
										<img id="myImg2" src="${pageContext.request.contextPath}/aiaas/kr/images/capture.jpg" alt="불러온 이미지">								
									</span>	
								 </div>									 
								 <span class="attach_file">첨부파일</span>
								 <span class="attach_file_tit">캡쳐.JPG <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_download_c.png" alt="다운로드"></span>	
								 <span class="attach_file_tit">캡쳐2.JPG <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_download_c.png" alt="다운로드"></span>
							 </div>							
							 <div class="txt_area">
								<div class="answer_area">
									<textarea rows="4" cols="50"></textarea>
									 <button type="submit">전송</button>
								</div>
								 <div class="answer_complete">
									 <span>maum.ai C/S</span>
									 <em>2019.03.09 17:25</em>
									 <p>확인을 해보니 현재 정상적으로 합계가 되어지는 것으로 보입니다.<br>
다시 한 번 시도 후에도 같은 현상이 발생하신다면 정확한 상황 설명 부탁드립니다. 감사합니다.</p>
								</div>
							 </div>
						</div>
						<button class="noticetit">Puchasing process<div class="right_txt"><span>2019.03.09 17:25</span> <em class="complete">완료</em></div></button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>	
						<button class="noticetit">Puchasing process<div class="right_txt"><span>2019.03.09 17:25</span> <em class="complete">완료</em></div></button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process<div class="right_txt"><span>2019.03.09 17:25</span> <em class="complete">완료</em></div></button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit last">Puchasing process<div class="right_txt"><span>2019.03.09 17:25</span> <em class="complete">완료</em></div></button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
					</div>
					<!-- 페이징 -->
                    <div class="pageing">
                        <a href="#" class="first">처음페이지</a>
                        <a href="#" class="prev">이전페이지</a>
                        <strong>1</strong>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#">4</a>
                        <a href="#">5</a>
                        <a href="#">6</a>
                        <a href="#" class="next">다음페이지</a>
                        <a href="#" class="end">마지막페이지</a>
                    </div>
                    <!-- //페이징 -->
				</div>
				<!-- //.notice -->
		
			</div>	
			<!--content-->

		</div>


<script type="text/javascript">
jQuery.event.add(window,"load",function(){
	$(document).ready(function (){
		
		
		// 전송 버튼	
		$('.answer_area button').on('click',function(){
			$(this).parent().hide();
			$('.answer_complete').fadeIn(300);	
		});
	
		//이미지 닫기
		$('.layer_close, #myModal').on('click',function(){
			$('.modal').fadeOut(300);

		});
		
		// live chat 플로팅
		$('#btn_flt_cb').on('click', function() {
			$.ajax({
				url: 'chatbot/bot_sully_floating.html',
				//url: '/kr/maumbot',
				type: 'GET',
				dataType: 'html',
				async: false,
				success:function(data){
					$('#livechatWrap').html(data);
				},
				error:function(){
					console.log("hi");
				}
			});
		});
		
		
		
	});	
});	



//notice 리스트	
var coll = document.getElementsByClassName("noticetit");
var i;

for (i = 0; i < coll.length; i++) {
  coll[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var noticetxt = this.nextElementSibling;
    if (noticetxt.style.maxHeight){
      noticetxt.style.maxHeight = null;
    } else {
      noticetxt.style.maxHeight = noticetxt.scrollHeight + "px";
    } 
  });
} 
		
	
// 이미지 모달
var modal = document.getElementById('myModal');
// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById('myImg');
var modalImg = document.getElementById("img01");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
}

var modal = document.getElementById('myModal2');
var img = document.getElementById('myImg2');
var modalImg = document.getElementById("img02");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
}
		</script>