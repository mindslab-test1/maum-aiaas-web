﻿		<!-- .contents -->
		<div class="contents">
			<div class="content api_content">
				<h1 class="api_tit ">MRC 봇</h1>
				<ul class="menu_lst bot_lst">
					<li class="tablink" onclick="openTap(event, 'chatdemo')" id="default"><button type="button">엔진</button></li>
					<li class="tablink" onclick="openTap(event, 'chatexample')"><button type="button">적용사례</button></li>
					<li class="tablink" onclick="openTap(event, 'chatmenu')"><button type="button">매뉴얼</button></li>
<%--					<li class="tablink"><a href="/member/krApiAccount" >API ID, key 발급</a></li>--%>
				</ul>
				<div class="demobox" id="chatdemo">
					<p>Machine Reading Comprehension 엔진을 이용하여 답변하는 <span style="color:#2cace5;">MRC봇</span></p>
					<span class="sub">아무리 긴 텍스트도 읽고 검색하여 답변해줍니다. 현재는 위키와 뉴스 본문을 읽어주는 봇이 준비되어있습니다.</span>

					<div class="chatbot_tab">
						<button class="tablinks wiki" onclick="openCity(event, 'wiki')" id="defaultOpen">
							<img src="${pageContext.request.contextPath}/aiaas/common/images/img_chatbot_wiki.png" alt="wikibot">
							<span>MRC 위키봇</span>
						</button>
						<button class="tablinks news" onclick="openCity(event, 'news')">
							<img src="${pageContext.request.contextPath}/aiaas/common/images/img_chatbot_news.png" alt="newsbot">
							<span>MRC 뉴스봇</span>
						</button>
<%--					  	<button class="tablinks weather" onclick="openCity(event, 'weather')">--%>
<%--							<img src="${pageContext.request.contextPath}/aiaas/common/images/img_chatbot_weather.png" alt="weatherbot">--%>
<%--							<span>날씨봇</span>--%>
<%--						</button>--%>
					</div>


					<!-- chatbot_box -->
					<div class="chatbot_box" id="wiki">
						<!-- .chat_mid -->
						<div class="chat_mid">
							<div class="bot_infoBox">
								<div class="thumb"><img src="${pageContext.request.contextPath}/aiaas/common/images/img_chatbot_wiki.png" alt="wikibot"></div>
								<div class="txt">지식을 모두 섭렵한 챗봇 계의 석학, 위키봇 입니다.<br>
									제 상식을 한번 테스트해 보시겠어요?</div>
								<ul class="info_btnBox">
									<li><button type="button">2002년 월드컵 우승 국가는 어디야?</button></li>
									<li><button type="button">브라질 수도는?</button></li>
									<li><button type="button">아이폰을 만든 회사는?</button></li>
								</ul>
							</div>
							<ul class="talkLst">
								<li class="newDate">
									<span><!-- 날짜는 스크립트가 정의--></span>
								</li>
							</ul>
						</div>
						<!-- //.chat_mid -->
						<!-- .chat_btm -->
						<div class="chat_btm">
							<form id="formChat1" name="formChat1">
								<textarea class="textArea" placeholder="메세지를 입력해 주세요"></textarea>
								<input type="button" class="btn_chat" title="전송" value="전송">
							</form>
						</div>
						<!-- //.chat_btm -->
<%--						<div class="remark">발표: 기상청 &nbsp;&nbsp;&nbsp;제공: 공공데이터포털</div>--%>
					</div>


					<!-- //.chatbot_box -->


					<!-- chatbot_box -->
					<div class="chatbot_box" id="news">
						<!-- .chat_mid -->
						<div class="chat_mid">
							<div class="bot_infoBox">
								<div class="thumb"><img src="${pageContext.request.contextPath}/aiaas/common/images/img_chatbot_news.png" alt="newsbot"></div>
								<div class="txt">
									지난 뉴스가 궁금하시다구요?<br>
									1500만개 기사 중에 관련 내용을 찾아드립니다.
								</div>
								<ul class="info_btnBox">
									<li><button type="button">2018 동계올림픽 개최지역은?</button></li>
									<li><button type="button">신조어 소확행의 뜻은?</button></li>
									<li><button type="button">4월27일 판문점 평화의 집에서 열린 회의는?</button></li>
								</ul>
							</div>
							<ul class="talkLst">
								<li class="newDate">
									<span><!-- 날짜는 스크립트가 정의--></span>
								</li>
							</ul>
						</div>
						<!-- //.chat_mid -->
						<!-- .chat_btm -->
						<div class="chat_btm">
							<form id="formChat2" name="formChat2">
								<textarea class="textArea" placeholder="메세지를 입력해 주세요"></textarea>
								<input type="button" class="btn_chat" title="전송" value="전송">
							</form>
						</div>
						<!-- //.chat_btm -->
<%--						<div class="remark">발표: 기상청 &nbsp;&nbsp;&nbsp;제공: 공공데이터포털</div>--%>
					</div>
					<!-- //.chatbot_box -->



					<!-- chatbot_box -->
					<div class="chatbot_box" id="weather">						
						<!-- .chat_mid -->
						<div class="chat_mid">
							<div class="bot_infoBox">
								<div class="thumb"><img src="${pageContext.request.contextPath}/aiaas/common/images/img_chatbot_weather.png" alt="weatherbot"></div>
								<div class="txt">
									잠깐!! 우산 챙기셨나요?<br>
									오늘의 날씨가 궁금하시면 저에게 물어보세요.
								</div>
								<ul class="info_btnBox">
									<li><button type="button">오늘 날씨 알려줘</button></li>
									<li><button type="button">오늘 성남 날씨는?</button></li>
									<li><button type="button">내일 비 와?</button></li>
								</ul>
							</div>
							<ul class="talkLst">								
								<li class="newDate">
									<span><!-- 날짜는 스크립트가 정의--></span>
								</li> 
							</ul>
						</div>
						<!-- //.chat_mid -->
						<!-- .chat_btm -->
						<div class="chat_btm">
							<form id="formChat3" name="formChat3">
								<textarea class="textArea" placeholder="메세지를 입력해 주세요"></textarea>
								<input type="button" class="btn_chat" title="전송" value="전송">
							</form>
						</div>
						<!-- //.chat_btm -->
						<div class="remark">발표: 기상청 &nbsp;&nbsp;&nbsp;제공: 공공데이터포털</div>
					</div>
					<!-- //.chatbot_box -->

				</div>
				<!-- .demobox -->




				<!--.chatmenu-->
				<div class="demobox bot_menu" id="chatmenu">
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API 공통 가이드
							</div>
							<p class="sub_title">개발 환경 세팅</p>
							<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
							<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
							<p class="sub_title">키 발급</p>
							<p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
							<p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
							<p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
							<p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
							<p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
						</div>
						<div class="guide_group">
							<div class="title">
								IRQA (Information Retrieval Question Answering)
							</div>
							<p class="sub_txt">마인즈랩의 IRQA는 다양한 분야의 지식 소통이 가능한 인공지능 소프트웨어입니다.</p>

						</div>
					</div>
					<!--//.guide_box-->
				</div>
				<!--//chatmenu-->
				<!--.chatexample-->
				<div class="demobox" id="chatexample">
					<p><em style="color:#2cace5;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>
					<span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
					<!-- 날씨봇n위키봇 -->
					<div class="useCasesBox">
						<ul class="lst_useCases">
							<li>
								<dl>
									<dt>
										<em>CASE 01</em>
										<span>인공지능 비서</span>
									</dt>
									<dd class="txt">날씨를 알려주는 챗봇을 바로 갖다 붙이면, 인공지능 비서가 날씨를 알려줍니다.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_weatherBot"><span>날씨봇</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>AI Tutor</span>
									</dt>
									<dd class="txt">다양한 지식 정보를 대화 형식으로 제공받을 수 있는 현존하는 가장 간편한 지식 소통 인공지능 소프트웨어로, 다양한 분야의 콘텐츠에 접목될 수 있습니다.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_wikiBot"><span>위키봇</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 03</em>
										<span>장학 퀴즈</span>
									</dt>
									<dd class="txt">1,300만 건 이상의 비정형 지식베이스와 3,800만 건의 학습 데이터를 통해 정확도 높은 답변을 제공합니다.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_wikiBot"><span>위키봇</span></li>
										</ul>
									</dd>
								</dl>
							</li>
						</ul>
					</div>
					<!-- //날씨봇n위키봇 -->
				</div>
			</div>

		</div>
		<!-- //.contents -->

<input type="hidden" id="AUTH_ID">
<input type="hidden" id="SESSION_ID">
<input type="hidden" id="thumb">
<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/common.js"></script>	
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/chatbot/app.js?v=20200106"></script>
<script type="text/javascript">   

	/* news봇 추가, wiki봇 변경으로 weather봇만 기존 코드를 사용하고 나머지는 새로 추가한 코드 사용
	*  공통 코드 및 news,wiki봇 코드는 여기에 있고, weather봇용 코드는 /aiaas/kr/js/chatbot/app.js(영문과 공용) 에 있음.
	* -- 2021.01.06 YGE
	*/

	var chatName = '';

	chatSingIn();
	document.getElementById("defaultOpen").click();
	document.getElementById("default").click();

	
//	챗봇 탭 메뉴
	function openCity(evt, botName) {
		let i, tabContent, tabLinks;
		tabContent = document.getElementsByClassName("chatbot_box");
		for (i = 0; i < tabContent.length; i++) {
			tabContent[i].style.display = "none";
		}
		tabLinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tabLinks.length; i++) {
			tabLinks[i].className = tabLinks[i].className.replace(" active", "");
		}
		document.getElementById(botName).style.display = "block";
		evt.currentTarget.className += " active";
		chatName = botName;

		if($('#'+chatName+' .chat_mid .talkLst li').length===1){
			$('#'+chatName+' .bot_infoBox').css({
				'display':'block'
			});
			$('#'+chatName+' .talkLst').css({
				'display':'none'
			});
		}

		switch (botName){
			case "wiki":
				document.getElementById('thumb').value='/aiaas/common/images/img_chatbot_wiki.png';
				break;
			case "news":
				document.getElementById('thumb').value='/aiaas/common/images/img_chatbot_news.png';
				break;
			case "weather":
				document.getElementById('thumb').value='/aiaas/common/images/img_chatbot_weather.png';
				chatOpen("Weather");
				break;
		}
	}

	function getAmPm(){
		return new Date().getHours() >= 12 ? '오후' : '오전';
	}
	function getTime(){
		let	thisHours = new Date().getHours() >=13 ?  new Date().getHours()-12 : new Date().getHours(); //현재 시
		let	thisMinutes = new Date().getMinutes() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes(); //현재 분
		return thisHours + ':' + thisMinutes;
	}

	// chat_mid height값 조정
	function changeChatMidHeight() {
		let winWidth = $(window).width();
		let cahtbotWrapHeight = $('#cahtbotWrap').height();
		let minusHeight = (winWidth < 760 ? 130 : 145);
		$('#cahtbotWrap').each(function () {
			$('.chatbot_box .chat_mid').css({'height': Math.round(cahtbotWrapHeight - minusHeight),});
		});
	}

	function appendAnswerTalk(result){
        $('#'+chatName+' .chat_mid .talkLst').append(
            '<li class="bot">'+
            '<span class="thumb"><img src="' + document.getElementById('thumb').value + '"></span>'+
            '<span class="cont"> '+
            '<em class="txt">' + result + '</em> '+
            '</span> '+
            '</li>'
        );
    }


    function appendTalkTime(){
        $('#'+chatName+' .chat_mid .talkLst .bot:last-child .cont:last-child').append(
            '<em class="date"><strong>' + getAmPm() + '</strong>' + getTime() + '</em>'
        );
    }

    // textArea disabled 해제 & 로딩 UI 제거
    function endTalkLoadingUI(){
        $('#'+chatName+' .chat_btm .textArea').attr('disabled', false);
        $('#'+chatName+' .chat_mid .talkLst li.bot:last-child').remove();
    }



	$(document).ready(function () {
		// 날짜, 요일 시간 정의
		let year = new Date().getFullYear();  //현재 년도
		let month = new Date().getMonth() + 1;  //현재 월
		let date = new Date().getDate();  //현재 일
		let week = ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'];	  //요일 정의
		let thisWeek = week[new Date().getDay()];	//현재 요일


		// 오늘 날짜 입력
		$('.talkLst li.newDate span').each(function () {
			$(this).append(year + '년 ' + month + '월 ' + date + '일 ' + thisWeek);
		});

		// 첫멘트 시간
		$('.chatbot_box .chat_mid .talkLst li.bot .cont:last-child').append('<em class="date"><strong>' + getAmPm() + '</strong>' + getTime() + '</em>');

		// 내용있을 시 스크롤 최하단
		$('.chatbot_box .chat_mid').scrollTop($('.chatbot_box .chat_mid')[0].scrollHeight);

		// 채팅입력 (Enter)
		$('.chatbot_box .chat_btm .textArea').keyup(function (event) {
			if (event.keyCode === 13) {
				$('#'+chatName+' .btn_chat').trigger('click');
			}
		});



		// 추천질문 (text 출력)
		$('.info_btnBox li button').on('click', function () {
			var recomQust = $(this).text();

			$('.chatbot_box .chat_btm .textArea').val(recomQust);
			$('#'+chatName+' .btn_chat').trigger('click');

			$('.chatbot_box .bot_infoBox').css({'display': 'none'});
			$('.chatbot_box .talkLst').css({'display': 'block'});

			changeChatMidHeight();

			$('.chatbot_box .chat_mid').scrollTop($('.chatbot_box .chat_mid')[0].scrollHeight);
			$('.chatbot_box .chat_btm .textArea').val('');

		});

		// 채팅입력 (text 출력)
		$('.btn_chat').on('click', function() {
			let $chatTextArea = $('#'+chatName+' .chat_btm .textArea');

			// textarea 텍스트 값 및 엔터처리
			let textValue = $chatTextArea.val().trim().replace(/(?:\r\n|\r|\n)/g, '<br>');

			$chatTextArea.prop('disabled', true);

			if( $chatTextArea.val().replace(/\s/g,"").length === 0){
				$('#'+chatName+' .chat_btm .textArea').val('');

				return;
			}

			// 채팅창에 text 출력
			$('#'+chatName+' .chat_mid .talkLst').append(
					'<li class="user"> '+
					'<span class="cont"> '+
					'<em class="txt">' + textValue + '</em> '+
					'<em class="date"><strong>' + getAmPm() + '</strong>' + getTime() + '</em> '+
					'</span> '+
					'</li>'
			);

			//로딩 UI 추가
			$('#'+chatName+' .chat_mid .talkLst').append(
					'<li class="bot">'+
						'<span class="thumb"><img src="' + document.getElementById('thumb').value + '" alt="chatbot_img"></span>'+
						'<span class="cont">'+
							'<em class="txt">'+
								'<span class="chatLoading">'+
									'<strong class="chatLoading_item01"></strong>'+
									'<strong class="chatLoading_item02"></strong>'+
									'<strong class="chatLoading_item03"></strong>'+
								'</span>'+
							'</em> '+
						'</span> '+
					'</li>'
			);

			if(chatName === "weather"){
				sendTalk(textValue);
			}else{
				sendSimpleQaReq(textValue);
			}


			$('.chatbot_box .bot_infoBox').css({ 'display':'none' });
			$('.chatbot_box .talkLst').css({ 'display':'block' });

			$('#'+chatName+' .chat_btm .textArea').val('');

			changeChatMidHeight();
			$('#'+chatName+' .chat_mid').scrollTop($('#'+chatName+' .chat_mid')[0].scrollHeight);

		});

	});

//---------------------------------- news, wiki봇용 ---------------------------------------//
	function sendSimpleQaReq(talkMessage){
		let data = {
			"question": talkMessage,
			"ntop": 1,
			"domain": chatName,
			"${_csrf.parameterName}" : "${_csrf.token}"
		};

        $.ajax({
            url		: "/api/chat/simpleQA",
            data    : data,
			type	: "post",
            success : function(response) {

            	if(response === null || response === ""){
					endTalkLoadingUI();
					console.log("chatbot SendTalk error! ");
					alert("응답을 받아오지 못했습니다. 다시 시도해 주세요.");
					return;
				}

                console.log(response);

                // textArea disabled 해제 & 로딩 UI 제거
                endTalkLoadingUI();

                let answer = response.payload.answer;
                appendAnswerTalk(answer);
                appendTalkTime();

                $('#'+chatName+' .chat_mid').scrollTop($('#'+chatName+' .chat_mid')[0].scrollHeight);
            },
            error : function(xhr) {
                endTalkLoadingUI();
                console.log("chatbot SendTalk error! ", xhr);
                alert("응답을 받아오지 못했습니다. 다시 시도해 주세요.");
            }

        });
	}


	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablink");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
</script>


