<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>


<body>
<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>이미지 업로드 실패</h5>
            <p>이미지 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
                업로드 되지 않았습니다.</p>
            <span>* 지원가능 파일 확장자: .jpg, .bmp<br>
* JPG 파일을 사용하기를 권고드립니다.<br>
* 이미지 용량 300KB 이하만 가능합니다.</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->
<!-- .contents -->
<div class="contents">
    <!-- [D] 21.09.23 .content 영역 전체 수정 필요합니다. -->
    <!-- .content -->
    <div class="content api_content">
        <h1 class="api_tit">스타일 코멘트</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'avrdemo')" id="defaultOpen">
                <button type="button">엔진</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'avrexample')">
                <button type="button">적용사례</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'avrmenu')">
                <button type="button">매뉴얼</button>
            </li>
        </ul>

        <!-- .avrdemo -->
        <div class="demobox" id="avrdemo">
            <p>
                <span>스타일 코멘트</span> <small>(AIR-style Comment)</small>
            </p>
            <span class="sub">이미지에서 사람이 입고 있는 의상에 관한 적절한 코멘트를 해줍니다.</span>

            <!--demo_layout-->
            <div class="demo_layout air_eg styleComment">
                <!--avr_1-->
                <div class="avr_1" style="display: block;">
                    <div class="fl_box" opacity="1" style="opacity: 1;">
                        <p><em class="far fa-image"></em><strong>샘플 파일</strong>로 테스트 하기</p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="option" value="1" checked="">
                                    <label for="sample1" class="female">
                                        <div class="img_area">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_styleComment_sample.jpg" alt="sample image">
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-image"></em><strong>내 파일</strong>로 해보기</p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-image hidden"></em>
                                <label for="demoFile" class="demolabel">이미지 업로드</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".jpg, .bmp">
                            </div>
                            <ul>
                                <li>* 이미지 지원가능 파일 확장자: jpg, bmp</li>
                                <li>* 이미지 내에는 한 사람만 있어야 합니다.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">결과보기</button>
                    </div>
                </div>
                <!--avr_1-->

                <!--avr_2-->
                <div class="avr_2" style="display: none;">
                    <p><em class="far fa-image"></em>이미지 분석중</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"></path><g><path fill="#f7778a" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"></path><animateTransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"></animateTransform></g></svg>

                        <p>이미지를 분석하는 데 약간의 시간이 소요됩니다. (최대 5분)</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
                    </div>

                </div>
                <!--avr_2-->

                <!--avr_3-->
                <div class="avr_3" style="display: none;">
                    <div class="origin_file">
                        <p><em class="far fa-image"></em>입력 파일</p>
                        <div class="sample_box">
                            <div class="imgBox">
                                <img id="input_img" src="${pageContext.request.contextPath}/aiaas/common/images/img_styleComment_sample.jpg" alt="원본 이미지">
                            </div>
                        </div>
                    </div>
                    <div class="result_file">
                        <p><em class="far fa-file-alt"></em>스타일 코멘트</p>
                        <div class="commentBox">
                            <div class="textBox">
                                <div id="resultTxt">

                                </div>
                                <!-- [D] 21.09.23 - .result_false : 비정상 인식 결과일 때  -->
                                <!-- <div id="resultTxt">결과를 인식하지 못하였습니다. 다시 시도해 주세요.</div> -->
                            </div>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
                    </div>
                </div>
                <!--avr_3-->
            </div>
            <!--// demo_layout-->

            <div class="engineInfo">
                <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_air.png" alt="AIR 로고">
                <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_etri.png" alt="ETRI 로고">
                <p>해당 엔진은 대한민국 과학기술정보통신부가 지원하는 정부 R&D 과제 &ldquo;고령 사회에 대응하기 위한 실환경 휴먼케어로봇 기술 개발(AIR)&rdquo; 중 한국전자통신연구원(ETRI)의 연구과제 성과물 입니다.</p>
            </div>
        </div>
        <!-- //.avrdemo -->

        <!--.avrmenu-->
        <div class="demobox vision_menu" id="avrmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">API 공통 가이드</div>

                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>

                <div class="guide_group">
                    <div class="title">스타일 코멘트 <small>(AIR-style Comment)</small></div>

                    <p class="sub_txt">이미지에서 사람이 입고 있는 의상에 관한 적절한 코멘트를 해줍니다.</p>
                    <span class="sub_title">준비사항</span>
                    <p class="sub_txt">- Input: 이미지 파일</p>
                    <ul>
                        <li>확장자 : .jpg, .bmp</li>
                        <li>용량 : 3MB 이하 </li>
                    </ul>
                </div>
            </div>
        </div>
        <!--//avrmenu-->

        <!--.avrexample-->
        <div class="demobox" id="avrexample">
            <p><em style="color:#f7778a;font-weight: 400;">적용사례</em> <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>

            <!-- 의상 코디 -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>의상 코디</span>
                            </dt>
                            <dd class="txt">스타일링된 의상에 적절한 코멘트를 하여 코디해 줍니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_stlCmt"><span>스타일 코멘트</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //의상 코디 -->
        </div>
        <!--//.avrexample-->
    </div>
    <!-- //.content -->
</div>
<!-- //.contents -->
</body>

<script type="text/javascript">


    var sampleImage;

    //파일명 변경
    document.querySelector("#demoFile").addEventListener('change', function (ev) {
        var demoFileInput = document.getElementById('demoFile');
        var demoFile = demoFileInput.files[0];
        var demoFileSize = demoFile.size;
        var max_demoFileSize = 1024 * 5000;//1kb는 1024바이트
        var demofile = document.querySelector("#demoFile");
        if(demoFileSize > max_demoFileSize || !demofile.files[0].type.match(/image.*/)){
            $('.pop_simple').show();
            // 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
            $('.fl_box').css("opacity", "0.5");
            $('#demoFile').val('');
            $('.avr_1 .btn_area').append('<span class="disBox"></span>');

            return;
        }


        document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
        var element = document.getElementById( 'uploadFile' );
        element.classList.remove( 'btn' );
        element.classList.add( 'btn_change' );
        $('.fl_box').css("opacity", "0.5");
    });


    jQuery.event.add(window,"load",function(){

        //샘플
        function loadSample() {
            var blob = null;
            var xhr = new XMLHttpRequest();
            xhr.open("GET", "/aiaas/common/images/img_styleComment_sample.jpg");
            xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
            xhr.onload = function()
            {
                blob = xhr.response;//xhr.response is now a blob object
                sampleImage = new File([blob], "img_styleComment_sample.jpg");
                var imgSrcURL = URL.createObjectURL(blob);
                var styleComment_output=document.getElementById('input_img');
                styleComment_output.setAttribute("src",imgSrcURL);
            };

            xhr.send();
        }


        $(document).ready(function (){
            loadSample();


            $('.radio label').on('click',function(){
                $('.fl_box').attr('opacity',1);
                $('em.close').click();
            });


            // step1->step2  (close button)
            $('em.close').on('click', function () {
                $('.fl_box').css("opacity", "1");
                $('#uploadFile label').text('이미지 업로드');
                $('#uploadFile').removeClass("btn_change");
                $('#uploadFile').addClass("btn");
                $('#demoFile').val("");

                //파일명 변경
                document.querySelector("#demoFile").addEventListener('change', function (ev) {
                    document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
                    var element = document.getElementById( 'uploadFile' );
                    element.classList.remove( 'btn' );
                    element.classList.add( 'btn_change' );
                });
            });


            // 처음으로 버튼
            $('.btn_back1').on('click', function () {

                $('.avr_2').hide();
                $('.avr_1').fadeIn(300);
            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('.avr_3').hide();
                $('.avr_1').fadeIn(300);

                document.querySelector("#demoFile").addEventListener('change', function (ev) {
                    document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;

                });
            });

            //결과보기 버튼
            $('#sub').on('click',function (){

                var demoFileTxt = $('#demoFile').val(); //파일을 추가한 input 박스의 값
                var formData = new FormData();
                var demoFile;

                //파일을 추가한 input 값 없을 때
                if(demoFileTxt === ""){
                    var option = $("input[type=radio][name=option]:checked").val();
                    loadSample();
                    demoFile = sampleImage;

                }

                //파일을 추가한 input 값 있을 때
                else {
                    var demoFileInput = document.getElementById('demoFile');
                    // console.log(demoFileInput);
                    demoFile = demoFileInput.files[0];
                    // console.log(demoFile);


                }

                formData.append('file',demoFile);
                formData.append('${_csrf.parameterName}', '${_csrf.token}');

                $('.avr_1').hide();
                $('.avr_2').fadeIn(300);

                xhr = $.ajax({
                    type: "POST",
                    async: true,
                    url: 'https://182.162.19.14:9941/styleComment', //여기 url 추가
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(result){
                        let resultData = result.comment;
                        $('#resultTxt').text(resultData);

                        var imgSrcURL = URL.createObjectURL(demoFile);
                        var styleComment_output = document.getElementById('input_img');
                        styleComment_output.setAttribute("src",imgSrcURL);

                        $('.avr_1').hide();
                        $('.avr_2').hide();
                        $('.avr_3').fadeIn(300);

                    },
                    error: function(jqXHR, error){
                        if(jqXHR.status === 0){
                            return false;
                        }

                        alert("서버와 연결이 되지 않습니다.\n잠시 후에 다시 이용해주세요.");
                        console.dir(error);
                        window.location.reload();
                    }
                });


            });

        });
    });


    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

</script>