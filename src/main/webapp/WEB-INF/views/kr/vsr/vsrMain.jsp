<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-07-10
  Time: 17:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>파일 업로드 실패</h5>
            <p>파일 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
                업로드 되지 않았습니다.</p>
            <span>* 지원가능 파일 확장자: .mp4, .avi, mkv<br>
                  * 해상도 720p 이하, 길이 15초 이하 제한<br>
                  * fps가 높을수록 처리 속도가 느려집니다.</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents api_content">
    <!-- .content -->
    <div class="content">
        <h1 class="api_tit">얼굴 추적</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'vsrdemo')" id="defaultOpen">
                <button type="button">엔진</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'vsrexample')">
                <button type="button">적용사례</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'vsrmenu')">
                <button type="button">매뉴얼</button>
            </li>
            <%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <!-- .demobox -->
        <div class="demobox" id="vsrdemo">
            <p>영상 해상도를 높이는 <span>Video Super Resolution</span></p>
            <span class="sub">저화질 영상의 해상도를 2배 높여 줍니다.</span>
            <!--vsr_box-->
            <div class="demo_layout faceTracking_box vsr_box">
                <!--tr_1-->
                <div class="tr_1">
                    <div class="fl_box">
                        <p><em class="far fa-file-video"></em><strong>샘플 파일</strong>로 테스트 하기</p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="option" value="1" checked>
                                    <label for="sample1" class="">
                                        <video id="sampleVideo1" controls width="485" height="269" poster="${pageContext.request.contextPath}/aiaas/common/images/vsr_sample.png">
                                            <source src="${pageContext.request.contextPath}/aiaas/common/video/VSR_sample.mp4?ver=20200716"
                                                    type="video/mp4">
                                            IE 8 이하는 비디오가 나오지 않습니다. IE 버전을 업데이트 하시길 바랍니다.
                                        </video>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-video"></em><strong>내 파일</strong>로 해보기</p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-video hidden"></em>
                                <label for="demoFile" class="demolabel">영상 업로드</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".avi, .mp4, .mkv">
                            </div>
                            <ul>
                                <li>* 지원가능 파일 확장자: .mp4, .avi, .mkv</li>
                                <li>* 해상도 720p 이하, 길이 15초 이하 제한</li>
                                <li>* fps가 높을수록 처리 속도가 느려집니다.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">결과보기</button>
                    </div>
                </div>
                <!--tr_1-->
                <!--tr_2-->
                <div class="tr_2">
                    <p><em class="far fa-file-video"></em>해상도 높이는 중</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px"
                             viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42"
                                                                             d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/>
                            <g>
                                <path fill="#f7778a" fill-opacity="1"
                                      d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/>
                                <animatetransform attributeName="transform" type="translate"
                                                  values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0"
                                                  calcMode="discrete" dur="1820ms" repeatCount="indefinite"/>
                            </g></svg>

                        <p>약간의 시간이 소요 됩니다.</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
                    </div>

                </div>
                <!--tr_2-->
                <!--tr_3-->
                <div class="tr_3">

                    <div class="result_file">
                        <p><em class="far fa-file-video"></em>입력 파일</p>
                        <div class="file_box">
                            <video id="inputVideo" controls width="485" height="269">
                                <source src="" type="video/mp4">
                                IE 8 이하는 비디오가 나오지 않습니다. IE 버전을 업데이트 하시길 바랍니다.
                            </video>
                        </div>
                        <p style="padding:17px 0 0 0;"><em class="far fa-file-video"></em>결과 파일</p>
                        <div class="file_box">
                            <video id="outputVideo" controls width="485" height="269">
                                <source src="" type="video/mp4">
                                IE 8 이하는 비디오가 나오지 않습니다. IE 버전을 업데이트 하시길 바랍니다.
                            </video>
                            <a href="" id="resultDwn" class="dwn" download="vsr.mp4" ><em class="far fa-arrow-alt-circle-down"></em>결과 파일 다운로드</a>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2" id=""><em class="fas fa-redo"></em>처음으로</button>
                    </div>
                </div>
                <!--tr_3-->
            </div>
            <!--//.vsr_box-->

        </div>
        <!-- //.demobox -->
        <!--.vsr-->
        <div class="demobox vision_menu" id="vsrmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        Video Super Resolution <small></small>
                    </div>
                    <p class="sub_txt">저화질 영상의 해상도를 2배 높여 줍니다.
                    </p>

                    <span class="sub_title">
								준비사항
							</span>
                    <p class="sub_txt">- Input: 저화질 영상 </p>
                    <ul>
                        <li>확장자: .mp4, .avi</li>
                        <li>해상도: 720p 이하</li>
                        <li>길이: 15초 이하</li>
                        <li>* fps가 높을수록 처리 속도가 느려집니다. </li>
                    </ul>
                    <span class="sub_title">
								 실행 가이드
							</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/vsr/download</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>file</td>
                            <td>Super Resolution 처리 할 영상 파일 </td>
                            <td>file</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                        curl --location --request POST 'http://api.maum.ai/vsr/download' \<br>
                        --form 'apiId= {발급받은 API ID}' \<br>
                        --form 'apiKey= {발급받은 API KEY}' \<br>
                        --form 'file={동영상 파일 path}'
                    </div>

                    <p class="sub_txt">④ Response 예제 </p>

                    <div class="code_box">
                        (.mp4 File Download)
                    </div>
                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.vsrmenu-->
        <!--.vsrexample-->
        <div class="demobox" id="vsrexample">
            <p><em style="color:#f7778a;font-weight: 400;">적용사례</em> <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
            <!-- vsr -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>CCTV 속 인물 탐지</span>
                            </dt>
                            <dd class="txt">CCTV 영상 속 인물의 작은 사진을 손실률을 줄이면서 확대하여 Identify합니다. CCTV 뿐만 아니라, 비디오 또는 사진 내 작은 인물의 이미지를 확대에 활용할 수 있습니다. </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_tr"><span>ESR</span></li>
                                    <li class="ico_avr"><span>AVR</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>옛날 영상 복원</span>
                            </dt>
                            <dd class="txt">옛날 영상 또는 저화질의 비디오의 복원에 활용될 수 있습니다. 딥러닝 기술을 이용하여 저화질의 영상을 복원해 보세요.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_den"><span>음성 정제</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>보안 카메라 적용</span>
                            </dt>
                            <dd class="txt">저화질의 보안 카메라와 연결하여 보다 정확하게 출입 통제 및 이상행동 감지가 가능합니다.
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_ftr"><span>얼굴 추적</span></li>
                                    <li class="ico_anor"><span>이상 행동</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!--vsr  -->
        </div>
        <!--//.vsrexample-->
    </div>
    <!-- //.content -->
</div>

<script>

    var sample1SrcUrl;
    var sample1File;
    var ajaxXHR;


    function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/common/video/VSR_sample.mp4");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function () {
            blob = xhr.response; //xhr.response is now a blob object
            sample1File = new File([blob], "sample1.mp4", {type:"video/mp4"});
            sample1SrcUrl = URL.createObjectURL(blob);
        };
        xhr.send();
    }



    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {

            loadSample1();

            //파일명 변경
            document.querySelector("#demoFile").addEventListener('change', function (ev) {

                let demoFile = ev.target.files[0];

                if(demoFile == null) return; // x 를 눌러 업로드할 파일을 지운 경우

                //파일 용량 체크
                var max_demoFileSize = 1024 * 1024 * 15;//1kb는 1024바이트


                if (!demoFile.type.match(/video.mp4/) && !demoFile.type.match(/video.avi/) && !demoFile.type.match(/video.mkv/)) {
                    $('.pop_simple').show();
                    $('#demoFile').val(null);

                } else {

                    let video = document.createElement('video');
                    video.preload = 'metadata';
                    video.onloadedmetadata = function() {
                        window.URL.revokeObjectURL(video.src);
                        let dur =  video.duration;
                        console.log (dur);

                        if (dur > 15) {
                            console.log("15초 이상의 비디오는 업로드 할 수 없습니다.");
                            $('.pop_simple').show();
                            $('#demoFile').val('');

                        } else {
                            document.querySelector(".demolabel").innerHTML = demoFile.name;
                            $('#uploadFile').removeClass('btn').addClass('btn_change');
                            $('.fl_box').css("opacity", "0.5");
                            $('#sample1').prop('checked', false);
                        }
                    };
                    video.src = URL.createObjectURL(demoFile);
                    const delete1 = video.delete;
                    // console.log(delete1);
                }
            });


            // close button
            $('em.close').on('click', function () {
                //console.log('업로드 취소');
                $('.fl_box').css("opacity", "1");
                $(this).parent().removeClass("btn_change").addClass("btn");
                $(this).parent().children('.demolabel').text('영상 업로드');
                $('#demoFile').val('');
                $('#sample1').prop('checked', true);

            });

            $('.fl_box').on('click', function () {
                $(this).css("opacity", "1");
            });


            $('.sample_box').on('click', function () {
                $('em.close').trigger('click');
                $('#sample1').prop('checked', true);
            });


            // 처음으로 버튼
            $('.btn_back1').on('click', function () {
                ajaxXHR.abort();
                $('.tr_2').hide();
                $('.tr_1').fadeIn(300);
            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('#inputVideo').get(0).pause();
                $('#outputVideo').get(0).pause();
                $('.tr_3').hide();
                $('.tr_1').fadeIn(300);
                $('.fl_box').css("opacity", "1");
                $('#sample1').trigger('click');
                $('#demoFile').val('');
            });


            //알림 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });


            $('.btn_start').on('click', function () {

                let inputFile;
                let inputFileUrl;

                if ($('#sample1').prop('checked')) { // 샘플 파일로 해보기

                    let blob;
                    blob = sample1File;
                    inputFileUrl = sample1SrcUrl;

                    if (blob == null) {
                        alert("동영상 파일 로딩 중입니다. 잠시 후 다시 시도해 주세요.");
                        return;

                    } else {
                        // filename += '.' + file.type.split('/').pop();
                        inputFile = sample1File;
                    }

                } else { // 내 파일로 해보기
                    inputFile = $('#demoFile').get(0).files[0];

                    if(inputFile == null || inputFile == ""){
                        alert("샘플을 선택하거나 영상을 업로드해 주세요!");
                        return;
                    }

                    inputFileUrl = URL.createObjectURL(inputFile);
                    console.log(typeof(inputFile));
                }

                $('.tr_1').hide();
                $('.tr_2').fadeIn(300);
                $('#inputVideo').attr('src', inputFileUrl);

                sendApiRequest(inputFile);
            });
        });
    });


    function sendApiRequest(file) {

        var formData = new FormData();
        formData.append('file', file);
        formData.append('${_csrf.parameterName}', '${_csrf.token}');

        ajaxXHR = new XMLHttpRequest();
        ajaxXHR.responseType ="blob";
        ajaxXHR.onreadystatechange = function(){
            if (ajaxXHR.readyState === 4 && ajaxXHR.status === 200){

                let resultBlob = new Blob([ajaxXHR.response], {type: 'video/mp4'});
                let srcUrl = URL.createObjectURL(resultBlob);
                $('#outputVideo').attr('src', srcUrl);
                $('#resultDwn').attr('href', srcUrl);

                $('.tr_2').hide();
                $('.tr_3').fadeIn(300);
                $('.vsr_box').css('border','none');

                $('#inputVideo').get(0).play();
                $('#outputVideo').get(0).play();
            }
        };

        ajaxXHR.open('POST', '/api/vsr');
        ajaxXHR.send(formData);

        ajaxXHR.timeout = 90000;
        ajaxXHR.ontimeout = function() {
            alert("timeout...!");
            window.location.reload();
        };

        ajaxXHR.addEventListener("abort", function(){
            // $('.tr_3').hide();
            // $('.tr_1').fadeIn(300);
        });
    }

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

</script>