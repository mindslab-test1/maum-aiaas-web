<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ include file="../common/common_aihuman_header.jsp" %>

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<!-- #wrap -->
<div id="wrap" class="maumUI visual_header maumOrchestraBuilder">
    <%@ include file="../common/header.jsp" %>


    <!-- #contents -->
    <div id="contents">
        <div class="content">
            <div class="visual_cnt">
                <div class="visual_text">
                    <h2><img src="${pageContext.request.contextPath}/aiaas/aibuilder/images/img_aihumanorder.svg" alt="AI Human Order"></h2>
                    <h3>“세상의 모든 <b>AI</b>는 <b>마음</b>으로 통한다.”</h3>
                    <p><img src="${pageContext.request.contextPath}/aiaas/aibuilder/images/title_maumai.svg" alt="인공인간 플랫폼 maum.ai"></p>
                </div>
                <div class="bg_vid">
                    <div>
                        <canvas width="1440" height="630" id="screen1"></canvas>
                    </div>
                </div>
            </div>
            <div class="section_top">
                <h3>Make Your Own AI Human</h3>
                <p>당신이 원하는 인공인간을 만들어보세요.</p>
                <span class="line"></span>
            </div>
            <ol class="accordion_wrap">
                <li class="accordion job_accd">
                    <div class="acd_title">
                        <div class="title_left">
                                <span class="title_icon">
                                    <img src="${pageContext.request.contextPath}/aiaas/aibuilder/images/ico_browser_56px.svg" alt="browser icon">
                                </span>
                            <p class="step_num">STEP 1</p>
                            <h3 class="acd_tit">
                                <span class="tit_title">JOB</span>
                                <span class="tit_span">어떤 직업의 인공인간을 원하시나요?</span>
                            </h3>
                        </div>
                        <div class="title_right">
                            <span class="icon"></span>
                        </div>
                    </div>
                    <div class="acd_cnt">
                        <div class="swiper job_slider">
                            <div class="swiper-wrapper">
                            </div>
                        </div>
                    </div>
                </li>
                <li class="accordion who_accd">
                    <div class="acd_title">
                        <div class="title_left">
                                <span class="title_icon">
                                    <img src="${pageContext.request.contextPath}/aiaas/aibuilder/images/ico_people_56px.svg" alt="people icon">
                                </span>
                            <p class="step_num">STEP 2</p>
                            <h3 class="acd_tit">
                                <span class="tit_title">WHO</span>
                                <span class="tit_span">인공인간의 외모와 목소리를 골라보세요.</span>
                            </h3>
                        </div>
                        <div class="title_right">
                            <span class="icon"></span>
                        </div>
                    </div>
                    <div class="acd_cnt">
                        <div class="swiper who_slider">
                            <div class="swiper-wrapper">
                            </div>
                        </div>
                    </div>
                </li>
                <li class="accordion work_accd">
                    <div class="acd_title">
                        <div class="title_left">
                                <span class="title_icon">
                                    <img src="${pageContext.request.contextPath}/aiaas/aibuilder/images/ico_job_56px.svg" alt="job icon">
                                </span>
                            <p class="step_num">STEP 3</p>
                            <h3 class="acd_tit">
                                <span class="tit_title">WORK</span>
                                <span class="tit_span">인공인간이 할 수 있는 기능을 선택하세요.</span>
                            </h3>
                        </div>
                        <div class="title_right">
                            <span class="icon"></span>
                        </div>
                    </div>
                    <div class="acd_cnt">
                        <div class="work_cnt">
                        </div>
                    </div>
                </li>
                <li class="accordion display_accd">
                    <div class="acd_title">
                        <div class="title_left">
                                <span class="title_icon">
                                    <img src="${pageContext.request.contextPath}/aiaas/aibuilder/images/ico_kiosk_56px.svg" alt="kiosk icon">
                                </span>
                            <p class="step_num">STEP 4</p>
                            <h3 class="acd_tit">
                                <span class="tit_title">DISPLAY</span>
                                <span class="tit_span">인공인간이 활동할 기기를 선택하세요.</span>
                            </h3>
                        </div>
                        <div class="title_right">
                            <span class="icon"></span>
                        </div>
                    </div>
                    <div class="acd_cnt">
                        <div class="swiper display_slider">
                            <div class="swiper-wrapper">
                            </div>
                        </div>
                    </div>
                </li>
                <li class="accordion tools_accd">
                    <div class="acd_title">
                        <div class="title_left">
                                <span class="title_icon">
                                    <img src="${pageContext.request.contextPath}/aiaas/aibuilder/images/ico_brief_56px.svg" alt="brief icon">
                                </span>
                            <p class="step_num">STEP 5</p>
                            <h3 class="acd_tit">
                                <span class="tit_title">TOOLS</span>
                                <span class="tit_span">인공인간의 다양한 기술을 만나보세요.</span>
                            </h3>
                        </div>
                        <div class="title_right">
                            <span class="icon"></span>
                        </div>
                    </div>
                    <div class="acd_cnt">
                        <div class="tools_cnt">
                        </div>
                    </div>
                </li>
                <li class="accordion orderDetails_accd">
                    <div class="acd_title">
                        <div class="title_left">
                                <span class="title_icon">
                                    <img src="${pageContext.request.contextPath}/aiaas/aibuilder/images/ico_list_56px.svg" alt="list icon">
                                </span>
                            <p class="step_num">Completion</p>
                            <h3 class="acd_tit">
                                <span class="tit_title">Order Summary</span>
                                <span class="tit_span">당신만의 인공인간은 이렇게 만들어집니다.</span>
                            </h3>
                        </div>
                        <div class="title_right">
                            <span class="icon"></span>
                        </div>
                    </div>
                    <div class="acd_cnt">
                        <table class="orderDetails_cnt">
                            <colgroup>
                                <col width="136px;">
                                <col width="*">
                            </colgroup>
                            <tr class="job">
                                <th>JOB</th>
                                <td>
                                    <div class="select">
                                        <div>
                                            <p class="txt_details"></p>
                                            <div class="pirce">
                                                <span class="won">₩</span>
                                                <span class="cost">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="actor">
                                <th>WHO</th>
                                <td>
                                    <div class="select">
                                        <div>
                                            <p class="txt_details"></p>
                                            <div class="pirce">
                                                <span class="won">₩</span>
                                                <span class="cost">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="workflow">
                                <th>WORK</th>
                                <td>
                                    <div class="select">
                                        <div>
                                            <p class="txt_details"></p>
                                            <div class="pirce">
                                                <span class="won">₩</span>
                                                <span class="cost">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="device">
                                <th>DISPLAY</th>
                                <td>
                                    <div class="select">
                                        <div>
                                            <p class="txt_details"></p>
                                            <div class="pirce">
                                                <span class="won">₩</span>
                                                <span class="cost">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="line0 tools">
                                <th>TOOLS</th>
                                <td>
                                    <div class="select">
                                        <div>
                                            <p class="txt_details"></p>
                                            <div class="pirce">
                                                <span class="won">₩</span>
                                                <span class="cost">0</span>
                                            </div>
                                        </div>
                                        <div>
                                            <p class="txt_details"></p>
                                            <div class="pirce">
                                                <span class="won">₩</span>
                                                <span class="cost">0</span>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr class="line0">
                                <th> </th>
                                <td>
                                    <div class="total">
                                        <div>
                                            <p class="txt_details">Total</p>
                                            <div class="pirce">
                                                <span class="won">₩</span>
                                                <span class="cost">0</span>
                                            </div>
                                        </div>
                                        <p class="vat">(VAT 별도)</p>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </li>
            </ol>
            <div class="estimate">
                <h3>인공인간 제작을 의뢰합니다.</h3>

                <div class="estimate_table">
                    <div class="estimate_tr">
                        <div class="estimate_th">
                            <b>이름</b>(필수)
                        </div>
                        <div class="estimate_td">
                            <input type="text" placeholder="이름을 입력해 주세요." autocomplete="off">
                        </div>
                    </div>
                    <div class="estimate_tr">
                        <div class="estimate_th">
                            <b>연락처</b>(필수)
                        </div>
                        <div class="estimate_td">
                            <input type="number" name="tel" class="mandantory" placeholder="‘-’없이 연락처를 입력해 주세요." title="연락처" maxlength="12" oninput="maxLengthCheck(this)" autocomplete="off">
                        </div>
                    </div>
                    <div class="estimate_tr">
                        <div class="estimate_th">
                            <b>이메일</b>(필수)
                        </div>
                        <div class="estimate_td">
                            <input type="email" name="email" class="mandantory" placeholder="이메일 주소를 입력해 주세요." title="이메일" autocomplete="off">
                        </div>
                    </div>
                    <div class="estimate_tr">
                        <div class="estimate_th">
                            <b>회사/소속</b>
                        </div>
                        <div class="estimate_td">
                            <input type="text" placeholder="회사명이나 소속단체를 입력해주세요." autocomplete="off">
                        </div>
                    </div>
                    <div class="estimate_tr">
                        <div class="estimate_th">
                            <b>연락가능 시간대</b>
                        </div>
                        <div class="estimate_td">
                            <input type="text" placeholder="ex)저녁7시 이후" autocomplete="off">
                        </div>
                    </div>
                </div>

                <div class="rd_area">
                    <div class="rd_cont">
                        <input type="checkbox" id="chAgree" name="chAgreeBtn" value="chAgree" />
                        <label for="chAgree"><span></span></label>
                    </div>
                    <div>
                        <strong>동의합니다.</strong>
                        <p>수집하는 개인정보 항목 : 이름, 연락처<br>
                            작성해주시는 개인 정보는 문의 접수 및 고객 불만 해결을 위해 1년간 보관됩니다.<br>
                            본 동의를 거부할 수 있으나, 미동의 시 견적서 접수가 불가능합니다.</p>
                    </div>
                </div>

                <button class="send">견적서 접수</button>
            </div>
        </div>
    </div>

    <!-- #footer -->
    <%@ include file="../common/footerLanding.jsp" %>
</div>


<!-- script -->
<%--<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/aibuilder/js/jquery-1.11.2.min.js"></script>--%>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/aibuilder/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/aibuilder/js/html5div.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/aibuilder/js/html5shiv.min.js"></script>






<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>


<script type="text/javascript">
	//scroll event
	var wrapOffset = $('#wrap').offset();

	if ($(document).scrollTop() > wrapOffset.top) {
		$('#wrap').addClass('transform');
	} else {
		$('#wrap').removeClass('transform');
	}

	$(window).scroll(function() {
		var scrollLocate = $(window).scrollTop();

		$('.maumUI.transform .maum_sta').css('left', 0 - $(this).scrollLeft());

		if ($(document).scrollTop() > wrapOffset.top) {
			$('#wrap').addClass('transform');
		} else {
			$('#wrap').removeClass('transform');
		}
		if (scrollLocate > 50) {
			$('#header').addClass('transform');
		}
		if (scrollLocate < 50) {
			$('#header').removeClass('transform');
		}
	});
</script>
<script>
    var imgNum = 25;
    var img = new Image();
    var seq_play = true;
    var canvas = document.getElementById('screen1');
    var ctx = canvas.getContext('2d');

    img.src = "/aiaas/aibuilder/images/Parallax9/Parallax9_00025.png";

    playSequence();

    function playSequence() {
        var timer = setInterval(function() {
            if(seq_play){
                if (imgNum == 150) {
                    imgNum = 25;
                }
                imgNum++;
            }
            player(imgNum);
        }, 40);
    }

    function player(num) {
        if( num < 100 ){
            num = "0" + num;
        }
        img.src = "/aiaas/aibuilder/images/Parallax9/Parallax9_00" + num + ".png";
    }

    img.addEventListener('load' ,function(e) {
        ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
        ctx.drawImage(img, 0, 0);
    });

    function maxLengthCheck(object){
        if (object.value.length > object.maxLength){
            object.value = object.value.slice(0, object.maxLength);
        }
    }


    function email_check( email ) {
        var regex=/([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        return (email != '' && email != 'undefined' && regex.test(email));
    }

    document.getElementsByClassName("send")[0].addEventListener("click", function(){
        var email = document.querySelector("input[type=email]").value;
        if( email == '' || email == 'undefined') return;
        if(! email_check(email) ) {
            alert("이메일 형식으로 적어주세요.")
            document.querySelector("input[type=email]").focus();
            return false;
        }
    })

    $(document).ready(function(){
        $(".accordion .acd_title").click(function() {
            $(this).toggleClass("selected");
            $(".accordion .acd_title").not(this).removeClass("selected");
            var target = $(this).siblings(".acd_cnt");
            var other = $(".accordion .acd_title").not(this).parents(".accordion").find(".acd_cnt");
            target.stop().slideToggle(600);
            other.stop().slideUp(600);
        });

        var jobSwiper = new Swiper(".job_slider", {
            slidesPerView: 5,
            spaceBetween: 20,
        });

        var whoSwiper = new Swiper(".who_slider", {
            slidesPerView: 5,
            spaceBetween: 20,
        });

        var displaySwiper = new Swiper(".display_slider", {
            slidesPerView: 5,
            spaceBetween: 20,
        });

        $("input[name='rdJobBtn']:radio, input[name='rdWhoBtn']:radio, input[name='rdDispBtn']:radio").change(function () {
            $(this).parents(".accordion").addClass("on");
        });

        $("input[name='chkWorkBtn']:checkbox").change(function () {
            $(this).parents(".accordion").addClass("on");
            $(".tools_accd").addClass("on");
            if( $("input[name='chkWorkBtn']:checked").length == 0 ){
                $(this).parents(".accordion").removeClass("on");
                $(".tools_accd").removeClass("on");
            }
        });
    });


    let req = {
        method: 'POST',
        headers: new Headers({"Content-Type": "application/json", '${_csrf.headerName}': '${_csrf.token}'}),
        body: JSON.stringify({'text': "안녕하세요"}),
        mode: 'cors',
        cache: 'default'
    };
    getJobList();
    getActorList();

    getWorkList();
    getDisplayList();
    getToolsList();

    $("button.send").on("click", function() {
        sendMail($(this));
    });


    function numberWithCommas(x) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    function sendMail($this) {
        if(validateInputCheck()) {
            sendReceipt();
        }
    }
    function validateInputCheck() {
        let inputList = $("input.mandantory");
        for(let i=0; i<inputList.length; i++) {
            let item = inputList[i];
            let text = $(item).val();
            let title = $(item).attr("title");
            if(text == null || text.trim().length == 0) {
                alert("필수값을 입력해 주세요. ("+title+")");
                return false;
            }
        }
        if(!$("#chAgree").is(":checked")) {
            alert("견적서 요청을 보내려면 먼저 개인정보 수집동의에 체크해 주세요.");
            return false;
        }
        return true;
    }
    function getJobList() {
        $.ajax({
            url		:"/recept/estimate/getJobList",
            cache   : false,
            async   : true,
            type	:"POST",
            dataType: "JSON",
            success : function(result) {
                if(result != null) {
                    let jobListHtml = $.templates("#jobTemplate").render(result);
                    let $jobList = $("div.job_slider > .swiper-wrapper").html(jobListHtml);
                    $jobList.find("div.rd_cont").on("click", function() {
                        let jobCode = $(this).find("input").val();
                        checkGroupBtn($(this));

                        getWorkByJobId(jobCode);
                        getDeviceByJobId(jobCode);
                        setDetails("job", $(this));
                    });
                }
            },
            error : function(xhr, status, error) {
            }
        });
    }
    function getWorkByJobId(jobId) {
        $.ajax({
            url		:"/recept/estimate/getWorkByJobId",
            cache   : false,
            async   : true,
            type	:"POST",
            dataType: "JSON",
            data : { "jobId" : jobId },
            success : function(result) {
                if(result != null) {
                    let workflowHtml = $.templates("#workflowTemplate").render(result);
                    let $workflowList = $("div.work_cnt").html(workflowHtml);
                    $workflowList.find("div.chk_round").on("click", function() {
                        let workflowCode = $(this).find("input").val();
                        checkGroupBtn($(this));

                        setDetails("workflow", $(this));
                        getToolsByWorkId();
                    });
                }
            },
            error : function(xhr, status, error) {
            }
        });
    }
    function getDeviceByJobId(jobId) {
        $.ajax({
            url		:"/recept/estimate/getDisplayByJobId",
            cache   : false,
            async   : true,
            type	:"POST",
            dataType: "JSON",
            data : { "jobId" : jobId },
            success : function(result) {
                if(result != null) {
                    console.log(result);
                    // let displayListHtml = $.templates("#displayTemplate").render(result);
                    // let $displayList = $("div.display_slider > .swiper-wrapper").html(displayListHtml);
                    // $displayList.find("div.rd_cont").on("click", function() {
                    //     let diplayCode = $(this).find("input").val();
                    //     checkGroupBtn($(this));
                    //
                    //     setDetails("device", $(this));
                    // });
                    let displayListHtml = $.templates("#displayTemplate").render(result);
                    let $displayList = $("div.display_slider > .swiper-wrapper").html(displayListHtml);
                    $displayList.find("div.rd_cont").on("click", function() {
                        let diplayCode = $(this).find("input").val();
                        checkGroupBtn($(this));

                        setDetails("device", $(this));
                    });
                }
            },
            error : function(xhr, status, error) {
            }
        });
    }
    function getActorList() {
        $.ajax({
            url		:"/recept/estimate/getActorList",
            cache   : false,
            async   : true,
            type	:"POST",
            dataType: "JSON",
            success : function(result) {
                if(result != null) {
                    let actorListHtml = $.templates("#actorTemplate").render(result);
                    let $actorList = $("div.who_slider > .swiper-wrapper").html(actorListHtml);
                    $actorList.find("div.rd_cont").on("click", function() {
                        let actorCode = $(this).find("input").val();
                        checkGroupBtn($(this));

                        setDetails("actor", $(this));
                    });
                }
            },
            error : function(xhr, status, error) {
            }
        });
    }
    function getWorkList() {
        $.ajax({
            url		:"/recept/estimate/getWorkList",
            cache   : false,
            async   : true,
            type	:"POST",
            dataType: "JSON",
            success : function(result) {
                if(result != null) {
                    let workflowHtml = $.templates("#workflowTemplate").render(result);
                    let $workflowList = $("div.work_cnt").html(workflowHtml);
                    $workflowList.find("div.chk_round").on("click", function() {
                        let workflowCode = $(this).find("input").val();
                        checkGroupBtn($(this));
                        setDetails("workflow", $(this));
                    });
                }
            },
            error : function(xhr, status, error) {
            }
        });
    }
    function getToolsByWorkId() {
        let param = {}
        let workIds = selectedWorkIdCodes;

        param.workId = workIds;
        $.ajax({
            url		:"/recept/estimate/getToolsByWorkId",
            cache   : false,
            async   : true,
            type	:"POST",
            dataType: "JSON",
            data : param,
            success : function(result) {
                if(result != null) {
                    let toolsHtml = $.templates("#toolsTemplate").render(result);
                    let $toolList = $("div.tools_cnt").html(toolsHtml);
                    $toolList.find("div.item").on("click", function() {
                        let toolCode = $(this).find("input").val();
                        checkGroupBtn($(this));
                        setDetails("tools", $(this));
                    });
                }
            },
            error : function(xhr, status, error) {
            }
        });
    }
    function getDisplayList() {
        $.ajax({
            url		:"/recept/estimate/getDisplayList",
            cache   : false,
            async   : true,
            type	:"POST",
            dataType: "JSON",
            success : function(result) {
                if(result != null) {
                    let displayListHtml = $.templates("#displayTemplate").render(result);
                    let $displayList = $("div.display_slider > .swiper-wrapper").html(displayListHtml);
                    $displayList.find("div.rd_cont").on("click", function() {
                        let diplayCode = $(this).find("input").val();
                        checkGroupBtn($(this));

                        setDetails("device", $(this));
                    });
                }
            },
            error : function(xhr, status, error) {
            }
        });
    }
    function getToolsList() {
        $.ajax({
            url		:"/recept/estimate/getToolsList",
            cache   : false,
            async   : true,
            type	:"POST",
            dataType: "JSON",
            success : function(result) {
                if(result != null) {
                    let toolsHtml = $.templates("#toolsTemplate").render(result);
                    let $toolList = $("div.tools_cnt").html(toolsHtml);
                    $toolList.find("div.item").on("click", function() {
                        let toolCode = $(this).find("input").val();
                        checkGroupBtn($(this));
                        setDetails("tools", $(this));
                    });
                }
            },
            error : function(xhr, status, error) {
            }
        });
    }

    function checkGroupBtn($this) {
        if($this.parents(".acd_cnt").find("input:checked").size() > 0) {
            $this.parents(".accordion").addClass("on");
        } else {
            $this.parents(".accordion").removeClass("on");
        }
    }




    let selectItems = {};
    let selectedPrice = {};
    let selectedWorkIdCodes = [];
    function setDetails(type, object) {
        let data = {};
        if(type == 'job' || type == 'actor' || type == 'device') {
            data.title = object.find("input").attr("title");
            data.code = object.find("input").attr("value");
            data.price = object.find("input").attr("price");
            data.vprice = numberWithCommas(object.find("input").attr("price"));
            selectItems[type] = data;
            selectedPrice[type] = data.price*1;
        } else if(type == 'workflow' || type == 'tools') {
            let checkedList = object.parents("div").eq(0).find("div.item").find("input:checked");
            let dataList = [];
            let priceTemp = 0;
            if(type == 'workflow') selectedWorkIdCodes = [];
            $.each(checkedList, function(i, value) {
                let data = {};
                data.title = $(value).attr("title");
                data.code = $(value).attr("value");
                data.price = $(value).attr("price");
                data.vprice = numberWithCommas(object.find("input").attr("price"));
                dataList.push(data);
                if(type == 'workflow') selectedWorkIdCodes.push(data.code);
                priceTemp += data.price*1;
            });
            selectItems[type] = dataList;
            selectedPrice[type] = priceTemp;
        }

        let totalPrice = 0;

        for(let key in selectedPrice) {
            totalPrice += selectedPrice[key]*1;
        }
        selectItems["totPrice"] = totalPrice;
        selectItems["viewPrice"] = numberWithCommas(totalPrice);
        let detailHtml = $.templates("#detailTemplate").render(selectItems);
        $("table.orderDetails_cnt").html(detailHtml);
    }

    function sendReceipt() {
        let sendData = {};
        for(let key in selectItems) {
            if(key == 'job') {
                sendData.jobCode = selectItems[key].code;
            }
            else if(key == 'actor') {
                sendData.actorCode = selectItems[key].code;
            }
            else if(key == 'workflow') {
                sendData.workflow = [];
                selectItems[key].forEach(value=> {
                    sendData.workflow.push(value.code);
                });
            }
            else if(key == 'device') {
                sendData.device = selectItems[key].code;
            }
            else if(key == 'tools') {
                sendData.tools = [];
                selectItems[key].forEach(value=> {
                    sendData.tools.push(value.code);
                });
            }
            else if(key == 'totPrice') {
                sendData.totPrice = selectItems[key];
            }
            sendData.name = $("input[name=name]").val();
            sendData.tel = $("input[name=tel]").val();
            sendData.email = $("input[name=email]").val();
            sendData.company = $("input[name=company]").val();
            sendData.time = $("input[name=time]").val();
        }
        // console.log("sendData : "+sendData);
        // console.log("sendData.name : "+sendData.name);
        // console.log("sendData.tel : "+sendData.tel);
        // console.log("sendData.email : "+sendData.email);
        // console.log("sendData.company : "+sendData.company);
        // console.log("sendData.time : "+sendData.time);

        if(sendData.jobCode == null || sendData.jobCode == '') {
            alert("JOB 항목을 선택해 주세요");
            return;
        }
        if(sendData.actorCode == null || sendData.actorCode == '') {
            alert("WHO 항목을 선택해 주세요");
            return;
        }
        if(sendData.device == null || sendData.device == '') {
            alert("DISPLAY 항목을 선택해 주세요");
            return;
        }
        if(sendData.workflow == null || sendData.workflow.length == 0) {
            alert("WORK 항목을 선택해 주세요");
            return;
        }
        if(sendData.tools == null || sendData.tools.length == 0) {
            alert("TOOLS 항목을 선택해 주세요");
            return;
        }
        $.ajax({
            url		:"/recept/estimate/receiptPrice",
            cache   : false,
            async   : true,
            type	:"POST",
            dataType: "JSON",
            data: sendData,
            success : function(result) {
                if(result) {
                    if(confirm("견적서 요청을 완료했습니다.") == true) {
                        location.replace(location.href);
                    } else {
                        location.replace(location.href);
                    }
                } else {
                    alert("견적서 요청에 실패했습니다. 고객센터에 문의 바랍니다.");
                }
            },
            error : function(xhr, status, error) {
            }
        });


    }
</script>
<script type="text/x-jsrender" id="jobTemplate">
<div class="swiper-slide">
    <div class="rd_cnt">
        <div class="rd_cont">
            <input type="radio" id="rdJob{{:jobCode}}" name="rdJobBtn" value="{{:jobCode}}" price="{{:price}}" title="{{:jobName}}" />
            <label for="rdJob{{:jobCode}}"><span></span></label>
        </div>
    </div>
    <div class="slide_cnt_cont">
        <h4 class="tit_txt">{{:jobName}}</h4>
        <p class="tit_txt_en">{{:jobDesc}}</p>
        <img src="${pageContext.request.contextPath}/aiaas/aibuilder{{:imgUrl}}" alt="{{:jobName}}">
        <a href="{{:targetUrl}}" class="btn_detail_link" target="_blank">
            자세히 보기
            <img src="${pageContext.request.contextPath}/aiaas/aibuilder/images/icon_export_16px.svg" alt="내보내기 아이콘">
        </a>
    </div>
</div>
</script>
<script type="text/x-jsrender" id="actorTemplate">
<div class="swiper-slide">
    <div class="rd_cnt">
        <div class="rd_cont">
            <input type="radio" id="rdJob{{:actorCode}}" name="rdWhoBtn" value="{{:actorCode}}" price="{{:price}}" title="{{:actorName}}" />
            <label for="rdJob{{:actorCode}}"><span></span></label>
        </div>
    </div>
    <div class="slide_cnt_cont">
        <h4 class="tit_txt">{{:actorName}}</h4>
        <p class="tit_txt_en">{{:actorDesc}}</p>
        <img class="img_long" src="${pageContext.request.contextPath}/aiaas/aibuilder{{:imgUrl}}" alt="{{:actorName}}">
        <a href="{{:targetUrl}}" class="btn_detail_link" target="_blank">
            자세히 보기
            <img src="${pageContext.request.contextPath}/aiaas/aibuilder/images/icon_export_16px.svg" alt="내보내기 아이콘">
        </a>
    </div>
</div>
</script>
<script type="text/x-jsrender" id="workflowTemplate">
<div class="chk_round item">
    <input type="checkbox" name="chkWorkBtn" id="chkWork{{:workCode}}" value="{{:workCode}}" price="{{:price}}" title="{{:workName}}" {{:enabled != 'Y' ? 'disabled' : ''}}>
    <label for="chkWork{{:workCode}}">{{:workName}}</label>
</div>
</script>
<script type="text/x-jsrender" id="displayTemplate">
<%--<div class="swiper-slide disabled">--%>
<div class="swiper-slide {{:enabled != 'Y' ? 'disabled' : ''}}" style="width: 232px; margin-right: 20px;">
    <div class="rd_cnt">
        <div class="rd_cont">
            <input type="radio" id="rdJob{{:dvcCode}}" name="rdDispBtn" value="{{:dvcCode}}" price="{{:price}}" title="{{:dvcName}}" {{:enabled != 'Y' ? 'disabled' : ''}} />
            <label for="rdJob{{:dvcCode}}"><span></span></label>
        </div>
    </div>
    <div class="slide_cnt_cont">
        <h4 class="tit_txt">{{:dvcName}}</h4>
        <p class="tit_txt_en">{{:dvcDesc}}</p>
        <div class="box_area">
            <p>근접센서</p>
            <p>Edge A1</p>
            <p class="disabled">열화상 카메라</p>
        </div>
        <a href="{{:targetUrl}}" class="btn_detail_link" target="_blank">
            문의하기
            <img src="${pageContext.request.contextPath}/aiaas/aibuilder/images/icon_export_16px.svg" alt="내보내기 아이콘">
        </a>
    </div>
</div>
</script>
<script type="text/x-jsrender" id="toolsTemplate">
<div class="{{:singleYn == 'Y' ? 'rd_cont' : 'chk_box'}} item">
    {{if singleYn == 'Y'}}
    <input type="radio" name="{{:toolGrpCode}}" id="chkTools{{:priority}}" value="{{:toolCode}}" price="{{:price}}" title="{{:toolName}}" {{:enabled != 'Y' ? 'disabled' : ''}}>
    {{else}}
    <input type="checkbox" name="chkToolsBtn" id="chkTools{{:priority}}" value="{{:toolCode}}" price="{{:price}}" title="{{:toolName}}" {{:enabled != 'Y' ? 'disabled' : ''}}>
    {{/if}}
    <label for="chkTools{{:priority}}">{{:toolName}}</label>
</div>
</script>
<script type="text/x-jsrender" id="detailTemplate">
<colgroup>
    <col width="136px;">
    <col width="*">
</colgroup>
<tr class="job">
    <th>JOB</th>
    <td>
        <div class="select">
            <div>
                <p class="txt_details">{{:job != null ? job.title : ''}}</p>
                <div class="pirce">
                    <span class="won">₩</span>
                    <span class="cost">{{:job != null ? job.vprice : 0}}</span>
                </div>
            </div>
        </div>
    </td>
</tr>
<tr class="actor">
    <th>WHO</th>
    <td>
        <div class="select">
            <div>
                <p class="txt_details">{{:actor != null ? actor.title : ''}}</p>
                <div class="pirce">
                    <span class="won">₩</span>
                    <span class="cost">{{:actor != null ? actor.vprice : 0}}</span>
                </div>
            </div>
        </div>
    </td>
</tr>
<tr class="workflow">
    <th>WORK</th>
    <td>
        <div class="select">
            <div>
                <p class="txt_details">
                    {{if workflow != null}}
                        {{for workflow}}
                            {{:title}} /
                        {{/for}}
                    {{/if}}
                </p>
                <div class="pirce">
                    <span class="won">₩</span>
                    <span class="cost">0</span>
                </div>
            </div>
        </div>
    </td>
</tr>
<tr class="device">
    <th>DISPLAY</th>
    <td>
        <div class="select">
            <div>
                <p class="txt_details">{{:device != null ? device.title : ''}}</p>
                <div class="pirce">
                    <span class="won">₩</span>
                    <span class="cost">{{:device != null ? device.vprice : 0}}</span>
                </div>
            </div>
        </div>
    </td>
</tr>
<tr class="line0 tools">
    <th>TOOLS</th>
    <td>
        <div class="select">
            {{if tools != null}}
                {{for tools}}
                <div>
                    <p class="txt_details">[{{:title}}] <span class="fw300">(maum.ai 구독회원 무료)</span></p>
                    <div class="pirce">
                        <span class="won">₩</span>
                        <span class="cost">{{:price}}</span>
                    </div>
                </div>
                {{/for}}
            {{else}}
                <div>
                    <p class="txt_details"></p>
                    <div class="pirce">
                        <span class="won">₩</span>
                        <span class="cost">0</span>
                    </div>
                </div>
            {{/if}}
        </div>
    </td>
</tr>
<tr class="line0 total">
    <th> </th>
    <td>
        <div class="total">
            <div>
                <p class="txt_details">Total</p>
                <div class="pirce">
                    <span class="won">₩</span>
                    <span class="cost">{{:viewPrice}}</span>
                </div>
            </div>
            <p class="vat">(VAT 별도)</p>
        </div>
    </td>
</tr>
</script>