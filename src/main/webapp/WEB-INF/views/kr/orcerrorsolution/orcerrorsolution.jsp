<%--
  Created by IntelliJ IDEA.
  User: password-mindslab
  Date: 2021-11-22
  Time: 오후 3:54
To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<html>
<head>
    <title>AI Builder</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@400;500;700&display=swap" rel="stylesheet">
    <%--    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/all.min.css" />--%>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/orcbuilder.css" />
    <%--    <link rel="stylesheet" href="${pageContext.request.contextPath}/https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">--%>
    <%--    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/swiper-6.2.0.css?ver=<%=fmt.format(lastModifiedStyle)%>">--%>
    <%--    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/maum_landing.css?ver=<%=fmt.format(lastModifiedStyle)%>">--%>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/fontawesome-all.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/font.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/all.min.css" />
</head>

<body class="is-preload">

<img src="${pageContext.request.contextPath}/aiaas/kr/images/orcbuilder/errorsolution1.png" />
<h3>1. 자물쇠 아이콘 클릭</h3>
<h3>2. 사이트 설정 탭 클릭</h3>
<img src="${pageContext.request.contextPath}/aiaas/kr/images/orcbuilder/errorsolution2.png" />
<h3>3. 안전하지 않은 콘텐츠 탭에서 차단으로 설정된 것을 허용</h3>
</body>
</html>