<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">

<!-- .contents -->
<div class="contents">
    <div class="content api_content">
        <h1 class="api_tit">스타일 변환</h1>
        <ul class="menu_lst">
            <li class="tablinks active" onclick="openTap(event, 'txtStyTrans_demo')" id="defaultOpen">
                <button type="button">엔진</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'txtStyTrans_example')">
                <button type="button">적용사례</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'txtStyTrans_menu')">
                <button type="button">매뉴얼</button>
            </li>
        </ul>
        <!--.conv_demo-->
        <div class="demobox demobox_nlu" id="txtStyTrans_demo">
            <p><span>스타일 변환</span></p>
            <span class="sub">문장을 원하는 스타일로 변경하는 동시에 의미는 일관성 있게 전달해줍니다.</span>

            <!--.itf_box-->
            <div class="itf_box correct_box txtStyTrans_box">
                <!--.step01-->
                <div class="step01">
                    <div class="demo_top">
                        <span>Step 1. 예문 선택</span>
                    </div>
                    <div class="ift_topbox">
                        <p>스타일 변환을 진행할 문장을 선택해주세요.</p>
                        <ul class="ift_lst">
                            <li>
                                <div class="radio">
                                    <input type="radio" id="radio" name="sentence" value="내일 저녁으로 삼겹살 어때?"
                                           checked="checked">
                                    <label for="radio">내일 저녁으로 삼겹살 어때?</label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="radio2" name="sentence" value="끝나고 시간이 되면 참석하도록 하겠습니다.">
                                    <label for="radio2">끝나고 시간이 되면 참석하도록 하겠습니다. </label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="radio3" name="sentence" value="user-input">
                                    <label for="radio3"></label>
                                </div>
                                <input type="text" class="iptTxt" placeholder="직접 입력 (최소 5자 이상 입력해 주세요.)">
                                <div class="guide_txt">* 예문보다 쉬운 문장으로 입력해보세요. 예문보다 복잡한 문장은 스타일 변환이 어려울 수 있습니다.</div>
                                <div class="txtCount">(<span>0</span>/80 자)</div>
                            </li>
                        </ul>
                    </div>

                    <div class="btm_box">
                        <div class="fl">
                            <div class="demo_top">Step 2. 스타일 선택</div>
                            <div class="demo_slt_box">
                                <div class="radioBox">
                                    <!-- [D] 성경구절 checked default -->
                                    <input type="radio" id="style01" name="txtStyle" value="bible" checked>
                                    <label for="style01">성경구절</label>
                                    <input type="radio" id="style02" name="txtStyle" value="hashtag">
                                    <label for="style02">해시태그</label>
                                    <input type="radio" id="style03" name="txtStyle" value="twitter">
                                    <label for="style03">트윗</label>
                                    <input type="radio" id="style04" name="txtStyle" value="messenger">
                                    <label for="style04">메신저</label>
                                    <input type="radio" id="style05" name="txtStyle" value="speech">
                                    <label for="style05">연설문</label>
                                    <input type="radio" id="style06" name="txtStyle" value="non_fiction">
                                    <label for="style06">논픽션</label>
                                    <input type="radio" id="style07" name="txtStyle" value="personal_conversation">
                                    <label for="style07">대화</label>
                                    <input type="radio" id="style08" name="txtStyle" value="script">
                                    <label for="style08">대본</label>
                                </div>
                            </div>
                        </div>

                        <div class="fr">
                            <div class="demo_top">Step 3. 문장 수 선택</div>
                            <div class="demo_slt_box">
                                <div class="sltBox">
                                    <select class="select" id="returnSequence">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <!-- [D] 3문장 selected default -->
                                        <option value="3" selected>3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    <em class="fas fa-caret-down"></em>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_transfer" id="btnTransfer">변환 하기</button>
                    </div>
                </div>
                <!--//.step01-->

                <div class="loading_wrap">
                    <p><em class="far fa-file-alt"></em> 스타일 변환중</p>
                    <div class="loding_box">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px"
                             viewBox="0 0 128 16" xml:space="preserve"><path fill="#c9c3ec" fill-opacity="0.42"
                                                                             d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"></path>
                            <g>
                                <path fill="#27c1c1" fill-opacity="1"
                                      d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"></path>
                                <animateTransform attributeName="transform" type="translate"
                                                  values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0"
                                                  calcMode="discrete" dur="1820ms"
                                                  repeatCount="indefinite"></animateTransform>
                            </g></svg>

                        <p>약간의 시간이 소요 됩니다.</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
                    </div>

                </div>

                <!--.step02-->
                <div class="step02">
                    <div class="demo_top">
                        <em class="far fa-file-alt"></em><span>스타일 변환 결과</span>
                    </div>
                    <div class="result_box">
                        <ul>
                            <li>
                                <dl>
                                    <dt><span>입력</span></dt>
                                    <dd>
                                        <p id="inputSentence"></p>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt><span>스타일 변환</span></dt>
                                    <dd id="resultTextarea">
                                        <p></p>
                                    </dd>
                                </dl>
                            </li>
                        </ul>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_reset" id="btn_reset"><em class="fas fa-redo-alt"></em>처음으로
                        </button>
                    </div>
                </div>
                <!--//.step02-->
            </div>
            <!--//.itf_box-->

        </div>
        <!-- .conv_demo -->

        <!--.conv_menu-->
        <div class="demobox" id="txtStyTrans_menu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1) REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2) 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1) Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2) 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3) [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4) 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>

                <div class="guide_group">
                    <div class="title">스타일 변환 <small>(Text Style Transfer)</small></div>
                    <p class="sub_txt">문장을 원하는 스타일로 변경하는 동시에 의미는 일관성 있게 전달해줍니다.</p>

                    <span class="sub_title">준비사항</span>
                    <p class="sub_txt">- Input: 한글 문장</p>

                    <span class="sub_title">실행 가이드</span>

                    <!-- Upload START-->
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/text-style/transfer</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tbody>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId</td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey</td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>originalText</td>
                            <td>스타일 변환 대상 문장</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>numReturnSequences</td>
                            <td>리턴 받을 변환된 문장의 갯수</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>targetStyle</td>
                            <td>변환할 스타일</td>
                            <td>string</td>
                        </tr>
                        </tbody>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
<pre>
curl --location --request POST 'http://api.maum.ai/text-style/transfer' \
--header 'Content-Type: multipart/form-data' \
--data-raw '{
	"apiId": "{발급받은 API ID}",
	"apiKey": "{발급받은 API KEY}",
	"originalText": "내일 저녁으로 삼겹살 어때?",
	"numReturnSequences": "3",
	"targetStyle": "bible"
}’		
</pre>
                    </div>

                    <p class="sub_txt">④ Response 파라미터 설명</p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tbody>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>API 동작여부</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>payload</td>
                            <td>변환된 문장 배열</td>
                            <td>string</td>
                        </tr>
                        </tbody>
                    </table>

                    <span class="table_tit">message: API 동작여부</span>
                    <table>
                        <tbody>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>요청 처리 상태를 설명하는 문자열 (Success/ Fail)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>요청 처리 상태에 대한 status code (0: Success)</td>
                            <td>int</td>
                        </tr>
                        </tbody>
                    </table>

                    <p class="sub_txt">⑤ Response 예제 </p>

                    <div class="code_box">
<pre>
{
	"message": {
		"message": "Success",
		"status": 0
	},
	"payload": [
		"내일 저녁으로 삼겹살을 드시겠습니까",
		"내일 저녁으로 삼겹살을 드시겠습니까?",
		"내일 저녁으로 삼겹살을 먹는 것이 어떠하뇨"
	]
}		
</pre>
                    </div>
                    <!-- Upload END-->
                </div>
            </div>
        </div>
        <!--//conv_menu-->

        <!--.conv_example-->
        <div class="demobox" id="txtStyTrans_example">
            <p><em style="color:#27c1c1;font-weight: 400;">적용사례</em> <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>

            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>유튜버</span>
                            </dt>
                            <dd class="txt">유튜브나 브이로그 등 영상 컨셉에 맞게 콘텐츠 스타일을 변환하여 즉각적으로 반영할 수 있습니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_tst"><span>스타일 변환</span></li>
                                    <li class="ico_tts"><span>TTS</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>가상 비서 <strong>(Virtual Assistant)</strong></span>
                            </dt>
                            <dd class="txt">아바타와 함께 활용하여 아바타 컨셉에 맞는 말투를 구사하도록 도와줍니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_tst"><span>스타일 변환</span></li>
                                    <li class="ico_lipSyncAvt"><span>Lip-Sync Avatar</span></li>
                                    <li class="ico_bot"><span>챗봇</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>개인 블로그</span>
                            </dt>
                            <dd class="txt">다양한 본문들을 개성에 맞는 스타일로 변환하여 개인 블로그에 활용합니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_tst"><span>스타일 변환</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //conversion -->
        </div>
        <!--//.conv_example-->
    </div>
</div>
<!-- //.contents -->

<!---------------------- Local Resources ---------------------->
<!-- Local Script -->
<script type="text/javascript">

    let ajaxXHR = null;

    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {

            initPage();

            $('.iptTxt').on('focus', function () {
                $('#radio3').trigger('click');
            });

            $('.iptTxt').keyup(function (e) {
                var content = $(this).val();
                $('.txtCount span').html(content.length);

                if (content.length > 80) {
                    $(this).val(content.substring(0, 80));
                    $('.txtCount span').html(80);
                }
            });

            $('#btnTransfer').on('click', function () {

                let sentence;
                let txtStyle = $('input[type=radio][name=txtStyle]:checked').val();
                let numReturnSequences = $('#returnSequence option:selected').val();

                if ($('input[type=radio][name=sentence]:checked').val() == 'user-input') {   // 사용자 입력 문장
                    let content = $('.iptTxt').val();
                    if (content.length < 5) {
                        alert('최소 5자 이상 입력해 주세요.');
                        return false;
                    } else {
                        sentence = content;
                    }
                } else {                                                                    // 예시 문장
                    sentence = $('input[type=radio][name=sentence]:checked').val();
                }

                let formData = new FormData();
                formData.append("originalText", sentence);
                formData.append("targetStyle", txtStyle);
                formData.append("numReturnSequences", numReturnSequences);
                formData.append($("#key").val(), $("#value").val());

                sendApiRequest(formData);

                $('.step01').hide();
                $('.loading_wrap').show();
            });

            $('.btn_back1').on('click', function () {
                ajaxXHR.abort();
                $('.loading_wrap').hide();
                $('.step01').show();
            });
            $('.btn_reset').on('click', function () {
                $('.step02').hide();
                $('.step01').show();
            });
        });
    });

    function sendApiRequest(formData) {
        ajaxXHR = $.ajax({
            type: 'POST',
            async: true,
            url: '/api/styleTransfer/styleTransferApi',
            data: formData,
            processData: false,
            contentType: false,
            error: function (error) {
                if (error.status === 0) {
                    return false;
                }
                alert("서버와 연결이 되지 않습니다.\n잠시 후에 다시 이용해주세요.");
                console.dir(error);
                window.location.reload();
            },
            success: function (response) {
                // console.log(response);

                if(response.code === 200) {

                    let strToList = response.data;
                    let resultText = "";
                    for(let i=0 ; i<strToList.length ; i++) {
                        const tempHtml = "<p>" + strToList[i] + "</p>";
                        resultText = resultText + tempHtml;
                    }

                    $('.loading_wrap').hide();
                    $('.step02').show();
                    $('#inputSentence').text(formData.get("originalText"));
                    $('#resultTextarea').html(resultText);
                } else {
                    alert("서버와 연결이 되지 않습니다.\n잠시 후에 다시 이용해주세요.");
                    window.location.reload();
                }
            }
        });
    }

    function initPage() {
        $('.iptTxt').val("");
        $('#inputSentence').text();
        $('#resultTextarea').html("");
    }

    // API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace("active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    document.getElementById("defaultOpen").click();
</script>