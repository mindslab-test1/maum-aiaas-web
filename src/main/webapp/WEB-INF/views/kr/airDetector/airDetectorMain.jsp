<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<body>
<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>이미지 업로드 실패</h5>
            <p>이미지 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
                업로드 되지 않았습니다.</p>
            <span>* 지원가능 파일 확장자: .jpg, .bmp<br>
* JPG 파일을 사용하기를 권고드립니다.<br>
* 이미지 용량 300KB 이하만 가능합니다.</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->
<!-- .contents -->
<div class="contents">
    <!-- [D] 21.09.23 .content 영역 전체 수정 필요합니다. -->
    <!-- .content -->
    <div class="content api_content">
        <h1 class="api_tit">스타일 코멘트</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'avrdemo')" id="defaultOpen">
                <button type="button">엔진</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'avrexample')">
                <button type="button">적용사례</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'avrmenu')">
                <button type="button">매뉴얼</button>
            </li>
        </ul>

        <!-- .avrdemo -->
        <div class="demobox" id="avrdemo">
            <p>
                <span>고령자 소지품 검출</span> <small>(AIR-ObjectDetection)</small>
            </p>
            <span class="sub">이미지 내 고령자 소지품(15종)을 검출할 수 있는 엔진입니다.</span>

            <!--demo_layout-->
            <div class="demo_layout air_eg airDetector">
                <!--avr_1-->
                <div class="avr_1" style="display: block;">
                    <div class="fl_box" opacity="1" style="opacity: 1;">
                        <p><em class="far fa-image"></em><strong>샘플 파일</strong>로 테스트 하기</p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="option" value="1" checked>
                                    <label for="sample1" class="female">
                                        <div class="img_area" id="origin_img">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_airDetector_sample.jpg"
                                                 alt="sample image">
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-image"></em><strong>내 파일</strong>로 해보기</p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-image hidden"></em>
                                <label for="demoFile" class="demolabel">이미지 업로드</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".jpg, .bmp">
                            </div>
                            <ul>
                                <li>* 이미지 지원가능 파일 확장자: jpg, bmp</li>
                                <li>* 검출 대상 : 컵, 펜, 모자, 핸드폰, 양말, 안경, 수건, 지팡이, 신문, 리모컨, 열쇠, 지갑, 담배갑, 약통, 약봉지</li>
                            </ul>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">결과보기</button>
                    </div>
                </div>
                <!--avr_1-->

                <!--avr_2-->
                <div class="avr_2" style="display: none;">
                    <p><em class="far fa-image"></em>이미지 분석중</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px"
                             viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42"
                                                                             d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"></path>
                            <g>
                                <path fill="#f7778a" fill-opacity="1"
                                      d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"></path>
                                <animateTransform attributeName="transform" type="translate"
                                                  values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0"
                                                  calcMode="discrete" dur="1820ms"
                                                  repeatCount="indefinite"></animateTransform>
                            </g></svg>

                        <p>이미지를 분석하는 데 약간의 시간이 소요됩니다. (최대 5분)</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
                    </div>

                </div>
                <!--avr_2-->

                <!--avr_3-->
                <div class="avr_3" style="display: none;">
                    <div class="origin_file">
                        <p><em class="far fa-image"></em>입력 파일</p>
                        <div class="imgBox">
                            <img id="input_img" src="" alt="원본 이미지">
                        </div>
                    </div>
                    <div class="result_file" >
                        <p><em class="far fa-file-image"></em>결과 파일</p>
                        <div class="imgBox">
                            <img id="output_img" src="" alt="결과 이미지" />
                        </div>
                        <a id="save" onclick="downloadResultImg();" class="btn_dwn"><em class="far fa-arrow-alt-circle-down"></em> 결과 파일 다운로드</a>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
                    </div>
                </div>
                <!--avr_3-->
            </div>
            <!--// demo_layout-->

            <div class="engineInfo">
                <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_air.png" alt="AIR 로고">
                <img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_etri.png" alt="ETRI 로고">
                <p>해당 엔진은 대한민국 과학기술정보통신부가 지원하는 정부 R&D 과제 &ldquo;고령 사회에 대응하기 위한 실환경 휴먼케어로봇 기술 개발(AIR)&rdquo; 중
                    한국전자통신연구원(ETRI)의 연구과제 성과물 입니다.</p>
            </div>
        </div>
        <!-- //.avrdemo -->

        <!--.avrmenu-->
        <div class="demobox vision_menu" id="avrmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">API 공통 가이드</div>

                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>

                <div class="guide_group">
                    <div class="title">고령자 소지품 검출 <small>(AIR-ObjectDetection)</small></div>

                    <p class="sub_txt">이미지 내 고령자 소지품(15종)을 검출할 수 있는 엔진입니다.</p>
                    <span class="sub_title">준비사항</span>
                    <p class="sub_txt">- Input: 이미지 파일</p>
                    <ul>
                        <li>확장자 : .jpg, .bmp</li>
                        <li>용량 : 300KB 이하</li>
                    </ul>
                </div>
            </div>
        </div>
        <!--//avrmenu-->

        <!--.avrexample-->
        <div class="demobox" id="avrexample">
            <p><em style="color:#f7778a;font-weight: 400;">적용사례</em> <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>

            <pre>
                       {
                       "result_file": "blob:http://localhost:8080/da73720f-bced-41ed-9f4f-611850f83ab7"
                       }
                       </pre>

            <!-- 소지품 검출 -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>소지품 검출</span>
                            </dt>
                            <dd class="txt">이미지 내 고령자의 소지품을 검출할 수 있습니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_airDtct"><span>고령자 소지품 검출</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //소지품 검출 -->
        </div>
        <!--//.avrexample-->
    </div>
    <!-- //.content -->
</div>
<!-- //.contents -->
</body>

<script type="text/javascript">


    //파일명 변경
    document.querySelector("#demoFile").addEventListener('change', function (ev) {

        $('.fl_box').on('click', function(){
            $(this).css("opacity", "1");
            $('.avr_1 .btn_area .disBox').remove();
        });

        //이미지 용량 체크
        var demoFileInput = document.getElementById('demoFile');
        var demoFile = demoFileInput.files[0];
        var demoFileSize = demoFile.size;
        var max_demoFileSize = 1024 * 300;//1kb는 1024바이트
        var demofile = document.querySelector("#demoFile");

        if(demoFileSize > max_demoFileSize || !demofile.files[0].type.match(/image.*/)){
            $('.pop_simple').show();
            // 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
            $('.fl_box').css("opacity", "0.5");
            $('#demoFile').val('');
            $('.avr_1 .btn_area').append('<span class="disBox"></span>');

        }else{
            document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
            var element = document.getElementById( 'uploadFile' );
            element.classList.remove( 'btn' );
            element.classList.add( 'btn_change' );
            $('.fl_box').css("opacity", "0.5");
            $('.avr_1 .btn_area .disBox').remove();
        }

    });


    var sampleImage;
    var data;

    jQuery.event.add(window,"load",function(){
        function loadSample() {
            var blob = null;
            var xhr = new XMLHttpRequest();
            xhr.open("GET", "${pageContext.request.contextPath}/aiaas/common/images/img_airDetector_sample.jpg");
            xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
            xhr.onload = function()
            {
                blob = xhr.response;//xhr.response is now a blob object
                sampleImage = new File([blob], "img_airDetector_sample.jpg");

                var imgSrcURL = URL.createObjectURL(blob);
                var airDetect_output=document.getElementById('input_img');
                airDetect_output.setAttribute("src",imgSrcURL);

                var output=document.getElementById('output_img');
                output.setAttribute("src",imgSrcURL);

            };
            xhr.send();
        }



        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#input_img').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function (){
            loadSample();


            $("#demoFile").change(function(){
                readURL(this);
            });

            //결과보기 버튼 누를 때
            $('#sub').on('click',function (){
                var demoFileTxt = $('#demoFile').val(); //파일을 추가한 input 박스의 값

                if(demoFileTxt == ""){
                    var demoFile;
                    var option = $("input[type=radio][name=option]:checked").val();
                    if(option == 1){
                        loadSample();
                        demoFile = sampleImage;
                    }
                    //샘플전송
                    var formData = new FormData();

                    formData.append('file',demoFile);
                    formData.append('${_csrf.parameterName}', '${_csrf.token}');

                    var request = new XMLHttpRequest();
                    //
                    request.responseType ="blob";
                    request.onreadystatechange = function(){
                        if (request.readyState === 4){
                            var blob = request.response;
                            var imgSrcURL = URL.createObjectURL(blob);

                            var airDetect_output=document.getElementById('output_img');
                            airDetect_output.setAttribute("src",imgSrcURL);
                            $("#result_file").attr("src", imgSrcURL);

                            data = new Blob([request.response], {type:'image/jpeg'});

                            $('.avr_1').hide();
                            $('.avr_2').hide();
                            $('.avr_3').fadeIn(300);
                        }
                    };
                    request.open('POST', 'https://182.162.19.14:9952/airDetect');
                    request.send(formData);

                    $('.avr_1').hide();
                    $('.avr_2').fadeIn(300);
                }
                else {
                    var demoFileInput = document.getElementById('demoFile');
                    var demoFile = demoFileInput.files[0];
                    var formData = new FormData();

                    formData.append('file',demoFile);
                    formData.append('${_csrf.parameterName}', '${_csrf.token}');

                    var request = new XMLHttpRequest();
                    request.responseType ="blob";
                    request.onreadystatechange = function(){
                        if (request.readyState === 4){
                            var blob = request.response;
                            var imgSrcURL = URL.createObjectURL(blob);

                            //팝업 원본
                            var origin_img= $('#input_img').attr('src');
                            $("#origin_img").attr("src", origin_img);

                            //결과
                            var airDetect_output=document.getElementById('output_img')
                            airDetect_output.setAttribute("src",imgSrcURL);
                            //결과 (팝업)
                            $("#result_file").attr("src", imgSrcURL);
                            data = new Blob([blob], {type:'image/jpeg'});


                            $('.avr_1').hide();
                            $('.avr_2').hide();
                            $('.avr_3').fadeIn(300);
                        }
                    };
                    request.open('POST', 'https://182.162.19.14:9952/airDetect');
                    request.send(formData);

                    $('.avr_1').hide();
                    $('.avr_2').fadeIn(300);
                }
            });


            // step1 (close button)
            $('em.close').on('click', function () {
                $('.fl_box').css("opacity", "1");
                $(this).parent().removeClass("btn_change");
                $(this).parent().addClass("btn");
                $(this).parent().children('.demolabel').text('이미지 업로드');
                $('#demoFile').val('');
                //파일명 변경
                document.querySelector("#demoFile").addEventListener('change', function (ev) {
                    document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
                    var element = document.getElementById( 'uploadFile' );
                    element.classList.remove( 'btn' );
                    element.classList.add( 'btn_change' );
                });
            });

            // step2->step3
            $('.btn_back1').on('click', function () {
                $('.avr_2').hide();
                $('.avr_1').fadeIn(300);
                $('.fl_box').css("opacity", "1");
            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('.avr_3').hide();
                $('.avr_1').fadeIn(300);
                $('.fl_box').css("opacity", "1");
                var label_change = $('.demolabel');
                label_change.text('이미지 업로드');
                label_change.parent().removeClass('btn_change');
                label_change.parent().addClass('btn');
                $('#demoFile').val('');
            });

            // 결과파일 팝업 (새창)
            $('.result_check').on('click', function () {
                $('.pop_confirm').fadeIn(300);

            });

            // 결과 팝업창 닫기
            $('.close, .pop_bg').on('click', function () {
                $('.pop_confirm').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
        });
    });

    function downloadResultImg(){
        var img = document.getElementById('output_img');
        var link = document.getElementById("save");
        link.href = img.src.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
        link.download = "airDetector_IMAGE.JPG";
    }

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }
    document.getElementById("defaultOpen").click();

</script>