<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-05-21
  Time: 15:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>



<%@ include file="../common/common_header.jsp" %>


<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->



<!-- #container -->
<div id="wrap" class="user_container maumUI">
    <%@ include file="../common/header.jsp" %>
    <!-- .contents -->
    <div class="contents">
        <!--content-->
        <div class="content">
            <h1>프로필</h1>
            <!-- .mypageBox -->
            <div class="mypageBox">
                <div class="contentarea">
                    <div class="basic_tit">
                        <!--						<img src="resources/images/img_profile.png" alt="profile img">-->
                        <form id="form" runat="server">
                            <div class="imginput">
                                <img id="image_section" src="${pageContext.request.contextPath}/aiaas/kr/images/img_profile.png" alt="your image" />
                                <!-- 									<label for="imgInput" class="file_label">사진 수정</label>
                                                                    <input type="file" id="imgInput" class="upload-hidden" >  -->
                            </div>
                            <!--
                                                            <label for="imgInput" class="file_label">파일선택</label>
                                                            <input type="file" id="imgInput" class="upload-hidden" >  -->
                        </form>
                        <!-- <input type="text" placeholder="Mindslab" class="username"> -->
                    </div>
                    <div class="basic_info">
                        <form id="memberForm" name="memberForm" method="post">
                            <input type="hidden" id="userEmail" name="email" value="${fn:escapeXml(sessionScope.accessUser.email)}" />
                            <input type="hidden" id="category2" name="category2" value="kr" />
                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                            <h5>기본 정보</h5>
                            <ul>

                                <li>
                                    <span>이메일</span>
                                    <em>${fn:escapeXml(sessionScope.accessUser.email)}</em>
                                </li>
                                <li>
                                    <span>이름</span>
                                    <input type="text" id="name" name="name"  value="" maxlength="20" placeholder="이름" class="username">
                                </li>
                                <li>
                                    <span>연락처</span>
                                    <input type="tel" pattern="[0-9]{3}-[0-9]{4}-[0-9]{4}" id="phone" name="phone" value="" class="userTel">
                                </li>
                                <li>
                                    <span>회사/소속</span>
                                    <input type="text" id="company" name="company"  value="" maxlength="40" >
                                </li>
                            </ul>

                            <span class="checks">
                                <input type="checkbox" id="marketingAgree" name="marketingAgree">
                                <label for="marketingAgree">
                                    <em class="far fa-check-circle"></em> 마음 AI 최신 정보 및 이벤트 알림 메일을 수신합니다.
                                </label>
                            </span>
                        </form>
                    </div>
                    <div class="btn">
                        <a onclick="setUpdate();" class="btn_profile_save">저장</a>
                    </div>
                </div>
            </div>

        </div>
        <!--content-->

    </div>
    <!-- //.contents -->

</div>



<script type="text/javascript">

    $(document).ready(function(){
        getMemberDetail();
    });

    /** 계정 - 상세 조회  */
    function getMemberDetail(){
        let email = $("#userEmail").val();

        if(email != ""){
            $.ajax({
                url	  : "/member/getDetail",
                data    : $("#memberForm").serialize(),
                dataType: "JSON",
                cache   : false,
                async   : true,
                type	: "POST",
                success : function(obj) {
                    getMemberDetailCallback(obj);
                },
                error 	: function(xhr, status, error) {}

            });
        }
    }

    /** 계정 - 상세 조회  콜백 함수 */
    function getMemberDetailCallback(obj){

        if(obj != null){
            let name 			= obj.name;
            let company 		= obj.company;
            let phone           = obj.phone;
            let marketingAgree  = obj.marketingAgree;

            $("#name").val(name);
            $("#company").val(company);
            $("#phone").val(phone);
            $("#marketingAgree").val(marketingAgree);

            if(marketingAgree === 1){
                $("#marketingAgree:checkbox").attr("checked", true);
            } else {
                $("#marketingAgree:checkbox").attr("checked", false);
            }
        }
    }


    /** 프로필 저장 */
    function setUpdate(){

        let name = $("#name").val();

        if(name == ""){
            alert("이름을 입력해주세요.");
            $("#name").focus();
            return;
        }

        let marketingAgree = $('#marketingAgree').is(":checked");
        if(marketingAgree === true) {
            $("#marketingAgree").val(1);
        } else {
            $("#marketingAgree").val(0);
        }

        let yn = confirm("저장하시겠습니까?");
        if(yn){

            $.ajax({
                type	: "POST",
                url  	: '/member/setUpdate',
                data    : $("#memberForm").serialize(),
                dataType: "JSON",
                cache   : false,
                async   : true,
                success : function(obj) {
                    alert("프로필 정보가 저장 되었습니다.");
                    setUpdateCallback(obj);
                    window.location.reload();
                },
                error 	: function(xhr, status, error) {}
            });
        }
    }

    function setUpdateCallback(obj){

        if(obj != null){

            let result = obj.result;

            if(result !== "SUCCESS"){
                alert("저장 실패하였습니다.");
            }
        }
    }

</script>