<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-05-21
  Time: 16:08
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="console.maum.ai.member.constant.UserStatus"%>

<%@ include file="../common/common_header.jsp" %>

<c:set var="userStatus" value="${requestScope['userStatus']}"/>
<c:choose>
    <c:when test="${fn:containsIgnoreCase(userStatus, 'INIT')}">
        <c:import url="/payment/billingForm?method=billingFreeRegistered"/>
    </c:when>
    <c:otherwise>
        <c:import url="/payment/billingForm?method=billingChange"/>
    </c:otherwise>
</c:choose>

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<!-- .plan_oklayer -->
<div class="lyr_plan plan_oklayer">
    <div class="lyr_plan_bg"></div>
    <!-- .lyrWrap -->
    <div class="lyrWrap">
        <button class="btn_lyrWrap_close" type="button">제품 리스트 닫기</button>
        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <em class="fas fa-briefcase"></em>
            <p>플랜 변경</p>
            <span>플랜 변경이 완료되었습니다.</span>
            <button id="planChangeResult" class="btn_blue">OK</button>
        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.lyrWrap -->
</div>
<!-- //.plan_oklayer -->

<!-- .lyr_planconf -->
<div class="lyr_planconf">
    <div class="lyr_plan_bg"></div>
    <!-- .lyrWrap -->
    <div class="lyrWrap">
        <button class="btn_lyrWrap_close" type="button">
            <em class="fas fa-times"></em>
        </button>
        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <div class="lyr_conts">
                <div class="contBox step01 current">
                    <strong>마음AI 서비스 구독을 해지하시겠습니까?</strong>
                    <p>구독이 취소되더라도 구독 결제를 소급하여 환불하지 않으며 이전에 <br>청구된 구독 요금은 취소일에 비례하여 계산되지 않습니다. <br>중도 해지 시 환불은 없고, <br>다음 카드 결제일 전날까지 서비스 이용은 유지됩니다. </p>
                    <strong>그래도 해지 하시겠습니까?</strong>
                    <div class="btnBox">
                        <button type="button" class="btn_cancle">아니오</button>
                        <button type="button" class="btn_unscribe">네</button>
                    </div>
                </div>
                <div class="contBox step02">
                    <strong>더 나은 서비스를 만들 수 있도록 의견을 남겨주세요.</strong>
                    <p>사용 중인 서비스를 선택해 주세요. (중복 선택 가능)</p>
                    <div class="svc_slt_box">
                        <div class="checkBox">
                            <input type="checkbox" id="cloudApi" class="srv" value="Cloud API">
                            <label for="cloudApi">Cloud API</label>
                        </div>
                        <div class="checkBox">
                            <input type="checkbox" id="aiBuilder" class="srv" value="AI Builder">
                            <label for="aiBuilder">AI Builder</label>
                        </div>
                        <div class="checkBox">
                            <input type="checkbox" id="maumMinutes" class="srv" value="maum 회의록">
                            <label for="maumMinutes">maum 회의록</label>
                        </div>
                        <div class="checkBox">
                            <input type="checkbox" id="fast" class="srv" value="FAST 대화형 AI">
                            <label for="fast">FAST 대화형 AI</label>
                        </div>
                        <div class="checkBox">
                            <input type="checkbox" id="ava" class="srv" value="AVA">
                            <label for="ava">AVA</label>
                        </div>
                        <div class="checkBox">
                            <input type="checkbox" id="edgeAi" class="srv" value="Edge AI platform">
                            <label for="edgeAi">Edge AI</label>
                        </div>
                        <div class="checkBox">
                            <input type="checkbox" id="maumData" class="srv" value="maum DATA">
                            <label for="maumData">maum DATA</label>
                        </div>
                        <div class="checkBox">
                            <input type="checkbox" id="maumAcademy" class="srv" value="마음 아카데미">
                            <label for="maumAcademy">마음 아카데미</label>
                        </div>
                        <div class="checkBox">
                            <input type="checkbox" id="slt_none" value="없음">
                            <label for="slt_none">없음</label>
                        </div>
                    </div>
                    <p>구체적인 해지 이유를 입력해주세요.</p>
                    <div class="textarea">
                        <textarea id="unsubDesc"></textarea>
                    </div>
                    <div class="btnBox">
                        <button type="button" class="btn_cancle">취소</button>
                        <button type="button" id="terminateBtn" class="">구독 해지</button>
                    </div>
                </div>
                <div class="contBox step03">
                    <strong>마음AI 서비스 구독 해지가 정상적으로 완료 되었습니다.</strong>
                    <p>다음 카드 결제일 전날까지 서비스 이용은 유지 됩니다.<br>첫 1달 Free 사용자의 경우, 가입 해지 시 모든 서비스가 즉시 중단 됩니다.</p>
                    <div class="btnBox">
                        <a href="https://maum.ai" class="btn_move_home">홈으로</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.lyrWrap -->
</div>
<!-- //.lyr_planconf -->

<div class="lyr_receipt">
    <div class="lyr_plan_bg"></div>
    <!-- .productWrap -->
    <div class="lyrWrap">
        <button class="btn_lyrWrap_close" type="button">제품 리스트 닫기</button>
        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_receipt.png" alt="receipt" class="receiptPop">
            <p>영수증</p>
            <ul>
                <li><span>날짜</span><strong id="receipt_date"></strong></li>
                <li><span>제품</span><strong id="receipt_good"></strong></li>
                <li><span>결제카드</span><strong id="receipt_method"></strong></li>
                <li><span>결제구분</span><strong>일시불</strong></li>
                <li><span>승인번호</span><strong id="receipt_pano"></strong></li>
                <li><span>가격</span><strong class="price" id="receipt_price"></strong></li>
            </ul>
            <button id="receiptBtn" class="btn_blue">확인</button>

        </div>
        <!-- //.lyr_bd -->

    </div>
    <!-- //.productWrap -->
</div>
<!-- //.lyr_receipt -->
<!-- .plan_change -->
<div class="lyr_plan plan_change" id="unsubscribeComplete">
    <div class="lyr_plan_bg"></div>
    <!-- .lyrWrap -->
    <div class="lyrWrap">
        <button class="btn_lyrWrap_close" type="button">제품 리스트 닫기</button>
        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <em class="fas fa-check"></em>
            <p>멤버십 해지</p>
            <span>멤버십 해지가 완료되었습니다.</span>
            <button class="btn_blue" id="completBtn">OK</button>
        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.lyrWrap -->
</div>
<!-- //.plan_change -->

<!-- #container -->
<div id="wrap" class="user_container maumUI">
    <%@ include file="../common/header.jsp" %>
    <!-- .contents -->
    <div class="contents">
        <div class="content">
            <h1>결제 정보</h1>
            <!--.mypageBox-->
            <div class="mypageBox">
                <div class="account">
                    <div class="stn_1">
                        <ul>
                            <li><span>플랜</span><em id="plan_name">BUSINESS</em>
                                <%--									<button type="button" class="changePlan" id="changePlanBtn">변경</button>--%>
                                <%--									<span class="unsubscribe" style="cursor:pointer;margin:0 0 0 20px;font-weight: 300;">구독해지</span>--%>
                                <button type="button" class="unsubscribe">구독해지</button>
                            </li>
                            <li><span>가격</span><strong id="plan_price">&#8361; 0</strong> / 1 개월</li>
                            <li><span>결제 정보</span><em id="plan_pay_info" ></em><button type="button" class="changeMethod">변경</button></li>
                            <li><span id="nextBillingTitle">다음 결제정보</span><em id="nextBilling"><em id="nextBillingDate"></em><em>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</em><em id="nextBillingGoodName"></em><em>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</em><em id="nextBillingPrice"></em></em></li>
                        </ul>
                    </div>
                    <div class="stn_2">
                        <p>결제 내역</p>
                        <table class="payment_tbl">
                            <colgroup>
                                <col width="25%"><col width="25%"><col width="16%"><col width="16%"><col width="14%">
                            </colgroup>
                            <thead>
                            <tr class="thead">
                                <th scope="col">사용 기간</th>
                                <th scope="col">결제 정보</th>
                                <th scope="col">가격</th>
                                <th scope="col">결제일</th>
                                <th scope="col">영수증</th>
                            </tr>
                            </thead>
                            <tbody id="list_body">
                            </tbody>
                        </table>
                        <!-- 페이징 -->
                        <%--							<div class="pageing">--%>
                        <%--								<a href="#" class="first">처음페이지</a>--%>
                        <%--								<a href="#" class="prev">이전페이지</a>--%>
                        <%--								<strong>1</strong>--%>
                        <%--								<a href="#">2</a>--%>
                        <%--								<a href="#">3</a>--%>
                        <%--								<a href="#">4</a>--%>
                        <%--								<a href="#">5</a>--%>
                        <%--								<a href="#">6</a>--%>
                        <%--								<a href="#" class="next">다음페이지</a>--%>
                        <%--								<a href="#" class="end">마지막페이지</a>--%>
                        <%--							</div>--%>
                        <!-- //페이징 -->
                    </div>
                </div>
            </div>
            <!--//account-->
        </div>
    </div>
    <!-- //.contents -->
</div>

<script type="text/javascript">

    var status;
    var paymentList = ${paymentList};
    var nextBillingInfo = ${nextBillingInfo};
    console.dir(nextBillingInfo);

    function cardNoDisplay (cardNo) {
        if(cardNo != null) {
            if(cardNo == "paypalcard"){
                return cardNo;
            }else {
                return cardNo.toString().replace(/(\d|\*){4}/g, "$& ");
            }
        }else {
            return "";
        }
    }

    function priceDisplay (price) {
        return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {

            var payDate;
            var goodName;
            var price;

            // --------------------------------- 현재 결제 정보 dispaly
            var paymentNow = {};
            paymentNow = ${billingInfo};
            console.dir(paymentNow);

            if(paymentNow){
                payDate = paymentNow.payDate;
                goodName = paymentNow.goodName;
            }

            try {
                if(paymentNow.issuerName === '카드없음') {
                    console.log('status = 0');
                    $(".changeMethod").attr('class','subscribe').text('등록');
                    $('#plan_pay_info').text('결제 정보가 없습니다.');
                    $('#nextBillingDate').text(nextBillingInfo);
                    $('#nextBillingGoodName').text('BUSINESS');
                    $('#nextBillingPrice').text('99,000');
                    $("#list_body").append(
                        "<tr><td colspan='5'>결제 내역이 없습니다</td></tr>"
                    )
                } else {
                    $("input[name='plan'][value=" + paymentNow.goodName + "]").attr("checked", "checked");
                    $("#plan_name").html(paymentNow.goodName);
                    if(paymentNow.cardNo == "paypalcard"){
                        console.log('paypalcard');
                        price = "&#36; " + paymentNow.priceUs;
                        $("#plan_price").html(price);
                        $("#plan_pay_info").html("PayPal");
                        $(".changeMethod").remove();
                    }else{
                        $("#plan_price").html("&#8361; " + priceDisplay(paymentNow.price));
                        $("#plan_pay_info").html(paymentNow.issuerName + "카드 " + cardNoDisplay(paymentNow.cardNo));
                        $('#nextBillingPrice').text(priceDisplay(paymentNow.price));
                        if(paymentList[0].payment === 'Free'){
                            console.log('status : 1');
                            $("#list_body").append(
                                "<tr><td colspan='5'>결제 내역이 없습니다</td></tr>"
                            )
                        }else {
                            console.log('status : 2');
                        }
                    }

                    console.dir(paymentList);

                    for (let i = 0; i < paymentList.length; i++){
                        if(paymentList[i].payment != 'Free'){
                            console.log("==============================");
                            console.log(paymentList[i]);
                            console.log("==============================");
                            if(paymentList[i].payment == 'PayPal'){
                                $("#list_body").append(
                                    "<tr>" +
                                    "<td scope='row' >" + paymentList[i].dateFrom + " ~ " + paymentList[i].dateTo + "</td>" +
                                    "<td style=\"text-align: center;\">"+ paymentList[i].cardNo+ "</td>" +
                                    "<td>&#36; " + priceDisplay(paymentList[i].price)+ "</td>" +
                                    "<td>" + paymentList[i].payDate + "</td>" +
                                    "<td><img src='${pageContext.request.contextPath}/aiaas/kr/images/ico_receipt.png' alt='receipt' class='receiptPop' data-pano='"+ paymentList[i].pano +"'></td>" +
                                    "</tr>"
                                )
                            }else{
                                $("#list_body").append(
                                    "<tr>" +
                                    "<td scope='row' >" + paymentList[i].dateFrom + " ~ " + paymentList[i].dateTo + "</td>" +
                                    "<td>" + paymentList[i].issuerName  + "card " + cardNoDisplay(paymentList[i].cardNo) + "</td>" +
                                    "<td>&#8361; " + priceDisplay(paymentList[i].price) + "</td>" +
                                    "<td>" + paymentList[i].payDate + "</td>" +
                                    "<td><img src='${pageContext.request.contextPath}/aiaas/kr/images/ico_receipt.png' alt='receipt' class='receiptPop' data-pano='"+ paymentList[i].pano +"'></td>" +
                                    "</tr>"
                                )
                            }
                        }
                    }
                }
            } catch (err) {
                if (paymentNow == null) {
                    $(".changeMethod").remove();
                    $(".changePlan").text('가격정책');
                }
            }

            $("#nextBillingDate").html(payDate);
            $("#nextBillingGoodName").html(goodName);
            $("#nextBillingPrice").html(price);


            $.ajax({
                url: "/member/getUserStatus",
                data: {"${_csrf.parameterName}": "${_csrf.token}"},
                type: "POST",
                dataType: "JSON",

                success: function (response) {

                    status = response;

                    if (response == ${UserStatus.UNSUBSCRIBING}) {

                        $('.stn_1 .unsubscribe').hide();
                        $('.stn_1 .subscribe').hide();
                        $(".changeMethod").hide();
                        $('#plan_pay_info').text('-');
                        $('#nextBillingTitle').text("구독 만료일");
                        $('#nextBillingGoodName').text('BUSINESS');
                        $('#nextBillingPrice').text('99,000');
                        $('.stn_1 li:eq(0)').append('<button type="button" class="unsubscribe2" disabled>[ 구독 해지 중 ]</button> ');

                        console.log(paymentNow);
                        if(paymentList.length == 0){ //status가 0이었던 사람이 해지 한 경우
                            status = 0;
                            console.log("status가 0 이었던 사람이 해지 한 경우");
                            $("#nextBillingDate").text(nextBillingInfo);
                            $('.stn_1 li:eq(0)').append('<button type="button" id="btn_cancelUnsub" class="cancel_unsub">해지 취소</button>');
                        }else{
                            if(paymentList[0].payment === 'Free'){ //status가 1이었던 사람이 해지 한 경우
                                status = 1;
                                console.log('status가 1 이었던 사람이 해지 한 경우');
                                $("#nextBillingDate").text(nextBillingInfo.payDate);
                                $("#nextBillingGoodName").text(nextBillingInfo.goodName);
                                $("#nextBillingPrice").text(priceDisplay(paymentNow.price));
                                $('.stn_1 li:eq(0)').append('<button type="button" id="btn_cancelUnsub" class="cancel_unsub">해지 취소</button>');
                            }else {//status가 2이었던 사람이 해지 한 경우
                                status = 2;
                                if(paymentNow.cardNo != "paypalcard"){
                                    console.log(paymentNow.cardNo)
                                    $('.stn_1 li:eq(0)').append('<button type="button" id="btn_cancelUnsub" class="cancel_unsub">해지 취소</button>');
                                }
                            }
                        }

                        console.log(status);

                        //해지 취소 버튼
                        $('#btn_cancelUnsub').on("click", function () {
                            console.log(status);
                            $.ajax({
                                url: "/member/updateUserStatus",
                                data: {
                                    status: status,
                                    "${_csrf.parameterName}": "${_csrf.token}"
                                },
                                dataType: "JSON",

                                success: function (respo) {

                                    if (respo == 1) {
                                        alert("구독 해지가 취소 되었습니다.");
                                        $('.unsubscribe2').remove();
                                        $('#btn_cancelUnsub').remove();
                                        $('.stn_1 .unsubscribe').show();

                                        window.location.reload();
                                    } else {
                                        alert("서버와의 통신이 원활하지 않습니다.");
                                        console.log('사용자의 정보를 업데이트할 수 없습니다.');
                                    }
                                },
                                error: function () {
                                    alert("서버와의 통신이 원활하지 않습니다.");
                                    console.log("사용자 정보를 업데이트할 수 없습니다.");
                                }
                            });
                        });

                    }
                },
                error: function (e) {
                    alert("서버와의 통신이 원활하지 않습니다.");
                    console.log("사용자 정보를 가져올 수 없습니다.");
                }
            });

            $('#changePlanBtn').on('click', function () {
                if (paymentNow == null) {
                    window.location.href = "/support/pricing"
                    return;
                }

                $('#changePlan').fadeIn(300);
                $('.plan_oklayer').hide();

                $("#timing_now").click(function () {
                    $("#radio_timing_now").attr("checked", "checked");
                });
                $("#timing_later").click(function () {
                    $("#radio_timing_later").attr("checked", "checked");
                });

                $("#changeSubmit").click(function () {
                    console.log($("input[name='plan']:checked").val());

                    if ($("#radio_timing_now").is(":checked")) {
                        window.location.href = "/support/pricing";
                    } else if ($("#radio_timing_later").is(":checked")) {
                        $.ajax({
                            url: "/payment/planChangeConfirm",
                            data: {
                                'userNo': '${accessUser.userno}',
                                'goodName': $("input[name='plan']:checked").val(),
                                '${_csrf.parameterName}': '${_csrf.token}'
                            }
                        }).success(function (e) {
                            console.log(e);

                            if (e == 0) {
                                $.ajax({
                                    url: "/member/changePlanNextBilling",
                                    data: {
                                        'goodName': $("input[name='plan']:checked").val(),
                                        '${_csrf.parameterName}': '${_csrf.token}'
                                    }
                                }).success(function () {
                                    $('.lyr_plan').hide();
                                    $('.plan_oklayer').show();
                                })
                            } else {
                                alert("현재 적용된 요금제와 동일한 요금제 입니다.");
                            }
                        })
                    }
                })
            });

            // --------------------------------- 카드 등록 버튼 (2021.02.26 MRS)
            $('.subscribe').on('click', function(){
                let signature;

                function setParam(price, goodName, sign, callback) {
                    $("#billingPrice").val(price);
                    $("#billingGoodName").val(goodName);
                    $("#billingSignature").val(sign);
                    console.dir($("#billingForm"));
                    callback();
                }
                signature = $("#businessSignature").val();

                function payFunc() {
                    INIStdPay.pay('billingForm');
                }

                setParam("99000", "BUSINESS", signature, payFunc);

            })


            // --------------------------------- 카드 정보 수정 function (inicis From)
            $('.changeMethod').on('click', function () {

                let signature;

                console.log($("input[name='returnUrl']").val());

                function setParam(price, goodName, sign, callback) {
                    $("#billingPrice").val(price);
                    $("#billingGoodName").val(goodName);
                    $("#billingSignature").val(sign);
                    console.dir($("#billingForm"));
                    callback();
                }

                function payFunc() {
                    INIStdPay.pay('billingForm');
                }

                if (paymentNow == null) {
                    alert("현재 적용된 결제정보가 없습니다.");
                    return;
                }

                if (paymentNow.goodName == "BASIC") {
                    signature = $("#basicSignature").val();
                } else if (paymentNow.goodName == "BUSINESS") {
                    signature = $("#businessSignature").val();
                } else {
                    alert("error! " + paymentNow.goodName);
                    return;
                }

                setParam(paymentNow.price, paymentNow.goodName, signature, payFunc);

            });

            // --------------------------------- 영수증 출력 function
            $('.receiptPop').on('click', function () {
                var pano = $(this).data("pano");

                $.ajax({
                    url: "/member/payinfo",
                    data: {pano: pano},
                    "${_csrf.parameterName}" : "${_csrf.token}"
                }).
                done(function (data) {
                    console.log(data);
                    let payinfo = JSON.parse(data);
                    console.dir(payinfo);

                    $("#receipt_date").html(payinfo.payDate);
                    $("#receipt_good").html(payinfo.goodName);
                    if(payinfo.issuerName == "PayPal"){
                        $("#receipt_method").html(payinfo.cardNo);
                        $("#receipt_price").html("$" + priceDisplay(payinfo.price));
                    }else {
                        $("#receipt_method").html(payinfo.issuerName + " " + cardNoDisplay(payinfo.cardNo));
                        $("#receipt_price").html(priceDisplay(payinfo.price) + " 원");
                    }
                    $("#receipt_pano").html(payinfo.pano.replace("mindslab01_", ""));
                });

                $('.lyr_receipt').fadeIn(300);
            });

            // --------------------------------- 영수증 확인 버튼 function
            $("#receiptBtn").click(function () {
                $('.lyr_receipt').fadeOut(300);
            });

            $("#notTerminate").click(function () {
                $('.lyr_planconf').fadeOut(300);
            });

            // product layer popup
            $('.btn_lyrWrap_close, .lyr_plan_bg').on('click', function () {
                // $('#changePlan').fadeOut(300);
                // $('.lyr_method').fadeOut(300);
                $(".plan_change").fadeOut(300);
                $('.lyr_receipt').fadeOut(300);
                $('.lyr_planconf').fadeOut(300);
                $('.plan_oklayer').fadeOut();
                $('body').css({
                    'overflow': ''
                });
            });

            $('#planChangeResult').on('click',function () {
                window.location.reload();
            });

            $('.unsubscribe').on('click', function () {
                $('.lyr_planconf').fadeIn(300);
                if($('.contBox.step02').hasClass('current')){
                    console.log('ggg');
                    $('.contBox').removeClass('current');
                }
                $('.contBox.step01').addClass('current')
            });
            // 구독해지 단계 이동
            $('.btn_unscribe').on('click', function(){
                $(this).parents('.contBox').removeClass('current');
                $(this).parents('.contBox').next('.contBox').addClass('current');
            });

            //피드백 선택 및 해지 이유 입력 기능
            $("input[type='checkbox']").on('click', inputcheck);
            $("#unsubDesc").on('input', checkBtn);

            function inputcheck(){
                if($(this).attr('id')==='slt_none'){
                    $(".srv").prop('checked', false);
                }else {
                    $("#slt_none").prop('checked', false);
                }
                checkBtn();
            }
            function checkBtn(){
                var selectService = $("input[type='checkbox']");
                var unsubDesc = $("#unsubDesc").val();
                var unsebBtn = $("#terminateBtn");

                if(selectService.is(':checked') === false || unsubDesc === '' ){
                    unsebBtn.removeClass('btn_unscribe');
                }else{
                    unsebBtn.addClass('btn_unscribe');
                }
            }


            $('#terminateBtn').on('click', function () {
                var selectServiceArr = [];

                $("input[type='checkbox']:checked").each(function(){
                    selectServiceArr.push($(this).val());
                });

                // var usability = 0;
                // if (typeof $("input[name='radio_usability']:checked").val() != "undefined") usability = $("input[name='radio_usability']:checked").val();
                //
                // var demo = 0;
                // if (typeof $("input[name='radio_demo']:checked").val() != "undefined") demo = $("input[name='radio_demo']:checked").val();

                $.ajax({
                    url: "/payment/planCancel",
                    type: "POST",
                    data: {
                        "${_csrf.parameterName}": "${_csrf.token}",
                        "payMethod" : paymentNow.issuerName,
                        "feedbackUsability": '-1',
                        "feedbackDemo": '-1',
                        "feedbackEtc": selectServiceArr.join(", ") +' : '+$('#unsubDesc').val()
                    },

                    success: function (response) {
                        if (response.msg == "FAIL") {
                            alert("Subscription cancel is failed.");
                            console.log("Fail : " + response.errMsg);
                            // $('.lyr_planconf').hide();
                            $(this).parents('.contBox').removeClass('current');
                            $(this).parents('.contBox').next('.contBox').addClass('current');
                            return;
                        }
                        $('.plan_change').fadeIn(300);
                        $('.lyr_planconf').hide();

                    },
                    error: function (response) {
                        console.log(response);
                        alert("error");
                        $('.lyr_planconf').hide();
                        location.reload();
                    }
                });
            });
            // product layer popup
            $('.btn_cancle, .btn_lyrWrap_close, .lyr_plan_bg').on('click', function (){
                $('.lyr_planconf').fadeOut(300);
                $('body').css({
                    'overflow': ''
                });
            });


            $("#unsubscribeComplete #completBtn, #unsubscribeComplete .lyr_plan_bg, #unsubscribeComplete .btn_lyrWrap_close").on('click', function () {
                $("#unsubscribeComplete").fadeOut(300);
                location.reload();
                <%--if(status == ${UserStatus.FREE}||status == ${UserStatus.INIT}) {--%>
                <%--    console.log('free');--%>
                <%--    document.getElementById('logout-form').submit();--%>
                <%--} else {--%>
                <%--    location.reload();--%>
                <%--}--%>
            });
        });
    });
</script>

