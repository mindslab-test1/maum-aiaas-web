<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-05-21
  Time: 16:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<%@ include file="../common/common_header.jsp" %>
<!-- General Script -->
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/Chart.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/utils.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jsrender.min.js"></script>
<!-- #wrap -->
<div id="wrap" class="user_container maumUI">
    <!-- #header -->
    <%@ include file="../common/header.jsp" %>
    <!-- //#header -->
    <!-- .contents -->
    <div class="contents">
        <!-- .content -->
        <div class="content">
            <h1>서비스 이용현황</h1>
            <!--.mypageBox-->
            <div class="mypageBox">
                <!-- .account -->
                <div class="account">
                    <p>API ID,Key</p>
                    <div class="stn_1 stn">
                        <dl>
                            <dt>ID</dt>
                            <dd>${apiId} </dd>
                        </dl>
                        <dl>
                            <dt>Key</dt>
                            <dd>${apikey} </dd>
                        </dl>
                    </div>

                    <p>API 전체 사용량<span>서비스 기간 : <em>${dateFrom} </em> ~ <em>${dateTo} </em></span></p>
                    <div class="stn">
                        <div class="chartBox">
                            <canvas id="myChart"></canvas>
                        </div>
                        <p class="usage_txt">총 사용량<br><span><strong id="idleRate"></strong>%</span></p>
                    </div>

                    <p>API 서비스별 사용량</p>
                    <div class="stn">
                        <div class="tbl_usage">
                            <table summary="분류, 사용 서비스, 사용한 양/제공량, 사용비율로 구성">
                                <caption class="hide">API 서비스별 사용량</caption>
                                <colgroup>
                                    <col width="20%"><col width="35%"><col width="25%"><col width="20%">
                                </colgroup>

                                <thead>
                                <tr>
                                    <th scope="col">분류</th>
                                    <th scope="col">사용 서비스</th>
                                    <th scope="col">사용한 양 / 제공량</th>
                                    <th scope="col">사용비율 (%)</th>
                                </tr>
                                </thead>
                                <tbody id="engineUsageList">
<%--                                <tr>
                                    <td scope="row">음성</td>
                                    <td>TTS</td>
                                    <td>200만자 / 400만자</td>
                                    <td>50%</td>
                                </tr>
                                <tr>
                                    <td scope="row">음성</td>
                                    <td>STT</td>
                                    <td>25분 / 2500분</td>
                                    <td>5%</td>
                                </tr>
                                <tr>
                                    <td scope="row">시각</td>
                                    <td>인물포즈인식</td>
                                    <td>8000건 / 40000건</td>
                                    <td>10%</td>
                                </tr>
                                <tr>
                                    <td scope="row">음성</td>
                                    <td>TTS</td>
                                    <td>200만자 / 400만자</td>
                                    <td>50%</td>
                                </tr>
                                <tr>
                                    <td scope="row">음성</td>
                                    <td>STT</td>
                                    <td>25분 / 2500분</td>
                                    <td>5%</td>
                                </tr>
                                <tr>
                                    <td scope="row">시각</td>
                                    <td>인물포즈인식</td>
                                    <td>8000건 / 40000건</td>
                                    <td>10%</td>
                                </tr>
                                <tr>
                                    <td scope="row">음성</td>
                                    <td>TTS</td>
                                    <td>200만자 / 400만자</td>
                                    <td>50%</td>
                                </tr>
                                <tr>
                                    <td scope="row">음성</td>
                                    <td>STT</td>
                                    <td>25분 / 2500분</td>
                                    <td>5%</td>
                                </tr>
                                <tr>
                                    <td scope="row">시각</td>
                                    <td>인물포즈인식</td>
                                    <td>8000건 / 40000건</td>
                                    <td>10%</td>
                                </tr>
                                <tr>
                                    <td scope="row">시각</td>
                                    <td>인물포즈인식</td>
                                    <td>8000건 / 40000건</td>
                                    <td>10%</td>
                                </tr>--%>
                                </tbody>
                            </table>
                        </div>

                        <div class="pageing">
<%--                            <a class="btn_paging_prev" href="#none">
                                <span class="hide">이전 페이지로 이동</span>
                                <em class="fas fa-chevron-left"></em>
                            </a>
                            <span class="list">
                                <a href="#none" class="on">1</a>
                                <a href="#none">2</a>
                                <a href="#none">3</a>
                                <a href="#none">4</a>
                                <a href="#none">5</a>
                            </span>
                            <a class="btn_paging_next" href="#none">
                                <span class="hide">다음 페이지로 이동</span>
                                <em class="fas fa-chevron-right"></em>
                            </a>--%>
                        </div>
                    </div>


                    <p>API 상세 사용 내역</p>
                    <input type="hidden" id="function_name" name="function_name" value="getApiAccountList" />
                    <input type="hidden" id="current_page_no" name="current_page_no" value="1" />
                    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                    <div class="stn">
                        <div class="sort_group">
                            <span>분류</span>
                            <select class="select" id="search_category">
                                <option value="all">전체보기</option>
                                <option value="음성">음성</option>
                                <option value="시각">시각</option>
                                <option value="언어">언어</option>
                                <option value="대화">대화</option>
                                <option value="영어교육">영어교육</option>
                            </select>

                            <span>서비스명</span>
                            <select class="select" id="search_service">
                                <option value="all">전체보기</option>
                                <option value="TTS">음성생성(TTS)</option>
                                <option value="STT">음성인식(STT)</option>
                                <option value="DENOISE">음성정제</option>
                                <option value="DAP">Voice Filter, Voice Recognition</option>
                                <option value="LIPSYNC">Lip Sync Avatar</option>
                                <option value="AVATAR">Face-to-Face Avatar</option>
                                <option value="ESR">Enhanced Super Resolution</option>
                                <option value="INS">Face Recognition</option>
                                <option value="VSR">Video Super Resolution</option>
                                <option value="TTI">AI 스타일링</option>
                                <option value="TXR">텍스트 제거</option>
                                <option value="VCASubtitleExtract">이미지 자막 인식</option>
                                <option value="PoseExtract">인물 포즈 인식</option>
                                <option value="VCAFaceTracking">얼굴 추적</option>
                                <option value="FEAT">헤어 컬러, 의상 특징 인식</option>
                                <option value="PlateRecog">차량 번호판 인식</option>
                                <option value="AVR">얼굴 마스킹, 차량 유리창 마스킹</option>
                                <option value="AnomalyDetect">이상행동 감지</option>
                                <option value="BRACKET">치아 교정기 포지셔닝</option>
                                <option value="CORRECT">문장 교정</option>
                                <option value="KONG">한글 변환</option>
                                <option value="NLP">자연어 이해</option>
                                <option value="MRC">AI 독해</option>
                                <option value="XDC">텍스트 분류</option>
                                <option value="GPT">문장 생성</option>
                                <option value="HMD">패턴 분류</option>
                                <option value="ITF">의도 분류</option>
                                <option value="EngEdu_STT">교육용 STT</option>
                                <option value="EngEdu_Pron">문장 발음 평가</option>
                                <option value="EngEdu_Phonics">파닉스 평가</option>
                            </select>

                            <button type="submit" class="btn_srch" id="search_btn">검색</button>
                        </div>

                        <div class="tbl_usage">
                            <table summary="No, 분류, 서비스 명, 사용량, 사용일자로 구성">
                                <caption class="hide">API 상세 사용 내역</caption>
                                <colgroup>
                                    <col width="10%"><col width="10%"><col width="40%"><col width="10%"><col width="30%">
                                </colgroup>

                                <thead>
                                <tr>
                                    <th scope="col">No</th>
                                    <th scope="col">분류</th>
                                    <th scope="col">서비스 명</th>
                                    <th scope="col">사용량</th>
                                    <th scope="col">사용일자</th>
                                </tr>
                                </thead>
                                <tbody id="apiUsageList">
                                    <!-- 사용 내역 -->
                                </tbody>
                            </table>
                        </div>

                        <div class="pageing" id="paging">
<%--                            <a class="btn_paging_prev" href="#none">
                                <span class="hide">이전 페이지로 이동</span>
                                <em class="fas fa-chevron-left"></em>
                            </a>
                            <span class="list">
                                <a href="#none" class="on">1</a>
                                <a href="#none">2</a>
                                <a href="#none">3</a>
                                <a href="#none">4</a>
                                <a href="#none">5</a>
                            </span>
                            <a class="btn_paging_next" href="#none">
                                <span class="hide">다음 페이지로 이동</span>
                                <em class="fas fa-chevron-right"></em>
                            </a>--%>
                        </div>
                    </div>
                </div>
                <!-- //.account -->
            </div>
            <!-- //.mypageBox -->
        </div>
        <!-- //.content -->
    </div>
    <!-- //.contents -->
</div>
<!-- //#wrap -->

<!-- Local Script -->


<%-- 서비스별 사용량 --%>
<script id="serviceListTemplate" type="text/x-jsrender">
    <tr>
        <td>{{>engineGrp}}</td>
        <td>
            {{if service=='STT'}}STT
            {{else service=='TTS'}}TTS
            {{else service=='DENOISE'}}음성정제
            {{else service=='DAP'}}Voice Filter, Voice Recognition
            {{else service=='LIPSYNC'}}Lip Sync Avatar
            {{else service=='AVATAR'}}Face-to-Face Avatar
            {{else service=='ESR'}}Enhanced Super Resolution
            {{else service=='INS'}}Face Recognition
            {{else service=='INS_APP'}}Face Recognition
            {{else service=='VSR'}}Video Super Resolution
            {{else service=='TTI'}}AI 스타일링
            {{else service=='TXR'}}텍스트 제거
            {{else service=='VCASubtitleExtract'}}이미지 자막 인식
            {{else service=='PoseExtract'}}인물 포즈 인식
            {{else service=='VCAFaceTracking'}}얼굴 추적
            {{else service=='FEAT'}}헤어 컬러, 의상 특징 인식
            {{else service=='PlateRecog'}}차량 번호판 인식
            {{else service=='AVR'}}얼굴 마스킹, 차량 유리창 마스킹
            {{else service=='AnomalyDetect'}}이상행동 감지
            {{else service=='BRACKET'}}치아 교정기 포지셔닝
            {{else service=='CORRECT'}}문장 교정
            {{else service=='KONG'}}한글 변환
            {{else service=='NLP'}}자연어 이해
            {{else service=='MRC'}}AI 독해
            {{else service=='XDC'}}텍스트 분류
            {{else service=='GPT'}}문장 생성
            {{else service=='HMD'}}패턴 분류
            {{else service=='ITF'}}의도 분류
            {{else service=='EngEdu_STT'}}교육용 STT
            {{else service=='EngEdu_Pron'}}문장 발음 평가
            {{else service=='EngEdu_Phonics'}}파닉스 평가
            {{else service=='EduEng_STT'}}교육용 STT
            {{else service=='EduEng_Pron'}}문장 발음 평가
            {{else service=='EduEng_Phonics'}}파닉스 평가
            {{else service=='QA'}}뉴스봇
            {{else}}{{>service}}
            {{/if}}
        </td>
        <td>{{>usageAmount}} {{>unitName}} / {{>limitAmount}} {{>unitName}}</td>
        <td>{{>rate}} %</td>
    </tr>
</script>
<%--서비스 상세 사용 내역 --%>
<script id="listTemplate" type="text/x-jsrender">
    <tr>
        <td>{{:#index+1}}</td>
        <td>{{>engineGrp}}</td>
        <td>
            {{if service=='STT'}}STT
            {{else service=='TTS'}}TTS
            {{else service=='DENOISE'}}음성정제
            {{else service=='DAP'}}Voice Filter, Voice Recognition
            {{else service=='LIPSYNC'}}Lip Sync Avatar
            {{else service=='AVATAR'}}Face-to-Face Avatar
            {{else service=='ESR'}}Enhanced Super Resolution
            {{else service=='INS'}}Face Recognition
            {{else service=='INS_APP'}}Face Recognition
            {{else service=='VSR'}}Video Super Resolution
            {{else service=='TTI'}}AI 스타일링
            {{else service=='TXR'}}텍스트 제거
            {{else service=='VCASubtitleExtract'}}이미지 자막 인식
            {{else service=='PoseExtract'}}인물 포즈 인식
            {{else service=='VCAFaceTracking'}}얼굴 추적
            {{else service=='FEAT'}}헤어 컬러, 의상 특징 인식
            {{else service=='PlateRecog'}}차량 번호판 인식
            {{else service=='AVR'}}얼굴 마스킹, 차량 유리창 마스킹
            {{else service=='AnomalyDetect'}}이상행동 감지
            {{else service=='BRACKET'}}치아 교정기 포지셔닝
            {{else service=='CORRECT'}}문장 교정
            {{else service=='KONG'}}한글 변환
            {{else service=='NLP'}}자연어 이해
            {{else service=='MRC'}}AI 독해
            {{else service=='XDC'}}텍스트 분류
            {{else service=='GPT'}}문장 생성
            {{else service=='HMD'}}패턴 분류
            {{else service=='ITF'}}의도 분류
            {{else service=='EngEdu_STT'}}교육용 STT
            {{else service=='EngEdu_Pron'}}문장 발음 평가
            {{else service=='EngEdu_Phonics'}}파닉스 평가
            {{else service=='EduEng_STT'}}교육용 STT
            {{else service=='EduEng_Pron'}}문장 발음 평가
            {{else service=='EduEng_Phonics'}}파닉스 평가
            {{else service=='QA'}}뉴스봇
            {{else}}{{>service}}
            {{/if}}
        </td>
        <td>{{>usage}}</td>
        <td>{{>date}}</td>
    </tr>
</script>

<script type="text/javascript">

    let $apiId = "${apiId}";
    let $dateFrom = "${dateFrom}";
    let $dateTo = "${dateTo}";

    let usageList;
    let categoryName;
    let serviceName;

    $(document).ready(function (){
        getApiUsageList();

        $('#search_btn').on('click', function() {
            searchService();
        });

    });

    function getApiUsageList() {

        $.ajax({
            url		:"/member/apiUsageList",
            data    : {
                "apiId": $apiId,
                "dateFrom": $dateFrom,
                "dateTo": $dateTo,
                "${_csrf.parameterName}" : "${_csrf.token}"
            },
            dataType:"JSON",
            cache   : false,
            async   : true,
            type	:"POST",
            success: function(data){

                console.log(data);
                console.log(data.usageList.length);
                console.log(data.engineUsageList);

                if(data.usageList.length > 0 && data.engineUsageList.length > 0){
                    usageList = data.usageList;
                    const totalUsageList = data.totalUsageList;
                    const engineUsageList = data.engineUsageList;

                    paging(engineUsageList.length, 10, 10, 1, $('#engineUsageList'), $('#serviceListTemplate'), engineUsageList);
                    paging(usageList.length, 15, 10, 1, $('#apiUsageList'), $('#listTemplate'), usageList);

                    let pieCategoryList = [];
                    for(let i=0 ; i<totalUsageList.length ; i++) {
                        pieCategoryList.push(totalUsageList[i].rate.toFixed(3));
                    }
                    const idleRate = ( 100-totalUsageList[ totalUsageList.length-1 ].rate ).toFixed(2);
                    $('#idleRate').text(idleRate);
                    drawPieChart(pieCategoryList);
                }else {
                    // api 사용량이 없을 때
                    $('#idleRate').text('0.00');
                    drawPieChart([0,0,0,0,0,100]);
                    $('#engineUsageList').html(
                        '<tr> <td colspan="5" class="empty_data">사용량이 없습니다.</td> </tr>'
                    );
                    $('#apiUsageList').html(
                        '<tr> <td colspan="5" class="empty_data">사용량이 없습니다.</td> </tr>'
                    );
                }

            },
            error 	: function(xhr, status, error) {
                console.log("fail");
            }
        });
    }

    function paging(totalData, dataPerPage, pageCount, currentPage, board, template, list) {
        const totalPage = Math.ceil(totalData / dataPerPage);   // 총 페이지 수
        const pageGroup = Math.ceil(currentPage / pageCount);   // 페이지 그룹

        // 템플릿에 데이터 그리기
        let startIndex = (currentPage-1) * dataPerPage;
        let endIndex = startIndex + dataPerPage;
        board.empty();
        board.html(template.render(list.slice(startIndex, endIndex)));

        let last = pageGroup * pageCount;       // 화면에 보여질 마지막 페이지 번호
        if(last > totalPage){
            last = totalPage;
        }
        let first = last - (pageCount-1);       // 화면에 보여질 첫번째 페이지 번호
        if(first <= 0) {
            first = 1;
        }
        let next = last + 1;
        let prev = first - 1;
        if(prev == 0) {
            prev = 1;
        }
        /*
        console.log("currentPage = " + currentPage);
        console.log("last : " + last);
        console.log("first : " + first);
        console.log("next : " + next);
        console.log("prev : " + prev);
        */
        if(totalPage < 1) {
            first = last;
        }

        const pages = board.parent().parent().parent().children('.pageing');
        pages.empty();
        // 이전 페이지 버튼
        pages.append('<a class="btn_paging_prev"><span class="hide">이전 페이지로 이동</span><em class="fas fa-chevron-left"></em></a> <span class="list">');
        // 페이지 생성
        for(let i=first ; i<=last ; i++) {
            if(i==currentPage) {
                pages.append('<a href="#none" class="on" id="' + i + '">' + i + '</a>');
            } else {
                pages.append('<a href="#none" id="' + i + '">' + i + '</a>');
            }
        }
        // 다음 페이지 버튼
        pages.append('</span><a class="btn_paging_next" href="#none"><span class="hide">다음 페이지로 이동</span><em class="fas fa-chevron-right"></em></a>');

        pages.children('a.on').css({'color':'#3f7bde', 'font-weight':'700'});
        $('.pageing a').hover(function() {
            $(this).css({'color':'#3f7bde', 'font-weight':'700'});
        });

        pages.children('a').on('click', function() {
            let $item = $(this);
            let selectedPage = $item.text();

            if(selectedPage == "이전 페이지로 이동") {            // 이전 페이지
                if(currentPage <= 1) {
                    alert("첫 번째 페이지입니다.");
                } else {
                    let pageIndex = prev;
                    if(pageIndex < 1) {
                        pageIndex = 1;
                    }
                    paging(totalData, dataPerPage, pageCount, pageIndex, board, template, list);
                }
            } else if (selectedPage == "다음 페이지로 이동") {    // 다음 페이지
                if(currentPage == totalPage) {
                    alert("마지막 페이지입니다.");
                } else {
                    let pageIndex = next;
                    if(pageIndex > totalPage) {
                        pageIndex = totalPage;
                    }
                    paging(totalData, dataPerPage, pageCount, pageIndex, board, template, list);
                }
            } else {
                paging(totalData, dataPerPage, pageCount, selectedPage, board, template, list);
            }
        });
    }

    function categoryFilter(element) {
        if(element.engineGrp === categoryName) {
            return true;
        }
    }

    function serviceFilter(element) {
        if(element.service === serviceName) {
            return true;
        }
    }

    function searchService() {

        categoryName = $('#search_category :selected').val();
        serviceName = $('#search_service :selected').val();

        let searchVal;

        // `분류`에 선택된 값이 있다고 하더라도 `서비스명`에 선택된 값에 따라 필터링
        if(serviceName != "all") {
            searchVal = usageList.filter(serviceFilter);
        } else if(categoryName != "all") {
            searchVal = usageList.filter(categoryFilter);
        } else {
            searchVal = usageList;
        }

        //console.log(searchVal);
        $('#apiUsageList').html('');
        if(searchVal.length == 0) {     // 검색결과 없을 시,
            $('#apiUsageList').html(
                '<tr> <td colspan="5" class="empty_data">서비스 분류 및 서비스 명을 선택하고 검색 버튼을 선택해 주세요.</td> </tr>'
            );
        } else {                        // 검색결과 존재 시,
            paging(searchVal.length, 10, 10, 1, $('#apiUsageList'), $('#listTemplate'), searchVal);
        }
    }

    function drawPieChart(rate) {
        var config = {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [
                        rate[0],  // purple  (음성)
                        rate[1],  // red (시각)
                        rate[2],  // green (언어)
                        rate[3],  // blue (대화)
                        rate[4],  // orange (영어교육)
                        rate[5],  // grey (남은 사용량)
                    ],
                    borderWidth: 0,
                    backgroundColor: [
                        window.chartColors.purple,
                        window.chartColors.red,
                        window.chartColors.green,
                        window.chartColors.blue,
                        window.chartColors.orange,
                        window.chartColors.grey,
                    ],
                    label: 'Dataset 1'
                }],
                labels: [
                    '음성',
                    '시각',
                    '언어',
                    '대화',
                    '영어교육',
                    '남은 사용량'
                ],
                fontColor: '#f7778a',
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                legend: {
                    labels: {
                        padding: 18,
                        fontColor: '#002b49',
                        fontSize: 13,
                        fontStyle: 500,
                        boxWidth: 15,
                        // boxHeight: 15,
                        cornerRadius: 3,
                    },
                    position: 'bottom',
                },
                cutoutPercentage: 65,
                animation: {
                    animateScale: true,
                    animateRotate: true
                },
                tooltips: {
                    backgroundColor: '#ffffff',
                    cornerRadius: 3,
                    borderWidth: 1,
                    borderColor: '#cfd5eb',
                    caretSize: 0,
                    titleFontColor: '#cfd5eb',
                    bodyFontColor: '#002b49',
                    bodyFontSize: 14,
                }
            }
        };

        var ctx = document.getElementById('myChart').getContext('2d');
        window.myDoughnut = new Chart(ctx, config);
    }

</script>
