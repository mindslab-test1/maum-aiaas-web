<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-07-08
  Time: 10:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
    <div class="content api_content">
        <h1  class="api_tit">문장 교정</h1>
        <ul class="menu_lst">
            <li class="tablinks" onclick="openTap(event, 'correct_demo')" id="defaultOpen"><button type="button">엔진</button></li>
            <li class="tablinks" onclick="openTap(event, 'correct_example')"><button type="button">적용사례</button></li>
            <li class="tablinks" onclick="openTap(event, 'correct_menu')"><button type="button">매뉴얼</button></li>
            <%--			<li class="tablinks"><a href="/member/krApiAccount" >API ID, key 발급</a></li>--%>
        </ul>
        <!--.conv_demo-->
        <div class="demobox demobox_nlu" id="correct_demo">
            <p><span>문장 교정</span> </p>
            <span class="sub">잘못된 한글 문장을 문맥에 맞게 교정해줍니다. </span>

            <!--.itf_box-->
            <div class="itf_box correct_box">
                <!--.step01-->
                <div class="step01">
                    <div class="demo_top">
                        <em class="far fa-list-alt"></em><span>샘플 선택</span>
                    </div>
                    <div class="ift_topbox">
                        <p>문장 교정을 진행할 문장을 선택해 주세요.</p>
                        <ul class="ift_lst">
                            <li>
                                <div class="radio">
                                    <input type="radio" id="radio" name="sentence" value="한꾹인뜰만 알아뽈 쑤 있께 짝썽하껬씁니따." checked="checked">
                                    <label for="radio">한꾹인뜰만 알아뽈 쑤 있께 짝썽하껬씁니따.</label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="radio2" name="sentence" value="늗게 베운 도듁이 냘 세는 쥴 모룬다.">
                                    <label for="radio2">늗게 베운 도듁이 냘 세는 쥴 모룬다.</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <p class="txt_or">또는</p>
                    <div class="demo_top">
                        <em class="far fa-keyboard"></em><span>문장 입력</span>
                    </div>
                    <div class="text_area">
                        <textarea id="text-contents" rows="10" placeholder="문장 교정을 진행할 문장을 입력해주세요. " maxlength="120"></textarea>
                        <div class="text_info">
                            <span class=""><strong id="count">0</strong>/120자</span>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_search" id="find_btn">문장 교정</button>
                    </div>
                </div>
                <!--//.step01-->
                <div class="loading_wrap">
                    <p><em class="far fa-file-alt"></em> 문장 교정중</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#c9c3ec" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#27c1c1" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

                        <p>약간의 시간이 소요 됩니다.</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1" ><em class="fas fa-redo"></em>처음으로</button>
                    </div>

                </div>
                <!--.step02-->
                <div class="step02">
                    <div class="demo_top">
                        <em class="far fa-file-alt"></em><span> 문장 교정 결과</span>
                    </div>
                    <div class="result_box">
                        <ul>
                            <li>
                                <span>입력</span>
                                <p id="input_sentence"></p>
                            </li>
                            <li>
                                <span>문장 교정</span>
                                <p id="result_sentence"></p>
                            </li>
                        </ul>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_reset" id="btn_reset"><em class="fas fa-redo-alt"></em>처음으로</button>
                    </div>
                </div>
                <!--//.step02-->
            </div>
            <!--//.itf_box-->

        </div>
        <!-- .conv_demo -->

        <!--.conv_menu-->
        <div class="demobox" id="correct_menu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>
                <div class="guide_group">
                    <div class="title">문장 교정 <small>(Bert Correction)</small></div>
                    <p class="sub_txt">잘못된 한글 문장을 문맥에 맞게 교정해줍니다.</p>
                    <span class="sub_title">
							준비사항
						</span>
                    <p class="sub_txt">① Input: 한글 문장</p>
                    <span class="sub_title">
							 실행 가이드
						</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/correct/sentence</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId</td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>sentence</td>
                            <td>문장 교정을 진행할 문장 (120자 이하) </td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
							<pre>
curl --location --request POST 'http://api.maum.ai/correct/sentence' \
--header 'Content-Type: application/json' \
--data-raw '{
    "apiId": "{발급받은 API ID}",
    "apiKey": "{발급받은 API KEY}",
    "sentence": "한꾹인뜰만 알아뽈 쑤 있께 짝썽하껬씁니따."
}'
</pre>
                    </div>

                    <p class="sub_txt">④ Response 파라미터 설명 </p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>타입</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>API 동작여부</td>
                            <td>list</td>
                        </tr>
                        <tr>
                            <td>result</td>
                            <td>문장 교정 진행 결과 문장</td>
                            <td>string</td>
                        </tr>

                    </table>
                    <span class="table_tit">message: API 동작여부</span>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>타입</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>요청 처리 상태를 설명하는 문자열 (Success/ Fail)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>요청 처리 상태에 대한 status code (0: Success)</td>
                            <td>number</td>
                        </tr>
                    </table>

                    <p class="sub_txt">⑤ Response 예제 </p>
                    <div class="code_box">
<pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "result": "한국인들만 알아볼 수 있게 작성하겠습니다."
}
</pre>

                    </div>
                </div>
            </div>

        </div>
        <!--//conv_menu-->
        <!--.conv_example-->
        <div class="demobox" id="correct_example">
            <p><em style="color:#27c1c1;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
            <%--conversion--%>
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>STT 성능 개선</span>
                            </dt>
                            <dd class="txt">STT로 인식한 Text를 맞춤법에 맞는 정확한 문장으로 수정하여 더욱 완성도 있는 결과물을 생성 합니다. ( 활용 예시) 회의록 등 )</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_sds"><span>SDS</span></li>
                                    <li class="ico_bqa"><span>NQA</span></li>
                                    <li class="ico_nlu"><span>NLU</span></li>
                                    <li class="ico_stt"><span>STT</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>챗봇</span>
                            </dt>
                            <dd class="txt">챗봇 내에서 이뤄지는 대화를 문맥에 맞추어서 교정하여, 매끄러운 대화를 이어갑니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_weatherBot"><span>챗봇</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>문서 자동 작성</span>
                            </dt>
                            <dd class="txt">OCR로 인식한 문서 내의 텍스트를 교정하여 더 높은 정확도를 보여줍니다.</dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //conversion -->
        </div>
        <!--//.conv_example-->


    </div>
</div>
<!-- //.contents -->




<script type="text/javascript">

    $(document).ready(function (){
        var ajaxXHR;
        var $textArea = $('#text-contents');

        //===== radioBox event =====
        $('input[type=radio]').on('click', function(){
            $textArea.text("");
            $textArea.val("");
        });

        //===== textArea event =====
        $textArea.on('change paste input keyup', function(){
            $('input[type=radio]:checked').prop("checked", false);
            countTextAreaLength ()
        });

        //===== 문장교정 =====
        $('#find_btn').on('click',function(){
            var $checkedRadio = $('input[type=radio]:checked');
            var $checkedtxt =$checkedRadio.val();
            var sentence = "";

            if( $textArea.val().trim() === ""){
                if($checkedRadio.length === 0){
                    alert("문장 교정을 할 예문을 선택하거나 텍스트를 입력해주세요.");
                    return;
                }
                sentence = $checkedtxt;
            }else{
                sentence = $textArea.val();
            }
            $('.itf_box .step01').hide();
            $('.itf_box .loading_wrap').fadeIn();

            ajaxXHR = $.ajax({
                url: '/api/correct/sentence',
                async: true,
                headers: {
                    "Content-Type": "application/json"
                },
                data: {
                    "context": sentence,
                    "${_csrf.parameterName}" : "${_csrf.token}"
                },
                error: function(error){
                    if (error.status === 0) {
                        return false;
                    }
                    console.log(error);
                },
                success: function(data){
                    console.log(unescape(data));
                    var responseData = JSON.parse(data);

                    $('#input_sentence').text(sentence);
                    $('#result_sentence').text(responseData.result);


                    $('.itf_box .step02').fadeIn();
                    $('.itf_box .loading_wrap').hide();
                    // correctResult(responseData);
                }
            });
        });


        $('#btn_reset, .btn_back1').on('click',function(){
            ajaxXHR.abort();
            countTextAreaLength ();
            var textAreaObj = document.getElementById('result_sentence');
            textAreaObj.scrollTop = textAreaObj.offsetHeight - 300;
            document.getElementById('result_sentence').innerHTML = "";

            $('.itf_box .step02').hide();
            $('.itf_box .step01').fadeIn();
            $('.itf_box .loading_wrap').hide();
        });

        function countTextAreaLength (){
            var content = $('#text-contents').val();
            if (content.length >= 120) {
                $('#count').html(120);
                return;
            }
            $('#count').html(content.length);
        }
    });


    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();



</script>
