<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-07-14
  Time: 08:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!--.pop_simple 얼굴 움직임 영상 파일 알림창 -->
<div class="pop_simple" id="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>파일 업로드 실패</h5>
            <p>파일 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
                업로드 되지 않았습니다.</p>
            <span>* 지원가능 파일 확장자: .mp4<br>
* 동영상파일 용량 30MB 이하만 가능합니다</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .pop_simple 특정 인물 사진 파일 알림창 -->
<div class="pop_simple" id="pop_simple2">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>파일 업로드 실패</h5>
            <p>파일 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
                업로드 되지 않았습니다.</p>
            <span>* 지원가능 파일 확장자: .jpg, .png<br>
                * 이미지 파일 용량: 2MB 이하 <br>
                 * 얼굴이 차지하는 면적이 전체 이미지의 1/4 이상</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->
<!-- .contents -->
<div class="contents api_content">
    <div class="content api_content">
        <h1 class="api_tit vision_lst">Face-to-Face Avatar</h1>

        <!-- .demobox -->
        <div class="demobox dropEg vis_avatar">
            <p><span>Face-to-Face Avatar</span></p>
            <span class="sub">사진 속 인물이 영상 속 얼굴의 움직임에 따라 움직이는 엔진 입니다.<br> 특정 인물의 얼굴로 재미있는 영상을 제작할 수 있습니다.</span>

            <div class="demoCont process">
                <h4>프로세스</h4>
                <div class="processBox">
                    <div class="stepBox step1">
                        <p class="tit">Step 1 <span>(입력)</span></p>

                        <div class="cont">
                            <p class="fileType">영상 파일</p>
                            <div class="videoBox">
                                <video controls="" width="272" height="153"
                                       poster="${pageContext.request.contextPath}/aiaas/common/images/img_aiavatar_thumb.png">
                                    <source src="${pageContext.request.contextPath}/aiaas/common/video/avatar_sample_.mp4"
                                            type="video/mp4">
                                    IE 8 이하는 비디오가 나오지 않습니다. IE 버전을 업데이트 하시길 바랍니다.
                                </video>
                            </div>

                            <em class="fas fa-plus"></em>

                            <p class="fileType">이미지 파일</p>
                            <div class="imgBox">
                                <img src="${pageContext.request.contextPath}/aiaas/common/images/img_avatar1.png"
                                     alt="sample image">
                            </div>
                        </div>
                    </div>

                    <div class="stepBox step2">
                        <p class="tit">Step 2 <span>(작동)</span></p>

                        <div class="cont">
                            <img class="egIcon"
                                 src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_avatar.svg"
                                 alt="Face-to-Face Avatar">
                            <p class="egName">Face-to-Face Avatar</p>
                        </div>
                    </div>

                    <div class="stepBox step3">
                        <p class="tit">Step 3 <span>(완성)</span></p>

                        <div class="cont">
                            <p class="fileType">결과 영상</p>
                            <div class="videoBox">
                                <video controls="" width="272" height="153">
                                    <source src="${pageContext.request.contextPath}/aiaas/common/video/avatar_result_sample.mp4"
                                            type="video/mp4">
                                    IE 8 이하는 비디오가 나오지 않습니다. IE 버전을 업데이트 하시길 바랍니다.
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="demoCont strength">
                <h4>기술의 특장점</h4>
                <ul class="comm_lst">
                    <li>
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_vis_avatar_strength01.svg"
                             alt="strength image">
                        <p class="tit">정확한 얼굴 인식</p>
                        <p class="desc">사진 속 얼굴 인식에<br> 특화된 엔진을 제공합니다.</p>
                    </li>
                    <li>
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_vis_avatar_strength02.svg"
                             alt="strength image">
                        <p class="tit">디테일한 움직임</p>
                        <p class="desc">입 모양뿐만 아니라<br> 표정, 움직임까지 따라 할 수 있습니다.</p>
                    </li>
                    <li>
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_vis_avatar_strength03.svg"
                             alt="strength image">
                        <p class="tit">쉽고 간단한 엔진</p>
                        <p class="desc">이미지 한 장만으로<br> 재미있는 영상을 만들 수 있습니다.</p>
                    </li>
                </ul>
            </div>

            <div class="demoCont useCase">
                <h4>활용 사례</h4>
                <ul class="comm_lst">
                    <li>
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_vis_avatar_useCase01.png"
                             alt="use case image">
                        <p class="tit">뉴스 제작</p>
                    </li>
                    <li>
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_vis_avatar_useCase02.png"
                             alt="use case image">
                        <p class="tit">VR,게임 캐릭터 제작</p>
                    </li>
                    <li>
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_vis_avatar_useCase03.png"
                             alt="use case image">
                        <p class="tit">영상 콘텐츠 제작</p>
                    </li>
                </ul>
            </div>
        </div>
        <!-- //.demobox -->
    </div>
    <!-- //.contents -->
<%--    <div class="content">--%>
<%--        <h1 class="api_tit">Face-to-Face Avatar</h1>--%>
<%--        <ul class="menu_lst vision_lst">--%>
<%--            <li class="tablinks" onclick="openTap(event, 'avatardemo')" id="defaultOpen">--%>
<%--                <button type="button">엔진</button>--%>
<%--            </li>--%>
<%--            <li class="tablinks" onclick="openTap(event, 'avatarexample')">--%>
<%--                <button type="button">적용사례</button>--%>
<%--            </li>--%>
<%--            <li class="tablinks" onclick="openTap(event, 'avatarmenu')">--%>
<%--                <button type="button">매뉴얼</button>--%>
<%--            </li>--%>
<%--            &lt;%&ndash;            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>&ndash;%&gt;--%>
<%--        </ul>--%>
<%--        <!-- .demobox -->--%>
<%--        <div class="demobox" id="avatardemo">--%>
<%--            <p><span>Face-to-Face Avatar</span></p>--%>
<%--            <span class="sub">동영상 내 인물의 얼굴 움직임을 포착하여, 사진 속 특정 인물이 이를 따라 움직이는 영상을 만드는 엔진입니다.</span>--%>
<%--            <!--avatar_box-->--%>
<%--            <div class="demo_layout avatar_box">--%>
<%--                <!--tr_1-->--%>
<%--                <div class="tr_1">--%>
<%--                    <div class="fl_box">--%>
<%--                        <p><em class="far fa-file-video"></em><strong>샘플 파일</strong>로 테스트 하기</p>--%>
<%--                        <div class="sample_box">--%>
<%--                            <div class="sample_1">--%>
<%--                                <div class="top_box">--%>
<%--                                    <p>얼굴 움직임 영상 파일</p>--%>
<%--                                    <video id="sampleVideo1" controls width="324" height="180" poster="${pageContext.request.contextPath}/aiaas/common/images/img_aiavatar_thumb.png">--%>
<%--                                        <source src="${pageContext.request.contextPath}/aiaas/common/video/avatar_sample_.mp4"--%>
<%--                                                type="video/mp4">--%>
<%--                                        IE 8 이하는 비디오가 나오지 않습니다. IE 버전을 업데이트 하시길 바랍니다.--%>
<%--                                    </video>--%>
<%--                                    <em class="fas fa-plus"></em>--%>
<%--                                </div>--%>
<%--                                <div class="under_box">--%>
<%--                                    <p>특정 인물 사진 파일</p>--%>
<%--                                    <div class="radio">--%>
<%--                                        <input type="radio" id="sample1" name="sample" value="1" checked>--%>
<%--                                        <label for="sample1" class="">--%>
<%--                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_avatar1.png" alt="sample1" />--%>
<%--                                            <span>Sample 1</span>--%>
<%--                                        </label>--%>
<%--                                    </div>--%>
<%--                                    <div class="radio">--%>
<%--                                        <input type="radio" id="sample2" name="sample" value="2">--%>
<%--                                        <label for="sample2" class="">--%>
<%--                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_avatar2.png" alt="sample2" />--%>
<%--                                            <span>Sample 2</span>--%>
<%--                                        </label>--%>
<%--                                    </div>--%>
<%--                                </div>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                    <div class="fr_box">--%>
<%--                        <p><em class="far fa-file-video"></em><strong>내 파일</strong>로 해보기</p>--%>
<%--                        <div class="uplode_box">--%>
<%--                            <div class="top_box">--%>
<%--                                <p>얼굴 움직임 영상 파일</p>--%>
<%--                                <div class="btn" id="uploadFile">--%>
<%--                                    <em class="fas fa-times hidden close"></em>--%>
<%--                                    <em class="far fa-file-image hidden"></em>--%>
<%--                                    <input type="file" id="demoFile" class="demoFile" accept=".mp4">--%>
<%--                                    <label for="demoFile" class="demolabel">파일 업로드</label>--%>
<%--                                </div>--%>
<%--                                <ul>--%>
<%--                                    <li>* 지원가능 파일 확장자: .mp4  </li>--%>
<%--                                    <li>* 영상 크기: 30MB 이하</li>--%>
<%--                                    <li>* 영상 길이: 20초 이하</li>--%>
<%--                                </ul>--%>
<%--                                <em class="fas fa-plus"></em>--%>
<%--                            </div>--%>
<%--                            <div class="under_box">--%>
<%--                                <p>특정 인물 사진 파일</p>--%>
<%--                                <div class="btn" id="uploadFile2">--%>
<%--                                    <em class="fas fa-times hidden close"></em>--%>
<%--                                    <em class="far fa-file-image hidden"></em>--%>
<%--                                    <input type="file" id="demoFile2" class="demoFile" accept=".jpg, .png">--%>
<%--                                    <label for="demoFile2" class="demolabel">파일 업로드</label>--%>
<%--                                </div>--%>
<%--                                <ul>--%>
<%--                                    <li>* 지원가능 파일 확장자: .jpg, .png </li>--%>
<%--                                    <li>* 이미지 파일 용량: 2MB 이하 </li>--%>
<%--                                    <li>* 얼굴이 차지하는 면적이 전체 이미지의 1/4 이상</li>--%>
<%--                                </ul>--%>
<%--                                <span class="desc">* 해상도가 높을수록 생성되는 영상의 품질이 좋습니다.</span>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>

<%--                    <div class="btn_area">--%>
<%--                        <button type="button" class="btn_start" id="sub_avatar">생성하기</button>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--                <!--tr_1-->--%>
<%--                <!--tr_2-->--%>
<%--                <div class="tr_2">--%>
<%--                    <p><em class="far fa-file-video"></em>AI Avatar 생성 중</p>--%>
<%--                    <div class="loding_box ">--%>
<%--                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"--%>
<%--                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px"--%>
<%--                             viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42"--%>
<%--                                                                             d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/>--%>
<%--                            <g>--%>
<%--                                <path fill="#f7778a" fill-opacity="1"--%>
<%--                                      d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/>--%>
<%--                                <animatetransform attributeName="transform" type="translate"--%>
<%--                                                  values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0"--%>
<%--                                                  calcMode="discrete" dur="1820ms" repeatCount="indefinite"/>--%>
<%--                            </g></svg>--%>

<%--                        <p>약간의 시간이 소요 됩니다.</p>--%>
<%--                    </div>--%>
<%--                    <div class="btn_area">--%>
<%--                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>--%>
<%--                    </div>--%>

<%--                </div>--%>
<%--                <!--tr_2-->--%>
<%--                <!--tr_3-->--%>
<%--                <div class="tr_3">--%>
<%--                    <div class="result_file">--%>
<%--                        <div class="input_box">--%>
<%--                            <p><em class="far fa-file-video"></em>입력 파일</p>--%>
<%--                            <div class="file_box">--%>
<%--                                <div class="inner">--%>
<%--                                    <p>얼굴 움직임 영상 파일</p>--%>
<%--                                    <video id="inputVideo" controls width="324" height="180">--%>
<%--                                        <source src="" type="video/mp4">--%>
<%--                                        IE 8 이하는 비디오가 나오지 않습니다. IE 버전을 업데이트 하시길 바랍니다.--%>
<%--                                    </video>--%>
<%--                                </div>--%>
<%--                                <em class="fas fa-plus"></em>--%>
<%--                                <div class="inner">--%>
<%--                                    <p>특정 인물 사진 파일</p>--%>
<%--                                    <img id="inputImg" src="" alt="특정 인물 사진 파일" />--%>
<%--                                </div>--%>

<%--                            </div>--%>
<%--                        </div>--%>
<%--                        <div class="result_box">--%>
<%--                            <p><em class="far fa-file-video"></em>결과 파일</p>--%>
<%--                            <div class="file_box">--%>
<%--                                <video id="outputVideo" controls width="462" height="256">--%>
<%--                                    <source src="" type="video/mp4">--%>
<%--                                    IE 8 이하는 비디오가 나오지 않습니다. IE 버전을 업데이트 하시길 바랍니다.--%>
<%--                                </video>--%>
<%--                                <a id="download_avatar" href="" class="dwn" download="avatar.mp4" ><em class="far fa-arrow-alt-circle-down"></em> 결과 파일 다운로드</a>--%>
<%--                            </div>--%>
<%--                        </div>--%>
<%--                    </div>--%>
<%--                    <div class="btn_area">--%>
<%--                        <button type="button" class="btn_back2" id=""><em class="fas fa-redo"></em>처음으로</button>--%>
<%--                    </div>--%>
<%--                </div>--%>
<%--                <!--tr_3-->--%>
<%--            </div>--%>
<%--            <!--//.avatar_box-->--%>

<%--        </div>--%>
<%--        <!-- //.demobox -->--%>
<%--        <!--.avatarmenu-->--%>
<%--        <div class="demobox vision_menu" id="avatarmenu">--%>
<%--            <!--guide_box-->--%>
<%--            <div class="guide_box">--%>
<%--                <div class="guide_common">--%>
<%--                    <div class="title">--%>
<%--                        API 공통 가이드--%>
<%--                    </div>--%>
<%--                    <p class="sub_title">개발 환경 세팅</p>--%>
<%--                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>--%>
<%--                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>--%>
<%--                    <p class="sub_title">키 발급</p>--%>
<%--                    <p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>--%>
<%--                    <p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>--%>
<%--                    <p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>--%>
<%--                    <p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>--%>
<%--                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>--%>
<%--                </div>--%>
<%--                <div class="guide_group">--%>
<%--                    <div class="title">Face-to-Face Avatar</div>--%>
<%--                    <p class="sub_txt">동영상 내 인물의 얼굴 움직임을 포착하여, 사진 속 특정 인물이 이를 따라 움직이는 영상을 만드는 엔진입니다.</p>--%>
<%--                    <span class="sub_title">--%>
<%--								준비사항--%>
<%--					</span>--%>
<%--                    <p class="sub_txt">- Input 1: 얼굴의 움직임이 있는 영상 파일</p>--%>
<%--                    <ul>--%>
<%--                        <li>확장자: .mp4</li>--%>
<%--                        <li>용량: 30MB 이하</li>--%>
<%--                        <li>길이: 20초 이하 </li>--%>
<%--                    </ul>--%>
<%--                    <p class="sub_txt">- Input 2: 특정 인물의 얼굴 이미지 파일</p>--%>
<%--                    <ul>--%>
<%--                        <li>확장자: .jpg, .png</li>--%>
<%--                        <li>용량: 2MB 이하</li>--%>
<%--                        <li>얼굴이 차지하는 면적이 전체 이미지의 ¼ 이상</li>--%>
<%--                    </ul>--%>
<%--                    <span class="sub_title">--%>
<%--								 실행 가이드--%>
<%--							</span>--%>
<%--                    <p class="sub_txt">① Request  </p>--%>
<%--                    <ul>--%>
<%--                        <li>Method : POST</li>--%>
<%--                        <li>URL : https://api.maum.ai/avatar/download</li>--%>
<%--                    </ul>--%>
<%--                    <p class="sub_txt">② Request 파라미터 설명 </p>--%>
<%--                    <table>--%>
<%--                        <tr>--%>
<%--                            <th>키</th>--%>
<%--                            <th>설명</th>--%>
<%--                            <th>type</th>--%>
<%--                        </tr>--%>
<%--                        <tr>--%>
<%--                            <td>apiId </td>--%>
<%--                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청 </td>--%>
<%--                            <td>string</td>--%>
<%--                        </tr>--%>
<%--                        <tr>--%>
<%--                            <td>apiKey </td>--%>
<%--                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청 </td>--%>
<%--                            <td>string</td>--%>
<%--                        </tr>--%>
<%--                        <tr>--%>
<%--                            <td>video</td>--%>
<%--                            <td>얼굴의 움직임 영상 파일 </td>--%>
<%--                            <td>file</td>--%>
<%--                        </tr>--%>
<%--                        <tr>--%>
<%--                            <td>images</td>--%>
<%--                            <td>특정 인물의 얼굴 이미지 파일 (최대 5개)</td>--%>
<%--                            <td>file</td>--%>
<%--                        </tr>--%>
<%--                    </table>--%>
<%--                    <p class="sub_txt">③ Request 예제 </p>--%>
<%--                    <div class="code_box">--%>
<%--                        curl --location --request POST 'http://api.maum.ai/avatar/download' \<br>--%>
<%--                        --form 'apiId= {발급받은 API ID}' \<br>--%>
<%--                        --form 'apiKey= {발급받은 API KEY}' \<br>--%>
<%--                        --form 'images= {이미지 파일 path}' \<br>--%>
<%--                        --form 'images= {이미지 파일 path}' \<br>--%>
<%--                        --form 'images= {이미지 파일 path}' \<br>--%>
<%--                        --form 'images= {이미지 파일 path}' \<br>--%>
<%--                        --form 'images= {이미지 파일 path}' \<br>--%>
<%--                        --form 'video= {영상 파일 path}'--%>
<%--                    </div>--%>

<%--                    <p class="sub_txt">④ Response 예제 </p>--%>

<%--                    <div class="code_box">--%>
<%--                        (.mp4 File Download)--%>
<%--                    </div>--%>

<%--                </div>--%>
<%--            </div>--%>
<%--            <!--//.guide_box-->--%>
<%--        </div>--%>
<%--        <!--.avatarmenu-->--%>
<%--        <!--.avatarexample-->--%>
<%--        <div class="demobox" id="avatarexample">--%>
<%--            <p><em style="color:#f7778a;font-weight: 400;">적용사례</em> <small>(Use Cases)</small></p>--%>
<%--            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>--%>
<%--            <!-- avatar -->--%>
<%--            <div class="useCasesBox">--%>
<%--                <ul class="lst_useCases">--%>
<%--                    <li>--%>
<%--                        <dl>--%>
<%--                            <dt>--%>
<%--                                <em>CASE 01</em>--%>
<%--                                <span>영상 제작</span>--%>
<%--                            </dt>--%>
<%--                            <dd class="txt">뉴스 또는 인터뷰와 같이 정보 전달 영상을 제작할 때, 직접 화자의 모습을 들어내지 않고 음성과 함께 나타낼 수 있습니다.--%>
<%--                            </dd>--%>
<%--                            <dd class="api_itemBox">--%>
<%--                                <ul class="lst_api">--%>
<%--                                    <li class="ico_tts"><span>TTS</span></li>--%>
<%--                                </ul>--%>
<%--                            </dd>--%>
<%--                        </dl>--%>
<%--                    </li>--%>
<%--                    <li>--%>
<%--                        <dl>--%>
<%--                            <dt>--%>
<%--                                <em>CASE 02</em>--%>
<%--                                <span>VR, 게임 캐릭터</span>--%>
<%--                            </dt>--%>
<%--                            <dd class="txt">VR이나 게임 속 Npc등을 원하는 얼굴로 자유롭게 제작하기 수월합니다.</dd>--%>

<%--                        </dl>--%>
<%--                    </li>--%>

<%--                </ul>--%>
<%--            </div>--%>
<%--            <!--avatar  -->--%>
<%--        </div>--%>
<%--        <!--//.avatarexample-->--%>
<%--    </div>--%>
<%--    <!-- //.content -->--%>
</div>
<script>

    var sample1SrcUrl;
    var sample2SrcUrl;
    var sample3SrcUrl;
    var sample1File;
    var sample2File;
    var sample3File;

    var request;

/*    function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/common/video/avatar_sample_.mp4");
        xhr.responseType = "blob";
        xhr.onload = function () {
            blob = xhr.response;
            sample1File = new File([blob], "avatar_sample_.mp4");
            sample1SrcUrl = URL.createObjectURL(blob);

            var demo1 = document.getElementById('demoFile');
            demo1.setAttribute("src", sample1SrcUrl);
        };
        xhr.send();
    }

    //sample2 (세종대왕)
    function loadSample2() {

        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.responseType = "blob";
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/common/images/img_avatar1.png");
        xhr.onload = function () {
            blob = xhr.response;
            sample2File = new File([blob], "img_avatar1.png");
            sample2SrcUrl = URL.createObjectURL(blob);
        };
        xhr.send();
    }

    //sample3 (의문의 남성)
    function loadSample3() {

        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.responseType = "blob";
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/common/images/img_avatar2.png");
        xhr.onload = function () {
            blob = xhr.response;
            sample3File = new File([blob], "img_avatar2.png");
            sample3SrcUrl = URL.createObjectURL(blob);
        };
        xhr.send();
    }*/

    function sendApiRequest(videoFile, imageFile) {

        var formData = new FormData();
        formData.append('video', videoFile);
        formData.append('image', imageFile);
        formData.append('${_csrf.parameterName}', '${_csrf.token}');

        request = new XMLHttpRequest();
        request.responseType = "blob";
        request.onreadystatechange = function() {

            if(request.readyState === 4 && request.status === 200) {

                if(request.response == null){
                    alert("서버에서 응답을 받아오지 못했습니다. 다시 시도해 주세요");
                    window.location.reload();
                }

                let resultBlob = new Blob([request.response], {type: 'video/mp4'});
                let srcUrl = URL.createObjectURL(resultBlob);
                $('#outputVideo').attr('src', srcUrl);
                $('#download_avatar').attr('href', srcUrl);

                $('.avatar_box').css('border', 'none');

                $('.tr_2').hide();
                $('.tr_3').fadeIn(300);

                $('#outputVideo').get(0).play();
            }
        };
        request.open('POST', '/api/avatar/getAvatarApi');
        request.send(formData);
        request.timeout = 2500000;

        request.ontimeout = function() {
            alert('timeout');
            window.location.reload();
        };
    }

    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {

/*
            loadSample1();
            loadSample2();
            loadSample3();
*/

            $('input[type="radio"]').on('click', function () {
                $('em.close').trigger('click');
            });

            $('#sub_avatar').on('click', function () {

                let uploadVideoFlag = $('#uploadFile .demolabel').text();
                let uploadImageFlag = $('#uploadFile2 .demolabel').text();

                let videoFile;
                let imageFile;

                // 파일을 하나라도 업로드한 경우
                if(uploadVideoFlag !== "파일 업로드" || uploadImageFlag !== "파일 업로드") {    //업로드 확인
                    if (uploadVideoFlag === "파일 업로드") {
                        alert("얼굴 움직임 영상 파일을 업로드해주세요.");
                        return;
                    }
                    else if (uploadImageFlag === "파일 업로드") {
                        alert("특정 인물 사진 파일을 업로드해주세요.");
                        return;
                    }

                    videoFile = $('#demoFile').get(0).files[0];
                    imageFile = $('#demoFile2').get(0).files[0];

                } else {

                    if($('input[name="sample"]:checked').val() === undefined){
                        alert("샘플을 선택하거나 파일을 업로드해 주세요!");
                        return;
                    }

                    let radioCheckValue = document.getElementsByName('sample');

                    videoFile = sample1File;

                    if( radioCheckValue[0].checked == true ) {
                        imageFile = sample2File;
                    } else {
                        imageFile = sample3File;
                    }
                }

                $('#inputVideo').attr('src', URL.createObjectURL(videoFile));
                $('#inputImg').attr('src', URL.createObjectURL(imageFile));

                $('.tr_1').hide();
                $('.tr_2').fadeIn();

                sendApiRequest(videoFile, imageFile);
            });

            // close button
            $('em.close').on('click', function () {

                $('.fl_box').css("opacity", "1");
                $(this).parent().removeClass("btn_change").addClass("btn");
                $(this).parent().children('.demolabel').text('파일 업로드');
                $(this).parent().find('input').val(null);

            });

            // $('.sample_box').on('click', function () {
            //     $('em.close').trigger('click');
            // });

            // 처음으로 버튼
            $('.btn_back1').on('click', function () {
                request.abort();

                $('.tr_2').hide();
                $('.tr_1').fadeIn(300);

            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('#outputVideo').get(0).pause();
                $('.tr_3').hide();
                $('.tr_1').fadeIn(300);
            });


            //알림 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });

        });
    });

    $(document).ready(function () {
        //파일 업로드
        $('.demoFile').each(function () {
            var $input = $(this);
            var $inputId= $input.attr('id');
            var $label = $input.next('.demolabel');

            $input.on('change', function(element) {
                let files = element.target.files[0];
                // console.log(files);
                let labeltxt =files.name;
                let $demoFileSize = files.size;
                let max_demoFileSize;

                $('.fl_box').css("opacity", "0.5");
                if ($inputId == "demoFile") {

                    max_demoFileSize = 1024 * 1024 * 30;//1kb는 1024바이트

                    if ($demoFileSize > max_demoFileSize || (!files.type.match(/video.mp4*/))) {
                        $('#pop_simple').show();
                        // 팝업창 닫기
                        $('.pop_close, .pop_bg, .btn a').on('click', function () {
                            $('.pop_simple').fadeOut(300);
                            $('body').css({
                                'overflow': ''
                            });
                        });
                        $('#' + $inputId).val(null);

                    } else {

                        $label.html(labeltxt);

                        $(this).parent().removeClass('btn').addClass('btn_change');
                    }
                } else if ($inputId == "demoFile2") {

                    max_demoFileSize = 1024 * 1024 * 2;//1kb는 1024바이트

                    if ($demoFileSize > max_demoFileSize || (!$input[0].files[0].type.match(/image.jp*/) && !$input[0].files[0].type.match(/image.png/))) {
                        $('#pop_simple2').show();
                        // 팝업창 닫기
                        $('.pop_close, .pop_bg, .btn a').on('click', function () {
                            $('.pop_simple').fadeOut(300);
                            $('body').css({
                                'overflow': ''
                            });
                        });
                        $('#' + $inputId).val('');

                    } else {
                        $label.html(labeltxt);

                        $(this).parent().removeClass('btn').addClass('btn_change');
                    }
                }
                $('input[type="radio"]').prop('checked', false);

            });
        });
    });

    //API 탭
/*    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }*/

    // Get the element with id="defaultOpen" and click on it
    // document.getElementById("defaultOpen").click();

</script>