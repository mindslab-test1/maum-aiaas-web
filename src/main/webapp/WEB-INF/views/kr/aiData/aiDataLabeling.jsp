<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-01-10
  Time: 16:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<%--  CSS/JS 파일 캐싱 방지 --%>
<%

    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>


<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">

    <!-- Open Graph Tag -->
    <meta property="og:title"            content="maum.ai"/>
    <meta property="og:type"             content="website"/><!-- 웹 페이지 타입 -->
    <meta property="og:url"              content="https://maum.ai"/>
    <meta property="og:image"            content="${pageContext.request.contextPath}/aiaas/common/images/maum.ai_web.png"/>
    <meta property="og:description"      content="웹인공지능이 필요할땐 마음AI"/>

    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/common/css/all.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/reset.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/font.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/login_renew.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/data_common.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/data_labelling.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/data_labelling.js"></script>


    <title>AI 데이터 라벨링 서비스</title>
</head>
<body>
<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<header class="common_header">
    <div class="header_box">
        <h1><a href="/?lang=ko"><img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_title_navy.svg" alt="maum.ai logo"></a>
            <span>인공지능이 필요할 땐 마음AI</span>
        </h1>
        <!--.sta-->
        <div class="sta">
            <a href="/home/pricingPage?lang=ko" class="go_price">가격정책 </a>
            <a href="http://maumacademy.maum.ai/" class="go_academy" target="_blank">마음 아카데미</a>
            <a class="btn_sign" href="javascript:login()">마음AI 로그인</a>
            <!--.etcMenu-->
            <div class="etcMenu">
                <ul>
                    <li class="lang">
                        <p class="lang_select">한국어<em class="fas fa-chevron-down"></em></p>
                        <ul class="lst">
                            <li><a href="/?lang=en" target="_self"><em>English</em></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!--//.etcMenu-->
        </div>
        <!--//.sta-->
    </div>
</header>

<!-- #container -->
<div id="container">
    <!-- .contents -->
    <div class="contents">
        <!-- .stn stn_title -->
        <div class="stn stn_title">
            <!-- .cont_box -->
            <div class="cont_box clearfix">
                <div class="img_box">
                    <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_data_labelling.png" alt="AI 데이터 라벨링 이미지">
                </div>

                <div class="txt_box">
                    <h2 class="title"><span>AI 데이터 라벨링</span> 서비스</h2>
                    <h2 class="m_title"><span>AI 데이터<br>라벨링</span> 서비스</h2>

                    <h3>고품질 인공지능 데이터 구축 서비스를 경험하세요</h3>

                    <p>전문 데이터 사이언티스트의 컨설팅 서비스!<br>
                        문의 내용과 연락처를 남겨주시면 확인 후 바로 연락 드립니다.</p>

                    <button class="btn_inquiry" id="btn1">작업 문의하기</button>
                </div>
            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn stn_title -->

        <!-- .stn stn_service -->
        <div class="stn stn_service">
            <!-- .cont_box -->
            <div class="cont_box clearfix">
                <h3>인공지능을 위한 학습 데이터 수집∙가공∙분석 전문 플랫폼으로,<br>
                    고품질의 데이터셋을 정확하고 빠르게 구축합니다.</h3>

                <div class="cont_lst">
                    <div class="img_box">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_service_list1.png" alt="데이터 구축 리스트 이미지1">
                    </div>

                    <dl>
                        <dt>수집, 가공, 분석하는 전문 시스템</dt>
                        <dd>음성, 텍스트, 시각 데이터를 비롯한 모든 AI 학습용 데이터 서비스를 제공합니다.</dd>
                    </dl>
                </div>

                <div class="cont_lst">
                    <div class="img_box">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_service_list2.png" alt="데이터 구축 리스트 이미지1">
                    </div>

                    <dl>
                        <dt>AI 엔진으로 빠른 전처리 실행</dt>
                        <dd>AI 엔진을 활용한 다양한 분야의 프로젝트 경험을 바탕으로 고객의 인공지능이 최고의 성능을 만들어낼 수 있도록 합니다.</dd>
                    </dl>
                </div>

                <div class="cont_lst">
                    <div class="img_box">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_service_list3.png" alt="데이터 구축 리스트 이미지1">
                    </div>

                    <dl>
                        <dt>데이터 학습효과 측정</dt>
                        <dd>인공지능을 위한 학습 데이터 수집∙가공∙분석 전문 플랫폼으로, 고품질의 데이터셋을 정확하고 빠르게 구축합니다. </dd>
                    </dl>
                </div>
            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn stn_service -->

        <!-- .stn stn_process -->
        <div class="stn stn_process">
            <!-- .cont_box -->
            <div class="cont_box">
                <h3>서비스 프로세스</h3>

                <ol class="process_lst">
                    <li>
                        <em>1</em>
                        <span class="txt">작업 의뢰 및<br>데이터 사이언티스트와 작업 설계</span>
                        <span class="fas fa-angle-double-right"></span>
                    </li>
                    <li>
                        <em>2</em>
                        <span class="txt">AI 엔진으로 데이터 전처리</span>
                        <span class="fas fa-angle-double-right"></span>
                    </li>
                    <li>
                        <em>3</em>
                        <span class="txt">작업 및 검수</span>
                        <span class="fas fa-angle-double-right"></span>
                    </li>
                    <li class="change_lst_num">
                        <em>6</em>
                        <em>4</em>
                        <span class="txt">작업완료</span>
                        <span class="txt_m">데이터 학습효과 측정</span>
                        <span class="fas fa-angle-double-left"></span>
                    </li>
                    <li>
                        <em>5</em>
                        <span class="txt">학습 결과에 따른<br>데이터 설계 조정</span>
                        <span class="fas fa-angle-double-left"></span>
                    </li>
                    <li class="change_lst_num">
                        <em>4</em>
                        <em>6</em>
                        <span class="txt">데이터 학습효과 측정</span>
                        <span class="txt_m">작업완료</span>
                    </li>
                </ol>
            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn stn_process -->

        <!-- .stn stn_case -->
        <div class="stn stn_case">
            <!-- .cont_box -->
            <div class="cont_box">
                <h3>데이터 구축 사례</h3>

                <dl class="case_lst">
                    <dt>서울시 노후경유차 차량번호 인식</dt>
                    <dd>
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_case_list1.png" alt="노후경유차 차량번호 인식 이미지">
                        <span>AI 기반 차량 학습 시스템으로 번호판의 위치와 글씨를 인식해 차량 검출, 차종 인식, 차량 번호판 위치 검출, 번호판 글씨 인식 등 서울시 노후경유차를 단속하는데 활용됩니다.</span>
                    </dd>
                </dl>

                <dl class="case_lst">
                    <dt>수원시 이상행동 cctv</dt>
                    <dd>
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_case_list2.png" alt="이상행동 cctv 화면">
                        <span>이상행동 영상은 프레임 단위로 레이블링을 하여 이상행동에 대해 객체와 동작들의 흐름과 관계를 확인하고, 관련 데이터를 모아서 이상행동 도출 데이터를 얻을 수 있습니다.</span>
                    </dd>
                </dl>

                <dl class="case_lst">
                    <dt>음성인식 / 음성생성 Data</dt>
                    <dd>
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_case_list3.png" alt="음성인식 이미지">
                        <span>음성인식, 음성생성 데이터는 성격에 따라 콜센터, 시니어케어 상담, 낭독체, 대화체 등을 보유하고 있어 사용 목적에 따라 다양한 분야에서 활용될 수 있습니다.</span>
                    </dd>
                </dl>
            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn stn_case -->

        <!-- .stn stn_inquiry -->
        <div class="stn stn_inquiry" id="inquiry">
            <!-- .cont_box -->
            <div class="cont_box">
                <div class="info_box">
                    <h3>전문 데이터 사이언티스트의 컨설팅 서비스</h3>
                    <p class="detail">문의 내용과 연락처를 남겨주시면 확인 후 바로 연락 드립니다.</p>
                    <p class="m_detail">문의 내용과 연락처를 남겨주시면<br>확인 후 바로 연락 드립니다.</p>
                    <p class="number">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_phone.png" alt="전화기">
                        <span>1661&minus;3222</span>
                    </p>
                    <p class="mail">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_mail.png" alt="메일">
                        <span>maumdata@mindslab.ai</span>
                    </p>
                </div>

                <div class="inquiry_box">
                    <%--<form>--%>
                        <div class="my_info">
                            <input type="text" id="office" placeholder="">
                            <label for="office"><span class="fas fa-building"></span>회사명/소속</label>
                        </div>

                        <div class="my_info">
                            <input type="text" id="name">
                            <label for="name"><span class="fas fa-user"></span>이름</label>
                        </div>
                        <div class="my_info">
                            <input type="email" id="mail">
                            <label for="mail"><span class="fas fa-envelope"></span>Email</label>
                        </div>
                        <div class="my_info">
                            <input type="tel" pattern="[0-9]{3}-[0-9]{4}-[0-9]{4}" id="phone" >
                            <label for="phone"><span class="fas fa-mobile-alt"></span>연락처</label>
                        </div>

                        <div class="my_txt">
                            <textarea id="txt" ></textarea>
                            <label for="txt"><span class="fas fa-file-alt"></span>문의내용</label>
                        </div>

                        <button class="btn_inquiry" id="sendMailToHello_aiDataKr">작업 문의하기</button>
                    <%--</form>--%>
                </div>
            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn stn_inquiry -->
    </div>
    <!-- //.contents -->
</div>
<!-- //#container -->

<script type="text/javascript">
    $(window).load(function() {
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
    });
    $(document).ready(function() {

        $('#sendMailToHello_aiDataKr').on('click', function(){

            var title = $("#name").val() + " (" + $("#phone").val() + ") 님의 문의 사항입니다.";
            var msg = "이름 : " + $("#name").val() + "\n연락처 : " + $("#phone").val() + "\n회사 : " + $("#office").val() + "\n문의 내용 : " + $("#txt").val();

            var formData = new FormData();

            formData.append('fromaddr', $('#mail').val());
            formData.append('toaddr', 'maumdata@mindslab.ai');
            formData.append('subject', title);
            formData.append('message', msg);
            formData.append($("#key").val(), $("#value").val());

            if($('.my_info input').val()==='' || $('.my_txt textarea').val()==='') {

                alert("내용을 모두 입력해야 이메일을 전달할 수 있어요!")

            }else{

                $.ajax({
                    type 	: 'POST',
                    async   : true,
                    url  	: '/support/sendContactMail',
                    dataType : 'text',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success : function(obj) {
                        $("#name").val();
                        $("#office").val();
                        $("#phone").val();
                        $("#txt").val();
                        $("#mail").val();

                        alert("이메일을 보냈습니다. 담당자 확인 후 연락 드리겠습니다 :)");
                        window.location.href = "/aiData/krAiDataLabeling";
                    },
                    error 	: function(xhr, status, error) {
                        console.log("error");
                        alert("Contact Us 메일발송 요청 실패하였습니다.");
                        window.location.href = "/aiData/krAiDataLabeling";
                    }
                });
            }
        });

        // language (pc)
        $('.header_box .sta .etcMenu ul li.lang').each(function(){
            $(this).on('click',function(){
                $(this).toggleClass('active');
            });
            $('.header_box .sta .etcMenu ul li.lang ul.lst li a').on('click',function(){
                $(this).removeClass('active');
            });
            $('#container').on('click',function(){
                $('.header_box .sta .etcMenu ul li.lang').removeClass('active');
            });
        });

        // language (mobile)
        $('ul.m_etcMenu li.lang').each(function(){
            $(this).on('click',function(){
                $(this).toggleClass('active');
            });
            $('ul.m_etcMenu li.lang ul.lst li a').on('click',function(){
                $(this).removeClass('active');
            });
        });

    });

</script>

</body>
</html>
