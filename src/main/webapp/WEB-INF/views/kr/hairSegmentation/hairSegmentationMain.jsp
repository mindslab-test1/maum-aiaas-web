<%--
  Created by IntelliJ IDEA.
  User: YGE
  Date: 2020-11-17
  Time: 오후 5:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>



<!-- 5 .pop_simple -->
<div class="pop_simple" id="api_upload_fail_popup">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>이미지 업로드 실패</h5>
            <p>이미지 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
                업로드 되지 않았습니다.</p>
            <span>
                * 지원가능 파일 확장자: .jpg<br>
                * 이미지 용량 : 3MB 이하<br>
                * 이미지 사이즈 : 200x200 px 이상<br>
            </span>
        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents api_content">
    <!-- .content -->
    <div class="content">
        <h1 class="api_tit">헤어 컬러 인식</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'avrdemo')" id="defaultOpen"><button type="button">엔진</button></li>
            <li class="tablinks" onclick="openTap(event, 'avrexample')"><button type="button">적용사례</button></li>
            <li class="tablinks" onclick="openTap(event, 'avrmenu')"><button type="button">매뉴얼</button></li>
            <%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <!-- .demobox -->
        <div class="demobox hairSegmentation" id="avrdemo">
            <p><span>헤어 컬러 인식</span> <small>(Hair Segmentation)</small></p>
            <span class="sub">이미지 속 머리카락 영역을 인식하여 표시해주고, 색상을 분류해줍니다.</span>
            <!--demo_layout-->
            <div class="demo_layout">
                <!--avr_1-->
                <div class="avr_1">
                    <div class="fl_box">
                        <p>
                            <em class="far fa-file-image"></em>
                            <strong>샘플 파일</strong>로 테스트 하기
                        </p>

                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="avr_option" value="plate" checked>
                                    <label for="sample1" class="female">
                                        <div class="img_area">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_hairColor_sample.png" alt="sample img 1">
                                        </div>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="fr_box">
                        <p>
                            <em class="far fa-file-image"></em>
                            <strong>내 파일</strong>로 해보기
                        </p>

                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-image hidden"></em>
                                <label for="demoFile" class="demolabel">이미지 업로드</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".jpg">
                            </div>

                            <div class="result_guide">
                                <p>아래 컬러중에서 인식합니다.</p>
                                <ul class="detect_color">
                                    <li>
                                        <img id="blackHairImg" src="${pageContext.request.contextPath}/aiaas/common/images/img_hairColor_bk.png" alt="헤어컬러인식 샘플이미지">
                                        <span>Black</span>
                                    </li>
                                    <li>
                                        <img id="brownkHairImg" src="${pageContext.request.contextPath}/aiaas/common/images/img_hairColor_bwn.png" alt="헤어컬러인식 샘플이미지">
                                        <span>Brown</span>
                                    </li>
                                    <li>
                                        <img id="blondHairImg" src="${pageContext.request.contextPath}/aiaas/common/images/img_hairColor_bld.png" alt="헤어컬러인식 샘플이미지">
                                        <span>Blond</span>
                                    </li>
                                    <li>
                                        <img id="redHairImg" src="${pageContext.request.contextPath}/aiaas/common/images/img_hairColor_red.png" alt="헤어컬러인식 샘플이미지">
                                        <span>Red</span>
                                    </li>
                                </ul>
                            </div>

                            <ul>
                                <li>* 지원가능 파일 확장자: .jpg</li>
                                <li>* 이미지 용량 : 3MB 이하</li>
                                <li>* 이미지 사이즈 : 200x200 px 이상</li>
                                <li>* 얼굴 정면이 인식되어야 인식이 가능합니다.</li>
                                <li>* 이미지 해상도가 높을수록 인식의 정확도가 높습니다.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">결과보기</button>
                    </div>
                </div>
                <!--avr_1-->

                <!--avr_2-->
                <div class="avr_2">
                    <p><em class="far fa-file-image"></em>이미지 컬러 분석중</p>

                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px"
                             viewBox="0 0 128 16" xml:space="preserve">
                                    <path fill="#fcc6ce" fill-opacity="0.42"
                                          d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/>
                            <g>
                                <path fill="#f7778a" fill-opacity="1"
                                      d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/>
                                <animatetransform attributeName="transform" type="translate"
                                                  values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0"
                                                  calcMode="discrete" dur="1820ms"
                                                  repeatCount="indefinite"/>
                            </g>
                                </svg>
                        <p>약간의 시간이 소요 됩니다.</p>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
                    </div>
                </div>
                <!--avr_2-->
                <!--avr_3-->
                <div class="avr_3">
                    <p class="tit"><em class="far fa-file-image"></em>입력 파일</p>
                    <div class="origin_file file_box">
                        <div class="imgBox">
                            <img id="input_img" src="${pageContext.request.contextPath}/aiaas/common/images/img_hairColor_sample.png" alt="입력 이미지">
                        </div>
                    </div>
                    <%--output --%>

                    <p class="tit"><em class="far fa-file-image"></em>결과 파일</p>
                    <div class="result_file file_box">
                        <div class="result_box">
                            <div class="imgBox">
                                <img id="output_img" src="" alt="결과 이미지">
                            </div>
                            <a id="save" class="btn_dwn" onclick="downloadResultImg();">
                                <em class="far fa-arrow-alt-circle-down"></em> 결과 파일 다운로드
                            </a>
                        </div>

                        <div class="result_box">
                            <div class="result_color">
                                <span>머리색상&colon;</span>
                                <span class="result_img" id="result_color">
                                    <img src="${pageContext.request.contextPath}/aiaas/common/images/img_hairColor_bwn.png" alt="헤어 컬러 인식 이미지">
                                    <em>Brown</em>
                                </span>
                            </div>

                            <div class="result_guide">
                                <p>아래 컬러중에서 인식합니다.</p>
                                <ul class="detect_color">
                                    <li>
                                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_hairColor_bk.png" alt="헤어컬러인식 샘플이미지">
                                        <span>Black</span>
                                    </li>
                                    <li>
                                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_hairColor_bwn.png" alt="헤어컬러인식 샘플이미지">
                                        <span>Brown</span>
                                    </li>
                                    <li>
                                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_hairColor_bld.png" alt="헤어컬러인식 샘플이미지">
                                        <span>Blond</span>
                                    </li>
                                    <li>
                                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_hairColor_red.png" alt="헤어컬러인식 샘플이미지">
                                        <span>Red</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
                    </div>
                </div>
                <!--avr_3-->
            </div>
            <!--// demo_layout-->

            <div class="engineInfo">
                <img src="${pageContext.request.contextPath}/aiaas/common/images/logo_air.png" alt="AIR 로고">
                <img src="${pageContext.request.contextPath}/aiaas/common/images/logo_etri.png" alt="ETRI 로고">
                <p>해당 엔진은 대한민국 과학기술정보통신부가 지원하는 정부 R&D 과제 &ldquo;고령 사회에 대응하기 위한 실환경 휴먼케어로봇 기술 개발(AIR)&rdquo; 중 한국전자통신연구원(ETRI)의 연구과제 성과물 입니다.</p>
            </div>

        </div>
        <!-- //.demobox -->

        <!--.avrmenu-->
        <div class="demobox vision_menu" id="avrmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">API 공통 가이드</div>

                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>

                <div class="guide_group">
                    <div class="title">헤어 컬러 인식 <small>(Hair Segmentation)</small></div>

                    <p class="sub_txt">이미지 속 머리카락 영역을 인식하여 표시해주고, 색상을 분류해줍니다.</p>
                    <span class="sub_title">준비사항</span>
                    <p class="sub_txt">- Input: 이미지 파일</p>
                    <ul>
                        <li>확장자 : .jpg</li>
                        <li>용량 : 3MB 이하 </li>
                        <li>이미지 사이즈 : 200x200px 이상</li>
                        <li>얼굴 정면이 인식되어야 인식이 가능합니다.</li>
                        <li>이미지 해상도가 높을수록 인식의 정확도가 높습니다.</li>
                    </ul>
                    <span class="sub_title">실행 가이드</span>
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/feat/getHairInfo</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>image</td>
                            <td>type : file(.jpg) 이미지 파일</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                                <pre>
curl --location --request POST
'https://api.maum.ai/feat/getHairInfo' \
--header 'Content-Type: multipart/form-data' \
--form 'apiId= 발급받은 API ID' \
--form 'apiKey= 발급받은 API KEY' \
--form 'image= 헤어를 인식할 이미지 파일'
</pre>
                    </div>

                    <p class="sub_txt">④ Response 예제 </p>

                    <div class="code_box">
                                <pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "payload": {
        "resultImage": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",
        "hairInfo": "Black Hair"
    }
}
</pre>
                    </div>
                </div>
            </div>
        </div>
        <!--//avrmenu-->

        <!--.avrexample-->
        <div class="demobox" id="avrexample">
            <p><em style="color:#f7778a;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
            <!-- 가상 염색 서비스 -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>가상 염색 서비스</span>
                            </dt>
                            <dd class="txt">미용실에서 염색 예상 결과 서비스에 활용됩니다.</dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //가상 염색 서비스 -->
        </div>
        <!--//.avrexample-->


    </div>
    <!-- //.content -->
</div>

<script type="text/javascript">
    var request = null;
    var sample1File;
    var sample1SrcUrl;

    //파일명 변경
    document.querySelector("#demoFile").addEventListener('change', function (ev) {

        let file = this.files[0];

        if (file === undefined || file === null || file == "") { return; }

        let fileName = file.name;
        let fileSize = file.size;
        let maxSize = 1024 * 1024 * 3; //3MB

        if (!file.type.match(/image\/jpeg/) ) {
            console.log(".jpg 파일이 아닙니다.");
            this.value = null;
            $('#api_upload_fail_popup').fadeIn(300);

        } else if(fileSize > maxSize) {
            console.log("3MB 이하의 파일을 업로드해 주세요.");
            this.value = null;
            $('#api_upload_fail_popup').fadeIn(300);

        } else {
            let size = (fileSize / 1048576).toFixed(3); //size in mb
            $('input[type="radio"]:checked').prop("checked", false);
            $('.demolabel').text(fileName + ' (' + size + 'MB)');
            $('#uploadFile').removeClass('btn').addClass('btn_change');
            $('.fl_box').css("opacity", "0.5");
        }

    });


    jQuery.event.add(window,"load",function(){

        //샘플
        function loadSample1() {
            let blob = null;
            let xhr = new XMLHttpRequest();
            xhr.open("GET", "/aiaas/common/images/img_hairColor_sample.png");
            xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
            xhr.onload = function()
            {
                blob = xhr.response;//xhr.response is now a blob object
                sample1File = new File([blob], "img_hairColor_sample.png");
                sample1SrcUrl = URL.createObjectURL(blob);
            };

            xhr.send();
        }


        $(document).ready(function (){

            loadSample1();

            $('.radio label').on('click',function(){
                $('.fl_box').attr('opacity',1);
                $('em.close').click();
            });

            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });


            // step1->step2  (close button)
            $('em.close').on('click', function () {
                $('#demoFile').val(null);
                $('.demolabel').text('이미지 업로드');
                $(this).parent().removeClass("btn_change").addClass("btn");
                $('.fl_box').css("opacity", "1");
            });


            // 처음으로 버튼
            $('.btn_back1').on('click', function () {
                request.abort();
                $('.avr_2').hide();
                $('.avr_1').fadeIn(300);
            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('.avr_3').hide();
                $('.avr_1').fadeIn(300);
            });

            //결과보기 버튼
            $('#sub').on('click',function (){

                let $checked = $('input[type="radio"]:checked');
                let $demoFile = $("#demoFile");
                let formData = new FormData();
                let inputFile;
                let url;

                //내 파일로
                if ($checked.length === 0) {
                    if ($demoFile.val() === "" || $demoFile.val() === null) {
                        alert("샘플을 선택하거나 파일을 업로드해 주세요.");
                        return 0;
                    }
                    inputFile = $demoFile.get(0).files[0];
                    url = URL.createObjectURL(inputFile);
                }
                //샘플로
                else {
                    url = sample1SrcUrl;
                    inputFile = sample1File;
                }

                formData.append('image',inputFile);
                formData.append('${_csrf.parameterName}', '${_csrf.token}');

                $('#input_img').attr('src', url);
                $('.avr_1').hide();
                $('.avr_2').fadeIn(300);

                request = $.ajax({
                    type: "POST",
                    async: true,
                    url: '/api/hairSegmentation', //여기 url 추가
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(result){
                        /*결과 오류*/
                        if(result === undefined || result === ""){
                            setResultError_UI();
                            showResultDiv_UI();
                            return;
                        }

                        let resultData = JSON.parse(result);

                        if(resultData.payload.hairInfo === undefined){
                            setResultError_UI();
                            showResultDiv_UI();
                            return;
                        }

                        /*결과 정상*/
                        let resultColor = resultData.payload.hairInfo;

                        $('#result_color em').text(resultColor);
                        $('#output_img').attr('src', "data:image/jpeg;base64," + resultData.payload.resultImage);

                        if(resultColor.search("Black") !== -1){
                            $('#result_color img').attr('src', $('#blackHairImg').attr('src'));
                        }else if(resultColor.search("Brown") !== -1){
                            $('#result_color img').attr('src', $('#brownkHairImg').attr('src'));
                        }else if(resultColor.search("Blond") !== -1){
                            $('#result_color img').attr('src', $('#blondHairImg').attr('src'));
                        }else if(resultColor.search("Red") !== -1){
                            $('#result_color img').attr('src', $('#redHairImg').attr('src'));
                        }

                        showResultDiv_UI();
                    },

                    error: function(jqXHR, error){
                        if(jqXHR.status === 0){
                            return false;
                        }

                        alert("서버와 연결이 되지 않습니다.\n잠시 후에 다시 이용해주세요.");
                        console.dir(error);
                        window.location.reload();
                    }
                });


            });

        });
    });


    function downloadResultImg(){
        let img = document.getElementById('output_img');
        let link = document.getElementById("save");
        link.href = img.src.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
        link.download = "hairSegmentation.jpg";
    }



    function setResultError_UI(){
        $('#result_color em').text("");
        $('#result_color img').attr('src',"");
        $('#output_img').attr('src',"");
    }
    function showResultDiv_UI(){
        $('.avr_1').hide();
        $('.avr_2').hide();
        $('.avr_3').fadeIn(300);
    }



    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();


</script>
