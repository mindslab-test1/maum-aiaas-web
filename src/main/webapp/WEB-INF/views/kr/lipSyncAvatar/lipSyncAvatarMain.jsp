<%--
  Created by IntelliJ IDEA.
  User: YGE
  Date: 2020-12-03
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!-- .contents -->
<div class="contents api_content">
    <!-- .content -->
    <div class="content">
        <h1 class="api_tit">Lip Sync Avatar</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'avatardemo')" id="defaultOpen">
                <button type="button">엔진</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'avatarexample')">
                <button type="button">적용사례</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'avatarmenu')">
                <button type="button">매뉴얼</button>
            </li>
        </ul>

        <!-- .demobox -->
        <div class="demobox lipSyncAvatar" id="avatardemo">
            <p><span>Lip Sync Avatar</span></p>
            <span class="sub">입력한 텍스트에 따라 자연스럽게 말하는 아바타가 생성됩니다.</span>
            <!--demo_layout-->
            <div class="demo_layout">
                <!--avr_1-->
                <div class="avr_1">

                    <div class="avatar_box">
                        <p>
                            <em class="far fa-image"></em>
                            <strong>아바타 선택</strong>
                        </p>

                        <!-- [D] 210316 AMR bg_list 추가 및 구조 수정 -->
                        <div class="bg_list sample">
                            <!-- [D] sample_list에서 선택된 input:radio의 index와 동일한 sample_box에 "active" class가 추가되어야 합니다. -->
                            <div class="sample_list">
                                <div class="radio">
                                    <input type="radio" id="avatar01" name="avatar" value="short_lipsync_server" checked> <!-- value : 모델명 -->
                                    <label for="avatar01" data-text="안녕하세요? 2021년 마인즈랩 마음 AI 아바타 인사드립니다.">
                                        아나운서
                                    </label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="avatar02" name="avatar" value="lipsync_curator_woman">
                                    <label for="avatar02" data-text="이번에는 고흐의 소개와 그림의 특징에 관해 자세히 소개하도록 하겠습니다.">
                                        도슨트
                                    </label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="avatar03" name="avatar" value="lipsync_teacher_man">
                                    <label for="avatar03" data-text="안녕하세요, 여러분, 즐거운 한국사 시간입니다. 모두 교재 준비되었나요?">
                                        남선생님
                                    </label>
                                </div>
                            <sec:authorize access="hasAnyRole('ADMIN', 'INTERNAL')">
                                <div class="radio" style="margin-top: 15px;">
                                    <input type="radio" id="avatar06" name="avatar" value="michael">
                                    <label for="avatar06" data-text="안녕하세요 고객 여러분. 저는 마인즈랩 마이클입니다.">
                                        마이클
                                    </label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="avatar05" name="avatar" value="pcmcolor_lipsync_server">
                                    <label for="avatar05" data-text="시청자 여러분 안녕하세요. 저는 AI 아나운서 박철민입니다.">
                                        박철민 아나운서
                                    </label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="avatar07" name="avatar" value="sbh">
                                    <label for="avatar07" data-text="안녕하세요 손병희 교수입니다. 오늘은 비대면 수업을 진행하도록 하겠습니다.">
                                        손병희
                                    </label>
                                </div>
                            </sec:authorize>
                                <!-- [D] 210315 AMR 도슨트와 안내원은 추후에 추가될 예정이라고 전달받았습니다. -->
                                <!-- <div class="radio">
                                    <input type="radio" id="avatar04" name="avatar">
                                    <label for="avatar04">여자 도슨트</label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="avatar05" name="avatar">
                                    <label for="avatar05">여자 안내원</label>
                                </div> -->
                            </div>

                            <div class="sample_view">
                                <!-- [D] sample_list에서 선택된 input:radio의 index와 동일한 sample_box에 "active" class가 추가되어야 합니다. -->
                                <div class="sample_box active">
                                    <div class="video_box">
                                        <video src="${pageContext.request.contextPath}/aiaas/common/video/sample_M1_sora.mp4" controls></video>
                                    </div>
                                </div>
                                <div class="sample_box">
                                    <div class="video_box">
                                        <video src="${pageContext.request.contextPath}/aiaas/common/video/curator_tilt.mp4" controls></video>
                                    </div>
                                </div>
                                <div class="sample_box">
                                    <div class="video_box">
                                        <video src="${pageContext.request.contextPath}/aiaas/common/video/lipSync_model_teacherM.mp4" controls></video>
                                    </div>
                                </div>
                            <sec:authorize access="hasAnyRole('ADMIN', 'INTERNAL')">
                                <div class="sample_box">
                                    <div class="video_box">
                                        <video src="${pageContext.request.contextPath}/aiaas/common/video/lipsync_michael.mp4" controls></video>
                                    </div>
                                </div>
                                <div class="sample_box">
                                    <div class="video_box">
                                        <video src="${pageContext.request.contextPath}/aiaas/common/video/pcmcolor_lipsync_server.mp4" controls></video>
                                    </div>
                                </div>
                                <div class="sample_box">
                                    <div class="video_box">
                                        <video src="${pageContext.request.contextPath}/aiaas/common/video/lipsync_sbh.mp4" controls></video>
                                    </div>
                                </div>
                            </sec:authorize>
                                <!-- [D] 210315 AMR 도슨트와 안내원은 추후에 추가될 예정이라고 전달받았습니다. -->
                                <!-- <div class="sample_box">
                                    <div class="video_box">
                                        <video src="common/video/lipSync_sample.mp4" controls autoplay></video>
                                    </div>
                                </div>
                                <div class="sample_box">
                                    <div class="video_box">
                                        <video src="common/video/lipSync_sample.mp4" controls autoplay></video>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>

                    <div class="bg_box">
                        <p>
                            <em class="far fa-image"></em>
                            <strong>화질 선택</strong>
                        </p>

                        <ul class="bg_list rslt_slt">
                            <li>
                                <div class="radio">
                                    <input type="radio" id="HighDefinition" value="HD" name="resolution" checked>
                                    <label for="HighDefinition">고화질 (720p)</label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="StandardDefinition" value="SD" name="resolution">
                                    <label for="StandardDefinition">일반화질 (360p)</label>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="bg_box">
                        <p>
                            <em class="far fa-image"></em>
                            <strong>배경 선택</strong>
                        </p>

                        <ul class="bg_list">
                            <li>
                                <div class="radio">
                                    <input type="radio" id="bg1" name="bg" checked>
                                    <label for="bg1">
                                        <div class="img_area">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_lipSync_bg1.png" alt="배경 이미지1">
                                        </div>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="bg2" name="bg">
                                    <label for="bg2">
                                        <div class="img_area">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_lipSync_bg2.png" alt="배경 이미지2">
                                        </div>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="bg3" name="bg">
                                    <label for="bg3">
                                        <div class="img_area">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_lipSync_bg3.png" alt="배경 이미지3">
                                        </div>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="bg4" name="bg">
                                    <label for="bg4">
                                        <div class="img_area">
                                            <span>배경없음</span>
                                        </div>
                                    </label>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="txt_box">
                        <p>
                            <em class="far fa-keyboard"></em>
                            <strong>듣고 싶은 문장을 직접 입력해주세요.</strong>
                        </p>

                        <div class="textarea">
                            <!-- [D] placeholder외에 기본으로 입력되는 문장(안녕하세요?~인사드립니다.)은 유지해주세요 -->
                            <textarea id="text-contents" class="lipSync_txt" placeholder="아바타가 말할 텍스트를 50자 이내로 입력하세요." maxlength="50">안녕하세요? 2021년 마인즈랩 마음 AI 아바타 인사드립니다.</textarea>
                        </div>

                        <span class="txt_length"><strong id="count"></strong> &sol; 50자</span>
                        <ul class="info_txt">
                            <li>*현재 모델은 한국어에 최적화되어있습니다.</li>
                            <li>*아바타 및 TTS 모델은 커스터마이징 학습이 가능합니다.</li>
                            <li>*특수문자(예: #, *, ~ ), 특수단위(예: ㎖, ㎡, ㎧ ㎉ 등), 다른 언어 등 현재 TTS 모델에서 수정이 필요한 경우, 고객센터로 문의주세요.</li>
                        </ul>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">생성하기</button>
                    </div>
                </div>
                <!--avr_1-->
                <!--avr_2-->
                <div class="avr_2">
                    <p><em class="far fa-file-video"></em>아바타 생성중</p>

                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve">
                                    <path fill="#fcc6ce" fill-opacity="0.42"
                                          d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z" />
                            <g>
                                <path fill="#f7778a" fill-opacity="1"
                                      d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z" />
                                <animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms"
                                                  repeatCount="indefinite" />
                            </g>
                                </svg>
                        <!-- [D] .waiting_num에 현재 사용인원 넣어주세요 -->
                        <p id="waitingText">현재 <span class="waiting_num">0</span>명의 사용자가 사용하고 있습니다.<br>조금만 기다려주세요.</p>
                        <p id="processingText">처리중입니다. 약간의 시간이 소요됩니다.</p>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
                    </div>
                </div>
                <!--avr_2-->
                <!--avr_3-->
                <div class="avr_3">
                    <p class="tit"><em class="far fa-file-video"></em>결과 파일</p>
                    <div class="result_file file_box">
                        <div class="result_box">
                            <video id="outputVideo" src="" controls autoplay></video>
                        </div>

                        <div class="txt_box">
                            <span>입력 문장</span>
                            <div class="textarea">
                                <p id="outputText" class="input_"></p>
                            </div>
                        </div>

                        <a id="save" class="btn_dwn" download="lipSyncAvatar.mp4" href="">
                            <em class="far fa-arrow-alt-circle-down"></em> 결과 파일 다운로드
                        </a>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
                    </div>
                </div>
                <!--avr_3-->
            </div>
            <!--//.demo_layout-->
        </div>
        <!-- //.demobox -->

        <!--.avrmenu-->
        <div class="demobox vision_menu" id="avatarmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">API 공통 가이드</div>

                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>

                <div class="guide_group">
                    <div class="title">Lip Sync Avatar</div>

                    <p class="sub_txt">입력한 텍스트에 따라 자연스럽게 말하는 아바타가 생성됩니다.</p>
                    <span class="sub_title">준비사항</span>
                    <p class="sub_txt">- Input 1: 듣고 싶은 문장 텍스트</p>
                    <ul>
                        <li>제약사항 1) : 1000자 이내의 문장(한글 기준)</li>
                        <li>제약사항 2) : 긴 문장을 넣을수록 동영상 처리하는 시간이 오래 걸립니다</li>
                    </ul>
                    <p class="sub_txt">- Input 2: 배경 이미지 파일</p>
                    <ul>
                        <li>확장자 : .jpg, .png</li>
                        <li>용량 : 10MB 이하 </li>
                    </ul>
                    <span class="sub_title">실행 가이드</span>

                    <!-- Upload START-->
                    <em>&#8203;Upload : 발화할 텍스트 및 배경 이미지를 넣고 request key를 받아옵니다.</em>
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/lipsync/upload</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>text</td>
                            <td>문장 (*1000자 이내)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>image</td>
                            <td>(optional) 배경 이미지</td>
                            <td>file</td>
                        </tr>
                        <tr>
                            <td>model</td>
                            <td>
                                (optional) 아바타 모델 이름 (default = baseline)
                            </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>transparent</td>
                            <td>
                                (optional) 배경 투명 여부 (default = false)
                                <br>* transparent=true인데 image가 주어졌을 경우 transparent 옵션은 무시됩니다.
                            </td>
                            <td>boolean</td>
                        </tr>
                        <tr>
                            <td>resolution</td>
                            <td>(optional) 아바타 제작 화질(HD, SD). (default = HD)</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                                <pre>
curl --location --request POST
'https://api.maum.ai/lipsync/upload' \
--header 'Content-Type: multipart/form-data' \
--form 'apiId= "발급받은 API ID"' \
--form 'apiKey= "발급받은 API KEY"' \
--form 'text= "안녕하세요? 2021년 마인즈랩 마음 AI 아바타 인사드립니다."' \
--form 'image= @"{이미지 파일}"' \
--form 'model="baseline"' \
--form 'transparent="true"' \
--form 'resolution="HD"'
</pre>
                    </div>

                    <p class="sub_txt">④ Response 파라미터 설명 </p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message </td>
                            <td>API 동작여부</td>
                            <td>object</td>
                        </tr>
                        <tr>
                            <td>payload</td>
                            <td>결과</td>
                            <td>object</td>
                        </tr>
                    </table>

                    <span class="table_tit">message: API 동작여부</span>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>요청 처리 상태를 설명하는 문자열  (Success/ Fail)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>요청 처리 상태에 대한 status code (0: Success)</td>
                            <td>number</td>
                        </tr>
                    </table>

                    <span class="table_tit">payload: 결과</span>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>requestKey</td>
                            <td>아바타 생성을 위한 request Key</td>
                            <td>string</td>
                        </tr>
                    </table>

                    <p class="sub_txt">⑤ Response 예제 </p>

                    <div class="code_box">
                                <pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "payload": {
        "requestKey": "요청 key"
    }
}
</pre>
                    </div>
                    <!-- Upload END-->


                    <!-- Status check START-->
                    <em>&#8203;Status Check : 요청의  처리 상태를 확인합니다.</em>
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/lipsync/statusCheck</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>requestKey</td>
                            <td>Upload 요청시 발급받은 요청 식별용 key</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                                <pre>
curl --location --request POST
'https://api.maum.ai/lipsync/statusCheck' \
--header 'Content-Type: application/json' \
--form 'apiId= "발급받은 API ID"' \
--form 'apiKey= "발급받은 API KEY"' \
--form 'requestKey= "Upload 요청시 발급받은 요청 식별용 key"'
</pre>
                    </div>

                    <p class="sub_txt">④ Response 파라미터 설명 </p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message </td>
                            <td>API 동작여부</td>
                            <td>object</td>
                        </tr>
                        <tr>
                            <td>payload</td>
                            <td>결과</td>
                            <td>object</td>
                        </tr>
                    </table>

                    <span class="table_tit">message: API 동작여부</span>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>요청 처리 상태를 설명하는 문자열  (Success/ Fail)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>요청 처리 상태에 대한 status code (0: Success)</td>
                            <td>number</td>
                        </tr>
                    </table>

                    <span class="table_tit">payload: 결과</span>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>statusCode</td>
                            <td>요청 처리 상태 status code<br/>
                                &emsp;0 : Not yet processing started<br/>
                                &emsp;1 : Processing<br/>
                                &emsp;2 : Done<br/>
                                &emsp;3 : Invalid Request Key<br/>
                                &emsp;4 : Processing Error<br/>
                                &emsp;5 : Deleted
                            </td>
                            <td>number</td>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>요청 처리 상태 메시지</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>waiting</td>
                            <td>처리 대기중인 요청 갯수</td>
                            <td>number</td>
                        </tr>
                    </table>

                    <p class="sub_txt">⑤ Response 예제 </p>

                    <div class="code_box">
                        <pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "payload": {
        "statusCode": 1,
        "message": "Now Processing",
        "waiting": 0
    }
}
                        </pre>
                    </div>
                    <!-- Status check END-->



                    <!-- Download START-->
                    <em>&#8203;Download : 처리 완료된 영상을 다운로드 받습니다.</em>
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/lipsync/download</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>requestKey</td>
                            <td>Upload 요청시 발급받은 요청 식별용 key</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                                <pre>
curl --location --request POST
'https://api.maum.ai/lipsync/download' \
--header 'Content-Type: application/json' \
--form 'apiId= "발급받은 API ID"' \
--form 'apiKey= "발급받은 API KEY"' \
--form 'requestKey= "Upload 요청시 발급받은 요청 식별용 key"'
</pre>
                    </div>
                    <p class="sub_txt">④ Response 설명 </p>
                    <div class="code_box">정
                        Content-Type : application/octet-stream<br/>
                        (.mp4 File Download)
                    </div>
                    <!-- Download END-->



                </div>
            </div>
        </div>
        <!--//avrmenu-->


        <!--.avatarexample-->
        <div class="demobox" id="avatarexample">
            <p><em style="color:#f7778a;font-weight: 400;">적용사례</em> <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
            <!-- Lip Sync Avatar -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>영상 제작</span>
                            </dt>
                            <dd class="txt">뉴스 또는 인터뷰와 같이 정보 전달 영상을 제작할 때, 가상의 인물의 음성과 영상으로 함께 나타낼 수 있습니다.</dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>가상 비서<strong>(Virtual Assistant)</strong></span>
                            </dt>
                            <dd class="txt">개인 비서처럼 사용자가 요구하는 작업을 처리하고 사용자에게 특화된 서비스를 제공하는 소프트웨어 에이전트에 활용</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_bot"><span>챗봇</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>키오스크<strong>가상 컨시어지</strong></span>
                            </dt>
                            <dd class="txt">호텔 컨시어지에 필요한 모든 서비스를 제공해주는 버추얼 컨시어지의 형태로 활용되어집니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_conciergeBot"><span>호텔 컨시어지 챗봇</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //Lip Sync Avatar -->
        </div>
        <!--//.avatarexample-->
    </div>
    <!-- //.content -->
</div>
<script>

    let sampleFileList = [
        {filePath: "${pageContext.request.contextPath}/aiaas/common/images/img_lipSync_bg1.png", fileName : "img_lipSync_bg1.png", file : null},
        {filePath: "${pageContext.request.contextPath}/aiaas/common/images/img_lipSync_bg2.png", fileName : "img_lipSync_bg2.png", file : null},
        {filePath: "${pageContext.request.contextPath}/aiaas/common/images/img_lipSync_bg3.png", fileName : "img_lipSync_bg3.png", file : null},
        <%--{filePath: "${pageContext.request.contextPath}/aiaas/common/images/img_lipSync_bg4.png", fileName : "img_lipSync_bg4.png", file : null},--%>
        {filePath: "", fileName : "transparent", file : null}
    ]

    let request = null;
    let timerId = null;
    let apiUrl = "${apiUrl}";


    function loadSample(fileObj, index){
        let blob = null;
        let xhr = new XMLHttpRequest();
        xhr.open("GET", fileObj.filePath);
        xhr.responseType = "blob";
        xhr.onload = function(){
            blob = xhr.response;
            sampleFileList[index].file = new File([blob], fileObj.fileName);
        }
        xhr.send();
    }


    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {

            // 샘플 이미지 로드
            sampleFileList.forEach((file, idx)=>{
                if(file.fileName !== "transparent"){
                    loadSample(file, idx);
                }
            });

            // 글자수 count
            countTextAreaLength();

            // 아바타 선택시 문장 변경
            $('.sample_list .radio label').on('click', function(){
                $('.sample_view video').each(function (idx, el){
                    $(el).get(0).pause();
                });
                $('.sample_view .sample_box').removeClass('active');
                $('.sample_view .sample_box').eq($('.sample_list .radio label').index($(this))).addClass('active');
                $('#text-contents').val($(this).data("text"));
                countTextAreaLength();
            });


            // step2->step1
            $('.btn_back1').on('click', function () {
                if(timerId != null) clearTimeout(timerId);
                if(request) request.abort();

                $('.avr_2').hide();
                $('.avr_1').fadeIn(300);
            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('#outputVideo').get(0).pause();
                $('.avr_3').hide();
                $('.avr_1').fadeIn(300);
            });

            // 문장 입력 글자수
            $('.lipSync_txt').on('input keyup paste change', countTextAreaLength);


            // api 호출
            $('#sub').on('click', function () {

                $('.sample_view video').each(function (idx, el){
                    $(el).get(0).pause();
                });

                let text = $('#text-contents').val();

                if(text.trim() === ""){
                    alert("듣고 싶은 문장을 입력해주세요.");
                    return false;
                }

                let selectedIdx = $('.radio input[name=bg]').index( $('.radio input[name=bg]:checked') );
                let image = sampleFileList[selectedIdx].file;
                let model = $('.radio input[name=avatar]:checked').val();
                let transparent = (selectedIdx === 3);

                let resolution = $('.radio input[name=resolution]:checked').val();
                let width = 0;
                let height = 0;
                if(resolution == 'HD') {    // 720P인 경우
                    width = 1280;
                    height = 720
                } else if (resolution == 'SD') {    // 360P인 경우
                    width = 640;
                    height = 360;
                }

                requestUpload(image, text, model, transparent, width, height);

                $('#processingText').show();
                $('#waitingText').hide();
                $('#outputText').text(text);

                $('.avr_1').hide();
                $('.avr_2').fadeIn(200);
            });

        });
    });


    function requestUpload(image, text, model, transparent, width, height){
        console.log("image :" + image + "\ntext :" + text + "\nmodel : " + model + "\ntransparent : " + transparent + "\nwidth : " + width + "\nheight : " + height);

        let formData = new FormData();
        formData.append('image', image);
        formData.append('text', text);
        formData.append('model', model);
        formData.append('transparent', transparent);
        formData.append('width', width);
        formData.append('height', height);
        formData.append('${_csrf.parameterName}', '${_csrf.token}');

        request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if(request)
            if(request.readyState === 4 && request.status === 200) {
                if(!request.response){
                    handleError("Empty response : upload api");
                    return;
                }
                let reqKey = JSON.parse(request.response).payload.requestKey;
                callBackUpload(reqKey);
            }
        };
        request.open('POST', '/api/lipSyncAvatar');
        request.send(formData);
        request.timeout = 60000;
        request.ontimeout = function() { handleError("Timeout error : upload api"); }
        request.onabort = function(){ }
    }


    /** statusCheck API의 STATUS 값에 따라 분기 처리
     * 0 : 대기중 - 대기중인 사용자의 수를 화면에 보여줌 -> setTimeout으로 다시 api call
     * 1 : 처리중 - 대기가 끝나고 api 처리를 기다리는 중, 처리중 문구를 보여줌 -> setTimeout으로 다시 api call
     * 2 :  완료  - handleSuccess()
     * 3 : WRONG_KEY
     * 4 : ERROR
     * 5 : DELETED
    */
    function callBackUpload(reqKey){

        console.log("%c callBackUpload()",'color:green');

        timerId = setTimeout(function delayFunc(){

            getStatusPromise(reqKey).then((res) => {
                console.log("%c getStatusPromise() -> then()",'color:green', reqKey);
                if(res){
                    let payload = res;
                    console.log("STATUS : ", payload.statusCode, payload.message);
                    switch (payload.statusCode){
                        case 0:
                            $('#waitingText .waiting_num').text(payload.waiting);
                            $('#processingText').hide();
                            $('#waitingText').show();
                            timerId = setTimeout(delayFunc, 2000);
                            break;
                        case 1:
                            $('#waitingText .waiting_num').text("");
                            $('#waitingText').hide();
                            $('#processingText').show();
                            timerId = setTimeout(delayFunc, 2000);
                            break;
                        case 2:
                            handleSuccess(reqKey);
                            break;
                        default:
                            handleError("statusCheck message : " + payload.message);
                            return;
                    }
                }else{
                    handleError("Empty response : statusCheck api");
                }

            })
            .catch((errMsg) => {
                handleError(errMsg);
            });
        }, 0);
    }


    // 상태 확인 api
    function getStatusPromise(reqKey){
        let reqJson = {
            "apiId" : "${fn:escapeXml(sessionScope.accessUser.apiId)}",
            "apiKey": "${fn:escapeXml(sessionScope.accessUser.apiKey)}",
            "requestKey": reqKey
        }

        return new Promise((resolve, reject) => {
            request = new XMLHttpRequest();
            request.onreadystatechange = function(){
                if(request.readyState === XMLHttpRequest.DONE){
                    if(request.status === 200 ){
                        resolve(JSON.parse(request.response).payload);
                    }else{
                        console.log(request.response);
                        reject('Error: in call getStatus with response status - ' + request.status);
                    }
                }
            };
            request.open('POST', apiUrl + '/lipsync/statusCheck');
            request.setRequestHeader("Content-Type", "application/json");
            request.send(JSON.stringify(reqJson));
            request.timeout = 60000;
            request.onerror = () => {
                reject('Error\n'
                    + "status : " + request.status
                    + "\n message : " + request.response);
            }
            request.onabort = function(){ }
        });
    }

    function handleSuccess(reqKey){

        console.log("%c handleSuccess() ",'color:blue');

        getOutputPromise(reqKey)
        .then((res)=>{
            let result = new Blob([res], {type: 'video/mp4'});
            let srcUrl = URL.createObjectURL(result);

            $('#outputVideo').attr('src', srcUrl);
            $('#save').attr('href', srcUrl );

            $('.avr_2').fadeOut(200);
            $('.avr_3').fadeIn(200);
        })
        .catch((errMsg)=>{
            handleError(errMsg);
        });
    }

    function handleError(errorMsg){
        alert(errorMsg);
        window.location.reload();
    }


    // 결과 요청 api
    function getOutputPromise(reqKey){

        let reqJson = {
            "apiId" : "${fn:escapeXml(sessionScope.accessUser.apiId)}",
            "apiKey": "${fn:escapeXml(sessionScope.accessUser.apiKey)}",
            "requestKey": reqKey
        }

        return new Promise((resolve, reject) => {
            request = new XMLHttpRequest();
            request.responseType = 'blob';
            request.onreadystatechange = function(){
                if(request.readyState === XMLHttpRequest.DONE){
                    if(request.status === 200 ){
                        request.response == null
                            ? reject("Empty response : output download api")
                            : resolve(request.response);
                    }
                    else{
                        reject('Error: ' + request.status);
                    }
                }
            };
            request.open('POST', apiUrl + '/lipsync/download');
            request.setRequestHeader("Content-Type", "application/json");
            request.send(JSON.stringify(reqJson));
            request.timeout = 60000;
            request.onerror = () => {
                reject('Error\n'
                    + "status : " + request.status
                    + "\n message : " + request.response);
            }
            request.onabort = function(){
                console.error("abort : getDownload api");
            }
        });
    }


    function countTextAreaLength (){
        $('#count').html($('#text-contents').val().length);
    }


    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

</script>