<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


	<!-- 5 .pop_simple -->
	<div class="pop_simple " id="api_fail_popup">
		<div class="pop_bg"></div>
		<!-- .popWrap -->
		<div class="popWrap" style="position: inherit">
			<button class="pop_close" type="button">팝업 닫기</button>
			<!-- .pop_bd -->
			<div class="pop_bd">
				<em class="fas fa-exclamation-triangle"></em>
				<p style="font-size: small"><strong></strong></p>
				<p style="font-size: small">서버에서 응답을 받아오지 못했습니다.<br>다시 시도해 주세요.</p>

			</div>
			<!-- //.pop_bd -->
			<div class="btn" id="close_api_fail_popup">
				<a class="">확인</a>
			</div>
		</div>
		<!-- //.popWrap -->
	</div>
	<!-- //.pop_simple -->

	<!-- .contents -->
	<div class="contents">        	
		<div class="content api_content">
			<h1 class="api_tit">음성생성</h1>
            <ul class="menu_lst voice_menulst">
                <li class="tablinks" onclick="openTap(event, 'ttsdemo')" id="defaultOpen"><button type="button">엔진</button></li>
				<li class="tablinks" onclick="openTap(event, 'ttsexample')"><button type="button">적용사례</button></li>
                <li class="tablinks" onclick="openTap(event, 'ttsmenu')"><button type="button">매뉴얼</button></li>
<%--                <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
            </ul>
			<div class="demobox ttsdemo apittsdemo" id="ttsdemo">
				<p>자연스러운 <em>음성생성</em>  <small>(Speech Generation)</small></p>
				<span class="sub">실제 목소리처럼 자연스러운 음성을 만듭니다.</span>
				<div class="selectarea">
					<div class="radio">
						<input type="hidden" id="voice1" value="인공지능의 모든 것, 마음AI에서 경험해보세요. 다양한 엔진들을 만나 보실 수 있습니다.">
						<input type="radio" id="1" name="voice" data-lang="ko_KR" data-speakerid="" checked="checked">
						<label for="1">
							<span><strong>한국어</strong> 성인 여자</span>
							<div class="btn_bar">
								<div class="tit"><p>보이스 선택 </p><em class="fas fa-caret-down"></em></div>
								<ul>
									<li>보이스 선택</li>
									<li data-speakerid="kor_female1">차분한</li>
									<li data-speakerid="kor_female2">발랄한</li>
									<li data-speakerid="kor_female3">자연스러운</li>
									<li data-speakerid="kor_female4">정직한</li>
									<li data-speakerid="kor_female5">개성있는</li>
<%--									<li data-speakerid="baseline_kor">친근한</li>--%>
								</ul>
							</div>
						</label>

					</div>
					<div class="radio">
						<input type="hidden" id="voice2" value="인공지능이 필요할 땐, 마음AI를 찾아주세요. 99,000원에 모두 이용 가능합니다.">
						<input type="radio" id="2" name="voice" data-lang="kor_kids" data-speakerid="">
						<label for="2">
							<span><strong>한국어</strong> 성인 남자</span>
							<div class="btn_bar">
								<div class="tit"><p>보이스 선택</p> <em class="fas fa-caret-down"></em></div>
								<ul>
									<li>보이스 선택</li>
									<li data-speakerid="kor_male1">자연스러운</li>
									<li data-speakerid="kor_male2">진중한</li>
									<li data-speakerid="kor_male3">차분한</li>
									<li data-speakerid="kor_male4">친근한</li>
									<li data-speakerid="kor_male5_cheolmin">박철민 아나운서</li>
								</ul>
							</div>
						</label>
					</div>
					<div class="radio">
						<input type="hidden" id="voice3" value="마인즈랩의 음성생성 기술은 아무도 따라할 수 없어, 세계 최고인것 같아.">
						<input type="radio" id="3" name="voice" data-lang="kor_kids" data-speakerid="">
						<label for="3">
							<span><strong>한국어</strong> 남/여 아이</span>
							<div class="btn_bar">
								<div class="tit"><p>보이스 선택 </p> <em class="fas fa-caret-down"></em></div>
								<ul>
									<li>보이스 선택</li>
									<li data-speakerid="kor_kids_m">남자아이</li>
									<li data-speakerid="kor_kids_f">여자아이</li>
								</ul>
							</div>
						</label>
					</div>
					<div class="radio">
						<input type="hidden" id="voice4" value="Minds Lab's Voice Generation is not a voice synthesis. It is a state of the art future technology.">
						<input type="radio" id="4" name="voice" data-lang="en_US" data-speakerid="">
						<label for="4">
							<span><strong>영어</strong> 남/여 성인</span>
							<div class="btn_bar">
								<div class="tit"><p>보이스 선택 </p> <em class="fas fa-caret-down"></em></div>
								<ul>
									<li>보이스 선택</li>
									<li data-speakerid="eng_male1">Brandon</li>
									<li data-speakerid="baseline_eng">Selena</li>
								</ul>
							</div>
						</label>
					</div>
				</div>

				<div class="tts_demo">
					<!-- .tabUi -->
					<div class="tabUi demoTab">
						<!-- #demo01 -->
						<div id="demo01" class="tab_contents">
							<div class="demoBox">								
                                <div class="desc_tts">
                                    <p>아래 문장은 예시 문장입니다. 청취하고 싶은 문장을 직접 입력해주세요.</p>
<%--                                    <p><em class="fas fa-lightbulb"></em> 무료 Trial은 25자까지만 지원됩니다. 필요 시 AI Service를 이용해주세요.</p>--%>
                                </div>
								<!-- .step01 -->
								<div class="demo_intro">
									<div class="demo_infoTxt">
                                        <textarea id="text-contents" class="textArea apitextArea" rows="8" maxlength="1000" placeholder="음성을 생성할 텍스트를 1000자 이내로 입력하세요."></textarea>
                                        <div class="text_info">												
                                            <span class=""><strong id="count">0</strong>/1000자</span>
                                        </div>
                                    </div>
									<div class="btnBox">
<%--										<div class="holeBox">--%>
<%--											<div class="hole">--%>
<%--												<em></em><em></em><em></em><em></em><em></em>--%>
<%--											</div>--%>
<%--										</div>--%>
										<button class="demoRecord" >
                                            <span>
                                                <em class="fas fa-play-circle"></em><strong>음성 생성</strong>
<%--                                                <span class="tooltiptext">버튼 클릭</span>--%>
                                            </span>
                                        </button>
									</div>
								</div>
								<!-- //.step01 -->
								
								<!-- .step02 -->
								<div class="demo_recording">
									<div class="demo_infoTxt">음성을 변환 중입니다. <br>잠시만 기다려 주세요.</div>	
									<!-- recordingBox -->
									<div class="recordingBox">
										<div class="recording1 loader loader2">	
										</div>
									</div>
									<!-- //recordingBox -->
								</div>
								<!-- //.step02 -->
			
								<!-- .step03 -->
								<div class="demo_recording2">
									<div class="demo_infoTxt">재생 되고 있습니다.</div>
									
									<!-- recording -->
									<div class="recording">
										<span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>
										<span></span><span></span><span></span><span></span><span></span><span></span><span></span>
									</div>
									<!-- //recording -->
									
									<div class="btnBox">
										<button id="resetBtn" class="demoReset btnbtn"><span>이전으로</span></button>
										<button class="dwn" onclick="fileDownload()"><span>다운로드</span></button>
									</div>
								</div>
								<!-- //.step03 -->
								
								<!-- .step04 -->
<%--								<div class="demo_result">--%>
<%--									<div class="demo_player">--%>
<%--										<audio src="" preload="auto" controls></audio>--%>
<%--									</div>--%>
<%--									<div class="btnBox">--%>
<%--										<button class="demoReset"><span>RESET</span></button>--%>
<%--										<button class="dwn"><span>Download</span></button>--%>
<%--									</div>--%>
<%--								</div>--%>
								<!-- //.step05 -->
							</div>
						</div>
						<!-- //#demo01 -->
					</div>
					<!-- //.tabUi -->
					<div class="remark">
						*한국어, 영어 외 다른 언어가 필요하시면, 고객지원센터로 연락 주세요.<br>
						*특수문자(예: #,*,~ ), 특수단위(예: ㎖, ㎡, ㎧ ㎉ 등)  등 현재 TTS 모델에서 수정이 필요한 경우, 커스트마이징해드립니다.

					</div>
				</div>
			</div>
            <!--//.ttsdemo-->
			<!--.ttsmenu-->
			<div class="demobox voice_menu" id="ttsmenu">
				<!--guide_box-->
				<div class="guide_box">
					<div class="guide_common">
						<div class="title">
							API 공통 가이드
						</div>
						<p class="sub_title">개발 환경 세팅</p>
						<p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
						<p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
						<p class="sub_title">키 발급</p>
						<p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
						<p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
						<p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
						<p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
						<p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
					</div>
					<!--guide_group-->
					<div class="guide_group">

						<div class="title">
							TTS <small>(Text-To-Speech)</small>
						</div>
						<p class="sub_txt">마인즈랩의 TTS API는 텍스트 문장 또는 파일을 음성으로 변환시켜주는 API입니다.<br>
							20분 학습만으로도 다양한 모델로 자연스럽게 음성을 합성해주는 마인즈랩의 API를 경험해보세요.</p>

						<span class="sub_title">
								준비사항
							</span>
						<p class="sub_txt">① Input: 텍스트(문장)
							<span><small>*제약사항: 1000자 이내의 문장(한글/영어 기준)</small></span>
						</p>
						<p class="sub_txt">② 아래 Model 중 택 1  </p>
						<ul>
							<li>한국어 성인 여자 - 차분한 (kor_female1)</li>
							<li>한국어 성인 여자 - 발랄한 (kor_female2)</li>
							<li>한국어 성인 여자 - 자연스러운 (kor_female3)</li>
							<li>한국어 성인 여자 - 정직한 (kor_female4)</li>
							<li>한국어 성인 여자 - 개성있는 (kor_female5)</li>
<%--							<li>한국어 성인 여자 - 친근한 (baseline_kor)</li>--%>
							<li>한국어 성인 남자 - 자연스러운 (kor_male1)</li>
							<li>한국어 성인 남자 - 진중한 (kor_male2)</li>
							<li>한국어 성인 남자 - 차분한 (kor_male3)</li>
							<li>한국어 성인 남자 - 친근한 (kor_male4)</li>
							<li>한국어 남자 아이(kor_kids_m)</li>
							<li>한국어 여자 아이(kor_kids_f)</li>
							<li>영어 - Selena (baseline_eng)</li>
							<li>영어 - Brandon (eng_male1)</li>
						</ul>
						<span class="sub_title">
								 실행 가이드
							</span>
						<p class="sub_txt">① Request  </p>
						<ul>
							<li>Method : POST</li>
							<li>URL : https://api.maum.ai/tts/stream</li>
						</ul>
						<p class="sub_txt">② Request 파라미터 설명 </p>
						<table>
							<tr>
								<th>키</th>
								<th>설명</th>
								<th>type</th>
							</tr>
							<tr>
								<td>apiId</td>
								<td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청 </td>
								<td>string</td>
							</tr>
							<tr>
								<td>apiKey</td>
								<td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청 </td>
								<td>string</td>
							</tr>
							<tr>
								<td>text</td>
								<td>문장 (*1000자 이내)</td>
								<td>string</td>
							</tr>
							<tr>
								<td>voiceName</td>
								<td>사용할 Voice Model Name <br>
									<small>* 그 외 다른 모델 사용이 필요할 시에는 문의하세요 </small></td>
								<td>string</td>
							</tr>
						</table>
						<p class="sub_txt">③ Request 예제 </p>
						<div class="code_box">
<pre>
curl -X POST \
  https://api.maum.ai/tts/stream \
  -H 'Content-Type: application/json' \
  -d '{
    "apiId" : (*ID 요청필요),
    "apiKey" : (*Key 요청필요),
    "text" : 안녕하세요, 반갑습니다. Minds Lab 상담원입니다.,
    "voiceName" : baseline_kor
}'
</pre>
						</div>
						<p class="sub_txt">④ Response  예제 </p>
						<div class="code_box">
							audio/x-wav 타입의 chunked data (StreamBody)


						</div>
					</div>
					<!--//.guide_group-->
				</div>
				<!--//.guide_box-->
			</div>
			<!--//ttsmenu-->
			<!--.ttsexample-->
			<div class="demobox" id="ttsexample">
				<p><em style="font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>
				<span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
				<div class="useCasesBox">
					<ul class="lst_useCases">
						<li>
							<dl>
								<dt>
									<em>CASE 01</em>
									<span>AI 자동 해피콜</span>
								</dt>
								<dd class="txt">설문조사, 상품 품질 조사와 같은 반복적인 성격의 전화를 인공지능으로 대신하는 AI 자동콜 해피콜에 응용할 수 있습니다.</dd>
								<dd class="api_itemBox">
									<ul class="lst_api">
										<li class="ico_tts"><span>TTS</span></li>
									</ul>
								</dd>
							</dl>
						</li>
						<li>
							<dl>
								<dt>
									<em>CASE 02</em>
									<span>안내 서비스</span>
								</dt>
								<dd class="txt">다양한 목적의 안내나 어시스턴트 서비스에 실제처럼 자연스러운 TTS 음성을 활용할 수 있습니다.</dd>
								<dd class="api_itemBox">
									<ul class="lst_api">
										<li class="ico_tts"><span>TTS</span></li>
									</ul>
								</dd>
							</dl>
						</li>
						<li>
							<dl>
								<dt>
									<em>CASE 03</em>
									<span>유튜버</span>
								</dt>
								<dd class="txt">유튜브나 브이로그 등 영상 콘텐츠에 즉각적으로 반영할 수 있습니다.</dd>
								<dd class="api_itemBox">
									<ul class="lst_api">
										<li class="ico_tts"><span>TTS</span></li>
									</ul>
								</dd>
							</dl>
						</li>
						<li>
							<dl>
								<dt>
									<em>CASE 04</em>
									<span>게임</span>
								</dt>
								<dd class="txt">게임 상의 등장인물 음성이나 음향 효과 등을 만들 수 있습니다.</dd>
								<dd class="api_itemBox">
									<ul class="lst_api">
										<li class="ico_tts"><span>TTS</span></li>
									</ul>
								</dd>
							</dl>
						</li>
						<li>
							<dl>
								<dt>
									<em>CASE 05</em>
									<span>아동 교육 콘텐츠</span>
								</dt>
								<dd class="txt">아이들을 대상으로 한 교육용 콘텐츠에 접목할 수 있습니다.</dd>
								<dd class="api_itemBox">
									<ul class="lst_api">
										<li class="ico_tts"><span>TTS</span></li>
									</ul>
								</dd>
							</dl>
						</li>
						<li>
							<dl>
								<dt>
									<em>CASE 06</em>
									<span>뉴스 브리핑</span>
								</dt>
								<dd class="txt">고객이 원하는 목소리로 개인화된 뉴스 브리핑 서비스를 제공할 수 있습니다.</dd>
								<dd class="api_itemBox">
									<ul class="lst_api">
										<li class="ico_tts"><span>TTS</span></li>
										<li class="ico_xdc"><span>XDC</span></li>
									</ul>
								</dd>
							</dl>
						</li>
					</ul>
				</div>
				<!-- //음성 인식(TTS) -->
			</div>
		</div>
		<!-- //.content -->
	</div>
	<!-- //.contents -->

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/tts/app.js?20190816"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/common/js/tts/pcm-player.js"></script>

<script type="text/javascript">
jQuery.event.add(window, "load", function () {
	$(document).ready(function () {
		var $textArea = $('.demoBox .textArea');
		var $holeBox = $('.holeBox');

		$holeBox.hide();

		$('.selectarea .radio').on("click",function(){
			$textArea.val($('#voice'+$(this).children('input[type="radio"]').attr("id")).val());
			countTextAreaLength();
			// $holeBox.show();
		});

		$('.selectarea .radio:first-child').trigger('click');

		// textArea event
		$textArea.on('input keyup paste change', function () {
			if ($textArea.val().trim() !== "") {
				// $holeBox.show();
			} else {
				$holeBox.hide();
			}
			countTextAreaLength(); // typing count
		});

		// step01 > step02  : 음성생성 버튼 클릭 event
		$('.tts_demo button.demoRecord').on('click', function () {
			if ($textArea.val().trim() === "") {
				alert("내용을 입력해 주세요.");
				$textArea.val("");
				$(this).focus();
				return 0;
			}

			var $checked = $("li.checked");
			var text = $textArea.val();
			var voiceName = $checked.data("speakerid");

			if($checked.text()=== "" || $checked.text() ==="보이스 선택") {
				alert('보이스를 선택해 주세요.')
			}else {
				speak(text, voiceName);
				$holeBox.hide();
				$('.desc_tts').hide();
				$('.demo_intro').hide();
				$('.selectarea').hide();
				$('.demo_recording').show();
			}
		});

		// step04 > step01
		$('.tts_demo .demoReset').on('click', function () {
			//$('#count').html(0);
			// $textArea.val("");
			$('.demo_recording').hide();
			$('.demo_recording2').hide();
			$('.selectarea').show();
			$('.demo_intro').show();
			$('.desc_tts').show();
		});

		$('.tit').on('click',function(){
			var openClass = $(this).parent();
			if(openClass.hasClass('open')){
				$('.btn_bar').removeClass('open');
				$('.btn_bar ul').slideUp(200);

				$(this).parent().removeClass('open');
				$(this).next().slideUp(200);
			}else {
				$('.btn_bar').removeClass('open');
				$('.btn_bar ul').slideUp(200);
				$(this).parent().addClass('open');
				$(this).next().slideDown(200);
			}
		})


		$('.btn_bar li').on('click', function(){
			$('.tit p').text('보이스 선택');
			$('.btn_bar li').removeClass('checked')
			$(this).parent().parent().children('.tit').children('p').text($(this).text());
			$(this).parent().parent().removeClass('open');
			$(this).parent().slideUp(200);
			$(this).addClass('checked')
		});

		// 팝업 닫기
		$('.pop_close, .pop_bg, .btn a').on('click', function () {
			$('.pop_simple').fadeOut(300);
			$('body').css({ 'overflow': ''});
		});

	});
});


var pcmPlayer = null;
function speak(text, voiceName){

	var _url = "/api/tts/ttsStream";

	console.log(text);
	console.log(voiceName);

	if (pcmPlayer != null) {
		pcmPlayer.destroy();
	}

	pcmPlayer = new PCMPlayer({
		encoding: '16bitInt',
		channels: 1,
		sampleRate: 22050,
		flushingTime: 500
	}, 'kr');

	let req = {
		method: 'POST',
		headers: new Headers({"Content-Type": "application/json", '${_csrf.headerName}': '${_csrf.token}'}),
		body: JSON.stringify({'text': text, 'voiceName':voiceName}),
		mode: 'cors',
		cache: 'default'
	};

	fetch(_url, req).then((response) => {
		if(response.ok){
			const reader = response.body.getReader();
			let data = null;

			function read() {
				return reader.read().then(({value, done}) => {
					if (done) {
						pcmPlayer.lastChunk = true;
					} else {
						if (data != null) {
							let temp = new Uint8Array(value.length + 1);
							temp.set(data, 0);
							temp.set(value, 1);
							data = temp;
						} else {
							data = value;
						}
						if (data.length % 2 === 0) {
							pcmPlayer.feed(data);
							data = null;
						} else {
							pcmPlayer.feed(data.slice(0, data.length - 1));
							data = data.slice(data.length - 1, data.length);
						}
						return read();
					}
				})
			}
			return read();
		}else{
			console.log('Network error.');
			$('#api_fail_popup .pop_bd p:nth-child(2) strong').text("Network error");
			$('#api_fail_popup').fadeIn(300);
			$('.tts_demo .demoReset').click();
		}
	}).then( () => {
		console.log('all stream bytes queued for decoding');
	}).catch(e => {
		console.dir(e);
		console.log("speak error");
		$('#api_fail_popup .pop_bd p:nth-child(2) strong').text("Speak error");
		$('#api_fail_popup').fadeIn(300);
		$('.tts_demo .demoReset').click();
	})

}


function fileDownload() {
	var file_name_key = "TTS.wav";
	var file_name = "TTS.wav";
	location.href = encodeURI("/api/tts/ttsDwn?fileNameKey=" + file_name_key + "&fileName=" + file_name);
}

function countTextAreaLength (){
	var content = $('#text-contents').val();
	if (content.length >= 1000) {
		$('#count').html(1000);
		return;
	}
	$('#count').html(content.length);
}

//API 탭
function openTap(evt, menu) {
	var i, demobox, tablinks;
	demobox = document.getElementsByClassName("demobox");
	for (i = 0; i < demobox.length; i++) {
		demobox[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(menu).style.display = "block";
	evt.currentTarget.className += " active";
}
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();

</script>