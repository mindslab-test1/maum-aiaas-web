<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-01-15
  Time: 10:57
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/data_analysis.css">

<div class="contents api_content">
    <div class="content">
        <h1>데이터 상관분석</h1>
        <ul class="menu_lst analy_lst">
            <li class="tablinks" onclick="openTap(event, 'data_demo')" id="defaultOpen"><button type="button">엔진</button></li>
            <li class="tablinks" onclick="openTap(event, 'data_example')"><button type="button">적용사례</button></li>
            <li class="tablinks" onclick="openTap(event, 'data_menu')"><button type="button">매뉴얼</button></li>
<%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <div class="demobox voicefont_demobox" id="data_demo">
            <p><span style="color:#5E77FF">데이터 상관분석</span></p>
            <span class="sub">데이터의 연관 관계를 분석하는 다양한 알고리즘들을 제공합니다.<br>
아래의 샘플 데이터를 통해 인공지능의 데이터 분석을 경험해보세요. 문의 주시면 비즈니스에 적용, 활용할 수 있도록 도와드립니다. hello@mindslab.ai </span>

            <!--dataAnalysis_box-->
            <div class="demo_layout dataAnalysis_box">
                <div class="data_1">
                    <p><em class="fas fa-table"></em>두가지 분석 데이터 준비</p>
                    <div class="csv_box">
                        <strong>Input 1: 변수 정의서</strong>
                        <a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vTHzuaKHFxn_BltLv4-VpwnkVZ6rpVDdDsxi17d0h6jxWAX7IDPQ4ttQGykYggO8YZZY1qrq97l3Bh-/pub?output=xlsx" download="변수 정의">.xlsx 다운로드</a>
                        <div class="csv_file">
                            <iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vTHzuaKHFxn_BltLv4-VpwnkVZ6rpVDdDsxi17d0h6jxWAX7IDPQ4ttQGykYggO8YZZY1qrq97l3Bh-/pubhtml"></iframe>
                        </div>
                    </div>
                    <div class="csv_box">
                        <strong>Input 2: 데이터셋</strong>
                        <a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ8NEJ_99VVK4WSZ9uvffZxtfB-D6c43YjsRGokHmebZSDNOxvHi_yfxrx7gvpG4n93A6TUIVK_XaOM/pub?output=xlsx" download="데이터셋">.xlsx 다운로드</a>
                        <div class="csv_file">
                            <iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vQ8NEJ_99VVK4WSZ9uvffZxtfB-D6c43YjsRGokHmebZSDNOxvHi_yfxrx7gvpG4n93A6TUIVK_XaOM/pubhtml"></iframe>
                        </div>
                    </div>
                    <p><em class="fas fa-chart-bar"></em>상관도 분석을 위한 알고리즘</p>
                    <div class="algorithm">
                        <ul>
                            <li><em class="fas fa-check"></em> <span>F-regression</span> (F-test를 통해 입력 변수의 중요도를 판단하는 기법)</li>
                            <li><em class="fas fa-check"></em> <span>Mutual Info. Regression </span> (Mutual Info. Value를 계산하여 상관도를 측정하는 기법)</li>
                            <li><em class="fas fa-check"></em> <span>Random Forest Classifier</span>  (Radom Forest 모델을 이용하여 상관도를 측정하는 기법)</li>
                        </ul>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="start_btn" id="">결과 보기</button>
                    </div>
                </div>

                <!--data_2-->
                <div class="data_2">
                    <p><em class="far fa-chart-bar"></em>데이터 상관분석 중</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#c9c3ec" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#5E77FF" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

                        <p>약간의 시간이 소요 됩니다.</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
                    </div>


                </div>
                <!--data_2-->
                <!--data_3-->
                <div class="data_3">
                    <p><em class="far fa-chart-bar"></em>분석 결과</p>
                    <div class="data_result">
                        <textarea id="resultTxt" placeholder="텍스트 결과가 나옵니다."></textarea>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
                    </div>


                </div>
                <!--data_3-->

            </div>
            <!--// dataAnalysis_box-->
        </div>

        <!--.data_menu-->
        <div class="demobox analy_menu" id="data_menu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        데이터 상관분석 <small>Feature Selection</small>
                    </div>
                    <p class="sub_txt">Feature Selection을 통하여 데이터간의 상관관계를 분석해줍니다.</p>

                    <span class="sub_title">
								준비사항
					</span>
                    <p class="sub_txt">- Input: 변수 정의서(.csv), 데이터 셋 (.csv)</p>
                    <ul>
                        <li>확장자: csv</li>
                        <li>기준: 웹사이트 참고 </li>
                    </ul>
                    <span class="sub_title">
								 실행 가이드
							</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/ocr/dataSelection</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>var_file</td>
                            <td>변수 정의서(.csv) (웹사이트 참고)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>data_file</td>
                            <td>데이터셋(.csv) (웹사이트 참고)</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                        curl --location --request POST 'http://api.maum.ai/ocr/dataSelection' \<br>
                        --header 'Content-Type: multipart/form-data' \<br>
                        --form 'apiId= 발급받은 API ID' \<br>
                        --form 'apiKey= 발급받은 API KEY' \<br>
                        --form 'var_file= 변수 정의서'\<br>
                        --form 'data_file= 데이터셋'<br>
                    </div>

                    <p class="sub_txt">④ Response 예제 </p>

                    <div class="code_box">
<pre>
{
    "result_file":

"# Total data sample number = 99

Variable name | Type | Index | Priority | Null num | Outer num | Null+Outer | Mean | Std |

************************************************************************************************************************

X1 | number | 1 | -1 | 1 | 0 | 1 | 60.102 | 5.955 |
X2 | number | 2 | -1 | 0 | 2 | 2 | 0.055 | 0.028 |
X3 | number | 3 | -1 | 0 | 2 | 2 | 100.216 | 5.838 |
Y | number | 4 | 0 | 1 | 0 | 1 | 14.918 | 3.050 |

Processing refine_dataset_col_by_priority : 100 x 4 -> 100 x 4

Processing refine_dataset_col_by_null_replacement...

************************************************************************************************************************

# Dataset refinement process by deleting columns(s) which has null number greater than threshold, 0.50

> 0 columns detected and deleted.

> dataset dim : 99 x 4 -> 99 x 4

Processing refine_dataset_row_by_nan_replacement...

************************************************************************************************************************

# Dataset refinement process by deleting row vector in which one of them doesn't satisfy its range requirement.
> Null data in sample vector
- X1 : 1
- Y : 1
> Number type data in sample vector not satisfying range information
- X2 : 2
- X3 : 2
> String type data in sample vector not satisfying range information

Processing refine_dataset_row_by_range :
100 x 4 -> 95 x 4

************************************************************************************************************************

# Divide dataset by type, input vs output and number vs string.
> Input number features = 3
> Input string features = 0
> Output number features = 1
> Output string features = 0

************************************************************************************************************************


# Feature selection by variance threshold normalized by output variance
> Sorted variance of number type input variables
1 : X1 : 3.856313
2 : X3 : 3.738211
3 : X2 : 0.000083
---------------------------threshold : 0.000001

> num_type input dataset : 94 x 3 -> 94 x 3

************************************************************************************************************************


# Feature selection by f_regression
> Sorted variance of number type input variables
1 : X2 : 0.425449
-------------------p_value threshold : 0.500000
2 : X1 : 0.806054
3 : X3 : 0.883038

> num_type input dataset : 94 x 3 -> 94 x 3

************************************************************************************************************************


# Feature selection by mutual_info_regression
> Sorted variance of number type input variables
1 : X1 : 0.000000
2 : X2 : 0.000000
3 : X3 : 0.000000

> num_type input dataset : 94 x 3 -> 94 x 3

************************************************************************************************************************


# Feature selection by random forest classifier
> Sorted weight of number type input variables
1 : X2 : 0.452163
2 : X3 : 0.298808
3 : X1 : 0.249028
------------------mi_value threshold : 0.001000

> num_type input dataset : 94 x 3 -> 94 x 3"

}
</pre>
                    </div>
                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.data_menu-->

        <!--.data_example-->
        <div class="demobox" id="data_example">
            <p><em style="color:#5E77FF;font-weight: 400;">적용사례</em> <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
            <!-- 데이터 상관분석 -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>포스코 데이터 분석</span>
                            </dt>
                            <dd class="txt">공정 과정 중 투입된 합금철량, 발생된 슬래그량, 조업 소요시간, 조업 완료 시 용강 온도 등 데이터를 이용해서 통계적인 특성을 파악하고, X-Y 인자간의 상관관계를 파악합니다.
                                <span><em class="fas fa-book-reader"></em> Reference: POSCO</span>
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_dataAnaly"><span>데이터 분석</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>상관관계분석</span>
                            </dt>
                            <dd class="txt">입력된 데이터들의 통계적인 특성과 feature selection 알고리즘 (F-regression, Mutual Info. Regression, Random Forest Classifier)을 이용하여 상관관계를 분석합니다.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_dataAnaly"><span>데이터 분석</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!--데이터 상관분석 -->
        </div>
        <!--//.data_example-->
    </div>

    <input type="file" id="varFile" class="demoFile" hidden>
    <input type="file" id="dataFile" class="demoFile" hidden>

</div>
<!-- .contents -->


<script type="text/javascript">
    var ajaxXHR;

    $(document).ready(function () {

        $('.start_btn').on('click', function() {
            $('.data_1').hide();
            $('.data_2').fadeIn();
            $("html").scrollTop(0);

            var formData = new FormData();
            formData.append('var_file', varFile);
            formData.append('data_file', dataFile);
            formData.append('${_csrf.parameterName}', '${_csrf.token}');

            ajaxXHR = $.ajax({
                type: "POST",
                async: true,
                url: '/api/dataAnalysis', //여기 url 추가
                data: formData,
                processData: false,
                contentType: false,
                success: function(result){

                    console.log(result);
                    $('textarea[id=resultTxt]').val(result[0].body);

                    $('.data_2').hide();
                    $('.data_3').fadeIn();
                },
                error: function(jqXHR, error){
                    if(jqXHR.status === 0){
                        return false;
                    }

                    alert("서버와 연결이 되지 않습니다.\n잠시 후에 다시 이용해주세요.");
                    console.dir(error);
                    window.location.reload();
                }
            });
        });

        $('.btn_back1').on('click', function() {
            ajaxXHR.abort();
            $('.data_2').hide();
            $('.data_1').fadeIn();
            $('.data_3').hide();

        });

        initDataFile();
    });


    var varFile;
    var dataFile;

    function initDataFile() {
        // loadContent("/aiaas/common/files/data_analysis___dat_file.csv", "data_analysis___dat_file.csv", "data")
        loadContent("/aiaas/common/files/data_analysis___var_file.csv", "data_analysis___var_file.csv", "var")
        loadContent("/aiaas/common/files/data_analysis___dat_file.csv", "data_analysis___dat_file.csv", "data")
    }

    function loadContent(path, filename, target) {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", path);
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function()
        {
            blob = xhr.response;//xhr.response is now a blob object
            if(target == "var") varFile = new File([blob], filename);
            else dataFile = new File([blob], filename);
        };

        xhr.send();
    }

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>