<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">


<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>이미지 업로드 실패</h5>
            <p>이미지 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
                업로드 되지 않았습니다.</p>
            <span>* 지원가능 파일 확장자: .png, .jpg, .bmp<br>
* 이미지 용량 4MB 이하만 가능합니다.<br>
            </span>
        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

    <!-- .contents -->
    <div class="contents">
        <div class="content api_content">
            <h1 class="api_tit">얼굴 애니메이션 필터</h1>
            <ul class="menu_lst vision_lst">
                <li class="tablinks active" onclick="openTap(event, 'afdemo')" id="defaultOpen">
                    <button type="button">엔진</button>
                </li>
                <li class="tablinks" onclick="openTap(event, 'afexample')">
                    <button type="button">적용사례</button>
                </li>
                <li class="tablinks" onclick="openTap(event, 'afmenu')">
                    <button type="button">매뉴얼</button>
                </li>
            </ul>
            <!-- .demobox -->
            <div class="demobox" id="afdemo">
                <p><span>얼굴 애니메이션 필터</span> <small>(Anime Face)</small></p>
                <span class="sub">나의 얼굴을 애니메이션으로 생성하고, style 값을 조정하여 애니메이션을 꾸밀 수 있습니다.</span>

                <!--animeFace_box-->
                <div class="demo_layout animeFace_box">
                    <!-- .avr_1 -->
                    <div class="avr_1">
                        <div class="fl_box">
                            <p><em class="far fa-file-image"></em><strong>샘플 파일</strong>로 테스트 하기</p>
                            <div class="sample_box">
                                <div class="radio">
                                    <input type="radio" id="picture" name="sample_slt" value="sampleImage" checked>
                                    <label for="picture">
                                        <span class="label">사진</span>
                                        <div class="img_area">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_animeFace_sample01.png" alt="sample picture"/>
                                        </div>
                                        <p>애니메이션 필터 효과 및 스타일 적용</p>
                                    </label>
                                </div>

                                <div class="radio">
                                    <input type="radio" id="animation" name="sample_slt" value="sampleAnimation">
                                    <label for="animation">
                                        <span class="label">애니메이션</span>
                                        <div class="img_area">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_animeFace_sample02.png" alt="sample animation"/>
                                        </div>
                                        <p>랜덤으로 생성되는 애니메이션 이미지 스타일 적용</p>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="fr_box">
                            <p><em class="far fa-file-image"></em><strong>내 파일</strong>로 해보기</p>
                            <div class="uplode_box">
                                <div class="btn" id="uploadFile">
                                    <em class="fas fa-times hidden close"></em>
                                    <em class="far fa-file-image hidden"></em>
                                    <label for="demoFile" class="demolabel">이미지 업로드</label>
                                    <input type="file" id="demoFile" class="demoFile" accept=".jpg, .png, .bmp">
                                </div>
                                <ul>
                                    <li>* 지원가능 파일 확장자: .png, .jpg, .bmp</li>
                                    <li>* PNG 파일을 사용하기를 권고합니다.</li>
                                    <li>* 이미지 용량 4MB 이하만 가능합니다.</li>
                                </ul>
                            </div>
                        </div>

                        <div class="btn_area">
                            <button type="button" class="btn_apply">필터 & 스타일 적용</button>
                        </div>
                    </div>
                    <!-- //.avr_1 -->

                    <!-- .avr_2 -->
                    <div class="avr_2">
                        <p><em class="far fa-file-image"></em>필터 적용 중</p>
                        <div class="loding_box ">
                            <svg xmlns="http://www.w3.org/2000/svg"
                                 version="1.0" width="144px" height="18px"
                                 viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42"
                                                                                 d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"></path>
                                <g>
                                    <path fill="#f7778a" fill-opacity="1"
                                          d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"></path>
                                    <animateTransform attributeName="transform" type="translate"
                                                      values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0"
                                                      calcMode="discrete" dur="1820ms"
                                                      repeatCount="indefinite"></animateTransform>
                                </g></svg>

                            <p>약간의 시간이 소요 됩니다. (약 2분 내외)</p>
                        </div>
                        <div class="btn_area">
                            <button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
                        </div>
                    </div>
                    <!-- //.avr_2 -->

                    <!-- .avr_3 -->
                    <div class="avr_3">
                        <p><em class="far fa-file-image"></em>필터 & 스타일 적용</p>
                        <div class="result_file">
                            <div class="effect_view">
                                <div class="imgBox">
                                    <img src="" id="resultImg" alt="result image">
                                    <div class="circle_loading">
                                        <div class="inner_circle">
                                            <div>&nbsp;</div>
                                            <div>&nbsp;</div>
                                            <div><div>&nbsp;</div></div>
                                            <div><div>&nbsp;</div></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- [D] input file이 사진인 경우 보여줄 요소로 addClass('on')하면 보여짐 -->
                                <div class="effect_control on">
                                    <div class="range_slider">
                                        <label>애니메이션 효과</label>
                                        <!-- [D] 초기값 : 2 -->
                                        <input id="effectControlInput" class="slide_bar" type="range" value="2" min="0" max="5" step="1">
                                    </div>
                                    <ul class="range_value">
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                </div>

                                <!-- [D] input file이 애니메이션인 경우 보여줄 요소로 addClass('on')하면 보여짐 -->
                                <button type="button" class="btn_creat_newImg"><em class="fas fa-sync-alt"></em>새 이미지 생성
                                </button>
                            </div>

                            <div class="style_adjust">
                                <div class="style_control">
                                    <div class="range_slider">
                                        <label>Style 조절</label>
                                        <!-- [D] 초기값 : 0 -->
                                        <input id="styleControlInput" class="slide_bar" type="range" value="1" min="1" max="5" step="1">
                                    </div>
                                    <ul class="range_value">
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                </div>

                                <div class="style_slt">
                                    <div class="tit">Style 선택 :</div>

                                    <div class="list_wrap">
                                        <ul class="btn_list">
                                            <li>
                                                <button type="button" class="btn_contrast active" value="portrait">대비</button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn_variation" value="mutate">변형</button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn_individuality" value="psi_post">개성</button>
                                            </li>
                                        </ul>

                                        <ul class="slt_list">
                                            <li>
                                                <select class="select">
                                                    <option value="" selected>헤어 스타일</option>
                                                    <option value="blunt_bangs">앞머리 일자</option>
                                                    <option value="long_hair">긴 머리</option>
                                                </select>
                                            </li>
                                            <li>
                                                <select class="select">
                                                    <option value="" selected>헤어 컬러</option>
                                                    <option value="blonde_hair">금발</option>
                                                    <option value="black_hair">검정</option>
                                                    <option value="brown_hair">갈색</option>
                                                    <option value="red_hair">붉은색</option>
                                                    <option value="purple_hair">보라색</option>
                                                    <option value="pink_hair">분홍색</option>
                                                    <option value="silver_hair">은색</option>
                                                </select>
                                            </li>
                                            <li>
                                                <select class="select">
                                                    <option value="" selected>눈동자</option>
                                                    <option value="blue_eyes">파란색</option>
                                                    <option value="green_eyes">녹색</option>
                                                </select>
                                            </li>
                                            <li>
                                                <select class="select">
                                                    <option value="" selected>입</option>
                                                    <option value="smile">미소</option>
                                                    <option value="closed_mouth">입 다물기</option>
                                                    <option value="open_mouth">입 벌리기</option>
                                                </select>
                                            </li>
                                            <li>
                                                <select class="select">
                                                    <option value="" selected>악세사리</option>
                                                    <option value="bow">장신구</option>
                                                    <option value="hair_ornament">머리 장식</option>
                                                    <option value="hat">모자</option>
                                                </select>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="btnBox">
                                <a id="reset" class="btn_reset"><em class="fas fa-undo-alt"></em> 이미지 새로고침</a>
                                <a id="save" onclick="downloadResultImg();" class="btn_dwn"><em
                                        class="far fa-arrow-alt-circle-down"></em> 결과 파일 다운로드</a>
                            </div>
                        </div>

                        <div class="btn_area">
                            <button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
                        </div>
                    </div>
                    <!-- //.avr_3 -->
                </div>
                <!--//.animeFace_box-->
            </div>
            <!-- //.demobox -->

            <!--.afmenu-->
            <div class="demobox vision_menu" id="afmenu">
                <!--guide_box-->
                <div class="guide_box">
                    <div class="guide_common">
                        <div class="title">API 공통 가이드</div>
                        <p class="sub_title">개발 환경 세팅</p>
                        <p class="sub_txt">1) REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                        <p class="sub_txt">2) 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                        <p class="sub_title">키 발급</p>
                        <p class="sub_txt">1) Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                        <p class="sub_txt">2) 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                        <p class="sub_txt">3) [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                        <p class="sub_txt">4) 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                        <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                    </div>

                    <div class="guide_group">
                        <div class="title">얼굴 애니메이션 필터 <small>(Anime Face)</small></div>
                        <p class="sub_txt">나의 얼굴을 애니메이션으로 생성하고, style 값을 조정하여 애니메이션을 꾸밀 수 있습니다.</p>

                        <span class="sub_title">준비사항</span>
                        <p class="sub_txt">- Input: 이미지 파일</p>
                        <ul>
                            <li>확장자 : .png, .jpg, .bmp</li>
                            <li>용량 : 4MB 이하</li>
                        </ul>

                        <span class="sub_title">실행 가이드</span>
                        <em>Sample Make : Random한 Anime Face를 생성합니다.</em>

                        <!-- Upload START-->
                        <p class="sub_txt">① Request </p>
                        <ul>
                            <li>Method : POST</li>
                            <li>URL : https://api.maum.ai/anime/sample:make</li>
                        </ul>
                        <p class="sub_txt">② Request 파라미터 설명</p>
                        <table>
                            <tbody>
                            <tr>
                                <th>키</th>
                                <th>설명</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>apiId</td>
                                <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>apiKey</td>
                                <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청</td>
                                <td>string</td>
                            </tr>
                            </tbody>
                        </table>

                        <p class="sub_txt">③ Request 예제</p>
                        <div class="code_box">
<pre>
curl -X POST 'https://api.maum.ai/anime/sample:make' \
-H 'Content-Type: application/json' \
-d '{
    "apiId" : (발급받은 API ID),
    "apiKey" : (발급받은 API KEY)
}'
</pre>
                        </div>

                        <p class="sub_txt">④ Response 파라미터 설명</p>
                        <span class="table_tit">Response</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>키</th>
                                <th>설명</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>message</td>
                                <td>API 동작여부</td>
                                <td>Object</td>
                            </tr>
                            <tr>
                                <td>payload</td>
                                <td>결과</td>
                                <td>Object</td>
                            </tr>
                            </tbody>
                        </table>

                        <span class="table_tit">message: API 동작여부</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>키</th>
                                <th>설명</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>message</td>
                                <td>요청 처리 상태를 설명하는 문자열 (Success/ Fail)</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>status</td>
                                <td>요청 처리 상태에 대한 status code (0: Success)</td>
                                <td>int</td>
                            </tr>
                            </tbody>
                        </table>

                        <span class="table_tit">payload: 결과</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>키</th>
                                <th>설명</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>img</td>
                                <td>AnimeFace Image Byte Array</td>
                                <td>byte</td>
                            </tr>
                            <tr>
                                <td>latent</td>
                                <td>AnimeFace .pkl File Byte Array</td>
                                <td>byte</td>
                            </tr>
                            </tbody>
                        </table>

                        <p class="sub_txt">⑤ Response 예제</p>
                        <div class="code_box">
<pre>
{
	"message": {
		"message": "Success",
		"status": 0
	},
	"payload": {
		"img": "byte[]",
		"latent": "byte[]"
	}
}			
</pre>
                        </div>

                        <em>Face Make : Human Face Image를 Input으로 넣을 때, 비슷한 모습의 Anime Face Image를 생성합니다.</em>
                        <p class="sub_txt">① Request</p>
                        <ul>
                            <li>Method : POST</li>
                            <li>URL : https://api.maum.ai/anime/face:make</li>
                        </ul>

                        <p class="sub_txt">② Request 파라미터 설명</p>
                        <table>
                            <tbody>
                            <tr>
                                <th>키</th>
                                <th>설명</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>apiId</td>
                                <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>apiKey</td>
                                <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>image</td>
                                <td>type:file (.png, .jpg, .bmp) 이미지 파일</td>
                                <td>file</td>
                            </tr>
                            <tr>
                                <td>steps</td>
                                <td>비슷한 모습의 Image를 찾기 위해 사용할 step수(not required, default = 500)</td>
                                <td>int</td>
                            </tr>
                            </tbody>
                        </table>

                        <p class="sub_txt">③ Request 예제</p>
                        <div class="code_box">
<pre>
curl -X POST 'https://api.maum.ai/anime/face:make' \
-H 'Content-Type: multipart/form-data' \
-F 'apiId= 발급받은 API ID' \
-F 'apiKey= 발급받은 API KEY' \
-F 'image= @인식할 얼굴 이미지 파일' \
-F 'steps= default= 500' \
</pre>
                        </div>

                        <p class="sub_txt">④ Response 파라미터 설명</p>
                        <span class="table_tit">Response</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>키</th>
                                <th>설명</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>message</td>
                                <td>API 동작여부</td>
                                <td>Object</td>
                            </tr>
                            <tr>
                                <td>payload</td>
                                <td>결과</td>
                                <td>Object</td>
                            </tr>
                            </tbody>
                        </table>

                        <span class="table_tit">message: API 동작여부</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>키</th>
                                <th>설명</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>message</td>
                                <td>요청 처리 상태를 설명하는 문자열 (Success/ Fail)</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>status</td>
                                <td>요청 처리 상태에 대한 status code (0: Success)</td>
                                <td>int</td>
                            </tr>
                            </tbody>
                        </table>

                        <span class="table_tit">payload: 결과</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>키</th>
                                <th>설명</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>img</td>
                                <td>AnimeFace Image Byte Array</td>
                                <td>byte</td>
                            </tr>
                            <tr>
                                <td>latent</td>
                                <td>AnimeFace .pkl File Byte Array</td>
                                <td>byte</td>
                            </tr>
                            </tbody>
                        </table>

                        <p class="sub_txt">⑤ Response 예제</p>
                        <div class="code_box">
<pre>
{
	"message": {
		"message": "Success",
		"status": 0
	},
	"payload": {
		"img": "byte[]",
		"latent": "byte[]"
	}
}				
</pre>
                        </div>

                        <em>Style Edit : Anime Face Image와 정해진 Style key값(open_mouth, blonde_hair, ...)들의 세기를 조절한 이미지를
                            생성합니다.</em>
                        <p class="sub_txt">① Request</p>
                        <ul>
                            <li>Method : POST</li>
                            <li>URL : https://api.maum.ai/anime/style:edit</li>
                        </ul>

                        <p class="sub_txt">② Request 파라미터 설명</p>
                        <table>
                            <tbody>
                            <tr>
                                <th>키</th>
                                <th>설명</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>apiId</td>
                                <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>apiKey</td>
                                <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>latent</td>
                                <td>얼굴 애니메이션 .pkl File</td>
                                <td>file</td>
                            </tr>
                            <tr>
                                <td>dict</td>
                                <td>style 편집을 위한 .txt file</td>
                                <td>file</td>
                            </tr>
                            </tbody>
                        </table>

                        <p class="sub_txt">③ Request 예제</p>
                        <div class="code_box">
<pre>
curl -X POST 'https://api.maum.ai/anime/style:edit' \
-H 'Content-Type: multipart/form-data' \
-F 'apiId= 발급받은 API ID' \
-F 'apiKey= 발급받은 API KEY' \
-F 'latent= @pkl 파일' \
-F 'dict= @txt 파일' \
</pre>
                        </div>

                        <p class="sub_txt">④ Response 파라미터 설명</p>
                        <span class="table_tit">Response</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>키</th>
                                <th>설명</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>message</td>
                                <td>API 동작여부</td>
                                <td>Object</td>
                            </tr>
                            <tr>
                                <td>payload</td>
                                <td>결과</td>
                                <td>Object</td>
                            </tr>
                            </tbody>
                        </table>

                        <span class="table_tit">message: API 동작여부</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>키</th>
                                <th>설명</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>message</td>
                                <td>요청 처리 상태를 설명하는 문자열 (Success/ Fail)</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>status</td>
                                <td>요청 처리 상태에 대한 status code (0: Success)</td>
                                <td>int</td>
                            </tr>
                            </tbody>
                        </table>

                        <span class="table_tit">payload: 결과</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>키</th>
                                <th>설명</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>img</td>
                                <td>AnimeFace Image Byte Array</td>
                                <td>byte</td>
                            </tr>
                            <tr>
                                <td>latent</td>
                                <td>AnimeFace .pkl File Byte Array</td>
                                <td>byte</td>
                            </tr>
                            </tbody>
                        </table>

                        <p class="sub_txt">⑤ Response 예제</p>
                        <div class="code_box">
<pre>
{
	"message": {
		"message": "Success",
		"status": 0
	},
	"payload": {
		"img": "byte[]",
		"latent": "byte[]"
	}
}				
</pre>
                        </div>
                    </div>
                </div>
                <!--//.guide_box-->
            </div>
            <!--.afmenu-->

            <!--.afexample-->
            <div class="demobox" id="afexample">
                <p><em style="color:#f7778a;font-weight: 400;">적용사례</em> <small>(Use Cases)</small></p>
                <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
                <div class="useCasesBox">
                    <!-- 얼굴 애니메이션 필터-->
                    <ul class="lst_useCases">
                        <li>
                            <dl>
                                <dt>
                                    <em>CASE 01</em>
                                    <span>캐릭터 콘텐츠</span>
                                </dt>
                                <dd class="txt">애니메이션 캐릭터를 활용하여 영화, 게임 등에 활용할 수 있습니다.</dd>
                                <dd class="api_itemBox">
                                    <ul class="lst_api">
                                        <li class="ico_anmFc"><span>얼굴 애니메이션 필터</span></li>
                                    </ul>
                                </dd>
                            </dl>
                        </li>
                        <li>
                            <dl>
                                <dt>
                                    <em>CASE 02</em>
                                    <span>유튜버</span>
                                </dt>
                                <dd class="txt">애니메이션 캐릭터를 유튜브나 브이로그 등 영상 컨셉에 맞게 스타일을 꾸며 사용할 수 있습니다.</dd>
                                <dd class="api_itemBox">
                                    <ul class="lst_api">
                                        <li class="ico_anmFc"><span>얼굴 애니메이션 필터</span></li>
                                    </ul>
                                </dd>
                            </dl>
                        </li>
                    </ul>
                    <!--얼굴 애니메이션 필터-->
                </div>
            </div>
            <!--//.afexample-->
        </div>
    </div>
    <!-- //.contents -->

<!---------------------- Local Resources ---------------------->
<!-- Local Script -->
<script type="text/javascript">

    let request = null;
    let sampleImg = null;

    let initialResultImg = null;
    let initialPklFile = null;

    let inputImg = null;
    let resultImg = null;
    let pklFile = null;
    let dictObj = {"psi_post" : 1};
    let dictFile = null;
    let selectedStyle = "portrait"; // 초기값 "대비"

    const effectValue = {
        0 : 100,
        1 : 300,
        2 : 500,
        3 : 1000,
        4 : 1500,
        5 : 2000
    }
    const styleValue = {
        1 : 0, // psi_post 1
        2 : 5,
        3 : 10,
        4 : 15,
        5 : 20
    }


    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {

            loadSampleImg();

            // 파일 업로드 기능
            document.querySelector("#demoFile").addEventListener('change', function (ev) {
                let demoFileInput = document.getElementById('demoFile');
                let demoFile = demoFileInput.files[0];
                let demoFileSize = demoFile.size;
                let max_demoFileSize = 1024 * 1024 * 4; //1kb는 1024바이트
                //파일 용량, 확장자 체크
                if(demoFileSize > max_demoFileSize || (!demoFile.type.match(/image.png/) && !demoFile.type.match(/image.bmp/) && !demoFile.type.match(/image.jp*/))){
                    $('.pop_simple').show();
                    $('#demoFile').val('');
                } else {
                    $('.demolabel').html(demoFile.name);
                    $('#uploadFile').removeClass('btn');
                    $('#uploadFile').addClass('btn_change');
                    $('.fl_box').css("opacity", "0.5");
                    $('#sample1').prop('checked', false);
                }
            });

            $('em.close').on('click', function () {
                $('.fl_box').css('opacity', '1');
                $('#uploadFile label').text('이미지 업로드');
                $('#uploadFile').removeClass("btn_change");
                $('#uploadFile').addClass("btn");
                $('#demoFile').val("");
            });


            // 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({ 'overflow': '', });
            });

            // [D] 결과화면의 style 선택 영역의 button과 select 선택 시 효과
            $('.btn_list li button, .slt_list li .select').on('click', function () {
                $('.btn_list li button, .slt_list li .select').removeClass('active');
                $(this).addClass('active');
                selectedStyle = $(this).val();
            });

            $('.btn_apply').on('click', function () {

                let selectVal = $('input[type=radio][name=sample_slt]:checked').val();

                if($('#demoFile').val() !== "") {    // 사용자 이미지 업로드
                    inputImg = $('#demoFile').get(0).files[0];
                    submitImg();

                } else if(selectVal === 'sampleAnimation') { // 랜덤 애니메이션
                    submitRandomAnimationImg();

                } else if(selectVal === 'sampleImage') { // 샘플 이미지 선택
                    inputImg = sampleImg;
                    submitImg();
                }

                $('.avr_1').hide();
                $('.avr_2').show();
            });

            $('.btn_back1').on('click', function () {
                if (request) { request.abort(); }
                $('.avr_2').hide();
                $('.avr_1').show();
            });

            $('.btn_back2').on('click', function () {
                if (request) { request.abort(); }

                initialResultImg = null;
                initialPklFile = null;

                inputImg = null;
                pklFile = null;
                dictFile = null;
                dictObj = {"psi_post" : 1};
                selectedStyle = "";

                $('.slt_list select option:eq(0)').attr('selected', 'selected');
                $('.slt_list select').val("");

                $('#styleControlInput').val(1);
                $('#effectControlInput').val(2);
                $('.btn_list li button, .slt_list li .select').removeClass('active');
                $('.btn_contrast').click();

                applyFill01(sliders01.querySelector('input'));
                applyFill02(sliders02.querySelector('input'));

                $('.avr_3').hide();
                $('.avr_1').show();

            });

            // 이미지 새로고침 -- 처음 데이터로 변경
            $('#reset').on('click', function(){
                $('#resultImg').attr("src", "data:image/jpeg;base64," + initialResultImg);
                pklFile = initialPklFile;
                dictObj = {"psi_post": 1};
            });

            $('.btn_creat_newImg').on('click', function () {
                $('.imgBox').addClass('loading');
                $('.result_file').addClass('disabled');
                submitRandomAnimationImg();
            });


            // face:make
            $('#effectControlInput').on('change', function(){
                $('.imgBox').addClass('loading');
                $('.result_file').addClass('disabled');
                submitImg();
            });

            // style:edit
            $('#styleControlInput').on('change', function(){
                if(selectedStyle !== ""){

                    if(selectedStyle === "psi_post" && styleValue[$(this).val()] === 0){
                        dictObj[selectedStyle] = 1;
                    }else{
                        dictObj[selectedStyle] = styleValue[$(this).val()];
                    }
                    console.log(JSON.stringify(dictObj));

                    dictFile = createDictTextFile(JSON.stringify(dictObj));

                    $('.imgBox').addClass('loading');
                    $('.result_file').addClass('disabled');

                    submitStyleEdit();
                }
            });


        });
    });



    // 랜덤 애니메이션 이미지
    function submitRandomAnimationImg() {

        let formData = new FormData();
        formData.append($("#key").val(), $("#value").val());

        request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status !== 0) {

                let json = JSON.parse(request.response);
                let img = json.payload.img;
                let latent = json.payload.latent;

                initialResultImg = img;
                pklFile = createLatentPklFile(latent);
                initialPklFile = pklFile;

                $('.imgBox').removeClass('loading');
                $('.result_file').removeClass('disabled');

                $('#resultImg').attr("src", "data:image/jpeg;base64," + img);
                $('.avr_2').hide();
                $('.avr_3').show();

                $('.effect_control').removeClass('on');
                $('.btn_creat_newImg').addClass('on');
            }
        };

        request.open('POST', '/api/animeFace/sample:make');
        request.send(formData);
        request.addEventListener("abort", function () {});

    }

    // 샘플 이미지, 사용자 업로드 이미지
    function submitImg(){

        let formData = new FormData();
        formData.append($("#key").val(), $("#value").val());
        formData.append('file', inputImg);
        formData.append('steps', effectValue[$('#effectControlInput').val()]);

        request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status !== 0){

                if(request.response && request.response !== ""){
                    let json = JSON.parse(request.response);
                    let img = json.payload.img;
                    let latent = json.payload.latent;

                    console.log(json.message);

                    if(!initialResultImg) { initialResultImg = img; }
                    if(!initialPklFile){ initialPklFile = pklFile; }
                    pklFile = createLatentPklFile(latent);

                    $('.imgBox').removeClass('loading');
                    $('.result_file').removeClass('disabled');
                    $('.effect_control').addClass('on');
                    $('.btn_creat_newImg').removeClass('on');

                    $('#resultImg').attr("src", "data:image/jpeg;base64," + img);
                    $('.avr_2').hide();
                    $('.avr_3').show();

                } else{
                    console.log("status code : " + request.status + "\nstatus text : " + request.responseText);
                    alert("서버와 통신이 원활하지 않습니다.");
                }

            }
        };

        request.open('POST', '/api/animeFace/face:make');
        request.send(formData);
        request.addEventListener("abort", function(){ });

    }


    function submitStyleEdit() {
        let formData = new FormData;

        formData.append($("#key").val(), $("#value").val());
        formData.append("latent", pklFile);
        formData.append("dict", dictFile);

        request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status !== 0){

                if(request.response || request.response !== ""){
                    console.log(JSON.parse(request.response).message);

                    let json = JSON.parse(request.response);
                    let img = json.payload.img;
                    let latent = json.payload.latent;

                    pklFile = createLatentPklFile(latent);

                    $('.imgBox').removeClass('loading');
                    $('.result_file').removeClass('disabled');
                    $('#resultImg').attr("src", "data:image/jpeg;base64," + img);
                }else{
                    console.log("status code : " + request.status + "\nstatus text : " + request.responseText);
                    alert("서버와의 통신이 원활하지 않습니다.");
                }

            }
        };

        request.open('POST', '/api/animeFace/style:edit');
        request.send(formData);
        request.addEventListener("abort", function(){ });
    }

    // create latent pickle file (.pkl)
    function createLatentPklFile(latent) {
        let filename = Date.now() + Math.random().toString(36).substring(2, 15) + '.pkl';
        let contentType = 'application/octet-stream';
        let blob = b64toBlob(latent, contentType);

        return new File([blob], filename);
    }

    // create dict text file (.txt)
    function createDictTextFile(text) {
        let filename = Date.now() + Math.random().toString(36).substring(2, 15) + '.txt';
        return new File([text], filename, {type: 'text/plain'});
    }

    function b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        let byteCharacters = atob(b64Data);
        let byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            let slice = byteCharacters.slice(offset, offset + sliceSize);

            let byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            let byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        return new Blob(byteArrays, {type: contentType});
    }

    // 이미지 다운로드
    function downloadResultImg(){
        let img = document.getElementById('resultImg');
        let link = document.getElementById("save");
        link.href = img.src.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
        link.download = "AnimeFaceResult.png";
    }


    // ------------------------ slide bar
    var settings = {
        fill: '#2c3f51',
        background: '#cfd5eb'
    }

    const sliders01 = document.querySelector('.effect_control .range_slider');
    const sliders02 = document.querySelector('.style_control .range_slider');


    sliders01.querySelector('input').addEventListener('input', (event) => {
        applyFill01(event.target);
    });
    applyFill01(sliders01.querySelector('input'));

    function applyFill01(sliders01) {
        const percentage = 100 * (sliders01.value - sliders01.min) / (sliders01.max - sliders01.min);
        const bg = `linear-gradient(90deg, \${settings.fill} \${percentage}%, \${settings.background} \${percentage+0.1}%)`;
        sliders01.style.background = bg;
    }

    sliders02.querySelector('input').addEventListener('input', (event) => {
        applyFill02(event.target);
    });
    applyFill02(sliders02.querySelector('input'));

    function applyFill02(sliders02) {
        const percentage = 100 * (sliders02.value - sliders02.min) / (sliders02.max - sliders02.min);
        const bg = `linear-gradient(90deg, rgba(247, 119, 138) \${percentage}%, \${settings.background} \${percentage+0.1}%)`;
        sliders02.style.background = bg;
    }
    // ------------------------ //slide bar


    // 샘플 이미지 준비
    function loadSampleImg() {
        let blob = null;
        let xhr = new XMLHttpRequest();
        xhr.open("GET", "/aiaas/common/images/img_animeFace_sample01.png");
        xhr.responseType = "blob";
        xhr.onload = function()
        {
            blob = xhr.response;
            sampleImg = new File([blob], "img_animeFace_sample01.png");
        };

        xhr.send();
    }

    // API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace("active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    document.getElementById("defaultOpen").click();
</script>