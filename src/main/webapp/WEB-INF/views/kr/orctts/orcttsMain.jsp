<%--
  Created by IntelliJ IDEA.
  User: password-mindslab
  Date: 2021-11-22
  Time: 오후 3:54
To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<html>
<head>
    <title>AI Builder</title>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+KR:wght@400;500;700&display=swap" rel="stylesheet">
<%--    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/all.min.css" />--%>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/orcbuilder.css" />
<%--    <link rel="stylesheet" href="${pageContext.request.contextPath}/https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">--%>
<%--    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/swiper-6.2.0.css?ver=<%=fmt.format(lastModifiedStyle)%>">--%>
<%--    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/maum_landing.css?ver=<%=fmt.format(lastModifiedStyle)%>">--%>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/fontawesome-all.min.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/font.css" />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/all.min.css" />
</head>

<body class="is-preload">
<%--<%@ include file="../common/header.jsp" %>--%>

<!-- .page loading -->
<div id="pageldg">
    <span class="out_bg">
        <em>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <b>&nbsp;</b>
        </em>
    </span>
</div>
<!-- //.page loading -->
<!-- Wrapper -->
<div id="wrapper">
<%--    <%@ include file="../common/header.jsp" %>--%>
<%--    <%@ include file="../common/common_header.jsp" %>--%>
    <!-- Main -->
    <div id="main">
        <div class="inner">
            <!-- Header -->
            <header id="header">
                <h1>
                    <a href="#" class="logo"><strong>AI Builder</strong></a>
                </h1>
            </header>

                <section>
                    <h2>마인즈랩 TTS</h2>
                    <p>원하는 내용을 입력하고 목소리를 선택한 다음 '음성 생성'을 클릭하여 보세요.</p>

                    <h3>Text to speak</h3>
                    <textarea id="inputText" placeholder="Enter your message" rows="6"></textarea>

                    <div class="row gtr-200">
                        <div class="col-6">
                            <h3>목소리 선택</h3>
                                <select name="voice" id ="voice_select">
                                    <option value="">목소리 선택</option>
                                    <option value="kor_female1">한국어 성인 여자 - 차분한</option>
                                    <option value="kor_female2">한국어 성인 여자 - 발랄한</option>
                                    <option value="kor_female3">한국어 성인 여자 - 자연스러운</option>
                                    <option value="kor_female4">한국어 성인 여자 - 정직한</option>
                                    <option value="kor_female5">한국어 성인 여자 - 개성있는</option>
                                    <option value="kor_male1">한국어 성인 남자 - 자연스러운</option>
                                    <option value="kor_male2">한국어 성인 남자 - 진중한</option>
                                    <option value="kor_male3">한국어 성인 남자 - 차분한</option>
                                    <option value="kor_male4">한국어 성인 남자 - 친근한</option>
                                    <option value="kor_kids_m">한국 남자 아이</option>
                                    <option value="kor_kids_f">한국 여자 아이</option>
                                    <option value="baseline_eng">영어 - Selena</option>
                                    <option value="eng_male1">영어 - Brandon</option>
                            </select>
                        </div>
                    </div>

                    <div class="row gtr-200">
                        <div class="col-6">
                            <button type="button" id='enter' class='primary' >음성 생성</button>
                            <p></p>
                            <h5>
                                <a href="/home/orcerrorsolution" onclick="window.open(this.href,'_blank','width=700,height=1000,toolbars=no,scrollbars=no'); return false;"><strong>실행이 되지 않을 때 해결방법</strong></a>
                            </h5>
                        </div>

                        <div class="col-6 col-12-medium">
                            <audio class="audio" controls id="audio" src=""  type="audio/wav"></audio>
                            <img id="loading" src="${pageContext.request.contextPath}/aiaas/kr/images/loading.gif" width="50" style="display:none" />
                        </div>
                    </div>


                </section>


            <section>
                <h3>AI 서비스 제작을 문의합니다.</h3>
                <div class="row gtr-200">
                    <div class="col-6 col-12-medium">
                        <label for="name">이름</label>
                        <input type="text" id="name" placeholder="이름" autocomplete="off">

                        <label for="phone">연락처</label>
                        <input type="tel" pattern="[0-9]{3}-[0-9]{4}-[0-9]{4}" id="phone" placeholder="연락처" autocomplete="off">

                        <label for="mail">이메일</label>
                        <input type="email" id="mail" placeholder="이메일" autocomplete="off">

                        <label for="company">회사명</label>
                        <input type="text" id="company" placeholder="회사명" autocomplete="off">

                        <label for="company">연락가능 시간대</label>
                        <input type="text" id="time" placeholder="Ex) 오후 12시 이후" autocomplete="off">
                    </div>

                </div>

                <h3>개인정보 수집 동의</h3>

                <blockquote>수집하는 개인정보 항목: 이름, 연락처, 이메일 작성해주시는 개인 정보는 문의 접수 및 고객 불만 해결을 위해 1년간 보관됩니다.
                    본 동의를 거부할 수 있으나, 미동의 시 견적서 접수가 불가능합니다.</blockquote>

                <input type="checkbox" id="personal_info">
                <label for="personal_info">[필수]개인정보 수집 동의</label>
            </section>
            <section>

                <button class="primary" id="sendMailToHello_inquiry">문의하기</button>
            </section>

        </div>
    </div>

    <!-- Sidebar -->
    <div id="sidebar">
        <div class="inner">

            <!-- Menu -->
            <nav id="menu">

                <header class="major">
                    <h2>Menu</h2>
                </header>
                <ul>
                    <li>
                        <span class="opener">TTS(Text to Speech)</span>
                        <ul>
                            <li><a href="/home/orcttsMain">마인즈랩 - TTS</a></li>
                            <li><a href="#">GOOGLE - TTS</a></li>
                            <li><a href="#">MICROSOFT - TTS</a></li>
                            <li><a href="#">KAKAO - TTS</a></li>
                        </ul>
                    </li>
                    <li>
                        <span class="opener">STT(Speech to Text)</span>
                        <ul>
                            <li><a href="/home/orcsttMain">마인즈랩 - STT</a></li>
                            <li><a href="#">GOOGLE - STT</a></li>
                            <li><a href="#">MICROSOFT - STT</a></li>
                            <li><a href="#">KAKAO - STT</a></li>
                        </ul>
                    </li>
                    <li><a href="/home/orcttlMain">Text To Lipsync</a></li>
<%--                    <li>--%>
<%--                        <span class="opener">Translate</span>--%>
<%--                        <ul>--%>
<%--                            <li><a href="#">마인즈랩 - Translate</a></li>--%>
<%--                            <li><a href="#">GOOGLE - Translate</a></li>--%>
<%--                            <li><a href="#">MICROSOFT - Translate</a></li>--%>
<%--                            <li><a href="#">KAKAO - Translate</a></li>--%>
<%--                        </ul>--%>
<%--                    </li>--%>
                </ul>
            </nav>
        </div>
    </div>

</div>
<%@ include file="../common/footerLanding.jsp" %>

<!-- Scripts -->
<%--<script src="assets/js/orcjquery.min.js"></script>--%>
<%--<script src="assets/js/browser.min.js"></script>--%>
<%--<script src="assets/js/breakpoints.min.js"></script>--%>
<%--<script src="assets/js/util.js"></script>--%>
<%--<script src="assets/js/main.js"></script>--%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/swiper-4.5.1.min.css"/>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/cloudApi_service_main.css">

<!-- Local Script Library -->
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/swiper-4.5.1.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/orcjquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/orcbrowser.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/orcbreakpoints.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/orcutil.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/orcmain.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/orcttsmain.js"></script>
<script type="text/javascript">
    // swiper
    var getHttps;
    var swiperSuccess = new Swiper('.success_list_wrap', {
        initialSlide: 0,
        slidesPerGroup: 1,
        slidesPerView: 4,
        slidesPerColumn: 4,
        slidesPerColumnFill: 'row',
        // spaceBetween: 0,
        allowSlideNext: false,
        allowSlidePrev: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
        breakpoints: {
            768: {
                slidesPerView: 1,
                slidesPerColumn: 2,
                // spaceBetween: 0,
                allowSlideNext: true,
                allowSlidePrev: true,
            }
        }
    });

    var swiperEngines = new Swiper('.engines_list_wrap', {
        slidesPerGroup: 1,
        slidesPerView: 4,
        slidesPerColumn: 2,
        slidesPerColumnFill: 'row',
        allowSlideNext: false,
        allowSlidePrev: false,
        pagination: {
            el: '.swiper-pagination',
            clickable: true
        },
        breakpoints: {
            768: {
                slidesPerView: 1,
                allowSlideNext: true,
                allowSlidePrev: true,
            }
        }
    });

    $(window).resize(function () {
        swiperSuccess.update();
        swiperEngines.update();
    });

    // fixed된 header에 가로스크롤을 적용하기 위한 코드
    $(window).scroll(function () {
        $('.maumUI.transform .maum_sta').css('left', 0 - $(this).scrollLeft());
    });

    // #aside_btns 페이지 위로 버튼 display 이벤트
    $(window).scroll(function () {
        var scrollLocate = $(window).scrollTop();
        if (scrollLocate > 50) {
            $('#header').addClass('transform');
            $('#aside_btns .page_top').css('display', 'block');
        }
        if (scrollLocate < 50) {
            $('#header').removeClass('transform');
            $('#aside_btns .page_top').css('display', 'none');
        }
    });

    // 최상단 배너 닫기 버튼 클릭 이벤트
    $('.btn_banner_close').on('click', function () {
        var $banner = $(this).parents('.top_banner_area');
        $banner.remove();
        $('#wrap').removeClass('top_banner');
    });

    // page top 버튼 클릭 이벤트
    $('#aside_btns .page_top').on('click', function () {
        console.log('top click')

        $('html, body').animate({
            scrollTop: 0
        }, 500);
    });
</script>
<!-- AMR //공통으로 적용되는 script -->
<script>

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();



</script>


<script src="https://cdn.jsdelivr.net/npm/js-base64@3.6.1/base64.min.js"></script>

<script type="text/javascript">
    function validateEmail(sEmail) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        }
        else {
            return false;
        }
    }

    $(document).ready(function() {
        $('#sendMailToHello_inquiry').on('click', function () {

            if (validateEmail($('#mail').val()) == false) {
                alert("이메일 형식을 확인해 주세요!");
            } else if ($('#name').val() === '' || $('#phone').val() === '' || $('#mail').val() === '' || $('#time').val() === '' || $('#company').val() === '') {

                alert("내용을 모두 입력해야 이메일을 전달할 수 있어요!");

            } else if ($('#personal_info').prop('checked') == false) {

                alert("개인정보 수집 동의란에 체크해주셔야 문의하기가 완료됩니다.");

            } else {


                let title = "[AI builder] " + $("#name").val() + "님의 문의 사항입니다.";
                let msg = "이름 : " + $("#name").val() + "<br>이메일 : " + $('#mail').val() + "<br>연락처 : " + $("#phone").val() + "<br>회사명 : " + $("#company").val() + "<br>연락가능 시간대 : " + $("#time").val()
                    + "<br><br><br>* " + " '문의하기' 페이지를 통해 발송낸 메일입니다.";

                let formData = new FormData();

                formData.append('fromaddr', $('#mail').val());
                formData.append('toaddr', 'hello@mindslab.ai');
                formData.append('subject', title);
                formData.append('message', msg);
                formData.append($("#key").val(), $("#value").val());

                $.ajax({
                    type: 'POST',
                    async: true,
                    url: '/support/sendContactMail',
                    dataType: 'text',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (obj) {

                        alert("문의 접수가 완료 되었습니다.");

                        $("#name").val('');
                        $("#office").val('');
                        $("#phone").val('');
                        $("#company").val('');
                        $("#time").val('');
                        $("#mail").val('');
                        var placeholderLabel = $('.inquiry_box input, .inquiry_box textarea');
                        placeholderLabel.siblings('label').show();

                        $('#mail_success').fadeIn();

                    },
                    error: function (xhr, status, error) {
                        console.log("error");
                        alert("Contact Us 메일발송 요청 실패하였습니다.");
                        window.location.href = "/";
                    }
                });
            }
        });
    });

</script>


</body>
</html>