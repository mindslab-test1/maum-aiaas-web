<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-02-20
  Time: 09:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
    <div class="content">
        <h1>maum 회의록 </h1>
        <div class="demobox data_demobox">
            <p><span>maum 회의록 : 클라우드 회의록 시스템</span></p>
            <span class="sub">회의 내용의 녹음만으로 시스템에 UPLOAD하여 작은 수정 작업만으로 <br>
모든 사용자의 구분 및 발화 내용을 정리할 수 있습니다.</span>
            <div class="data_box minutes_box">
                <ul>
                    <li>
                        <h3>실시간 녹음기능</h3>
                        <p>버튼 한 번으로 실시간 녹음이 되고,<br>
                            종료 버튼을 통하여 회의 결과를<br>
                            실시간에 가깝게 처리합니다.
                        </p>
                    </li>
                    <li>
                        <h3>Download 기능</h3>
                        <p>녹음된 음성파일은 cloud에 보관되고<br>
                            언제 어디서든 download가 가능하며,<br>
                            회의 결과 및 음성파일은 별도로 저장하여 회의 참석자들에게 공유가 가능합니다.
                        </p>
                    </li>
                    <li>
                        <h3>회의실 관리 기능</h3>
                        <p>회의실 예약 관리를 한 번에. 별도의 구축비용 없이, 회의실의 상태를 조회/추가/삭제 가능합니다. 또한 예약 시스템을 통하여<br>
                            사용자 간 편리성을 제공합니다.</p>
                    </li>

                </ul>
                <a class="minutes_btn Button_Minutes">maum 회의록 바로가기 <em class="fas fa-angle-right"></em></a>
            </div>

        </div>

    </div>
    <!-- //.content -->
</div>
<!-- //.contents -->

<%-- 회의록 관련 스크립트 --%>
<script type="text/javascript">
    $('.Button_Minutes').click(function () {
        var newWindow = window.open('about:blank');
        newWindow.location.href = 'https://minutes.maum.ai/';
    });
</script>