<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2019-09-19
  Time: 16:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
    <div class="content">
        <h1>데이터 정제 서비스</h1>
        <div class="demobox data_demobox">
            <p><span>가지고 있는 데이터를 활용하고 싶으신가요?</span></p>
            <span class="sub">전문 AI 컨설턴트와 상담하여 갖고 있는 데이터를 적극 활용하여 인공지능 엔진을 만들어보세요.<br>
적은 데이터도 가공하고 정제해드립니다. 문의: <em>maumdata@mindslab.ai</em></span>
            <div class="data_box">
                <ul class="dt_service">
                    <li>
                        <h3>전문 AI 컨설턴트의 <br>
                            밀착 지원 </h3>
                        <p>인공지능을 사용하고 싶지만, 어떤 데이터가 필요한지 모르신다구요? 저희 AI 전문 컨설턴트가  밀착 지원하여 필요한 데이터를 가공하고 정제해드립니다. 지금 바로 신청해보세요.</p>
                    </li>
                    <li>
                        <h3>엔진 연구 개발에 직접 관여한 <br>
                            Researcher의 노하우</h3>
                        <p>마인즈랩에서는 알고리즘부터 데이터까지, 직접 연구하여 AI 엔진들을 개발하기 때문에, 타사에서 지니고 있지 못하는 전문적인 지식 및 노하우들을 경험으로 습득하였습니다. 엔진 개발에 직접 참여한 연구자들의 데이터 정제 서비스를 경험해보세요.</p>
                    </li>
                    <li>
                        <h3>시장에서 이미 입증된 <br>
                            AI 데이터 서비스 </h3>
                        <p>100개 이상의 AI 프로젝트를 진행하면서 구축해놓은 데이터 셋은 업계 최대 규모 수준입니다. 이를 목적에 맞게 정제하여 제공해드립니다.</p>
                    </li>

                </ul>

            </div>


        </div>


    </div>
    <!-- //.content -->
</div>
<!-- //.contents -->