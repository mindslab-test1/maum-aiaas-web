<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2019-09-19
  Time: 16:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
    <div class="content">
        <h1>데이터 정제 서비스</h1>
        <div class="demobox data_demobox">
            <p><span>maum Data</span></p>
            <span class="sub">Customized AI 서비스를 제공하기 위한 maum.ai만의 데이터 편집 툴입니다.<br>
전문적이고 정교한 데이터 정제 작업을 경험해보세요.  문의 : <em>maumdata@mindslab.ai</em></span>
            <div class="data_box date_new">
                <div class="step_area">
                    <h5><span>1</span> 작업 의뢰</h5>
                    <p>의뢰자가<br>Customized<br>AI Model을 위한<br>작업을 의뢰<br>(데이터 제공)</p>
                </div>
                <div class="step_area">
                    <h5 class="span_color"><span>2</span> maum Data Edit Tool </h5>
                    <div class="step_center">
                        <div class="tool_box tool_voice">
                            <strong>Voice</strong>
                            <span>음성 듣고<em class="fas fa-chevron-right"></em></span>
                            <span>문장 단위별로<br>자르고<em class="fas fa-chevron-right"></em></span>
                            <span>다시 듣고<br>전사 작업</span>
                        </div>
                        <div class="tool_box tool_lang">
                            <strong>Language</strong>
                            <span>문단을 읽고<em class="fas fa-chevron-right"></em></span>
                            <span>질문을 만들고<em class="fas fa-chevron-right"></em></span>
                            <span>답변을 문단에서<br>찾고 이유 태깅</span>
                        </div>
                        <div class="tool_box tool_vision">
                            <strong>Vision</strong>
                            <span>영상을 보고<em class="fas fa-chevron-right"></em></span>
                            <span>구간을 선택하고<em class="fas fa-chevron-right"></em></span>
                            <span>분류 카테고리<br>선택</span>
                        </div>
                    </div>
                </div>
                <div class="step_area last_step">
                    <h5><span>3</span> 완료 및 데이터 전달</h5>
                    <p>Customized<br>AI Model<br>생성하여<br>의뢰자에게 전달</p>
                </div>
                <a href="${maumDataLink}" target="_blank" class="go_tool">maum Data Edit Tool 바로가기</a>

            </div>


        </div>

    </div>
    <!-- //.content -->
</div>
<!-- //.contents -->