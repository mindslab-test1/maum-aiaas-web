<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2019-09-19
  Time: 16:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
    <div class="content">
        <h1>판매 데이터</h1>
        <div class="demobox data_demobox">
            <p><span>데이터가 필요하신가요?</span></p>
            <span class="sub">실제 인공지능 엔진 학습에 활용된 데이터 셋을 판매 중에 있습니다.<br>
더 다양한 데이터 셋 구입을 원하시면 데이터팀에 문의하세요. 문의 : <em>maumdata@mindslab.ai</em></span>
            <div class="data_box">
                <ul>
                    <li>
                        <h3>TTS 녹음용<br>
                            표준 스크립트 </h3>
                        <p>글자를 음성 언어로 변환하기 위한 기본적인 학습 데이터 셋입니다. 다양한 감정 표현과 문장 어미를 TTS가 자연스럽게 구사하기 위한 녹음용 표준 스크립트입니다. 마인즈랩의 노하우가 담긴 표준 스크립트로 지금 바로 인공지능 TTS를 구축해보세요.</p>
                    </li>
                    <li>
                        <h3>실제 고객 상담원<br>
                            Y씨의 음성파일 </h3>
                        <p>실제 상담원 발화 패턴에 따라 녹음이 진행된 마인즈랩 고객센터 여성 상담원 Y씨의 실제 녹음 음성 파일입니다. 일반 고객센터 상담원 목소리로 차분하고 안정감 있는 톤입니다. 3시간 분량의 음성파일 및 스크립트를 제공해드립니다.</p>
                    </li>
                    <li>
                        <h3>MRC 35만건<br>
                            학습용 데이터 </h3>
                        <p>정확한 AI 기계 독해(MRC)를 위한 25만개의 본문 & 질문 페이링 데이터와 10만 개의 뉴스 데이터 셋을 포함하고 있습니다. 기계 독해뿐만 아니라 다양한 언어 지능 학습에 사용할 수 있습니다.</p>
                    </li>

                </ul>
                <div class="etc_box">그 외 다양한 데이터</div>
            </div>


        </div>

    </div>
    <!-- //.content -->
</div>
<!-- //.contents -->