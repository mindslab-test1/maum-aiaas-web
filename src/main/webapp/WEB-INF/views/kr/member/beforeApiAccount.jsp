<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<link href="https://fonts.googleapis.com/css?family=Vollkorn" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<script type="text/javascript">
	$(document).ready(function(){
		getApiAccountList();
	});
	
	/** 이용내역 - 목록 조회  */
	function getApiAccountList(currentPageNo){

		if(currentPageNo === undefined){
			currentPageNo = "1";
		}
		
		$("#current_page_no").val(currentPageNo);
		
		$.ajax({	
			url		:"/member/getApiAccountList",
		    data    : $("#apiAccoutForm").serialize(),
	      	dataType:"JSON",
	      	cache   : false,
			async   : true,
			type	:"POST",	
	      	success : function(obj) {
				$("#apiAccountList").html($("#listTemplate").render(obj));
				$("#pagination").html(obj.data.pagination);
	      },	       
	      error 	: function(xhr, status, error) {}
	        
	     });
	}	
</script>

<script id="listTemplate" type="text/x-jsrender">
{{for data.list}}
									<tr>
										<td scope="row" >{{>num}}</td>
										<td>{{>createDate}}</td>
										<td>{{>type}}</td>
										<td>{{>point}}</td>
										<td><em></em>{{>point}}</td>
									</tr>
{{/for}}
</script>
<!-- .contents -->
		<div class="contents">
			<div class="content">
				<h1>API 정보</h1>
				<!--.demobox_nlu-->
				<div class="demobox">
					<div class="account">
						<p>API 계정</p>
						<div class="stn_1">
							<dl>
								<dt>ID</dt>
								<dd>${fn:escapeXml(sessionScope.accessUser.email)}</dd>						
							</dl>
							<dl>
								<dt>Key</dt>
								<dd>kdiengoxihc37772</dd>						
							</dl>
							<p>* Business Plan을 이용하시는 경우에만 API ID 발급이 가능합니다. 서비스 업그레이드를 원하시면 <a href="/doc/pricing">여기</a>를 눌러주세요.</p>
						</div>
						<form id="apiAccoutForm" name="apiAccoutForm">
						<div class="stn_2">
							<input type="hidden" id="function_name" name="function_name" value="getApiAccountList" />
							<input type="hidden" id="current_page_no" name="current_page_no" value="1" />
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />						
							<p>API 사용 내역</p>
							<table>
								<colgroup>
									<col width="10%"><col width="20%"><col width="240px"><col width="20%"><col width="20%">
								</colgroup>
								<thead>
									<tr class="thead">
										<th scope="col">No.</th>
										<th scope="col">일자</th>
										<th scope="col">서비스</th>
										<th scope="col">이용량</th>
										<th scope="col">월 누적량</th>
									</tr>
								</thead>
								<tbody id="apiAccountList">
								</tbody>
							</table>	
							<!-- 페이징 -->
							<div id="pagination"></div>
							<!-- //페이징 -->
						</div>	
						</form>						
					</div>
				</div>
				<!--//account-->
			</div>

		</div>
		<!-- //.contents -->