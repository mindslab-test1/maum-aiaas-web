<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<c:import url="/payment/billingForm?method=billingChange"></c:import>
<%@ page import="console.maum.ai.member.constant.UserStatus"%>

<!-- .lyr_plan -->
<div class="lyr_plan" id="changePlan">
	<div class="lyr_plan_bg"></div>
	<!-- .productWrap -->
	<div class="lyrWrap">				
		<button class="btn_lyrWrap_close" type="button">제품 리스트 닫기</button>				
		<!-- .lyr_bd -->
		<div class="lyr_bd">
			<em class="fas fa-briefcase"></em>
			<p>플랜 변경</p>
			<div class="planstep">
				<span class="txt_fl">플랜 선택</span>
				<div class="selectarea">
					<div class="radio">
						<input type="radio" id="plan_basic" name="plan" value="BASIC">
						<label for="plan_basic"><span>BASIC</span></label>
					</div>
					<div class="radio">
						<input type="radio" id="plan_business" name="plan" value="BUSINESS">
						<label for="plan_business"><span>BUSINESS</span></label>
					</div>
				</div>
			</div>
			<div class="planstep" style="padding: 10px 0px; margin-bottom: 20px;">
				<div class="radio" style="margin-left: 45px; width: 200px;">
					<input type="radio" id="radio_timing_now" name="timing" checked="checked">
					<label for="radio_timing_now"><span>지금 바로 변경</span></label>
				</div>
				<div class="radio" style="margin-left: 45px; width: 200px;">
					<input type="radio" id="radio_timing_later" name="timing">
					<label for="radio_timing_later"><span>다음 결제일 변경</span></label>
				</div>
			</div>
			<button id="changeSubmit" class="btn_blue planchangeBtn">OK</button>
		</div>
		<!-- //.lyr_bd -->
		
	</div>
	<!-- //.productWrap -->   
</div>
<!-- //.lyr_plan -->
<!-- .plan_oklayer -->
<div class="lyr_plan plan_oklayer">
	<div class="lyr_plan_bg"></div>
	<!-- .lyrWrap -->
	<div class="lyrWrap">
		<button class="btn_lyrWrap_close" type="button">제품 리스트 닫기</button>
		<!-- .lyr_bd -->
		<div class="lyr_bd">
			<em class="fas fa-briefcase"></em>
			<p>플랜 변경</p>
			<span>플랜 변경이 완료되었습니다.</span>
			<button id="planChangeResult" class="btn_blue">OK</button>
		</div>
		<!-- //.lyr_bd -->
	</div>
	<!-- //.lyrWrap -->
</div>
<!-- //.plan_oklayer -->
<!-- .lyr_planconf -->
<div class="lyr_planconf">
    <div class="lyr_plan_bg"></div>
    <!-- .lyrWrap -->
    <div class="lyrWrap">
        <button class="btn_lyrWrap_close" type="button">제품 리스트 닫기</button>
        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <em class="fas fa-sad-cry"></em>
            <p>멤버십을 해지하시겠습니까?</p>
			<span>구독이 취소되더라도 구독 결제를 소급하여 환불하지 않으며 이전에 청구된 구독 요금은 <br>
취소일에 비례하여 계산되지 않습니다.<br>
중도 해지시 환불은 없고, 다음 카드 결제일 전날까지 서비스 이용은 유지됩니다.<br>
첫1달 Free 사용자의 경우 가입 해지 시 모든 서비스가 즉시 중지됩니다.
</span>
			<div class="stn_survey">
				<p>해지 이유를 공유해주시면 더 나은 서비스를 만들도록 하겠습니다.</p>
				<div class="survey_box">
					<span class="txt_fl"><strong>1.웹사이트 사용성 만족도</strong>: 서비스 이용 방법, 사용 순서, 버튼 인식 등 </span>
					<div class="selectarea">
						<div class="radio">
							<input type="radio" id="1_1" name="radio_usability" value="5">
							<label for="1_1"><span>매우 만족</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="1_2" name="radio_usability" value="4">
							<label for="1_2"><span>만족</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="1_3" name="radio_usability" value="3">
							<label for="1_3"><span>보통</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="1_4" name="radio_usability" value="2">
							<label for="1_4"><span>불만족</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="1_5" name="radio_usability" value="1">
							<label for="1_5"><span>매우 불만족</span></label>
						</div>
					</div>
				</div>
				<div class="survey_box">
					<span class="txt_fl"><strong>2.데모 서비스 성능 만족도</strong>:  품질, 성능, 결과 만족도</span>
					<div class="selectarea">
						<div class="radio">
							<input type="radio" id="2_1" name="radio_demo" value="5">
							<label for="2_1"><span>매우 만족</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="2_2" name="radio_demo" value="4">
							<label for="2_2"><span>만족</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="2_3" name="radio_demo" value="3">
							<label for="2_3"><span>보통</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="2_4" name="radio_demo" value="2">
							<label for="2_4"><span>불만족</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="2_5" name="radio_demo" value="1">
							<label for="2_5"><span>매우 불만족</span></label>
						</div>
					</div>
				</div>

				<div class="survey_box">
					<span class="txt_fl"><strong>3.필요 서비스 요청 및 추가 의견</strong></span>
					<div class="text_area">
						<textarea id="input_etc"></textarea>
					</div>
				</div>

			</div>
			<span class="last_txt">해지 하시겠습니까?</span>
            <button class="btn" id="notTerminate">아니오</button>
            <button class="btn_blue" id="terminateBtn">네</button>

        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.lyrWrap -->
</div>
<!-- //.lyr_planconf -->

<div class="lyr_receipt">
	<div class="lyr_plan_bg"></div>
	<!-- .productWrap -->
	<div class="lyrWrap">				
		<button class="btn_lyrWrap_close" type="button">제품 리스트 닫기</button>				
		<!-- .lyr_bd -->
		<div class="lyr_bd">
			<img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_receipt.png" alt="receipt" class="receiptPop">
			<p>영수증</p>
			<ul>
				<li><span>날짜</span><strong id="receipt_date"></strong></li>
				<li><span>제품</span><strong id="receipt_good"></strong></li>
				<li><span>결제카드</span><strong id="receipt_method"></strong></li>
				<li><span>결제구분</span><strong>일시불</strong></li>
				<li><span>승인번호</span><strong id="receipt_pano"></strong></li>
				<li><span>가격</span><strong class="price" id="receipt_price"></strong></li>
			</ul>
			<button id="receiptBtn" class="btn_blue">확인</button>

		</div>
		<!-- //.lyr_bd -->

	</div>
	<!-- //.productWrap -->   
</div>
<!-- //.lyr_receipt -->
<!-- .plan_change -->
<div class="lyr_plan plan_change">
    <div class="lyr_plan_bg"></div>
    <!-- .lyrWrap -->
    <div class="lyrWrap">
        <button class="btn_lyrWrap_close" type="button">제품 리스트 닫기</button>
        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <em class="fas fa-check"></em>
            <p>멤버십 해지</p>
            <span>멤버십 해지가 완료되었습니다.</span>
            <button class="btn_blue" id="completBtn">OK</button>
        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.lyrWrap -->
</div>
<!-- //.plan_change -->
<!-- .contents -->
		<div class="contents">
			<div class="content">
				<h1>결제 정보</h1>
				<!--.demobox_nlu-->
				<div class="demobox">
					<div class="account">
						<div class="stn_1">
							<ul>
								<li><span>플랜</span><em id="plan_name">현재 적용중인 플랜이 없습니다.</em>
<%--									<button type="button" class="changePlan" id="changePlanBtn">변경</button>--%>
<%--									<span class="unsubscribe" style="cursor:pointer;margin:0 0 0 20px;font-weight: 300;">구독해지</span>--%>
										<button type="button" class="unsubscribe">구독해지</button>
								</li>
								<li><span>가격</span><strong id="plan_price">&#8361; 0</strong> / 1 개월</li>
								<li><span>결제 정보</span><em id="plan_pay_info" ></em><button type="button" class="changeMethod">변경</button></li>
								<li><span id="nextBillingTitle">다음 결제정보</span><em id="nextBilling"><em id="nextBillingDate"></em><em>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</em><em id="nextBillingGoodName"></em><em>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</em><em id="nextBillingPrice"></em></em></li>

							</ul>
						</div>

						<div class="stn_2">
							<p>결제 내역</p>
							<table class="payment_tbl">
								<colgroup>
									<col width="25%"><col width="25%"><col width="16%"><col width="16%"><col width="14%">
								</colgroup>
								<thead>
									<tr class="thead">
										<th scope="col">사용 기간</th>
										<th scope="col">결제 정보</th>
										<th scope="col">가격</th>
										<th scope="col">결제일</th>
										<th scope="col">영수증</th>
									</tr>
								</thead>
								<tbody id="list_body">
								</tbody>
							</table>	
							<!-- 페이징 -->
<%--							<div class="pageing">--%>
<%--								<a href="#" class="first">처음페이지</a>--%>
<%--								<a href="#" class="prev">이전페이지</a>--%>
<%--								<strong>1</strong>--%>
<%--								<a href="#">2</a>--%>
<%--								<a href="#">3</a>--%>
<%--								<a href="#">4</a>--%>
<%--								<a href="#">5</a>--%>
<%--								<a href="#">6</a>--%>
<%--								<a href="#" class="next">다음페이지</a>--%>
<%--								<a href="#" class="end">마지막페이지</a>--%>
<%--							</div>--%>
							<!-- //페이징 -->
						</div>	
					</div>
				</div>
				<!--//account-->
			</div>

		</div>
		<!-- //.contents -->

<script type="text/javascript">

	var status;
	var paymentList = ${paymentList};
	var nextBillingInfo = ${nextBillingInfo};
	console.dir(nextBillingInfo);

	function cardNoDisplay (cardNo) {
        if(cardNo != null) {
            if(cardNo == "paypalcard"){
                return cardNo;
            }else {
                return cardNo.toString().replace(/(\d|\*){4}/g, "$& ");
            }
        }else {
            return "";
        }
	}

	function priceDisplay (price) {
		return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	jQuery.event.add(window, "load", function () {
		$(document).ready(function () {

			var payDate;
			var goodName;
			var price;

			// --------------------------------- 현재 결제 정보 dispaly
			var paymentNow = {};
			paymentNow = ${billingInfo};
			console.dir(paymentNow);

			payDate = paymentNow.payDate;
			goodName = paymentNow.goodName;

			try {
				$("input[name='plan'][value=" + paymentNow.goodName + "]").attr("checked", "checked");
				$("#plan_name").html(paymentNow.goodName);
				$("#plan_price").html("&#8361; " + priceDisplay(paymentNow.price));
                if(paymentNow.cardNo == null) {
                    $("#plan_pay_info").html("");
                    $("#plan_price").html("&#36; " + "");
                }else if(paymentNow.cardNo == "paypalcard"){
					price = "&#36; " + paymentNow.priceUs;
					$("#plan_price").html(price);
					$("#plan_pay_info").html("PayPal");
					$(".changeMethod").remove();
                }else{
					price = "&#8361 " + paymentNow.price;
					$("#plan_price").html(price);
                    $("#plan_pay_info").html(paymentNow.issuerName + "카드 " + cardNoDisplay(paymentNow.cardNo));
                }

				console.dir(paymentList);
				console.log(paymentList.length);

                for (let i = 0; i < paymentList.length; i++){
                    if(paymentList[i].payment != 'Free'){
                        console.log("==============================");
                        console.log(paymentList[i]);
                        console.log("==============================");
                        if(paymentList[i].payment == 'PayPal'){
                            $("#list_body").append(
                                "<tr>" +
                                "<td scope='row' >" + paymentList[i].dateFrom + " ~ " + paymentList[i].dateTo + "</td>" +
                                "<td style=\"text-align: center;\">"+ paymentList[i].cardNo+ "</td>" +
                                "<td>&#36; " + priceDisplay(paymentList[i].price)+ "</td>" +
                                "<td>" + paymentList[i].payDate + "</td>" +
                                "<td><img src='${pageContext.request.contextPath}/aiaas/kr/images/ico_receipt.png' alt='receipt' class='receiptPop' data-pano='"+ paymentList[i].pano +"'></td>" +
                                "</tr>"
                            )
                        }else{
                            $("#list_body").append(
                                "<tr>" +
                                "<td scope='row' >" + paymentList[i].dateFrom + " ~ " + paymentList[i].dateTo + "</td>" +
                                "<td>" + paymentList[i].issuerName  + "card " + cardNoDisplay(paymentList[i].cardNo) + "</td>" +
                                "<td>&#8361; " + priceDisplay(paymentList[i].price) + "</td>" +
                                "<td>" + paymentList[i].payDate + "</td>" +
                                "<td><img src='${pageContext.request.contextPath}/aiaas/kr/images/ico_receipt.png' alt='receipt' class='receiptPop' data-pano='"+ paymentList[i].pano +"'></td>" +
                                "</tr>"
                            )
                        }
                    }
                }
			} catch (err) {
				if (paymentNow == null) {
					$(".changeMethod").remove();
					$(".changePlan").text('가격정책');
				}
			}

			//-------------------------------------------------------------

			$("#nextBillingDate").html(payDate);
			$("#nextBillingGoodName").html(goodName);
			$("#nextBillingPrice").html(price);

			$.ajax({
				url: "/member/getUserStatus",
				data: {"${_csrf.parameterName}": "${_csrf.token}"},
				type: "POST",
				dataType: "JSON",

				success: function (response) {

					status = response;

					if (response == ${UserStatus.UNSUBSCRIBING}) {
						$('.stn_1 .unsubscribe').hide();
						$('.stn_1 li:eq(0)').append('<button type="button" class="unsubscribe2" disabled>[ 구독 해지 중 ]</button> ');
						if(paymentNow.cardNo != "paypalcard"){
							$('.stn_1 li:eq(0)').append('<button type="button" id="btn_cancelUnsub" class="cancel_unsub">해지 취소</button>');
						}

						$('#btn_cancelUnsub').on("click", function () {
							$.ajax({
								url: "/member/updateUserStatus",
								data: {
									status: '${UserStatus.SUBSCRIBED}',
									"${_csrf.parameterName}": "${_csrf.token}"
								},
								dataType: "JSON",

								success: function (respo) {

									if (respo == 1) {
										alert("구독 해지가 취소 되었습니다.");
										$('.unsubscribe2').remove();
										$('#btn_cancelUnsub').remove();

										$('.stn_1 .unsubscribe').show();

										window.location.reload();
									} else {
										alert("서버와의 통신이 원활하지 않습니다.");
										console.log('사용자의 정보를 업데이트할 수 없습니다.');
									}
								},
								error: function () {
									alert("서버와의 통신이 원활하지 않습니다.");
									console.log("사용자 정보를 업데이트할 수 없습니다.");
								}
							});
						});

						$('#plan_pay_info').hide();
						$(".changeMethod").hide();
						$('#nextBillingTitle').text("구독 만료일");
						$("#nextBilling").html(paymentNow.payDate);

/*						$("#nextBillingDate").html(payDate);
						$("#nextBillingGoodName").html(goodName);
						$("#nextBillingPrice").html(price);*/
						if(nextBillingInfo != null) {
						    $("#nextBillingDate").html(nextBillingInfo.payDate);
							$("#nextBillingGoodName").html(nextBillingInfo.goodName);
							$("#nextBillingPrice").html(nextBillingInfo.price);
                        }
					}
				},
				error: function (e) {
					alert("서버와의 통신이 원활하지 않습니다.");
					console.log("사용자 정보를 가져올 수 없습니다.");
				}
			});

			$('#changePlanBtn').on('click', function () {
				if (paymentNow == null) {
					window.location.href = "/support/pricing"
					return;
				}

				$('#changePlan').fadeIn(300);
				$('.plan_oklayer').hide();

				$("#timing_now").click(function () {
					$("#radio_timing_now").attr("checked", "checked");
				});
				$("#timing_later").click(function () {
					$("#radio_timing_later").attr("checked", "checked");
				});

				$("#changeSubmit").click(function () {
					console.log($("input[name='plan']:checked").val());

					if ($("#radio_timing_now").is(":checked")) {
						window.location.href = "/support/pricing";
					} else if ($("#radio_timing_later").is(":checked")) {
						$.ajax({
							url: "/payment/planChangeConfirm",
							data: {
								'userNo': '${accessUser.userno}',
								'goodName': $("input[name='plan']:checked").val(),
								'${_csrf.parameterName}': '${_csrf.token}'
							}
						}).success(function (e) {
							console.log(e);

							if (e == 0) {
								$.ajax({
									url: "/member/changePlanNextBilling",
									data: {
										'goodName': $("input[name='plan']:checked").val(),
										'${_csrf.parameterName}': '${_csrf.token}'
									}
								}).success(function () {
									$('.lyr_plan').hide();
									$('.plan_oklayer').show();
								})
							} else {
								alert("현재 적용된 요금제와 동일한 요금제 입니다.");
							}
						})
					}
				})
			});

			// --------------------------------- 카드 정보 수정 function (inicis From)
			$('.changeMethod').on('click', function () {

				let signature;

				console.log($("input[name='returnUrl']").val());

				function setParam(price, goodName, sign, callback) {
					$("#billingPrice").val(price);
					$("#billingGoodName").val(goodName);
					$("#billingSignature").val(sign);
					console.dir($("#billingForm"));
					callback();
				}

				function payFunc() {
					INIStdPay.pay('billingForm');
				}

				if (paymentNow == null) {
					alert("현재 적용된 결제정보가 없습니다.");
					return;
				}

				if (paymentNow.goodName == "BASIC") {
					signature = $("#basicSignature").val();
				} else if (paymentNow.goodName == "BUSINESS") {
					signature = $("#businessSignature").val();
				} else {
					alert("error! " + paymentNow.goodName);
					return;
				}

				setParam(paymentNow.price, paymentNow.goodName, signature, payFunc);

			});
			//-------------------------------------------------------------

			// --------------------------------- 영수증 출력 function
			$('.receiptPop').on('click', function () {
				var pano = $(this).data("pano");

				$.ajax({
					url: "/member/payinfo",
					data: {pano: pano},
				"${_csrf.parameterName}" : "${_csrf.token}"
			}).
				done(function (data) {
					console.log(data);
					let payinfo = JSON.parse(data);
					console.dir(payinfo);

					$("#receipt_date").html(payinfo.payDate);
					$("#receipt_good").html(payinfo.goodName);
                    if(payinfo.issuerName == "PayPal"){
                        $("#receipt_method").html(payinfo.cardNo);
                        $("#receipt_price").html("$" + priceDisplay(payinfo.price));
                    }else {
                        $("#receipt_method").html(payinfo.issuerName + " " + cardNoDisplay(payinfo.cardNo));
                        $("#receipt_price").html(priceDisplay(payinfo.price) + " 원");
                    }
                    $("#receipt_pano").html(payinfo.pano.replace("mindslab01_", ""));
				});

				$('.lyr_receipt').fadeIn(300);
			});


			// ---------------------------------------------------------
			// --------------------------------- 영수증 확인 버튼 function
			$("#receiptBtn").click(function () {
				$('.lyr_receipt').fadeOut(300);
			});

			$("#notTerminate").click(function () {
				$('.lyr_planconf').fadeOut(300);
			});

			// product layer popup
			$('.btn_lyrWrap_close, .lyr_plan_bg').on('click', function () {
				$('#changePlan').fadeOut(300);
				// $('.lyr_method').fadeOut(300);
				$(".plan_change").fadeOut(300);
				$('.lyr_receipt').fadeOut(300);
				$('.lyr_planconf').fadeOut(300);
				$('.plan_oklayer').fadeOut();
				$('body').css({
					'overflow': ''
				});
			});

			$('#planChangeResult').click(function () {
				window.location.reload();
			});

			$('.unsubscribe').on('click', function () {
				$('.lyr_planconf').fadeIn(300);
				// $('.lyr_plan').fadeOut(300);
			});

			$('#terminateBtn').on('click', function () {
				var usability = 0;
				if (typeof $("input[name='radio_usability']:checked").val() != "undefined") usability = $("input[name='radio_usability']:checked").val();

				var demo = 0;
				if (typeof $("input[name='radio_demo']:checked").val() != "undefined") demo = $("input[name='radio_demo']:checked").val();

				$.ajax({
					url: "/payment/planCancel",
					type: "POST",
					data: {
						"${_csrf.parameterName}": "${_csrf.token}",
						"payMethod" : paymentNow.issuerName,
						"feedbackUsability": usability,
						"feedbackDemo": demo,
						"feedbackEtc": $('#input_etc').val()
					},

					success: function (response) {
						if (response.msg == "FAIL") {
							alert("Subscription cancel is failed.");
							console.log("Fail : " + response.errMsg);
							$('.lyr_planconf').hide();
							return;
						}
						$('.plan_change').fadeIn(300);
						$('.lyr_planconf').hide();

					},
					error: function (response) {
						alert("error");
						console.log(response);
						$('.lyr_planconf').hide();
                        location.reload();
					}
				});
			});

			$("#completBtn").on('click', function () {
				$(".plan_change").fadeOut(300);

				if(status == ${UserStatus.FREE}) {
					document.getElementById('logout-form').submit();
				} else {
					location.reload();
				}
			});
		});
	});
</script>