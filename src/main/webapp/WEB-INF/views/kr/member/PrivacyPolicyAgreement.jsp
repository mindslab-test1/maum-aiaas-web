<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="../common/common_header.jsp" %>

<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap" style="width:300px;">
        <button class="pop_close" type="button" hidden>닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-exclamation-triangle"></em>
            <p id="message">처리 완료 되었습니다.</p>

        </div>
        <!-- //.pop_bd -->
        <div class="btn" id="btn_ok">
            <a class="">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- wrap -->
<div id="wrap" class=" maumUI">
    <!-- header -->
    <%@ include file="../common/header.jsp" %>
    <!-- //header -->
    <div class="loginWrap">

        <div class="login_box signup_box mail_confirm">
            <div class="txt_box">
                <p class="animated fadeInUp">
                    <em class="fas fa-envelope-open-text"></em>
                </p>

                <span class="signup_desc animated fadeIn delay-1">
                    <strong>마인즈랩 소식 및 최신 AI 기술 동향 안내를 더이상 받지 못하게 됩니다.</strong>
                    수신거부를 원하시면, 회원의 경우 아래에 이메일주소를 기입 후 수신거부 버튼을 눌러주세요.<br>
                    비회원의 경우 customersuccess@mindslab.ai 로 수신거부 메일을 보내주세요.
				</span>

                <div class="email_outbox">
                    <form method="post">
                        <fieldset>
                            <legend>Email check</legend>
                            <div class="email_inbox">
                                <input id='input_email' type="email" placeholder="maum.ai에 가입한 이메일 주소"/>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <button type="button" class="input_done animated fadeIn delay-2" id="btn_reject">수신거부</button>
            </div>
        </div>
        <!-- #footer -->
        <div id="footer">
            <div class="lot_c">
                <div class="cyrt">
                    <p><a href="https://mindslab.ai:8080/kr/company" target="_blank"><strong>주식회사 마인즈랩 </strong></a> 경기도 성남시 분당구 대왕판교로644번길 49 다산타워 6층&nbsp;&nbsp;ㅣ&nbsp;&nbsp;<span>대표 유태준&nbsp;&nbsp;ㅣ&nbsp;&nbsp;사업자 등록번호 314-86-55446</span>
                        <strong class="footer_link"><a href="/home/pricingPage">가격정책</a><a href="/home/krTermsMain">이용약관</a><a href="mailto:hello@mindslab.ai">hello@mindslab.ai</a></strong>
                    </p>
                </div>
                <!-- 			<div class="cyrt"><span>MINDsLab &copy; 2019</span></div> -->
            </div>
        </div>
        <!-- //#footer -->

    </div>
</div>
<!-- //wrap -->

<script>
    var progress_flag = false;

    $('#btn_reject').click(function() {
        // 이미 진행중이면 무시한다.
        if(progress_flag == true) return;

        progress_flag = true;
        $.ajax({
                type: 'POST',
                url: '/member/rejectMarketingAgree',
                data: {
                    "email": $("#input_email").val()
                },
                success: function(data){
                    var result;
                    if(data.toString() != 'succ') {
                        result = '등록된 계정의 이메일이 아닙니다.';
                    }
                    else {
                        result = '수신 거부 설정되었습니다.';
                    }

                    $('#message').text(result);
                    $('.pop_simple').fadeIn(300);

                    progress_flag = false;
                },
                error: function(req, err){
                    $('#message').text('연결이 되지 않습니다. 잠시 후, 다시 시도하세요.');
                    $('.pop_simple').fadeIn(300);
                    progress_flag = false;
                }
            }
        );

    });

    $('#btn_ok').click(function () {
        $('.pop_simple').fadeOut(300);
    });

</script>
<script type="text/javascript">

    function linkExtService(site_url) {
        var newWindow = window.open('about:blank');
        newWindow.location.href = "${pageContext.request.contextPath}/login/linkExtService?serviceUrl=" + site_url;
    }

</script>