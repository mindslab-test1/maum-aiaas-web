<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<script type="text/javascript">
	
	$(document).ready(function(){
		getNationList();
		getMemberDetail();
	});
	
	/** 국적 리스트 조회  */
	function getNationList(){
		$.ajax({			
			url		:"/common/getNationSelectList",
		  	data    : $("#memberForm").serialize(),
	      	dataType:"JSON",
	      	cache   : false,
	      	async   : true,
	      	type	:"POST",	
	      	success : function(data) {
	      		$("#nationlist").html($("#nationTemplate").render(data));				
	      	},	       
	      	error 	: function(xhr, status, error) {}	        
	   	});
	}		

	/** 계정 - 상세 조회  */
	function getMemberDetail(){
		var email = $("#email").val();

		if(email != ""){
			$.ajax({				
			  url	  : "/member/getDetail",
			  data    : $("#memberForm").serialize(),
		      dataType: "JSON",
		      cache   : false,
			  async   : true,
			  type	: "POST",	
		      success : function(obj) {
		       	getMemberDetailCallback(obj);
		      },	       
		      error 	: function(xhr, status, error) {}
		        
			});		
		}
	}
	
	/** 계정 - 상세 조회  콜백 함수 */
	function getMemberDetailCallback(obj){
		
		var str = "";
		
		if(obj != null){ 
			var name 			= obj.name;
			var companyEmail 	= obj.companyEmail;
			var nationcd 		= obj.nationcd;
			var company 		= obj.company;
			var job 			= obj.job;
 
			$("#name").val(name);
			$("#company").val(company);
			$("#companyEmail").val(companyEmail);
			$("#job").val(job);
			
			$("select[id=nationcd]").val(nationcd).prop("selected", true);

		} else {
			return;
		}		
	}
	
	/** 프로필 저장 */
	function setUpdate(){
		
		var name = $("#name").val();
		var nationcd = $("#nationcd").val();
		var company = $("#company").val();
		var job = $("#job").val();
		
		if(name == ""){
			alert("이름을 입력해주세요.");
			$("#name").focus();
			return;			
		}

		var yn = confirm("저장하시겠습니까?");		
		if(yn){
			
			$.ajax({
				type 	: 'post',
				url  	: '/member/setUpdate',
				data    : $("#memberForm").serialize(),
				dataType: "JSON",
				cache   : false,
				async   : true,
				type	: "POST",	
				success : function(obj) {
					setUpdateCallback(obj);				
				},	       
				error 	: function(xhr, status, error) {}
			});		
		}				
	}
	
	function setUpdateCallback(obj){
		
		if(obj != null){		
			
			var result = obj.result;
			
			if(result == "SUCCESS"){				
				alert("저장 성공하였습니다.");
			} else {				
				alert("저장 실패하였습니다.");	
				return;
			}
		}
	}		
	
</script>	

<script id="nationTemplate" type="text/x-jsrender">
									<span>국적</span>
									<div class="selectType">
										<label for="nationcd">한국</label>
										<select id="nationcd" name="nationcd" title="국적선택하기">
{{for data.list}}
											<option value="{{>category3}}">{{>name}}</option>
{{/for}}
										</select>
									</div>
</script>

<!-- .contents -->
		<div class="contents"> 
			<!--content-->
            <div class="content">
				<h1>프로필</h1>				
				<!-- . -->
				
				<div class="mypageBox">
					<div class="contentarea">
						<div class="basic_tit">							
<!--						<img src="resources/images/img_profile.png" alt="profile img">-->
							<form id="form" runat="server">								
								<div class="imginput">
									<img id="image_section" src="${pageContext.request.contextPath}/aiaas/kr/images/img_profile.png" alt="your image" />
<!-- 									<label for="imgInput" class="file_label">사진 수정</label> 
									<input type="file" id="imgInput" class="upload-hidden" >  -->
								</div>
<!--
								<label for="imgInput" class="file_label">파일선택</label>
								<input type="file" id="imgInput" class="upload-hidden" >  -->
							</form>
							<!-- <input type="text" placeholder="Mindslab" class="username"> -->
						</div>
						<div class="basic_info">
							<form id="memberForm" name="memberForm" method="post">
							<input type="hidden" id="email" name="email" value="${fn:escapeXml(sessionScope.accessUser.email)}" />
							<input type="hidden" id="category2" name="category2" value="kr" />
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />							
							<h5>기본 정보</h5>
							<ul>
								<li>
									<span>이름</span>
									<input type="text" id="name" name="name"  value="" maxlength="20" placeholder="홍길동" class="username">						
								</li>
								<li>
									<span>이메일</span>
									<em>${fn:escapeXml(sessionScope.accessUser.email)}</em>							
								</li>
								<!-- <li>
									<span>비밀번호</span>
									<button class="pw_edit">비밀번호 변경</button>
								</li>
								<li>
									<span>언어 선택</span>
									<div class="selectType">
										<label for="language">언어선택하기</label>
										<select id="language">
											<option value="langChoice">언어</option>		
											<option value="kor">한국어</option>		
											<option value="eng">영어</option>
										</select>
									</div>
								</li> -->
							</ul>
							<h5>선택 사항</h5>
							<ul>
								<li id="nationlist">
								</li>
								<li>
									<span>회사</span>
									<input type="text" id="company" name="company"  value="" maxlength="40" >
								</li>
								<li>																	
									<span>업무 이메일</span>
									<input type="text" id="companyEmail" name="companyEmail"  value="" maxlength="40" >
								</li>								
								<li>
									<span>직종</span>
									<input type="text" id="job" name="job"  value="" maxlength="40" >
								</li>
							</ul>
							</form>
						</div>
						<div class="btn">
							<a onclick="setUpdate();" class="btn_profile_save">저장</a>
						</div>
					</div>	
				</div>
				
			</div>	
			<!--content-->

		</div>
		<!-- //.contents -->
		