<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<script type="text/javascript">
	// 커밋 테스트용.
	//var userno= "${fn:escapeXml(sessionScope.accessUser.userno)}";

	$(document).ready(function(){
		getApiAccountList();
	});

	/** 이용내역 - 목록 조회  */
	function getApiAccountList(currentPageNo){

		if(currentPageNo === undefined){
			currentPageNo = "1";
		}

		$("#current_page_no").val(currentPageNo);

		$.ajax({
			url		:"/member/getApiAccountList",
			data    : $("#apiForm").serialize(),
			dataType:"JSON",
			cache   : false,
			async   : true,
			type	:"POST",
			success : function(obj) {
				// $("#apiAccountList").html($("#apiList").render(obj));

			},
			error 	: function(xhr, status, error) {}

		});
	}

	function getUsageList(service){
		$.ajax({
			url		:"/member/getApiUsageList",
			data    : {
				"service" : service,
				"${_csrf.parameterName}" : "${_csrf.token}"
			},
			dataType:"JSON",
			cache   : false,
			async   : true,
			type	:"POST",
			success : function(obj) {
				$("#apiAccountList").html($("#apiList").render(obj));
				// $("#pagination").html(obj.data.pagination);

			},
			error 	: function(xhr, status, error) {
				console.log("fail");
			},
			success: function(data){
				console.log("success");
				insertDetailApiList(data)
			}

		});
	}

	function insertDetailApiList(detailList){
		detailList.forEach(function (currentValue, index, array) {
			/*console.log(currentValue.usage)
			console.log(currentValue.date);*/
			var plusApi = document.createElement("li");
			plusApi.innerHTML = "<span>"+currentValue.usage+"</span><span>"+currentValue.date.split(" ")[0]+"</span>"
			document.getElementById("useDetail").appendChild(plusApi);

		});
	}

	function deleteApiList(){
		var apiList = document.getElementById("useDetail");
		while(apiList.hasChildNodes())
		{
			apiList.removeChild(apiList.firstChild);
		}
	}

	function getUseLog(serviceName){
		$.ajax({
			url		:"/member/getUseLog",
			data    : {
				// "userno" : userno,
				"serviceName" : serviceName,
				"${_csrf.parameterName}" : "${_csrf.token}"
			},
			dataType:"JSON",
			type	:"POST",
			success : function(obj) {
				$("#useDetail").html($("#listTemplate").render(obj));
				// $("#pagination").html(obj.data.pagination);
			},
			error 	: function(xhr, status, error) {}

		});
	}

	/** id,key 발급 요청 */
	function sendMailApi(){
		var regEmail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;

		var name = $("#fm_name").val();
		var mailAddr = $("#fm_email").val();
		var company = $("#fm_company").val();
		var content = $("#fm_txt").val();
		var userEmail = "${fn:escapeXml(sessionScope.accessUser.email)}"

		if(content == ""){
			alert("서비스내용을 입력해주세요.");
			$("#fm_txt").focus();
			return;
		}
		if(name == ""){
			alert("사용 API를 입력해주세요.");
			$("#fm_name").focus();
			return;
		}
		if(company == ""){
			alert("사용 회사를 입력해주세요.");
			$("#fm_company").focus();
			return;
		}
		/*if(mailAddr == ""){
			alert("이메일주소를 입력해주세요.");
			$("#fm_email").focus();
			return;
		} else {
			if(!regEmail.test(mailAddr)) {
				alert('이메일 주소가 유효하지 않습니다');
				$("#fm_email").focus();
				return;
			}
		}*/

		var yn = confirm("ID,Key 발급 요청 하겠습니까?");
		if(yn){
			$.ajax({
				type :'post',
				url : '/support/createApiKeyId',
				data:{
					'${_csrf.parameterName}' : '${_csrf.token}'
				},
				cache	: false,
				async	: true,
				type	: "POST",
				success	: function (obj) {
					$.ajax({
						type 	: 'post',
						url  	: '/support/insertSupportIdKey',
						data: {
							'name' : $("#fm_name").val(),
							'company' : $("#fm_company").val(),
							'mailAddr' : $("#fm_email").val(),
							'content' : $("#fm_txt").val(),
							'userEmail' : userEmail,
							'${_csrf.parameterName}' : '${_csrf.token}'
						},
						dataType: "JSON",
						cache   : false,
						async   : true,
						type	: "POST",
						success : function(obj) {},
						error 	: function(xhr, status, error) {}
					});

					alert("apiKey와 Id가 성공적으로 발급 됐습니다.")
					window.location.reload()
				},
				error	: function(xhr, status, error){
					alert("ID,Key 발급 요청실패하였습니다.");
					window.location.reload()
				}
			});
		}
	}

	function sendMailApiCallback(obj){

		if(obj != null){

			var state = obj.state;

			if(state == "SUCCESS"){
				//alert("ID,Key 발급 요청하였습니다.");
				$('.lyr_idKey').fadeOut(300);
				$('.idKey_done').fadeIn(300);
			} else {
				alert("ID,Key 발급 요청실패하였습니다.");
				return;
			}
		}
	}

</script>

<script id="listTemplate" type="text/x-jsrender">
	{{for data.list}}
		<li><span>{{>kii}}</span><span>{{>date}}</span></li>
	{{/for}}
</script>

<script type="text/javascript">
	jQuery.event.add(window,"load",function(){

		function detailView(){
			$('.detail_view').on('click', function(){
				var serviceName = $(this).data("service");
				getUsageList(serviceName);
				console.log(serviceName);
				//getUseLog(serviceName);
				$('.lyr_detail_amount').fadeIn(300);
			});

			$('.idKey_btn').on('click', function(){
				$('.lyr_idKey').fadeIn(300);
			})
			$('.btn_lyrWrap_close').on('click', function(){
				$('.lyr_idKey').fadeOut(300);
				deleteApiList();
			})
			$('.btn_lyrWrap_close, .lyr_detail_bg, #completBtn').on('click', function () {
				$('.lyr_detail_amount').fadeOut(300);
				$('.lyr_idKey').fadeOut(300);
				$('.idKey_done').fadeOut(300);
			});
		};

		setTimeout(detailView, 500);
	});

</script>

<!-- .lyr_idKey -->
<div class="lyr_idKey">
	<div class="lyr_plan_bg"></div>
	<!-- .productWrap -->
	<div class="lyrWrap">
		<button class="btn_lyrWrap_close" type="button">제품 리스트 닫기</button>
		<!-- .lyr_bd -->
		<div class="lyr_bd">
			<form id="apimailForm" name="apimailForm">
				<em class="far fa-edit"></em>
				<p>ID, Key 신청</p>
				<span class="desc">*입력하신 정보 검토 후 API ID 및 Key를 발급해 드립니다.</span>
				<ul>
					<li>
						<span>서비스 내용</span>
						<input type="text" name="fm_txt" id="fm_txt" placeholder="예: 챗봇 만들기, 뉴스 분석">
					</li>
					<li>
						<span>사용 API</span>
						<input type="text" name="fm_name" id="fm_name" placeholder="예: STT, TTS, MRC">
					</li>
					<li>
						<span>사용 회사</span>
						<input type="text" name="fm_company" id="fm_company" placeholder="예: 마인즈랩, 마음커넥트">
					</li>
					<li style="visibility: hidden;">
						<span class="input_email">ID, Key <br>받을 email</span>
						<input type="text" name="fm_email" id="fm_email" value="${fn:escapeXml(sessionScope.accessUser.companyEmail)}" placeholder="예: myemail@mindslab.ai">
					</li>
				</ul>
				<!-- <button class="btn_blue" onclick="sendMailApi();">신청하기</button> -->

				<a class="btn_blue" onclick="sendMailApi();">신청하기</a>

			</form>
		</div>
		<!-- //.lyr_bd -->

	</div>
	<!-- //.productWrap -->
</div>
<!-- //.lyr_idKey -->

<!-- .idKey_done -->
<div class="lyr_plan idKey_done">
	<div class="lyr_plan_bg"></div>
	<!-- .lyrWrap -->
	<div class="lyrWrap">
		<button class="btn_lyrWrap_close" type="button">제품 리스트 닫기</button>
		<!-- .lyr_bd -->
		<div class="lyr_bd">
			<em class="fas fa-check"></em>
			<p>신청완료!</p>
			<span>근무일 기준 1~2일 안에 신청하신 email로 <br>ID, Key를 보내드립니다. </span>
			<button class="btn_blue" id="completBtn">창닫기</button>
		</div>
		<!-- //.lyr_bd -->
	</div>
	<!-- //.lyrWrap -->
</div>
<!-- //.idKey_done -->

<!-- .lyr_detail_amount -->

<div class="lyr_pw lyr_detail_amount">
	<div class="lyr_detail_bg"></div>
	<!-- .productWrap -->
	<div class="lyrWrap">
		<button class="btn_lyrWrap_close" type="button">제품 리스트 닫기</button>

		<!-- .lyr_bd -->
		<div class="lyr_bd">
			<em class="fas fa-poll"></em>
			<h3>사용량 상세보기</h3>
			<%--<table>
				<thead>
					<p>사용량 </p>
					<p>사용 날짜</p>
					<ul class="amountlst" id="useDetail">
				<li><span>23분 30초</span><span>2019.04.28</span></li>
					</ul>
				</thead>
				<tbody id="apiDetail">
				</tbody>
			</table>--%>
			<p>사용량 </p>
			<p>사용 날짜</p>
			<ul class="amountlst" id="useDetail">
				<%--<li><span>23분 30초</span><span>2019.04.28</span></li>
				<li><span>23분 30초</span><span>2019.04.28</span></li>--%>
			</ul>

		</div>
		<!-- //.lyr_bd -->

	</div>
	<!-- //.productWrap -->
</div>
<!-- //.lyr_detail_amount -->

<!-- .contents -->
<div class="contents">
	<div class="content">
		<h1>이용내역</h1>
		<!--.demobox_nlu-->
		<div class="demobox">
			<div class="account">
				<p>API for Developers</p>
				<div class="stn_1">
					<dl>
						<dt>ID</dt>
						<%--<dd></dd> --%>
						<dd>${apiId} </dd>
					</dl>
					<dl>
						<dt>Key</dt>
						<%--<dd></dd>--%>
						<dd>${apikey} </dd>
					</dl>
					<!--
                                                <button class="requestbtn">Request</button>
                                                <span>Business 이상 구독 시, ID/Key 발급이 가능합니다.<br>Business Upgrade 원하시는 경우, 눌러주세요.</span>
                    -->
					<c:if test="${apikey == '' || apikey == null}">
						<button type="button" class="idKey_btn">API ID/Key 신청하기</button>
						<p>* maum.ai API ID, Key는 개발자를 위한 서비스입니다. </p>
					</c:if>

				</div>

				<form id="apiForm" name="apiForm">
					<input type="hidden" id="function_name" name="function_name" value="getApiAccountList" />
					<input type="hidden" id="current_page_no" name="current_page_no" value="1" />
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
					<div class="stn_2">
						<p>API 사용 내역</p>
						<table>
							<colgroup>
								<col width="10%"><col width="240px"><col width="20%"><col width="20%"><col width="20%">
							</colgroup>
							<thead>
							<tr class="thead">
								<th scope="col">No.</th>
								<th scope="col">서비스</th>
								<th scope="col">사용한 양 / 제공량</th>
								<th scope="col">사용기간</th>
								<th scope="col">기타사항</th>
							</tr>
							</thead>
							<tbody id="apiAccountList">
<%--							<c:set var="no" value="0"></c:set>--%>
							<%--					<c:forEach var="result" items="${usageList}" varStatus="status">
                                                    <c:set var="no" value="${no+1}"></c:set>
                                                    <tr>
                                                        <td scope="row">${no}</td>
                                                        <td>${result.service}</td>
                                                        <td>${result.usage}<span title="상세보기" class="detail_view" data-service="${result.service}"><em class="fas fa-search"></em></span></td>
                                                        <td>${result.paymentDate}</td>
                                                        <td>${result.percentage}%</td>
                                                    </tr>
                                                </c:forEach>--%>
							<%--<script id="apiList" type="text/x-jsrender">
<%--                                {{for data.apiList}}--%>
<%--                                    <tr>--%>
<%--                                        <td scope="row">{{>no}}</td>--%>
<%--                                        <td>{{>service}}</td>--%>
<%--                                        <td>{{>alimit}}<span title="상세보기" class="detail_view" data-service="{{>service_Id}}"><em class="fas fa-search"></em></span></td>--%>
<%--                                        <td>{{>date}}</td>--%>
<%--                                        <td>{{>etc}}</td>--%>
<%--                                    </tr>--%>

<%--                                {{/for}}--%>
<%--						</script>--%>

                                    <tr>
                                        <td scope="row">11</td>
                                        <td>22</td>
                                        <td>33<span title="상세보기" class="detail_view" data-service="44"><em class="fas fa-search"></em></span></td>
                                        <td>55</td>
                                        <td>66</td>
                                    </tr>


							</tbody>
						</table>
						<!-- 페이징 -->
						<%--							<div class="pageing" id="pagination">--%>
					</div>
					<!-- //페이징 -->
				</form>
			</div>
		</div>
		<!--//account-->
	</div>
</div>
<!-- //.contents -->






