﻿<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%

    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/idr/croppie.css">
<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">

<!-- 1 .pop_common -->
<div class="pop_common sample_pop">
    <!-- .pop_bg 팝업 바탕 어두운 색 -->
    <div class="pop_bg"></div>
    <!-- //.pop_bg 팝업 바탕 어두운 색 -->

    <!-- .popWrap -->
    <div class="popWrap">
        <!-- 4 .pop_2btn 버튼 두개 팝업 -->
        <div class="pop_2btn">
            <div class="pop_bg2"></div>
            <!-- .popWrap -->
            <div class="popWrap">
                <em class="fas fa-times minipop_close"></em>
                <!-- .pop_bd -->
                <div class="pop_bd">
                    <em class="fas fa-money-bill"></em>
                    <p>화폐 선택</p>
                    <span>선택하신 <strong>샘플 파일</strong>로 테스트를<br>진행하시겠습니까?
			            </span>
                </div>
                <!-- //.pop_bd -->
                <div class="btn">
                    <button class="btn_gray">취소</button>
                    <button class="btn_blue" id="">확인</button>
                </div>
            </div>
            <!-- //.popWrap -->
        </div>
        <!-- //.pop_2btn -->

        <!-- .pop_hd -->
        <div class="pop_hd">
            <h3><em class="far fa-file-image"></em>&nbsp; 화폐 샘플 파일</h3>
            <em class="fas fa-times pop_sample_close"></em>
        </div>
        <!-- //.pop_hd -->
        <!-- .pop_bd -->
        <div class="pop_bd">

            <div class="country">
                <p class="p_kr"><img src="${pageContext.request.contextPath}/aiaas/common/images/img_kr.svg"  alt="한국 국기"/> 한국 (KRW)</p>
                <div class="option_box">
                    <div class="radio">
                        <input type="radio" id="sample_kr1" name="option" value="sample_1" checked>
                        <label for="sample_kr1" class="">
                            <em class="img_area">
                                <img id="sample_1" src="${pageContext.request.contextPath}/aiaas/common/images/img_ko1.png" alt="천원" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_kr2" name="option" value="sample_2" >
                        <label for="sample_kr2" class="">
                            <em class="img_area">
                                <img id="sample_2" src="${pageContext.request.contextPath}/aiaas/common/images/img_ko2.png" alt="오천원" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_kr3" name="option" value="sample_3" >
                        <label for="sample_kr3" class="">
                            <em class="img_area">
                                <img id="sample_3" src="${pageContext.request.contextPath}/aiaas/common/images/img_ko3.png" alt="만원" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_kr4" name="option" value="sample_4" >
                        <label for="sample_kr4" class="">
                            <em class="img_area">
                                <img id="sample_4" src="${pageContext.request.contextPath}/aiaas/common/images/img_ko4.png" alt="오만원" />
                            </em>
                        </label>
                    </div>
                </div>
            </div>
            <div class="country">
                <p><img src="${pageContext.request.contextPath}/aiaas/common/images/img_usd.svg"  alt="미국 국기"/>  미국 (USD)</p>
                <div class="option_box">
                    <div class="radio">
                        <input type="radio" id="sample_us5" name="option" value="sample_5" checked>
                        <label for="sample_us5" class="">
                            <em class="img_area">
                                <img id="sample_5" src="${pageContext.request.contextPath}/aiaas/common/images/img_usd1.png" alt="1달러" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_us6" name="option" value="sample_6" >
                        <label for="sample_us6" class="">
                            <em class="img_area">
                                <img id="sample_6" src="${pageContext.request.contextPath}/aiaas/common/images/img_usd2.png" alt="2달러" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_us7" name="option" value="sample_7" >
                        <label for="sample_us7" class="">
                            <em class="img_area">
                                <img id="sample_7" src="${pageContext.request.contextPath}/aiaas/common/images/img_usd3.png" alt="5달러" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_us8" name="option" value="sample_8" >
                        <label for="sample_us8" class="">
                            <em class="img_area">
                                <img id="sample_8" src="${pageContext.request.contextPath}/aiaas/common/images/img_usd4.png" alt="20달러" />
                            </em>
                        </label>
                    </div>
                </div>
            </div>
            <div class="country">
                <p><img src="${pageContext.request.contextPath}/aiaas/common/images/img_eu.svg"  alt="유럽 국기"/>  유럽 (EUR)</p>
                <div class="option_box">
                    <div class="radio">
                        <input type="radio" id="sample_eu9" name="option" value="sample_9" checked>
                        <label for="sample_eu9" class="">
                            <em class="img_area">
                                <img id="sample_9" src="${pageContext.request.contextPath}/aiaas/common/images/img_eur1.png" alt="20유로" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_eu10" name="option" value="sample_10" >
                        <label for="sample_eu10" class="">
                            <em class="img_area">
                                <img id="sample_10" src="${pageContext.request.contextPath}/aiaas/common/images/img_eur2.png" alt="100유로" />
                            </em>
                        </label>
                    </div>
                </div>
            </div>
            <div class="country">
                <p><img src="${pageContext.request.contextPath}/aiaas/common/images/img_cn.svg"  alt="중국 국기"/>  중국 (CNY)</p>
                <div class="option_box">
                    <div class="radio">
                        <input type="radio" id="sample_cn11" name="option" value="sample_11" checked>
                        <label for="sample_cn11" class="">
                            <em class="img_area">
                                <img id="sample_11" src="${pageContext.request.contextPath}/aiaas/common/images/img_cny1.png" alt="10위안" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_cn12" name="option" value="sample_12" >
                        <label for="sample_cn12" class="">
                            <em class="img_area">
                                <img id="sample_12" src="${pageContext.request.contextPath}/aiaas/common/images/img_cny2.png" alt="20위안" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_cn13" name="option" value="sample_13" >
                        <label for="sample_cn13" class="">
                            <em class="img_area">
                                <img id="sample_13" src="${pageContext.request.contextPath}/aiaas/common/images/img_cny3.png" alt="50위안" />
                            </em>
                        </label>
                    </div>

                </div>
            </div>
            <div class="country">
                <p><img src="${pageContext.request.contextPath}/aiaas/common/images/img_jp.svg" alt="일본 국기"/>  일본 (JPY)</p>
                <div class="option_box">
                    <div class="radio">
                        <input type="radio" id="sample_jp14" name="option" value="sample_14" checked>
                        <label for="sample_jp14" class="">
                            <em class="img_area">
                                <img id="sample_14" src="${pageContext.request.contextPath}/aiaas/common/images/img_jp1.png" alt="5000엔" />
                            </em>
                        </label>
                    </div>
                    <div class="radio">
                        <input type="radio" id="sample_jp15" name="option" value="sample_15" >
                        <label for="sample_jp15" class="">
                            <em class="img_area">
                                <img id="sample_15" src="${pageContext.request.contextPath}/aiaas/common/images/img_jp2.png" alt="10000엔" />
                            </em>
                        </label>
                    </div>


                </div>
            </div>
        </div>
        <!-- //.pop_bd -->
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_common -->

<!-- 1 .pop_common -->
<div class="pop_common samplebill_pop" id="samplebill_pop">
    <!-- .pop_bg 팝업 바탕 어두운 색 -->
    <div class="pop_bg"></div>
    <!-- //.pop_bg 팝업 바탕 어두운 색 -->

    <!-- .popWrap -->
    <div class="popWrap">
        <!-- .pop_hd -->
        <div class="pop_hd">
            <h3><em class="far fa-file-image"></em>&nbsp; 고지서 샘플</h3>
            <em class="fas fa-times pop_sample_close"></em>
        </div>
        <!-- //.pop_hd -->
        <!-- .pop_bd -->
        <div class="pop_bd">
            <img id="samplebill" class="bill_img" src="${pageContext.request.contextPath}/aiaas/common/images/sample_bill.png" alt="고지서 샘플 이미지" />
        </div>
        <!-- //.pop_bd -->
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_common -->

<!-- 2 .pop_common -->
<div class="pop_common samplebill_pop" id="hospital_pop">
    <!-- .pop_bg 팝업 바탕 어두운 색 -->
    <div class="pop_bg"></div>
    <!-- //.pop_bg 팝업 바탕 어두운 색 -->

    <!-- .popWrap -->
    <div class="popWrap">
        <!-- .pop_hd -->
        <div class="pop_hd">
            <h3 id="hospital_origin"><em class="far fa-file-image"></em>&nbsp; 진료비 영수증 샘플</h3>
            <em class="fas fa-times pop_sample_close"></em>
        </div>
        <!-- //.pop_hd -->
        <!-- .pop_bd -->
        <div class="pop_bd">
            <img id="samplem_edical" class="bill_img" src="${pageContext.request.contextPath}/aiaas/common/images/hospital-sample.jpg" alt="진료비 영수증 샘플" />
        </div>
        <!-- //.pop_bd -->
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_common -->

<!-- 3 .pop_common -->
<div class="pop_common upload_popup">
    <!-- .pop_bg 팝업 바탕 어두운 색 -->
    <div class="pop_bg"></div>
    <!-- //.pop_bg 팝업 바탕 어두운 색 -->
    <!-- .popWrap -->
    <div class="popWrap">
        <!-- .pop_hd -->
        <div class="pop_hd">
            <h3><em class="far fa-file-image"></em> 이미지 업로드</h3>
            <em class="fas fa-times pop_sample_close"></em>
        </div>
        <!-- //.pop_hd -->
        <!-- .pop_bd -->
        <div class="pop_bd">
            <p>이미지 종류를 선택하고 업로드를 해주세요.</p>
            <div class="option_box">
                <div class="radio">
                    <input type="radio" id="opt1" name="pop_option" value="화폐" checked>
                    <label for="opt1" class="">
                        <span>화폐</span>
                    </label>
                </div>
                <div class="radio">
                    <input type="radio" id="opt2" name="pop_option" value="고지서">
                    <label for="opt2" class="">
                        <span>고지서</span>
                    </label>
                </div>
                <div class="radio">
                    <input type="radio" id="opt3" name="pop_option" value="진료비영수증">
                    <label for="opt3" class="">
                        <span>진료비 영수증</span>
                    </label>
                </div>
                <div class="btn" id="uploadFile">
                    <em class="fas fa-times hidden close"></em>
                    <em class="far fa-file-image hidden"></em>
                    <label for="demoFile" class="demolabel">파일 선택</label>
                    <input type="file" id="demoFile" class="demoFile" accept=".jpg, .png" >
                </div>
            </div>
            <div class="btn_area">
                <button type="button" class="btn_start editImgBtn" id="editImgBtn2">이미지 편집</button>
            </div>
        </div>
        <!-- //.pop_bd -->
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_common -->

<!-- .contents -->
<div class="contents">
    <div class="content api_content">
        <h1 class="api_tit">문서 이미지 분석</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'recogdemo')" id="defaultOpen"><button type="button">엔진</button></li>
            <li class="tablinks" onclick="openTap(event, 'recogexample')"><button type="button">적용사례</button></li>
            <li class="tablinks" onclick="openTap(event, 'recogmenu')"><button type="button">매뉴얼</button></li>
<%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <div class="demobox" id="recogdemo">
            <p>딥러닝을 통한 문서 이미지 분석 및 인식 플랫폼 <span>DIARL</span><small>(Document Image Analysis, Recognition and Learning)</small></p>
            <span class="sub">각 나라 별 화폐, 고지서 또는 병원 진료비 계산서·영수증의 이미지를 활용하여 정보를 분석해줍니다.</span>
            <!--이미지인식box-->
            <div class="demo_layout pattern_box">
                <div class="pattern_1" >
                    <div class="fl_box" id="opacity">
                        <p><em class="far fa-file-image"></em><strong>샘플 파일</strong>로 테스트 하기</p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="sample" value="화폐" checked>
                                    <label for="sample1" class="">
                                        <em class="img_area">
                                            <img id="sampleImg_1" src="${pageContext.request.contextPath}/aiaas/kr/images/img_diarl_sample1.jpg" alt="화폐 샘플 이미지" />
                                        </em>
                                        <span>화폐</span>
                                    </label>
                                    <button class="sample_more"><em class="fas fa-plus-circle"></em>&nbsp;샘플 더보기</button>
                                </div>

                            </div>
                            <div class="sample_2">
                                <div class="radio">
                                    <input type="radio" id="sample2" name="sample" value="고지서" >
                                    <label for="sample2"  class="">
                                        <em class="img_area bill_area">
                                            <img id="sampleImg_2" class="bill_img" src="${pageContext.request.contextPath}/aiaas/common/images/img_diarl_sample2.png" alt="고지서 샘플 이미지" />
                                            <strong class="bill_detail"><em class="fas fa-search"></em> 자세히 보기</strong>
                                        </em>
                                        <span>고지서</span>
                                    </label>
                                    <span class="bill_desc">이미지 클릭시, 자세히 볼 수 있습니다.</span>
                                </div>
                            </div>
                            <div class="sample_3">
                                <div class="radio">
                                    <input type="radio" id="sample3" name="sample" value="진료비영수증" >
                                    <label for="sample3"  class="">
                                        <em class="img_area bill_area">
                                            <img id="sampleImg_3" class="bill_img" src="${pageContext.request.contextPath}/aiaas/common/images/hospital-sample.jpg" alt="진료비 영수증 샘플 이미지" />
                                            <strong class="hospital_detail"><em class="fas fa-search"></em> 자세히 보기</strong>
                                        </em>
                                        <span>병원<br>진료비<br>계산서·<br>영수증
                                        </span>
                                    </label>
                                    <span class="bill_desc">이미지 클릭시, 자세히 볼 수 있습니다.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-image"></em><strong>내 파일</strong>로 해보기</p>
                        <div class="uplode_box">
                            <button type="button" class="upload_pop">이미지 업로드</button>
                            <%--                            <div class="btn" id="uploadFile">--%>
                            <%--                                <em class="fas fa-times hidden close"></em>--%>
                            <%--                                <em class="far fa-file-image hidden"></em>--%>
                            <%--                                <label for="demoFile" class="demolabel">이미지 업로드</label>--%>
                            <%--                                <input type="file" id="demoFile" class="demoFile" accept=".jpg, .png" >--%>
                            <%--                            </div>--%>
                            <p class="img_desc"><em class="fas fa-exclamation"></em> 이미지 업로드 시 유의사항</p>
                            <div>
                                <span><em>화폐</em> : 5개국 (한국, 중국, 일본, 미국, 유로)화폐<br>직접 찍은 사진 권고 </span>
                                <span><em>고지서</em> : 일반지로요금(OCR), KT 통신요금,전기요금,<br>4대보험요금, 각종 서울특별시 지방세 고지서에 한함 </span>
                                <span><em>병원 진료비 계산서 ·영수증 </em> : 상급 종합병원에서 보편적으로 쓰이는 영수증 형식 권고</span>
                            </div>

                            <ul>
                                <li>* 지원가능 파일 확장자: .jpg, .png</li>
                                <li>* 이미지 용량 2MB 이하만 가능합니다.</li>
                            </ul>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_start editImgBtn" id="editImgBtn">분석하기</button>
                    </div>
                </div>
                <!--pattern_2-->
                <div class="pattern_2" >
                    <p><em class="far fa-file-image"></em>이미지 편집</p>
                    <p class="desc">이미지를 노란선 좌측, 상단에 맞춰 주세요.</p>
                    <div class="img_box">
                        <em class="fas fa-minus minus"></em>
                        <em class="fas fa-plus plus"></em>
                        <!--불러온 이미지 들어갈 곳-->
                        <img src="" alt="불러온 이미지" id="previewImg">
                        <div id="rotate_left" class="redobox">
                            <em class="fas fa-undo-alt"></em>
                            <span>회전</span>
                        </div>
                    </div>
                    <p class="desctxt">확대, 축소, 회전 버튼과 마우스 움직임을 통해 이미지를 편집할 수 있습니다.</p>
                    <span>예시) 아래 이미지와 동일한 위치에 맞춰주세요.</span>
                    <div class="bill_sample">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_cropsample_big.png" alt="고지서 위치 샘플 이미지" />
                    </div>
                    <div class="hospital_sample">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_cropsample_medical.png" alt="진료비 영수증 위치 샘플 이미지" />
                    </div>
                    <div class="money_sample">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_cropsample_money.png" alt="화폐 위치 샘플 이미지" />
                    </div>
                    <div class="btn">
                        <a id="editCancel">취소</a>
                        <a class="active" id="recogButton">결과보기</a>
                        <img id="loading_img" class="loading_img" src="${pageContext.request.contextPath}/aiaas/kr/images/loading.gif" alt="loading">
                    </div>
                </div>
                <!--pattern_2-->
                <!--loding_area-->
                <div class="loding_area">
                    <p><em class="far fa-file-image"></em>이미지 인식 및 분석 처리중</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#f7778a" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

                        <p>잠시만(1분 내외) 기다려 주세요.</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1" id="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
                    </div>
                </div>
                <!--loding_area-->
                <!--pattern_3-->
                <div class="pattern_3">
                    <div class="img_box">
                        <p><em class="far fa-file-image"></em>이미지 인식 및 분석 처리 결과</p>
                        <!--결과 이미지 들어갈 곳-->
                        <img id="resultImg" src="" alt="결과 이미지" >
                    </div>
                    <div class="resultBox" >
                        <div class="txtBox" id="note_table">
                            <dl class="dltype02">
                                <dt>분류</dt>
                                <dd>화폐</dd>
                            </dl>
                            <dl class="dltype02">
                                <dt>통화명</dt>
                                <dd id="currency"></dd>
                            </dl>
                            <dl class="dltype02">
                                <dt>국가명</dt>
                                <dd id="country"></dd>
                            </dl>
                            <dl class="dltype02">
                                <dt>금액</dt>
                                <dd id="amount"></dd>
                            </dl>
                        </div>
                        <div class="txtBox" id="bill_table" style="display: none;">
                            <dl class="dltype02">
                                <dt>분류</dt>
                                <dd>고지서</dd>
                            </dl>
                            <dl class="dltype02">
                                <dt>고지서 종류</dt>
                                <dd id="bill_type"></dd>
                            </dl>
                            <dl class="dltype02">
                                <dt>전자납부번호</dt>
                                <dd id="bill_code"></dd>
                            </dl>
                        </div>
                        <div class="btn_area">
                            <button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
                        </div>
                    </div>
                    <div class="result_hospital">
                        <p><em class="far fa-file-excel"></em>결과 파일(.csv)</p>
                        <div class="table_hospital">
                            <table class="tbl_view">
                                <colgroup id="col_group">
                                </colgroup>
                                <thead>
                                <%--                                    <tr>--%>
                                <%--                                        <th rowspan="2" colspan="2">항목</th>--%>
                                <%--                                        <th colspan="4">급여</th>--%>
                                <%--                                        <th colspan="2">비급여</th>--%>
                                <%--                                    </tr>--%>
                                <%--                                    <tr>--%>

                                <%--                                        <th colspan="3">일반 본인부담</th>--%>
                                <%--                                        <th rowspan="2">전액 본인부담</th>--%>
                                <%--                                        <th rowspan="2">선택 진료료</th>--%>
                                <%--                                        <th rowspan="2">선택 진료료<br>이외</th>--%>
                                <%--                                    </tr>--%>
                                <tr id="item_category">
                                    <th>항목코드</th>
                                    <th>항목명</th>
                                    <th colspan=""> 항목별 금액</th>
                                    <%--                                        <th>공단부담금</th>--%>
                                    <%--                                        <th>소계</th>--%>
                                </tr>
                                </thead>
                                <tbody id="item_result">
                                <%-- 결과 데이터 불러오는 곳--%>
                                <!--
                                    <tr class="">
                                        <td class="bold_txt">39500</td>
                                        <td class="align_left">한방물리요법료</td>
                                        <td class="align_right">2,455</td>
                                        <td class="align_right">10,455</td>
                                        <td class="align_right" id="sub_total">130,455</td>
                                        <td class="align_right"></td>
                                        <td class="align_right">10,455</td>
                                        <td class="align_right">130,455</td>
                                        <td class="align_right">130,455</td>
                                    </tr>

                                    <tr class="sum">
                                        <td colspan="2" class="bold_txt">합계</td>
                                        <td class="align_right">2,455</td>
                                        <td class="align_right">10,455</td>
                                        <td class="align_right"></td>
                                        <td class="align_right">2,455</td>
                                        <td class="align_right">2,455</td>
                                        <td class="align_right">130,455</td>
                                    </tr>
                                -->
                                </tbody>
                            </table>

                        </div>
                        <a class="img_pop" href="#" alt="원본 이미지"><em class="far fa-file-image"></em> 원본 이미지 확인</a>
                        <a id="save" onclick="downloadResultImg();" class="dwn_link" ><em class="far fa-arrow-alt-circle-down"></em> 결과 파일(.csv) 다운로드</a>
                        <div class="btn_area">
                            <button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
                        </div>

                    </div>


                    <!--						인식하지 못했을 때 style="display: block;" 해두세용-->
                    <div class="resultBox_fail" style="display: none;">
                        <div class="no_recog">인식할 수 없는 이미지 입니다.</div>
                        <div class="btn_area">
                            <button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
                        </div>
                    </div>
                </div>
                <!--pattern_3-->
            </div>

        </div>
        <!-- .demobox -->

        <!--.recogmenu-->
        <div class="demobox vision_menu" id="recogmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        DIAR <small>(Document Image Analysis, Recognition and Learning)</small>
                    </div>
                    <p class="sub_txt">이미지 내 텍스트를 인식합니다. 현재 API는 병원 영수증 인식만 제공합니다.</p>

                    <span class="sub_title">
								준비사항
					</span>
                    <p class="sub_txt">- Input: 병원 영수증 이미지 파일 </p>
                    <ul>
                        <li>확장자: .jpg, .png.</li>
                        <li>용량: 2MB 이하 </li>
                    </ul>
                    <span class="sub_title">
								 실행 가이드
							</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/ocr/hospitalReceipt</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>receipt_img</td>
                            <td>type:file (.jpg,.png)  </td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                        url --location --request POST 'http://api.maum.ai/ocr/hospitalReceipt' \<br>
                        --header 'Content-Type: application/x-www-form-urlencoded' \<br>
                        --form 'apiId= 발급받은 API ID' \<br>
                        --form 'apiKey= 발급받은 API KEY' \<br>
                        --form 'receipt_img= 영수증 이미지 파일'<br>
                    </div>

                    <p class="sub_txt">④ Response 예제 </p>

                    <div class="code_box">
<pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "result_img": {
        "name": "ori_4_rst.jpg",
        "data": "MzAzMjAs7JW97ZKI67mELCwsLC...LOy5mOujjOyerOujjOuMgCwsLCwsLA0K"
    },
    "result_csv": {
        "name": "ori_4_output.csv",
        "data": "MzAzMjAs7JW97ZKI67mELCwsLC...LOy5mOujjOyerOujjOuMgCwsLCwsLA0K"
    }
}
</pre>
                    </div>


                </div>
            </div>

        </div>
        <!--//recogmenu-->
        <!--.recogexample-->
        <div class="demobox" id="recogexample">
            <p><em style="color:#f7778a;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
            <!-- //화폐, 고지서 인식(DIARL) -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>화폐 인식</span>
                            </dt>
                            <dd class="txt">전세계 각국의 화폐를 자동으로 인식하고, 환율 조회까지 제공 가능한 부가서비스를 구현하는 데 활용할 수 있습니다.
                                <span><em class="fas fa-book-reader"></em> Reference: A은행 </span>
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_dir"><span>DIARL</span></li>
                                    <li class="ico_bot"><span>챗봇</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>고지서 인식</span>
                            </dt>
                            <dd class="txt">고지서, 영수증 등을 자동으로 인식하고 자동 납부까지 이뤄지는 서비스를 만드는 데 활용할 수 있습니다.
                                <span><em class="fas fa-book-reader"></em> Reference: A은행</span>
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_dir"><span>DIARL</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>병원영수증 인식</span>
                            </dt>
                            <dd class="txt">보험금 청구를 위해 첨부하는 병원 영수증을 자동으로 인식하여 금액들을 추출해주는 시스템입니다.
                                <span><em class="fas fa-book-reader"></em> Reference: A보험사</span>
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_dir"><span>DIARL</span></li>
                                    <li class="ico_dataAnaly"><span>데이터 분석</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //화폐, 고지서 인식(DIARL) -->
        </div>
        <!--//.recogexample-->

    </div>
</div>
<!-- //.contents -->

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/idr/croppie.js?ver=<%=fmt.format(lastModifiedStyle)%>"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/idr/recog.js?ver=20200203"></script>

<script>

    var sample1File;
    var sample2File;
    var sample3File;
    var sample1SrcUrl;
    var sample2SrcUrl;
    var sample3SrcUrl;


    jQuery.event.add(window,"load",function(){

        $(document).ready(function (){
            loadSample1();
            loadSample2();
            loadSample3();

            $('.sample_more').on('click', function () {
                $('.sample_pop').fadeIn(300);
            });

            $('.sample_pop .pop_bd .country .option_box .radio').on('click', function () {
                $('.pop_2btn').fadeIn(300);
            });

            $('.bill_detail').on('click', function () {
                $('#samplebill_pop').fadeIn(300);
            });
            $('.hospital_detail').on('click', function () {
                $('#hospital_pop').fadeIn(300);
            });

            $('.upload_pop').on('click', function () {
                $('.upload_popup').fadeIn(300);

            });

            // 공통 팝업창 닫기
            $('.pop_sample_close, .pop_bg').on('click', function () {
                $('.pop_common').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });

            $('.btn_gray, .minipop_close').on('click', function () {
                $('.pop_2btn').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });

            //샘플 화폐 선택하기 버튼
            $('.btn_blue').on('click', function () {
                var option = $("input[type=radio]:checked").val();
                var imgget = $('#'+option).attr('src');
                console.log(imgget);
                $('.pop_common').fadeOut(300);
                $('.pop_2btn').fadeOut(300);
                $('#sampleImg_1').trigger("click");
                $('#sampleImg_1').attr('src', imgget);
                loadSample1();
                $('body').css({
                    'overflow': '',
                });
            });

            // Remove uploaded image
            $('em.close').on('click', function () {
                $('#demoFile').val(null);
                $(this).parent().removeClass("btn_change");
                $(this).parent().addClass("btn");
                $(this).parent().children('.demolabel').text('파일 선택');
                $('.fl_box').css('opacity', '1');
                $('.upload_popup .btn_area').hide();
            });
            $('em.pop_sample_close').on('click', function () {
                console.log('팝업창 닫기')
                $('#demoFile').val(null);
                $(this).parent().removeClass("btn_change");
                $(this).parent().addClass("btn");
                $(this).parent().children('.demolabel').text('파일 선택');
                $('.fl_box').css('opacity', '1');
                $('.upload_popup .btn_area').hide();

            });

            // 파일 업로드 후 이벤트
            document.querySelector("#demoFile").addEventListener('change', function (ev) {

                if (this.files[0].type.match(/image.*/)){
                    $('input[type="radio"]:checked').prop("checked", true);

                    document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
                    var url = URL.createObjectURL(this.files[0]);

                    var element = document.getElementById( 'uploadFile' );
                    element.classList.remove( 'btn' );
                    element.classList.add( 'btn_change' );

                    $('.fl_box').css("opacity", "0.5");
                    var btn = $('.upload_popup .pop_bd .btn_area');
                    btn.fadeIn(300);

                }
                else{
                    this.value = null;
                    alert('이미지 파일을 업로드하세요.');
                }
            });

            // 샘플 선택 후 인식하기 누를 때
            $('#editImgBtn').on('click', function () {
                var $checked = $('input[name="sample"]:checked').val();
                if( $checked === "화폐"){
                    activateCroppie();
                    $('.desc').removeClass('desc_3');
                    $('.money_sample').fadeIn();
                    $('.bill_sample').hide();
                    $('.hospital_sample').hide();

                    $('em.close').trigger('click');
                    $('.upload_popup .btn_area').hide();
                    $('.upload_popup').hide();
                    $('.pattern_1').hide();
                    $('.pattern_2').fadeIn(300);
                }else if( $checked === "고지서"){
                    activateCroppie2();
                    $('.desc').removeClass('desc_3');
                    $('.bill_sample').fadeIn();
                    $('.money_sample').hide();
                    $('.hospital_sample').hide();

                    $('em.close').trigger('click');
                    $('.upload_popup .btn_area').hide();
                    $('.upload_popup').hide();
                    $('.pattern_1').hide();
                    $('.pattern_2').fadeIn(300);
                }else if($checked === "진료비영수증"){
                    console.log("진료비 영수증");

                    let file;

                    loadSample3();
                    file = sample3File;

                    $('.pattern_1').hide();
                    $('.loding_area').fadeIn();

                    sendReqHospitalReceipt(file);
                }
            });

            //팝업 옵션 선택 후 인식하기 누를 때
            $('#editImgBtn2').on('click', function () {
                let $demoFile = $("#demoFile");
                let url;
                let $checked = $('input[name="pop_option"]:checked').val();

                $('input[name="sample"]').prop('checked',false);

                //내 파일로
                if ( $demoFile.val() === "" || $demoFile.val() === null){
                    alert("샘플을 선택하거나 파일을 업로드해 주세요.");
                    return 0;
                }

                url = URL.createObjectURL($demoFile.get(0).files[0]);

                if( $checked === "화폐"){
                    console.log("화폐");
                    $('#previewImg').attr('src', url);
                    popCroppie(url);
                    $('.desc').removeClass('desc_3');
                    $('.money_sample').fadeIn();
                    $('.bill_sample').hide();
                    $('.hospital_sample').hide();
                    $('.pattern_2').fadeIn(300);

                    $('em.close').trigger('click');
                    $('.upload_popup .btn_area').hide();

                }else if( $checked === "고지서"){
                    console.log("고지서");
                    $('#previewImg').attr('src', url);
                    popCroppie();
                    $('.desc').removeClass('desc_3');
                    $('.bill_sample').fadeIn();
                    $('.money_sample').hide();
                    $('.hospital_sample').hide();
                    $('.pattern_2').fadeIn(300);

                    $('em.close').trigger('click');
                    $('.upload_popup .btn_area').hide();

                }else if( $checked === "진료비영수증"){
                    console.log("진료비영수증");
                    let file = $demoFile.get(0).files[0];
                    $('#samplem_edical').attr('src', url);
                    $('.loding_area').fadeIn();

                    sendReqHospitalReceipt(file);
                }
                $('.pattern_1').hide();
                $('.upload_popup').hide();
            });

            $('.img_pop').on('click', function () {
                $('#hospital_pop').fadeIn();
            });

        });


        function loadSample1() {
            let blob = null;
            let xhr = new XMLHttpRequest();
            let imgsrc = $('#sampleImg_1').attr('src');
            //console.log (imgsrc);
            xhr.open("GET", imgsrc);
            xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
            xhr.onload = function()
            {
                blob = xhr.response;//xhr.response is now a blob object
                if(!navigator.msSaveBlob){
                    sample1File = new File([blob], imgsrc);
                }else{
                    sample1File = new Blob([blob], imgsrc);
                }
                sample1SrcUrl = URL.createObjectURL(blob);
            };

            xhr.send();
        }

        function loadSample2() {
            let blob = null;
            let xhr = new XMLHttpRequest();
            xhr.open("GET", "/aiaas/common/images/img_diarl_sample2.png");
            xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
            xhr.onload = function()
            {
                blob = xhr.response;//xhr.response is now a blob object
                if(!navigator.msSaveBlob){
                    sample2SrcUrl = new File([blob], "img_diarl_sample2.png");
                }else{
                    sample2SrcUrl = new Blob([blob], "img_diarl_sample2.png");
                }
                sample2SrcUrl = URL.createObjectURL(blob);
            };

            xhr.send();
        }
        function loadSample3() {
            let blob = null;
            let xhr = new XMLHttpRequest();
            xhr.open("GET", "/aiaas/common/images/hospital-sample.jpg");
            xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
            xhr.onload = function()
            {
                blob = xhr.response;//xhr.response is now a blob object
                if(!navigator.msSaveBlob){
                    sample3File = new File([blob], "hospital-sample.jpg");
                }else{
                    sample3File = new Blob([blob], "hospital-sample.jpg");
                }
                sample3SrcUrl = URL.createObjectURL(blob);
            };

            xhr.send();
        }

    });

</script>