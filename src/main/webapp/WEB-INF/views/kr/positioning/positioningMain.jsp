<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-07-10
  Time: 14:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>파일 업로드 실패</h5>
            <p>파일 사이즈/용량이 초과되었거나, 확장자명 오류로<br>
                업로드 되지 않았습니다.</p>
            <span>* 지원가능 파일 확장자: .obj</span>
        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">확인</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents api_content">
    <!-- .content -->
    <div class="content">
        <h1 class="api_tit">얼굴 추적</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'bpdemo')" id="defaultOpen">
                <button type="button">엔진</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'bpexample')">
                <button type="button">적용사례</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'bpmenu')">
                <button type="button">매뉴얼</button>
            </li>
            <%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <!-- .demobox -->
        <div class="demobox" id="bpdemo">
            <p><span>치아 교정기 포지셔닝 </span><small>(Bracket Positioning) </small></p>
            <span class="sub">주어진 치아에 교정기를 부착할 위치를 선정합니다.</span>
            <!--faceTracking_box-->
            <div class="demo_layout faceTracking_box bp_box">
                <!--tr_1-->
                <div class="tr_1">
                    <div class="fl_box">
                        <p><em class="far fa-file-video"></em><strong>샘플 파일</strong>로 테스트 하기</p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="option" value="1" checked>
                                    <label for="sample1" class="">
                                       <img src="${pageContext.request.contextPath}/aiaas/common/images/img_bp_1.png" alt="치아 샘플 이미지" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-video"></em><strong>내 파일</strong>로 해보기</p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-image hidden"></em>
                                <label for="demoFile" class="demolabel">파일 업로드</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".obj">
                            </div>
                            <ul>
                                <li>* 지원가능 파일 확장자: .obj</li>
                                <li> * 치아 모델이 예시와 같이 세로축으로 정렬되어 있어야 합니다.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">위치 선정</button>
                    </div>
                </div>
                <!--tr_1-->
                <!--tr_2-->
                <div class="tr_2">
                    <p><em class="far fa-file-image"></em>치아 교정기 위치 선정중</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px"
                             viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42"
                                                                             d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/>
                            <g>
                                <path fill="#f7778a" fill-opacity="1"
                                      d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/>
                                <animatetransform attributeName="transform" type="translate"
                                                  values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0"
                                                  calcMode="discrete" dur="1820ms" repeatCount="indefinite"/>
                            </g></svg>

                        <p>약간의 시간이 소요 됩니다.</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>처음으로</button>
                    </div>

                </div>
                <!--tr_2-->
                <!--tr_3-->
                <div class="tr_3 tr_3_sample">

                    <div class="result_file">
                        <div class="fl">
                            <p><em class="far fa-file-image"></em>입력 파일</p>
                            <div class="inner_box">
                                <img src="${pageContext.request.contextPath}/aiaas/common/images/img_bp_1.png" alt="입력 파일" />
                            </div>
                        </div>
                        <div class="fr">
                            <p><em class="far fa-file-image"></em>결과 파일</p>
                            <div class="inner_box">
                                <div>
                                    <img src="${pageContext.request.contextPath}/aiaas/common/images/img_bp_2.png" alt="결과 파일 위치 영역" />
                                    <span>위치 영역</span>
                                    <a class="resultposition" href="" ><em class="far fa-arrow-alt-circle-down"></em>결과 파일 다운로드</a>
                                </div>
                                <div>
                                    <img src="${pageContext.request.contextPath}/aiaas/common/images/img_bp_3.png" alt="결과 파일 브라켓" />
                                    <span>브라켓</span>
                                    <a id="resultbracket" class="resultbracket" href="" ><em class="far fa-arrow-alt-circle-down"></em>결과 파일 다운로드</a>
                                </div>
                                <p>결과 파일은 .obj파일로 지원합니다.</p>
                            </div>
                        </div>
                        <div class="unfloat">
                            <span>Meshmixer로 3D 이미지를 mix한 이미지</span>
                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_bp_4.png" alt="Meshmixer로 3D 이미지를 mix한 이미지" />
                            <p>치아 전면 브라켓 위치</p>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
                    </div>
                </div>

                <div class="tr_3 tr_3_upload">

                    <div class="result_file">
                        <p><em class="far fa-file-image"></em>결과 파일</p>
                        <div class="inner_box">
                            <div class="dwn_box">
                                <span>위치 영역</span>
                                <a class="resultposition" href="" ><em class="far fa-arrow-alt-circle-down"></em>결과 파일 다운로드</a>
                            </div>
                            <div class="dwn_box">
                                <span>브라켓</span>
                                <a class="resultbracket" href="" ><em class="far fa-arrow-alt-circle-down"></em>결과 파일 다운로드</a>
                            </div>
                            <p>결과 파일은 .obj파일로 지원합니다.</p>
                        </div>

                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2"><em class="fas fa-redo"></em>처음으로</button>
                    </div>
                </div>
                <!--tr_3-->
            </div>
            <!--//.faceTracking_box-->

        </div>
        <!-- //.demobox -->
        <!--.ftmenu-->
        <div class="demobox vision_menu" id="bpmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1&#41; REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2&#41; 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1&#41; Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2&#41; 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3&#41; [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4&#41; 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        치아 교정기 포지셔닝  <small>(Bracket Positioning)</small>
                    </div>
                    <p class="sub_txt">주어진 치아에 교정기를 부착할 위치를 선정합니다.</p>
                    <span class="sub_title">
								준비사항
					</span>
                    <p class="sub_txt">- Input: 치아 .obj 파일 </p>
                    <ul>
                        <li>확장자: .obj</li>
                        <li>치아 모델이 수직으로 정렬되어 있어야 합니다. </li>
                    </ul>
                    <span class="sub_title">
								 실행 가이드
							</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL :  https://api.maum.ai/bracket/downloadZip</li>
                    </ul>
                    <p class="sub_txt">② Request 파라미터 설명 </p>
                    <table>
                        <tr>
                            <th>키</th>
                            <th>설명</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>사용자의 고유 ID. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>사용자의 고유 key. 마인즈랩 담당자에게 이메일로 발급요청 </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>file</td>
                            <td>치아 obj 파일 </td>
                            <td>file</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                        <pre>
curl --location --request POST 'http://api.maum.ai/bracket/downloadZip' \
--form 'apiId= {발급받은 API ID}' \
--form 'apiKey= {발급받은 API KEY}' \
--form 'file= {obj 파일 path}'</pre>
                    </div>

                    <p class="sub_txt">④ Response 예제 (Zip File Download)  </p>

                    <div class="code_box">
<pre>
확장자: .obj
ab: 브라켓  region: 브라켓 선정 위치
</pre>
                    </div>

                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.ftmenu-->
        <!--.ftexample-->
        <div class="demobox" id="bpexample">
            <p><em style="color:#f7778a;font-weight: 400;">적용사례</em> <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
            <!-- 치아교정 -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>교정기 위치 선정</span>
                            </dt>
                            <dd class="txt">치아 형태를 인식하여 교정기가 부착될 수 있는 위치를 선정합니다.</dd>
    <%--                        <dd class="api_itemBox">--%>
    <%--                            <ul class="lst_api">--%>
    <%--                                <li class="ico_ftr"><span>얼굴 추적</span></li>--%>
    <%--                                <li class="ico_sr"><span>자막 인식</span></li>--%>
    <%--                            </ul>--%>
    <%--                        </dd>--%>
                        </dl>
                    </li>
                </ul>
            </div>
            <!--치아교정   -->
        </div>
        <!--//.ftexample-->


    </div>
    <!-- //.content -->
</div>

<script>

    var sample1SrcUrl;
    var sample1File;
    var xhrAb;
    var xhrRegion;


    function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/kr/images/00positioning.obj");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function () {
            blob = xhr.response; //xhr.response is now a blob object
            sample1File = new File([blob], "00positioning.obj");
            sample1SrcUrl = URL.createObjectURL(blob);
        };
        xhr.send();
    }


    function sendApiRequest(file, sampleOrFile) {
        var formData = new FormData();
        formData.append('file', file);
        formData.append('${_csrf.parameterName}', '${_csrf.token}');

        $('.tr_2').fadeIn();

        $.when(getAb(formData), getRegion(formData)).done(function(resultAb, resultRegion){
                console.log("Result OK");

                var urlAb = URL.createObjectURL(resultAb);
                var urlRegion = URL.createObjectURL(resultRegion);

                $('.tr_2').hide();
                $('.bp_box').css('border','none');
                $('.tr_3_' + sampleOrFile).fadeIn(300);

                $('.resultbracket').attr('href', urlAb).attr('download',"bracketAb.obj");
                $('.resultposition').attr('href', urlRegion).attr('download',"bracketRegion.obj");

            }).fail(function(){
                console.log("Result fail");
                alert("결과 파일 가져오기에 실패했습니다. \n다시 시도해 주세요.");
                $('.tr_2').hide();
                $('.tr_1').fadeIn(300);
                $('.bp_box').css('border','solid 1px #cfd5eb');
            }
        );

        /* var xhr = new XMLHttpRequest();
         xhr.responseType = 'blob'; // !!필수!!
         xhr.onreadystatechange = function(){
             if (this.readyState == 4 && this.status == 200){

                 console.log(this.response, typeof this.response);
                 var url = URL.createObjectURL(this.response);

                 $('.tr_2').hide();
                 $('.bp_box').css('border','none');
                 $('.tr_3_sample').fadeIn(300);
                 $('#resultbracket').attr('href', url).attr('download',"result.obj");

             }
         };
         xhr.open('POST', '/api/bracket');
         xhr.send(formData);*/

    }



    function getAb(formData){
        let deferred = $.Deferred();
        xhrAb = new XMLHttpRequest();

        xhrAb.responseType = 'blob';
        xhrAb.open('POST', '/api/bracket/Ab');
        xhrAb.addEventListener('load', function() {
            if(xhrAb.status === 200){
                deferred.resolve(xhrAb.response);
            } else{
                deferred.reject("Request error : " + xhrAb.status);
            }
        }, false);

        xhrAb.addEventListener('abort', function() { });

        xhrAb.send(formData);
        return deferred.promise();
    }


    function getRegion(formData){
        let deferred = $.Deferred();
        xhrRegion = new XMLHttpRequest();

        xhrRegion.responseType = 'blob';
        xhrRegion.open('POST', '/api/bracket/Region');
        xhrRegion.addEventListener('load', function() {
            if (xhrRegion.status === 200) {
                deferred.resolve(xhrRegion.response);
            } else{
                deferred.reject("Request error : " + xhrRegion.status);
            }
        }, false);

        xhrRegion.addEventListener('abort', function() { });

        xhrRegion.send(formData);
        return deferred.promise();
    }



    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {

            loadSample1();

            // 위치 선정
            $('.btn_start').on('click', function () {

                let blob;
                let sampleOrFile;

                if ($('#sample1').prop('checked')) { // 샘플 파일로 해보기
                    sampleOrFile = "sample";
                    blob = sample1File;
                    $('.tr_1').hide();

                } else { // 내 파일로 해보기
                    sampleOrFile = "upload";
                    blob = document.getElementById('demoFile').files[0];
                    $('.tr_1').hide();
                }

                sendApiRequest(blob, sampleOrFile);

            });



            //파일 업로드
            document.querySelector("#demoFile").addEventListener('change', function (ev) {

                var demoFile = ev.target.files[0];

                let fileType = demoFile.name;
                fileType = fileType.substr(fileType.lastIndexOf("."));
                if(fileType != ".obj"){
                    alert("obj 파일을 업로드해 주세요");
                    $('em.close').click();
                    return;
                }

                // $('.fl_box').on('click', function () {
                //     $(this).css("opacity", "1");
                // });

                //파일 용량 체크
                // var demoFileInput = document.getElementById('demoFile');



                // var demoFileSize = demoFile.size;
                // var max_demoFileSize = 1024 * 1024 * 50;//1kb는 1024바이트

                document.querySelector(".demolabel").innerHTML = demoFile.name;
                $('#uploadFile').addClass('btn_change').removeClass('btn');
                $('.fl_box').css("opacity", "0.5");
                $('#sample1').prop('checked', false);

            });


           /* //파일명 변경
            document.querySelector("#demoFile").addEventListener('change', function (ev) {
                document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
                $('#uploadFile').addClass('btn_change');
                $('#uploadFile').removeClass('btn');
            });*/


            // 업로드 x
            $('em.close').on('click', function () {
                $('.fl_box').css("opacity", "1");
                $(this).parent().removeClass("btn_change");
                $(this).parent().addClass("btn");
                $(this).parent().children('.demolabel').text('파일 업로드');
                $('#demoFile').val(null);
            });


            // 샘플 클릭
            $('.sample_box').on('click', function () {
                $('em.close').trigger('click');
                $('#sample1').prop('checked', true);
                $('.fl_box').css("opacity", "1");
            });


            // 처음으로 버튼
            $('.btn_back1').on('click', function () {
                xhrRegion.abort();
                xhrAb.abort();
                $('.tr_2').hide();
                $('.tr_1').fadeIn(300);
                // $('.fl_box').css("opacity", "1");
                $('.bp_box').css('border','solid 1px #cfd5eb');
            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('.tr_3_sample').hide();
                $('.tr_3_upload').hide();
                $('.tr_1').fadeIn(300);
                // $('.fl_box').css("opacity", "1");
            });

            //알림 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
        });
    });


//API 탭
function openTap(evt, menu) {
    var i, demobox, tablinks;
    demobox = document.getElementsByClassName("demobox");
    for (i = 0; i < demobox.length; i++) {
        demobox[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(menu).style.display = "block";
    evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();

</script>