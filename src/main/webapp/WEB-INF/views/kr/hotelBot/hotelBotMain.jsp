<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-01-16
  Time: 15:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/hotelbot.css?ver=<%=fmt.format(lastModifiedStyle)%>" />

<!-- .contents -->
<div class="contents">
    <div class="content api_content bot_content">
        <h1 class="api_tit">금융봇</h1>
        <ul class="menu_lst bot_lst">
            <li class="tablinks" onclick="openTap(event, 'hotelbox')" id="defaultOpen"><button type="button">엔진</button></li>
            <li class="tablinks" onclick="openTap(event, 'hotelexample')"><button type="button">적용사례</button></li>
            <li class="tablinks" onclick="openTap(event, 'hotelmenu')"><button type="button">매뉴얼</button></li>
<%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <div class="demobox" id="hotelbox">
            <p>maum SDS<small>(Spoken Dialog System)</small>를 활용한 <span style="color:#2cace5;">호텔 컨시어지 챗봇</span></p>
            <span class="sub">호텔 예약 전부터 퇴실까지, 호텔 관련 문의 및 컨시어지 서비스를 담당하는 챗봇입니다.<br>
24시간 응대, 다국어 서비스를 통하여 고객 응대율을 높여줍니다.
</span>

            <!-- chatbot_box -->
            <div class="chatbot_box" id="hotelBot">
                <div class="hotelbot_box">
                    <div class="hotelbot">
                        <iframe src="https://sds.maum.ai/chatbot?hostNo=7&intent=%EC%B2%98%EC%9D%8C%EC%9C%BC%EB%A1%9C"></iframe>
                    </div>
                </div>

            </div>
            <!-- //.chatbot_box -->

        </div>
        <!-- .demobox -->

        <!--.nqamenu-->
        <div class="demobox bot_menu" id="hotelmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API 공통 가이드
                    </div>
                    <p class="sub_title">개발 환경 세팅</p>
                    <p class="sub_txt">1) REST API는 HTTP 요청을 보낼 수 있는 환경이라면 어디에서든 이용할 수 있습니다.</p>
                    <p class="sub_txt">2) 다양한 환경의 웹 서버에서 활용 가능합니다. (Python, Web, Javascript, Java)</p>
                    <p class="sub_title">키 발급</p>
                    <p class="sub_txt">1) Mindslab에서 제공되는 API 서비스를 사용하기 위해서는 키를 선지급 받아야합니다.</p>
                    <p class="sub_txt">2) 마음AI 플랫폼에서 Business Plan 이상 구독 신청을 하셔야합니다. (https://maum.ai) </p>
                    <p class="sub_txt">3) [계정메뉴]에서 본인의 API ID 및 Key를 확인해주세요.</p>
                    <p class="sub_txt">4) 발급받은 API ID 및 Key를 기억하여 아래 매뉴얼에 맞게 사용합니다.</p>
                    <p class="sub_txt">※ Mindslab API를 위한 ID와 키는 외부 유출 및 공유를 금합니다.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        호텔 컨시어지 챗봇 <small></small>
                    </div>
                    <p class="sub_txt">호텔 예약 전부터 퇴실까지, 호텔 관련 문의 및 컨시어지 서비스를 담당하는 챗봇입니다.<br>
                        24시간 응대, 다국어 서비스를 통하여 고객 응대율을 높여줍니다.
                    </p>

                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--//nqamenu-->
        <!--.nqaexample-->
        <div class="demobox" id="hotelexample">
            <p><em style="color:#2cace5;font-weight: 400;">적용사례</em>  <small>(Use Cases)</small></p>
            <span class="sub">마인즈랩의 앞선 기술을 다양하게 활용할 수 있습니다.</span>
            <%--챗봇(chatbot)--%>
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>다국어 <br>
서비스 챗봇</span>
                            </dt>
                            <dd class="txt">최신 AI 언어 모델인 ‘BERT’를 활용하여, 특정 언어에 종속되지 않는 다국어 서비스 챗봇을 쉽고 빠르게 제작할 수 있습니다. </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_sds"><span>SDS</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>Voice Bot</span>
                            </dt>
                            <dd class="txt">음성 인터페이스의 챗봇 서비스(Voice Bot)를 구현할 수 있습니다. </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_stt"><span>STT</span></li>
                                    <li class="ico_sds"><span>SDS</span></li>
                                    <li class="ico_tts"><span>TTS</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>서비스 특화<br>
챗봇 플랫폼 구축</span>
                            </dt>
                            <dd class="txt">단순하게 1개의 챗봇을 구현하는 것뿐만이 아닌, 호텔 챗봇 서비스와 같이 특정 서비스에 특화된 챗봇 플랫폼을 구축할 수 있습니다.
                                <br> 사업 제휴 문의 : <a href="/support/krContactUs" alt="contact us" style="color: #2dabe5;">Contact us</a> </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_sds"><span>SDS</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //챗봇(chatbot) -->

        </div>
        <!--//.nqaexample-->


    </div>

</div>
<!-- //.contents -->

<!-- //wrap -->
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/common.js"></script>

<script type="text/javascript">
    jQuery.event.add(window,"load",function(){
        $(document).ready(function (){

        });
    });

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>


