<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2019-11-22
  Time: 10:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>


<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">

    <!-- Open Graph Tag -->
    <meta property="og:title"            content="maum.ai"/>
    <meta property="og:type"             content="website"/><!-- 웹 페이지 타입 -->
    <meta property="og:url"              content="https://maum.ai"/>
    <meta property="og:image"            content="${pageContext.request.contextPath}/aiaas/common/images/maum.ai_web.png"/>
    <meta property="og:description"      content="웹인공지능이 필요할땐 마음AI"/>

    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/common/css/all.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/reset.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/font.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/login_renew.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/pop_common.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/kr/css/voicealbum.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-1.11.2.min.js"></script>

    <title>보이스 앨범</title>
</head>

<body>
<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<!-- 5 .pop_simple -->
<div class="pop_simple album_contact">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap">
        <em class="fas fa-times ico_close"></em>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <h1>We call it LOVE, <strong>Voice 앨범</strong> 서비스</h1>
            <p>보이스 앨범과 관련된 문의를 남겨 주시면 담당자가 확인 후 1-2일 내로 연락 드리겠습니다.</p>
            <div class="contact_info">
                <span>1661-3222</span>
                <span>hello@mindslab.ai</span>
            </div>
            <div class="contact_form">
                <div class="inquiry_box">
                    <div class="my_info">
                        <input type="text" id="name">
                        <label for="name"><span class="fas fa-user"></span>이름</label>
                    </div>
                    <div class="my_info">
                        <input type="email" id="mail">
                        <label for="mail"><span class="fas fa-envelope"></span>이메일</label>
                    </div>
                    <div class="my_info">
                        <input type="tel" pattern="[0-9]{3}-[0-9]{4}-[0-9]{4}" id="phone" >
                        <label for="phone"><span class="fas fa-mobile-alt"></span>연락처</label>
                    </div>

                    <div class="my_txt">
                        <textarea id="txt" ></textarea>
                        <label for="txt"><span class="fas fa-align-left"></span>문의내용</label>
                    </div>

                    <button class="btn_inquiry" id="sendMailToHello_voiceAlbum">문의하기</button>

                </div>
            </div>
        </div>
        <!-- //.pop_bd -->

    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<header class="common_header">
    <div class="header_box">
        <h1><a href="/login/loginForm?lang=ko"><img src="${pageContext.request.contextPath}/aiaas/kr/images/logo_title_navy.svg" alt="maum.ai logo"></a>
            <span>인공지능이 필요할 땐 마음AI</span>
        </h1>
        <!--.sta-->
        <div class="sta">
            <a href=/home/pricingPage?lang=ko" class="go_price">가격정책 </a>
            <a href="/home/academyForm" class="go_academy">마음 아카데미</a>
            <a class="btn_sign" href="javascript:login()">마음AI 로그인</a>
            <!--.etcMenu-->
            <div class="etcMenu">
                <ul>
                    <li class="lang">
                        <p class="lang_select">한국어<em class="fas fa-chevron-down"></em></p>
                        <ul class="lst">
                            <li><a href="/login/loginForm?lang=en" target="_self"><em>English</em></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!--//.etcMenu-->
        </div>
        <!--//.sta-->
    </div>
</header>

<!-- wrap -->
<div id="wrap">
    <div class="loginWrap">
        <!-- .album_cont -->
        <div class="album_cont">
            <div class="album_logo">
                <img src="${pageContext.request.contextPath}/aiaas/common/images/logo_voicealbum.svg" alt="voice album logo">
                <div class="title">
                    <h1><span>Voice</span> 앨범</h1>
                    <strong>We call it LOVE!</strong>
                </div>
            </div>
            <p class="desc_txt"><strong>소중한 사람의 목소리</strong>를 안전하게 보관하여 언제 어디서든 꺼내어 들을 수 있는 <strong>AI 감성 서비스 Open!</strong></p>
            <div class="text_box">
                <p>
                    왜 그때는 미처 몰랐을까요?<br>
                    <strong>그 목소리</strong>가 그토록 그립고 다시 듣고 싶을거라는 걸···.<br>
                    소중한 사람의 <strong>목소리</strong>를 <strong>Voice 앨범</strong>으로 만들어 들어 보세요.
                </p>
            </div>
            <div class="service_box">
                <p>서비스 이용 방법</p>
                <ul>
                    <li>
                        전문 컨설턴트와<br>
                        AI 보이스 제작 상담
                    </li>
                    <li>
                        가이드에 따른<br>
                        목소리 녹음
                    </li>
                    <li>
                        AI 보이스 생성 후<br>
                        원하는 문구 재현
                    </li>
                </ul>
            </div>
            <div class="service_box">
                <p class="contacttit" id="album_contact">서비스 신청 및 문의하기</p>
                <div class="contactbox">
                    <div class="contact">
                        <a href="tel:+8216613222">1661-3222</a>
                        <a href="mailto:hello@mindslab.ai">hello@mindslab.ai</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- //.album_cont -->

        <!-- .album_footer -->
        <div class="album_footer">
            <div class="footer_area">
                <p>
                    <a href="https://mindslab.ai:8080/kr/company" target="_blank" class="footerlogo"><img src="${pageContext.request.contextPath}/aiaas/common/images/mindslab-logo-h-white.svg" alt="footer logo"></a>
                    <span><a href="https://mindslab.ai:8080/kr/company" target="_blank">주식회사 마인즈랩</a> &nbsp;&nbsp;|</span>
                    <span>&nbsp;&nbsp; 경기도 성남시 분당구 대왕판교로644번길 49 다산타워 6층&nbsp;ㅣ&nbsp;</span>
                    <span>대표 유태준&nbsp;&nbsp;ㅣ&nbsp;&nbsp;사업자 등록번호 314-86-55446</span>

                </p>
            </div>
        </div>
        <!-- //album_footer -->
    </div>
</div>
<!-- //wrap -->

<script type="text/javascript">
    $(window).load(function() {
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
    });

    $(document).ready(function() {
        var placeholderLabel = $('.inquiry_box input, .inquiry_box textarea');
        placeholderLabel.on('focus', function(){
            $(this).siblings('label').hide();
        });
        placeholderLabel.on('focusout', function(){
            if($(this).val() === ''){
                $(this).siblings('label').show();
            }
        });


        $('#sendMailToHello_voiceAlbum').on('click', function(){
            var title = $("#name").val() + " (" + $("#phone").val() + ") 님의 문의 사항입니다.";
            var msg = "이름 : " + $("#name").val() + "\n연락처 : " + $("#phone").val() + "\n문의 내용 : " + $("#txt").val();

            var formData = new FormData();

            formData.append('fromaddr', $('#mail').val());
            formData.append('toaddr', 'hello@mindslab.ai');
            formData.append('subject', title);
            formData.append('message', msg);
            formData.append($("#key").val(), $("#value").val());

            if($('.my_info input').val()==='' || $('.my_txt textarea').val()==='') {

                alert("내용을 모두 입력해야 이메일을 전달할 수 있어요!")

            }else{

                $.ajax({
                    type 	: 'POST',
                    async   : true,
                    url  	: '/support/sendContactMail',
                    dataType : 'text',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success : function(obj) {
                        $("#name").val();
                        $("#phone").val();
                        $("#txt").val();
                        $("#mail").val();

                        alert("이메일을 보냈습니다. 담당자 확인 후 연락 드리겠습니다 :)");
                        window.location.href = "/voiceAlbum/krVoiceAlbum";
                    },
                    error 	: function(xhr, status, error) {
                        console.log("error");
                        alert("Contact Us 메일발송 요청 실패하였습니다.");
                        window.location.href = "/voiceAlbum/krVoiceAlbum";
                    }
                });
            }
        });

        $('#album_contact').on('click', function(){
            $('.album_contact').fadeIn();
        });
        // 공통 팝업창 닫기
        $('.ico_close, .pop_bg').on('click', function () {
            $('.album_contact').fadeOut(300);
            $('body').css({
                'overflow': '',
            });
        });

        // language (pc)
        $('.header_box .sta .etcMenu ul li.lang').each(function(){
            $(this).on('click',function(){
                $(this).toggleClass('active');
            });
            $('.header_box .sta .etcMenu ul li.lang ul.lst li a').on('click',function(){
                $(this).removeClass('active');
            });
            $('#container').on('click',function(){
                $('.header_box .sta .etcMenu ul li.lang').removeClass('active');
            });
        });

        // language (mobile)
        $('ul.m_etcMenu li.lang').each(function(){
            $(this).on('click',function(){
                $(this).toggleClass('active');
            });
            $('ul.m_etcMenu li.lang ul.lst li a').on('click',function(){
                $(this).removeClass('active');
            });
        });
    });
</script>

</body>
</html>
