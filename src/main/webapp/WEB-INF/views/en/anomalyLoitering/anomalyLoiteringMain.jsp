<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>Upload error</h5>
            <p>Error occurred due to filename extension or file size. <br>Please check the restriction again.</p>
            <span> * Supported files: .mp4<br>* Video file size under 50MB.</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">OK</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents api_content">
    <!-- .content -->
    <div class="content">
        <h1 class="api_tit">Loitering Detection</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks active" onclick="openTap(event, 'dtndemo')" id="defaultOpen">
                <button type="button">AI Engine</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'dtnexample')">
                <button type="button">Use Case</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'dtnmenu')">
                <button type="button">Manual</button>
            </li>
        </ul>

        <!-- .demobox -->
        <div class="demobox" id="dtndemo">
            <p><span>Loitering Detection</span></p>
            <span class="sub">By specifying an area, you will be notified when someone wanders the area for a long time.</span>
            <!--demo_layout-->
            <div class="demo_layout detection_box">
                <!--tr_1-->
                <div class="tr_1">
                    <div class="fl_box">
                        <p><em class="far fa-file-video"></em>Use Sample File</p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="anomaly_option" value="1" checked="">
                                    <label for="sample1" class="female">
                                        <video controls>
                                            <source src="${pageContext.request.contextPath}/aiaas/common/video/sample_loiteringDetect.mp4" type="video/mp4">
                                            IE 8 and below does not produce video. Please update the IE version.
                                        </video>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-video"></em>Use My file</p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-video hidden"></em>
                                <label for="demoFile" class="demolabel">Upload Video</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".mp4">
                            </div>
                            <ul>
                                <li>* Supported files: .mp4</li>
                                <li>* Video file size under 50MB</li>
                                <li>* The video must be taken in accordance with the CCTV environment.</li>
                                <li>* A person should be positioned inside the frame without being cut off.</li>
                                <li>* Use an angle and illumination image similar to the sample.</li>
                                <li>* With only one object to be detected in the video, the accuracy is high.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">Set Zone</button>
                    </div>
                </div>
                <!--tr_1-->

                <!-- edit_box -->
                <div class="edit_box">
                    <p><em class="far fa-file-image"></em>Set Zone</p>
                    <div id="edit_box" class="img_box">
                        <div id="draw_area" style="position: relative; z-index: 5; width: 100%; height: 100%">
                            <svg id="theSVG" width="100%" height="100%">
                                <polygon id="x" points="200 100 300 100 300 200 200 200" fill="#ffea00" style="position:absolute; left:0; top:0; fill-opacity:0; stroke:#ffea00; stroke-width:2; z-index:5"></polygon>
                            </svg>
                        </div>
                        <video id="editVideo" style="position: absolute; left: 0; top: 0">
                            <source src="" type="video/mp4">
                            IE 8 and below does not produce video. Please update the IE version.
                        </video>
                    </div>
                    <p class="desc">* Please specify the area to detect loitering. (ex. nearby parks etc.)</p>

                    <div class="shape_select">
                        <p class="tit">Select area shape</p>
                        <div class="btnBox">
                            <button type="button" class="btn_rect active" value="4">
                                <span>Rectangle</span>
                            </button>
                            <button type="button" class="btn_ptg" value="5">
                                <span>Pentagon</span>
                            </button>
                            <button type="button" class="btn_hxg" value="6">
                                <span>Hexagon</span>
                            </button>
                        </div>
                        <p class="guide_txt">You can fine-tune the zoning by selecting the vertices.</p>
                    </div>

                    <div class="btn_area">
                        <a class="btn_start btn_cancel">Reset</a>
                        <a class="btn_start" id="recogButton">Process</a>
                    </div>
                </div>
                <!-- //.edit_box -->

                <!--tr_2-->
                <div class="tr_2">
                    <p><em class="far fa-file-video"></em>In Progress</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"></path>
                            <g>
                                <path fill="#f7778a" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"></path>
                                <animateTransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"></animateTransform>
                            </g></svg>
                        <p>AI processing takes a while...(about 10 seconds)</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
                    </div>

                </div>
                <!--tr_2-->

                <!--tr_3-->
                <div class="tr_3">
                    <div class="result_file">
                        <p><em class="far fa-file-image"></em>Result</p>
                        <div class="result_box">
                            <!-- [D] 해당되는 상태값(.status span)에 addClass('on')하면 보여짐 -->
                            <div class="status">
                                <span class="on"> Loitering</span>
                                <span>Normal (Undetected)</span>
                            </div>
                            <!-- [D] 감지 내용 있는경우 img태그에, 정상인 경우는 em태그에 addClass('on')하면 보여짐 -->
                            <div class="img">
                                <img id="resultImg" class="on" src="" alt="result image">
                                <em class="fas fa-check"><span>Normal</span></em>
                            </div>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2"><em class="fas fa-redo"></em>Reset</button>
                    </div>
                </div>
                <!--tr_3-->
            </div>
            <!--//.demo_layout-->
        </div>
        <!-- //.demobox -->


        <!--.ftmenu-->
        <div class="demobox vision_menu" id="dtnmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">API Guideline</div>
                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1) REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2) It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID &amp; Key</p>
                    <p class="sub_txt">1) You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2) Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                    <p class="sub_txt">3) After subscription, request an ID and key with required information filled in.<br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4) After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                    <p class="sub_txt">5) Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>

                <div class="guide_group">
                    <div class="title">Loitering Detection</div>
                    <p class="sub_txt">By specifying an area, you will be notified when someone wanders the area for a long time.</p>

                    <span class="sub_title">Preparation</span>
                    <p class="sub_txt">- Input: video file</p>
                    <ul>
                        <li>File type : .mp4</li>
                        <li>Size : Under 50MB</li>
                    </ul>
                    <span class="sub_title">API Document</span>

                    <!-- Upload START-->
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/abnormal-behavior/loitering:detect</li>
                    </ul>
                    <p class="sub_txt">② Request parameters </p>
                    <table>
                        <tbody>
                        <tr>
                            <th>Key</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API  ID. Request from is required for the ID.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>video</td>
                            <td>type:file (.mp4) video file</td>
                            <td>file</td>
                        </tr>
                        <tr>
                            <td>roiList</td>
                            <td>area of interest information, Area information to detect loitering</td>
                            <td>string</td>
                        </tr>
                        </tbody>
                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
<pre>
url --location --request POST 'https://api.maum.ai/abnormal-behavior/loitering:detect' \
--header 'Content-Type: multipart/form-data' \
--form 'apiId= Own API Id'
--form 'apiKey= Own API Key'
--form 'video= Input video file'
--form 'roiList="{\"overall\":[0,130,1280,130,1280,720,0,720],\"loitering\":[7,260,11,701,1177,699,1177,168,495,149]}"'
</pre>
                    </div>

                    <p class="sub_txt">④ Response example </p>

                    <div class="code_box">
<pre>
{
	"message": {
		"message": "Success",
		"status": 0
	},
	"payload": {
		"status": "loitering",
		"abnormalImg": "w7/DmMO/w6AAEEpGSUYAAQEAAAEAAQAAw7/..."
	}
}
</pre>
                    </div>
                    <!-- Upload END-->
                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.ftmenu-->

        <!--.ftexample-->
        <div class="demobox" id="dtnexample">
            <p><em style="color:#f7778a; font-weight:500;">Use Cases</em></p>
            <span class="sub">Find out how AI can be applied to diverse area.</span>
            <div class="useCasesBox">
                <!-- 이상행동 감지-->
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>Rapid detection of danger</span>
                            </dt>
                            <dd class="txt">It recognizes in real time that someone is staying in a specific area for a certain amount of time or longer, and promptly alerts you as crime signs.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_litrDtc"><span>Loitering Detection</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
                <!--이상행동 감지-->
            </div>
        </div>
        <!--//.ftexample-->


    </div>
    <!-- //.content -->
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
<script>

    let ajaxXHR;

    const svgWidth = 500;
    const svgHeight = 280;
    let realVideoWidth = 0;
    let realVideoHeight = 0;
    let handles = [];
    let polygonInitPoints = {
        rect : '200 100 300 100 300 200 200 200',
        ptg : '250 100 300 140 280 195 220 195 200 140',
        hxg : '250 100 290 126 290 171 250 197 212 171 211 126'
    }

    jQuery.event.add(window,"load",function(){
        $(document).ready(function (){

            loadSample1();
            document.getElementById("defaultOpen").click();
            draggablePolygon(document.getElementById("x"));

            // 샘플 클릭시 업로드된 파일 제거
            $('.radio input, .radio video').on('click', function () {
                $('em.close').trigger('click');
                $('#sample1').prop('checked', true);
            });

            // 파일 업로드
            document.querySelector("#demoFile").addEventListener('change', function () {
                let demoFileInput = document.getElementById('demoFile');
                let demoFile = demoFileInput.files[0];
                let demoFileSize = demoFile.size;
                let max_demoFileSize = 1024 * 1024 * 50;//1kb는 1024바이트
                //파일 용량, 확장자 체크
                if (demoFileSize > max_demoFileSize || !demoFile.type.match(/video.mp4/)) {
                    $('.pop_simple').show();
                    $('#demoFile').val('');
                } else {
                    $('.demolabel').html(demoFile.name);
                    $('#uploadFile').removeClass('btn');
                    $('#uploadFile').addClass('btn_change');
                    $('.fl_box').css("opacity", "0.5");
                    $('#sample1').prop('checked', false);
                }
            });

            // 업로드된 파일명박스 제거
            $('em.close').on('click', function () {
                $('.fl_box').css("opacity", "1");
                $('#uploadFile').removeClass("btn_change");
                $('#uploadFile').addClass("btn");
                $('.demolabel').html('Upload Video');
                $('#demoFile').val('');
                $('#sample1').prop('checked', true);
            });

            // 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({ 'overflow': '', });
            });

            // 영역 설정 버튼 클릭
            $('#sub').on('click', function(){
                let $demoFile = $("#demoFile");
                let videoSrc;
                realVideoWidth = 0;
                realVideoHeight = 0;

                if ($demoFile.val() === "" || $demoFile.val() === null) {
                    videoSrc = $('.sample_box video source').attr('src');
                } else {
                    const blob = document.getElementById('demoFile').files[0];
                    videoSrc = URL.createObjectURL(blob);
                }

                $('#editVideo').attr('src',videoSrc);
                $('.btn_rect').click();
                $('.tr_1').hide();
                $('.edit_box').show();
            });


            $('#editVideo').on('loadedmetadata', function(){
                realVideoWidth = $(this).get(0).videoWidth;
                realVideoHeight = $(this).get(0).videoHeight;
            });


            /*---------------------------- 영역 설정 -------------------------------*/
            // 영역 모양 선택
            $('.shape_select button').on('click', function(){
                $('.shape_select button').removeClass('active');
                $(this).addClass('active');

                for(let i=0; i < handles.length; i++) { handles[i].remove(); }

                switch ($(this).val()){
                    case "4":
                        $('#x').attr('points', polygonInitPoints.rect);
                        break;
                    case "5":
                        $('#x').attr('points', polygonInitPoints.ptg);
                        break;
                    case "6":
                        $('#x').attr('points', polygonInitPoints.hxg);
                        break;
                    default:
                        break;
                }

                handles = [];
                draggablePolygon(document.getElementById("x"));

            });

            // 취소 버튼 클릭
            $('.btn_cancel').on('click', function(){
                $('.edit_box').hide();
                $('.tr_1').show();
            });

            // 결과 보기 버튼 클릭
            $('#recogButton').on('click', function(){
                let roiList;
                let formData = new FormData();

                if ($('#demoFile').val() === "" || $('#demoFile').val() === null) {
                    formData.append('video', sampleVideo);
                } else {
                    const blob = document.getElementById('demoFile').files[0];
                    let filename = Date.now() + Math.random().toString(36).substring(2, 15);
                    filename += '.' + blob.type.split('/').pop();
                    formData.append('video', new File([blob], filename, {type : "video/mp4", lastModified: Date.now()}));
                }

                roiList = setRoiList($('#x').attr('points'));
                formData.append('roiList', JSON.stringify(roiList));
                formData.append('${_csrf.parameterName}', '${_csrf.token}');

                console.log(roiList);

                ajaxXHR = $.ajax({
                    type: "POST",
                    async: true,
                    url: "/api/anomalyLoitering",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        if(result === "" || result === undefined){
                            console.log("result null");
                            return;
                        }

                        let resultData = JSON.parse(result);
                        console.log('result message : ', resultData.message);

                        if(resultData.payload.status === "normal"){
                            $('.tr_3 .status span').removeClass('on');
                            $('.tr_3 .status span').eq(1).addClass('on');
                            $('.tr_3 .img #resultImg').removeClass('on');
                            $('.tr_3 .img em').addClass('on');
                        }else{
                            $('.tr_3 .img #resultImg').attr('src', "data:image/jpeg;base64," + resultData.payload.abnormalImg);

                            $('.tr_3 .status span').removeClass('on');
                            $('.tr_3 .status span').eq(0).addClass('on');
                            $('.tr_3 .img #resultImg').addClass('on');
                            $('.tr_3 .img em').removeClass('on');
                        }

                        $('.tr_2').hide();
                        $('.tr_3').fadeIn(300);

                    },
                    error: function (jqXHR) {
                        if (jqXHR.status === 0) {
                            return false;
                        }
                        console.dir(jqXHR.error);
                        alert("Failed to get response from server.\nPlease try again");
                        window.location.reload();
                    }
                });


                $('.edit_box').hide();
                $('.tr_2').show();
            });
            /*---------------------------------------------------------------------------*/

            $('.btn_back1').on('click', function(){
                if(ajaxXHR){ ajaxXHR.abort(); }
                $('.tr_2').hide();
                $('.tr_1').show();
            });


            $('.btn_back2').on('click', function(){
                $('.tr_3 status #resultImg').attr('src', null);
                $('.tr_3').hide();
                $('.tr_1').show();
            });

        });
    });

    function setRoiList(points){
        let pointList = points.split(" ");
        let roiObj = {
            "overall" : [0, 0, realVideoWidth, 0, realVideoWidth, realVideoHeight, 0, realVideoHeight],
            "action" : []
        }

        for(let i=0; i<pointList.length; i++) {
            if(i % 2 === 0){
                roiObj.action.push(Number(((realVideoWidth * pointList[i]) / svgWidth).toFixed(0)));
            }else{
                roiObj.action.push(Number(((realVideoHeight * pointList[i]) / svgHeight).toFixed(0)));
            }
        }

        return roiObj;
    }


    function draggablePolygon(polygon) {
        let points = polygon.points;
        let svgRoot = $("#theSVG");

        for (let i = 0; i < points.numberOfItems; i++) {
            (function (i) { // close over variables for drag call back
                let point = points.getItem(i);

                let handle = document.createElement("div");
                handle.className = "handle";
                document.getElementById("edit_box").appendChild(handle);
                handles.push(handle);

                let base = svgRoot.position();
                base.left = 0;
                base.top = 0;
                // center handles over polygon
                let cs = window.getComputedStyle(handle, null);
                base.left -= (parseInt(cs.width) + parseInt(cs.borderLeftWidth) + parseInt(cs.borderRightWidth))/2;
                base.top -= (parseInt(cs.height) + parseInt(cs.borderTopWidth) + parseInt(cs.borderBottomWidth))/2;


                handle.style.left = base.left + point.x + "px";
                handle.style.top = base.top + point.y + "px";

                $(handle).draggable({
                    drag: function (event) {
                        setTimeout(function () { // jQuery apparently calls this *before* setting position, so defer
                            let pointX = parseInt(handle.style.left) - base.left;
                            let pointY = parseInt(handle.style.top) - base.top;
                            let svgWidth = svgRoot.width();
                            let svgHeight = svgRoot.height();

                            handle.style.left = base.left + Math.max(Math.min(Number(pointX.toFixed(2)), svgWidth), 0) + "px";
                            point.x = Math.max(Math.min(Number(pointX.toFixed(2)), svgWidth), 0);

                            handle.style.top = base.top + Math.max(Math.min(Number(pointY.toFixed(2)), svgHeight), 0) + "px";
                            point.y = Math.max(Math.min(Number(pointY.toFixed(2)), svgHeight), 0);
                        },0);
                    }
                });
            }(i));
        }
    }

    var sampleVideo;
    function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/aiaas/common/video/sample_loiteringDetect.mp4");
        xhr.responseType = "blob";
        xhr.onload = function () {
            blob = xhr.response;
            sampleVideo = new File([blob], "sample_loiteringDetect.mp4", {type : "video/mp4"});
        }
        xhr.send();
    }

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

</script>