<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/common.css">

<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">

<!-- .contents -->
<div class="contents">
    <div class="content api_content">
        <h1 class="api_tit">Text Style Transfer</h1>
        <ul class="menu_lst">
            <li class="tablinks active" onclick="openTap(event, 'txtStyTrans_demo')" id="defaultOpen">
                <button type="button">AI Engine</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'txtStyTrans_example')">
                <button type="button">Use Case</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'txtStyTrans_menu')">
                <button type="button">Manual</button>
            </li>
        </ul>
        <!--.conv_demo-->
        <div class="demobox demobox_nlu" id="txtStyTrans_demo">
            <p><span>Text Style Transfer</span></p>
            <span class="sub">It changes the sentence to the desired style while conveying the meaning consistently.</span>

            <!--.itf_box-->
            <div class="itf_box correct_box txtStyTrans_box">
                <!--.step01-->
                <div class="step01">
                    <div class="demo_top">
                        <span>Step 1. Sample</span>
                    </div>
                    <div class="ift_topbox">
                        <p>Select the sentence you wish to convert.</p>
                        <ul class="ift_lst">
                            <li>
                                <div class="radio">
                                    <input type="radio" id="radio" name="sentence" value="내일 저녁으로 삼겹살 어때?"
                                           checked="checked">
                                    <label for="radio">내일 저녁으로 삼겹살 어때?</label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="radio2" name="sentence" value="끝나고 시간이 되면 참석하도록 하겠습니다.">
                                    <label for="radio2">끝나고 시간이 되면 참석하도록 하겠습니다. </label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="radio3" name="sentence" value="user-input">
                                    <label for="radio3"></label>
                                </div>
                                <input type="text" class="iptTxt"
                                       placeholder="Type in (Input at least five characters.)">
                                <div class="guide_txt">* Please enter a sentence that is easier than the example sentence.<br>
                                    &nbsp;&nbsp;&nbsp;Sentences more complex than example sentences may be difficult to style.</div>
                                <div class="txtCount">(<span>0</span>/80 characters)</div>
                            </li>
                        </ul>
                    </div>

                    <div class="btm_box">
                        <div class="fl">
                            <div class="demo_top">Step 2. Text Style</div>
                            <div class="demo_slt_box">
                                <div class="radioBox">
                                    <!-- [D] 성경구절 checked default -->
                                    <input type="radio" id="style01" name="txtStyle" value="bible" checked>
                                    <label for="style01">Bible</label>
                                    <input type="radio" id="style02" name="txtStyle" value="hashtag">
                                    <label for="style02">Hashtag</label>
                                    <input type="radio" id="style03" name="txtStyle" value="twitter">
                                    <label for="style03">Twit</label>
                                    <input type="radio" id="style04" name="txtStyle" value="messenger">
                                    <label for="style04">Messenger</label>
                                    <input type="radio" id="style05" name="txtStyle" value="speech">
                                    <label for="style05">Speech</label>
                                    <input type="radio" id="style06" name="txtStyle" value="non_fiction">
                                    <label for="style06">Non-fiction</label>
                                    <input type="radio" id="style07" name="txtStyle" value="personal_conversation">
                                    <label for="style07">Conversation</label>
                                    <input type="radio" id="style08" name="txtStyle" value="script">
                                    <label for="style08">Script</label>
                                </div>
                            </div>
                        </div>

                        <div class="fr">
                            <div class="demo_top">Step 3. of Sentences</div>
                            <div class="demo_slt_box">
                                <div class="sltBox">
                                    <select class="select" id="returnSequence">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <!-- [D] 3문장 selected default -->
                                        <option value="3" selected>3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </select>
                                    <em class="fas fa-caret-down"></em>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="btn_area">
                        <!-- [D] 예문 직접 입력 시 5자 미만인 경우 'Transfer' 버튼 클릭 시 alert('최소한 5자 이상의 문장을 입력해주세요.') 띄워주세요 -->
                        <button type="button" class="btn_transfer" id="btnTransfer">Transfer</button>
                    </div>
                </div>
                <!--//.step01-->

                <div class="loading_wrap">
                    <p><em class="far fa-file-alt"></em> In progress</p>
                    <div class="loding_box">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px"
                             viewBox="0 0 128 16" xml:space="preserve"><path fill="#c9c3ec" fill-opacity="0.42"
                                                                             d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"></path>
                            <g>
                                <path fill="#27c1c1" fill-opacity="1"
                                      d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"></path>
                                <animateTransform attributeName="transform" type="translate"
                                                  values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0"
                                                  calcMode="discrete" dur="1820ms"
                                                  repeatCount="indefinite"></animateTransform>
                            </g></svg>

                        <p>AI processing takes a while..</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
                    </div>

                </div>

                <!--.step02-->
                <div class="step02">
                    <div class="demo_top">
                        <em class="far fa-file-alt"></em><span>Result</span>
                    </div>
                    <div class="result_box">
                        <ul>
                            <li>
                                <dl>
                                    <dt><span>Input</span></dt>
                                    <dd>
                                        <p id="inputSentence"></p>
                                    </dd>
                                </dl>
                            </li>
                            <li>
                                <dl>
                                    <dt><span>Result</span></dt>
                                    <dd id="resultTextarea">
                                        <p></p>
                                    </dd>
                                </dl>
                            </li>
                        </ul>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_reset" id="btn_reset"><em class="fas fa-redo-alt"></em>Reset
                        </button>
                    </div>
                </div>
                <!--//.step02-->
            </div>
            <!--//.itf_box-->

        </div>
        <!-- .conv_demo -->

        <!--.conv_menu-->
        <div class="demobox" id="txtStyTrans_menu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">API Guideline</div>
                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1) REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2) It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID &amp; Key</p>
                    <p class="sub_txt">1) You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2) Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform.
                        (https://maum.ai)</p>
                    <p class="sub_txt">3) After subscription, request an ID and key with required information filled in.
                        <br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4) After an agreement, Minds Lab would send you an ID, key within 1-2 business
                        days through email.</p>
                    <p class="sub_txt">5) Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>

                <div class="guide_group">
                    <div class="title">Text Style Transfer</div>
                    <p class="sub_txt">It changes the sentence to the desired style while conveying the meaning
                        consistently.</p>

                    <span class="sub_title">Preparation</span>
                    <p class="sub_txt">- Input: Sentence (Korean)</p>

                    <span class="sub_title">API Document</span>

                    <!-- Upload START-->
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/text-style/transfer</li>
                    </ul>
                    <p class="sub_txt">② Request parameters</p>
                    <table>
                        <tbody>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId</td>
                            <td>Unique API ID. Request from is required for the ID.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey</td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>originalText</td>
                            <td>Sentences for style conversion</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>numReturnSequences</td>
                            <td>Number of transformed sentences to be returned</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>targetStyle</td>
                            <td>Style to Convert</td>
                            <td>string</td>
                        </tr>
                        </tbody>
                    </table>
                    <p class="sub_txt">③ Request example</p>
                    <div class="code_box">
<pre>
curl --location --request POST 'http://api.maum.ai/text-style/transfer' \
--header 'Content-Type: multipart/form-data' \
--data-raw '{
	"apiId": "{Own API Id}",
	"apiKey": "{Own API Key}",
	"originalText": "How about pork belly for dinner tomorrow?",
	"numReturnSequences": "3",
	"targetStyle": "bible"
}’			
</pre>
                    </div>

                    <p class="sub_txt">④ Response parameters</p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tbody>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>Status of API request</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>payload</td>
                            <td>Converted sentence array</td>
                            <td>string</td>
                        </tr>
                        </tbody>
                    </table>

                    <span class="table_tit">message: API operation</span>
                    <table>
                        <tbody>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>String describing the request processing status (Success/Fail)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>Status code for request processing status (0: Success)</td>
                            <td>int</td>
                        </tr>
                        </tbody>
                    </table>

                    <p class="sub_txt">⑤ Response example</p>

                    <div class="code_box">
<pre>
{
	"message": {
		"message": "Success",
		"status": 0
	},
	"payload": [
		"내일 저녁으로 삼겹살을 드시겠습니까",
		"내일 저녁으로 삼겹살을 드시겠습니까?",
		"내일 저녁으로 삼겹살을 먹는 것이 어떠하뇨"
	]
}			
</pre>
                    </div>
                    <!-- Upload END-->
                </div>
            </div>
        </div>
        <!--//conv_menu-->

        <!--.conv_example-->
        <div class="demobox" id="txtStyTrans_example">
            <p><em style="color:#27c1c1;font-weight: 400;">Use Cases</em></p>
            <span class="sub">Find out how AI can be applied to diverse area.</span>

            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>YouTube Creator</span>
                            </dt>
                            <dd class="txt">Immediately reflect the content style by converting the content style to
                                suit the video concept such as YouTube or Vlog.
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_tst"><span>Text Style Transfer</span></li>
                                    <li class="ico_tts"><span>TTS</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>Virtual Assistant</span>
                            </dt>
                            <dd class="txt">It is used together with an avatar to help you use the tone that fits your
                                avatar concept.
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_tst"><span>Text Style Transfer</span></li>
                                    <li class="ico_lipSyncAvt"><span>Lip-Sync Avatar</span></li>
                                    <li class="ico_bot"><span>Chatbot</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>Personal Blog</span>
                            </dt>
                            <dd class="txt">Various texts are converted into styles that suit individuality and used in
                                personal blogs.
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_tst"><span>Text Style Transfer</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //conversion -->
        </div>
        <!--//.conv_example-->
    </div>
</div>
<!-- //.contents -->

<!---------------------- Local Resources ---------------------->
<!-- Local Script -->
<script type="text/javascript">

    let ajaxXHR = null;

    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {

            initPage();

            $('.iptTxt').on('focus', function () {
                $('#radio3').trigger('click');
            });

            $('.iptTxt').keyup(function (e) {
                var content = $(this).val();
                $('.txtCount span').html(content.length);

                if (content.length > 80) {
                    $(this).val(content.substring(0, 80));
                    $('.txtCount span').html(80);
                }
            });

            $('#btnTransfer').on('click', function () {

                let sentence;
                let txtStyle = $('input[type=radio][name=txtStyle]:checked').val();
                let numReturnSequences = $('#returnSequence option:selected').val();

                if ($('input[type=radio][name=sentence]:checked').val() == 'user-input') {   // 사용자 입력 문장
                    let content = $('.iptTxt').val();
                    if (content.length < 5) {
                        alert('Input at least five characters.');
                        return false;
                    } else {
                        sentence = content;
                    }
                } else {                                                                    // 예시 문장
                    sentence = $('input[type=radio][name=sentence]:checked').val();
                }

                let formData = new FormData();
                formData.append("originalText", sentence);
                formData.append("targetStyle", txtStyle);
                formData.append("numReturnSequences", numReturnSequences);
                formData.append($("#key").val(), $("#value").val());

                sendApiRequest(formData);

                $('.step01').hide();
                $('.loading_wrap').show();
            });

            $('.btn_back1').on('click', function () {
                ajaxXHR.abort();
                $('.loading_wrap').hide();
                $('.step01').show();
            });
            $('.btn_reset').on('click', function () {
                $('.step02').hide();
                $('.step01').show();
            });
        });
    });

    function sendApiRequest(formData) {
        ajaxXHR = $.ajax({
            type: 'POST',
            async: true,
            url: '/api/styleTransfer/styleTransferApi',
            data: formData,
            processData: false,
            contentType: false,
            error: function (error) {
                if (error.status === 0) {
                    return false;
                }
                alert("Failed to get response from server.\nPlease try again");
                console.dir(error);
                window.location.reload();
            },
            success: function (response) {
                // console.log(response);

                if(response.code === 200) {

                    let strToList = response.data;
                    let resultText = "";
                    for(let i=0 ; i<strToList.length ; i++) {
                        const tempHtml = "<p>" + strToList[i] + "</p>";
                        resultText = resultText + tempHtml;
                    }

                    $('.loading_wrap').hide();
                    $('.step02').show();
                    $('#inputSentence').text(formData.get("originalText"));
                    $('#resultTextarea').html(resultText);
                } else {
                    alert("Failed to get response from server.\nPlease try again");
                    window.location.reload();
                }
            }
        });
    }

    function initPage() {
        $('.iptTxt').val("");
        $('#inputSentence').text();
        $('#resultTextarea').html("");
    }

    // API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace("active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    document.getElementById("defaultOpen").click();
</script>