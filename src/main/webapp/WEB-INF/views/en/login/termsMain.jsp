<%--
  Created by IntelliJ IDEA.
  User: bliss
  Date: 2020-03-12
  Time: 오후 4:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<%--  CSS/JS 파일 캐싱 방지 --%>
<%

    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>


<!DOCTYPE html>
<html lang="ko">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">

    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">

    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/reset.css?ver=<%=fmt.format(lastModifiedStyle)%>" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/font.css?ver=<%=fmt.format(lastModifiedStyle)%>" />
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/login_renew.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/common/css/all.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/jquery-1.11.0.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/common.js"></script>

    <title>Terms & Conditions</title>
</head>
<body>
<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->
<%--<div class="common_header">--%>
<%--    <div class="header_box">--%>
<%--        <h1><a href="/?lang=en"><img src="${pageContext.request.contextPath}/aiaas/common/images/maumai.svg" alt="maum.ai logo"></a>--%>
<%--            &lt;%&ndash;					<span>Make your own AI Service</span>&ndash;%&gt;--%>
<%--        </h1>--%>
<%--        <!--.sta-->--%>
<%--        <div class="sta">--%>
<%--            <a href="/home/pricingPage?lang=en" class="go_price">Pricing <span>Plan</span></a>--%>
<%--            <a class="btn_sign" href="javascript:login()">maumAI Login</a>--%>
<%--            <!--.etcMenu-->--%>
<%--            <div class="etcMenu">--%>
<%--                <ul>--%>
<%--                    <li class="lang">--%>
<%--                        <p class="lang_select">English<em class="fas fa-chevron-down"></em></p>--%>
<%--                        <ul class="lst">--%>
<%--                            <li><a href="/?lang=ko" target="_self"><em>한국어</em></a></li>--%>
<%--                        </ul>--%>
<%--                    </li>--%>
<%--                </ul>--%>
<%--            </div>--%>
<%--            <!--//.etcMenu-->--%>
<%--        </div>--%>
<%--        <!--//.sta-->--%>
<%--    </div>--%>
<%--</div>--%>
<!-- #container -->
<div id="container" class="cont_term">
    <!-- .contents -->
    <div class="contents">
        <!--content-->
        <div class="content">

            <div class="demobox">
                <!--.terms_box-->
                <div class="terms_box">
                    <h1>Terms &amp; Conditions</h1>
                    <span>CHAPTER I GENERAL PROVISIONS </span><br>
                    <strong>Article 1 (Purpose)</strong><br>
                    These Terms and Conditions aim to prescribe the terms of use, the procedures for use, the matters related to fees, and all other matters required for the use of “maum AI” service (hereinafter the “Service”) provided by Minds Lab Inc. (hereinafter the “Company”).<br><br>

                    <strong>Article 2 (Effectiveness and Amendment of the Terms and Conditions)</strong><br>
                    ① These Terms and Conditions are posted and publicly announced on the Service’s website (maum.ai), and shall take effect upon the execution of the Service use agreement between the Company and the Customer who agree to these Terms and Conditions pursuant to Article 5 hereof.<br>
                    ② The Company may amend these Terms and Conditions when it deems necessary, and may change or suspend all or any part of the Service when such is required by law or an order, direction or recommendation of any governmental authorities, or any judgement of a court, or any change in the relevant policies of the Company and other separate business. In such case, the Company shall notify such amendment, change or suspension in accordance with Paragraph (1) above by no later than seven (7) days before the effective date thereof, and if the Company makes any amendment to these Terms and Conditions that is adverse to the users, the Company shall notify such amendment with at least thirty (30) days grace period.<br>
                    ③ If the Company notifies its amendment of these Terms and Conditions in accordance with Paragraph (2) above, and also clearly notifies the Customer that he/she will be deemed to have agreed if he/she fails to raise any objection to the Company until the scheduled effective date, and in spite of the Company’s such notice, if the Customer fails to expressly object, then the Customer shall be deemed to have agreed to such amendment of these Terms and Conditions.<br>
                    ④ If the Customer does not agree to the amended Terms and Conditions, he/she may terminate the Service use agreement.<br><br>

                    <strong>Article 3 (Other Rules)</strong><br>
                    Any matters that are not set forth in these Terms and Conditions shall be governed by the relevant laws and regulations, and separate terms and conditions for the additional services, regulations on use or detailed usage guidelines that are provided by the Company.<br><br>

                    <strong>Article 4 (Definitions)</strong><br>
                    As used in these Terms and Conditions, the following capitalized terms shall have the respective meanings defined below:<br>
                    ① “Customer”: An individual or a legal entity that applies to the Company for the use of the Service and executes the Service use agreement with the Company.<br>
                    ② “Service”: Korean and English Speech-to-Text (STT) services, customized machine (AI) voice generator services utilizing voice recording and synthesis technologies, voice/language/dialogue/visual recognition artificial intelligence API services, on-line chat consultation services, and cloud services providing on-line chat consultation on the websites or the applications (each referred to as the “Service” and collectively as the “Services”).<br>
                    ③ “Recurring Service Plan”: The Company’s each differentiated recurring service plan composed of different Service(s), limited or unlimited usage amount(s) and other additional service(s), the applicable Service fee for which is charged to and paid by the Customer on a recurring basis every 30 days after the initial payment thereof. Once the applicable Service fee for the Recurring Service Plan is processed, such fee is non-refundable and the Customer can use the Recurring Service Plan for 30 days, which is a mandatory usage period.<br>
                    ④ “Person in Charge of Payment”: A person primarily responsible for the payment of the Service fee for the Service provided under these Terms and Conditions.<br>
                    ⑤ “Google Email Account”: Google e-mail account entered by the Customer, which is for the identification of the Customer and the use of the Service by Customer.<br>
                    ⑥ “Password”: A combination of characters and numbers created by the Customer to protect his/her information.<br>
                    ⑦ “Spam Mail”: An electronic mail advertisement sent in bulk to the recipient against his/her intention to reject the receipt thereof or without his/her consent for receipt thereof.<br><br>

                    <span>CHAPTER II SERVICE USE AGREEMENT</span><br>
                    <strong>Article 5 (Execution of the Agreement)</strong><br>
                    The Service use agreement is executed between the Company and the Customer when the Customer desiring to use the Service applies for such use by filling out and submitting the prescribed application form to the Company or entering the required information in the online form pursuant to the application procedure of the Service website (maum.ai), and agrees to these Terms and Conditions, and subsequently when the Company approves such application for use.<br><br>

                    <strong>Article 6 (Application for Use)</strong><br>
                    ① The Company will notify these Terms and Conditions to the Customer who wishes to use the Service, and will handle the Customer’s application for use of the Service upon obtaining the Customer’s consent to these Terms and Conditions.<br>
                    ② The Customer shall submit to the Company each of the following documents in person or by fax, online or other means that is acceptable to the Company.
                    &nbsp; &nbsp;&nbsp; &nbsp;1. Application for use of the Service (prescribed form)<br>
                    ③ If the Customer who wishes to apply for the Service is a minor or quasi-incompetent person, then such Customer shall apply for the Services indicating his/her parent or legal representative as the applicant upon obtaining consent from his/her parent or legal representative.<br>
                    ④ If the Customer has applied for use of the Service by illegally using a third party’s name, such Customer’s use of the Service will be suspended, and he/she may be subject to penalty or punishment pursuant to the applicable laws and regulations.<br><br>

                    <strong>Article 7 (Approval of Application for Use)</strong><br>
                    ① Unless there is any special business or technical reason, the Company will approve the Customers’ applications for the Service in the order in which they were received.<br>
                    ② After its approval of the application for use of the Service, the Company will notify within three (3) business days each of the following matters to the Customer who applied for use of the Service by phone, e-mail or other means.<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;1. Expected date of opening the Service;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;2. Matters related to the fees;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;3. Matters related to the protection of the Customer’s rights and interests, and the Customer’s obligations; and4. Any other matters deemed to be important or necessary in connection with the use of the Service.<br>
                    ③ Notwithstanding anything to the contrary in Paragraph (2) above, with respect to any Service that can be applied and opened for use via website, the Company may omit such notice to the Customer that is required under Paragraph (2) above, and may notify the Customer of the fact that the opening of the Service is completed and the Customer’s unique numeric identifier after the completion of the Service opening.
                    <br><br>
                    <strong>Article 8 (Limitation on Approval of Application for Use)</strong><br>
                    ① The Company may reject its approval of the Customer’s application for use of the Service in any of the following events:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;1. If the Customer has applied for use of the Service under any third party’s name or false name; <br>
                    &nbsp; &nbsp;&nbsp; &nbsp;2. If the Customer has entered or filled in false information;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;3. If any record of the Customer’s non-payment is registered with the Credit Information Collection Agency, including the Korea Federation of Banks;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;4. If there is any likelihood of violating any applicable laws and regulations, or harming public peace and order and fine customs;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;5. If the Customer uses unlawful means against the Service, such as utilizing malware and bugs or abusing any weakness of the Service system;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;6. If the Customer uses or intends to use the Service for any unlawful purpose;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;7. If the Customer who is a minor has applied for use of the Service without consent of his/her parent or legal representative;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;8. If the Customer’s application was rejected before in accordance with these Terms and Conditions, except when the Customer obtained re-approval from the Company after the Company’s examination; or<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;9. Any other events where the approval of the Customer’s application is difficult due to any cause attributed to the Customer.<br>
                    ② Company may withhold its approval of the Customer’s application for use of the Service in any of the following events:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;1. If there is any equipment failure due to a natural disaster;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;2. If the Company deems that it is difficult to maintain the quality of the Company’s overall services due to its provision of the Service;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;3. If the Customer fails to pay the fees in a timely manner;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;4. If there is not enough equipment to support the provision of the Service or it is difficult to provide the Service for technical reason;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;5. If there is any element or likelihood of harm or injury to the Company’s business or operation; <br>
                    &nbsp; &nbsp;&nbsp; &nbsp;6. If there is any likelihood of distribution or display of pornographic or treasonable materials, or if there is any infringement, or likelihood of infringement, of any third party’s copyright;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;7. If there is any infringement, or likelihood of infringement, of any third party’s rights or privacy by means such as recording such third party’s voice without his/her consent;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;8. If the Customer re-applies or changes the Service use on the same day when the Service terminates or changes; or<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;9. If it is difficult for the Company to approve the use of the Service due to the Company’s other circumstances.<br>
                    ③ After the execution of the use agreement, the Company may revoke its approval for the use of the Service if any of the events or matters set forth under Paragraphs (1) and (2) above occurs.  <br>
                    ④ If the Company rejects, withholds or revokes its approval pursuant to Paragraphs (1), (2) or (3) above, the Company shall immediately notify the Customer, who applied for use of the Service, of such rejection, withholding or revocation by contacting the Customer with the contact information provided by the Customer. If such notification was not given to the Customer due to incorrect contact information provided by the Customer, the Company shall not be responsible for such failure, and the Customer has the obligation to confirm by inquiring with the Company.
                    <br><br>

                    <strong>Article 9 (Customer’s Right to Withdraw Application for Paid Service)</strong><br>
                    Any Customer who purchased any paid Service from the Company may withdraw his/her application for such paid Service within 7 days of his/her purchase; provided, however, that the Customer's right to withdraw his/her application for certain paid Service may be restricted in any of the following cases: <br>
                    &nbsp; &nbsp;&nbsp; &nbsp;1. If the Company has disclosed in the labelling or information of the paid Service that the application for that paid Service cannot be withdrawn;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;2. If the Company has provided a trial product;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;3. If the Company has made available the means for temporary or partial use<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;4. If the provision of the paid Service has already commenced.<br><br>

                    <strong>Article 10 (Amendment of Agreement)</strong><br>
                    ①If any of the following occurs, the Customer shall immediately submit to the Company the application for the amendment of agreement or notify the Company by changing and submitting the relevant information on-line:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;1. Any change to the tradename, name, address or contact information of the Customer and the Person in Charge of Payment;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;2. Any change to information of the Customer;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;3. Any change in the details of the Service (type of agreement(Recurring Service Plan, etc.), term, etc.); or<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;4. Any change to the fee payment methods.<br>
                    ② In case of the Recurring Service Plan, the Customer may not apply for additional use of the same or another service plan during the service period of such Recurring Service Plan, and may only request for change to a different Recurring Service Plan. When such request is made by the Customer, the requested Recurring Service Plan may be implemented immediately or upon expiration of the mandatory usage period of the then-current Recurring Service Plan.<br>
                    ③ The procedures for switching from one Recurring Service Plan to another shall be in accordance with the procedures for applying for the Service, and part of such procedures may be omitted with the Company’s consent.<br><br>

                    <strong>Article 11 (Renewal of Agreement)</strong><br>
                    The Company and the Customer may agree to fix the term of the Service use agreement, and the Service use agreement shall deemed to have automatically renewed for the same terms and conditions thereof unless the Customer expresses his/her intent to terminate such agreement before the expiration of then effective term.<br><br>

                    <strong>Article 12 (Succession of Customer’s Rights and Obligations)</strong><br>
                    Any Customer who desires to succeed to the right to use the Service by inheritance, merger, split, sale and purchase of business or for any other reasons the Company deems necessary shall apply for use of the Service by submitting to the Company a copy of its business registration certificate, attached thereto any relevant document evidencing its succession of right to use the Service.
                    <br><br>

                    <strong>Article 13 (Termination of Agreement and Termination Notice)</strong><br>
                    ① If the Customer desires to terminate the agreement, such Customer shall request termination to the Company via on-line, fax, e-mail, etc.; provided that, if the Customer requests termination of the Recurring Service Plan, it shall be terminated only upon the expiration of its then currently active mandatory usage period. The Company may request termination documents from the Customer in order to confirm his/her termination request prior to removing Customer's information and materials from the System in accordance with the Customer's termination request.<br>
                    ② The Company may, at its own discretion, terminate the agreement in any of the following events:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;1. If the Customer used any third party’s name or attached any false document;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;2. If the Customer refuses or hinders the verification of his/her actual use of the Service without any justifiable reason and thereby conceals any breach of these Terms and Conditions;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;3. If the Customer fails to resolve any reason for the suspension of the use of the Service within one (1) month from the date of commencement of such suspension;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;4. If the Customer does not request for the resumption of the Service within one (1) month from the expiration date of the temporary suspension of the Service;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;5. If the Customer infringes upon the intellectual property rights of the Company, other Customers or any third parties;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;6. If the Customer infringes upon any third party’s rights or privacy by means such as recording such third party’s voice without his/her consent;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;7. If the Customer causes serious failure, interruption or hindrance to, or intentionally obstructs, the Company’s operation of the system;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;8. If the Customer breaches his/her obligations provided for in these Terms and Conditions, including Article 23 hereof;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;9. If the Customer unlawfully uses any third party’s user ID, password or any other personal data;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;10. If the Customer inappropriately uses, accesses, copies, distributes or installs any program provided as the Service for other purposes;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;11. If the Customer duplicates or distributes, or commercially uses, any information and materials obtained by using the Service information without the Company’s prior approval;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;12. If the Customer violates any applicable laws and regulations related to telecommunication;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;13. If the Customer fails to resolve his/her default on payment obligations within five (5) days after the suspension of the Service due to his/her failure to pay the fees in a timely manner;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;14. If the Customer’s use of the Service was limited or suspended for twice or more on any pertinent year; or<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;15. If the Customer breaches these Terms and Conditions and any other conditions for use as determined by the Company.<br>
                    ③ If the Company desires to terminate the agreement under this Article, the Company shall notify the Customer at least seven (7) days in advance through the contact information provided by the Customer, and if such notification cannot be given to the Customer due to incorrect contact information provided by the Customer, the Company will post such notification on the Service website (maum.ai) for fourteen (14) days and thereby it will be deemed to have discharged its termination notice obligations.<br>
                    ④ If the Service use agreement is terminated pursuant to Paragraphs (1) and (2) of this Article, all resources allocated to the Customer’s system shall be retrieved and all information and materials of the Customer saved in the system shall be deleted, and the Customer may not claim for damages due to such retrieval and deletion.<br>
                    ⑤ If the Customer does not state his/her opinion at least three (3) days before the expected termination date in spite of termination notice under Paragraph (3) of this Article, the Customer shall be deemed to have no objection to such termination.<br><br>

                    <span>CHAPTER III OPENING AND USE OF SERVICE </span><br>
                    <strong>Article 14 (Opening of Service)</strong><br>
                    ① Unless there is any special reasons, the Company shall open the Service on the expected date of opening the Service under Paragraph (2) of Article 7. If the Company fails to open the Service on the expected date of opening the Service as notified, the Company shall immediately notify the Customer in writing (e-mail) or by phone(SMS), etc. of the reason for such failure and newly designated expected date of opening the Service.<br>
                    ② If the Customer who applied for use of the Service desires to postpone the opening date of the Service, such Customer shall notify thereof to the Company at least two (2) days before the expected date of opening the Service.<br>
                    ③ The Company shall notify the Customer that the opening of the Service is completed after it ensures that the Customer who applied for use of the Service can normally use the Service.<br><br>

                    <strong>Article 15 (Use of Service)</strong><br>
                    ① The Customer may use the Service from the time when the Company approves his/her application for use of the Service; provided, however, that the Customer may use the paid Service only after the full payment of the applicable Service fee. Unless there is any special reason, in principle, the Service use is available 24 hours a day, 365 days a year..<br>
                    ② Any date or time when the Service is not provided upon agreement between the Company and the Customer, and any date or time designated by the Company for routine inspection etc. shall be exceptions to the Service hours stated in Paragraph (1) above.<br><br>

                    <strong>Article 16 (Management of Customer’s ID)</strong><br>
                    ① In principle, the Google Email Account entered by the Customer when he/she applied for membership cannot be changed, and if the Customer desires to change it due to compelling circumstances, he/she must terminate the relevant ID and re-apply for membership.<br>
                    ② The Customer shall be responsible for the management and use of his/her Google Email Account, and shall be also responsible for any and all disadvantage that may occur due to any negligent use thereof or any third party’s unlawful use thereof.<br><br>

                    <strong>Article 17 (Limitation or Suspension of Use of the Service)</strong><br>
                    ① If there is any interruption in the use of the Service due to a national emergency, Service equipment failure, traffic surge in the use of the Service, key telecommunication business operator’s service failure or interruption, etc., the Company may limit or suspend all or part of the Service.<br>
                    ② The Company may limit or suspend all or part of the Service if any of the following events occur in connection with the Customer’s use of the Service, and may terminate the Service use agreement if such event is not resolved and continues to subsist:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;1. If the Customer fails to perform his/her obligations set forth in Article 23;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;2. If the Customer receives or provides any materials, which contravene the applicable laws, through the on-line chat consultation;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;3. If the Customer discloses any content or provides any service to any third party that is in violation of the Company’s policies regarding the Service operation as announced on the Service website;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;4. If the maintenance of the Service use agreement is deemed to be difficult due to the Customer’s insolvency, bankruptcy, application for reorganization, application for provisional measures, or any attachment, preliminary attachment, etc. on the Customer’s major assets;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;5. If the Customer infringes upon the intellectual property rights of the Company, other Customers or any third parties;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;6. If the Customer infringes upon any third party’s rights or privacy by means such as recording such third party’s voice without his/her consent;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;7. If there is a correction request is issued by an external agency such as the Information Communication Ethics Committee, or an authoritative interpretation is issued by the National Election Commission in connection with any illegal election campaign;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;8. If the Customer unlawfully uses any third party’s ID, password, and information of any legal entity or individual;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;9. If the Customer causes serious failure, interruption or hindrance to, or intentionally penetrates or obstructs, the system operation or network securities, etc.;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;10. If the Customer resells to, shares with (including any act of allowing access or use the Service or the system) or distributes to any third party the Service or any part thereof without the Company’s approval;<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;11. If the Customer fails to pay the Service fee for 30 days or more from the payment date; or<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;12. If the Customer violates any other applicable laws and regulations, these Terms and Conditions or any other conditions for use as determined by the Company.
                    <br><br>

                    <strong>Article 18 (Procedures for Limitation or Suspension of Use of the Service, and Means to Remove Limitation or Suspension of Use of the Service)</strong><br>
                    ① If the Company desires to limit or suspend the Customer’s use of the Service pursuant to the provisions set forth in Article 17, the Company shall notify such Customer or its agent 24 hours in advance of the reason, date and time, and period of such limitation or suspension via email, phone or other means; provided, however, that the Company may not provide such notice if it deems that the use of the Service needs to be urgently limited or suspended.<br>
                    ② If the Customer who has been notified of the limitation or suspension of his/her use of the Service pursuant to Paragraph (1) above may raise any objection he/she may have with respect to such limitation or suspension, and in such case, the Company may postpone its Service use limitation or suspension for a period it needs to confirm such objection.<br>
                    ③ During the period of Service use limitation or suspension, if the Company confirms that the reason for such limitation or suspension is resolved, the Company shall immediately remove such limitation or suspension and notify the corresponding Customer.<br>
                    ④ If the Service is interrupted by the Service use suspension, the Company may retrieve the Service account(or system) allocated to the corresponding Customer. The retrieval of the Service account is carried out by the Company at its own discretion after three (3) days from the Service use suspension, and the Customer may not claim for damages incurred due to such retrieval.<br><br>

                    <strong>Article 19 (Temporary Suspension of Service)</strong><br>
                    If the Company is unable to provide the Service due to unavoidable circumstances such as improvement in system, additional installation of equipment, routine inspection, management and operation of the facilities, etc., the Company may temporarily suspend the Service use by providing advance notice to the Customer, and shall immediately make the Service available to the Customer when the reason for such temporary suspension is resolved.<br><br>

                    <span>CHAPTER IV MAINTENANCE AND REPAIR OF SERVICE AND HANDLING OF SERVICE FAILURE </span><br>
                    <strong>Article 20 (Maintenance and Repair of Servers and Other Equipment)</strong><br>
                    ① The Company is responsible for maintaining and repairing the equipment that is essential for providing the Service to the Customer, including such as the server equipment, at all times and at an adequate level to allow the Customer’s use of the Service.<br>
                    ② The Customer shall take measures necessary for the system security (software patch work, security measures, etc.)<br><br>

                    <strong>Article 21 (Security Emergency)</strong><br>
                    ① If there is any program defect or failure that poses serious security issue or any other equivalent events, the Company may work en bloc on software patch with respect to such issue.<br>
                    ② If there is any serious and urgent security issue, the Company may exigently change the Customer’s information related to the Customer’s authentication.<br>
                    ③ Before taking emergency measures under Paragraphs (1) and (2) above, the Company shall notify the Customer of such measures by notice or e-mail. If it is difficult to notify the Customer in advance due to exigent circumstance, the Company shall notify the Customer ex post by notice or e-mail.<br><br>

                    <span>CHAPTER V OBLIGATIONS OF EACH PARTY TO THE AGREEMENT</span><br>
                    <strong>Article 22 (Company’s Obligations)</strong><br>
                    ① Unless there is any special reason, the Company shall make the Service available to the Customer who applied for use of such Service on the Customer’s desired opening date or within a period designated under these Terms and Conditions, and the Company has the obligation to provide uninterrupted and stable Service to the Customer; provided, however, that if the Service cannot be opened on the Customer’s desired opening date, the Company shall notify of such fact to the Customer prior to the desired opening date by phone, e-mail, mail or other means.<br>
                    ② Pursuant to the Act on Promotion of Information and Communications Network Utilization and Information Protection, the Company may not disclose or distribute to any third party, or use for commercial purposes, the Customer’s information it obtained in connection with the provision of the Service without the Customer’s prior consent; except when such disclosure is required by any relevant agency for the purpose of any investigation pursuant to the applicable laws and regulations, or is requested by the Information Communication Ethics Committee, or when the Company provides such information to any credit information business operator or credit information collection agency by obtaining the Customer’s consent due to the Customer’s failure to pay the Service fee within a certain period designated by the Company.<br>
                    ③ The Company shall endeavor to provide the utmost convenience to the Customer that is needed for the procedures and contents related to the agreement with the Customer, such as the execution of the Service use agreement, the amendment and termination of the Service use agreement, etc.<br>
                    ④ The Company shall immediately handle any comment or complaint raised by the Customer if it deems such comment or compliant is reasonable; provided, however, that if it is difficult to handle such comment or complaint immediately, the Company shall notify to the Customer the reason thereof and the expected schedule for handling such comment or complaint by e-mail, phone, bulletin board, etc.<br><br>

                    <strong>Article 23 (Customer’s Obligations)</strong><br>
                    ① The Customer shall comply with these Terms and Conditions and all matters prescribed by the applicable laws and regulations, and may not cause any serious interruption or failure to the Company’s performance of its business.<br>
                    ② In consideration of the Service use, the Customer has the obligation to pay the fee set forth under these Terms and Conditions on the designated payment date, and the Person in Charge of Payment shall be jointly and severally liable with respect to such payment obligation.<br>
                    ③ The Customer shall be responsible for any and all issues that may occur due to the non-payment of the Service fee; except when such non-payment is due to the Company’s negligence or any reason acknowledged by the Company.<br>
                    ④ The Customer shall protect his/her information against any penetration by virus programs or any illegal penetration from the outside through the account used by the Customer.<br>
                    ⑤ If any agreement details of the Customer, such as the address, contact information, business registration number, etc., changes, then the Customer shall immediately notify such change to the Company, and the Customer shall be responsible for any disadvantage that may occur due to the Customer’s failure to notify the Company of such change.<br>
                    ⑥ The Customer may proceed with any act that can be carried out at his/her own will, such as transfer of account or domain to a third party business, without the Company’s approval; provided, however, that the Company’s approval is required if the Customer fails to fully perform his/her obligations provided for in these Terms and Conditions, including any failure to pay the installation fee and Service fee or if the Customer desires to take out any solution or service provided by the Company.<br>
                    ⑦ The Customer shall be solely responsible for the management and preservation of the his/her information and materials in connection with the Service. For that, the Customer shall by itself utilize encryption methods with respect to his/her information and materials as needed, and shall regularly backup his/her information and materials to an independent and separate storage and apply up-to-date security patch or update at all times.<br>
                    ⑧ If the Customer provides a separate service to other customers or any third parties by utilizing the Service, then any dispute in connection with any and all information and materials related to such separate service (e.g., data files, document texts, computer software, music, audio file or any other sound, photos, videos or other images, etc.) shall be the responsibility of the Customer who provides such separate service or any person from whom such information or materials originated, and the Customer shall indemnify and hold the Company harmless from and against any claim that may be raised against the Company due to such certain information and materials.<br><br>

                    <span>CHAPTER VI SERVICE FEES</span><br>
                    <strong>Article 24 (Types of Fees)</strong><br>
                    ① In connection with the use of the Service, the Customer must pay the applicable Service fee by credit card on a recurring basis every 30 days. <br>
                    ② The details and changes regarding the fees are posted on the Service website..<br><br>

                    <strong>Article 25 (Calculation of Fees)</strong><br>
                    ① The Service fee for the Recurring Service Plan shall be the applicable fee calculated for a period of 30 days from the purchase date thereof.<br>
                    ② Under Paragraph (1) above, the Service fee shall be calculated for the entire period of 30 days even if the number of days used is less than 30 days, and if the Recurring Service Plan is changed to a different Recurring Service Plan, any Service fee already paid for the former Recurring Service Plan shall be non-refundable even if the latter Recurring Service Plan is immediately implemented.<br>
                    ③ The fee during the Service’s temporary suspension period shall be governed by the provisions of Article 18 of these Terms and Conditions.<br>
                    ④ All fees are exclusive of any value added taxes.
                    <br><br>

                    <strong>Article 26 (Payment Methods)</strong><br>
                    ① The Company charges the Service fee by credit card payment upon use of the Service, and the Customer shall pay such Service fee with a valid credit card.<br>
                    ② If the Person in Charge of Payment is a governmental organization, local government entity, foreign government agency located in Korea or any other entity acknowledged by the Company, then the centralized payment method (where a higher level agency or principal business operator, etc. pays en bloc) is acceptable to the Company.<br>
                    ③ If the Customer pays the Service fee under a third party’s name, the Customer has an obligation to notify thereof to the Company. The Customer shall be responsible for any Service suspension or disadvantage that may occur due to the Customer’s failure to notify the Company that the Service fee was paid under a third party’s name.
                    <br><br>

                    <strong>Article 27 (Payment Obligations)</strong><br>
                    ① Upon being charged for the Service fee, the Person in Charge of Payment shall pay the charged Service fee until the payment date and by the payment method designated by the Company.<br>
                    ② In principle, the Customer who enters into the Service use agreement shall bear the obligation to pay the Service fee.<br><br>

                    <strong>Article 28 (Objection to Fees)</strong><br>
                    ① If the Person in Charge of Payment has any objection to the Service fee charged, such objection may be raised within three (3) months from the billing date.<br>
                    ② Upon receipt of the objection under Paragraph (1) above, the Company shall notify the Customer of the result of its handling of the objection raised under Paragraph (1) above within ten (10) days after its receipt thereof, and if the Company fails to handle such objection within the said period due to compelling circumstances, the Company shall re-designate a period for handling such objection and notify the Person in Charge of Payment or the Customer.<br><br>

                    <strong>Article 29 (Reduction and Discount of Fees)</strong><br>
                    ① The Company may reduce or discount part of the initial costs or monthly Service fee with respect to the Customer or reseller, etc., with whom the Company had a separate discussion, and in such case, the means to apply such reduction or discount shall be determined by agreement between the Company and the Customer.<br>
                    ② A person who is qualified for the reduction or discount of fee shall notify the Company when his/her qualification changes.<br>
                    ③ If it is confirmed that a person who is not qualified for the reduction or discount of fee had his/her fee reduced or discounted, such person shall pay the reduced or discounted amount.<br><br>

                    <strong>Article 30 (Collection of Unpaid Fees)</strong><br>
                    ① If the Person in Charge of Payment fails to pay the fee in a timely manner, the Company may immediately upon such default demand payment, and in such case, the Company may issue a payment demand letter and re-designate the payment date.<br>
                    ② The payment demand under Paragraph (1) above shall be made within six (6) months from the original payment date, and if the Company fails to make payment demand within such period, the Company will not make any disposition for non-payment; except when Company’s such failure is due to a natural disaster or cause attributed to the Customer or the Person in Charge of Payment.<br>
                    ③ If the Person in Charge of Payment fails to pay the fee in a timely manner, the Company shall collect the unpaid fee, plus the amount equal to 2/100 (per month) of the unpaid fee calculated as of the day immediately following the original payment date.<br><br>

                    <strong>Article 31 (Collection of Evaded Fees)</strong><br>
                    If the Person in Charge of Payment evades the fee payment in violation of the applicable laws and regulations and these Terms and Conditions, the Company shall collect the amount equal to twice the evaded amount.<br><br>

                    <strong>Article 32 (Refund of Fees)</strong><br>
                    ① If the Customer withdraws his/her application in accordance with Article 9, the Company shall refund to the Customer within 3 business days any fees already paid.<br>
                    ② If the Person in Charge of Payment pays the fee in excess or makes any mistake in payment, the Company shall refund such amount paid in excess or under mistake, or adjust the subsequent fee accordingly.<br>
                    ③ If the Person in Charge of Payment who is entitled to a refund under Paragraph (1) above has also failed to pay the fee in a timely manner, the Company shall deduct such unpaid fee first from the refundable amount, and thereafter refund the remaining balance of the refundable amount, if any.<br><br>

                    <span>CHAPTER VII COMPENSATION FOR DAMAGES </span><br>
                    <strong>Article 33 (Scope of Liability)</strong><br>
                    ① If any interruption or failure occurs due to a cause attributed to the Company, and the Company fails to reach the monthly availability rate (as defined below) or the Customer suffers any damages due to such interruption or failure, the Company shall compensate the damages pursuant to Paragraph (2) below only when the Customer claims for damages.<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;* Failure Period: Measures the period from the time when the Customer notifies to the Company the fact that he/she was not able to use the Service (if the Company becomes aware of such fact before the Customer’s notice, then from the time when the Company became aware thereof) until the time when the Company resolved such interruption or failure and completed its failovers (despite the Company’s completion of its failover, if the interruption or failure is prolonged due to the Customer’s delay in taking additional measures, such prolonged period is not included in the Failure Period)<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;* Basis for calculating the damages * Monthly Availability Rate(%) = 100 x [1 – {the total failure time (minutes) when the Customer was not able to use the Service due to an interruption or failure attributed to the Company during a month use of the Service / a month use of the Service (minutes)}]<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;* Basis for calculating the damages: the monthly average damages claimed by the corresponding Customer for the most recent three (3) months (if less than 3 months, then such period shall apply)<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;Availability Rate	&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;Damages<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;Less than 99.9% and 99.5% or more	&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;10% of the Service fee<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;Less than 99.5% and 99.0% or more&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;20% of the Service fee<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;Less than 99.0% &nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; 30% of the Service fee<br>
                    ③ The Company shall not compensate the Customer for any damages if the Service interruption or failure is caused by any of the following reasons:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;1. A defect in any device, equipment, circuit, etc. purchased directly by the Customer;:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;2. A force majeure event, such as war, upheaval, natural disaster or any other equivalent national emergency, national or local accident, power outage, network failure, etc.;:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;3. Any cause attributed to the Customer or a third party or any loss of the Customer’s information or materials due to a cause attributed to the Customer or a third party;:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;4. An unavoidable reason due to the nature of the telecommunication service;:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;5. A suspension of the Service in order to prevent the spreading of the accident that occurred in the Customer’s information system;:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;6. A service provided by a third party communication business operator;:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;7. An inspection is inevitably required or a routine inspection is required and such has been notified in advance;:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;8. An invasion accident due to the Service user’s careless management of system security;:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;9. A suspension of the Service in order to prevent the spreading of the accident occurred in the Service user’s system;:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;10. An illegal invasion or penetration from the outside;:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;11. A suspension of the Service in order to prevent the spreading of the accident occurred in the user’s information system; or:<br>
                    &nbsp; &nbsp;&nbsp; &nbsp;12. A use of the service provided to the users for free.:<br>
                    ④ The Company’s liabilities with respect to the Service failure, interruption, etc. shall be limited to the compensation for damages under Paragraph (2) of this Article, and the Company shall not be liable for any additional costs incurred or any loss of expected profit, etc. suffered by the Customer.<br><br>

                    <strong>Article 34 (Compensation Claim for Damages)</strong><br>
                    ① If the Customer claims for damages against the Company, the Customer shall submit the reason for such claim, claimed damages and the basis for the calculation of such damages in writing.<br>
                    ② The right to claim for damages under Paragraph (1) above shall lapse if the Customer fails to exercise such right within six (6) months from the date when the relevant damages has incurred.<br><br>

                    <strong>Article 35 (Compensation Claim for Damages against Customer)</strong><br>
                    If the Customer breaches any term or condition set forth in these Terms and Conditions and thereby the Company compensates a third party or suffers any damages, then the Company may claim for damages (all costs, including attorneys’ fees, etc.) against such Customer.<br><br>

                    <strong>Article 36 (Special Matters)</strong><br>
                    ① If any special arrangement is required for any other services not included in these Terms and Conditions, the Company and the Customer may prepare and execute (affixing their respective signatures, names and seals) a separate service use agreement, and thereby the Company may provide, and the Customer may use, such services.<br>
                    ② If any dispute arises between the Company and the Customer in connection with these Terms and Conditions and the Service use, the Company and the Customer shall endeavor in good faith to resolve such dispute by mutual agreement.
                    <br><br>
                    &#91;ADDENDUM&#93;<br>
                    Article 1 (Effective Date)<br>
                    ⓛ  These Terms and Conditions shall come into effect on February 1, 2019.
                </div>
                <!--//terms_box-->

                <!--terms_box-->
                <div class="terms_box" id="conditions">
                    <h1>Minds Lab Inc. Privacy Policy</h1>

                    Minds Lab Inc. (the “Company”) hereby establishes and publishes our privacy policy in order to protect the personal data of the data subject pursuant to Article 27-2 of the Act on Promotion of Information and Communications Network Utilization and Information Protection and Article 30 of the Personal Information Protection Act, and swiftly and smoothly handle any complaint in connection therewith.<br><br>

                    <span>Article 1 (Purposes for Processing Personal Data)</span> <br>
                    The Company processes personal data for the following purposes. The personal data being processed will not be used for any purposes other than the following purposes, and if the purpose of use changes, the Company shall take necessary measures, such as obtaining separate consent, pursuant to Article 18 of the Personal Information Protection Act.<br><br>

                    1. Website Membership Registration and Management<br>
                    The personal data is processed for the following purposes: the verification of the intent to register for the membership, the identification and authentication of an individual’s identity as per the provision of membership-only service, the maintenance and administration of membership, the verification of an individual’s identity as per the implementation of limited identity verification system, the prevention of unlawful use of the service, the confirmation of consent of a legal representative in processing the personal data of a child under age of 14, various notification, handling of any complaints, etc.<br><br>
                    2. Provision of Goods and Services<br>
                    The personal data is processed for the following purposes: the provision of the service, sending any agreement or invoice, the provision of any customized individual service, the authentication of an individual’s identity, the fee payment and settlement, the collection of any debt, etc.<br><br>
                    3. Handling of Complaints<br>
                    The personal data is processed for the following purposes: the verification of the identity of the person who filed a complaint, the confirmation of the complaint filed, any communication and notification for the factual investigation, the notification of the result of handling the complaint, etc.<br><br>

                    <span>Article 2 (List of Personal Data being Processed) The Company processes the following items of personal data:</span><br>
                    1. Website Membership Registration and Management<br>
                    ․ Compulsory items: Name, e-mail address (Google ID)<br>
                    ․ Optional items: place of work, other information recorded by the user, voice information, content of recording<br><br>

                    2. Provision of Goods and Services<br>
                    ․ Compulsory items: Name, e-mail address(Google ID)<br>
                    ․ Additional compulsory items when the recurring service plan is purchased: i-PIN, payment information such as credit card number, bank account information, PayPal ID for overseas payment, etc.<br>
                    ․ Optional items: interest area, details of purchase history<br><br>

                    3. The following personal data may be automatically created and collected in the course of using the Internet-based services:<br>
                    ․ IP address, cookie, MAC address, service usage record, visit history, poor usage record, etc.<br><br>


                    <span>Article 3 (Processing of Personal Data and Retention Period) </span><br>
                    ① The Company processes and retains the personal data within the retention and usage period pursuant to the applicable laws and regulations or within the retention and usage period for which it obtained the data subject’s consent when collecting the personal data from the data subject.<br>

                    ② The processing and retention period of each personal data is as follows:<br>

                    1. Website Membership Registration and Management : Until the website membership withdrawal of the business operator/organization <br>
                    Provided that, if any of the events occurs, then until such event is concluded:<br>
                    1) If an investigation is underway due to a violation of the applicable laws and regulations, then until such investigation is concluded.<br>
                    2) If any debt remains outstanding as per the use of the website, then until such debt is settled.<br><br>

                    2. Provision of Goods and Services: Until complete provision of the goods and services, and complete payment and settlement of the fees<br>
                    Provided that, in any of the following cases, then until the expiration of the relevant period:<br>
                    1) Records on any transaction, such as marks, advertisements and contents of the contracts and performance thereof, under the Act on the Consumer Protection in Electronic Commerce.<br>
                    - Records on marks and advertisements : 6 months <br>
                    - Records on the execution of contract or revocation of offer, the payment, and the supply of the goods, etc.: 5 years<br>
                    - Records on the consumer’s complaint or the disposition of dispute : 3 years<br>
                    2) Retention of the Communication Confirmation Data under Article 41 of the Protection of Communications Secrets Act.<br>
                    - The date of telecommunication by subscribers, the time when the telecommunication commenced and ended, the subscriber number of the other party, the frequency of use, the data tracking the location of the base station: 1 year<br>
                    - The computer communications, the Internet log records, the data tracking the place of access: 3 months<br><br>

                    <span>Article 4(Entrustment of Personal Data Processing) </span><br>
                    ① Subject to the data subject’s consent, the Company entrusts the processing of personal data as follows for smooth handling of personal data processing:<br>
                    - Recurring payment of service fees for paid services<br>
                    - Entrustee: KG INICIS Co., Ltd. (for domestic payment) / PayPal (for overseas payment)<br>
                    - Entrusted Matter: Processing automatic credit card payment approvals for the recurring payments for paid services, cancelling payments, identifying individuals and verifying real names for the performance of services, verifying payment methods, preventing fraudulent payment transaction<br>
                    ② Pursuant to Article 26 of the Personal Information Protection Act, when entering into an entrustment agreement with an entrustee for the personal data processing, the Company clearly sets forth in the agreement or otherwise in writing the prohibition of processing personal data for purposes other than carrying out the entrusted matters, technical and managerial safeguards of personal data, restriction on any re-entrustment of personal data processing, supervision and overseeing of entrustee, liability for damages, and other responsibilities, and the Company is overseeing the entrustee whether it is processing the entrusted personal data in a safe manner.<br>
                    ③ If there is any change to the substance of the entrusted matters or change of entrustee, the Company will disclose such change without any delay in this Privacy Policy.<br><br>

                    <span>Article 5(Rights and Obligations of Data Subject and the Exercise Methods)</span><br>
                    ① The data subject may at any time exercise towards the Company each of the following rights regarding the personal data protection:<br>
                    1. Request for access to personal data<br>
                    2. Request for correction if there is any error, etc.<br>
                    3. Request for erasure<br>
                    4. Request for cessation of processing<br>
                    ② Data subject may exercise the rights set forth in Paragraph (1) above towards the Company in writing or by phone, e-mail, fax, etc., and the Company will take measures accordingly without any delay.<br>
                    ③ If the personal data requests for correction or erasure with respect to any error, etc. in the personal data, the Company does not use or provide the relevant personal data until the correction or erasure is completed.<br>
                    ④ The rights set forth in Paragraph (1) above may be exercised by the data subject’s agent, such as his/her legal representative or attorney-in-fact. In such case, the completed power-of-attorney form attached to the Enforcement Rules to Personal Information Protection Act as “Form 11” must be submitted.<br>
                    ⑤ The data subject shall not violate the Personal Information Protection Act and other applicable laws and regulations and infringe upon his/her own or any third party’s privacy or personal data being processed by the Company <br><br>

                    <span>Article 6(Destruction of Personal Data)</span><br>
                    ① The Company destroys the personal data without any delay when such personal data becomes no longer necessary, such as when the retention period of such personal data expires or when the purpose for processing such personal data is achieved<br>
                    ② If the personal data needs to be keep preserved pursuant to other applicable laws and regulations despite that the personal data retention period consented by the data subject has expired or the purpose of processing the personal data has been achieved, the Company preserves such personal data by transferring it to a separate database or different storage.<br>
                    ③ The procedure and method of destroying the personal data are as follows:<br>
                    1. Destroying procedure<br>
                    The Company selects the personal data that is required to be destroyed, and destroys such personal data after obtaining the Company’s Person in Charge of Personal Data Protection.<br><br>
                    2. Destroying method<br>
                    The Company destroys any personal data that is recorded and stored in digital file format by using a method that renders such record unplayable, such as the low-level formatting method, etc., and destroys any person data that is recorded and stored in paper documents is destroyed by shredding them with paper shredder or by incinerating them.<br><br>

                    <span>Article 7(Measures to Secure Safety of Personal Data) </span><br>
                    The Company takes following measures to secure the safety of the personal data:<br>
                    1. Managerial measures : Establishment and implementation of internal management plan, long-term employee training, etc.<br>
                    2. Technical measures : Access authority control on personal data processing system etc., installation of access control system, encryption of unique identification information, installation of security program<br>
                    3. Physical measures: Access control on data processing room, data storage room, etc.<br><br>

                    <span>Article 8(Matters relating to Installation and Operation of Automatic Personal Data Collection Equipment and Refusal of Automatic Collection)</span><br>
                    ① The Company utilizes “cookie” which saves and restores the usage information from time to time in order to provide a customized individual service to a user.<br>
                    ② Cookie is small data that the server (http) used in operating the website sends to the user’s computer browser, and may be saved in the hard disk in the user’s computer.<br>
                    A. Purpose of using cookie: Cookie is used in order to provide optimized information to the user through analysis of the user’s visit and use pattern of each service and website the user visited, popular search, secure connection, etc.<br>
                    B. Installation/operation of cookie and refusal thereof : The user may refuse the saving of cookie by changing the setting on the [Privacy] tab in the [Internet Option] menu which can be accessed by clicking on the [Tools] menu located on the upper side of the web browser.<br>
                    C. Please note that there may be difficulty is using the customized service if the cookie saving is refused.<br><br>

                    <span>Article 9 (Person in Charge of Personal Data Protection)</span> <br>
                    ① The Company is generally responsible for the tasks of processing of personal data, and the Company designated the Person in Charge of Personal Data Protection as follows in order to handle and provide remedies for the data subject’s complaint in connection with the processing of personal data<br>

                    ▶ Person in Charge of Personal Data Protection<br>
                    Name : Jun Hwan An <br>
                    Title : Director<br>
                    Contact Information : 	&#10094;031-625-4340&#10095;, 	&#10094;jay@mindslab.ai&#10095;, 	&#10094;031-625-4119&#10095; <br>
                    ※ Connects to the Department in Charge of Personal Data Protection. <br>
                    ▶ Department in Charge of Personal Data Protection<br>
                    Department Name : Management Support Team<br>
                    Person in Charge : Jun Hwan An<br>
                    Contact Information : 	&#10094;031-625-4340&#10095;, 	&#10094;jay@mindslab.ai&#10095;, 	&#10094;031-625-4119&#10095; <br><br>

                    ② The data subject can contact the Person or Department in Charge of Personal Data Protection for any and all matters in connection with the enquiries related to personal data protection, handling of complaint, remedies, etc. that arose in the course of using the Company’s service (or business). The Company will respond and handle the data subject’s enquiries without any delay.<br>

                    Article 10 (Request for Access to Personal Data) The data subject can direct to the following department his/her request for access to his/her personal data under Article 35 of the Personal Information Protection Act. The Company will endeavor to swiftly handle the data subject’s request for access to his/her personal data.<br>

                    ▶ Department in charge of receiving and handling request for personal data access<br>
                    Contact Information :	&#10094;support@mindslab.ai&#10095;<br><br>

                    <span>Article 11 (Remedies for Infringement on Rights)</span><br>
                    Data subject may contact the following agencies and inquire about the remedies for any personal data infringement, and consultation, etc.<br><br>

                    &#91;The following agencies are not affiliated with the Company, and please contact theme if you are not satisfied with the Company’s own handling of any complaint and provision of remedies, or you need further detailed assistance&#93;<br>
                    ▶ Korea Internet Security Center (operated by the Korea Internet &amp; Safety Agency)<br>
                    - Jurisdiction : Report any infringement of personal data, consultation request<br>
                    - Website : privacy.kisa.or.kr <br>
                    - Phone : 118 <br>
                    - Address : 3rd Floor, Korea Internet and Safety Agency, 9, Jinheung-gil (301-2, Bitgaram-dong), Naju-si, Jeollanam-do 58324<br><br>

                    ▶ Personal Data Dispute Mediation Committee<br>
                    - Jurisdiction : Application for personal data dispute mediation, collective dispute mediation (civil settlement)<br>
                    - Website : www.kopico.go.kr <br>
                    - Phone : 1833-6972<br>
                    - Address : 4th Floor, Government Complex-Seoul, 209, Sejong-daero, Jongno-gu, Seoul 03171<br><br>

                    ▶ Supreme Prosecutors’ Office, Cyber Crime Investigation Center : 02-3480-3573 (www.spo.go.kr)<br><br>

                    ▶ Korean National Police Agency Cyber Bureau : 182 (http://cyberbureau.police.go.kr)<br><br>

                    <span>Article 12 (Change of Privacy Policy)</span> <br>
                    ① This Privacy Policy is effective as of February 01, 2019.

                </div>
                <!--//terms_box-->


            </div>

        </div>
        <!--content-->
    </div>
    <!-- //.contents -->
</div>
<!-- //#container -->

<script type="text/javascript">
    $(window).load(function() {
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
    });

    $(document).ready(function() {
        // language (pc)
        $('.header_box .sta .etcMenu ul li.lang').each(function(){
            $(this).on('click',function(){
                $(this).toggleClass('active');
            });
            $('.header_box .sta .etcMenu ul li.lang ul.lst li a').on('click',function(){
                $(this).removeClass('active');
            });
            $('#container').on('click',function(){
                $('.header_box .sta .etcMenu ul li.lang').removeClass('active');
            });
        });

        // language (mobile)
        $('ul.m_etcMenu li.lang').each(function(){
            $(this).on('click',function(){
                $(this).toggleClass('active');
            });
            $('ul.m_etcMenu li.lang ul.lst li a').on('click',function(){
                $(this).removeClass('active');
            });
        });

    });



</script>
<script type="text/javascript">

    function login(){
        location.href = "${google_url}" + "?targetUrl=/?lang=en";

    }

</script>

</body>
</html>
