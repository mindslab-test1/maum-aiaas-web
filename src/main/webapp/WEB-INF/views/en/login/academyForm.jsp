<%--
  Created by IntelliJ IDEA.
  User: SMR
  Date: 2019-09-16
  Time: 오후 4:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="../common/common_header.jsp" %>


<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<!-- wrap -->
<div id="wrap">
    <div class="loginWrap">
        <div class="common_header">
            <div class="header_box">
                <h1><a href="/?lang=en"><img src="${pageContext.request.contextPath}/aiaas/en/images/logo_title_navy.svg" alt="maum.ai logo"></a>
                    <span>Make your own AI Service</span>
                </h1>
                <!--.sta-->
                <div class="sta">
                    <a href="/home/pricingPage?lang=en" class="go_price">Pricing <span>Plan</span></a>
                    <!--.etcMenu-->
                    <div class="etcMenu">
                        <ul>
                            <li class="lang">
                                <p class="lang_select">English<em class="fas fa-chevron-down"></em></p>
                                <ul class="lst">
                                    <li><a href="/?lang=ko" target="_self"><em>한국어</em></a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!--//.etcMenu-->
                </div>
                <!--//.sta-->
            </div>
        </div>

        <div class="academy_top">
            <div class="academy_logo">
                <img src="" alt="아카데미 로고" />
            </div>

        </div>

        안녕
    </div>
</div>
<!-- //wrap -->

<script type="text/javascript">
    $(window).load(function() {

    });
</script>

