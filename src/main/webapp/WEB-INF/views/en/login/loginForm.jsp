<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ include file="../common/common_header_v2.jsp" %>

<%--</jsp:include>--%>
<form id="logout-form" action='/logout' method="POST">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>

<!-- .page loading -->
<div id="pageldg">
    <span class="out_bg">
        <em>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <b>&nbsp;</b>
        </em>
    </span>
</div>
<!-- //.page loading -->

<!-- .lyr_info -->
<div class="lyr_info">
    <div class="lyr_plan_bg"></div>
    <!-- .lyrWrap -->
    <div class="lyrWrap">
        <button class="btn_lyrWrap_close" type="button">닫기</button>
        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <em class="far fa-address-card"></em>
            <p>Enter Your Information</p>
            <ul>
                <li>
                    <span>&ast;Name</span>
                    <input id="info_name" type="text" value="">
                </li>
                <li>
                    <span>&ast;Company</span>
                    <input id="info_company" type="text" value="">
                </li>
                <li>
                    <span>&ast;Email</span>
                    <input id="info_email" type="email" value="">
                </li>
                <li>
                    <span>Phone Number</span>
                    <input id="info_phone" type="text" value="">
                </li>
            </ul>
            <button class="btn_blue" id="downloadPDF">Download</button>
        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.lyrWrap -->
</div>
<!-- //.lyr_info -->


<!-- .lyr_event -->
<div class="lyr_consultant">
    <div class="lyr_consultant_bg"></div>
    <!-- .lyrWrap -->
    <div class="lyrWrap">
        <div class="lyr_hd">
            <h1>Become an <span>AI Consultant</span><br>
                in <span>4 Easy Steps</span></h1>
            <em class="fas fa-times btn_lyrWrap_close"></em>
        </div>

        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <ul>
                <li>
                    <p>
                        <strong>1. Join us</strong>
                        <span>Fill out your information and submit through our Google form to get started!</span>

                    </p>
                    <em class="fas fa-angle-double-down"></em>
                </li>
                <li>
                    <p>
                        <strong>2. Study & Learn</strong>
                        <span>Go through our training modules and explore our maum.ai to expand your knowledge in AI</span>
                        <%--                        <a href="/home/academyForm" target="_blank" class="academy_btn">아카데미 바로가기 <em class="fas fa-angle-right"></em></a>--%>
                    </p>
                    <em class="fas fa-angle-double-down"></em>
                </li>
                <li>
                    <p>
                        <strong>3. Take a Test</strong>
                        <span>Take a test to show what you have learned</span>
                    </p>
                    <em class="fas fa-angle-double-down"></em>
                </li>
                <li>
                    <p>
                        <strong>4. Get Started!</strong>
                        <span>Receive your certification and begin your AI Consultant journey</span>
                    </p>
                </li>
            </ul>

            <a href="https://docs.google.com/forms/d/e/1FAIpQLSczF2ih31tXMFZgJ5yF_5gzLEK-kRvGO25U3fJddEzGTNC4SA/viewform"
               target="_blank">Apply Now <em class="fas fa-angle-right"></em></a>

        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.lyrWrap -->
</div>
<!-- //.lyr_event -->

<!-- #header -->
<div id="header">
    <!-- .header_box -->
    <div class="header_box">
        <h1 class="fl"><a href="/?lang=en">
            <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/logo_maumai.svg"
                 alt="maum ai logo">
        </a></h1>

        <ul class="gnb fr">
            <li><a href="#our_services">Service</a></li>
            <li><a href="#ecoMINDs" class="go_ecominds">ecoMINDs</a></li>
            <li><a href="#" class="go_const">AI Consultant</a></li>
            <li><a href="/home/pricingPage?lang=en">Pricing</a></li>
            <li><a href="https://mindslab.ai:8080/en" target="_blank" title="MINDsLab">About Us</a></li>

            <!-- 로그인 X인 경우 -->
            <sec:authorize access="isAnonymous()">
                <li class="brd_btn"><a href="javascript:login()">Sign In</a></li>
            </sec:authorize>

            <!-- 로그인 O인 경우 -->
            <sec:authorize access="isAuthenticated()">
                <li class="user_info">
                    <div class="userBox">
                        <ul>
                            <li>
                                <p class="ico_user">
                                    <em class="far fa-user"></em>
                                    <span>${fn:escapeXml(sessionScope.accessUser.email)}</span>
                                    <em class="fas fa-angle-down"></em>
                                    <em class="fas fa-angle-up"></em>
                                </p>
                                <ul class="lst">
                                    <li class="ico_profile"><a target="_self" href="/user/profileMain">PROFILE</a></li>
                                    <li class="ico_account"><a target="_self" href="/user/apiAccountMain">API
                                        ACCOUNT</a></li>
                                    <li class="ico_payment"><a target="_self" href="/user/paymentInfoMain">PAYMENT</a>
                                    </li>
                                    <li class="ico_logout"><a href="#"
                                                              onclick="document.getElementById('logout-form').submit();">LOGOUT</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </li>
            </sec:authorize>

            <li class="lang">
                <div class="lang_box">
                    <span>English</span>
                    <span><a href="/?lang=kr" target="_self">Korean</a></span>
                </div>
            </li>
        </ul>


    </div>
    <!-- //.header_box -->
</div>
<!-- //#header -->

<!-- #wrap -->
<div id="wrap">
    <!-- aside -->
    <div class="aside">
        <!-- .aside_top -->
        <div class="aside_top">
            <h1><a href="/?lang=en"><img
                    src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/logo_maumai.svg"
                    alt="maumAI logo"></a></h1>

            <!-- 로그인 X인 경우 -->
            <sec:authorize access="isAnonymous()">
                <a class="btn_sign" href="javascript:login()">Sign In</a>
            </sec:authorize>

            <!-- 로그인 O인 경우 -->
            <sec:authorize access="isAuthenticated()">
                <a class="btn_sign" href="#none">${fn:escapeXml(sessionScope.accessUser.email)}</a>
            </sec:authorize>

        </div>
        <!-- //.aside_top -->
        <!-- .aside_mid -->
        <div class="aside_mid">
            <ul class="m_nav">
                <li>
                    <h2><a href="#none" class="">My Page <em class="fas fa-chevron-down"></em></a></h2>
                    <ul class="m_lst">
                        <li>
                            <h3><a href="/user/profileMain" target="_blank" title="profile"
                                   class="layer_m_btn">PROFILE</a></h3>
                            <h3><a href="/user/apiAccountMain" target="_blank" title="api_account" class="layer_m_btn">API
                                ACCOUNT</a></h3>
                            <h3><a href="/user/paymentInfoMain" target="_blank" title="payment" class="layer_m_btn">PAYMENT</a>
                            </h3>
                            <h3><a href="#"
                                   onclick="document.getElementById('logout-form').submit();">LOGOUT</a></h3>
                        </li>

                    </ul>
                </li>

                <li>
                    <h2><a href="#none" class="">Service <em class="fas fa-chevron-down"></em></a></h2>
                    <ul class="m_lst">
                        <li>
                            <h3><a href="https://builder.maum.ai/enLanding" class="layer_m_btn">AI Builder</a></h3>
                        </li>
                        <li>
                            <h3><a href="/main/enMainHome" class="layer_m_btn">Cloud API</a></h3>
                        </li>
                        <li>
                            <h3><a href="https://fast-aicc.maum.ai" class="layer_m_btn">FAST Conversational AI</a></h3>
                        </li>
                        <li>
                            <h3><a href="https://minutes.maum.ai/" class="layer_m_btn">Maum Minutes</a></h3>
                        </li>
                    </ul>
                </li>

                <li><h2><a href="#ecoMINDs" class="go_ecominds">ecoMINDs</a></h2></li>
                <li><a href="#" class="go_const">AI Consultant</a></li>
                <li><h2><a href="/home/pricingPage?lang=en">Pricing</a></h2></li>
                <li><h2><a href="https://mindslab.ai:8080/en" target="_blank" title="MINDsLab">About Us</a></h2></li>

            </ul>
        </div>
        <!-- //.aside_mid -->

        <!-- .aside_btm -->
        <div class="aside_btm">
            <span>English</span>
            <span><a href="/?lang=kr" target="_self">Korean</a></span>
        </div>
        <!-- .aside_btm -->

    </div>
    <div class="bg_aside"></div>
    <a class="btn_header_ham" href="#none"><span class="hamburger_icon"></span></a>
    <!-- //.aside -->

    <!-- #contents -->
    <div id="contents">
        <!-- .stn_visual -->
        <div class="stn_visual">
            <!-- .cont_box -->
            <div class="cont_box">
                <div class="img_area fr">
                    <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/img_maumai_service.png"
                         alt="maum ai service image">
                </div>

                <div class="txt_area fl">
                    <h2>Build Your Own <br>AI Service</h2>
                    <span>Your <em>Success</em> Is Our <em>Business</em></span>
                    <p>Everything you need from customizable AI engines<br>to AI models for your business to succeed</p>
                    <div class="btn_box">
                    <sec:authorize access="isAnonymous()">
                        <a href="https://builder.maum.ai/?lang=en" title="AI Builder" target="_blank">Try AI Builder</a>
                    </sec:authorize>
                    <sec:authorize access="isAuthenticated()">
                        <a href="https://builder.maum.ai/?lang=en" title="AI Builder" target="_blank">Try AI Builder</a>
                    </sec:authorize>
                    </div>
                    <div class="btn_box btn_go_maumBook">
                        <a href="${pageContext.request.contextPath}/event/maumBookEng" title="maum book event" target="_blank">See our success stories</a>
                    </div>
                </div>
            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn_visual -->

        <!-- .stn_services -->
        <div class="stn_services">
            <!-- .cont_box -->
            <div class="cont_box">
                <h3 id="our_services">
                    <p>
                        <span>Our Services</span>
                        <em><span>FREE</span> 1&hyphen;month trial</em>
                    </p>
                </h3>

                <ul>
                    <li class="ai_builder">
                        <h4>AI Builder</h4>
                        <span>Customize your AI toolset</span>
                        <p>Explore and create AI services tailored to your business with our engines.</p>
                        <a href="#none" id="mvpmaker_dwn" class="down_link">Download Info</a>
                        <div class="btn_box">
                        <sec:authorize access="isAnonymous()">
                            <a href="https://builder.maum.ai/?lang=en" title="AI Builder" target="_blank">Start Now</a>
                        </sec:authorize>
                        <sec:authorize access="isAuthenticated()">
                            <a href="https://builder.maum.ai/?lang=en" title="AI Builder" target="_blank">Start Now</a>
                        </sec:authorize>
                        </div>
                    </li>

                    <li class="cloud_api">
                        <h4>Cloud API</h4>
                        <span>The easiest way to use AI</span>
                        <p>30+ different AI engines including language, voice, and graphics for your business.</p>
                        <a href="#none" id="maumCloud_dwn" class="down_link">Download Info</a>
                        <div class="btn_box">
                            <a href="/main/enMainHome" title="Cloud API" target="_blank">Start Now</a>
                        </div>
                    </li>
                </ul>

                <ul>
                    <li class="fast_ai">
                        <h4>FAST Conversational AI</h4>
                        <span>Automate your CS operations</span>
                        <p>All-in-one customer service providing In/Out Bound Voice Bot to Hybrid chat consultants and
                            Auto QA systems.</p>
                        <div class="btn_box btn_box__noDwn">
                            <a href="https://fast-aicc.maum.ai/login?lang=en" title="FAST Conversational AI" target="_blank">Start Now</a>
                        </div>
                    </li>

                    <li class="minutes">
                        <h4>Maum Minutes</h4>
                        <span>Skip typing, Get talking</span>
                        <p>Don't worry about taking notes during meetings. Invite maum Minutes to transcribe your
                            amazing ideas.</p>

                        <div class="btn_box btn_box__noDwn">
                            <a style="cursor:default;">Coming Soon</a>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn_services -->

        <!-- .stn_structure -->
        <div class="stn_structure">
            <!-- .cont_box -->
            <div class="cont_box">
                <h3><span>maum.ai</span> service structure</h3>

                <ul class="service_lyr">
                    <li class="bg_green">
                        <span>Layer 1</span>
                        <em>Data &sol; Cleansing Service</em>
                    </li>
                    <li class="bg_bgreen">
                        <span>Layer 2</span>
                        <em>AI Model Training</em>
                    </li>
                    <li class="bg_blue">
                        <span>Layer 3</span>
                        <em>Engine API<span>Inference Engines</span></em>
                    </li>
                    <li class="bg_purple">
                        <span>Layer 4</span>
                        <em>Applications &sol; AI Services</em>
                    </li>
                </ul>

                <!-- .hidden_area -->
                <div class="hidden_area">
                    <!-- .archtecture_box -->
                    <div class="archtecture_box">
                        <!-- .item_set -->
                        <div class="item_set bg_purple">
                            <!-- .item_cont -->
                            <div class="item_cont">
                                <!-- .item_lst -->
                                <ul class="item_lst em_enter">
                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>FAST Conversational AI</em></dt>
                                                <dd class="tile_txt">
                                                    <em>
                                                        <span>Inbound</span>
                                                        <span>Outbound</span>
                                                    </em>
                                                    <em>
                                                        <span>Hybrid<br>Chat</span>
                                                        <span>AQA</span>
                                                    </em>
                                                </dd>
                                            </dl>
                                            <strong>
                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer4_voicebot.svg"
                                                     alt="background image">
                                                Voice Bot
                                            </strong>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>maum Minutes</em></dt>
                                                <dd>
                                                    <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer4_minutes.svg"
                                                         alt="background image">
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>voice Album</em></dt>
                                                <dd>
                                                    <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/voice.svg"
                                                         alt="background image">
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                </ul>
                                <!-- //.item_lst -->
                                <span class="lyr_bottom mvp_maker">AI Builder</span>
                            </div>
                            <!-- //.item_cont -->
                        </div>
                        <!-- //.item_set -->

                        <!-- .item_set -->
                        <div class="item_set bg_blue">
                            <!-- .item_cont -->
                            <div class="item_cont">
                                <!-- .item_lst -->
                                <ul class="item_lst">
                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Voice</em></dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico-spe-1-fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Text to Speech</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_spe_2_fold.svg"
                                                                     class="img_m" alt="background image">
                                                            </div>
                                                            <em>Speech to Text</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_spe_3_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Denoise</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_spe_5_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Voice Filter</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_spe_6_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Voice<br>Recognition</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Vision</em></dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_2_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>AI Styling</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_1_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Text Removal</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_4_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>AI Vehicle<br>Recognition</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_5_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Enhanced Super<br>Resolution</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_6_fold.svg"
                                                                     class="img_m" alt="background image">
                                                            </div>
                                                            <em>Face Recognition</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_7_fold.svg"
                                                                     class="img_m" alt="background image">
                                                            </div>
                                                            <em>Pose Reconition</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_8_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Face Tracking</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer3_imgsub.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Subtitles Extraction</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_9_fold.svg"
                                                                     class="img_s" alt="background image">
                                                            </div>
                                                            <em>Anomaly Detection</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Languages</em></dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_lan_2_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>MRC</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_lan_3_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>XDC</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_lan_6_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>HMD</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_lan_1_fold.svg"
                                                                     class="img_s" alt="background image">
                                                            </div>
                                                            <em>NLU</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_lan_5_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>GPT-2</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_lan_itf.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>ITF</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Analysis</em></dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer3_dataanal.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Correlation<br>Analysis</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Conversation</em></dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_bot_1_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Hotel Concierge<br>Bot</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_bot_1_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>NQA Bot</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_bot_2_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Weather&#47;Wiki Bot</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>English Education</em></dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_eng_1.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>STT for<br>Eng. education</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_eng_2.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Pronounciation<br>Scoring</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_eng_3.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Phonics<br>Assessment</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                </ul>
                                <!-- //.item_lst -->
                            </div>
                            <!-- //.item_cont -->
                        </div>
                        <!-- //.item_set -->

                        <!-- .item_set -->
                        <div class="item_set bg_bgreen">
                            <!-- .item_cont -->
                            <div class="item_cont">
                                <!-- .item_lst -->
                                <ul class="item_lst em_enter_down">
                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Voice</em></dt>
                                                <dd>
                                                    <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer2.svg"
                                                         alt="background image">
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Vision</em></dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer2.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Language &plus; MLT</em></dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer2.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Data Analysis</em></dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer2.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Conversation</em></dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer2.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>English Education</em></dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer2.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                </ul>
                                <!-- //.item_lst -->
                            </div>
                            <!-- //.item_cont -->
                        </div>
                        <!-- //.item_set -->

                        <!-- .item_set -->
                        <div class="item_set bg_green">
                            <!-- .item_cont -->
                            <div class="item_cont">
                                <!-- .item_lst -->
                                <ul class="item_lst em_enter_down">
                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Voice Data</em></dt>
                                                <dd>
                                                    <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer1.svg"
                                                         alt="background image">
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Vision Data</em></dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer1.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Language Data</em></dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer1.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Data Analysis</em></dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer1.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Conversation Data</em></dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer1.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>

                                    <li>
                                        <div class="inner_box">
                                            <dl>
                                                <dt><em>Eng. Edu Data</em></dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer1.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                </ul>
                                <!-- //.item_lst -->
                                <span class="lyr_bottom maum_data">Cloud Data Edit Tool</span>
                            </div>
                            <!-- //.item_cont -->
                        </div>
                        <!-- //.item_set -->
                    </div>
                    <!-- //.archtecture_box -->
                </div>
                <!-- //.hidden_area -->

                <!-- .stn_landing_m -->
                <div class="stn_landing_m mobile">
                    <div class="stn_landing_cont">
                        <div class="item_set bg_purple">
                            <div class="item_title">
                                <div>
                                    <span>Layer</span>
                                    <em>4</em>
                                </div>
                                <h3>Applications /
                                    AI Services</h3>
                            </div>
                            <div class="swiper-container preview">
                                <ul class="item_lst em_enter swiper-wrapper">
                                    <li class="swiper-slide">
                                        <a class="inner_box" href="#fastaicc" title="FAST AI">
                                            <dl>
                                                <dt>
                                                    <em>FAST Conversational AI</em>
                                                </dt>
                                                <dd class="tile_txt">
                                                    <em>
                                                        <span>Inbound</span>
                                                        <span>Outbound</span>
                                                    </em>
                                                    <em>
                                                        <span>Hybrid<br>Chat</span>
                                                        <span>AQA</span>
                                                    </em>
                                                </dd>
                                            </dl>
                                            <strong>
                                                Voice Bot
                                                <br>
                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer4_voicebot.svg"
                                                     alt="background image">
                                            </strong>
                                        </a>
                                    </li>
                                    <li class="swiper-slide">
                                        <a href="#minutes" title="maum Minutes" class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>maum Minutes <span class="open">open</span></em>
                                                </dt>
                                                <dd>
                                                    <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer4_minutes.svg"
                                                         alt="background image">
                                                </dd>
                                            </dl>
                                        </a>
                                    </li>
                                    <li class="swiper-slide">
                                        <a href="#voicealbum" class="inner_box" title="Voice Album">
                                            <dl>
                                                <dt>
                                                    <em>Voice Album</em>
                                                </dt>
                                                <dd>
                                                    <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/voice.svg"
                                                         alt="background image">
                                                </dd>
                                            </dl>
                                        </a>
                                    </li>
                                </ul>
                                <a class="link_mvp_maker" href="#mvpmaker">AI Builder</a>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                        <div class="item_set bg_blue">
                            <div class="item_title">
                                <div>
                                    <span>Layer</span>
                                    <em>3</em>
                                </div>
                                <h3>
                                    Engine API <span>Inference engines</span>
                                </h3>
                            </div>
                            <div class="swiper-container preview">
                                <ul class="item_lst swiper-wrapper">
                                    <li class="swiper-slide">
                                        <a class="inner_box" href="#maumCloud">
                                            <dl>
                                                <dt>
                                                    <em>Voice</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico-spe-1-fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Text to Speech</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_spe_2_fold.svg"
                                                                     class="img_m" alt="background image">
                                                            </div>
                                                            <em>Speech to Text</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_spe_3_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Denoise</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_spe_5_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Voice Filter</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_spe_6_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Voice Recognition</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </a>
                                    </li>
                                    <li class="swiper-slide">
                                        <a class="inner_box" href="#maumCloud">
                                            <dl>
                                                <dt>
                                                    <em>Vision</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_2_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>AI Styling</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_1_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Text Removal</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_4_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>AI Vehicle<br> Recognition</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_5_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Enhanced Super<br>Resolution</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_6_fold.svg"
                                                                     class="img_m" alt="background image">
                                                            </div>
                                                            <em>Face Recognition</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_7_fold.svg"
                                                                     class="img_m" alt="background image">
                                                            </div>
                                                            <em>Pose Recognition</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_8_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Face Tracking</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_vis_9_fold.svg"
                                                                     class="img_s" alt="background image">
                                                            </div>
                                                            <em>Abnormal <br>Behavior Detection</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </a>
                                    </li>
                                    <li class="swiper-slide">
                                        <a class="inner_box" href="#maumCloud">
                                            <dl>
                                                <dt>
                                                    <em>Languages</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_lan_2_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>MRC</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_lan_3_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>XDC</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_lan_6_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>HMD</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_lan_1_fold.svg"
                                                                     class="img_s" alt="background image">
                                                            </div>
                                                            <em>NLU</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_lan_5_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>GPT-2</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_lan_itf.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>ITF</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </a>
                                    </li>
                                    <li class="swiper-slide">
                                        <a class="inner_box" href="#maumCloud">
                                            <dl>
                                                <dt>
                                                    <em>Analysis</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_bot_2_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Correlation<br> Analysis</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </a>
                                    </li>
                                    <li class="swiper-slide">
                                        <a class="inner_box" href="#maumCloud">
                                            <dl>
                                                <dt>
                                                    <em>Conversation</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_bot_1_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>NQA Bot</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_bot_2_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Weather/Wiki Bot</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_bot_1_fold.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Hotel <br>Concierge Bot</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </a>
                                    </li>
                                    <li class="swiper-slide">
                                        <a class="inner_box" href="#maumCloud">
                                            <dl>
                                                <dt>
                                                    <em>English Education</em>
                                                </dt>
                                                <dd>
                                                    <ul>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_eng_1.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>STT for <br>Eng. education</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_eng_2.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Pronunciation<br>Scoring</em>
                                                        </li>
                                                        <li class="landing_ico">
                                                            <div class="img">
                                                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_eng_3.svg"
                                                                     alt="background image">
                                                            </div>
                                                            <em>Phonics <br>Assessment</em>
                                                        </li>
                                                    </ul>
                                                </dd>
                                            </dl>
                                        </a>
                                    </li>
                                </ul>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                        <div class="item_set bg_bgreen">
                            <div class="item_title">
                                <div>
                                    <span>Layer</span>
                                    <em>2</em>
                                </div>
                                <h3>AI Model
                                    Training</h3>
                            </div>
                            <div class="swiper-container preview">
                                <ul class="item_lst em_enter_down swiper-wrapper">
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>Voice</em>
                                                </dt>
                                                <dd>
                                                    <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer2.svg"
                                                         alt="background image">
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>Vision</em>
                                                </dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer2.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>Language &plus; MLT</em>
                                                </dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer2.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>Data</em>
                                                </dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer2.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>Conversation</em>
                                                </dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer2.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>English education</em>
                                                </dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer2.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                </ul>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                        <div class="item_set bg_green">
                            <div class="item_title">
                                <div>
                                    <span>Layer</span>
                                    <em>1</em>
                                </div>
                                <h3>Data /
                                    Cleansing service</h3>
                            </div>
                            <div class="swiper-container preview2">
                                <ul class="item_lst em_enter_down swiper-wrapper">
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>Voice Data</em>
                                                </dt>
                                                <dd>
                                                    <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer1.svg"
                                                         alt="background image">
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>Vision Data</em>
                                                </dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer1.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em style="font-size: 10px;line-height: 11px;">Language<br>
                                                        Data</em>
                                                </dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer1.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>Data Analysis</em>
                                                </dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer1.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em style="font-size: 10px;line-height: 11px;">Conversation<br> Data</em>
                                                </dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer1.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                    <li class="swiper-slide">
                                        <div class="inner_box">
                                            <dl>
                                                <dt>
                                                    <em>Eng. edu Data</em>
                                                </dt>
                                                <dd>
                                                    <div>
                                                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_layer1.svg"
                                                             alt="background image">
                                                    </div>
                                                </dd>
                                            </dl>
                                        </div>
                                    </li>
                                </ul>
                                <div class="go_editTool">
                                    <a href="#datatool" class="mobile_move">Cloud Data Edit Tool</a>
                                </div>
                                <div class="swiper-pagination"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //.stn_landing_m -->

                <div class="btn_expand">
                    <button type="button" class="down"></button>
                    <button type="button" class="up"></button>
                </div>
            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn_structure -->

        <!-- .stn_business -->
        <div id="ecoMINDs" class="stn_business">
            <!-- .cont_box -->
            <div class="cont_box">
                <div class="slide_box swiper-container">
                    <ul class="swiper-wrapper">
                        <li class="swiper-slide maumdata">
                            <div class="img_area fl">
                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/img_slide_maumdata.svg"
                                     alt="maumdata image">
                            </div>
                            <div class="txt_area fr">
                                <h6><span>maum DATA</span> <br>your own dataset</h6>
                                <p>Purchase already processed and analyzed<br>datasets or gather new data<br>to train
                                    your own AI model</p>
                                <div class="btn_box">
                                    <a href="https://data.maum.ai/?lang=en" target="_blank" title="maumDATA">maum DATA</a>
                                </div>
                            </div>
                            <span>&ast; service independent from maum.ai</span>
                        </li>
                        <li class="swiper-slide ecominds">
                            <div class="img_area fl">
                                <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/img_slide_ecominds.svg"
                                     alt="ecominds image">
                            </div>
                            <div class="txt_area fr">
                                <h6><span>ecoMINDs</span> <br>our AI ecosystem</h6>
                                <p>Our partnership project strives for success<br>in all parties. Join our family and
                                    introduce<br>the latest AI technologies for your startup.</p>
                                <div class="btn_box">
                                    <a href="${pageContext.request.contextPath}/aiaas/common/files/%5Bmaum.ai%5D%20Overall%20Service%20Guide_EN.pdf"
                                       target="_blank" title="eco MINDs"
                                       download="[maum.ai]_eco_MINDs_AI consultant_Document.pdf">ecoMINDs</a>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="slide_btn swiper-button-prev">
                    <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/arw_left_b.svg"
                         alt="left arrow">
                </div>

                <div class="slide_btn swiper-button-next">
                    <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/arw_right_b.svg"
                         alt="right arrow">
                </div>
            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn_business -->

        <!-- .press_release -->
        <div class="landing_box press_release">
            <div class="area_box">
                <div class="layer_img">
                    <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/toronto_cityscape.jpg"
                         alt="maum data logo">
                </div>
                <div class="layer_desc">
                    <h2>HELLO Toronto!</h2>
                    <h3>Minds Lab is Here</h3>
                    <p>We have launched our new global headquarters and we are excited to be a part of the leaders
                        of the AI industry</p>

                    <a href="https://www.linkedin.com/posts/mindslabai_minds-lab-joins-toronto-activity-6681948994354106368-s7U4"
                       target="_blank"
                       title="Read More">Read More</a>
                </div>

            </div>
        </div>
        <!-- .press_release -->
    </div>
    <!-- //#contents -->

    <!-- #footer -->
    <div id="footer">
        <!-- //.foot_box -->
        <div class="footer_box">
            <div class="contact_box fl">
                <h4>Contact Us</h4>
                <ul class="sns_box">
                    <li><a href="https://www.linkedin.com/company/mindslabai/" target="_blank">
                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_footer_linkedin.png"
                             alt="LinkedIn icon">
                        <span>LinkedIn</span>
                    </a></li>
                    <li><a href="https://www.facebook.com/pg/mindsinsight" target="_blank">
                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_footer_facebook.svg"
                             alt="Facebook icon">
                        <span>Facebook</span>
                    </a></li>
                    <li><a href="https://www.youtube.com/channel/UC8ocQaJFp-adzGtwcBuS3iQ?view_as=subscriber"
                           target="_blank">
                        <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/ico_footer_youtube.svg"
                             alt="Youtube icon">
                        <span>Youtube</span>
                    </a></li>
                </ul>
                <ul class="info_box">
                    <li><a href="/home/enTermsMain">Terms &amp; Conditions</a></li>
                    <li><a href="tel:+8216613222">&plus;82 1661&hyphen;3222</a></li>
                    <li><a href="mailto:hello@mindslab.ai">hello<span>&commat;mindslab.ai</span></a></li>
                </ul>
                <div class="cyrt_box">
                    <span>&copy; Copyright 2020 Minds Lab</span>
                    <span>601 Dasan Tower, 49 Daewangpangyo&hyphen;ro 644beon&hyphen;gil, Bundang&hyphen;gu, Seongnam&hyphen;si, Gyeonggi&hyphen;do,<br>Republic of Korea ㅣ CEO Taejoon Yoo ㅣ Company Registration Number 314&hyphen;86&hyphen;55446</span>
                </div>
            </div>

            <div class="inquiry_box fr">
                <div class="ipt_box">
                    <input type="text" id="name">
                    <label for="name"><span class="fas fa-user"></span>Name</label>
                </div>

                <div class="ipt_box">
                    <input type="tel" pattern="[0-9]{3}-[0-9]{4}-[0-9]{4}" id="phone">
                    <label for="phone"><span class="fas fa-mobile-alt"></span>Phone Number</label>
                </div>

                <div class="ipt_box">
                    <input type="email" id="mail">
                    <label for="mail"><span class="fas fa-envelope"></span>Email</label>
                </div>

                <div class="ipt_box">
                    <textarea id="txt"></textarea>
                    <label for="txt"><span class="fas fa-comment-dots"></span>Message</label>
                </div>

                <button type="submit" id="sendMailToHello_landing" class="btn_send">Send</button>
            </div>

            <div class="cyrt_box_m">
                <span>&copy; Copyright 2020 Minds Lab</span>
                <span>601 Dasan Tower, 49 Daewangpangyo&hyphen;ro 644beon&hyphen;gil, Bundang&hyphen;gu, Seongnam&hyphen;si, Gyeonggi&hyphen;do,<br>Republic of Korea ㅣ CEO Taejoon Yoo ㅣ Company Registration Number 314&hyphen;86&hyphen;55446</span>
                <a href="/home/enTermsMain">Terms &amp; Conditions</a>
            </div>
        </div>
        <!-- //.foot_box -->
    </div>
    <!-- //#footer -->
</div>


<!-- 2 .pop_confirm -->
<div class="pop_confirm">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap" style="min-width:460px">
        <button class="pop_close" type="button">close</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <!--아이콘 부분-->
            <em class="fas fa-exclamation-triangle"></em>
            <!--제목 부분-->
            <p>Browser not supported</p>
            <!--내용 부분-->
            <span>You are using a web browser we currently don't support.
				<span style="display:block;margin-top:4px;">Improve your experience on <strong>maum.ai</strong> by using <strong>Google Chrome</strong>.</span>
			</span>
        </div>
        <!-- //.pop_bd -->
        <!--창닫기 버튼 -->
        <div class="btn">
            <button class="btn_close btn_blue">Close</button>
        </div>
        <!--창닫기 버튼 -->
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_confirm -->


<!-- 2 .pop_confirm -->
<div class="pop_confirm" id="mail_success">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap">
        <!-- .pop_bd -->
        <div class="pop_bd">

            <!--내용 부분-->
            <p>Your question has been sent. <br>Someone will get back to you <br>as soon as possible.</p>
        </div>
        <!-- //.pop_bd -->
        <!--창닫기 버튼 -->
        <div class="btn">
            <button class="btn_close">OK</button>
        </div>
        <!--창닫기 버튼 -->
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_confirm -->


<script type="text/javascript">    window.onload = function () {
    var browse = navigator.userAgent.toLowerCase();

    if ((navigator.appName == 'Netscape' && browse.indexOf('trident') != -1) || (browse.indexOf("msie") != -1)) {
        $('.pop_confirm').fadeIn();
    }

    // 공통 팝업창 닫기
    $('.pop_close, .pop_bg, .btn a, .btn button').on('click', function () {
        $('.pop_confirm').fadeOut(300);
    });


};

</script>

<script type="text/javascript">
    $(window).load(function () {
        var errormsg = "${errormsg}";
        if (errormsg != '' && errormsg != null) {
            alert(errormsg);
        }

        $('#pageldg').addClass('pageldg_hide2').delay(300).queue(function () {
            $(this).remove();
        });
    });

    function validateEmail(sEmail) {
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(sEmail)) {
            return true;
        } else {
            return false;
        }
    }

    $(document).ready(function () {
        $('#sendMailToHello_landing').on('click', function () {

            if (validateEmail($('#mail').val()) == false) {
                alert("You have entered an invalid e-mail address. Please try again.");
            } else if ($('#name').val() === '' || $('#phone').val() === '' || $('#mail').val() === '' || $('#txt').val() === '') {
                alert("Please fill in all contents above.");
            } else {
                var title = "[maum.ai] "+$("#name").val() + " 님의 문의 사항입니다.";
                var msg = "이름 : " + $("#name").val() + "<br>이메일 : " + $('#mail').val() + "<br>연락처 : " + $("#phone").val() + "<br>문의 내용 : " + $("#txt").val()
                    + "<br><br><br>*Sent from maum.ai Landing Page(EN)";


                var formData = new FormData();

                formData.append('fromaddr', $('#mail').val());
                formData.append('toaddr', 'hello@mindslab.ai');
                formData.append('subject', title);
                formData.append('message', msg);
                formData.append($("#key").val(), $("#value").val());

                $.ajax({
                    type: 'POST',
                    async: true,
                    url: '/support/sendContactMail',
                    dataType: 'text',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (obj) {
                        $("#name").val('');
                        $("#office").val('');
                        $("#phone").val('');
                        $("#txt").val('');
                        $("#mail").val('');
                        var placeholderLabel = $('.inquiry_box input, .inquiry_box textarea');


                        placeholderLabel.siblings('label').show();

                        console.log("Mail success");
                        $('#mail_success').fadeIn();
                    },
                    error: function (xhr, status, error) {
                        console.log("error");
                        alert("Failed to send mail to Contact Us. Please send us an email directly to hello@mindslab.ai");
                        window.location.href = "/";
                    }
                });
            }
        });

        // 인풋 라벨
        var placeholderLabel = $('.inquiry_box input, .inquiry_box textarea');

        placeholderLabel.on('focus', function () {
            $(this).siblings('label').hide();
        });

        placeholderLabel.on('focusout', function () {
            if ($(this).val() === '') {
                $(this).siblings('label').show();
            }
        });
    });

    $(document).ready(function () {
        // 문의하기 버튼 스크롤 이동
        // $('.contact_link').on('click', function (e) {
        // 	var this_link = $(this).attr('href');
        // 	console.log(this_link);
        // 	e.preventDefault();
        // 	$('html, body').animate({scrollTop : $(this_link).offset().top}, 800);
        // });
        // 문의하기 버튼 스크롤 이동 (모바일)
        // $('.mobile_move').on('click', function (e) {
        // 	var this_link = $(this).attr('href');
        // 	console.log(this_link);
        // 	$('html, body').animate({scrollTop : $(this_link).offset().top}, 800);
        // });


        // 소개서 다운받기 버튼
        $('.down_link').on('click', function (e) {
            $('.lyr_info').fadeIn();
            downloadFlag = $(this).attr('id');
        });
        $('.btn_blue').on('click', function () {
            var name = $('#info_name').val();
            var company = $('#info_company').val();
            var email = $('#info_email').val();
            var phone = $('#info_phone').val();

            if (name !== "" && company !== "" && validateEmail(email) == true) {

                var title = name + " 님께서 자료를 다운로드 받았습니다.(EN)";
                var mailContents = "이름 : " + name + "<br>"
                    + "회사(소속) : " + company + "<br>"
                    + "Email : " + email + "<br>"
                    + "연락처 : " + phone + "<br>"
                    + "다운로드 받은 소개서 : " + downloadFlag;

                var formData = new FormData();
                formData.append('fromaddr', $("#info_email").val());
                formData.append('toaddr', 'hello@mindslab.ai');
                formData.append('subject', title);
                formData.append('message', mailContents);
                formData.append($("#key").val(), $("#value").val());

                $.ajax({
                    type: 'POST',
                    async: true,
                    url: '/support/sendContactMail',
                    dataType: 'text',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        $('#info_name').val("");
                        $('#info_company').val("");
                        $('#info_email').val("");
                        $('#info_phone').val("");
                        // console.log(result.toString());
                        console.log("sending info success");

                        var route = " maum.ai : 소개서 자료 다운로드 ";
                        var a = document.createElement('a');

                        // 만약에 소개서 다운받기 버튼의 아이디값에 따라 다운로드 (maumCloud_dwn/ mvpmaker_dwn/ minutes_dwn/ fastaicc_dwn/ voicealbum_dwn/ ecominds_dwn)
                        <%--클라우드API id=""--%>
                        if (downloadFlag == 'maumCloud_dwn') {
                            console.log("aaa");
                            a.href = "${pageContext.request.contextPath}/aiaas/common/files/%5Bmaum.ai%5D%20Cloud%20API%20service.pdf";
                            a.download = "[maum.ai] Cloud API service.pdf";
                        }
                            <%--MVP메이커  id="mvpmaker_dwn"--%>
                        else if (downloadFlag == 'mvpmaker_dwn') {
                            console.log("bbb");
                            a.href = "${pageContext.request.contextPath}/aiaas/common/files/%5Bmaum.ai%5D%20AI%20Builder.pdf";
                            a.download = "[maum.ai] AI Builder.pdf";
                        }
                            <%--Fast AICC id="fastaicc_dwn"--%>
                        else if (downloadFlag == 'fastaicc_dwn') {
                            console.log("ddd");
                            a.href = "${pageContext.request.contextPath}/aiaas/common/files/%5Bmaum.ai%5D%20FAST%20대화형%20AI%20서비스%20소개서.pdf";
                            a.download = "[maum.ai] FAST 대화형 AI 서비스 소개서.pdf";
                        }
                        a.style.display = 'none';
                        document.body.appendChild(a);
                        a.click();
                        delete a;

                        $('.lyr_info').hide();
                    }, error: function (err) {
                        console.log("parse HTML error! ", err);
                    }
                });
            } else {
                var alertMsg = "Please fill out the form correctly.";
                alert(alertMsg);
            }
        });

        // 팝업창 쿠키 확인
        cookiedata = document.cookie;
        if (cookiedata.indexOf("ncookie=done") < 0) {
            $('.lyr_event').fadeIn(); //  팝업창 아이디
        } else {
            $('.lyr_event').hide(); // 팝업창 아이디
        }

    });
</script>

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/swiper.min.js"></script>
<script>
    $(document).ready(function () {
        applySwiper('.slide_box', {initialSlide: 0});

        function applySwiper(selector, option) {
            var defaultOption = {
                initialSlide: 0,
                slidesPerView: 1,
                slidesPerGroup: 1,
                loop: true,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
            }
            return new Swiper(selector, $.extend(defaultOption, option))
        }

        if (window.location.hash == "#ecoMINDs") {
            var swiper = applySwiper($('.slide_box'), {initialSlide: 0});
            swiper.destroy();
            applySwiper('.slide_box', {initialSlide: 1});
        }

        // ecoMINDs swiper transition
        $('.go_ecominds').on('click', function () {
            if (clicked) {
                $('a.btn_header_ham').removeClass('active');
                $('.aside').animate({
                    width: '0',
                }, {duration: 200, queue: false});
                $('.btn_goTop').show();

                $('.bg_aside').animate({
                    opacity: 0,
                }, {duration: 150, queue: false});
                $('.bg_aside').css({
                    display: 'none',
                });
                $('body').css({
                    overflow: '',
                });

                clicked = false;
            }

            var swiper = applySwiper($('.slide_box'), {initialSlide: 0});
            swiper.destroy();
            applySwiper('.slide_box', {initialSlide: 1});
        });



    });
</script>

<script type="text/javascript">
    function login() {
        var path = window.location.pathname;
        location.href = "${google_url}" + "?targetUrl=" + path;
    }
</script>

<script>
    $(document).ready(function () {
        applySwiper('.preview', {slidesPerView: 1.5});
        applySwiper('.preview2', {slidesPerView: 2.5});

        function applySwiper(selector, option) {
            var defaultOption = {
                slidesPerView: 1,
                spaceBetween: 22,
                // centeredSlides: false,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                }
            }
            return new Swiper(selector, $.extend(defaultOption, option))
        }
    });
</script>
<%-- /NEW --%>