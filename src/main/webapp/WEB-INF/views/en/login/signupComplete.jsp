<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="../common/common_login_header.jsp" %>

<!-- wrap -->
<div id="wrap">

	<%@ include file="../common/login_header.jsp" %>

	<!-- #container -->
	<section id="container">
		<!-- .content -->
		<div class="content registrationContent">
			<div class="signUpCompleteWrap">
				<div class="paymentCompleteBox en">
					<h1>You are ready to go</h1>
					<p id="register_noti">Now enjoy your <em class="ft_point_orange">one-month free trial</em><br> within maum.ai service platform.</p>
					<ul>
						<li>
                            <span>
                                <em class="fas fa-chevron-right"></em>Method
                            </span>
							<span>Card</span>
						</li>
						<li>
                            <span>
                                <em class="fas fa-chevron-right"></em>Next Payment
                            </span>
							<span>${payDate }</span>
						</li>
					</ul>
				</div>

				<div class="btnBox">
					<a id="complete_btn" href="#none" title="메인으로 이동" target="_self">Start</a>
				</div>

				<ul class="detailList">
					<li>If canceled, no refund will be given and the service will be available until the day before the next payment.<br>You will not be refunded retroactively for subscription payments, and any previously charged<br> subscription fees will not be calculated in proportion to the cancellation date.</li>
					<li>For the first month free-users, please note that all services will stop immediately when you unsubscribe.</li>
					<li>You can cancel your subscription directly from Account Menu> Payment > Unsubscribe.</li>
				</ul>
			</div>

		</div>
		<!-- //.content -->
	</section>
	<!-- //#container -->
<%--	<div class="loginWrap">--%>
<%--		<div class="login_box signup_box">--%>
<%--			<div class="txt_box">--%>

<%--                <span class="signup_desc animated fadeInDown delay-1 signup_complete">--%>
<%--					<strong>Congratulation!</strong>--%>
<%--                    <em style="font-weight:normal; font-size:18px;" id="register_noti">You can use your one-month free service from now.--%>
<%--                        <br>Your first payment date is ${payDate }.</em>--%>
<%--				</span>--%>

<%--				<button type="button" id="complete_btn" class="input_done  animated flipInY delay-1" style="width:220px;">Explore maum.ai</button>--%>
<%--			</div>--%>

<%--			<div class="enroll_noti animated fadeIn delay-2">--%>
<%--				<div class="noti_icon">--%>
<%--					<img class="surprise_face" src="${pageContext.request.contextPath}/aiaas/kr/images/ico_alarm_after_enroll_2x.png" alt="구독해지안내 아이콘"/>--%>
<%--				</div>--%>
<%--				<ul>--%>
<%--					<li>If canceled, no refund will be given and the service will be available until the day before the next payment.</li>--%>
<%--					<li>You will not be refunded retroactively for subscription payments, and any previously charged subscription<br> fees will not be calculated in proportion to the cancellation date.</li>--%>
<%--					<li>For the first month free-users, please note that all services will stop immediately when you unsubscribe.</li>--%>
<%--					<li>You can cancel your subscription directly from My Account> Payment History> Unsubscribe.</li>--%>
<%--				</ul>--%>
<%--			</div>--%>
<%--		</div>--%>
<%--		--%>
<%--	</div>--%>
	<!-- footer -->
	<%@ include file="../common/login_footer.jsp" %>
	<!-- //footer -->
</div>
<!-- //wrap -->

<script type="text/javascript">
	$(window).load(function() {
		var errorMsg = "${errMsg}";
		if(errorMsg !== ""){
			console.log(errorMsg);
			// 에러 처리 어떻게 할까?
			alert(errorMsg);
			window.location.href = "/login/enLoginMain";
		}

		$("#complete_btn").on('click',function(){
			window.location.href="${btnUri}";
		});
	});

	/**
	 * copied from kr/signupComplete.jsp and was modified by YGE
	 * */
	var freeFlag = "${freeFlag}";
	console.log(freeFlag);
	if(freeFlag == 0) {
		var $register_noti = $('#register_noti');
		$register_noti.html("Now you are ready to go. Access the most of maum.ai.");
		$register_noti.prev().text('Welcome back!')
	}
</script>

