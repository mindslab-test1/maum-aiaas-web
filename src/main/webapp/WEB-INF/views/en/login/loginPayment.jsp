<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="../common/common_login_header.jsp" %>
<%--<head>--%>
<%--	<meta name="viewport" content="width=device-width, initial-scale=1">--%>
<%--	<meta http-equiv="X-UA-Compatible" content="IE=edge" />--%>
<%--	<meta name="robots" content="noindex,nofollow"/>--%>
<%--</head>--%>

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
	<div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->


<script
		src="https://www.paypal.com/sdk/js?client-id=${paypal_client_id}&currency=USD&vault=true&disable-funding=bancontact">
</script>
<script>
	paypal.Buttons({
		createSubscription: function(data, actions) {
			// Set up the transaction
			return actions.subscription.create({
				"plan_id": "${plan_id}",
				<%--"subscriber": {--%>
				<%--	"name": {--%>
				<%--		"full_name": "${userName}"--%>
				<%--	},--%>
				<%--	"email_address": "${userEmail}"--%>
				<%--},--%>
				<%--"application_context": { //얘네 작동 하는건지 모르겠음...--%>
				<%--	"brand_name": "MindsLab",--%>
				<%--	"locale": "en-US",--%>
				<%--	"shipping_preference": "NO_SHIPPING",--%>
				<%--	"user_action": "SUBSCRIBE_NOW",--%>
				<%--	"payment_method": {--%>
				<%--		"payer_selected": "PAYPAL",--%>
				<%--		"payee_preferred": "IMMEDIATE_PAYMENT_REQUIRED"--%>
				<%--	},--%>
				<%--	"return_url": "https://maum.ai",  //"http://127.0.0.1:8080/login/enSignupComplete", //얘네 작동 하는건지 모르겠음...--%>
				<%--	"cancel_url": "http://127.0.0.1:8080/member/enProfile"								//얘네 작동 하는건지 모르겠음...--%>
				<%--}--%>
			});
		},
		onApprove: function(data) {
			// Capture the funds from the transaction
			$.ajax({
				type: "POST",
				url : "/payment/payPalBilling",
				data : {
					"${_csrf.parameterName}" : "${_csrf.token}",
					"subsc_id" : data.subscriptionID,
					"payMethod" : "${payMethod}"
				},
				dataType : "json",
				success : function(response){
					if(response != null){
						var result = response.status;

						if(result === "success"){
							window.location.href= response.redirectUri;
						}else{
							alert("Billing update fail. Please contact us.");
							window.location.href= response.redirectUri;
						}
					}
				},
				error : function(err){
					console.log("fail --- "+ err.toString());
					alert("Payment info update fail.");
				}
			});
		},
		onCancel: function (data) {
			alert('Transaction Canceled');
			console.log(data);
		},
		onError: function (err) {
			console.log(err);
			location.href = "/login/paypalPaymentErr?errMsg="+ err.toString();
		}
	}).render('#paypal-button-container');
</script>
<!-- wrap -->
<div id="wrap">
	<!-- header -->
	<%@ include file="../common/login_header.jsp" %>
	<!-- //header -->
<%--	<div class="loginWrap">--%>
<%--		<div class="login_box signup_box">--%>
<%--			<div class="txt_box">--%>
<%--				<p class="animated fadeInUp">--%>
<%--					Make your own AI Service--%>
<%--				</p>--%>

<%--				<span class="signup_desc animated fadeIn delay-1">--%>
<%--					<strong>Last step before your service. </strong>--%>
<%--					<em id="payment_noti" class="extra_desc">Don’t worry about your <span class="m_block">first 1-month trial fee. It is free!</span></em>--%>
<%--				</span>--%>


<%--				<div class="form_box paypal_box animated fadeIn delay-2">--%>
<%--					<span>*Required</span>--%>
<%--					<div class="input_box input_pay paypal_area">--%>
<%--						<div class="txtfield">--%>
<%--							<label><em>*</em>Payment</label>--%>
<%--&lt;%&ndash;							<a href="#" id="businessBilling">Register</a>&ndash;%&gt;--%>
<%--							<div id="paypal-button-container"></div>--%>
<%--						</div>--%>
<%--					</div>--%>
<%--					<p id="payment_noti2"></p>--%>
<%--				</div>--%>
<%--			</div>--%>
<%--		</div>--%>
	<!-- #container -->
	<section id="container">
		<!-- .content -->
		<div class="content registrationContent">
			<div class="signUpCompleteWrap">
				<div class="signUpConfirmBox">
					<p>Congratulations for Signing Up!</p>
					<ul>
						<li><span><em class="fas fa-chevron-right"></em>Name</span><span id="userName" class="user">${fn:escapeXml(sessionScope.accessUser.name)}</span></li>
						<li><span><em class="fas fa-chevron-right"></em>Email</span><span id="userEmail" class="user">${fn:escapeXml(sessionScope.accessUser.email)}</span></li>
					</ul>
				</div>

				<div class="txt" id="txt">Almost done! To access our services, a registered payment method is required.<br>
					The first month's services are provided as <strong>a free trial</strong>, so we won’t charge any at all.<br>
					Register now and explore more of maum.ai.
				</div>

				<div class="signUpCompleteBox">
					<h1 id="title">Try it for Free!</h1>
					<p id="payment_noti">Don’t worry about your first month fee. It’s free!</p>
					<!-- [D] paypal 버튼영역-->
					<div id="paypal-button-container" style="width:320px;margin:0 auto;"></div>
					<p class="note">* For cards issued by Korea, please <a class="ft_point_orange" href="/login/krPayment">click here</a> to continue.</p>
				</div>
				<span>* If you unsubscribed before, the payment proceeds immediately.</span>
				<div class="btnBox">
					<a class="btn_sign" href="#none" title="Sign in">Sign in</a>
					<a id="serviceMainBtn" href="/" title="go to Home" target="_self">Home</a>
				</div>
			</div>

		</div>
		<!-- //.content -->
	</section>
	<!-- //#container -->
	<!-- footer -->
	<%@ include file="../common/login_footer.jsp" %>
	<!-- //footer -->
</div>

<!-- //wrap -->
	
<script type="text/javascript">
$(window).load(function() {    
	$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() { 
		$(this).remove(); 
	});
}); 
</script>
<script type="text/javascript">

jQuery.event.add(window,"load",function(){
	$(document).ready(function (){

		let $ClientId = "${client_id}";
		let $RedirectUri = "${redirect_uri}";
		let $SsoUrl = "${sso_url}";
		//해더 로그인 버튼
		$('.btn_sign').on('click',function(){
			var stateVal = uuidv4();
			location.href = $SsoUrl+"/maum/oauthLoginMain" + "?response_type=code&client_id=" + $ClientId + "&redirect_uri=" + encodeURIComponent($RedirectUri);
		});
		function uuidv4() {
			return ([1e7]+-1e3+-4e3+-8e3+-1e11).replace(/[018]/g, c =>
					(c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
			);
		}

		var payMethod = "${payMethod }";

		if (payMethod == null) {
			alert("Can't get info.");
		} else {
			if (payMethod == "INSTANT") {
				$('.signUpConfirmBox').hide();
				$('#txt').hide();
				$('#title').text('Setup your payment');
				$('#payment_noti').html("Welcome back to maum.ai!<br>Update your payment details and explore our services.");

				// $('#payment_noti').text("When re-enrolled the service, the business plan fee will be charged");
				// $('#payment_noti2').text("When re-enrolled the service, the business plan fee will be charged.");
			}
		}
	});	
});


</script>
	
