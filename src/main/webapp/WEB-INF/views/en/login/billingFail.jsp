<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<%@ include file="../common/common_login_header.jsp" %>


<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
	<div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<!-- wrap -->
<div id="wrap">
	<%@ include file="../common/login_header.jsp" %>
	<!-- #container -->
	<section id="container">
		<!-- .content -->
		<div class="content registrationContent">
			<div class="signUpCompleteWrap">
				<div class="paymentCompleteBox failed">
					<h1>
						<em class="fas fa-exclamation-circle"></em> Unexpected error has occurred
					</h1>
					<p>Payment verification failed. Please contact us for more details. (support@mindslab.ai)</p>
<%--					<ul>--%>
<%--						<li>--%>
<%--                            <span>--%>
<%--                                <em class="fas fa-chevron-right"></em>실패사유--%>
<%--                            </span>--%>
<%--							<span>${reason }</span>--%>
<%--						</li>--%>
<%--						<li>--%>
<%--                            <span>--%>
<%--                                <em class="fas fa-chevron-right"></em>문의--%>
<%--                            </span>--%>
<%--							<span>support@mindslab.ai</span>--%>
<%--						</li>--%>
<%--					</ul>--%>
				</div>

				<div class="btnBox">
					<a href="/login/enPayment#none">Back</a>
				</div>
			</div>

		</div>
		<!-- //.content -->
	</section>
	<!-- //#container -->
<%--	<section id="container">--%>
<%--		<!--.demobox-->--%>
<%--		<div class="login_box">--%>
<%--			<!-- .priceBox -->--%>
<%--			<div class="failure_box">--%>
<%--				<h1><em class="fas fa-exclamation-circle"></em> 결제 정보 등록에 실패하였습니다</h1>--%>
<%--				<span style="font-size: 15px;margin: 20px auto; width: 100%; display: block; text-align: center;color: #2c3f51;">실패사유를 확인하신 후 재시도를 하시거나, 계속 실패되시는 경우 고객센터로 문의주시기 바랍니다.</span>--%>
<%--				<table class="failure_tb" style="width:100%; margin:30px 0 0 0;">--%>
<%--					<tr>--%>
<%--						<th>실패사유</th>--%>
<%--						<td>${reason }</td>--%>
<%--					</tr>--%>
<%--					<tr>--%>
<%--						<th>주문번호</th>--%>
<%--						<td> ${moid }</td>--%>
<%--					</tr>--%>
<%--					<tr>--%>
<%--						<th>문의</th>--%>
<%--						<td class="emaildesc">support@mindslab.ai</td>--%>
<%--					</tr>--%>
<%--				</table>--%>
<%--				<a href="javascript:history.back();">돌아가기</a>--%>
<%--			</div>--%>
<%--			<!-- //.priceBox -->--%>
<%--		</div>--%>
<%--		<!--//.demobox-->--%>
<%--	</section>--%>
	<!-- footer -->
	<%@ include file="../common/login_footer.jsp" %>
	<!-- //footer -->
</div>
<!-- //wrap -->
	
<script type="text/javascript">
$(window).load(function() {    
	$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() { 
		$(this).remove(); 
	});
}); 
</script>

