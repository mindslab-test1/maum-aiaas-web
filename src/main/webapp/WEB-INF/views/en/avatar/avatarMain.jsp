<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-08-27
  Time: 08:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!--.pop_simple 얼굴 움직임 영상 파일 알림창 -->
<div class="pop_simple" id="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">Close</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>Upload error</h5>
            <p>Error occurred due to the image or file size. <br>Please check the restriction again.</p>
            <span>* Supported files: .mp4<br>
                 * Video size: 30MB or less<br>
                * Video length: 20 seconds or less</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">OK</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .pop_simple 특정 인물 사진 파일 알림창 -->
<div class="pop_simple" id="pop_simple2">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">Close</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>Upload error</h5>
            <p>Error occurred due to the image or file size. <br>Please check the restriction again.</p>
            <span>* Supported files: .jpg, .png<br>
                 * Image file size: 2MB or less<br>
                 * Face must occupy more than 1/4 of the total image</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">OK</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->
<!-- .contents -->
<div class="contents api_content">
    <div class="content api_content">
        <h1 class="api_tit vision_lst">Face-to-Face Avatar</h1>

        <!-- .demobox -->
        <div class="demobox dropEg vis_avatar">
            <p><span>Face-to-Face Avatar</span></p>
            <span class="sub">This is an engine whereby you can generate a video from a target image to match the movements from another source video.<br> You could generate interesting videos of certain people with this engine.</span>

            <div class="demoCont process">
                <h4>Process</h4>
                <div class="processBox">
                    <div class="stepBox step1">
                        <p class="tit">Step 1 <span>(Input)</span></p>

                        <div class="cont">
                            <p class="fileType">Video File</p>
                            <div class="videoBox">
                                <video controls="" width="272" height="153"
                                       poster="${pageContext.request.contextPath}/aiaas/common/images/img_aiavatar_thumb.png">
                                    <source src="${pageContext.request.contextPath}/aiaas/common/video/avatar_sample_.mp4"
                                            type="video/mp4">
                                    IE 8 and below does not produce video. Please update the IE version.
                                </video>
                            </div>

                            <em class="fas fa-plus"></em>

                            <p class="fileType">Image File</p>
                            <div class="imgBox">
                                <img src="${pageContext.request.contextPath}/aiaas/common/images/img_avatar1.png"
                                     alt="sample image">
                            </div>
                        </div>
                    </div>

                    <div class="stepBox step2">
                        <p class="tit">Step 2 <span>(Action)</span></p>

                        <div class="cont">
                            <img class="egIcon"
                                 src="${pageContext.request.contextPath}/aiaas/kr/images/menu/ico_vis_avatar.svg"
                                 alt="Face-to-Face Avatar">
                            <p class="egName">Face-to-Face Avatar</p>
                        </div>
                    </div>

                    <div class="stepBox step3">
                        <p class="tit">Step 3 <span>(Completion)</span></p>

                        <div class="cont">
                            <p class="fileType">Result</p>
                            <div class="videoBox">
                                <video controls="" width="272" height="153">
                                    <source src="${pageContext.request.contextPath}/aiaas/common/video/avatar_result_sample.mp4"
                                            type="video/mp4">
                                    IE 8 and below does not produce video. Please update the IE version.
                                </video>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="demoCont strength">
                <h4> Key Features</h4>
                <ul class="comm_lst">
                    <li>
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_vis_avatar_strength01.svg"
                             alt="strength image">
                        <p class="tit">Accurate face recognition</p>
                        <p class="desc">Provides an accurate face recognition<br> of the person within the target image.
                        </p>
                    </li>
                    <li>
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_vis_avatar_strength02.svg"
                             alt="strength image">
                        <p class="tit">Detailed movements</p>
                        <p class="desc">It can mimic facial<br> expressions and movements.</p>
                    </li>
                    <li>
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_vis_avatar_strength03.svg"
                             alt="strength image">
                        <p class="tit">Ease of use</p>
                        <p class="desc">An interesting video could be<br> generated with only one target image.</p>
                    </li>
                </ul>
            </div>

            <div class="demoCont useCase">
                <h4>Potential usage</h4>
                <ul class="comm_lst">
                    <li>
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_vis_avatar_useCase01.png"
                             alt="use case image">
                        <p class="tit">News</p>
                    </li>
                    <li>
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_vis_avatar_useCase02.png"
                             alt="use case image">
                        <p class="tit">VR / Game Characters</p>
                    </li>
                    <li>
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_vis_avatar_useCase03.png"
                             alt="use case image">
                        <p class="tit">Video Contents</p>
                    </li>
                </ul>
            </div>
        </div>
        <!-- //.demobox -->
    </div>
    <!-- //.contents -->
    <%--<div class="content">
        <h1 class="api_tit">Face-to-Face Avatar</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'avatardemo')" id="defaultOpen">
                <button type="button">AI Engine</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'avatarexample')">
                <button type="button">Use Case</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'avatarmenu')">
                <button type="button">Manual</button>
            </li>
            &lt;%&ndash;            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>&ndash;%&gt;
        </ul>
        <!-- .demobox -->
        <div class="demobox" id="avatardemo">
            <p><span>Face-to-Face Avatar</span></p>
            <span class="sub">The engine creates a video of the specific portrait image mirroring the facial motions from the input video.</span>
            <!--avatar_box-->
            <div class="demo_layout avatar_box">
                <!--tr_1-->
                <div class="tr_1">
                    <div class="fl_box">
                        <p><em class="far fa-file-video"></em><strong>Use Sample File</strong></p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="top_box">
                                    <p>Video with Facial Motions</p>
                                    <video id="sampleVideo1" controls width="324" height="180" poster="${pageContext.request.contextPath}/aiaas/common/images/img_aiavatar_thumb.png">
                                        <source src="${pageContext.request.contextPath}/aiaas/common/video/avatar_sample_.mp4"
                                                type="video/mp4">
                                        IE 8 and below does not produce video. Please update the IE version.
                                    </video>
                                    <em class="fas fa-plus"></em>
                                </div>
                                <div class="under_box">
                                    <p>Specific Portrait Image File</p>
                                    <div class="radio">
                                        <input type="radio" id="sample1" name="sample" value="1" checked>
                                        <label for="sample1" class="">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_avatar1.png" alt="sample1" />
                                            <span>Sample 1</span>
                                        </label>
                                    </div>
                                    <div class="radio">
                                        <input type="radio" id="sample2" name="sample" value="2">
                                        <label for="sample2" class="">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_avatar2.png" alt="sample2" />
                                            <span>Sample 2</span>
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-video"></em><strong>Use My File</strong></p>
                        <div class="uplode_box">
                            <div class="top_box">
                                <p>Video with Facial Motions</p>
                                <div class="btn" id="uploadFile">
                                    <em class="fas fa-times hidden close"></em>
                                    <em class="far fa-file-image hidden"></em>
                                    <input type="file" id="demoFile" class="demoFile" accept=".mp4">
                                    <label for="demoFile" class="demolabel">Upload File</label>
                                </div>
                                <ul>
                                    <li>* Supported files: .mp4</li>
                                    <li>* Video size: 30MB or less</li>
                                    <li>* Video length: 20 seconds or less</li>
                                </ul>
                                <em class="fas fa-plus"></em>
                            </div>
                            <div class="under_box">
                                <p>Specific Portrait Image File</p>
                                <div class="btn" id="uploadFile2">
                                    <em class="fas fa-times hidden close"></em>
                                    <em class="far fa-file-image hidden"></em>
                                    <input type="file" id="demoFile2" class="demoFile" accept=".jpg, .png">
                                    <label for="demoFile2" class="demolabel">Upload File</label>
                                </div>
                                <ul>
                                    <li>* Supported files: .jpg, .png</li>
                                    <li>* Image file size: 2MB or less</li>
                                    <li>* Face must occupy more than 1/4 of the total image</li>
                                </ul>
                                <span class="desc">* The higher the resolution, the better the quality of the generated video.</span>
                            </div>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub_avatar">Generate</button>
                    </div>
                </div>
                <!--tr_1-->
                <!--tr_2-->
                <div class="tr_2">
                    <p><em class="far fa-file-video"></em>In progress</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px"
                             viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42"
                                                                             d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/>
                            <g>
                                <path fill="#f7778a" fill-opacity="1"
                                      d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/>
                                <animatetransform attributeName="transform" type="translate"
                                                  values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0"
                                                  calcMode="discrete" dur="1820ms" repeatCount="indefinite"/>
                            </g></svg>

                        <p>AI processing takes a while.. (About 60 seconds)</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
                    </div>

                </div>
                <!--tr_2-->
                <!--tr_3-->
                <div class="tr_3">
                    <div class="result_file">
                        <div class="input_box">
                            <p><em class="far fa-file-video"></em>Input</p>
                            <div class="file_box">
                                <div class="inner">
                                    <p>Video with Facial Motions</p>
                                    <video id="inputVideo" controls width="324" height="180">
                                        <source src="" type="video/mp4">
                                        IE 8 and below does not produce video. Please update the IE version.
                                    </video>
                                </div>
                                <em class="fas fa-plus"></em>
                                <div class="inner">
                                    <p>Specific Portrait Image File</p>
                                    <img id="inputImg" src="" alt="Specific Portrait Image File" />
                                </div>

                            </div>
                        </div>
                        <div class="result_box">
                            <p><em class="far fa-file-video"></em>Result</p>
                            <div class="file_box">
                                <video id="outputVideo" controls width="462" height="256">
                                    <source src="" type="video/mp4">
                                    IE 8 and below does not produce video. Please update the IE version.
                                </video>
                                <a id="download_avatar" href="" class="dwn" download="avatar.mp4" ><em class="far fa-arrow-alt-circle-down"></em> Download Result</a>
                            </div>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2" id=""><em class="fas fa-redo"></em>Reset</button>
                    </div>
                </div>
                <!--tr_3-->
            </div>
            <!--//.avatar_box-->

        </div>
        <!-- //.demobox -->
        <!--.avatarmenu-->
        <div class="demobox vision_menu" id="avatarmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API Guideline
                    </div>
                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID &amp; Key</p>
                    <p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                    <p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                    <p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>
                <div class="guide_group">
                    <div class="title">Face-to-Face Avatar</div>
                    <p class="sub_txt">The engine creates a video of the specific portrait image mirroring the facial motions from the input video.</p>
                    <span class="sub_title">
						 Preparation
					</span>
                    <p class="sub_txt">- Input 1: Video with Facial Motions</p>
                    <ul>
                        <li>File type: .mp4</li>
                        <li>Size: Under 30MB</li>
                        <li>Length: Under 20 seconds</li>
                    </ul>
                    <p class="sub_txt">- Input 2: Specific Portrait Image File</p>
                    <ul>
                        <li>File type: .jpg, .png</li>
                        <li>Size: Under 2MB</li>
                        <li>Face must occupy more than 1/4 of the total image</li>
                    </ul>
                    <span class="sub_title">
                        API Document
					</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/avatar/download</li>
                    </ul>
                    <p class="sub_txt">② Request parameters </p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API ID. Request from is required for the ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>video</td>
                            <td>Input 1: Video with Facial Motions</td>
                            <td>file</td>
                        </tr>
                        <tr>
                            <td>images</td>
                            <td>Input 2: Specific Portrait Image File (Max: 5)</td>
                            <td>file</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                        curl --location --request POST 'http://api.maum.ai/avatar/download' \<br>
                        --form 'apiId= {Own API Id}'<br>
                        --form 'apiKey= {Own API Key}'<br>
                        --form 'images= {image file path}' \<br>
                        --form 'images= {image file  path}' \<br>
                        --form 'images= {image file  path}' \<br>
                        --form 'images= {image file  path}' \<br>
                        --form 'images= {image file  path}' \<br>
                        --form 'video= {video file  path}'
                    </div>

                    <p class="sub_txt">④ Response example </p>

                    <div class="code_box">
                        (.mp4 File Download)
                    </div>

                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.avatarmenu-->
        <!--.avatarexample-->
        <div class="demobox" id="avatarexample">
            <p><em style="color:#f7778a;font-weight: 400;">Use Cases</em></p>
            <span class="sub">Find out how AI can be applied to diverse area.</span>
            <!-- avatar -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>Video Production </span>
                            </dt>
                            <dd class="txt">When creating an informational video, such as news content or an interview, you can present it with the just the voice to not directly reveal the speaker.
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_tts"><span>TTS</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>VR, Game character</span>
                            </dt>
                            <dd class="txt">Whether in VR or in a game, you can easily have the faces you want for your NPCs.</dd>

                        </dl>
                    </li>

                </ul>
            </div>
            <!--avatar  -->
        </div>
        <!--//.avatarexample-->
    </div>
    <!-- //.content -->--%>
</div>
<script>

    var sample1SrcUrl;
    var sample2SrcUrl;
    var sample3SrcUrl;
    var sample1File;
    var sample2File;
    var sample3File;

    var request;

    function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/common/video/avatar_sample_.mp4");
        xhr.responseType = "blob";
        xhr.onload = function () {
            blob = xhr.response;
            sample1File = new File([blob], "avatar_sample_.mp4");
            sample1SrcUrl = URL.createObjectURL(blob);

            var demo1 = document.getElementById('demoFile');
            demo1.setAttribute("src", sample1SrcUrl);
        };
        xhr.send();
    }

    //sample2 (세종대왕)
    function loadSample2() {

        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.responseType = "blob";
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/common/images/img_avatar1.png");
        xhr.onload = function () {
            blob = xhr.response;
            sample2File = new File([blob], "img_avatar1.png");
            sample2SrcUrl = URL.createObjectURL(blob);
        };
        xhr.send();
    }

    //sample3 (의문의 남성)
    function loadSample3() {

        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.responseType = "blob";
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/common/images/img_avatar2.png");
        xhr.onload = function () {
            blob = xhr.response;
            sample3File = new File([blob], "img_avatar2.png");
            sample3SrcUrl = URL.createObjectURL(blob);
        };
        xhr.send();
    }

    function sendApiRequest(videoFile, imageFile) {

        var formData = new FormData();
        formData.append('video', videoFile);
        formData.append('image', imageFile);
        formData.append('${_csrf.parameterName}', '${_csrf.token}');

        request = new XMLHttpRequest();
        request.responseType = "blob";
        request.onreadystatechange = function() {

            if(request.readyState === 4 && request.status === 200) {

                if(request.response == null){
                    alert("서버에서 응답을 받아오지 못했습니다. 다시 시도해 주세요");
                    window.location.reload();
                }

                let resultBlob = new Blob([request.response], {type: 'video/mp4'});
                let srcUrl = URL.createObjectURL(resultBlob);
                $('#outputVideo').attr('src', srcUrl);
                $('#download_avatar').attr('href', srcUrl);

                $('.avatar_box').css('border', 'none');

                $('.tr_2').hide();
                $('.tr_3').fadeIn(300);

                $('#outputVideo').get(0).play();
            }
        };
        request.open('POST', '/api/avatar/getAvatarApi');
        request.send(formData);
        request.timeout = 2500000;

        request.ontimeout = function() {
            alert('timeout');
            window.location.reload();
        };
    }

    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {

            loadSample1();
            loadSample2();
            loadSample3();

            $('input[type="radio"]').on('click', function () {
                $('em.close').trigger('click');
            });

            $('#sub_avatar').on('click', function () {

                let uploadVideoFlag = $('#uploadFile .demolabel').text();
                let uploadImageFlag = $('#uploadFile2 .demolabel').text();

                let videoFile;
                let imageFile;

                // 파일을 하나라도 업로드한 경우
                if(uploadVideoFlag !== "Upload File" || uploadImageFlag !== "Upload File") {    //업로드 확인
                    if (uploadVideoFlag === "Upload File") {
                        alert("Upload Video with Facial Motions.");
                        return;
                    }
                    else if (uploadImageFlag === "Upload File") {
                        alert("Upload Specific Portrait Image File.");
                        return;
                    }

                    videoFile = $('#demoFile').get(0).files[0];
                    imageFile = $('#demoFile2').get(0).files[0];

                } else {

                    if($('input[name="sample"]:checked').val() === undefined){
                        alert("Please select a sample or upload a file!");
                        return;
                    }

                    let radioCheckValue = document.getElementsByName('sample');

                    videoFile = sample1File;

                    if( radioCheckValue[0].checked == true ) {
                        imageFile = sample2File;
                    } else {
                        imageFile = sample3File;
                    }
                }

                $('#inputVideo').attr('src', URL.createObjectURL(videoFile));
                $('#inputImg').attr('src', URL.createObjectURL(imageFile));

                $('.tr_1').hide();
                $('.tr_2').fadeIn();

                sendApiRequest(videoFile, imageFile);
            });

            // close button
            $('em.close').on('click', function () {

                $('.fl_box').css("opacity", "1");
                $(this).parent().removeClass("btn_change").addClass("btn");
                $(this).parent().children('.demolabel').text('Upload File');
                $(this).parent().find('input').val(null);

            });

            // $('.sample_box').on('click', function () {
            //     $('em.close').trigger('click');
            // });

            // 처음으로 버튼
            $('.btn_back1').on('click', function () {
                request.abort();

                $('.tr_2').hide();
                $('.tr_1').fadeIn(300);

            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('#outputVideo').get(0).pause();
                $('.tr_3').hide();
                $('.tr_1').fadeIn(300);
            });


            //알림 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });

        });
    });

    $(document).ready(function () {
        //파일 업로드
        $('.demoFile').each(function () {
            var $input = $(this);
            var $inputId= $input.attr('id');
            var $label = $input.next('.demolabel');

            $input.on('change', function(element) {
                let files = element.target.files[0];
                // console.log(files);
                let labeltxt =files.name;
                let $demoFileSize = files.size;
                let max_demoFileSize;

                $('.fl_box').css("opacity", "0.5");
                if ($inputId == "demoFile") {

                    max_demoFileSize = 1024 * 1024 * 30;//1kb는 1024바이트

                    if ($demoFileSize > max_demoFileSize || (!files.type.match(/video.mp4*/))) {
                        $('#pop_simple').show();
                        // 팝업창 닫기
                        $('.pop_close, .pop_bg, .btn a').on('click', function () {
                            $('.pop_simple').fadeOut(300);
                            $('body').css({
                                'overflow': ''
                            });
                        });
                        $('#' + $inputId).val(null);

                    } else {

                        $label.html(labeltxt);

                        $(this).parent().removeClass('btn').addClass('btn_change');
                    }
                } else if ($inputId == "demoFile2") {

                    max_demoFileSize = 1024 * 1024 * 2;//1kb는 1024바이트

                    if ($demoFileSize > max_demoFileSize || (!$input[0].files[0].type.match(/image.jp*/) && !$input[0].files[0].type.match(/image.png/))) {
                        $('#pop_simple2').show();
                        // 팝업창 닫기
                        $('.pop_close, .pop_bg, .btn a').on('click', function () {
                            $('.pop_simple').fadeOut(300);
                            $('body').css({
                                'overflow': ''
                            });
                        });
                        $('#' + $inputId).val('');

                    } else {
                        $label.html(labeltxt);

                        $(this).parent().removeClass('btn').addClass('btn_change');
                    }
                }
                $('input[type="radio"]').prop('checked', false);

            });
        });
    });

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

</script>