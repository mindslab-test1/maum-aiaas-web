<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <!-- Cache reset -->
    <meta http-equiv="Expires" content="Mon, 06 Jan 2016 00:00:01 GMT">
    <meta http-equiv="Expires" content="-1">
    <meta http-equiv="Pragma" content="no-cache">
    <meta http-equiv="Cache-Control" content="no-cache">
    <!-- Open Graph Tag -->
    <meta property="og:title" content="maum AI Book Free Giveaway Event">
    <meta property="og:type" content="website"> <!-- 웹 페이지 타입 -->
    <meta property="og:url" content="">
    <meta property="og:image" content="common/images/maumBook/img_svc_prev.png">
    <meta property="og:description" content="To learn more about success stories of implementing AI with maum AI, enter the giveaway event right now!">

    <!---------------------- General Resources ---------------------->
    <!-- General CSS -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/swiper.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/maumBookEvent.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/common/css/all.css">
    <!-- General Script -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/jquery-migrate-3.3.1.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/swiper.min.js"></script>

    <title>Event || Maum Book</title>

    <!-- Facebook Pixel Code -->
    <script>
        !function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ? n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            }
            ;
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2585314051771727');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=2585314051771727&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->

</head>
<body>
<!-- .page loading -->
<div id="pageldg">
    <span class="out_bg">
        <em>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <strong>&nbsp;</strong>
            <b>&nbsp;</b>
        </em>
    </span>
</div>
<!-- //.page loading -->

<!-- #wrap -->
<div id="wrap" class="event en_ver">
    <!-- #contents -->
    <div id="contents">
        <div class="stn intro">
            <div class="contBox">
                <div class="msg_lead">
                    <p>&ldquo;Will I be able to introduce AI into my business?&rdquo;</p>
                    <p>&ldquo;How can I implement AI?&rdquo;</p>
                    <p>&ldquo;Do I have to develop AI technology myself?&rdquo;</p>
                </div>

                <div class="svc_start">
                    <p class="msg_key">No need to worry.</p>
                    <p class="msg_key">The AI platform maum AI can solve it.</p>

                    <img src="${pageContext.request.contextPath}/aiaas/en/images/logo_maumAi_en.png" alt="maum AI logo" class="maum_logo">
                    <p class="msg_start">The best way to introduce artificial intelligence, maum AI</p>
                    <p class="msg_start">Would you like to visit maum ai?</p>

                    <div class="btnBox">
                        <a href="https://maum.ai" title="maum AI">No</a>
                        <a href="#none" class="btn_go_event">Yes!</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="stn success_cases">
            <div class="contBox">
                <p class="sub_tit">Many companies have already succeeded in implementing AI together with maum AI.</p>
                <strong class="main_tit">AI success stories!</strong>

                <!-- .scs_cases_box -->
                <div class="scs_cases_box swiper-container">
                    <ul class="cases_lst swiper-wrapper">
                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>Air Service</span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_incheonAirport.png" alt="Incheon Airport logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name">Incheon Airport Control Center Messaging System</p>
                                    <p class="svc_desc">Built with maum AI's communication visualization technology and
                                        text analysis technology.</p>
                                    <div class="viewBox dualItem">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_ic_aprt01.png"
                                                 alt="Incheon Airport success case01">
                                            <p>Real-time monitoring for aerial communication<br> between controllers and
                                                pilots.</p>
                                        </div>
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_ic_aprt02.png" alt="Incheon Airport success case02">
                                            <p>Real-time aircraft safety monitoring<br> for areas with radio
                                                interference.</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>
<%--                        <li class="swiper-slide">--%>
<%--                            <dl class="svc_detail">--%>
<%--                                <dt>--%>
<%--                                    <div class="category_label">--%>
<%--                                        <span>Public Enterprise</span>--%>
<%--                                    </div>--%>
<%--                                    <div class="logoBox">--%>
<%--                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_kbs.svg" alt="KBS logo">--%>
<%--                                    </div>--%>
<%--                                </dt>--%>
<%--                                <dd>--%>
<%--                                    <p class="svc_name">KBS Digital viewer column broadcasts</p>--%>
<%--                                    <p class="svc_desc">Broadcast with high-quality TTS voice generation technology.</p>--%>
<%--                                    <div class="viewBox">--%>
<%--                                        <div class="viewItem">--%>
<%--                                            <button type="button" class="btn_video_play" title="play button"></button>--%>
<%--                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_kbs.png" alt="KBS success case">--%>
<%--                                            <p>AI voices that are as natural and smooth as a real person's</p>--%>
<%--                                        </div>--%>
<%--                                    </div>--%>
<%--                                </dd>--%>
<%--                            </dl>--%>
<%--                        </li>--%>
                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>Manufacturing</span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_posco.svg" alt="POSCO logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name">Posco Smart Factory</p>
                                    <p class="svc_desc">Real-time factory monitoring technology has<br> improved the
                                        economic power of the manufacturing process</p>
                                    <div class="viewBox">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_posco.png" alt="POSCO success case">
                                            <p>Introducing AI Smart Factory for productivity and cost reduction</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>
                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>Finance</span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_hanaBank.svg" alt="hanabank logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name svc_label">Hana Bank HAI Banking <span>For <strong>5</strong> years</span>
                                    </p>
                                    <p class="svc_desc">Established HAI banking, an AI bank service,<br> that
                                        encompasses the latest AI technologies in language and vision.</p>
                                    <div class="viewBox">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_hanaBank.png" alt="hanabank success case">
                                            <p>Building AI financial services that are<br> different from overseas remittances to utility bill payments</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>
                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>Finance</span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_hyundai.svg" alt="Hyundai Marine logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name svc_label">Hyundai Marine AI Voice Bot
                                        <span>For <strong>4</strong> years</span></p>
                                    <p class="svc_desc">Established an AI counsultation service<br> that combines
                                        real-time STT technology and chatbot technology.</p>
                                    <div class="viewBox">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_hyundai.png" alt="Hyundai Marine success case">
                                            <p>AI voice bot's automated service satisfies both customers and consultant</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>
                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>Local Government</span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_seoul.svg" alt="Seoul Metropolitan City logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name">Seoul deep learning vehicle recognition</p>
                                    <p class="svc_desc">Made with maum AI's vehicle recognition solution and video
                                        analysis system.</p>
                                    <div class="viewBox">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_seoul.png" alt="Seoul Metropolitan City success case">
                                            <p>Reduced traffic volume of old diesel vehicles by 30 per cent.</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>
                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>Local Government</span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_suwon.svg" alt="Suwon City logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name">Abnormal Behaviour CCTV for Suwon City</p>
                                    <p class="svc_desc">Introduced an AI abnormal behavior detection and analysis
                                        system.</p>
                                    <div class="viewBox">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_suwon.png"
                                                 alt="Suwon City success case">
                                            <p>Improve the city's safety and inspection rate through the AI abnormal
                                                behavior CCTV system.</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>
                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>Local Government</span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_kyeongnam.svg" alt="Gyeongnam logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name">Mobile AI meeting minutes sytem in Gyeongsangnam-do provincial
                                        government</p>
                                    <p class="svc_desc">High Capacity - High performance AI minutes system has been<br>
                                        applied to wireless microphones that can be accessed through multiple
                                        connections.</p>
                                    <div class="viewBox">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_kyeongnam.png" alt="Gyeongnam success case">
                                            <p>Smart AI minutes to increase work efficiency</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>
                        <li class="swiper-slide">
                            <dl class="svc_detail">
                                <dt>
                                    <div class="category_label">
                                        <span>Local Governemt</span>
                                    </div>
                                    <div class="logoBox">
                                        <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/logo_ulju.png" alt="Ulsan Metropolitan City logo">
                                    </div>
                                </dt>
                                <dd>
                                    <p class="svc_name">Ulsan City Ulju-gun Mobile On Device AI minutes system</p>
                                    <p class="svc_desc">Implemented a highly portable conference equipment with a smart
                                        microphone.</p>
                                    <div class="viewBox">
                                        <div class="viewItem">
                                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_cases_ulju.png" alt="Ulsan Metropolitan City success case">
                                            <p>AI minutes that is easy to work with through real-time documentation</p>
                                        </div>
                                    </div>
                                </dd>
                            </dl>
                        </li>
                    </ul>
                    <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-pagination"></div>
                </div>
                <!-- //.scs_cases_box -->
            </div>
        </div>

        <div class="stn strength">
            <div class="contBox">
                <p class="sub_tit">Solving the worries about how to implement artificial intelligence</p>
                <strong class="main_tit">The 3 innovations unique to maum AI</strong>
                <ul class="strt_lst">
                    <li>
                        <div class="lst_cont">
                            <span>Implementation of various services through more than 30 artificial intelligence technologies</span>
                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/ico_strt01.svg" alt="strength icon">
                        </div>
                    </li>
                    <li>
                        <div class="lst_cont">
                            <span>Product performance and stability verified by companies all around the world</span>
                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/ico_strt02.svg" alt="strength icon">
                        </div>
                    </li>
                    <li>
                        <div class="lst_cont">
                            <span>Free consultation support from design to implementation</span>
                            <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/ico_strt03.svg" alt="strength icon">
                        </div>
                    </li>
                </ul>
            </div>
        </div>

        <div class="stn eventBanner">
            <div class="contBox">
                <p class="sub_tit">Check all the tips to success with our</p>
                <strong class="main_tit">maum AI book</strong>
                <div class="imgBox">
                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/maumBook/img_maumAi_book.png" alt="maum AI book">
                </div>
                <p class="desc_txt">The Maum AI Book contains the success cases of<br> how companies in need of
                    technological innovation have introduced AI.</p>
            </div>
        </div>

        <div class="stn contact">
            <div class="contBox">
                <p class="sub_tit">Many companies have already succeeded in introducing artificial intelligence through
                    maum AI.</p>
                <strong class="main_tit">Receive a free maum AI book if you leave a consultation inquiry.</strong>
                <div class="contactform">
                    <dl>
                        <dt>Acquisition Method<span>(required)</span></dt>
                        <dd>
                            <div class="radioBox">
                                <input type="radio" name="contact_type" id="eBook" value="ebook" checked>
                                <label for="eBook">E-book</label>
                            </div>
<%--
                            <div class="radioBox">
                                <input type="radio" name="contact_type" id="maumBook" value="mail">
                                <label for="maumBook">Mail</label>
                            </div>
--%>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Name<span>(required)</span></dt>
                        <dd><input type="text" id="userName" class="ipt_txt" autocomplete="off"
                                   placeholder="Please input your name."></dd>
                    </dl>
                    <dl>
                        <dt>Contact<span>(required)</span></dt>
                        <dd><input type="tel" id="userTel" class="ipt_tel"
                                   placeholder="Please input your contact without '-'."></dd>
                    </dl>
                    <dl>
                        <dt>Inquiry Content<em>(Optional)</em></dt>
                        <dd>
                            <textarea id="usertxt" placeholder="Please input your inquiry."></textarea>
                        </dd>
                    </dl>
                    <dl>
                        <dt>Consent to collect personal information</dt>
                        <dd>
                            <div class="assentBox">
                                <div class="checkBox">
                                    <input type="checkbox" name="assent" id="assent">
                                    <label for="assent">I agree.</label>
                                </div>
                                <p>Personal Information to be Collected: Name, Contact
                                    <br>
                                    The personal information you fill out will be kept for 1 year for receiving
                                    inquiries and resolving customer complaints.
                                    <br>
                                    You can reject the consent, but if you do not agree, we will not be able to accept
                                    any inquiries.
                                    <br>
                                    <br>
                                    <span class="ft_point01">Professional consultants will contact you individually within 3 business days.</span>
                                </p>
                            </div>
                        </dd>
                    </dl>
                    <!-- [D] 문의하기 버튼 클릭 시 성공적으로 문의접수 된 경우 alert 등장 (문의가 완료되었습니다. 버튼:확인 -> 확인버튼 클릭 시에는 maum.ai 홈페이지로 이동) -->
                    <button type="button" id="inquireBtn" disabled="true">Inquire</button>
                </div>
                <img src="${pageContext.request.contextPath}/aiaas/en/images/logo_maumAi_en.png" alt="maum AI logo">
            </div>
        </div>
    </div>
    <!-- //#contents -->

    <!-- 신청 바로가기 버튼 -->
    <button type="button" class="btn_go_contact">
        <span>consultation inquiry</span>
    </button>
    <!-- //신청하기 버튼 -->
</div>
<!-- //#wrap -->

<!---------------------- Local resources ---------------------->
<!-- page Landing -->
<script type="text/javascript">
    $(window).load(function () {
        //page loading delete  
        $('#pageldg').addClass('pageldg_hide').delay(300).queue(function () {
            $(this).remove();
        });
    });
</script>

<!-- Local Script -->
<script type="text/javascript">
    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {
            // 동영상 레이어 팝업
            $('.btn_video_play').on('click', function () {
                $('body').css('overflow', 'hidden');

                $('body').append(
                    '<div class="lyr_bg_dim"></div>\
                    <div id="lyr_video_player" class="lyrPlayBox">\
                        <div class="lyr_ct">\
                            <video controls autoplay>\
                                <source src="${pageContext.request.contextPath}/aiaas/common/video/kbs_success_case.mp4" type="video/mp4">\
                            </video>\
                        </div>\
                        <button type="button" class="btn_lyr_close">\
                            <em class="fas fa-times" title="close button"></em>\
                        </button>\
                    </div>');

                $('.btn_lyr_close, .lyr_bg_dim').on('click', function () {
                    $('body').css('overflow', '');
                    $('.lyr_bg_dim').remove();
                    $('#lyr_video_player').remove();
                });
            });

            //Layer popup open
            $('.btn_lyr_open').on('click', function () {
                var winHeight = $(window).height() * 0.7,
                    hrefId = $(this).attr('href');

                $('body').css('overflow', 'hidden');
                $('body').find(hrefId).wrap('<div class="lyrWrap"></div>');
                $('body').find(hrefId).before('<div class="lyr_bg"></div>');

                //Layer popup close
                $('.btn_lyr_close, .lyr_bg').on('click', function () {
                    $('body').css('overflow', '');
                    $('body').find(hrefId).unwrap();
                    $('.lyr_bg').remove();
                });
            });

            //Layer popup close
            var assentClicked = false;
            $('#assent').on('click', function () {
                if (!assentClicked) {
                    $('#btn_request').prop('disabled', '');
                    assentClicked = true;
                } else {
                    $('#btn_request').prop('disabled', 'disabled');
                    assentClicked = false
                }
            });

            // intro에서 오디오 재생 버튼
            var audio = document.getElementById('ava_audio');
            $('.btn_play').on('click', function () {
                audio.play();
            });

            // intro에서 이벤트 페이지 이동
            $('.btnBox .btn_go_event').on('click', function () {
                $('html').animate({scrollTop: 0,}, 500);
                $('.intro').hide();
                $('.stn:not(.intro)').fadeIn(300);
                if ($(window).width() <= 768) {
                    $('.btn_go_contact').fadeIn();
                } else {
                    $('.btn_go_contact').fadeIn();
                }
            });

            // swiper
            new Swiper('.scs_cases_box', {
                slidesPerGroup: 1,
                slidesPerView: 1,
                loop: true,
                observer: true,
                observeParents: true,
                pagination: {
                    el: '.swiper-pagination',
                    clickable: true,
                },
                navigation: {
                    prevEl: '.swiper-button-prev',
                    nextEl: '.swiper-button-next',
                },
                autoplay: {
                    delay: 6000,
                    disableOnInteraction: false,
                },
                breakpoints: {
                    320: {
                        spaceBetween: 20,
                        autoHeight: true,
                    },
                },
            });

            // 문의 영역으로 이동
            $('.btn_go_contact').on('click', function () {
                var contactOffset = $('.contact').offset();
                $('html').animate({scrollTop: contactOffset.top}, 300);
            });

            // 문의 영역 이동 시 버튼 제거
            $(window).scroll(function () {
                var contactOffset = $('.contact').offset();
                var contactOffsetTop = contactOffset.top;
                var scrollTop = $('html').scrollTop() + $('.contact').height();

                if ($(window).width() > 768 && scrollTop >= contactOffsetTop) {
                    $('.btn_go_contact').fadeOut();
                } else if ($(window).width() > 768 && scrollTop < contactOffsetTop) {
                    $('.btn_go_contact').fadeIn();
                } else if ($(window).width() <= 768 && scrollTop >= contactOffsetTop) {
                    $('.btn_go_contact').fadeOut();
                } else if ($(window).width() <= 768 && scrollTop < contactOffsetTop) {
                    $('.btn_go_contact').fadeIn();
                }
            });

            // 문의하기 연락처 숫자입력 제한
/*            $('#userTel').on('keyup', function () {
                $(this).val($(this).val().replace(/[^0-9]/g, ''));
            });*/

            // 문의하기 필수 내용 입력, 동의체크 시 버튼 컬러 활성화
            $('.ipt_txt').on('input', checkInput);
            $('.ipt_tel').on('input', checkInput);
            $('#assent').on('click', checkInput);

            function checkInput() {
                var userName = $('.ipt_txt').val();
                var userTel = $('.ipt_tel').val();
                var assentCheck = $('#assent')
                var inquireButton = $('#inquireBtn');

                if (userName == '' || userTel == '' || assentCheck.is(':checked') == false) {
                    inquireButton.prop('disabled', true);
                } else {
                    inquireButton.prop('disabled', false);
                }
            }
        });

        //문의하기 메일 발송 2021.03.11 SMR
        $('#inquireBtn').on('click', function () {
            var type = $('input[type=radio]:checked').val();
            var agreeCheck = $('input[type=checkbox]:checked').is(':checked');
            var checkText;

            agreeCheck ? checkText = '동의' : checkText = '비동의'

            let title = "[마음북] " + $("#userName").val() + "님의 문의 사항입니다.";
            let msg = "이름 : " + $("#userName").val()
                + "<br>연락처 : " + $("#userTel").val()
                + "<br>수령 방법 : " + type
                + "<br>문의 내용 : " + $("#usertxt").val()
                + "<br>개인 정보 수신 동의 : " + checkText;

            let formData = new FormData();

            formData.append('fromaddr', 'maumBook');
            formData.append('toaddr', 'hello@mindslab.ai');
            formData.append('subject', title);
            formData.append('message', msg);

            $.ajax({
                type: 'POST',
                async: true,
                url: '/support/sendContactMail',
                dataType: 'text', //서버가 응답할 때 보내줄 데이터 타입
                data: formData,
                processData: false, //query string false
                contentType: false, // multipart/form-data 설정
                success: function () {
                    $("#userName").val('');
                    $("#userTel").val('');
                    $("#usertxt").val('');
                    $("#eBook").prop('checked', true);
                    $("#assent").prop('checked', false);

                    alert('Your event registration has been completed')
                    window.location.href = '/';
                },

                error: function (xhr, status, error) {
                    console.log("error", error);
                    alert("Your event registration has failed.\nPlease try again in a few minutes.");
                }
            });
        });
    });
</script>
</body>
</html>