<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-07-10
  Time: 14:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">Close</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>Upload error</h5>
            <p>Error occurred due to the image or file size. <br>Please check the restriction again.</p>
            <span>*  Supported file: .obj <br>
                * The tooth model must be aligned with the vertical axis as shown in the sample file.</span>
        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">OK</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents api_content">
    <!-- .content -->
    <div class="content">
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'bpdemo')" id="defaultOpen">
                <button type="button">AI Engine</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'bpexample')">
                <button type="button">Use Case</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'bpmenu')">
                <button type="button">Manual</button>
            </li>
            <%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <!-- .demobox -->
        <div class="demobox" id="bpdemo">
            <p><span>Bracket Positioning</span></p>
            <span class="sub">The engine selects the location to attach the bracket on a given tooth.</span>
            <!--faceTracking_box-->
            <div class="demo_layout faceTracking_box bp_box">
                <!--tr_1-->
                <div class="tr_1">
                    <div class="fl_box">
                        <p><em class="far fa-file-video"></em><strong>Use Sample File</strong></p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="option" value="1" checked>
                                    <label for="sample1" class="">
                                       <img src="${pageContext.request.contextPath}/aiaas/common/images/img_bp_1.png" alt="sample Image" />
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-video"></em><strong>Use My File</strong></p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-image hidden"></em>
                                <label for="demoFile" class="demolabel">Upload File</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".obj">
                            </div>
                            <ul>
                                <li>* Supported file: .obj </li>
                                <li>* The tooth model must be aligned with the vertical axis as shown in the sample file.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">Position</button>
                    </div>
                </div>
                <!--tr_1-->
                <!--tr_2-->
                <div class="tr_2">
                    <p><em class="far fa-file-image"></em>In progress</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px"
                             viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42"
                                                                             d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/>
                            <g>
                                <path fill="#f7778a" fill-opacity="1"
                                      d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/>
                                <animatetransform attributeName="transform" type="translate"
                                                  values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0"
                                                  calcMode="discrete" dur="1820ms" repeatCount="indefinite"/>
                            </g></svg>

                        <p>AI processing takes a while...</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
                    </div>

                </div>
                <!--tr_2-->
                <!--tr_3-->
                <div class="tr_3 tr_3_sample">

                    <div class="result_file">
                        <div class="fl">
                            <p><em class="far fa-file-image"></em>Input</p>
                            <div class="inner_box">
                                <img src="${pageContext.request.contextPath}/aiaas/common/images/img_bp_1.png" alt="Input" />
                            </div>
                        </div>
                        <div class="fr">
                            <p><em class="far fa-file-image"></em>Output</p>
                            <div class="inner_box">
                                <div>
                                    <img src="${pageContext.request.contextPath}/aiaas/common/images/img_bp_2.png" alt="Output Region" />
                                    <span>Region</span>
                                    <a class="resultposition" href="" ><em class="far fa-arrow-alt-circle-down"></em> Download Result</a>
                                </div>
                                <div>
                                    <img src="${pageContext.request.contextPath}/aiaas/common/images/img_bp_3.png" alt="Output Bracket" />
                                    <span>Bracket</span>
                                    <a id="resultbracket" class="resultbracket" href="" ><em class="far fa-arrow-alt-circle-down"></em> Download Result</a>
                                </div>
                                <p>The result file is provided as an .obj file.</p>
                            </div>
                        </div>
                        <div class="unfloat">
                            <span>A combined 3D image by Meshmixer</span>
                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_bp_4.png" alt="A combined 3D image by Meshmixer" />
                            <p>Front tooth bracket position</p>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2"><em class="fas fa-redo"></em>Reset</button>
                    </div>
                </div>

                <div class="tr_3 tr_3_upload">

                    <div class="result_file">
                        <p><em class="far fa-file-image"></em>Output</p>
                        <div class="inner_box">
                            <div class="dwn_box">
                                <span>Region</span>
                                <a class="resultposition" href="" ><em class="far fa-arrow-alt-circle-down"></em> Download Result</a>
                            </div>
                            <div class="dwn_box">
                                <span>Bracket</span>
                                <a class="resultbracket" href="" ><em class="far fa-arrow-alt-circle-down"></em> Download Result</a>
                            </div>
                            <p>The result file is provided as an .obj file.</p>
                        </div>

                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2"><em class="fas fa-redo"></em>Reset</button>
                    </div>
                </div>
                <!--tr_3-->
            </div>
            <!--//.faceTracking_box-->

        </div>
        <!-- //.demobox -->
        <!--.ftmenu-->
        <div class="demobox vision_menu" id="bpmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API Guideline
                    </div>
                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID &amp; Key</p>
                    <p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                    <p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                    <p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        Bracket Positioning
                    </div>
                    <p class="sub_txt">The engine selects the location to attach the bracket on a given tooth.</p>
                    <span class="sub_title">
								Preparation
					</span>
                    <p class="sub_txt">- Input: Tooth image </p>
                    <ul>
                        <li>File type: .obj</li>
                        <li>The tooth model must be aligned with the vertical axis</li>
                    </ul>
                    <span class="sub_title">
								 API Document
					</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL :  https://api.maum.ai/bracket/downloadZip</li>
                    </ul>
                    <p class="sub_txt">② Request parameters </p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API ID. Request from is required for the ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>file</td>
                            <td>Input obj file</td>
                            <td>file</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request 예제 </p>
                    <div class="code_box">
                        <pre>
curl --location --request POST 'http://api.maum.ai/bracket/downloadZip' \
--form 'apiId= {Own API Id}'
--form 'apiKey= {Own API Key}'
--form 'file= {obj file  path}'</pre>
                    </div>

                    <p class="sub_txt">④ - Response example (Zip File Download)  </p>

                    <div class="code_box">
<pre>
File type: .obj
ab: Bracket
region: Position
</pre>
                    </div>

                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.ftmenu-->
        <!--.ftexample-->
        <div class="demobox" id="bpexample">
            <p><em style="color:#f7778a;font-weight: 400;">Use Cases</em> </p>
            <span class="sub">Find out how AI can be applied to diverse area.</span>
            <!-- 치아교정 -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>Braces Positioning</span>
                            </dt>
                            <dd class="txt">Recognizes the tooth shape and selects the optimal location for the bracket to be attached..</dd>
<%--                            <dd class="api_itemBox">--%>
<%--                                <ul class="lst_api">--%>
<%--                                    <li class="ico_ftr"><span>얼굴 추적</span></li>--%>
<%--                                    <li class="ico_sr"><span>자막 인식</span></li>--%>
<%--                                </ul>--%>
<%--                            </dd>--%>
                        </dl>
                    </li>
                </ul>
            </div>
            <!--치아교정   -->
        </div>
        <!--//.ftexample-->


    </div>
    <!-- //.content -->
</div>

<script>

    var sample1SrcUrl;
    var sample1File;
    var xhrAb;
    var xhrRegion;


    function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/kr/images/00positioning.obj");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function () {
            blob = xhr.response; //xhr.response is now a blob object
            sample1File = new File([blob], "00positioning.obj");
            sample1SrcUrl = URL.createObjectURL(blob);
        };
        xhr.send();
    }


    function sendApiRequest(file, sampleOrFile) {
        var formData = new FormData();
        formData.append('file', file);
        formData.append('${_csrf.parameterName}', '${_csrf.token}');

        $('.tr_2').fadeIn();

        $.when(getAb(formData), getRegion(formData)).done(function(resultAb, resultRegion){
                console.log("Result OK");

                var urlAb = URL.createObjectURL(resultAb);
                var urlRegion = URL.createObjectURL(resultRegion);

                $('.tr_2').hide();
                $('.bp_box').css('border','none');
                $('.tr_3_' + sampleOrFile).fadeIn(300);

                $('.resultbracket').attr('href', urlAb).attr('download',"bracketAb.obj");
                $('.resultposition').attr('href', urlRegion).attr('download',"bracketRegion.obj");

            }).fail(function(){
                console.log("Result fail");
                alert("Failed to get result file.\nplease try again.");
                $('.tr_2').hide();
                $('.tr_1').fadeIn(300);
                $('.bp_box').css('border','solid 1px #cfd5eb');
            }
        );

        /* var xhr = new XMLHttpRequest();
         xhr.responseType = 'blob'; // !!필수!!
         xhr.onreadystatechange = function(){
             if (this.readyState == 4 && this.status == 200){

                 console.log(this.response, typeof this.response);
                 var url = URL.createObjectURL(this.response);

                 $('.tr_2').hide();
                 $('.bp_box').css('border','none');
                 $('.tr_3_sample').fadeIn(300);
                 $('#resultbracket').attr('href', url).attr('download',"result.obj");

             }
         };
         xhr.open('POST', '/api/bracket');
         xhr.send(formData);*/

    }



    function getAb(formData){
        let deferred = $.Deferred();
        xhrAb = new XMLHttpRequest();

        xhrAb.responseType = 'blob';
        xhrAb.open('POST', '/api/bracket/Ab');
        xhrAb.addEventListener('load', function() {
            if(xhrAb.status === 200){
                deferred.resolve(xhrAb.response);
            } else{
                deferred.reject("Request error : " + xhrAb.status);
            }
        }, false);

        xhrAb.addEventListener('abort', function() { });

        xhrAb.send(formData);
        return deferred.promise();
    }


    function getRegion(formData){
        let deferred = $.Deferred();
        xhrRegion = new XMLHttpRequest();

        xhrRegion.responseType = 'blob';
        xhrRegion.open('POST', '/api/bracket/Region');
        xhrRegion.addEventListener('load', function() {
            if (xhrRegion.status === 200) {
                deferred.resolve(xhrRegion.response);
            } else{
                deferred.reject("Request error : " + xhrRegion.status);
            }
        }, false);

        xhrRegion.addEventListener('abort', function() { });

        xhrRegion.send(formData);
        return deferred.promise();
    }



    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {

            loadSample1();

            // 위치 선정
            $('.btn_start').on('click', function () {

                let blob;
                let sampleOrFile;

                if ($('#sample1').prop('checked')) { // 샘플 파일로 해보기
                    sampleOrFile = "sample";
                    blob = sample1File;
                    $('.tr_1').hide();

                } else { // 내 파일로 해보기
                    sampleOrFile = "upload";
                    blob = document.getElementById('demoFile').files[0];
                    $('.tr_1').hide();
                }

                sendApiRequest(blob, sampleOrFile);

            });



            //파일 업로드
            document.querySelector("#demoFile").addEventListener('change', function (ev) {

                var demoFile = ev.target.files[0];

                let fileType = demoFile.name;
                fileType = fileType.substr(fileType.lastIndexOf("."));
                if(fileType != ".obj"){
                    alert("Please upload obj file.");
                    $('em.close').click();
                    return;
                }

                // $('.fl_box').on('click', function () {
                //     $(this).css("opacity", "1");
                // });

                //파일 용량 체크
                // var demoFileInput = document.getElementById('demoFile');



                // var demoFileSize = demoFile.size;
                // var max_demoFileSize = 1024 * 1024 * 50;//1kb는 1024바이트

                document.querySelector(".demolabel").innerHTML = demoFile.name;
                $('#uploadFile').addClass('btn_change').removeClass('btn');
                $('.fl_box').css("opacity", "0.5");
                $('#sample1').prop('checked', false);

            });


           /* //파일명 변경
            document.querySelector("#demoFile").addEventListener('change', function (ev) {
                document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
                $('#uploadFile').addClass('btn_change');
                $('#uploadFile').removeClass('btn');
            });*/


            // 업로드 x
            $('em.close').on('click', function () {
                $('.fl_box').css("opacity", "1");
                $(this).parent().removeClass("btn_change");
                $(this).parent().addClass("btn");
                $(this).parent().children('.demolabel').text('Upload File');
                $('#demoFile').val(null);
            });


            // 샘플 클릭
            $('.sample_box').on('click', function () {
                $('em.close').trigger('click');
                $('#sample1').prop('checked', true);
                $('.fl_box').css("opacity", "1");
            });


            // 처음으로 버튼
            $('.btn_back1').on('click', function () {
                xhrRegion.abort();
                xhrAb.abort();
                $('.tr_2').hide();
                $('.tr_1').fadeIn(300);
                // $('.fl_box').css("opacity", "1");
                $('.bp_box').css('border','solid 1px #cfd5eb');
            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('.tr_3_sample').hide();
                $('.tr_3_upload').hide();
                $('.tr_1').fadeIn(300);
                // $('.fl_box').css("opacity", "1");
            });

            //알림 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
        });
    });


//API 탭
function openTap(evt, menu) {
    var i, demobox, tablinks;
    demobox = document.getElementsByClassName("demobox");
    for (i = 0; i < demobox.length; i++) {
        demobox[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(menu).style.display = "block";
    evt.currentTarget.className += " active";
}

// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();

</script>