<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

		<div class="contents">
			<div class="content api_content">
				<h1 class="api_tit">Machine RC</h1>
				<ul class="menu_lst">
					<li class="tablinks" onclick="openTap(event, 'mrcdemo')" id="defaultOpen">Demo</li>
					<li class="tablinks" onclick="openTap(event, 'mrcmenu')">Manual</li>
					<li class="tablinks" onclick="openTap(event, 'mrcexample')">Use Case</li>
					<li class="tablinks"><a href="/support/enPricing">Purchase</a></li>
				</ul>
				<!--.demobox_mrc-->
				<div class="demobox demobox_mrc" id="mrcdemo">
					<p>Machine Reading Comprehension (MRC)</p>
					<span class="sub">Provide the most relevant answers by comprehending the whole passage.</span>

					<%--<p class="temp_txt">The service is currently available only in Korean. Other languages will be available soon.</p>--%>

					<!--.mrc_box-->
					<div class="mrc_box">
					    <%-- <div class="mrclang_select">
                            <span>Language</span>
                            <div class="radio">
                                <input type="radio" id="radioType01" name="radio" checked="checked"><label for="radioType01">Korean</label>
                            </div>
                            <div class="radio">
                                <input type="radio" id="radioType02" name="radio"><label for="radioType02">English</label>
                            </div>
                        </div> --%>

						<!--.step01-->
						<div class="step01">
							<div class="demo_top">
								<p>Insert Texts</p>
							</div>
							<div class="tab">
                                  <button class="tablink" onclick="clickTap(event, 'exsample')" id="defaultOpen2" >Example</button>
                                  <button class="tablink" onclick="clickTap(event, 'article')" style="display: none;">Article</button>
                            </div>
							<div class="text_area tabcontent" id="exsample">
								<textarea rows="7" id="id_input_text" placeholder="Please copy and paste articles from the news or web documents."></textarea>
							</div>

							<div class="text_area tabcontent"  id="article">
                                <div class="searchType">
                                    <input type="text" class="input_textType" title="" placeholder="Plaese search the news article.">
                                    <span class="btn"><button type="button" class="btn_search">Search</button></span>
                                </div>
                                <ul class="article_lst">
                                    <li>
                                        <div class="radio">
                                            <input type="radio" id="radio" name="radio_article" checked="checked">
                                            <label for="radioType01">자동차 수리비 견적을 인공지능이 뽑는다</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="radio">
                                            <input type="radio" id="radio2" name="radio_article">
                                            <label for="radioType01">삼성전자, 세계적 인공지능 전문 연구기관 캐나다 밀라연구소 입성</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="radio">
                                            <input type="radio" id="radio3" name="radio_article">
                                            <label for="radioType01">삼성전자, 세계적 인공지능 전문 연구기관 캐나다 밀라연구소 입성</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="radio">
                                            <input type="radio" id="radio4" name="radio_article">
                                            <label for="radioType01">삼성전자, 세계적 인공지능 전문 연구기관 캐나다 밀라연구소 입성</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="radio">
                                            <input type="radio" id="radio5" name="radio_article">
                                            <label for="radioType01">삼성전자, 세계적 인공지능 전문 연구기관 캐나다 밀라연구소 입성</label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="radio">
                                            <input type="radio" id="radio6" name="radio_article">
                                            <label for="radioType01">삼성전자, 세계적 인공지능 전문 연구기관 캐나다 밀라연구소 입성</label>
                                        </div>
                                    </li>
                                </ul>
                                <div class="btn_area">
                                    <button type="button" class="btn_select" id="" >OK</button>
                                </div>
                            </div>

						</div>	
						<!--//.step01-->
						<!--.step02-->
						<div class="step02">
							<div class="demo_top">									
								<p>Ask Questions</p>
				            </div>
							<div class="text_area">
								<textarea class="example_area" id="id_input_question" placeholder="Ask questions on the article above."></textarea>
							</div> 
							<div class="btn_area">
                                <button type="button" class="btn_search" id="find_answer_btn"><em class="fas fa-file-invoice"></em>
									Find Answers
<!--								데모 가능하면 팁 해제
									<span class="tooltiptext"> 
										Click Button 
									</span>
-->
								</button>
<!--							<span class="disBox"></span>-->
							</div>
						</div>	
						<!--//.step02-->

						<!--.step03-->
						<div class="step03">
							<div class="demo_top">
								<p style="width: 135px;">MRC Result</p>
							</div>

							<div class="resultBox">
								Reliability: <span>93.25%</span>
								</div>
							<div class="result_txt">
								신성 로마 제국(라틴어: Sacrum Romanum Imperium)은 중세에서 근대 초까지 이어진 기독교 성향이 강한 유럽 국가들의 정치적 연방체이다. 프랑크 왕국이 베르됭 조약(843년)으로 나뉜 동쪽에서 독일 왕이 마자르족을 격퇴한 후 <strong>교황</strong>으로부터 황제의 관을 수여받아 신성 로마 제국 건국을 선포하였다. 신성 로마 제국은 초기에는 강력한 중앙집권 국가였으나, 점차 이탈리아에 대한 간섭으로 독일 지역에 소홀히 하면서 여러 제후들에 의해 분할 상태가 되었다. <strong>30년 전쟁(1618~48년)</strong>이 일어난 나라로 유명하며, 30년 전쟁에 패배하여 베스트팔렌 조약(1648년)으로 많은 영토를 잃었다		
							</div>		
							<div class="btn_area">
							    <button type="button" class="btn_another" id="">Ask another question</button>
								<button type="button" class="btn_reset" id="" >Start Again</button>	
							</div>
						</div>
						<!--//.step03-->
					</div>						
					<!--//.mrc_box-->			
					
				</div>
				<!--.demobox_mrc -->
				
				<!--.mrcmenu-->
				<div class="demobox" id="mrcmenu">				
					
					<!--guide_box-->
					<div class="guide_box">
					    <div class="guide_common">
							<div class="title">
								API Guideline
							</div>
							<p class="sub_title">Set up Environment</p>
							<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
							<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
							<p class="sub_title">ID &amp; Key <span>*Business and Enterprise only</span></p>
							<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
							<p class="sub_txt">2&#41; Send an email to hello@mindslab.ai with your name and email address.</p>
							<p class="sub_txt">3&#41; After the agreement, Minds Lab would send you an ID with a key.</p>
							<p class="sub_txt">4&#41; Remember the ID and key.</p>
							<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s ID and key.</p>
						</div>
						<div class="guide_group">
							<div class="title">
								MRC <small>(Machine Reading Comprehension)</small>		
							</div>
							<p class="sub_txt">MINDs Lab's MRC is an innovative artificial intelligence (AI) technology that can instantly decode any document,regardless of content or volume, and answer your questions accurately.</p>

							<span class="sub_title">
								Getting Started			
							</span>
							<p class="sub_txt">① Input: Passage (text), question (text)</p>
							<p class="sub_txt">② Available Model </p>
							<ul>
								<li>Default</li>					
							</ul>
							<span class="sub_title">
								 Guide			
							</span>
							<p class="sub_txt">① Request  </p>
							<ul>
								<li>Method : POST</li>
								<li>URL : https://api.maum.ai/api/mrc/</li>										
							</ul>
							<p class="sub_txt">② Request Parameter </p>
							<table>
								<tr>
									<th>Key</th>
									<th>Explanation</th>
									<th>type</th>
								</tr>
								<tr>
									<td>ID</td>
									<td>Request ID via email (hello@mindslab.ai) </td>
									<td>string</td>
								</tr>
								<tr>
									<td>key</td>
									<td>Request key via email (hello@mindslab.ai) </td>
									<td>string</td>
								</tr>
								<tr>
									<td>cmd</td>
									<td>Type in keywords that indicates the API (runMRC) </td>
									<td>string</td>
								</tr>
								<tr>
									<td>sentence</td>
									<td>passage (text)</td>
									<td>string</td>
								</tr>
								<tr>
									<td>question</td>
									<td>questions that required answers (text) </td>
									<td>string</td>
								</tr>							
							</table>
							<p class="sub_txt">③ Request Example </p>
							<div class="code_box">
		curl -X POST \<br>
	<span>https://api.maum.ai/api/mrc/ \</span><br>
	<span>-H 'Content-Type: application/x-www-form-urlencoded' \</span><br>
	<span>-F ID=(*ID 요청 필요) \</span><br>
	<span>-F key=(*key 요청 필요) \</span><br>
	<span>-F cmd=runMRC \</span><br>
	<span>-F 'sentence=신성 로마 제국(라틴어: Sacrum Romanum Imperium)은 중세에서 근대 초까지 이어진 기독교 성향이 강한 유럽 국가들의 정치적 연방체이다. 프랑크 왕국이 베르됭 조약(843년)으로 나뉜 동쪽에서 독일 왕이 마자르족을 격퇴한 후 교황으로부터 황제의 관을 수여받아 신성 로마 제국 건국을 선포하였다. '
	</span><br>
	<span>-F 'question=신성 로마 제국은 언제 세워졌어?'</span>
							</div>

							<p class="sub_txt">④ Explanation on Response Parameter </p>
							<span class="table_tit">Response</span>
							<table>
								<tr>
									<th>Key</th>
									<th>Explanation</th>
									<th>type</th>
								</tr>
								<tr>
									<td>data</td>
									<td>The result of API data (json format) </td>
									<td>list</td>
								</tr>
								<tr>
									<td>status</td>
									<td>API Operation (Success / Fail)</td>
									<td>string</td>
								</tr>
								<tr>
									<td>extra_data</td>
									<td>Additional Data  </td>
									<td>string</td>
								</tr>
							</table>
							<span class="table_tit">data: API Result </span>
							<table>
								<tr>
									<th>Key</th>
									<th>Explanation</th>
									<th>type</th>
								</tr>
								<tr>
									<td>answers</td>
									<td>Comprehension Result (json list format) </td>
									<td>list</td>
								</tr>
								<tr>
									<td>original</td>
									<td>Input passage (text)  </td>
									<td>string</td>
								</tr>							
							</table>
							<span class="table_tit">answers: Comprehension Result</span>
							<table>
								<tr>
									<th>Key</th>
									<th>Explanation</th>
									<th>type</th>
								</tr>
								<tr>
									<td>answer</td>
									<td>answers to questions (text)</td>
									<td>string</td>
								</tr>
								<tr>
									<td>prob</td>
									<td>The probability of the result (0~1)</td>
									<td>number</td>
								</tr>							
							</table>
							<p class="sub_txt">⑤ Response Example </p>
							<div class="code_box">
	{<br>
	<span>"data": [</span><br>
	<span class="twoD">{</span><br>
	<span class="thirD">"answers": [</span><br>
	<span class="fourD">{</span><br>
	<span class="fivD">"answer": "베르됭 조약(843년",</span><br>
	<span class="fivD"> "prob": 0.1667664647102356</span><br>
	<span class="fourD">}</span><br>
	<span class="thirD">],</span><br>
	<span class="thirD">"original": "신성 로마 제국(라틴어: Sacrum Romanum Imperium)은 중세에서 근대 초까지 이어진 기독교 성향이 강한 유럽 국가들의 정치적 연방체이다. 프랑크 왕국이 베르됭 조약(843년)으로 나뉜 동쪽에서 독일 왕이 마자르족을 격퇴한 후 교황으로부터 황제의 관을 수여받아 신성 로마 제국 건국을 선포하였다."</span><br>

	<span class="twoD">}</span><br>
	<span>],</span><br>
	<span>"status": "Success",</span><br>
	<span>"extra_data": "None"</span><br>
	}


							</div>
						</div>
					</div>
					<!--//.guide_box-->		
				</div>
				<!--//mrcmenu-->
				<!--.mrcexample-->
				<div class="demobox" id="mrcexample">	
					<div class="coming_txt">
						<h5>Coming Soon.</h5>
					</div>
				</div>
				
			</div>

		</div>
		<!-- //.contents -->


<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/mrc.js"></script>

<script type="text/javascript">
$(window).load(function() {
	$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
		$(this).remove();
	});
});
</script>
<script type="text/javascript">
jQuery.event.add(window,"load",function(){
	$(document).ready(function (){		
		
		//mrc
		// Layer (네이버 기사 검색)
		$('#btn_lyr_naver').on('click',function(){
			$('.lyr_wrap').fadeIn(300);
			$('#lyr_naver').fadeIn(300);
		});
		
		// Layer (예문선택)
		$('#btn_lyr_sample').on('click',function(){
			$('.lyr_wrap').fadeIn(300);
			$('#lyr_sample').fadeIn(300);
		});
		
		// Layer (질문선택)
		$('#btn_lyr_question').on('click',function(){
			$('.lyr_wrap').fadeIn(300);
			$('#lyr_question').fadeIn(300);
		});
		
		// Layer (리스트 선택)
		$('.lyr_wrap .lyr_lst li a').on('click',function(){
			$('.lyr_wrap .lyr_lst li a').removeClass('active');
			$(this).addClass('active');
		});
		
		// Layer (close)
		$('.lyr_wrap .btrnBox .btnType01').on('click',function(){
			$('.lyr_wrap').fadeOut(300);
			$(this).parent().parent().parent().fadeOut(300);
		});
		
		// Layer (close)
		$('.lyr_bg').on('click',function(){
			$('.lyr_wrap').fadeOut(300);
			$('.lyr_box').fadeOut(300);
		});
		
		
		// product layer popup     
		$('.close, .lyr_bg').on('click', function () {
			$('.demobox_mrc .lyr_wrap').fadeOut(300);			
			$('body').css({
				'overflow': '',
			});
		});	
		
		
		// step01 > step02
		$('.mrc_box .step01 textarea').on('input keyup paste', function() {			
			var txtValLth = $(this).val().length;
			
			if ( txtValLth > 0) {
				$('.demobox_mrc .mrc_box .step02').show();
				$('.progress li:nth-child(2)').addClass('active'); 	
				
			} else {
				$('.demobox_mrc .mrc_box .step02').hide();
				$('.demobox_mrc .mrc_box .step03').hide();
				$('.demobox_mrc .mrc_box .step02 .btnBox').show();
				$('.progress li:nth-child(2)').removeClass('active'); 	
			}
		});
		
		// step02 button active
		$('.mrc_box .step02 textarea').on('input keyup paste', function() {			
			var txtValLth = $(this).val().length;
			
			if ( txtValLth > 0) {
				$('.mrc_box .step02 .btn_area button').removeClass('disable');	
				$('.mrc_box .step02 .btn_area button').removeAttr('disabled');	
				$('.mrc_box .step02 .btn_area .disBox').remove();
			} else {
				$('.mrc_box .step02 .btn_area button').addClass('disable');	
				$('.mrc_box .step02 .btn_area button').attr('disabled');
				$('.mrc_box .step03').hide();
				$('.mrc_box .step02 .btn_area').append('<span class="disBox"></span>');
			}
		});
		
		/*
		// step02 > step03
		$('.mrc_box .step02 .btn_search').on('click',function(){	
			$('.mrc_box .step01').hide();	
			$('.mrc_box .step02').hide();	
			$('.mrc_box .step03').fadeIn();
			$('.progress li:nth-child(3)').addClass('active'); 	
		});
		*/
		
		// step03 > step01
		$('.mrc_box .step03 .btn_reset').on('click',function(){
			$('.mrc_box .text_area textarea').val('');
			$('.mrc_box .step01').show();
			$('.mrc_box .step02').hide();
			$('.mrc_box .step03').hide();
			$('.mrc_box .step02 .btn_area').show();
			$('.mrc_box .step02 .btn_area button').addClass('disable');	
			$('.mrc_box .step02 .btn_area button').attr('disabled');
			$('.mrc_box .step02 .btn_area').append('<span class="disBox"></span>');
			$('.progress li:nth-child(1)').addClass('active'); 	
			$('.progress li:nth-child(2)').removeClass('active');
			$('.progress li:nth-child(3)').removeClass('active');
		});
		
		$('.mrc_box .step03 .btn_another').on('click',function(){
			$('.mrc_box .step01').show();
			$('.mrc_box .step02').show();
			$('.demoBox .step02 .btnBox button').removeClass('disabled');	
			$('.demoBox .step02 .btnBox button').removeAttr('disabled');
			$('.mrc_box .step03').hide();
			$('.mrc_box .step02 .btn_area').show();
			$('.progress li:nth-child(1)').addClass('active'); 	
			$('.progress li:nth-child(2)').removeClass('active');
			$('.progress li:nth-child(3)').removeClass('active');
		});

		//mrc
		
		//=================================
        //              한국어 
        //=================================

        //질문예제 선택시, 예문 index확인을 위한 변수
        var sample_index = -1;
        var pre_sample_index = -1;

        // step01 > step02
		$('.demoBox .step01 .textArea').on('input keyup paste change', function() {	
			var txtValLth = $(this).val().length;
			if ( txtValLth > 0) {
				$('.type_deepqa .demoBox .step02').show();
			} else {
				$('.type_deepqa .demoBox .step02').hide();
				$('.type_deepqa .demoBox .step03').hide();
				$('.type_deepqa .demoBox .step02 .btnBox').show();
			}
		});
		
		// step02 button active
		$('.demoBox .step02 .textArea').on('input keyup paste change', function() {			
			var txtValLth = $(this).val().length;
			
			if ( txtValLth > 0) {
				$('.demoBox .step02 .btnBox button').removeClass('disabled');	
				$('.demoBox .step02 .btnBox button').removeAttr('disabled');
				$('.demoBox .step02 .btnBox .holeBox').show();
				$('.type_deepqa .demoBox .btnBox').show();
				$('.demoBox .step02 .btnBox .disBox').remove(); 
			} else {
				$('.demoBox .step02 .btnBox button').addClass('disabled');	
				$('.demoBox .step02 .btnBox button').attr('disabled');
				$('.demoBox .step02 .btnBox .holeBox').hide();
				$('.demoBox .step03').hide();
				$('.demoBox .step02 .btnBox').append('<span class="disBox"></span>');
			}
		});
		
		// step02 > step03__Find Answer
        $('.demoBox .step02 .textArea').keypress(function(e){
			if(e.keyCode==13)
                $('#find_answer_btn').trigger('click');
		});
        $("#find_answer_btn").on('click',function(){
            var sentence = document.getElementById('id_input_text').value;
            var question = document.getElementById('id_input_question').value;
            if(sentence == "" || $.trim(sentence)==""){
                alert("Please copy and paste articles from the news or web documents!");
                return false;
            }
            if(question == "" || $.trim(question)==""){
                alert("Ask questions on the article above!");
                $("#id_input_question").text("");
                $("#id_input_question").focus();
                return false;
            }
            var result="";
            var original="";
            var param = {
				'cmd': 'runMRC',
                'ID' : 'minds-api-sales-demo',
				'key': '814826d160684a489dcbeb7445ecd644',
                'sentence' : sentence,
                'question' : question,
                '${_csrf.parameterName}' : '${_csrf.token}'
            };
            //url: '/api/mrc/',
            var resultStr ="";
            var confedenceLv;
            $.ajax({
                url: '/api/mrc/',
                async: false,
                type: 'POST',
                headers: {
						"Content-Type": "application/x-www-form-urlencoded"
				},
                data: param,
                error: function(request, status, error){
                    if (request.statusText == ""){
						alert("서버에 연결할 수 없습니다.");
					}else if (request.statusText == "INTERNAL SERVER ERROR"){
						alert("서버에서 답을 불러올 수 없습니다.");
					}else{
						alert(request.statusText);
					}
                },
                success: function(data){
                    console.log(data);
                    console.log(JSON.stringify(responseData));
                    console.log(JSON.stringify(data));
                    //debugger;
                    var responseData = JSON.parse(data);
                    var responseString = JSON.stringify(responseData);
                    console.log(JSON.stringify(responseData));
                    console.log(JSON.stringify(data));
                    result = responseData['data'][0]['answers'][0]['answer'];
                    original = responseData['data'][0]['original'];
                    resultStr = original.toString().replace(result.toString(), "<strong>"+result.toString()+"</strong>");
                    confedenceLv = responseData['data'][0]['answers'][0]['prob'].toFixed(4) * 100;
                }
            });
            //결과 화면
            $(this).parent().hide();
			$('.demoBox .step01').hide();	
			$('.demoBox .step02').hide();


            // $(".resultBox").text("신뢰도: "+confedenceLv+" %");
			$(".resultBox").text("Reliability : "+confedenceLv+" %");

            $('.step03 .result_txt').html(resultStr);
			$('.type_deepqa .demoBox .step03').show();


			$('.mrc_box .step01').hide();	
			$('.mrc_box .step02').hide();	
			$('.mrc_box .step03').fadeIn();
			$('.progress li:nth-child(3)').addClass('active'); 	
			
        });
		
/* 		// step03 > step01
		$('.demoBox .step03 .btn_reset').on('click',function(){
			// $('.type_deepqa .demoBox .textArea').val('');
			console.log($('.type_deepqa .demoBox .textArea').val(''));
			$('.type_deepqa .demoBox .step01').show();
			$('.type_deepqa .demoBox .step02').show();
			$('.type_deepqa .demoBox .step03').hide();
			$('.type_deepqa .demoBox .step02 .btnBox').show();
			$('.demoBox .step02 .btnBox button').addClass('disabled');	
			$('.demoBox .step02 .btnBox button').attr('disabled');
			$('.demoBox .step02 .btnBox .holeBox').hide();
			$('.demoBox .step02 .btnBox').append('<span class="disBox"></span>');
		});
 */
		//(네이버 기사 검색) Layer
		$('#btn_lyr_naver').on('click',function(){
			$('.lyr_wrap').fadeIn(300);
			$('#lyr_naver').fadeIn(300);
            $(".loading_gif").hide();
            $(".article_item").removeClass('active');
            document.getElementById("search_input").focus();
        });
            //검색(search_button)
        $('#search_input').keydown(function(e){
			if(e.keyCode==13)
                $('#search_button').trigger('click');
		});
        $('#search_button').click(function(){
			var txt = document.getElementById("search_input").value;
			if (txt == null || txt == ""){
				alert("검색어를 입력해주세요.");
				return;
			}
			var param = {'keyword' : txt, '${_csrf.parameterName}' : '${_csrf.token}'};
			$("#search_results").hide();
			$(".loading_gif").show();

			$.ajax({
				type: 'POST',
				url: '/api/search_news',
				dataType: 'json',
				data: param,		
				error: function(xhr, status, error){
					alert('[에러] 검색어를 찾을 수 없습니다.');
					$(".loading_gif").hide();
					$("#search_results").show();
				}, success: function(data) {
 					//console.log(JSON.stringify(data));
 					//data = data['items'];
                    $(".article_item").each(function (index) {
                        if (index >= Object.keys(data).length) {
                            return 0;
                        }
                        //console.log(data[index]["desc"]);
                        //console.log(data[index]["title"]);
                        $(this).attr('value', data[index]["desc"]);
                        $(this).html(data[index]["title"]);
                    });
					$(".loading_gif").hide();
					$("#search_results").show();
				}
			});
		});
		$(".article_item").click(function(){
			$(".article_item").removeClass('chosen');
			document.getElementById("article_content_naver").value = $(this).attr('value');
			//$(this).toggleClass('chosen');
		});
		
            //확인버튼
        $("#insert_selected_content").click(function() {
        	//debugger;
			var txt = $("#article_content_naver").val();
			var id_input_text = document.getElementById("id_input_text").value;
            if(txt != id_input_text){
                $("#id_input_question").val("");
            }
			document.getElementById("id_input_text").value = txt;
			document.getElementById("search_input").value = "";
			$(".article_item").attr('value', "");
            $(".article_item").text("");
			$(".article_item").removeClass('chosen');
            $('#id_input_text').trigger('keyup');
            sample_index = -1;
            pre_sample_index = -1;
            
			
		});

        //(예문선택) Layer
		$('#btn_lyr_sample').on('click',function(){
			$('.lyr_wrap').fadeIn(300);
			$('#lyr_sample').fadeIn(300);
            sample_index = -1;
		});
		$(".sample_item").click(function(){
			$(".sample_item").removeClass('chosen');
			document.getElementById("article_content_sample").value = $(this).attr('value');
			$(this).toggleClass('chosen');
			sample_index = $(".sample_item").index(this);
		});
            //확인버튼
        $("#insert_selected_sample").click(function() {
			var txt = $("#article_content_sample").val();
			var id_input_text = document.getElementById("id_input_text").value;
            if(txt != id_input_text){
                $("#id_input_question").val("");
            }
			document.getElementById("id_input_text").value = txt;
			$(".sample_item").removeClass('chosen');
            $('#id_input_text').trigger('keyup');
            pre_sample_index = sample_index;
		});

		
		//(질문선택) Layer
        var questionArr = new Array();
        questionArr[0] = new Array('신성 로마 제국은 언제 세워졌어?', '신성 로마 제국이 뭐야');
        questionArr[1] = new Array('마쓰에 소요 사건 때문에 몇 명 죽었어?', '마쓰에 소요 사건 언제 일어났어?');
        questionArr[2] = new Array('비룡저수지는 어디 있어?', '비룡저수지의 시설관리자는?', '비룡저수지는 언제 만들어졌어?');
        questionArr[3] = new Array('도시전설 뜻 알려줘', '도시전설은 어떻게 전승 돼?','도시전설 좀 설명해줘');
        questionArr[4] = new Array('FAST AI 고객센터가 뭐야?', '마인즈랩 대표는 누구야?', 'FAST AI 고객센터에 사용된 기술들을 알려줘');
        questionArr[5] = new Array('프로야구 왜 안했어?', '오지환이 주목받는 이유는?', '오지환의 포지션은?');
        questionArr[6] = new Array('피해자 현황', '태풍 이름이 뭐야?', '간사이공항 갈 수 있어?', '간사이공항은 어떻게 됐어?', '아베가 뭐라고 했어?');
        questionArr[7] = new Array('채소 가격이 왜 급등했어?', '소비자 물가를 낮춘 항목은?', '채소류는 몇 퍼센트 올랐어?');
        questionArr[8] = new Array('특수학교 이름이 뭐야?', '학교 설립에 누가 합의했어?', '왜 뒷말이 많지?');
        questionArr[9] = new Array('아마존때문에 위협받고 있는 회사는?', '아마존이 진출한 시장이 뭐야?', '광고주들이 아마존을 선택한 이유는?');
        $('#btn_lyr_question').on('click',function(){
            if(pre_sample_index==-1){
                alert("예문을 먼저 선택해 주세요!");
                return false;
            }
			$('.lyr_wrap').fadeIn(300);
			$('#lyr_question').fadeIn(300);
            $(".question_item").text("");
            $(".question_item").each(function(index){
                if(index >= questionArr[pre_sample_index].length){
                    return 0;
                }
                $(this).attr('value', questionArr[pre_sample_index][index]);
                $(this).html(questionArr[pre_sample_index][index]);
            });
		});
		$(".question_item").click(function(){
			$(".question_item").removeClass('chosen');
			document.getElementById("article_content_question").value = $(this).attr('value');
			$(this).toggleClass('chosen');
		});
		
            //확인버튼
        $("#insert_selected_question").click(function() {
            /* if(!$(".question_item").hasClass("chosen")){
                return 0;
            } */
			var txt = $("#article_content_question").val();
			document.getElementById("id_input_question").value = txt;
			$(".question_item").removeClass('chosen');
            $("#id_input_question").trigger("keyup");
		});

/*
        // 공통
		// Layer (리스트 선택)
		$('#tab_demo01 .lyr_wrap .lyr_lst li a').on('click',function(){
            $(".question_item").removeClass('chosen');
            $(".article_content").val($(this).attr('value'));
			$(this).toggleClass('chosen');
			$('#tab_demo01 .lyr_wrap .lyr_lst li a').removeClass('active');
			$(this).addClass('active');
            if($(this).hasClass("sample_item")){
                sample_index = $(".sample_item").index(this);
            }
            // console.log("리스트선택 sampleIdx:",sample_index," pre_sampleIdx:",pre_sample_index," ***예문선택에서만 바뀜");
		});
		// Layer (close) //확인버튼click
		$('#tab_demo01 .lyr_wrap .btrnBox .btnType01').on('click',function(e){
			$('#tab_demo01 .lyr_wrap').fadeOut(300);
            $(".lyr_box").fadeOut(300);
			$(".article_content").val("");
            $('#tab_demo01 .lyr_wrap .lyr_lst li a').removeClass('active');
            $("#id_input_question").focus();
            // console.log("확인버튼 sampleIdx:",sample_index," pre_sampleIdx:",pre_sample_index);
		});
		// Layer (close) //배경click
		$('#tab_demo01 .lyr_bg').on('click',function(){
            if($("#lyr_naver").is(":visible")){
                document.getElementById("search_input").value = "";
			    $(".article_item").attr('value', "");
                $(".article_item").text("");
			    $(".article_item").removeClass('chosen');
                $(".article_item").removeClass('active');
            }
            if($("#lyr_sample").is(":visible")){
                $(".sample_item").removeClass('chosen');
                $(".sample_item").removeClass('active');
                sample_index = pre_sample_index;
            }
            if($("#lyr_question").is(":visible")){
                $(".question_item").removeClass('chosen');
                $(".question_item").removeClass('active');
            }
			$('#tab_demo01 .lyr_wrap').fadeOut(300);
			$('.lyr_box').fadeOut(300);
		});
*/		
	});
});


</script>

