<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<div class="contents">
	<div class="content api_content">
		<h1 class="api_tit">MRC</h1>
		<ul class="menu_lst">
			<li class="tablinks" onclick="openTap(event, 'mrcdemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
			<li class="tablinks" onclick="openTap(event, 'mrcexample')"><button type="button">Use Case</button></li>
			<li class="tablinks" onclick="openTap(event, 'mrcmenu')"><button type="button">Manual</button></li>
<%--			<li class="tablinks"><a href="/member/enApiAccount">API ID, key</a></li>--%>
		</ul>
		<!--.demobox_mrc-->
		<div class="demobox demobox_mrc" id="mrcdemo">
			<p><span>MRC</span> <small>(Machine Reading Comprehension)</small></p>
			<span class="sub">Provide the most relevant answers by comprehending the whole passage.</span>
			<!--.mrc_box-->
			<div class="mrc_box">
				<div class="mrclang_select">
					<span>Language</span>
					<div class="radio">
						<input type="radio" id="radioType02" name="radio" checked="checked" value="eng"><label for="radioType02">English</label>
					</div>
					<div class="radio">
						<input type="radio" id="radioType01" name="radio" value="kor"><label for="radioType01">Korean</label>
					</div>

				</div>

				<!--.step01-->
				<div class="step01">
					<div class="demo_top">
						<p>Insert Texts</p>
					</div>
					<div class="tab">
						<button class="tablink" onclick="clickTap(this, 'example')" id="defaultOpen2" >Example</button>
<%--						<button class="tablink" onclick="clickTap(this, 'article')" >Article</button>--%>
					</div>
					<div class="text_area tabcontent"  id="example">
						<textarea rows="7" id="id_input_text" placeholder="Please copy and paste articles from the news or web documents.">Born in Hungary in 1913 as Friedmann Endre Ernő, Capa was forced to leave his native country after his involvement in anti government protests. Capa had originally wanted to become a writer, but after his arrival in Berlin had first found work as a photographer. He later left Germany and moved to France due to the rise in Nazism. He tried to find work as a freelance journalist and it was here that he changed his name to Robert Capa, mainly because he thought it would sound more American.</textarea>
					</div>					

					<%--<div class="text_area tabcontent"  id="article">
						<div class="searchType">
							<input type="text" class="input_textType" title="" id="search_input" placeholder="Plaese search the news article.">
							<span class="btn"><button type="button" class="btn_search" id="search_button">Search</button></span>
						</div>
						<ul class="article_lst">
							<li for="radio">
								<div class="radio">
									<input type="radio" id="radio" name="radio_article">
									<label for="radio" class="article_item"></label>
								</div>
							</li>
							<li for="radio">
								<div class="radio">
									<input type="radio" id="radio2" name="radio_article">
									<label for="radio2" class="article_item" value=""></label>
								</div>
							</li>
							<li for="radio">
								<div class="radio">
									<input type="radio" id="radio3" name="radio_article">
									<label for="radio3" class="article_item" value=""></label>
								</div>
							</li>
							<li for="radio">
								<div class="radio">
									<input type="radio" id="radio4" name="radio_article">
									<label for="radio4" class="article_item" value=""></label>
								</div>
							</li>
							<li for="radio">
								<div class="radio">
									<input type="radio" id="radio5" name="radio_article">
									<label for="radio5" class="article_item" value=""></label>
								</div>
							</li>
							<li for="radio">
								<div class="radio">
									<input type="radio" id="radio6" name="radio_article">
									<label for="radio6" class="article_item" value=""></label>
								</div>
							</li>
						</ul>
						<div class="btn_area">
							<button type="button" class="btn_select" id="insert_selected_content">OK</button>
						</div>
					</div>--%>

				</div>
				<!--//.step01-->
				<!--.step02-->

				<div class="step02">
					<div class="demo_top">
						<p>Ask Questions</p>
					</div>
					<div class="text_area">
						<textarea class="example_area" id="id_input_question" placeholder="Ask questions on the article above.">Why did Capa changed his name?</textarea>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_search" id="find_answer_btn"><em class="fas fa-file-invoice"></em>Process
						</button>
					</div>
				</div>
				<!--//.step02-->

				<!--.step03-->
				<div class="step03">
					<div class="demo_top">
						<p style="width: 135px;">MRC Result</p>
					</div>
					<div class="resultBox">
						Reliability: <span>93.25%</span>
					</div>
					<div class="result_txt">
						신성 로마 제국(라틴어: Sacrum Romanum Imperium)은 중세에서 근대 초까지 이어진 기독교 성향이 강한 유럽 국가들의 정치적 연방체이다. 프랑크 왕국이 베르됭 조약(843년)으로 나뉜 동쪽에서 독일 왕이 마자르족을 격퇴한 후 <strong>교황</strong>으로부터 황제의 관을 수여받아 신성 로마 제국 건국을 선포하였다. 신성 로마 제국은 초기에는 강력한 중앙집권 국가였으나, 점차 이탈리아에 대한 간섭으로 독일 지역에 소홀히 하면서 여러 제후들에 의해 분할 상태가 되었다. <strong>30년 전쟁(1618~48년)</strong>이 일어난 나라로 유명하며, 30년 전쟁에 패배하여 베스트팔렌 조약(1648년)으로 많은 영토를 잃었다
					</div>
					<div class="btn_area">
<%--						<button type="button" class="btn_another">Ask another question</button>--%>
						<button type="button" class="btn_reset">Reset</button>
					</div>
				</div>
				<!--//.step03-->
			</div>
			<!--//.mrc_box-->

		</div>
		.<!--.demobox_mrc -->

		<!--.mrcmenu-->
		<div class="demobox" id="mrcmenu">

			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API Guideline
					</div>
					<p class="sub_title">Set up Environment</p>
					<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
					<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
					<p class="sub_title">ID &amp; Key</p>
					<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
					<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
					<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
						<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
					<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
					<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
					<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
				</div>
				<div class="guide_group">
					<div class="title">
						Bert MRC <small>(Machine Reading Comprehension)</small>
					</div>
					<p class="sub_txt">MINDs Lab's MRC is an innovative artificial intelligence (AI) technology that can instantly decode any document,regardless of content or volume, and answer your questions accurately.</p>

					<span class="sub_title">
						Preparation
					</span>
					<p class="sub_txt">① Input: Passage (text), Question (text)</p>
					<p class="sub_txt">② Language: Choose one below  </p>
					<ul>
						<li>Korean (kor)</li>
						<li>English (eng)</li>

					</ul>
					<span class="sub_title">
						API Document
					</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/bert.mrc/</li>
					</ul>
					<p class="sub_txt">② Request parameters </p>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId </td>
							<td>Unique API ID. Request from is required for the ID.</td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey</td>
							<td>Unique API key. Request from is required for the key.</td>
							<td>string</td>
						</tr>
						<tr>
							<td>lang </td>
							<td>Selected language (kor / eng) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>context </td>
							<td>Input passage (text)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>question</td>
							<td>Input question (text) </td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request example </p>
					<div class="code_box">
						curl -X POST \ <br>
						https://api.maum.ai/api/bert.mrc/ \<br>
						-H 'Content-Type: application/json' \<br>
						-d '{<br>
						<span>"apiId":"(*Request for ID)",</span><br>
						<span>"apiKey":"(*Request for key)",</span><br>
						<span>"lang":"eng",</span><br>
						<span>"context":"Born in Hungary in 1913 as Friedmann
Endre Ernő, Capa was forced to leave his native country
after his involvement in anti government protests. Capa
had originally wanted to become a writer, but after his arrival in Berlin had first found work as a photographer.
He later left Germany and moved to France due to the rise
in Nazism. He tried to find work as a freelance
journalist and it was here that he changed his name to
Robert Capa, mainly because he thought it would sound
more American.",</span><br>
						<span>"question":"Why did Capa changed his name?"</span><br>
						}'

					</div>

					<p class="sub_txt">④ Response parameters</p>
					<span class="table_tit">Response</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>message </td>
							<td>Status of API request </td>
							<td>list</td>
						</tr>
						<tr>
							<td>answer </td>
							<td>Output answer (text) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>prob </td>
							<td>Reliability of answer (0~1) </td>
							<td>number</td>
						</tr>
						<tr>
							<td>startIdx  </td>
							<td>Start point of the answer in the passage</td>
							<td>number</td>
						</tr>
						<tr>
							<td>endIdx </td>
							<td>End point of the answer in the passage </td>
							<td>number</td>
						</tr>
					</table>
					<span class="table_tit">message: Status of API request</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>message </td>
							<td>Status (Success/ Fail)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>status</td>
							<td>Status code (0: Success)  </td>
							<td>number</td>
						</tr>
					</table>

					<p class="sub_txt">⑤ Response example </p>
					<div class="code_box">
<pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "answer": "he thought it would sound more American",
    "prob": 0.2143673,
    "startIdx": 105,
    "endIdx": 111
}
</pre>
					</div>
				</div>
			</div>
			<!--//.guide_box-->
		</div>
		<!--//mrcmenu-->
		<!--.mrcexample-->
		<div class="demobox" id="mrcexample">
			<p><em style="font-weight: 400;color:#27c1c1;">Use Cases</em>  </p>
			<span class="sub">Find out how AI can be applied to diverse area.</span>
			<!-- AI 독해(MRC) -->
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>Unstructured Data Recognition</span>
							</dt>
							<dd class="txt">Accurately recognize a variety of unformatted and unstructured data. Through learning and Q&A systems for news, blogs, manual data, and other forms of data it can provide high data recognition rates and accurate answers.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_mrc"><span>MRC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>English education services</span>
							</dt>
							<dd class="txt">After children read an English book, MRC ask questions about the book, listen to the child answers, search and find them. It can be used for various English education services. </dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_mrc"><span>MRC</span></li>
									<li class="ico_stt"><span>STT</span></li>
									<li class="ico_tts"><span>TTS</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //AI 독해(MRC) -->
		</div>

	</div>

</div>
<!-- //.contents -->


<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/mrc.js"></script>


<script type="text/javascript">
	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){


			$("#find_answer_btn").on('click',function(){
                let sentence = $('#id_input_text').val();
                let question = $('#id_input_question').val();
				let language = $("input[type=radio][name=radio]:checked").val();
				if(sentence == "" || $.trim(sentence)==""){
					alert("Please copy and paste articles from the news or web documents!");
					return false;
				}
				if(question == "" || $.trim(question)==""){
					alert("Ask questions on the article above!");
					$("#id_input_question").val("");
					$("#id_input_question").focus();
					return false;
				}else{
                    let result="";
                    let original="";
                    let param = {
						'sentence' : sentence,
						'question' : question,
						'lang' : language,
						'${_csrf.parameterName}' : '${_csrf.token}'
					};

					let resultStr ="";
					let confidenceLv = null;
					$.ajax({
						url: '/api/mrc/',
						async: false,
						type: 'POST',
						headers: {
							"Content-Type": "application/x-www-form-urlencoded"
						},
						data: param,
						error: function(request){
							if (request.statusText == ""){
								alert("INTERNAL SERVER ERROR #1");
							}else if (request.statusText == "INTERNAL SERVER ERROR"){
								alert("INTERNAL SERVER ERROR #2");
							}else{
								alert(request.statusText);
							}
						},
						success: function(data){
							let responseData = JSON.parse(data);

							result = responseData['data'][0]['answers'][0]['answer'];
							original = responseData['data'][0]['original'];
							resultStr = original.toString().replace(result.toString(), "<strong>"+result.toString()+"</strong>");
							confidenceLv = (responseData['data'][0]['answers'][0]['prob']*100).toFixed(2);
						}

					});

					//결과 화면
					$(".resultBox").text("Reliability: "+confidenceLv+" %");
					$('.step03 .result_txt').html(resultStr);

					$('.mrc_box .step01').hide();
					$('.mrc_box .step02').hide();
					$('.mrc_box .step03').fadeIn();
				}

			});


			$('#search_button').click(function(){
				var txt = document.getElementById("search_input").value;
				if (txt == null || txt == ""){
					alert("검색어를 입력해주세요.");
					return;
				}
				var param = {'keyword' : txt, '${_csrf.parameterName}' : '${_csrf.token}'};
				$("#search_results").hide();

				$.ajax({
					type: 'POST',
					url: '/api/search_news',
					dataType: 'json',
					data: param,
					error: function(){
						alert('[에러] 검색어를 찾을 수 없습니다.');
						$("#search_results").show();
					}, success: function(data) {
						//console.log(JSON.stringify(data));
						//data = data['items'];
						$(".article_item").each(function (index) {
							if (index >= Object.keys(data).length) {
								return 0;
							}
							//console.log(data[index]["desc"]);
							//console.log(data[index]["title"]);
							$(this).attr('value', data[index]["desc"]);
							$(this).html(data[index]["title"]);
						});
						$("#search_results").show();
					}
				});
			});


		});

	});


</script>

