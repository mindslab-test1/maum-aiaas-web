<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">

<div class="contents">
	<div class="content api_content">
		<h1 class="api_tit">Voice Filter</h1>
		<ul class="menu_lst voice_menulst">
			<li class="tablinks" onclick="openTap(event, 'vfdemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
			<li class="tablinks" onclick="openTap(event, 'vfexample')"><button type="button">Use Case</button></li>
			<li class="tablinks" onclick="openTap(event, 'vfmenu')"><button type="button">Manual</button></li>
<%--			<li class="tablinks"><a href="/member/enApiAccount" >API ID & Key</a></li>--%>
		</ul>
		<!-- .demobox -->
		<div class="demobox" id="vfdemo">
			<p><span>Voice Filter</span> </p>
			<span class="sub">Extracts a specific speaker's voice from multiple voices.</span>
			<!--vf_box-->
			<div class="demo_layout vf_box">
				<!--vf_1-->
				<div class="vf_1" >
					<div class="fl_box">
						<p><em class="far fa-file-audio"></em><strong>Use Sample File</strong></p>
						<div class="sample_box">
							<div class="sample first_sample">
								<div class ="sample_tit ">
									<p>Specific<br>Speaker</p>
								</div>
								<!--player-->
								<div class="player">
									<div class="button-items">
										<audio id="music" class="music" preload="auto" onended="audioEnded($(this))">
											<source src="${pageContext.request.contextPath}/aiaas/kr/audio/voicefilter/voice_ref.wav">
											<p>Alas, your browser doesn't support html5 audio.</p>
										</audio>
										<div id="slider" class="slider">
											<div id="elapsed" class="elapsed"></div>
										</div>
										<p id="timer" class="timer">0:00</p>
										<p class="timer_fr">0:00</p>
										<div class="controls">
											<div id="play" class="play sample_play">
											</div>
											<div id="pause" class="pause">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="sample">
								<div class ="sample_tit ">
									<p>Multiple<br>Speakers</p>
								</div>
								<!--player-->
								<div class="player">
									<div class="button-items">
										<audio id="music2" class="music" preload="auto" onended="audioEnded($(this))">
											<source src="${pageContext.request.contextPath}/aiaas/kr/audio/voicefilter/voice_mixed.wav">
											<p>Alas, your browser doesn't support html5 audio.</p>
										</audio>
										<div class="slider">
											<div class="elapsed"></div>
										</div>
										<p class="timer">0:00</p>
										<p class="timer_fr">0:00</p>
										<div class="controls">
											<div class="play sample_play" >
											</div>
											<div class="pause" >
											</div>
										</div>
									</div>
								</div>
								<!--player-->
							</div>
						</div>
						<div class ="sample_box vf_play">
							<div class="sample">
								<div class ="sample_tit ">
									<p>Speaker from<br>mixture</p>
								</div>
								<!--player-->
								<div class="player">
									<div class="button-items">
										<audio id="music3" class="music" preload="auto" onended="audioEnded($(this))">
											<source src="${pageContext.request.contextPath}/aiaas/kr/audio/voicefilter/voice_result.wav">
											<p>Alas, your browser doesn't support html5 audio.</p>
										</audio>
										<div class="slider">
											<div class="elapsed"></div>
										</div>
										<p class="timer">0:00</p>
										<p class="timer_fr">0:00</p>
										<div class="controls">
											<div class="play sample_play" >
											</div>
											<div class="pause" >
											</div>
										</div>
									</div>
								</div>
								<!--player-->
							</div>
						</div>
					</div>
					<div class="fr_box">
						<p><em class="far fa-file-audio"></em><strong>Use My File</strong></p>
						<div class="uplode_box">
							<div class="file first_file">
								<div class ="file_tit">
									<p>Specific<br>Speaker</p>
								</div>
								<div class="btn" id="uploadFile">
									<em class="fas fa-times hidden close"></em>
									<em class="far fa-file-audio hidden"></em>
									<label for="demoFile" class="demolabel">Upload File</label>
									<input type="file" id="demoFile" class="demoFile"  >
								</div>
							</div>
							<div class="file sec_file">
								<div class ="file_tit ">
									<p>Multiple<br>Speakers</p>
								</div>
								<div class="btn" id="uploadFile2">
									<em class="fas fa-times hidden close2"></em>
									<em class="far fa-file-audio hidden"></em>
									<label for="demoFile2" class="demolabel2">Upload File</label>
									<input type="file" id="demoFile2" class="demoFile"  >
								</div>
							</div>
							<ul>
								<li>* .wav file only</li>
								<li>* Sample rate 16000 / mono channel</li>
								<li>* Under 2MB file size</li>

							</ul>
						</div>
						<div class="btn_area">
							<button type="button" class="btn_start" id="change_txt">Process</button>
						</div>
					</div>

				</div>
				<!--vf_1-->
				<!--vf_2-->
				<div class="vf_2">
					<p><em class="far fa-file-audio"></em>In progress</p>
					<div class="loding_box ">
						<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#c9c3ec" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#7e71d1" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

						<p>AI processing takes a while..</p>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_back1" id=""><em class="fas fa-redo"></em>Reset</button>
					</div>


				</div>
				<!--vf_2-->
				<!--vf_3-->
				<div class="vf_3">
					<p><em class="far fa-file-audio"></em>Result</p>
					<div class="result_file" >
						<div class="result_fl">
							<div class="result result_first" >
								<div class ="file_tit">
									<p>Specific<br>Speaker</p>
								</div>
								<!--player-->
								<div class="player">
									<div class="button-items">
										<audio id="music4" class="music" preload="auto" onended="audioEnded($(this))">
											<source src="${pageContext.request.contextPath}/aiaas/kr/audio/voicefilter/voice_ref.wav" >
											<p>Alas, your browser doesn't support html5 audio.</p>
										</audio>
										<div class="slider">
											<div class="elapsed"></div>
										</div>
										<p class="timer">0:00</p>
										<p class="timer_fr">0:00</p>
										<div class="controls">
											<div class="play" >
											</div>
											<div class="pause" >
											</div>
										</div>
									</div>
								</div>
								<!--player-->
							</div>
							<div class="result" >
								<div class ="file_tit file_tit2">
									<p>Multiple<br>Speakers</p>
								</div>
								<!--player-->
								<div class="player">
									<div class="button-items">
										<audio id="music5" class="music" preload="auto" onended="audioEnded($(this))">
											<source src="${pageContext.request.contextPath}/aiaas/kr/audio/voicefilter/voice_mixed.wav">
											<p>Alas, your browser doesn't support html5 audio.</p>
										</audio>
										<div class="slider">
											<div class="elapsed"></div>
										</div>
										<p class="timer">0:00</p>
										<p class="timer_fr">0:00</p>
										<div class="controls">
											<div class="play" >
											</div>
											<div class="pause" >
											</div>
										</div>
									</div>
								</div>
								<!--player-->
							</div>
						</div>
						<div class="result_fr">
							<div class ="file_tit">
								<p>Speaker from<br>mixture</p>
							</div>
							<!--player-->
							<div class="player">
								<div class="button-items">
									<audio id="music6" class="music" preload="auto" onended="audioEnded($(this))">
										<source src="${pageContext.request.contextPath}/aiaas/kr/audio/voicefilter/voice_mixed.wav" >
										<p>Alas, your browser doesn't support html5 audio.</p>
									</audio>
									<div class="slider">
										<div class="elapsed"></div>
									</div>
									<p class="timer">0:00</p>
									<p class="timer_fr">0:00</p>
									<div class="controls">
										<div class="play" >
										</div>
										<div class="pause" >
										</div>
									</div>
								</div>
							</div>
							<!--player-->
							<a id="btn_dwn" class="btn_dwn"><em class="far fa-arrow-alt-circle-down"></em> Download Result</a>
						</div>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_back2"><em class="fas fa-redo"></em>Reset</button>
					</div>
				</div>
			</div>
				<!--vf_3-->

		</div>
		<!-- //.demobox -->
		<!--.vfmenu-->
		<div class="demobox voice_menu" id="vfmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API Guideline
					</div>
					<p class="sub_title">Set up Environment</p>
					<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
					<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
					<p class="sub_title">ID &amp; Key</p>
					<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
					<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
					<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
						<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
					<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
					<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
					<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
				</div>
				<div class="guide_group">
					<div class="title">
						Voice Filter
					</div>
					<p class="sub_txt">Minds Lab's Voice Filter detect and extract the voice of a specific speaker frommultiple voices, whether it is a conversation or a mixture.</p>

					<span class="sub_title">
						Preparation
					</span>
					<p class="sub_txt">Input: 2 type of sound files</p>
					<ul>
						<li>File type: .wav</li>
						<li>Sample rate: 16000</li>
						<li>Channels: mono</li>
						<li>Note: Required both files below.<br>
							<ul style="border:none;width: calc(100% - 70px);margin: 0;padding: 0 0 0 30px;">
								<li style="list-style: disc;">File 1. Multiple speakers with a specific speaker</li>
								<li style="list-style: disc;">File 2. Specific speaker’s voice only</li>
							</ul>
						</li>
					</ul>
					<span class="sub_title">
						 API Document
					</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/dap/voiceFilter/</li>
					</ul>
					<p class="sub_txt">② Request parameters </p>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId </td>
							<td>Unique API  ID. Request from is required for the ID </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>Unique API key. Request from is required for the key </td>
							<td>string</td>
						</tr>
						<tr>
							<td>mixedVoice</td>
							<td>File 1 (wav) - multiple speakers </td>
							<td>string</td>
						</tr>
						<tr>
							<td>reqVoice</td>
							<td>File 2 (wav) - specific speaker</td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request example</p>
					<div class="code_box">
<pre>
curl -X POST \
   'https://api.maum.ai/api/dap/voiceFilter/ \
   -H 'content-type: multipart/form-data;
boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
   -F apiId=(*Request for ID) \
   -F apiKey=(*Request for key) \
   -F 'mixedVoice=@mixedsample.wav’\
   -F 'reqVoice=@speaker.wav'
</pre>
					</div>

					<p class="sub_txt">④ Response example </p>

					<div class="code_box">
						<audio src="${pageContext.request.contextPath}/aiaas/kr/audio/voicefilter.wav" preload="auto" controls lang="en"></audio>
						<span style="display: block; padding: 0;">* Extract specific speaker’s voice from multiple speakers’ file</span>
					</div>
				</div>
			</div>
			<!--//.guide_box-->
		</div>
		<!--.vfmenu-->
		<!--.vfexample-->
		<div class="demobox" id="vfexample">
			<p><em style="font-weight: 400;">Use Cases</em>  </p>
			<span class="sub">Find out how AI can be applied to diverse area.</span>
			<!-- voice filter -->
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>Automatic minutes</span>
							</dt>
							<dd class="txt">Even when voices overlap between speakers in a conference or discussion involving multiple speakers, only the voice of a specific speaker can be recognized and separated.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_vf"><span>Voice Filter</span></li>
									<li class="ico_stt"><span>STT</span></li>
<%--									<li class="ico_dia"><span>Diarization</span></li>--%>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>Specific speech extraction</span>
							</dt>
							<dd class="txt">From the voice file spoken by several people, it is possible to extract the voice of a specific person and apply it to speaker authentication service based on the extracted voice</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_vf"><span>Voice Filter</span></li>
									<li class="ico_stt"><span>STT</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //Voice Filter(Voice Filter) -->
		</div>
		<!--//.vfexample-->
	</div>
	<!-- //.content -->
</div>
<!-- //.contents -->


<script type="text/javascript">

	var timelineWidth = $('#slider').get(0).offsetWidth;
	var sample1File;
	var sample2File;
	var sample1SrcUrl;
	var sample2SrcUrl;


	// Set audio duration
	$('.music').each(function(){
		$(this).on("canplay", function () {
			var $parent = $(this).parent();
			var dur = this.duration;
			var fl_dur = Math.floor(dur);
			var endTime = toTimeFormat(fl_dur+"");
			$parent.children('.timer_fr').text(endTime);
		});
	});

	$(document).ready(function () {
		loadSample1();
		loadSample2();


		// Time update event
		$('.music').on("timeupdate", function(){
			var curIdx = $('.music').index($(this));
			timeUpdate($(this), curIdx);
		});

		// Audio play
		$('.play').on('click', function (me) {
			var curIdx = $('.play').index(this);

			$('.play').each(function (idx,e) {
				if (e !== me.currentTarget) {
					$('.pause').eq(idx).css("display","none");
					$('.play').eq(idx).css("display","block");
					$('.music').eq(idx).trigger("pause");
				}
			});

			$('.music').eq(curIdx).trigger("play");
			$(this).css("display","none");
			$('.pause').eq(curIdx).css("display","block");
		});

		// Audio pause
		$('.pause').on('click', function () {
			var curIdx = $('.pause').index(this);

			$('.music').eq(curIdx).trigger("pause");
			$(this).css("display","none");
			$('.play').eq(curIdx).css("display","block");
		});


		// 파일 업로드 후 이벤트
		document.querySelector("#demoFile").addEventListener('change', function (ev) {
			if(this.files[0] === undefined){
				$('em.close').trigger('click');
				return;
			}

			if (this.files[0].type.match(/audio\/wav/)){
				var file = this.files[0];
				var name = file.name;
				var size = (file.size / 1048576).toFixed(3); //size in mb
				$('.demolabel').text(name + ' (' + size + 'MB)');

				$('#uploadFile').removeClass( 'btn' );
				$('#uploadFile').addClass( 'btn_change' );
				//$('.fl_box').css("opacity", "0.5");
			}
			else{
				this.value = null;
				alert('Please upload a wav file.');
			}
		});


		// Remove uploaded file
		$('em.close').on('click', function () {
			$('#demoFile').val(null);
			$('.demolabel').text('Upload File');
			$(this).parent().removeClass("btn_change");
			$(this).parent().addClass("btn");
			//$('.fl_box').css('opacity', '1');
		});

		
		// 파일 업로드 후 이벤트
		document.querySelector("#demoFile2").addEventListener('change', function (ev) {
			if(this.files[0] === undefined){
				$('em.close2').trigger('click');
				return;
			}

			if (this.files[0].type.match(/audio\/wav/)){
				var file = this.files[0];
				var name = file.name;
				var size = (file.size / 1048576).toFixed(3); //size in mb
				$('.demolabel2').text(name + ' (' + size + 'MB)');

				$('#uploadFile2').removeClass( 'btn' );
				$('#uploadFile2').addClass( 'btn_change' );
				//$('.fl_box').css("opacity", "0.5");
			}
			else{
				this.value = null;
				alert('Please upload a wav file.');
			}
		});
		
		// Remove uploaded file
		$('em.close2').on('click', function () {
			$('#demoFile2').val(null);
			$('.demolabel2').text('Upload File');
			$(this).parent().removeClass("btn_change");
			$(this).parent().addClass("btn");
			//$('.fl_box').css('opacity', '1');
		});

		// step2->step3
		$('.btn_back1').on('click', function () {
			$('.vf_2').hide();
			$('.vf_3').fadeIn(300);

		});

		// Result -> Reset
		$('.btn_back2').on('click', function () {
			$('.vf_3').hide();
			$('.vf_1').fadeIn(300);

			// audio UI setting
			$('.music').each(function(){ this.currentTime = 0; });
			$('.elapsed').css("width", "0px");
			$('.timer').text("0:00");
		});


		$('#return').on('click', function () {
			window.location.reload();
		});


		$('#change_txt').on('click',function (){

			var reqFileTxt = $('#demoFile').val(); //파일을 추가한 input 박스의 값
			var reqTxt = reqFileTxt.split(".").pop(-1).toLowerCase();

			var mixedFileTxt = $('#demoFile2').val(); //파일을 추가한 input 박스의 값
			var mixedTxt = mixedFileTxt.split(".").pop(-1).toLowerCase();

			if(mixedTxt != "wav" || reqTxt != "wav"){
				alert("Invalid file format Only wav files are supported.");
				window.location.reload();
			}
			else {
				$('.vf_1').hide();
				$('.vf_2').fadeIn(300);

				var reqVoiceInput = document.getElementById('demoFile');
				console.log(reqVoiceInput);
				var reqVoice = reqVoiceInput.files[0];
				console.log(reqVoice);
				var mixedVoiceInput = document.getElementById('demoFile2');
				console.log(mixedVoiceInput);
				var mixedVoice = mixedVoiceInput.files[0];
				console.log(mixedVoice);

				// var form = $('form')[0];
				var formData = new FormData();

				formData.append('reqVoice',reqVoice);
				formData.append('mixedVoice',mixedVoice);

				formData.append('${_csrf.parameterName}', '${_csrf.token}');
				var request = new XMLHttpRequest();
				//
				request.responseType ="blob";
				request.onreadystatechange = function(){
					if (request.readyState === 4 && request.status !== 0){
						console.log(request.response);
						var blob = request.response;
						var audioSrcURL = window.URL.createObjectURL(blob);

						var link = document.getElementById("btn_dwn");
						link.href = audioSrcURL;
						link.download = "voicefilter.wav" ;

						$('#music6').attr('src',audioSrcURL);
						$('.vf_2').hide();
						$('.vf_3').fadeIn(300);
		                $('.play').eq(5).trigger('click');
					}
				};
				request.open('POST', '/api/dap/voicefilter');
				request.send(formData);
				
				request.addEventListener("abort", function(){ });

				// step2->step1
				$('.btn_back1').on('click', function () {
					if(request){ request.abort(); }

					var curIdx = $('.pause').index(this);
					$('.music').eq(curIdx).trigger("pause");

					// audio UI setting
					$('.music').each(function(){ this.currentTime = 0; });
					$('.elapsed').css("width", "0px");
					$('.timer').text("0:00");

					$('.vf_2').hide();
					$('.vf_3').hide();
					$('.vf_1').fadeIn(300);
				});				
			}

		});

	});
	
//------------------ Functions --------------------------

	// Update current play time and player bar
    //      obj : current .music jquery object
    //      idx : currnet .music index (= audio player index)
	function timeUpdate(obj, idx) {
		var cur_music = obj.get(0);
		var playHead = $('.elapsed').eq(idx).get(0);
		var timer = $('.timer').eq(idx).get(0);

		var playPercent = timelineWidth * (cur_music.currentTime / cur_music.duration);
		playHead.style.width = playPercent + "px";

		var secondsIn = Math.floor(cur_music.currentTime);
		var curTime = toTimeFormat(secondsIn+"");
		timer.innerHTML = curTime;
	}


	function audioEnded(audio){
	    var idx = $('.music').index(audio);
        // audio UI setting
        $('.pause').eq(idx).trigger('click');
        $('.music').eq(idx).get(0).currentTime = 0;
        $('.elapsed').eq(idx).css("width", "0px");
        $('.timer').eq(idx).text("0:00");
    }	
	
	function loadSample1() {
		var blob = null;
		var xhr = new XMLHttpRequest();
		xhr.open("GET", "/aiaas/kr/audio/voicefilter/voice_ref.wav");
		xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
		xhr.onload = function()
		{
			blob = xhr.response;//xhr.response is now a blob object
			sample1File = new File([blob], "voice_ref.wav");
			sample1SrcUrl = URL.createObjectURL(blob);
		};

		xhr.send();
	}

	function loadSample2() {
		var blob = null;
		var xhr = new XMLHttpRequest();
		xhr.open("GET", "/aiaas/kr/audio/voicefilter/voice_mixed.wav");
		xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
		xhr.onload = function()
		{
			blob = xhr.response;//xhr.response is now a blob object
			sample2File = new File([blob], "voice_mixed.wav");
			sample2SrcUrl = URL.createObjectURL(blob);
		};

		xhr.send();
	}

	function toTimeFormat (text) {
		var sec_num = parseInt(text, 10); // don't forget the second param
		var hours   = Math.floor(sec_num / 3600);
		var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
		var seconds = sec_num - (hours * 3600) - (minutes * 60);

		// if (hours   < 10) {hours   = "0"+hours;}
		if (minutes < 10) {minutes = minutes;}
		if (seconds < 10) {seconds = "0"+seconds;}

		return minutes+':'+seconds;
	}

</script>
<script>
	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
</script>
