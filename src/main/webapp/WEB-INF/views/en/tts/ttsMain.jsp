<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- 5 .pop_simple -->
<div class="pop_simple " id="api_fail_popup">
	<div class="pop_bg"></div>
	<!-- .popWrap -->
	<div class="popWrap" style="position: inherit">
		<button class="pop_close" type="button">Close pop-up</button>
		<!-- .pop_bd -->
		<div class="pop_bd">
			<em class="fas fa-exclamation-triangle"></em>
			<p style="font-size: small"><strong></strong></p>
			<p style="font-size: small">Failed to get response from server<br>Please try again</p>

		</div>
		<!-- //.pop_bd -->
		<div class="btn" id="close_api_fail_popup">
			<a class="">OK</a>
		</div>
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_simple -->


<div class="contents">
	<!-- .api_content -->
	<div class="content api_content">
		<h1 class="api_tit">Speech Generation</h1>
		<ul class="menu_lst voice_menulst">
			<li class="tablinks" onclick="openTap(event, 'ttsdemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
			<li class="tablinks" onclick="openTap(event, 'ttsexample')"><button type="button">Use Case</button></li>
			<li class="tablinks" onclick="openTap(event, 'ttsmenu')"><button type="button">Manual</button></li>
<%--			<li class="tablinks"><a href="/member/enApiAccount">API ID, key</a></li>--%>
		</ul>
		<!--.ttsdemo-->
		<div class="demobox ttsdemo apittsdemo" id="ttsdemo">
			<p><span>Speech Generation</span></p>
			<span class="sub">Generate human-like and natural sounding voice.</span>
			<div class="selectarea">
				<div class="radio">
					<input type="hidden" id="voice4" value="Minds Lab's Voice Generation is not a voice synthesis. It is a state of the art future technology.">
					<input type="radio" id="4" name="voice" data-lang="en_US" data-speakerid=""  checked="checked">
					<label for="4">
						<span><strong>English</strong> </span>
						<div class="btn_bar">
							<div class="tit"><p>Voice Type </p> <em class="fas fa-caret-down"></em></div>
							<ul>
								<li>Voice Type</li>
								<li data-speakerid="eng_male1">Brandon</li>
								<li data-speakerid="baseline_eng">Selena</li>
							</ul>
						</div>
					</label>
				</div>
				<div class="radio">
					<input type="hidden" id="voice1" value="인공지능의 모든 것, 마음AI에서 경험해보세요. 다양한 엔진들을 만나 보실 수 있습니다.">
					<input type="radio" id="1" name="voice" data-lang="ko_KR" data-speakerid="">
					<label for="1">
						<span><strong>Korean</strong> Female</span>
						<div class="btn_bar">
							<div class="tit"><p>Voice Type </p><em class="fas fa-caret-down"></em></div>
							<ul>
								<li>Voice Type</li>
								<li data-speakerid="kor_female1">Calm</li>
								<li data-speakerid="kor_female2">Cheerful</li>
								<li data-speakerid="kor_female3">Natural</li>
								<li data-speakerid="kor_female4">Warm</li>
								<li data-speakerid="kor_female5">Eccentric</li>
								<li data-speakerid="baseline_kor">Sweet</li>
							</ul>
						</div>
					</label>

				</div>
				<div class="radio">
					<input type="hidden" id="voice2" value="인공지능이 필요할 땐, 마음AI를 찾아주세요. 99,000원에 모두 이용 가능합니다.">
					<input type="radio" id="2" name="voice" data-lang="kor_kids" data-speakerid="">
					<label for="2">
						<span><strong>Korean</strong> Male</span>
						<div class="btn_bar">
							<div class="tit"><p>Voice Type</p> <em class="fas fa-caret-down"></em></div>
							<ul>
								<li>Voice Type</li>
								<li data-speakerid="kor_male1">Genuine</li>
								<li data-speakerid="kor_male2">Confident</li>
								<li data-speakerid="kor_male3">Calm</li>
								<li data-speakerid="kor_male4">Friendly</li>
							</ul>
						</div>
					</label>
				</div>
				<div class="radio">
					<input type="hidden" id="voice3" value="마인즈랩의 음성생성 기술은 아무도 따라할 수 없어, 세계 최고인것 같아.">
					<input type="radio" id="3" name="voice" data-lang="kor_kids" data-speakerid="">
					<label for="3">
						<span><strong>Korean </strong> Kids</span>
						<div class="btn_bar">
							<div class="tit"><p>Voice Type </p> <em class="fas fa-caret-down"></em></div>
							<ul>
								<li>Voice Type</li>
								<li data-speakerid="kor_kids_m">Boy</li>
								<li data-speakerid="kor_kids_f">Girl</li>
							</ul>
						</div>
					</label>
				</div>

			</div>
<%--			<div class="selectarea">--%>
<%--				<div class="radio">--%>
<%--					<input type="hidden" id="voice4" value="Minds Lab's Voice Generation is not a voice synthesis. It is a state of the art future technology.">--%>
<%--					<input type="radio" id="4" name="voice" data-lang="en_US" data-speakerid="baseline_eng" checked="checked" >--%>
<%--					<label for="4"><img src="${pageContext.request.contextPath}/aiaas/en/images/img_tts4.png" alt="English Female"><span>English <br><strong>Female</strong></span></label>--%>
<%--				</div>--%>
<%--				<div class="radio">--%>
<%--					<input type="hidden" id="voice1" value="마인즈랩의 음성 생성은, 기존 음성 합성과 확연히 다릅니다. 가장 최신의 인공지능을 경험해보세요.">--%>
<%--					<input type="radio" id="1" name="voice" data-lang="ko_KR" data-speakerid="baseline_kor">--%>
<%--					<label for="1"><img src="${pageContext.request.contextPath}/aiaas/en/images/img_tts1.png" alt="Female"><span>Korean <br><strong>Female</strong></span></label>--%>
<%--				</div>--%>
<%--				<div class="radio">--%>
<%--					<input type="hidden" id="voice2" value="마인즈랩의 음성생성 기술은 아무도 따라할 수 없어, 세계 최고인것 같아.">--%>
<%--					<input type="radio" id="2" name="voice" data-lang="kor_kids" data-speakerid="kor_kids_m">--%>
<%--					<label for="2"><img src="${pageContext.request.contextPath}/aiaas/kr/images/img_tts2.png" alt="남자아이"><span>Korean <br>Boy</span></label>--%>
<%--				</div>--%>
<%--				<div class="radio">--%>
<%--					<input type="hidden" id="voice3" value="마인즈랩의 음성생성 기술은 아무도 따라할 수 없어, 세계 최고인것 같아.">--%>
<%--					<input type="radio" id="3" name="voice" data-lang="kor_kids" data-speakerid="kor_kids_f">--%>
<%--					<label for="3"><img src="${pageContext.request.contextPath}/aiaas/kr/images/img_tts3.png" alt="여자아이"><span>Korean <br>Girl</span></label>--%>
<%--				</div>--%>
<%--			</div>--%>

			<div class="tts_demo demoBox">
				<!-- .tabUi -->
				<div class="tabUi demoTab">
					<!-- #demo01 -->
					<div id="demo01" class="tab_contents">
						<div class="demoBox">

							<!-- .step01 -->
							<div class="demo_intro">

								<div class="demo_infoTxt">
									<textarea id="text-contents" class="textArea apitextArea" rows="8" maxlength="1000" placeholder="Type in a sentence within 1000 characters.">Minds Lab's Voice Generation is not a voice synthesis. It is a state of the art future technology.</textarea>
									<div class="text_info">
										<span class=""><strong id="count">0</strong>/1000</span>
									</div>
								</div>

								<div class="btnBox">
<%--									<div class="holeBox">--%>
<%--										<div class="hole">--%>
<%--											<em></em><em></em><em></em><em></em><em></em>--%>
<%--										</div>--%>
<%--									</div>--%>
									<button class="demoRecord"><span><em class="fas fa-play-circle"></em><strong>Process</strong></span></button>
								</div>
							</div>
							<!-- //.step01 -->
							<!-- .step02 -->
							<div class="demo_recording">
								<div class="demo_infoTxt">Please wait. <br>The text is currently converting to audio.</div>
								<!-- recordingBox -->
								<div class="recordingBox">

									<div class="recording1 loader">
									</div>
								</div>
								<!-- //recordingBox -->
							</div>
							<!-- //.step02 -->

							<!-- .step03 -->
							<div class="demo_recording2">
								<div class="demo_infoTxt">Audio is playing.</div>
								<!-- recording -->
								<div class="recording">
									<span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>
									<span></span><span></span><span></span><span></span><span></span><span></span><span></span>
								</div>
								<!-- //recording -->
								<div class="btnBox">
									<button id="resetBtn" class="demoReset"><span>Reset</span></button>
 									<button class="dwn" onclick="fileDownload()"><span>Download</span></button>
<%--									<button class="demoReplay btnbtn"><span>Replay</span></button>--%>
								</div>

							</div>
							<!-- //.step03 -->
							<!-- .step04 -->
<%--							<div class="demo_result">--%>
<%--								<div class="demo_infoTxt">Now the recording is available for download.</div>--%>
<%--								<div class="demo_player">--%>
<%--									<audio src="..//aiaas/en/audio/aekukka.mp3" preload="auto" controls></audio>--%>
<%--								</div>--%>
<%--								<div class="btnBox">--%>
<%--									<button class="demoReset"><span>Start Again</span></button>--%>
<%--									<button class="dwn"><span>Download</span></button>--%>
<%--								</div>--%>
<%--							</div>--%>
							<!-- //.step04 -->
						</div>
					</div>
					<!-- //#demo01 -->

				</div>
				<!-- //.tabUi -->
				<div class="remark">
					* Please contact us if you inquire additional languages<br>
					* We provide customized service to adjust the current model, such as special characters (ex., #, *, ~) or special units (ex., ㎖, ㎡, ㎧ ㎉, ...).
				</div>
			</div>
		</div>
		<!--//.ttsdemo-->
		<!--.ttsmenu-->
		<div class="demobox voice_menu" id="ttsmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API Guideline
					</div>
					<p class="sub_title">Set up Environment</p>
					<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
					<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
					<p class="sub_title">ID &amp; Key</p>
					<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
					<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
					<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
						<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
					<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
					<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
					<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
				</div>
				<!--guide_group-->
				<div class="guide_group">

					<div class="title">
						Voice Generation (Text-To-Speech)
					</div>
					<p class="sub_txt">MINDs lab Voice Generation API converts texts into natural human voice.</p>

					<span class="sub_title">
						Preparation
					</span>
					<p class="sub_txt">① Input: Text(String) *Under 1000 characters </p>
					<p class="sub_txt">② Language: Choose one below</p>
					<ul>
						<li>English - Selena (baseline_eng)</li>
						<li>English - Brandon (eng_male1)</li>
						<li>Korean Female - Calm (kor_female1)</li>
						<li>Korean Female - Cheerful (kor_female2)</li>
						<li>Korean Female - Natural (kor_female3)</li>
						<li>Korean Female - Warm (kor_female4)</li>
						<li>Korean Female - Eccentric (kor_female5)</li>
						<li>Korean Female - Sweet (baseline_kor)</li>
						<li>Korean Male - Genuine (kor_male1)</li>
						<li>Korean Male - Confident (kor_male2)</li>
						<li>Korean Male - Calm (kor_male3)</li>
						<li>Korean Male - Friendly (kor_male4)</li>
						<li>Korean kids - Boys(kor_kids_m)</li>
						<li>Korean kids girls(kor_kids_f)</li>
					</ul>
					<span class="sub_title">
						API Document
					</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/tts/stream</li>
					</ul>
					<p class="sub_txt">② Request Parameters</p>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId </td>
							<td>Unique API ID. Request from is required for the ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey</td>
							<td>Unique API key. Request from is required for the key. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>text</td>
							<td>Input string (*Under 1000 characters)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>voiceName</td>
							<td>Selected voice model</td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request example</p>
					<div class="code_box">
<pre>
curl -X POST \
  https://api.maum.ai/tts/stream \
  -H 'Content-Type: application/json' \
  -d '{
    "apiId": "(*Request for ID)",
    "apiKey": "(*Request for key)",
    "text": "Welcome to Minds Lab.",
    "voiceName": "baseline_eng"
}'</pre>
					</div>
					<p class="sub_txt">④ Response example </p>
					<div class="code_box">
						audio/x-wav type chunked data (StreamBody)
					</div>
				</div>
				<!--//.guide_group-->
			</div>
			<!--//.guide_box-->
		</div>
		<!--//ttsmenu-->
		<!--.ttsexample-->
		<div class="demobox" id="ttsexample">
			<p><em style="font-weight: 400;">Use Cases</em>  </p>
			<span class="sub">Find out how AI can be applied to diverse area.</span>
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>AI auto Assisted Call</span>
							</dt>
							<dd class="txt">It can be applied to AI auto assisted call, which replaces repetitive calls such as surveys and product quality surveys with AI</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_tts"><span>TTS</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>Guide Service</span>
							</dt>
							<dd class="txt">Use realistic TTS voice for various purposes such as guidance and assistant service.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_tts"><span>TTS</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 03</em>
								<span>YouTube Creators</span>
							</dt>
							<dd class="txt">Produce creative video contents much easier using TTS for YouTube or Vlog.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_tts"><span>TTS</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 04</em>
								<span>Game Characters</span>
							</dt>
							<dd class="txt">Create character voices, sound effects, and more in games.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_tts"><span>TTS</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 05</em>
								<span>Education Contents</span>
							</dt>
							<dd class="txt">Create educational contents for kids.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_tts"><span>TTS</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 06</em>
								<span>News Briefing</span>
							</dt>
							<dd class="txt">Provide personalized news briefing service in any voice the customer needs.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_tts"><span>TTS</span></li>
									<li class="ico_xdc"><span>XDC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //음성 인식(TTS) -->
		</div>

	</div>
</div>


<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/tts/app.js?20190816"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/common/js/tts/pcm-player.js"></script>

<script type="text/javascript">
	jQuery.event.add(window, "load", function () {
        $(document).ready(function () {
			var $textArea = $('.demoBox .textArea');
			var $holeBox = $('.holeBox');

			$holeBox.hide();

			$('.selectarea .radio').on("click",function(){
				$textArea.val($('#voice'+$(this).children('input[type="radio"]').attr("id")).val());
				countTextAreaLength();
				// $holeBox.show();
			});

			$('.selectarea .radio:first-child').trigger('click');


            // textArea event
			$textArea.on('input keyup paste change', function () {
                if ($textArea.val().trim() !== "") {
					// $holeBox.show();
                } else {
					$holeBox.hide();
                }
				countTextAreaLength(); // typing count
            });


            // textArea 'enter key' event
			$textArea.keypress(function (e) {
				if (e.keyCode === 13) {
                    $('.demo_intro .btnBox .demoRecord').trigger('click');
                }
			});

            // step01 > step02  : 음성생성 버튼 클릭 event
            $('.tts_demo button.demoRecord').on('click', function () {
				if ($textArea.val().trim() === "") {
					alert("Please input text.");
					$textArea.val("");
					$(this).focus();
					return 0;
				}

				var $checked =$("li.checked");
				var text = $textArea.val();
				var s_id = $checked.data("speakerid");

				if($checked.text()=== "" || $checked.text() ==="Voice Type") {
					alert('Choose the Voice Type.')
				}else {
					speak(text, s_id);

					$('.demo_intro').hide();
					$('.selectarea').hide();
					$('.demo_recording').show();
				}

            });

            // step04 > step01
            $('.tts_demo .demoReset').on('click', function () {
				// $('.textArea').val("");
				// $('#count').html(0);
                $('.demo_recording').hide();
                $('.demo_recording2').hide();
                $('.selectarea').show();
                $('.demo_intro').show();
			});

			$('.tit').on('click',function(){
				var openClass = $(this).parent();
				if(openClass.hasClass('open')){
					$('.btn_bar').removeClass('open');
					$('.btn_bar ul').slideUp(200);

					$(this).parent().removeClass('open');
					$(this).next().slideUp(200);
				}else {
					$('.btn_bar').removeClass('open');
					$('.btn_bar ul').slideUp(200);
					$(this).parent().addClass('open');
					$(this).next().slideDown(200);
				}
			})


			$('.btn_bar li').on('click', function(){
				$('.tit p').text('Voice Type');
				$('.btn_bar li').removeClass('checked')
				$(this).parent().parent().children('.tit').children('p').text($(this).text());
				$(this).parent().parent().removeClass('open');
				$(this).parent().slideUp(200);
				$(this).addClass('checked')
			});

			// 팝업 닫기
			$('.pop_close, .pop_bg, .btn a').on('click', function () {
				$('.pop_simple').fadeOut(300);
				$('body').css({ 'overflow': ''});
			});

		});
	});

	var pcmPlayer = null;
	function speak(text, voiceName){

		var _url = "/api/tts/ttsStream";

		console.log(text);
		console.log(voiceName);

		if (pcmPlayer != null) {
			pcmPlayer.destroy();
		}

		pcmPlayer = new PCMPlayer({
			encoding: '16bitInt',
			channels: 1,
			sampleRate: 22050,
			flushingTime: 500
		}, 'en');

		let req = {
			method: 'POST',
			headers: new Headers({"Content-Type": "application/json", '${_csrf.headerName}': '${_csrf.token}'}),
			body: JSON.stringify({'text': text, 'voiceName':voiceName}),
			mode: 'cors',
			cache: 'default'
		};

		fetch(_url, req).then((response) => {
			if(response.ok){
				const reader = response.body.getReader();
				let data = null;

				function read() {
					return reader.read().then(({value, done}) => {
						if (done) {
							pcmPlayer.lastChunk = true;
						} else {
							if (data != null) {
								let temp = new Uint8Array(value.length + 1);
								temp.set(data, 0);
								temp.set(value, 1);
								data = temp;
							} else {
								data = value;
							}
							if (data.length % 2 === 0) {
								pcmPlayer.feed(data);
								data = null;
							} else {
								pcmPlayer.feed(data.slice(0, data.length - 1));
								data = data.slice(data.length - 1, data.length);
							}
							return read();
						}
					})
				}
				return read();
			}else{
				console.log('Network error.');
				$('#api_fail_popup .pop_bd p:nth-child(2) strong').text("Network error");
				$('#api_fail_popup').fadeIn(300);
				$('.tts_demo .demoReset').click();
			}
		}).then( () => {
			console.log('all stream bytes queued for decoding');
		}).catch(e => {
			console.dir(e);
			console.log("speak error");
			$('#api_fail_popup .pop_bd p:nth-child(2) strong').text("Speak error");
			$('#api_fail_popup').fadeIn(300);
			$('.tts_demo .demoReset').click();
		})

	}


	function fileDownload() {
		var file_name_key = "TTS.wav";
		var file_name = "TTS.wav";
		location.href = encodeURI("/api/tts/ttsDwn?fileNameKey=" + file_name_key + "&fileName=" + file_name);
	}

	function countTextAreaLength (){
		let content = $('#text-contents').val();
		if (content.length >= 1000) {
			$('#count').html(1000);
			return;
		}
		$('#count').html(content.length);
	}



	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();

</script>