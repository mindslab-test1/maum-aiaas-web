<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
	<!-- .content -->
	<div class="content api_content">
		<h1 class="api_tit">GPT-2</h1>
		<ul class="menu_lst">
			<li class="tablinks" onclick="openTap(event, 'mrcdemo')" id="defaultOpen">Demo</li>
			<li class="tablinks" onclick="openTap(event, 'mrcmenu')">Manual</li>
			<li class="tablinks" onclick="openTap(event, 'mrcexample')">Use Case</li>
			<li class="tablinks"><a href="/member/enApiAccount">Purchase</a></li>
		</ul>
		<!-- .demobox -->
		<div class="demobox demobox_mrc" id="mrcdemo">
			<p>GPT-2<small style="font-size:14px;"> (Generative Pre-Training)</small></p>
			<span class="sub">AI Composition</span>
			<div class="mrc_box">
				<div class="step01">
					<div class="mrc_wrap">
						<div class="demo_top">
							<p>Insert Text</p>
						</div>
					</div>
					<div class="selectarea">
						<div class="select_box">
							<label for="lang">Language</label>
							<select id="lang">
								<option value="eng">English</option>
								<option value="kor">Korean</option>
							</select>
						</div>
					</div>
					<div class="text_area">
						<textarea rows="12" maxlength="1050" id="id_input_text" placeholder="Please put the sentence that is the source of the sentence generation."></textarea>
					</div>
					<div class="btn_area">
						<button type="button" class="start_btn" id="classify_content" disabled><em class="fas fa-file-invoice"></em>Writing</button>
						<%--<span class="disBox"></span>--%>
					</div>
				</div>
				<div class="step03">
					<div class="demo_top">
						<p style="width: 180px;">Original Sentence</p>
					</div>

					<div class="result_txt requestText"></div>
					<br><br>
					<div class="demo_top">
						<p style="width: 180px;">Result Sentence</p>
					</div>

					<div class="result_txt resultText"></div>
					<div class="btn_area">
						<button type="button" class="btn_reset" id="" >Back</button>
					</div>
				</div>
			</div>
		</div>
		<!-- //.demobox -->
		
		<!--.mrcmenu-->
		<div class="demobox" id="mrcmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API Guideline
					</div>
					<p class="sub_title">Set up Environment</p>
					<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
					<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
					<p class="sub_title">ID &amp; Key <span>*Business and Enterprise only</span></p>
					<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
					<p class="sub_txt">2&#41; Send an email to hello@mindslab.ai with your name and email address.</p>
					<p class="sub_txt">3&#41; After the agreement, Minds Lab would send you an ID with a key.</p>
					<p class="sub_txt">4&#41; Remember the ID and key.</p>
					<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s ID and key.</p>
				</div>
				<div class="guide_group">
					<div class="title">
						GPT <small>()</small>
					</div>
					<p class="sub_txt"> Sub-Title</p>

				</div>
			</div>
			<!--//.guide_box-->
		</div>
		<!--//mrcmenu-->
		<!--.mrcexample-->
		<div class="demobox" id="mrcexample">
			<div class="coming_txt">
				<h5>Coming soon</h5>
			</div>
		</div>
	</div>
	<!-- //.content -->
</div>
<!-- //.contents -->

<script type="text/javascript">
	$(window).load(function() {
		$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
			$(this).remove();
		});
	});
</script>
<script type="text/javascript">
	jQuery.event.add(window,"load",function(){
		$(document).ready(function() {
			$('.step03').hide();
			
			$('.mrc_box .text_area textarea').on('input keyup paste', function() {
				var txtValLth = $(this).val().length;
				if ( txtValLth > 0) {
					$('.progress li:nth-child(2)').addClass('active');
					$('.mrc_box .btn_area button').removeClass('disable');
					$('.mrc_box .btn_area button').removeAttr('disabled');
					$('.mrc_box .btn_area button').addClass('start_btn');
				} else {
					$('.progress li:nth-child(2)').removeClass('active');
					$('.mrc_box .btn_area button').addClass('disable');
					$('.mrc_box .btn_area button').attr('disabled');
					$('.mrc_box .btn_area button').removeClass('start_btn');
				}
			});
			
			/* $('#classify_content').on('click', function () {
				$(".step01").hide();
				$('.step03').show();
				//$(".step03").fadeIn(300);
			}); */
			
			$('.btn_reset').on('click', function () {
                $('#id_input_text').val("");
                $(".result_txt").val("");
				$('.step03').hide();
				$('.mrc_wrap').show();
				
				$(".step01").show();
			});
			
			var hideResult = true;
			$("#classify_content").click(function(){
				$("#loading_gif").show();
								
				if(hideResult) {
					var txt = document.getElementById('id_input_text').value;
					var language = document.getElementById('lang').value;
					
					if(txt == null || txt.length <= 0) {
						alert("Please put the source of the sentence generation.");	
						$("#loading_gif").hide();
						return;
					}
					
					$.ajax({
						type: 'POST',
						url: '/api/gpt',
						data: {
							"context": txt,
							"apiId": "minds-api-performance-test",
							"apiKey": "4SBRGrq9",
							"language": language,
							'${_csrf.parameterName}' : '${_csrf.token}'
						},
						error: function(xhr, status, error){
							$(".step01").show();
							$(".step03").hide();
							$("#loading_gif").hide();
							alert('[Error] Request fail.');
							console.dir(xhr.responseText);
							console.dir(status);
							console.dir(error);
						},
						success: function(data) {

							$(".step01").hide();
							$(".step03").show();
							$(".step03").fadeIn();
							
							var responseData = JSON.parse(data);
							var responseString = JSON.stringify(responseData);
							
							var status = responseData['message']['status'];
							if(status == 0) {
								// 성공
								//결과 화면
								var resultStr = responseData['result'];
								$(".resultText").html(resultStr);
								var reqText = responseData['reqText'];
								$(".requestText").html(reqText);
							}
						}
					}).done(function(data) {
						$("#loading_gif").hide();
						console.dir(data);
					});
				}
			});
		});
	});
	
	//API 탭  	
	function openTap(evt, menu) {
	  var i, demobox, tablinks;
	  demobox = document.getElementsByClassName("demobox");
	  for (i = 0; i < demobox.length; i++) {
	    demobox[i].style.display = "none";
	  }
	  tablinks = document.getElementsByClassName("tablinks");
	  for (i = 0; i < tablinks.length; i++) {
	    tablinks[i].className = tablinks[i].className.replace(" active", "");
	  }
	  document.getElementById(menu).style.display = "block";
	  evt.currentTarget.className += " active";
	}	
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();	
</script>