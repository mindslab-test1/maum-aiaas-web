<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
	<!-- .content -->
	<div class="content api_content">
		<h1 class="api_tit">GPT-2</h1>
		<ul class="menu_lst">
			<li class="tablinks" onclick="openTap(event, 'gptdemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
			<li class="tablinks" onclick="openTap(event, 'gptexample')"><button type="button">Use Case</button></li>
			<li class="tablinks" onclick="openTap(event, 'gptmenu')"><button type="button">Manual</button></li>
<%--			<li class="tablinks"><a href="/member/enApiAccount" >API ID, key</a></li>--%>
		</ul>
		<!-- .gptdemo -->
		<div class="demobox demobox_gpt" id="gptdemo">
			<p><span>GPT-2 </span><small style="font-size:14px;">(Generative Pre-Training)</small></p>
			<span class="sub">Generates multiple sentences related with the given text.
<%--				<br>* NLG can be applicable in various ways. Contact us for customized service.--%>
			</span>
			<div class=" gpt_box">
				<div class="mrclang_select" >
					<span>Language</span>
					<div class="radio">
						<input type="radio" id="radio_lang_eng" name="radio_lang" checked="checked" value="eng"><label for="radio_lang_eng">English</label>
					</div>
					<div class="radio">
						<input type="radio" id="radio_lang_kor" name="radio_lang" value="kor"><label for="radio_lang_kor">Korean</label>
					</div>

				</div>

				<!-- =================================================================================================================== -->
				<!-- 예문 및 사용자 입력 -->
				<!-- =================================================================================================================== -->
				<!--.step01-->
				<div class="step01">
					<div class="demo_top first_top">
						<em class="far fa-list-alt"></em><span>Sample</span>
					</div>
					<div id='layer_sample' class="ift_topbox">
						<p>Select input sentence <span style="color:#576068;font-weight:400;"> (The demo model used news data) </span></p>
						<ul class="ift_lst gpt_lst">
							<li>
								<div class="radio">
									<input type="radio" id="radio_sample_1" name="radio_sample" checked="checked" value="The social network's audience continues to grow too. It hit 2.41 billion monthly active users in the quarter, up from 2.38 billion in the quarter prior.">
									<label id='label_sample_1' for="radio_sample_1">The social network's audience continues to grow too. It hit 2.41 billion monthly active users in the quarter, up from 2.38 billion in the quarter prior.</label>
								</div>
							</li>
							<li>
								<div class="radio">
									<input type="radio" id="radio_sample_2" name="radio_sample" value="Wall Street is growing worried that Netflix's best days could be behind it.">
									<label id='label_sample_2' for="radio_sample_2">Wall Street is growing worried that Netflix's best days could be behind it.</label>
								</div>
							</li>
						</ul>
					</div>
					<p class="txt_or">Or</p>
					<div class="demo_top">
						<em class="far fa-keyboard"></em><span>Input text</span>
					</div>
					<div class="text_area">
						<textarea id='input_user' rows="7" placeholder="Input the text to generate relevant sentences." class="start_input"></textarea>
					</div>

					<div class="btn_area">
						<button type="button" class="btn_start" id="btn_gen">Process</button>
					</div>
				</div>
				<!--//.step01-->

				<!-- =================================================================================================================== -->
				<!-- 1차 진행중 -->
				<!-- =================================================================================================================== -->
				<!--step02-->
				<div id='layer_progress_1' class="step02">
					<div class="demo_top">
						<span>In progress</span>
					</div>
					<div class="loding_box ">
						<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#c9c3ec" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#27c1c1" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

						<p>AI processing takes a while..</p>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_back1" ><em class="fas fa-redo"></em>Reset</button>
					</div>

				</div>
				<!--//step02-->



				<!--step03-->
				<div id="house_result" class="step03">

					<!-- =================================================================================================================== -->
					<!-- 1차 결과 -->
					<!-- =================================================================================================================== -->
					<div id="layer_result_1" class="step03_1">
						<div class="demo_top">
							<span>Result</span>
						</div>
						<div class="txt_box">
							<p>Input: <span id='text_request_1' class="result_txt requestText">최저임금위원회는 전원회의를 개최해, 2020년 적용 최저임금 수준을 의결했다.</span></p>
							<div class="gpt_result">
								<span>Output sentences</span>
								<div id='text_result_1' class="result_txt resultText">20일 개막한 충남 논산 딸기 축제장에 인공지능(AI) 안내 로봇 '먹보 딸기'가 등장해 눈길을 끌고 있다.
									'말하는 인공지능 체험관'에서 만나볼 수 있는 먹보 딸기는 중학생 수준의 언어능력을 가지고 있다.
									관람객과 어린이들을 상대로 딸기 축제 홍보는 물론 관광지 안내, 일상적인 대화까지 척척 해낸다.
									또 어른들에게는 트로트, 아이들에게는 동요를 들려주며 웃음을 유발하는 재롱도 부려 사랑을 듬뿍 받고 있다.
									'상큼한 딸기향에 실려 오는 달콤한 만남'을 주제로 한 올해 논산 딸기 축제는 오는 24일까지 이어진다.</div>
							</div>
						</div>
					</div>

					<!-- =================================================================================================================== -->
					<!-- 2차 결과 -->
					<!-- =================================================================================================================== -->
					<!--문장가상 추가-->
					<div id="layer_result_2" class="step03_2">
						<div class="demo_top">
							<span>Result</span>
						</div>
						<div class="txt_box">
							<p>Input: <span id='text_request_2' class="result_txt requestText">최저임금위원회는 전원회의를 개최해, 2020년 적용 최저임금 수준을 의결했다.</span></p>
							<div class="gpt_result">
								<span>Output sentences</span>
								<div id='text_result_2' class="result_txt resultText">
									20일 개막한 충남 논산 딸기 축제장에 인공지능(AI) 안내 로봇 '먹보 딸기'가 등장해 눈길을 끌고 있다.
									'말하는 인공지능 체험관'에서 만나볼 수 있는 먹보 딸기는 중학생 수준의 언어능력을 가지고 있다.
									관람객과 어린이들을 상대로 딸기 축제 홍보는 물론 관광지 안내, 일상적인 대화까지 척척 해낸다.
									또 어른들에게는 트로트, 아이들에게는 동요를 들려주며 웃음을 유발하는 재롱도 부려 사랑을 듬뿍 받고 있다.
									'상큼한 딸기향에 실려 오는 달콤한 만남'을 주제로 한 올해 논산 딸기 축제는 오는 24일까지 이어진다.</div>
							</div>
						</div>
					</div>

					<!-- =================================================================================================================== -->
					<!-- 3차 결과 -->
					<!-- =================================================================================================================== -->
					<!--문장가상 추가-->
					<div id="layer_result_3" class='step03_3'>
						<div class="demo_top">
							<span>Result</span>
						</div>
						<div class="txt_box">
							<p>Input: <span id='text_request_3' class="result_txt requestText">최저임금위원회는 전원회의를 개최해, 2020년 적용 최저임금 수준을 의결했다.</span></p>
							<div class="gpt_result">
								<span>Output sentences</span>
								<div id='text_result_3' class="result_txt resultText">20일 개막한 충남 논산 딸기 축제장에 인공지능(AI) 안내 로봇 '먹보 딸기'가 등장해 눈길을 끌고 있다.
									'말하는 인공지능 체험관'에서 만나볼 수 있는 먹보 딸기는 중학생 수준의 언어능력을 가지고 있다.
									관람객과 어린이들을 상대로 딸기 축제 홍보는 물론 관광지 안내, 일상적인 대화까지 척척 해낸다.
									또 어른들에게는 트로트, 아이들에게는 동요를 들려주며 웃음을 유발하는 재롱도 부려 사랑을 듬뿍 받고 있다.
									'상큼한 딸기향에 실려 오는 달콤한 만남'을 주제로 한 올해 논산 딸기 축제는 오는 24일까지 이어진다.</div>
							</div>
						</div>
					</div>

					<!-- =================================================================================================================== -->
					<!-- 4차 결과 -->
					<!-- =================================================================================================================== -->
					<!--문장가상 추가-->
					<div id="layer_result_4" class="step03_4">
						<div class="demo_top">
							<span>Result</span>
						</div>
						<div class="txt_box">
							<p>Input: <span id='text_request_4' class="result_txt requestText">최저임금위원회는 전원회의를 개최해, 2020년 적용 최저임금 수준을 의결했다.</span></p>
							<div class="gpt_result">
								<span>Output sentences</span>
								<div id='text_result_4' class="result_txt resultText">20일 개막한 충남 논산 딸기 축제장에 인공지능(AI) 안내 로봇 '먹보 딸기'가 등장해 눈길을 끌고 있다.
									'말하는 인공지능 체험관'에서 만나볼 수 있는 먹보 딸기는 중학생 수준의 언어능력을 가지고 있다.
									관람객과 어린이들을 상대로 딸기 축제 홍보는 물론 관광지 안내, 일상적인 대화까지 척척 해낸다.
									또 어른들에게는 트로트, 아이들에게는 동요를 들려주며 웃음을 유발하는 재롱도 부려 사랑을 듬뿍 받고 있다.
									'상큼한 딸기향에 실려 오는 달콤한 만남'을 주제로 한 올해 논산 딸기 축제는 오는 24일까지 이어진다.</div>
							</div>
						</div>
					</div>

					<!-- =================================================================================================================== -->
					<!-- 추가 입력 -->
					<!-- =================================================================================================================== -->
					<div id="layer_add_input" class="add_area">
						<div class="demo_top">
							<em class="far fa-keyboard"></em>Input text
						</div>
						<div class="text_area">
							<textarea id='input_more' rows="7" placeholder="Add sentences for more generation."></textarea>
						</div>
					</div>

					<!-- =================================================================================================================== -->
					<!-- 추가 진행중 -->
					<!-- =================================================================================================================== -->
					<div id="layer_progress_2" class="add_lording">
						<div class="demo_top">
							<span>In progress</span>
						</div>
						<div class="loding_box " id="lording_temp">
							<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#c9c3ec" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#27c1c1" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

							<p>AI processing takes a while..</p>
						</div>
					</div>
					<!-- //add_loding-->


					<!-- =================================================================================================================== -->
					<!-- 버턴들 -->
					<!-- =================================================================================================================== -->
					<div id='layer_add_btn' class="btn_area">
						<button type="button" class="btn_back2" ><em class="fas fa-redo"></em>Reset</button>
						<button type="button" class="btn_add" id="button_add_input">Process <span>more</span></button>
					</div>

				</div>
				<!--//step03-->



			</div>
		</div>
		<!-- .gptdemo -->
		<!-- //.demobox -->

		<!--.gptmenu-->
		<div class="demobox" id="gptmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API Guideline
					</div>
					<p class="sub_title">Set up Environment</p>
					<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
					<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
					<p class="sub_title">ID &amp; Key</p>
					<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
					<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
					<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
						<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
					<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
					<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
					<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
				</div>
				<div class="guide_group">
					<div class="title">
						Generative Pre-Training <small>(GPT-2)</small>
					</div>
					<p class="sub_txt">Generates several sentences with similarity to the input string.</p>

					<span class="sub_title">
						 Preparation
					</span>
					<p class="sub_txt">① Input: String (text)</p>
					<p class="sub_txt">② Language: Choose one below</p>
					<ul>
						<li>Korean (kor)</li>
						<li>English (eng)</li>
					</ul>
					<span class="sub_title">
						API Document
					</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/gpt/</li>
					</ul>
					<p class="sub_txt">② Request parameters </p>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId</td>
							<td>Unique API ID. Request from is required for the ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>Unique API key. Request from is required for the key.</td>
							<td>string</td>
						</tr>
						<tr>
							<td>lang</td>
							<td>Selected language (kor / eng)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>context</td>
							<td>Input string (text)</td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request example </p>
					<div class="code_box">
<pre>
curl -X POST \
https: //api.maum.ai/api/gpt/ \
-H 'Content-Type: application/json' \
-d '{
    "apiId": "(*Request for ID)",
    "apiKey": "(*Request for key)",
    "context": "Tell me about the weather forecast.",
    "lang": "eng"
}'
</pre>
					</div>

					<p class="sub_txt">④ Response parameters</p>
					<span class="table_tit">Response</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>message</td>
							<td>Status of API request</td>
							<td>list</td>
						</tr>
						<tr>
							<td>result</td>
							<td>Generated sentences (Up to 3)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>reqText</td>
							<td>Input string</td>
							<td>string</td>
						</tr>
					</table>
					<span class="table_tit">message: Status of API request</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>message</td>
							<td>Status (Success/ Fail)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>status</td>
							<td>Status code (0: Success)</td>
							<td>number</td>
						</tr>
					</table>

					<p class="sub_txt">⑤ Response example </p>
					<div class="code_box">
<pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "result": "Best of luck - we should be able to request that.
	So far, it's been lovely. ‘ It is known for its beautiful view -
	but this mountain biking trail offering an up-close view of the snow-covered slopes
	is snow facing extinction.",
    "reqText": "Tell me about the weather forecast."
}
</pre>

					</div>
				</div>
			</div>
			<!--//.guide_box-->
		</div>
		<!--//gptmenu-->
		<!--.gptexample-->
		<div class="demobox" id="gptexample">
			<p><em style="font-weight: 400;color:#27c1c1;">Use Cases</em>  </p>
			<span class="sub">Find out how AI can be applied to diverse area.</span>
			<%--문장 생성(NLG)--%>
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>Generate Sentences for Deep-learning</span>
							</dt>
							<dd class="txt">Create sentences in bulk for AI deep-learning engines related to language intelligence.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_nlg"><span>GPT-2</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //문장 생성(NLG)-->
		</div>
	</div>
	<!-- //.content -->
</div>
<!-- //.contents -->


<script type="text/javascript">
	<%--jQuery.event.add(window,"load",function(){--%>
	<%--	$(document).ready(function() {--%>
	<%--		$('.step03').hide();--%>
	<%--		--%>
	<%--		$('.mrc_box .text_area textarea').on('input keyup paste', function() {--%>
	<%--			var txtValLth = $(this).val().length;--%>
	<%--			if ( txtValLth > 0) {--%>
	<%--				$('.progress li:nth-child(2)').addClass('active');--%>
	<%--				$('.mrc_box .btn_area button').removeClass('disable');--%>
	<%--				$('.mrc_box .btn_area button').removeAttr('disabled');--%>
	<%--				// $('.mrc_box .btn_area button').addClass('start_btn');--%>
	<%--			} else {--%>
	<%--				$('.progress li:nth-child(2)').removeClass('active');--%>
	<%--				// $('.mrc_box .btn_area button').addClass('disable');--%>
	<%--				$('.mrc_box .btn_area button').attr('disabled');--%>
	<%--				// $('.mrc_box .btn_area button').removeClass('start_btn');--%>
	<%--			}--%>
	<%--		});--%>
	<%--		--%>
	<%--		/* $('#classify_content').on('click', function () {--%>
	<%--			$(".step01").hide();--%>
	<%--			$('.step03').show();--%>
	<%--			//$(".step03").fadeIn(300);--%>
	<%--		}); */--%>
	<%--		--%>
	<%--		$('.btn_reset').on('click', function () {--%>
    <%--            $('#id_input_text').val("");--%>
    <%--            $(".result_txt").val("");--%>
	<%--			$('.step03').hide();--%>
	<%--			$('.mrc_wrap').show();--%>
	<%--			--%>
	<%--			$(".step01").show();--%>
	<%--		});--%>
	<%--		--%>
	<%--		var hideResult = true;--%>
	<%--		$("#classify_content").click(function(){--%>
	<%--			$("#loading_gif").show();--%>
	<%--							--%>
	<%--			if(hideResult) {--%>
	<%--				var txt = document.getElementById('id_input_text').value;--%>
	<%--				var language = $("input[type=radio][name=radio]:checked").val();--%>
	<%--				--%>
	<%--				if(txt == null || txt.length <= 0) {--%>
	<%--					alert("기본 문장을 입력해 주세요.");	--%>
	<%--					$("#loading_gif").hide();--%>
	<%--					return;--%>
	<%--				}--%>
	<%--				--%>
	<%--				$.ajax({--%>
	<%--					type: 'POST',--%>
	<%--					url: '/api/gpt',--%>
	<%--					data: {--%>
	<%--						"context": txt,--%>
	<%--						"apiId": "minds-api-performance-test",--%>
	<%--						"apiKey": "4SBRGrq9",--%>
	<%--						"language": language,--%>
	<%--						'${_csrf.parameterName}' : '${_csrf.token}'--%>
	<%--					},--%>
	<%--					error: function(xhr, status, error){--%>
	<%--						$(".step01").show();--%>
	<%--						$(".step03").hide();--%>
	<%--						$("#loading_gif").hide();--%>
	<%--						alert('[에러] 문단을 분석하는 데에 실패했습니다.');--%>
	<%--						console.dir(xhr.responseText);--%>
	<%--						console.dir(status);--%>
	<%--						console.dir(error);--%>
	<%--					},--%>
	<%--					success: function(data) {--%>

	<%--						$(".step01").hide();--%>
	<%--						$(".step03").show();--%>
	<%--						$(".step03").fadeIn();--%>
	<%--						--%>
	<%--						var responseData = JSON.parse(data);--%>
	<%--						var responseString = JSON.stringify(responseData);--%>
	<%--						--%>
	<%--						var status = responseData['message']['status'];--%>
	<%--						if(status == 0) {--%>
	<%--							// 성공--%>
	<%--							//결과 화면--%>
	<%--							var resultStr = responseData['result'];--%>
	<%--							$(".resultText").html(resultStr);--%>
	<%--							var reqText = responseData['reqText'];--%>
	<%--							$(".requestText").html(reqText);--%>
	<%--						}--%>
	<%--					}--%>
	<%--				}).done(function(data) {--%>
	<%--					$("#loading_gif").hide();--%>
	<%--					console.dir(data);--%>
	<%--				});--%>
	<%--			}--%>
	<%--		});--%>
	<%--	});--%>
	<%--});--%>

	var hAjax = null;
	var step = 0;

	/* 데이타 로딩 시, 페이지 초기화 */
	jQuery.event.add(window,"load",function(){
		$(document).ready(function() {
			init();
		});
	});

	function init() {
		setSample("eng");

	}


	/* ============================================================================================================== */
	// LANG
	/* ============================================================================================================== */

	$("input:radio[name=radio_lang]").click(function() {
		setSample($("input[type=radio][name=radio_lang]:checked").val());
	});


	/* ============================================================================================================== */
	// INPUT
	/* ============================================================================================================== */

	var g_input_text = null;

	/* 사용자 입력 포커스 */
	$('#input_user').focus(function() {
		g_input_text = $('#input_user').text();
	});
	$('#input_more').focus(function() {
		g_input_text = $('#input_more').text();
	});

	/* 사용자 입력 */
	$('#input_user').on('input keyup paste', function() {
		g_input_text = $(this).val();
	});
	$('#input_more').on('input keyup paste', function() {
		g_input_text = $(this).val();
	});


	//직접 입력할 때 예문 흐리게하기
	$('.start_input').on('click', function () {
		$('.step01 .first_top').css('opacity', '.5');
		$('.step01 .ift_topbox').css('opacity', '.5');
	});

	//다시 예문 클릭하면 선명하게 하기
	$('.ift_topbox').on('click', function () {
		$('.step01 .first_top').css('opacity', '1');
		$('.step01 .ift_topbox').css('opacity', '1');
	});



	/* ============================================================================================================== */
	// SAMPLE
	/* ============================================================================================================== */

	var eng_sample = [
		"Nearly half of the US population will see temperatures of at least 95 degrees over the next seven days, according to meteorologists.",
		"Wall Street is growing worried that Netflix's best days could be behind it."
	];
	var kor_sample = [
		"제헌절인 오늘 중부 내륙과 전북 내륙에는 낮 동안 소나기가 오는 곳이 있겠습니다.",
		"최저임금위원회는 전원회의를 개최해, 2020년 적용 최저임금 수준을 의결했다."
	];

	$('#layer_sample').on('click', function() {
		$('#input_user').val('');

		g_input_text = $("input[type=radio][name=radio_sample]:checked").val();
	});
	$('#layer_sample').click();

	function setSample(lang) {
		if(lang == "kor") {
			for(var xx = 0; xx < 2; xx ++) {
				$('#radio_sample_' + (xx+1)).val(kor_sample[xx]);
				$('#label_sample_' + (xx+1)).html(kor_sample[xx]);
			}
		}
		else {
			for(var xx = 0; xx < 2; xx ++) {
				$('#radio_sample_' + (xx+1)).val(eng_sample[xx]);
				$('#label_sample_' + (xx+1)).html(eng_sample[xx]);
			}
		}
	}


	/* ============================================================================================================== */
	// 돌아가기 버튼
	/* ============================================================================================================== */

	$(".btn_back1").on('click', function() {
		if(hAjax != null) hAjax.abort();
		hAjax = null;

		initUI();
	});

	$(".btn_back2").on('click', function() {
		if(hAjax != null) hAjax.abort();
		hAjax = null;

		initUI();
	});

	function initUI() {
		/* 예문, 사용자입력 레이어 숨기기 */
		$('.step01').fadeIn(300);

		/* 진행중 레이어 숨기기 */
		$('.step02').hide();

		/* 결과 레이어 숨기기 */
		for(var xx = 1; xx <= 4; xx++) {
			$('#layer_result_' + xx).hide();
		}
		$('.step03').hide();

		/* 추가 문장생성 버튼 보이기 */
		$(".btn_add").css('display','inline-block')

		$('#input_user').val('');
		$('#input_more').val('');
		g_input_text = '';

		$('#layer_sample').click();

		step = 0;
	}

	/* ============================================================================================================== */
	// 생성 버턴
	/* ============================================================================================================== */

	$("#btn_gen").click(genSentence);
	$(".btn_add").click(genSentence);

	function genSentence() {


		var txt = g_input_text;
		var lang = $("input[type=radio][name=radio_lang]:checked").val();

		console.log("text = " + txt);
		if(txt == null || txt.length <= 0) {
			alert("Select a sample sentence or type one.");
			return;
		}

		step++;

		if(step == 1) {
			$('.step01').hide();
			$('#layer_progress_1').fadeIn(300);
		}
		else {
			$('#layer_add_btn').hide();
			$('#layer_add_input').hide();
			$('#layer_progress_2').fadeIn(300);
		}

		hAjax = $.ajax({
			type: 'POST',
			url: '/api/gpt',
			data: {
				"context": txt,
				"apiId": "minds-api-performance-test",
				"apiKey": "4SBRGrq9",
				"lang": lang,
				'${_csrf.parameterName}' : '${_csrf.token}'
			},
			error: function(xhr, status, error){
				hAjax = null;

				if(error != 'abort') alert('Internal Server Error');

				console.dir(xhr.responseText);
				console.dir(status);
				console.dir(error);
			},
			success: function(data) {
				hAjax = null;
				var responseData = JSON.parse(data);
				var responseString = JSON.stringify(responseData);

				var status = responseData['message']['status'];
				if(status == 0) {
					$('#house_result').fadeIn(300);
					$('#layer_add_btn').fadeIn(300);
					$('#layer_add_input').fadeIn(300);


					if(step == 01) $('#layer_progress_1').hide();
					else $('#layer_progress_2').hide();

					$('#layer_result_' + step).fadeIn(300);

					// 성공

					//결과 화면
					var resultStr = responseData['result'];
					var reqText = responseData['reqText'];

					$("#text_request_" + step).html(reqText);
					$("#text_result_" + step).html(resultStr);

					$('#layer_progress_1').hide();
					$('#layer_progress_2').hide();

					$('#input_more').val('');
					g_input_text = '';

					if(step >= 3) {
						$(".btn_add").hide();
						$("#layer_add_input").hide();
					}
				}
			}
		}).done(function(data) {

			console.dir(data);
		});
	}



	//API 탭  	
	function openTap(evt, menu) {
	  var i, demobox, tablinks;
	  demobox = document.getElementsByClassName("demobox");
	  for (i = 0; i < demobox.length; i++) {
	    demobox[i].style.display = "none";
	  }
	  tablinks = document.getElementsByClassName("tablinks");
	  for (i = 0; i < tablinks.length; i++) {
	    tablinks[i].className = tablinks[i].className.replace(" active", "");
	  }
	  document.getElementById(menu).style.display = "block";
	  evt.currentTarget.className += " active";
	}	
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();	
</script>