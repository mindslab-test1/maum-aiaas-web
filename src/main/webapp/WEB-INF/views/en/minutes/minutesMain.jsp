<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-03-24
  Time: 16:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
    <div class="content">
        <h1>Cloud Meeting Notes System</h1>
        <div class="demobox data_demobox">
            <p><span>Cloud Meeting Notes System</span></p>
            <span class="sub">Our maum Minutes will type out and summarize your amazing ideas with a single recording of your meeting.</span>
            <div class="data_box minutes_box">
                <ul>
                    <li>
                        <h3>Live Recording</h3>
                        <p>Record live with the press of a button, and have an immediate log of meeting contents and results as soon as it ends.
                        </p>
                    </li>
                    <li>
                        <h3>Ability to Download</h3>
                        <p>Recorded voice files are stored in the cloud for download anywhere, anytime. The resulting conference log can be saved separately and distributed to all attendees.
                        </p>
                    </li>
                    <li>
                        <h3>Meeting Room Reservation Changes</h3>
                        <p>Manage your meeting room reservations at once. You can see, add  or delete the status of the conference room without any additional cost. In addition, through the reservation system
                            It provides convenience between users.</p>
                    </li>

                </ul>
                <a class="minutes_btn Button_Minutes">maum Minutes <em class="fas fa-angle-right"></em></a>
            </div>

        </div>

    </div>
    <!-- //.content -->
</div>
<!-- //.contents -->

<%-- 회의록 관련 스크립트 --%>
<script type="text/javascript">
    $('.Button_Minutes').click(function () {
        var newWindow = window.open('about:blank');
        newWindow.location.href = 'https://minutes.maum.ai/';
    });
</script>