<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">


<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>Upload error</h5>
            <p>Error occurred due to filename extension or file size. <br>Please check the restriction again.</p>
            <span> * Supported file: .png, .jpg, .bmp<br>
                * Image file size under 4MB.</span>
        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">OK</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->


    <!-- .contents -->
    <div class="contents">
        <div class="content api_content">
            <h1 class="api_tit">Anime Face</h1>
            <ul class="menu_lst vision_lst">
                <li class="tablinks active" onclick="openTap(event, 'afdemo')" id="defaultOpen">
                    <button type="button">AI Engine</button>
                </li>
                <li class="tablinks" onclick="openTap(event, 'afexample')">
                    <button type="button">Use Case</button>
                </li>
                <li class="tablinks" onclick="openTap(event, 'afmenu')">
                    <button type="button">Manual</button>
                </li>
            </ul>
            <!-- .demobox -->
            <div class="demobox" id="afdemo">
                <p><span>Anime Face</span></p>
                <span class="sub">Generate an animated version of your face, customize, style and decorate.</span>

                <!--animeFace_box-->
                <div class="demo_layout animeFace_box">
                    <!-- .avr_1 -->
                    <div class="avr_1">
                        <div class="fl_box">
                            <p><em class="far fa-file-image"></em><strong>Use Sample File</strong></p>
                            <div class="sample_box">
                                <div class="radio">
                                    <input type="radio" id="picture" name="sample_slt" value="sampleImage" checked>
                                    <label for="picture">
                                        <span class="label">Picture</span>
                                        <div class="img_area">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_animeFace_sample01.png" alt="sample picture">
                                        </div>
                                        <p>Try out the animation filter with this sample image.</p>
                                    </label>
                                </div>

                                <div class="radio">
                                    <input type="radio" id="animation" name="sample_slt" value="sampleAnimation">
                                    <label for="animation">
                                        <span class="label">Animation</span>
                                        <div class="img_area">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_animeFace_sample02.png" alt="sample animation">
                                        </div>
                                        <p>Style and decorate the randomly generated animation.</p>
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="fr_box">
                            <p><em class="far fa-file-image"></em><strong>Use My File</strong></p>
                            <div class="uplode_box">
                                <div class="btn" id="uploadFile">
                                    <em class="fas fa-times hidden close"></em>
                                    <em class="far fa-file-image hidden"></em>
                                    <label for="demoFile" class="demolabel">Upload File</label>
                                    <input type="file" id="demoFile" class="demoFile" accept=".jpg, .png, .bmp">
                                </div>
                                <ul>
                                    <li>* Supported Extensions: .png, .jpg, .bmp</li>
                                    <li>* We recommend the PNG format.</li>
                                    <li>* Only supports images under 4 MB.</li>
                                </ul>
                            </div>
                        </div>

                        <div class="btn_area">
                            <button type="button" class="btn_apply">Apply Filters<br> & Styles</button>
                        </div>
                    </div>
                    <!-- //.avr_1 -->

                    <!-- .avr_2 -->
                    <div class="avr_2">
                        <p><em class="far fa-file-image"></em>Applying filter</p>
                        <div class="loding_box ">
                            <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
                                 xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px"
                                 viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42"
                                                                                 d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"></path>
                                <g>
                                    <path fill="#f7778a" fill-opacity="1"
                                          d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"></path>
                                    <animateTransform attributeName="transform" type="translate"
                                                      values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0"
                                                      calcMode="discrete" dur="1820ms"
                                                      repeatCount="indefinite"></animateTransform>
                                </g></svg>

                            <p>AI processing takes a while...(about 2 minutes)</p>
                        </div>
                        <div class="btn_area">
                            <button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
                        </div>
                    </div>
                    <!-- //.avr_2 -->

                    <!-- .avr_3 -->
                    <div class="avr_3">
                        <p><em class="far fa-file-image"></em>Customize Results</p>
                        <div class="result_file">
                            <div class="effect_view">
                                <div class="imgBox">
                                    <img src="" id="resultImg" alt="result image">
                                    <div class="circle_loading">
                                        <div class="inner_circle">
                                            <div>&nbsp;</div>
                                            <div>&nbsp;</div>
                                            <div><div>&nbsp;</div></div>
                                            <div><div>&nbsp;</div></div>
                                        </div>
                                    </div>
                                </div>

                                <!-- [D] input file이 사진인 경우 보여줄 요소로 addClass('on')하면 보여짐 -->
                                <div class="effect_control on">
                                    <div class="range_slider">
                                        <label>Animation Effect</label>
                                        <!-- [D] 초기값 : 2 -->
                                        <input id="effectControlInput" class="slide_bar" type="range" value="2" min="0" max="5" step="1">
                                    </div>
                                    <ul class="range_value">
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                </div>

                                <!-- [D] input file이 애니메이션인 경우 보여줄 요소로 addClass('on')하면 보여짐 -->
                                <button type="button" class="btn_creat_newImg"><em class="fas fa-sync-alt"></em> Create
                                    a new image
                                </button>
                            </div>

                            <div class="style_adjust">
                                <div class="style_control">
                                    <div class="range_slider">
                                        <label>Style Controls</label>
                                        <!-- [D] 초기값 : 0 -->
                                        <input id="styleControlInput" class="slide_bar" type="range" value="1" min="1" max="5" step="1">
                                    </div>
                                    <ul class="range_value">
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                        <li></li>
                                    </ul>
                                </div>

                                <div class="style_slt">
                                    <div class="tit">Choose Style :</div>

                                    <div class="list_wrap">
                                        <ul class="btn_list">
                                            <li>
                                                <button type="button" class="btn_contrast active" value="portrait">Contrast</button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn_variation" value="mutate">Transform</button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn_individuality" value="psi_post">Variate</button>
                                            </li>
                                        </ul>

                                        <ul class="slt_list">
                                            <li>
                                                <select class="select">
                                                    <option value="" selected>Hair Style</option>
                                                    <option value="blunt_bangs">Straight Bangs</option>
                                                    <option value="long_hair">Longer Hair</option>
                                                </select>
                                            </li>
                                            <li>
                                                <select class="select">
                                                    <option value="" selected>Hair Color</option>
                                                    <option value="blonde_hair">Blonde</option>
                                                    <option value="black_hair">Black</option>
                                                    <option value="brown_hair">Brown</option>
                                                    <option value="red_hair">Reddish</option>
                                                    <option value="purple_hair">Purple</option>
                                                    <option value="pink_hair">Pink</option>
                                                    <option value="silver_hair">Silvery</option>
                                                </select>
                                            </li>
                                            <li>
                                                <select class="select">
                                                    <option value="" selected>Iris</option>
                                                    <option value="blue_eyes">Blue</option>
                                                    <option value="green_eyes">Green</option>
                                                </select>
                                            </li>
                                            <li>
                                                <select class="select">
                                                    <option value="" selected>Mouth</option>
                                                    <option value="smile">Smile</option>
                                                    <option value="closed_mouth">Closed</option>
                                                    <option value="open_mouth">Open</option>
                                                </select>
                                            </li>
                                            <li>
                                                <select class="select">
                                                    <option value="" selected>Accessorize</option>
                                                    <option value="bow">Accessory</option>
                                                    <option value="hair_ornament">Hair acc.</option>
                                                    <option value="hat">Hat</option>
                                                </select>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <div class="btnBox">
                                <a id="reset" class="btn_reset"><em class="fas fa-undo-alt"></em> Image Refresh</a>
                                <a id="save" onclick="downloadResultImg();" class="btn_dwn"><em
                                        class="far fa-arrow-alt-circle-down"></em> Download Result</a>
                            </div>
                        </div>

                        <div class="btn_area">
                            <button type="button" class="btn_back2"><em class="fas fa-redo"></em>Reset</button>
                        </div>
                    </div>
                    <!-- //.avr_3 -->
                </div>
                <!--//.animeFace_box-->
            </div>
            <!-- //.demobox -->

            <!--.afmenu-->
            <div class="demobox vision_menu" id="afmenu">
                <!--guide_box-->
                <div class="guide_box">
                    <div class="guide_common">
                        <div class="title">API Guideline</div>

                        <p class="sub_title">Set up Environment</p>
                        <p class="sub_txt">1) REST API is available wherever you can send HTTP requests.</p>
                        <p class="sub_txt">2) It can be used in Python, Web, Javascript and Java.</p>
                        <p class="sub_title">ID &amp; Key</p>
                        <p class="sub_txt">1) You need a unique ID and key for Minds Lab’s API services.</p>
                        <p class="sub_txt">2) Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform.
                            (https://maum.ai)</p>
                        <p class="sub_txt">3) After subscription, request an ID and key with required information filled
                            in.<br>
                            <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                        <p class="sub_txt">4) After an agreement, Minds Lab would send you an ID, key within 1-2
                            business days through email.</p>
                        <p class="sub_txt">5) Remember the ID and key and follow the instruction below.</p>
                        <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                    </div>

                    <div class="guide_group">
                        <div class="title">Anime Face</div>
                        <p class="sub_txt">Generate an animated version of your face, customize, style and decorate.</p>

                        <span class="sub_title">Preparation</span>
                        <p class="sub_txt">- Input: Image File</p>
                        <ul>
                            <li>File type : .png, .jpg, .bmp</li>
                            <li>Size : Under 4MB</li>
                        </ul>

                        <span class="sub_title">API Document</span>
                        <em>Sample Make : Generates a random Anime Face.</em>

                        <!-- Upload START-->
                        <p class="sub_txt">① Request</p>
                        <ul>
                            <li>Method : POST</li>
                            <li>URL : https://api.maum.ai/anime/sample:make</li>
                        </ul>
                        <p class="sub_txt">② Request parameters</p>
                        <table>
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>apiId</td>
                                <td>Unique API ID. Request from is required for the ID.</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>apiKey</td>
                                <td>Unique API key. Request from is required for the key.</td>
                                <td>string</td>
                            </tr>
                            </tbody>
                        </table>

                        <p class="sub_txt">③ Request example</p>
                        <div class="code_box">
<pre>
curl -X POST 'https://api.maum.ai/anime/sample:make' \
-H 'Content-Type: application/json' \
-d '{
    "apiId" : (Own API Id),
    "apiKey" : (Own API Key)
}'
</pre>
                        </div>

                        <p class="sub_txt">④ Response parameters</p>
                        <span class="table_tit">Response</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>message</td>
                                <td>API operation</td>
                                <td>Object</td>
                            </tr>
                            <tr>
                                <td>payload</td>
                                <td>result</td>
                                <td>Object</td>
                            </tr>
                            </tbody>
                        </table>

                        <span class="table_tit">message: API operation</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>message</td>
                                <td>String describing the request processing status (Success/Fail)</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>status</td>
                                <td>Status code for request processing status (0: Success)</td>
                                <td>int</td>
                            </tr>
                            </tbody>
                        </table>

                        <span class="table_tit">payload: result</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>img</td>
                                <td>AnimeFace Image Byte Array</td>
                                <td>byte</td>
                            </tr>
                            <tr>
                                <td>latent</td>
                                <td>AnimeFace .pkl File Byte Array</td>
                                <td>byte</td>
                            </tr>
                            </tbody>
                        </table>

                        <p class="sub_txt">⑤ Response example</p>
                        <div class="code_box">
<pre>
{
	"message": {
		"message": "Success",
		"status": 0
	},
	"payload": {
		"img": "byte[]",
		"latent": "byte[]"
	}
}			
</pre>
                        </div>

                        <em>Face Make : When a Human Face Image is input as an input, an Anime Face Image with a similar
                            appearance is created.</em>
                        <p class="sub_txt">① Request</p>
                        <ul>
                            <li>Method : POST</li>
                            <li>URL : https://api.maum.ai/anime/face:make</li>
                        </ul>

                        <p class="sub_txt">② Request parameters</p>
                        <table>
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>apiId</td>
                                <td>Unique API ID. Request from is required for the ID.</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>apiKey</td>
                                <td>Unique API key. Request from is required for the key.</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>image</td>
                                <td>type:file (.png, .jpg, .bmp) image file</td>
                                <td>file</td>
                            </tr>
                            <tr>
                                <td>steps</td>
                                <td>Number of steps to use to find similar images(not required, default = 500)</td>
                                <td>int</td>
                            </tr>
                            </tbody>
                        </table>

                        <p class="sub_txt">③ Request example</p>
                        <div class="code_box">
<pre>
curl -X POST 'https://api.maum.ai/anime/face:make' \
-H 'Content-Type: multipart/form-data' \
-F 'apiId= Own API Id' \
-F 'apiKey= Own API Key' \
-F 'image= @image file' \
-F 'steps= default= 500' \
</pre>
                        </div>

                        <p class="sub_txt">④ Response parameters</p>
                        <span class="table_tit">Response</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>message</td>
                                <td>API operation</td>
                                <td>Object</td>
                            </tr>
                            <tr>
                                <td>payload</td>
                                <td>result</td>
                                <td>Object</td>
                            </tr>
                            </tbody>
                        </table>

                        <span class="table_tit">message: API operation</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>message</td>
                                <td>String describing the request processing status (Success/Fail)</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>status</td>
                                <td>Status code for request processing status (0: Success)</td>
                                <td>int</td>
                            </tr>
                            </tbody>
                        </table>

                        <span class="table_tit">payload: result</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>img</td>
                                <td>AnimeFace Image Byte Array</td>
                                <td>byte</td>
                            </tr>
                            <tr>
                                <td>latent</td>
                                <td>AnimeFace .pkl File Byte Array</td>
                                <td>byte</td>
                            </tr>
                            </tbody>
                        </table>

                        <p class="sub_txt">⑤ Response example</p>
                        <div class="code_box">
<pre>
{
	"message": {
		"message": "Success",
		"status": 0
	},
	"payload": {
		"img": "byte[]",
		"latent": "byte[]"
	}
}				
</pre>
                        </div>

                        <em>Style Edit : Creates an image that adjusts the strength of the Anime Face Image and the set
                            style key values (open_mouth, blonde_hair, ...).</em>
                        <p class="sub_txt">① Request</p>
                        <ul>
                            <li>Method : POST</li>
                            <li>URL : https://api.maum.ai/anime/style:edit</li>
                        </ul>

                        <p class="sub_txt">② Request parameters</p>
                        <table>
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>apiId</td>
                                <td>Unique API ID. Request from is required for the ID.</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>apiKey</td>
                                <td>Unique API key. Request from is required for the key.</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>latent</td>
                                <td>Anime Face .pkl File</td>
                                <td>file</td>
                            </tr>
                            <tr>
                                <td>dict</td>
                                <td>.txt file for style editing</td>
                                <td>file</td>
                            </tr>
                            </tbody>
                        </table>

                        <p class="sub_txt">③ Request example</p>
                        <div class="code_box">
<pre>
curl -X POST 'https://api.maum.ai/anime/style:edit' \
-H 'Content-Type: multipart/form-data' \
-F 'apiId= Own API Id' \
-F 'apiKey= Own API Key' \
-F 'latent= @pkl file' \
-F 'dict= @txt file' \
</pre>
                        </div>

                        <p class="sub_txt">④ Response parameters</p>
                        <span class="table_tit">Response</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>message</td>
                                <td>API operation</td>
                                <td>Object</td>
                            </tr>
                            <tr>
                                <td>payload</td>
                                <td>result</td>
                                <td>Object</td>
                            </tr>
                            </tbody>
                        </table>

                        <span class="table_tit">message: API operation</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>message</td>
                                <td>String describing the request processing status (Success/Fail)</td>
                                <td>string</td>
                            </tr>
                            <tr>
                                <td>status</td>
                                <td>Status code for request processing status (0: Success)</td>
                                <td>int</td>
                            </tr>
                            </tbody>
                        </table>

                        <span class="table_tit">payload: result</span>
                        <table>
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <th>Description</th>
                                <th>type</th>
                            </tr>
                            <tr>
                                <td>img</td>
                                <td>AnimeFace Image Byte Array</td>
                                <td>byte</td>
                            </tr>
                            <tr>
                                <td>latent</td>
                                <td>AnimeFace .pkl File Byte Array</td>
                                <td>byte</td>
                            </tr>
                            </tbody>
                        </table>

                        <p class="sub_txt">⑤ Response example</p>
                        <div class="code_box">
<pre>
{
	"message": {
		"message": "Success",
		"status": 0
	},
	"payload": {
		"img": "byte[]",
		"latent": "byte[]"
	}
}				
</pre>
                        </div>
                    </div>
                </div>
                <!--//.guide_box-->
            </div>
            <!--.afmenu-->

            <!--.afexample-->
            <div class="demobox" id="afexample">
                <p><em style="color:#f7778a;font-weight: 400;">Use Cases</em></p>
                <span class="sub">Find out how AI can be applied to diverse areas.</span>
                <div class="useCasesBox">
                    <!-- 얼굴 애니메이션 필터-->
                    <ul class="lst_useCases">
                        <li>
                            <dl>
                                <dt>
                                    <em>CASE 01</em>
                                    <span>Character Contents</span>
                                </dt>
                                <dd class="txt">The animated characters can be used in movies and games.</dd>
                                <dd class="api_itemBox">
                                    <ul class="lst_api">
                                        <li class="ico_anmFc"><span>Anime Face</span></li>
                                    </ul>
                                </dd>
                            </dl>
                        </li>
                        <li>
                            <dl>
                                <dt>
                                    <em>CASE 02</em>
                                    <span>Creators & Youtubers</span>
                                </dt>
                                <dd class="txt">The animated characters can be used in youtube or video-blogs.</dd>
                                <dd class="api_itemBox">
                                    <ul class="lst_api">
                                        <li class="ico_anmFc"><span>Anime Face</span></li>
                                    </ul>
                                </dd>
                            </dl>
                        </li>
                    </ul>
                    <!--얼굴 애니메이션 필터-->
                </div>
            </div>
            <!--//.afexample-->
        </div>
    </div>
    <!-- //.contents -->

<!---------------------- Local Resources ---------------------->
<!-- Local Script -->
<script type="text/javascript">

    let request = null;
    let sampleImg = null;

    let initialResultImg = null;
    let initialPklFile = null;

    let inputImg = null;
    let resultImg = null;
    let pklFile = null;
    let dictObj = {"psi_post" : 1};
    let dictFile = null;
    let selectedStyle = "portrait"; // 초기값 "대비"

    const effectValue = {
        0 : 100,
        1 : 300,
        2 : 500,
        3 : 1000,
        4 : 1500,
        5 : 2000
    }
    const styleValue = {
        1 : 0, // psi_post 1
        2 : 5,
        3 : 10,
        4 : 15,
        5 : 20
    }


    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {

            loadSampleImg();

            // 파일 업로드 기능
            document.querySelector("#demoFile").addEventListener('change', function (ev) {
                let demoFileInput = document.getElementById('demoFile');
                let demoFile = demoFileInput.files[0];
                let demoFileSize = demoFile.size;
                let max_demoFileSize = 1024 * 1024 * 4; //1kb는 1024바이트
                //파일 용량, 확장자 체크
                if(demoFileSize > max_demoFileSize || (!demoFile.type.match(/image.png/) && !demoFile.type.match(/image.bmp/) && !demoFile.type.match(/image.jp*/))){
                    $('.pop_simple').show();
                    $('#demoFile').val('');
                } else {
                    $('.demolabel').html(demoFile.name);
                    $('#uploadFile').removeClass('btn');
                    $('#uploadFile').addClass('btn_change');
                    $('.fl_box').css("opacity", "0.5");
                    $('#sample1').prop('checked', false);
                }
            });

            $('em.close').on('click', function () {
                $('.fl_box').css('opacity', '1');
                $('#uploadFile label').text('Upload File');
                $('#uploadFile').removeClass("btn_change");
                $('#uploadFile').addClass("btn");
                $('#demoFile').val("");
            });


            // 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({ 'overflow': '', });
            });

            // [D] 결과화면의 style 선택 영역의 button과 select 선택 시 효과
            $('.btn_list li button, .slt_list li .select').on('click', function () {
                $('.btn_list li button, .slt_list li .select').removeClass('active');
                $(this).addClass('active');
                selectedStyle = $(this).val();
            });

            $('.btn_apply').on('click', function () {

                let selectVal = $('input[type=radio][name=sample_slt]:checked').val();

                if($('#demoFile').val() !== "") {    // 사용자 이미지 업로드
                    inputImg = $('#demoFile').get(0).files[0];
                    submitImg();

                } else if(selectVal === 'sampleAnimation') { // 랜덤 애니메이션
                    submitRandomAnimationImg();

                } else if(selectVal === 'sampleImage') { // 샘플 이미지 선택
                    inputImg = sampleImg;
                    submitImg();
                }

                $('.avr_1').hide();
                $('.avr_2').show();
            });

            $('.btn_back1').on('click', function () {
                if (request) { request.abort(); }
                $('.avr_2').hide();
                $('.avr_1').show();
            });

            $('.btn_back2').on('click', function () {
                if (request) { request.abort(); }

                initialResultImg = null;
                initialPklFile = null;

                inputImg = null;
                pklFile = null;
                dictFile = null;
                dictObj = {"psi_post" : 1};
                selectedStyle = "";

                $('.slt_list select option:eq(0)').attr('selected', 'selected');
                $('.slt_list select').val("");

                $('#styleControlInput').val(1);
                $('#effectControlInput').val(2);
                $('.btn_list li button, .slt_list li .select').removeClass('active');
                $('.btn_contrast').click();

                applyFill01(sliders01.querySelector('input'));
                applyFill02(sliders02.querySelector('input'));

                $('.avr_3').hide();
                $('.avr_1').show();

            });

            // 이미지 새로고침 -- 처음 데이터로 변경
            $('#reset').on('click', function(){
                $('#resultImg').attr("src", "data:image/jpeg;base64," + initialResultImg);
                pklFile = initialPklFile;
                dictObj = {"psi_post": 1};
            });

            $('.btn_creat_newImg').on('click', function () {
                $('.imgBox').addClass('loading');
                $('.result_file').addClass('disabled');
                submitRandomAnimationImg();
            });


            // face:make
            $('#effectControlInput').on('change', function(){
                $('.imgBox').addClass('loading');
                $('.result_file').addClass('disabled');
                submitImg();
            });

            // style:edit
            $('#styleControlInput').on('change', function(){
                if(selectedStyle !== ""){

                    if(selectedStyle === "psi_post" && styleValue[$(this).val()] === 0){
                        dictObj[selectedStyle] = 1;
                    }else{
                        dictObj[selectedStyle] = styleValue[$(this).val()];
                    }
                    console.log(JSON.stringify(dictObj));

                    dictFile = createDictTextFile(JSON.stringify(dictObj));

                    $('.imgBox').addClass('loading');
                    $('.result_file').addClass('disabled');

                    submitStyleEdit();
                }
            });


        });
    });



    // 랜덤 애니메이션 이미지
    function submitRandomAnimationImg() {

        let formData = new FormData();
        formData.append($("#key").val(), $("#value").val());

        request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status !== 0) {

                if(request.response && request.response !== ""){
                    let json = JSON.parse(request.response);
                    let img = json.payload.img;
                    let latent = json.payload.latent;

                    initialResultImg = img;
                    pklFile = createLatentPklFile(latent);
                    initialPklFile = pklFile;

                    $('.imgBox').removeClass('loading');
                    $('.result_file').removeClass('disabled');

                    $('#resultImg').attr("src", "data:image/jpeg;base64," + img);
                    $('.avr_2').hide();
                    $('.avr_3').show();

                    $('.effect_control').removeClass('on');
                    $('.btn_creat_newImg').addClass('on');
                } else{
                    console.log("status code : " + request.status + "\nstatus text : " + request.responseText);
                    alert("Failed to get response from server.");
                }

            }
        };

        request.open('POST', '/api/animeFace/sample:make');
        request.send(formData);
        request.addEventListener("abort", function () {});

    }

    // 샘플 이미지, 사용자 업로드 이미지
    function submitImg(){

        let formData = new FormData();
        formData.append($("#key").val(), $("#value").val());
        formData.append('file', inputImg);
        formData.append('steps', effectValue[$('#effectControlInput').val()]);

        request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status !== 0){

                if(request.response && request.response !== ""){
                    let json = JSON.parse(request.response);
                    let img = json.payload.img;
                    let latent = json.payload.latent;

                    console.log(json.message);

                    if(!initialResultImg) { initialResultImg = img; }
                    if(!initialPklFile){ initialPklFile = pklFile; }
                    pklFile = createLatentPklFile(latent);

                    $('.imgBox').removeClass('loading');
                    $('.result_file').removeClass('disabled');
                    $('.effect_control').addClass('on');
                    $('.btn_creat_newImg').removeClass('on');

                    $('#resultImg').attr("src", "data:image/jpeg;base64," + img);
                    $('.avr_2').hide();
                    $('.avr_3').show();

                } else{
                    console.log("status code : " + request.status + "\nstatus text : " + request.responseText);
                    alert("Failed to get response from server.");
                }

            }
        };

        request.open('POST', '/api/animeFace/face:make');
        request.send(formData);
        request.addEventListener("abort", function(){ });

    }


    function submitStyleEdit() {
        let formData = new FormData;

        formData.append($("#key").val(), $("#value").val());
        formData.append("latent", pklFile);
        formData.append("dict", dictFile);

        request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState === 4 && request.status !== 0){

                if(request.response || request.response !== ""){
                    console.log(JSON.parse(request.response).message);

                    let json = JSON.parse(request.response);
                    let img = json.payload.img;
                    let latent = json.payload.latent;

                    pklFile = createLatentPklFile(latent);

                    $('.imgBox').removeClass('loading');
                    $('.result_file').removeClass('disabled');
                    $('#resultImg').attr("src", "data:image/jpeg;base64," + img);
                }else{
                    console.log("status code : " + request.status + "\nstatus text : " + request.responseText);
                    alert("Failed to get response from server.");
                }

            }
        };

        request.open('POST', '/api/animeFace/style:edit');
        request.send(formData);
        request.addEventListener("abort", function(){ });
    }

    // create latent pickle file (.pkl)
    function createLatentPklFile(latent) {
        let filename = Date.now() + Math.random().toString(36).substring(2, 15) + '.pkl';
        let contentType = 'application/octet-stream';
        let blob = b64toBlob(latent, contentType);

        return new File([blob], filename);
    }

    // create dict text file (.txt)
    function createDictTextFile(text) {
        let filename = Date.now() + Math.random().toString(36).substring(2, 15) + '.txt';
        return new File([text], filename, {type: 'text/plain'});
    }

    function b64toBlob(b64Data, contentType, sliceSize) {
        contentType = contentType || '';
        sliceSize = sliceSize || 512;

        let byteCharacters = atob(b64Data);
        let byteArrays = [];

        for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            let slice = byteCharacters.slice(offset, offset + sliceSize);

            let byteNumbers = new Array(slice.length);
            for (let i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            let byteArray = new Uint8Array(byteNumbers);

            byteArrays.push(byteArray);
        }

        return new Blob(byteArrays, {type: contentType});
    }

    // 이미지 다운로드
    function downloadResultImg(){
        let img = document.getElementById('resultImg');
        let link = document.getElementById("save");
        link.href = img.src.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
        link.download = "AnimeFaceResult.png";
    }


    // ------------------------ slide bar
    var settings = {
        fill: '#2c3f51',
        background: '#cfd5eb'
    }

    const sliders01 = document.querySelector('.effect_control .range_slider');
    const sliders02 = document.querySelector('.style_control .range_slider');


    sliders01.querySelector('input').addEventListener('input', (event) => {
        applyFill01(event.target);
    });
    applyFill01(sliders01.querySelector('input'));

    function applyFill01(sliders01) {
        const percentage = 100 * (sliders01.value - sliders01.min) / (sliders01.max - sliders01.min);
        const bg = `linear-gradient(90deg, \${settings.fill} \${percentage}%, \${settings.background} \${percentage+0.1}%)`;
        sliders01.style.background = bg;
    }

    sliders02.querySelector('input').addEventListener('input', (event) => {
        applyFill02(event.target);
    });
    applyFill02(sliders02.querySelector('input'));

    function applyFill02(sliders02) {
        const percentage = 100 * (sliders02.value - sliders02.min) / (sliders02.max - sliders02.min);
        const bg = `linear-gradient(90deg, rgba(247, 119, 138) \${percentage}%, \${settings.background} \${percentage+0.1}%)`;
        sliders02.style.background = bg;
    }
    // ------------------------ //slide bar


    // 샘플 이미지 준비
    function loadSampleImg() {
        let blob = null;
        let xhr = new XMLHttpRequest();
        xhr.open("GET", "/aiaas/common/images/img_animeFace_sample01.png");
        xhr.responseType = "blob";
        xhr.onload = function()
        {
            blob = xhr.response;
            sampleImg = new File([blob], "img_animeFace_sample01.png");
        };

        xhr.send();
    }

    // API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace("active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    document.getElementById("defaultOpen").click();
</script>
