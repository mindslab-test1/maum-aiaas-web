<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<script type="text/javascript">

	//var userno= "${fn:escapeXml(sessionScope.accessUser.userno)}";

	$(document).ready(function(){

		getApiAccountList();
	});

	/** 이용내역 - 목록 조회  */
	function getApiAccountList(currentPageNo){

		if(currentPageNo === undefined){
			currentPageNo = "1";
		}

		$("#current_page_no").val(currentPageNo);

		$.ajax({
			url		:"/member/getApiAccountList",
			data    : $("#apiForm").serialize(),
			dataType:"JSON",
			cache   : false,
			async   : true,
			type	:"POST",
			success : function(obj) {
				$("#apiAccountList").html($("#apiList").render(obj));
				// $("#pagination").html(obj.data.pagination);

			},
			error 	: function(xhr, status, error) {}

		});
	}
	
	function getUseLog(serviceName){
		$.ajax({
			url		:"/member/getUseLog",
			data    : {
				// "userno" : userno,
				"serviceName" : serviceName,
				"${_csrf.parameterName}" : "${_csrf.token}"
			},
			dataType:"JSON",
			type	:"POST",
			success : function(obj) {
				$("#useDetail").html($("#listTemplate").render(obj));
				// $("#pagination").html(obj.data.pagination);
			},
			error 	: function(xhr, status, error) {}

		});
	}
	
	/** id,key 발급 요청 */
	function sendMailApi(){
		var regEmail = /([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
		
		var name = $("#fm_name").val();
		var mailAddr = $("#fm_email").val();
		var company = $("#fm_company").val();
		var content = $("#fm_txt").val();
		var userEmail = "${fn:escapeXml(sessionScope.accessUser.email)}"
		
		if(content == ""){
			alert("Please enter a blank");
			$("#fm_txt").focus();
			return;
		}
		if(name == ""){
			alert("Please enter a blank");
			$("#fm_name").focus();
			return;
		}
		if(company == ""){
			alert("Please enter a blank");
			$("#fm_company").focus();
			return;
		}
		/*if(mailAddr == ""){
			alert("Please enter a blank");
			$("#fm_email").focus();
			return;
		}	*/

		var yn = confirm("\n" +	"Would you like to request of API ID, Key?");
		if(yn){
			$.ajax({
				type :'post',
				url : '/support/createApiKeyId',
				data:{
					'${_csrf.parameterName}' : '${_csrf.token}'
				},
				cache	: false,
				async	: true,
				type	: "POST",
				success	: function (obj) {
					$.ajax({
						type 	: 'post',
						url  	: '/support/insertSupportIdKey',
						data: {
							'name' : $("#fm_name").val(),
							'company' : $("#fm_company").val(),
							'mailAddr' : $("#fm_email").val(),
							'content' : $("#fm_txt").val(),
							'userEmail' : userEmail,
							'${_csrf.parameterName}' : '${_csrf.token}'
						},
						dataType: "JSON",
						cache   : false,
						async   : true,
						type	: "POST",
						success : function(obj) {},
						error 	: function(xhr, status, error) {}
					});

					alert("apiKey and Id have been issued successfully.")
					window.location.reload()
				},
				error	: function(xhr, status, error){
					alert("ID, Key issuance request failed.");
					window.location.reload()
				}
			});
		}				
	}
	
	function sendMailApiCallback(obj){
		
		if(obj != null){		
			
			var state = obj.state;
			
			if(state == "SUCCESS"){			
				//alert("ID,Key 발급 요청하였습니다.");
				$('.lyr_idKey').fadeOut(300);
				$('.idKey_done').fadeIn(300);	
			} else {				
				alert("ID,Key 발급 요청실패하였습니다.");	
				return;
			}
		}
	}		
	
</script>

<script id="listTemplate" type="text/x-jsrender">
	{{for data.list}}
		<li><span>{{>kii}}</span><span>{{>date}}</span></li>
	{{/for}}
</script>

<script type="text/javascript">
$(window).load(function() {
	$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
		$(this).remove();
	});
});
</script>
<script type="text/javascript">
jQuery.event.add(window,"load",function(){

	function detailView(){
		$('.detail_view').on('click', function(){
			let serviceName = $(this).data("service");
			console.log(serviceName);
			getUseLog(serviceName);
			$('.lyr_detail_amount').fadeIn(300);
			});

		$('.idKey_btn').on('click', function(){
			$('.lyr_idKey').fadeIn(300);
		})
		$('.btn_lyrWrap_close').on('click', function(){
			$('.lyr_idKey').fadeOut(300);
		})
		$('.btn_lyrWrap_close, .lyr_detail_bg, #completBtn').on('click', function () {
			$('.lyr_detail_amount').fadeOut(300);
			$('.lyr_idKey').fadeOut(300);
			$('.idKey_done').fadeOut(300);
		});		
	};

	setTimeout(detailView, 500);
});


</script>

<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">

	<div class="loading_itemBox">
		<span></span>
		<span></span>
		<span></span>
		<span></span>
	</div>
</div>
<!-- //.page loading -->

<!-- .lyr_idKey -->
<div class="lyr_idKey">
	<div class="lyr_plan_bg"></div>
	<!-- .productWrap -->
	<div class="lyrWrap">
		<button class="btn_lyrWrap_close" type="button">Close</button>
		<!-- .lyr_bd -->
		<div class="lyr_bd">
			<form id="apimailForm" name="apimailForm">			
				<em class="far fa-edit"></em>
				<p>ID, Key Request</p>
				<span class="desc">* The information you enter will be used for efficient service configuration and improvement.</span>
				<ul>
					<li>
						<span>Type of service</span>
						<input type="text" name="fm_txt" id="fm_txt" placeholder="e.g., Create a chatbot, News analysis">
					</li>
					<li>
						<span>API to use</span>
						<input type="text" name="fm_name" id="fm_name" placeholder="e.g., STT, TTS, MRC">
					</li>
					<li>
						<span>Company to use</span>
						<input type="text" name="fm_company" id="fm_company" placeholder="e.g., MindsLab, MaumConnect">
					</li>
<%--					<li>
						<span class="input_email">ID, Key<br>Receive email</span>
						<input type="text" name="fm_email" id="fm_email" placeholder="e.g., myemail@mindslab.ai">
					</li>--%>
				</ul>
				<!-- <button class="btn_blue" onclick="sendMailApi();">신청하기</button> -->
	 			
				<a class="btn_blue" onclick="sendMailApi();">Request</a>
					
			</form>
		</div>
		<!-- //.lyr_bd -->

	</div>
	<!-- //.productWrap -->
</div>
<!-- //.lyr_idKey -->

<!-- .idKey_done -->
<div class="lyr_plan idKey_done">
	<div class="lyr_plan_bg"></div>
	<!-- .lyrWrap -->
	<div class="lyrWrap">
		<button class="btn_lyrWrap_close" type="button">Close</button>
		<!-- .lyr_bd -->
		<div class="lyr_bd">
			<em class="fas fa-check"></em>
			<p>Completed!</p>
			<span>ID and Key will be sent to your email<br> within 1-2 business days. </span>
			<button class="btn_blue" id="completBtn">OK</button>
		</div>
		<!-- //.lyr_bd -->
	</div>
	<!-- //.lyrWrap -->
</div>
<!-- //.idKey_done -->

<!-- .lyr_detail_amount -->

<div class="lyr_pw lyr_detail_amount">
	<div class="lyr_detail_bg"></div>
	<!-- .productWrap -->
	<div class="lyrWrap">
		<button class="btn_lyrWrap_close" type="button">Close</button>

		<!-- .lyr_bd -->
		<div class="lyr_bd">
			<em class="fas fa-poll"></em>
			<h3>Service Usage</h3>
			<p>Amount </p>
			<p>Date</p>
			<ul class="amountlst" id="useDetail">
<%--				<li><span>23분 30초</span><span>2019.04.28</span></li>--%>
			</ul>
		</div>
		<!-- //.lyr_bd -->

	</div>
	<!-- //.productWrap -->
</div>
<!-- //.lyr_detail_amount -->

<!-- .contents -->
		<div class="contents">
			<div class="content">
				<h1>API Account</h1>
				<!--.demobox_nlu-->
				<div class="demobox">
					<div class="account">
						<p>API for Developers</p>
						<div class="stn_1">
							<dl>
								<dt>ID</dt>
								<dd>${apiId} </dd>
<%--								<dd>${fn:escapeXml(sessionScope.accessUser.email)}</dd>--%>
							</dl>
							<dl>
								<dt>Key</dt>
								<dd>${apikey} </dd>
							</dl>
<!--
							<button class="requestbtn">Request</button>
							<span>Business 이상 구독 시, ID/Key 발급이 가능합니다.<br>Business Upgrade 원하시는 경우, 눌러주세요.</span>
-->
							<%--<button type="button" class="idKey_btn">API ID/Key Request</button>
							<p>* Get a unique API ID/Key from maum.ai and use the service freely!!</p>--%>
							<c:if test="${apikey == '' || apikey == null}">
								<button type="button" class="idKey_btn">API ID/Key Request</button>
								<p>* Get a unique API ID/Key from maum.ai and use the service freely!!</p>
							</c:if>
						</div>

						<form id="apiForm" name="apiForm">
							<input type="hidden" id="function_name" name="function_name" value="getApiAccountList" />
							<input type="hidden" id="current_page_no" name="current_page_no" value="1" />
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
							<div class="stn_2">
								<p>History</p>
								<table>
									<colgroup>
										<col width="10%"><col width="240px"><col width="20%"><col width="20%"><col width="20%">
									</colgroup>
									<thead>
										<tr class="thead">
											<th scope="col">No.</th>
											<th scope="col">A Name of Service</th>
											<th scope="col">Used / Purchased</th>
											<th scope="col">Period</th>
											<th scope="col">Remarks</th>
										</tr>
									</thead>
									<tbody id="apiAccountList">
<%--								자 = char / 별도사용량제공 = Additional / 분 = min--%>
						<%--			<script id="apiList" type="text/x-jsrender">
										{{for data.apiList}}
											<tr>
												<td scope="row">{{>no}}</td>
												<td>{{>service}}</td>
												<td>{{>alimit}}<span title="Detail" class="detail_view" data-service="{{>service_Id}}"><em class="fas fa-search"></em></span></td>
												<td>{{>date}}</td>
												<td>{{>etc}}</td>
											</tr>

										{{/for}}
									</script>--%>

									</tbody>
								</table>
							<!-- 페이징 -->
<%--							<div class="pageing" id="pagination">--%>
							</div>
							<!-- //페이징 -->
						</form>
					</div>
				</div>
				<!--//account-->
			</div>
		</div>
		<!-- //.contents -->







