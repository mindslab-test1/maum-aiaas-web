<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ page import="console.maum.ai.member.constant.UserStatus"%>

<!-- .lyr_plan -->
<div class="lyr_plan" id="changePlan">
	<div class="lyr_plan_bg"></div>
	<!-- .productWrap -->
	<div class="lyrWrap">				
		<button class="btn_lyrWrap_close" type="button">Close</button>
		<!-- .lyr_bd -->
		<div class="lyr_bd">
			<em class="fas fa-briefcase"></em>
			<p>플랜 변경</p>
			<div class="planstep">
				<span class="txt_fl">플랜 선택</span>
				<div class="selectarea">
					<div class="radio">
						<input type="radio" id="plan_basic" name="plan" value="BASIC">
						<label for="plan_basic"><span>BASIC</span></label>
					</div>
					<div class="radio">
						<input type="radio" id="plan_business" name="plan" value="BUSINESS">
						<label for="plan_business"><span>BUSINESS</span></label>
					</div>
				</div>
			</div>
			<div class="planstep" style="padding: 10px 0px; margin-bottom: 20px;">
				<div class="radio" style="margin-left: 45px; width: 200px;">
					<input type="radio" id="radio_timing_now" name="timing" checked="checked">
					<label for="radio_timing_now"><span>지금 바로 변경</span></label>
				</div>
				<div class="radio" style="margin-left: 45px; width: 200px;">
					<input type="radio" id="radio_timing_later" name="timing">
					<label for="radio_timing_later"><span>다음 결제일 변경</span></label>
				</div>
			</div>
			<button id="changeSubmit" class="btn_blue planchangeBtn">OK</button>
		</div>
		<!-- //.lyr_bd -->
		
	</div>
	<!-- //.productWrap -->   
</div>
<!-- //.lyr_plan -->
<!-- .plan_oklayer -->
<div class="lyr_plan plan_oklayer">
	<div class="lyr_plan_bg"></div>
	<!-- .lyrWrap -->
	<div class="lyrWrap">
		<button class="btn_lyrWrap_close" type="button">Close</button>
		<!-- .lyr_bd -->
		<div class="lyr_bd">
			<em class="fas fa-briefcase"></em>
			<p>플랜 변경</p>
			<span>플랜 변경이 완료되었습니다.</span>
			<button id="planChangeResult" class="btn_blue">OK</button>
		</div>
		<!-- //.lyr_bd -->
	</div>
	<!-- //.lyrWrap -->
</div>
<!-- //.plan_oklayer -->
<!-- .lyr_planconf -->
<%--<div class="lyr_planconf">--%>
<%--    <div class="lyr_plan_bg"></div>--%>
<%--    <!-- .lyrWrap -->--%>
<%--    <div class="lyrWrap">--%>
<%--        <button class="btn_lyrWrap_close" type="button">Close</button>--%>
<%--        <!-- .lyr_bd -->--%>
<%--        <div class="lyr_bd">--%>
<%--            <em class="fas fa-sad-cry"></em>--%>
<%--            <p>Are you sure that you want to proceed to cancellation?</p>--%>
<%--            <span>You are eligible to use the service until the expiry date of your subscription even though you request the cancellation. Refund calculated proportionally to the cancellation date will not be applied. Please advise that your subscription will be terminated after the expiry date.--%>
<%--</span>--%>
<%--            <button class="btn" id="notTerminate">No</button>--%>
<%--            <button class="btn_blue" id="terminateBtn">Yes</button>--%>

<%--        </div>--%>
<%--        <!-- //.lyr_bd -->--%>
<%--    </div>--%>
<%--    <!-- //.lyrWrap -->--%>
<%--</div>--%>


<div class="lyr_planconf">
	<div class="lyr_plan_bg"></div>
	<!-- .lyrWrap -->
	<div class="lyrWrap">
		<button class="btn_lyrWrap_close" type="button">제품 리스트 닫기</button>
		<!-- .lyr_bd -->
		<div class="lyr_bd">
			<em class="fas fa-sad-cry"></em>
			<p>Cancel Your Membership?</p>
			<span>
				<ul>
					<li>Even if the membership is canceled you will not retroactively refund subscription payments.</li>
					<li>No refund will be given, and service usage will be maintained until the day before the next payment date.</li>
					<li>For the first month free users, all services stop immediately when you cancel the membership.</li>
				</ul>
			</span>
			<div class="stn_survey">
				<p>Please share your overall satisfaction level for cancellation.</p>
				<div class="survey_box">
					<span class="txt_fl"><strong>1. Website usability</strong>:  How to use the service, Order of use, Buttons, etc. </span>
					<div class="selectarea">
						<div class="radio">
							<input type="radio" id="1_1" name="radio_usability" value="5">
							<label for="1_1"><span>Very satisfied</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="1_2" name="radio_usability" value="4">
							<label for="1_2"><span>Satisfied</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="1_3" name="radio_usability" value="3">
							<label for="1_3"><span>Neutral</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="1_4" name="radio_usability" value="2">
							<label for="1_4"><span>Dissatisfied</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="1_5" name="radio_usability" value="1">
							<label for="1_5"><span>Very dissatisfied</span></label>
						</div>
					</div>
				</div>
				<div class="survey_box">
					<span class="txt_fl"><strong>2. Service Performance</strong>:  Quality, Performance, Results Satisfaction</span>
					<div class="selectarea">
						<div class="radio">
							<input type="radio" id="2_1" name="radio_demo" value="5">
							<label for="2_1"><span>Very satisfied</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="2_2" name="radio_demo" value="4">
							<label for="2_2"><span>Satisfied</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="2_3" name="radio_demo" value="3">
							<label for="2_3"><span>Neutral</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="2_4" name="radio_demo" value="2">
							<label for="2_4"><span>Dissatisfied</span></label>
						</div>
						<div class="radio">
							<input type="radio" id="2_5" name="radio_demo" value="1">
							<label for="2_5"><span>Very dissatisfied</span></label>
						</div>
					</div>
				</div>

				<div class="survey_box">
					<span class="txt_fl"><strong>3. Anything else to add. Your feedbacks make our service better.</strong></span>
					<div class="text_area">
						<textarea id="input_etc"></textarea>
					</div>
				</div>

			</div>
			<span class="last_txt">Sure to cancel?</span>
			<button class="btn" id="notTerminate">No</button>
			<button class="btn_blue" id="terminateBtn">Yes</button>
		</div>
		<!-- //.lyr_bd -->
	</div>
	<!-- //.lyrWrap -->
</div>

<!-- //.lyr_planconf -->

<div class="lyr_receipt">
	<div class="lyr_plan_bg"></div>
	<!-- .productWrap -->
	<div class="lyrWrap">				
		<button class="btn_lyrWrap_close" type="button">Close</button>
		<!-- .lyr_bd -->
		<div class="lyr_bd">
			<img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_receipt.png" alt="receipt" class="receiptPop">
			<p>Receipt</p>
			<ul>
				<li><span>Date</span><strong id="receipt_date"></strong></li>
				<li><span>Product</span><strong id="receipt_good"></strong></li>
				<li><span>Payment Information</span><strong id="receipt_method"></strong></li>
<%--				<li><span>결제구분</span><strong>일시불</strong></li>--%>
				<li><span>Confirmation Number</span><strong id="receipt_pano"></strong></li>
				<li><span>Amount</span><strong class="price" id="receipt_price"></strong></li>
			</ul>
			<button id="receiptBtn" class="btn_blue">OK</button>

		</div>
		<!-- //.lyr_bd -->

	</div>
	<!-- //.productWrap -->   
</div>
<!-- //.lyr_receipt -->
<!-- .plan_change -->
<div class="lyr_plan plan_change">
    <div class="lyr_plan_bg"></div>
    <!-- .lyrWrap -->
    <div class="lyrWrap">
        <button class="btn_lyrWrap_close" type="button">제품 리스트 닫기</button>
        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <em class="fas fa-check"></em>
            <p>Termination of membership</p>
            <span>Your membership has been terminated.</span>
            <button class="btn_blue" id="completBtn">OK</button>
        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.lyrWrap -->
</div>
<!-- //.plan_change -->
<!-- .contents -->
		<div class="contents">
			<div class="content">
				<h1>Account Information</h1>
				<!--.demobox_nlu-->
				<div class="demobox">
					<div class="account">
						<div class="stn_1">
							<ul>
								<li><span>Plan</span>
									<em id="plan_name">BUSINESS</em>
<%--									<button type="button" class="changePlan" id="changePlanBtn">변경</button>--%>
									<button type="button" class="unsubscribe" >Unsubscribe</button>
								</li>
								<li><span>Amount</span><strong id="plan_price">&#8361; 0</strong> / per month</li>
								<li><span>Method</span><em id="plan_pay_info" ></em>
<%--									<button type="button" class="changeMethod">변경</button>--%>
								</li>
								<li><span id="nextBillingTitle">Next Payment Info.</span><em id="nextBilling"><em id="nextBillingDate"></em><em>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</em><em id="nextBillingGoodName"></em><em>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;</em><em id="nextBillingPrice"></em></em></li>

							</ul>
						</div>

						<div class="stn_2">
							<p>Payment History</p>
							<table class="payment_tbl">
								<colgroup>
									<col width="25%"><col width="25%"><col width="16%"><col width="16%"><col width="14%">
								</colgroup>
								<thead>
									<tr class="thead">
										<th scope="col">Service Period</th>
										<th scope="col">Method</th>
										<th scope="col">Amount</th>
										<th scope="col">Payment Date</th>
										<th scope="col">Receipt</th>
									</tr>
								</thead>
								<tbody id="list_body">
								</tbody>
							</table>	
							<!-- 페이징 -->
<%--							<div class="pageing">--%>
<%--								<a href="#" class="first">처음페이지</a>--%>
<%--								<a href="#" class="prev">이전페이지</a>--%>
<%--								<strong>1</strong>--%>
<%--								<a href="#">2</a>--%>
<%--								<a href="#">3</a>--%>
<%--								<a href="#">4</a>--%>
<%--								<a href="#">5</a>--%>
<%--								<a href="#">6</a>--%>
<%--								<a href="#" class="next">다음페이지</a>--%>
<%--								<a href="#" class="end">마지막페이지</a>--%>
<%--							</div>--%>
							<!-- //페이징 -->
						</div>	
					</div>
				</div>
				<!--//account-->
			</div>

		</div>
		<!-- //.contents -->

<script type="text/javascript">

	var paymentList = ${paymentList}
	var nextBillingInfo = ${nextBillingInfo}
	console.dir(nextBillingInfo);

	function cardNoDisplay (cardNo) {
		if(cardNo != null) {
		    if(cardNo == "paypalcard"){
		        return cardNo;
            }else {
                return cardNo.toString().replace(/(\d|\*){4}/g, "$& ");
            }
		}else {
			return "";
		}
	}

	function priceDisplay (price) {
		return price.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	}

	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){
			var payDate;
			var goodName;
			var price;

			// --------------------------------- 현재 결제 정보 dispaly
			var paymentNow = {};
			paymentNow = ${billingInfo};
			console.dir(paymentNow);

			payDate = paymentNow.payDate;
			goodName = paymentNow.goodName;

			try {
				console.dir($("input[name='plan'][value="+paymentNow.goodName+"]"));
				$("input[name='plan'][value="+paymentNow.goodName+"]").attr("checked", "checked");
				$("#plan_name").html(paymentNow.goodName);
				if(paymentNow.cardNo == null) {
                    $("#plan_pay_info").html("");
					$("#plan_price").html("");
                }else if(paymentNow.cardNo == "paypalcard"){
					price = "&#36; " + paymentNow.priceUs;
					$("#plan_price").html(price);
                    $("#plan_pay_info").html("PayPal");
					$(".changeMethod").remove();
                }else{
					price = "&#8361 " + paymentNow.price;
					$("#plan_price").html(price);
                    $("#plan_pay_info").html(paymentNow.issuerName + "카드 " + cardNoDisplay(paymentNow.cardNo))
                }

				for (let i = 0; i < paymentList.length; i++){
				    if(paymentList[i].payment != 'Free'){
                        console.log("==============================");
                        console.log(paymentList[i]);
                        console.log("==============================");
					    if(paymentList[i].payment == 'PayPal'){
						    $("#list_body").append(
                                "<tr>" +
                                "<td scope='row' >" + paymentList[i].dateFrom + " ~ " + paymentList[i].dateTo + "</td>" +
                                "<td style=\"text-align: center;\">"+ paymentList[i].cardNo+ "</td>" +
                                "<td>&#36; " + priceDisplay(paymentList[i].price)+ "</td>" +
                                "<td>" + paymentList[i].payDate + "</td>" +
                                "<td><img src='${pageContext.request.contextPath}/aiaas/kr/images/ico_receipt.png' alt='receipt' class='receiptPop' data-pano='"+ paymentList[i].pano +"'></td>" +
                                "</tr>"
						    )
					    }else{
                            $("#list_body").append(
                                "<tr>" +
                                "<td scope='row' >" + paymentList[i].dateFrom + " ~ " + paymentList[i].dateTo + "</td>" +
                                "<td>" + paymentList[i].issuerName  + "card " + cardNoDisplay(paymentList[i].cardNo) + "</td>" +
                                "<td>&#8361; " + priceDisplay(paymentList[i].price) + "</td>" +
                                "<td>" + paymentList[i].payDate + "</td>" +
                                "<td><img src='${pageContext.request.contextPath}/aiaas/kr/images/ico_receipt.png' alt='receipt' class='receiptPop' data-pano='"+ paymentList[i].pano +"'></td>" +
                                "</tr>"
                            )
                        }
                    }
				}
			} catch(err) {
				if (paymentNow == null) {
					$(".changeMethod").remove();
					$(".changePlan").text('가격정책');
				}
			}

			$("#nextBillingDate").html(payDate);
			$("#nextBillingGoodName").html(goodName);
			$("#nextBillingPrice").html(price);

			//-------------------------------------------------------------
	/*		if(nextBillingInfo != null) {
				console.log((nextBillingInfo.payDate));
				console.log((nextBillingInfo.goodName));
				console.log((nextBillingInfo.price));
				$("#nextBillingDate").html(nextBillingInfo.payDate);
				$("#nextBillingGoodName").html(nextBillingInfo.goodName);
				$("#nextBillingPrice").html(nextBillingInfo.price);
			}else if (!nextBillingInfo) {
				$("#nextBilling").html("There is no information to pay.")
			}*/

            $.ajax({
                url: "/member/getUserStatus",
                data: {"${_csrf.parameterName}": "${_csrf.token}"},
                type: "POST",
                dataType: "JSON",

                success: function (response) {
                    status = response;
                    if (response == ${UserStatus.UNSUBSCRIBING}) {
                        $('.stn_1 .unsubscribe').hide();
						$('.stn_1 li:eq(0)').append('<button type="button" class="unsubscribe2" disabled>[ Unsubscribing ]</button>');
						if(paymentNow.cardNo != "paypalcard"){
							$('.stn_1 li:eq(0)').append('<button type="button" id="btn_cancelUnsub" class="cancel_unsub">Cancel subscription</button>');
						}


                        $('#plan_pay_info').hide();
                        $(".changeMethod").hide();
                        $('#nextBillingTitle').text("Expiration date");
                        $("#nextBilling").html(paymentNow.payDate);

                        if(nextBillingInfo != null) {
                            $("#nextBillingDate").html(nextBillingInfo.payDate);
                            $("#nextBillingGoodName").html(nextBillingInfo.goodName);
                            $("#nextBillingPrice").html(nextBillingInfo.price);
                        }
                    }
                },
                error: function (e) {
                    alert("서버와의 통신이 원활하지 않습니다.");
                    console.log("사용자 정보를 가져올 수 없습니다.");
                }
            });


			$('#changePlanBtn').on('click', function(){
				if (paymentNow == null) {
					window.location.href = "/support/pricing";
					return;
				}

				$('#changePlan').fadeIn(300);
				$('.plan_oklayer').hide();

				$("#timing_now").click(function() {
					$("#radio_timing_now").attr("checked", "checked");
				});
				$("#timing_later").click(function() {
					$("#radio_timing_later").attr("checked", "checked");
				});

				$("#changeSubmit").click(function() {
					console.log($("input[name='plan']:checked").val());

					if($("#radio_timing_now").is(":checked")) {
						window.location.href = "/support/pricing";
					} else if ($("#radio_timing_later").is(":checked")) {
						$.ajax({
							url: "/payment/planChangeConfirm",
							data: {
								'userNo' : '${accessUser.userno}',
								'goodName' : $("input[name='plan']:checked").val(),
								'${_csrf.parameterName}' : '${_csrf.token}'
							}
						}).success(function(e){
							console.log(e);

							if (e == 0) {
								$.ajax({
									url: "/member/changePlanNextBilling",
									data: {
										'goodName' : $("input[name='plan']:checked").val(),
										'${_csrf.parameterName}' : '${_csrf.token}'
									}
								}).success(function(){
									$('.lyr_plan').hide();
									$('.plan_oklayer').show();
								})
							} else {
								alert("현재 적용된 요금제와 동일한 요금제 입니다.");
							}
						})
                    }
				})
			});

			// --------------------------------- 카드 정보 수정 function (inicis From)
			$('.changeMethod').on('click', function(){

				let signature;

				console.log($("input[name='returnUrl']").val());

				function setParam(price, goodName, sign, callback) {
					$("#billingPrice").val(price);
					$("#billingGoodName").val(goodName);
					$("#billingSignature").val(sign);
					console.dir($("#billingForm"));
					callback();
				}

				function payFunc() {
					INIStdPay.pay('billingForm');
				}

				if (paymentNow == null) {
					alert("There is no payment information currently applied.");
					return;
				}

				if (paymentNow.goodName == "BASIC") {
					signature = $("#basicSignature").val();
				} else if (paymentNow.goodName == "BUSINESS") {
					signature = $("#businessSignature").val();
				} else {
					alert("error! " + paymentNow.goodName);
					return;
				}

				setParam(paymentNow.price, paymentNow.goodName, signature, payFunc);

			});
			//-------------------------------------------------------------

			// --------------------------------- 영수증 출력 function
			$('.receiptPop').on('click', function(){
				var pano = $(this).data("pano");
				$.ajax({
					url: "/member/payinfo",
					data: {pano: pano},
					"${_csrf.parameterName}" : "${_csrf.token}"

				}).done(function(data) {
					var payinfo = JSON.parse(data);
					console.dir(payinfo);

					$("#receipt_date").html(payinfo.payDate);
					$("#receipt_good").html(payinfo.goodName);
					if(payinfo.issuerName == "PayPal"){
                        $("#receipt_method").html(payinfo.cardNo);
                        $("#receipt_price").html("$ " + priceDisplay(payinfo.price));
                    }else {
                        $("#receipt_method").html(payinfo.issuerName + " " + cardNoDisplay(payinfo.cardNo));
                        $("#receipt_price").html(priceDisplay(payinfo.price) + "won");
                    }
					$("#receipt_pano").html(payinfo.pano.replace("mindslab01_", ""));

				});

				$('.lyr_receipt').fadeIn(300);
			});


			// ---------------------------------------------------------
			// --------------------------------- 영수증 확인 버튼 function
			$("#receiptBtn").click(function() {
				$('.lyr_receipt').fadeOut(300);
			});


			$("#notTerminate").click(function(){
				$('.lyr_planconf').fadeOut(300);
			});

			// product layer popup
			$('.btn_lyrWrap_close, .lyr_plan_bg').on('click', function () {
				$('#changePlan').fadeOut(300);
				$('.lyr_method').fadeOut(300);
                $(".plan_change").fadeOut(300);
                $('.lyr_receipt').fadeOut(300);
				$('.lyr_planconf').fadeOut(300);
				$('.plan_oklayer').fadeOut();
				$('body').css({
					'overflow': ''
				});
			});

			$('#planChangeResult').click(function() {
				window.location.reload();
			});

            $('.unsubscribe').on('click', function(){
                $('.lyr_planconf').fadeIn(300);
                // $('.lyr_plan').fadeOut(300);
            });

            $('#terminateBtn').on('click', function(){

            	// feedback용 값들 --- 추후 kr 페이지 참고하여 수정 필요!
				var usability = 0;
				if (typeof $("input[name='radio_usability']:checked").val() != "undefined") usability = $("input[name='radio_usability']:checked").val();

				var demo = 0;
				if (typeof $("input[name='radio_demo']:checked").val() != "undefined") demo = $("input[name='radio_demo']:checked").val();

				$.ajax({
					url: "/payment/planCancel",
					type: "POST",
					data: {
						"${_csrf.parameterName}": "${_csrf.token}",
						"payMethod" : paymentNow.issuerName,
						"feedbackUsability": usability,
						"feedbackDemo": demo,
						"feedbackEtc": $('#input_etc').val()
					},

					success: function (response) {
						if(response.msg == "FAIL"){
							alert("Subscription cancel is failed.");
							console.log("Fail : " + response.errMsg);
                            $('.lyr_planconf').hide();
							return;
						}
						$('.plan_change').fadeIn(300);
						$('.lyr_planconf').hide();
					},
					error: function (response) {
						alert("error");
						console.log(response);
                        $('.lyr_planconf').hide();
						window.location.reload();
					}
				});
            });

            $("#completBtn").on('click', function () {
                $(".plan_change").fadeOut(300);

                if(status == ${UserStatus.FREE}) {
                    document.getElementById('logout-form').submit();
                } else {
                    location.reload();
                }
            });
        });
	});
</script>