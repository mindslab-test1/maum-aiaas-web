<%--
  Created by IntelliJ IDEA.
  User: YGE
  Date: 2020-12-03
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<!-- .contents -->
<div class="contents api_content">
    <!-- .content -->
    <div class="content api_content">
        <h1 class="api_tit">Lip Sync Avatar</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'avrdemo')" id="defaultOpen">
                <button type="button">AI Engine</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'avrexample')">
                <button type="button">Use Case</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'avrmenu')">
                <button type="button">Manual</button>
            </li>
        </ul>

        <!-- .avrdemo -->
        <div class="demobox lipSyncAvatar" id="avrdemo">
            <p><span>Lip Sync Avatar</span></p>
            <span class="sub">A naturally speaking avatar is created following the inputted text.</span>

            <!--demo_layout-->
            <div class="demo_layout">
                <!--avr_1-->
                <div class="avr_1">
                    <div class="avatar_box">
                        <p>
                            <em class="far fa-image"></em>
                            <strong>Avatar</strong>
                        </p>

                        <!-- [D] 210316 AMR bg_list 추가 및 구조 수정 -->
                        <div class="bg_list sample">
                            <!-- [D] sample_list에서 선택된 input:radio의 index와 동일한 sample_box에 "active" class가 추가되어야 합니다. -->
                            <div class="sample_list">
                                <div class="radio">
                                    <input type="radio" id="avatar01" name="avatar" value="short_lipsync_server" checked> <!-- value : 모델명 -->
                                    <label for="avatar01" data-text="안녕하세요? 2021년 마인즈랩 마음 AI 아바타 인사드립니다.">
                                        Broadcaster
                                    </label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="avatar02" name="avatar" value="lipsync_curator_woman">
                                    <label for="avatar02" data-text="이번에는 고흐의 소개와 그림의 특징에 관해 자세히 소개하도록 하겠습니다.">
                                        Docent
                                    </label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="avatar03" name="avatar" value="lipsync_teacher_man">
                                    <label for="avatar03" data-text="안녕하세요, 여러분, 즐거운 한국사 시간입니다. 모두 교재 준비되었나요?">
                                        Teacher (M)
                                    </label>
                                </div>
                            <sec:authorize access="hasAnyRole('ADMIN', 'INTERNAL')">
                                <div class="radio" style="margin-top: 15px;">
                                    <input type="radio" id="avatar06" name="avatar" value="michael">
                                    <label for="avatar06" data-text="안녕하세요 고객 여러분. 저는 마인즈랩 마이클입니다.">
                                        CMO (Michael)
                                    </label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="avatar05" name="avatar" value="pcmcolor_lipsync_server">
                                    <label for="avatar05" data-text="시청자 여러분 안녕하세요. 저는 AI 아나운서 박철민입니다.">
                                        Broadcaster (Chul min Park)
                                    </label>
                                </div>
                                <div class="radio">
                                    <input type="radio" id="avatar07" name="avatar" value="sbh">
                                    <label for="avatar07" data-text="안녕하세요 손병희 교수입니다. 오늘은 비대면 수업을 진행하도록 하겠습니다.">
                                        Professor (Son)
                                    </label>
                                </div>
                            </sec:authorize>
                                <!-- [D] 210315 AMR 도슨트와 안내원은 추후에 추가될 예정이라고 전달받았습니다. -->
                                <!-- <div class="radio">
                                        <input type="radio" id="avatar04" name="avatar">
                                        <label for="avatar04">Docent</label>
                                    </div>
                                    <div class="radio">
                                        <input type="radio" id="avatar05" name="avatar">
                                        <label for="avatar05">Guide</label>
                                    </div> -->
                            </div>

                            <div class="sample_view">
                                <!-- [D] sample_list에서 선택된 input:radio의 index와 동일한 sample_box에 "active" class가 추가되어야 합니다. -->
                                <div class="sample_box active">
                                    <div class="video_box">
                                        <video src="${pageContext.request.contextPath}/aiaas/common/video/short_lipsync_server.mp4" controls></video>
                                    </div>
                                </div>
                                <div class="sample_box">
                                    <div class="video_box">
                                        <video src="${pageContext.request.contextPath}/aiaas/common/video/curator_tilt.mp4" controls></video>
                                    </div>
                                </div>
                                <div class="sample_box">
                                    <div class="video_box">
                                        <video src="${pageContext.request.contextPath}/aiaas/common/video/lipSync_model_teacherM.mp4" controls></video>
                                    </div>
                                </div>
                            <sec:authorize access="hasAnyRole('ADMIN', 'INTERNAL')">
                                <div class="sample_box">
                                    <div class="video_box">
                                        <video src="${pageContext.request.contextPath}/aiaas/common/video/lipsync_michael.mp4" controls></video>
                                    </div>
                                </div>
                                <div class="sample_box">
                                    <div class="video_box">
                                        <video src="${pageContext.request.contextPath}/aiaas/common/video/pcmcolor_lipsync_server.mp4" controls></video>
                                    </div>
                                </div>
                                <div class="sample_box">
                                    <div class="video_box">
                                        <video src="${pageContext.request.contextPath}/aiaas/common/video/lipsync_sbh.mp4" controls></video>
                                    </div>
                                </div>
                            </sec:authorize>
                                <!-- [D] 210315 AMR 도슨트와 안내원은 추후에 추가될 예정이라고 전달받았습니다. -->
                                <!-- <div class="sample_box">
                                        <div class="video_box">
                                            <video src="common/video/lipSync_sample.mp4" controls autoplay></video>
                                        </div>
                                    </div>
                                    <div class="sample_box">
                                        <div class="video_box">
                                            <video src="common/video/lipSync_sample.mp4" controls autoplay></video>
                                        </div>
                                    </div> -->
                            </div>
                        </div>
                    </div>

                    <div class="bg_box">
                        <p>
                            <em class="far fa-image"></em>
                            <strong>Resolution</strong>
                        </p>

                        <ul class="bg_list rslt_slt">
                            <li>
                                <div class="radio">
                                    <input type="radio" id="HighDefinition" value="HD"  name="resolution" checked>
                                    <label for="HighDefinition">High Definition (720p)</label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="StandardDefinition" value="SD"  name="resolution">
                                    <label for="StandardDefinition">Standard Definition (360p)</label>
                                </div>
                            </li>
                        </ul>
                    </div>


                    <div class="bg_box">
                        <p>
                            <em class="far fa-image"></em>
                            <strong>Background Selection</strong>
                        </p>

                        <ul class="bg_list">
                            <li>
                                <div class="radio">
                                    <input type="radio" id="bg1" name="bg" checked>
                                    <label for="bg1">
                                        <div class="img_area">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_lipSync_bg1.png" alt="background image1">
                                        </div>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="bg3" name="bg">
                                    <label for="bg3">
                                        <div class="img_area">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_lipSync_bg2.png" alt="background image2">
                                        </div>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="bg2" name="bg">
                                    <label for="bg2">
                                        <div class="img_area">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_lipSync_bg3.png" alt="background image3">
                                        </div>
                                    </label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="bg4" name="bg">
                                    <label for="bg4">
                                        <div class="img_area">
                                            <span>No background</span>
                                        </div>
                                    </label>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="txt_box">
                        <p>
                            <em class="far fa-keyboard"></em>
                            <strong>Please input the sentence you would like to hear.</strong>
                        </p>

                        <div class="textarea">
                            <!-- [D] placeholder외에 기본으로 입력되는 문장(안녕하세요?~인사드립니다.)은 유지해주세요 -->
                            <textarea id="text-contents" class="lipSync_txt" placeholder="Input the text the avatar will speak within 50 characters." maxlength="50">안녕하세요? 2021년 마인즈랩 마음 AI 아바타 인사드립니다.</textarea>
                        </div>

                        <span class="txt_length"><strong id="count">0</strong> &sol; 50characters</span>
                        <ul class="info_txt">
                            <li><b style="color:#ff2647">*Current model is optimized for Korean.</b></li>
                            <li>*Avatar and TTS models are customizable.</li>
                            <li>*If you need to modify the current TTS model, to use special characters (e.g., #, *, ~), special units (e.g., ㎖, ㎡, ㎧ ㎉, etc.),<br>&nbsp;&nbsp;or other languages, please contact the customer center.</li>
                        </ul>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">Generate</button>
                    </div>
                </div>
                <!--avr_1-->

                <!--avr_2-->
                <div class="avr_2">
                    <p><em class="far fa-file-video"></em>In Progress</p>

                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve">
                                    <path fill="#fcc6ce" fill-opacity="0.42"
                                          d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z" />
                            <g>
                                <path fill="#f7778a" fill-opacity="1"
                                      d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z" />
                                <animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms"
                                                  repeatCount="indefinite" />
                            </g>
                                </svg>
                        <!-- [D] .waiting_num에 현재 사용인원 넣어주세요 -->
                        <p id="waitingText">Currently used by <span class="waiting_num">0</span>users.</p>
                        <p id="processingText">Please wait a moment.</p>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
                    </div>
                </div>
                <!--avr_2-->

                <!--avr_3-->
                <div class="avr_3">
                    <p class="tit"><em class="far fa-file-video"></em>Result</p>
                    <div class="result_file file_box">
                        <div class="result_box">
                            <video id="outputVideo" src="" controls autoplay></video>
                        </div>

                        <div class="txt_box">
                            <span>Input Text</span>
                            <div class="textarea">
                                <p id="outputText" class="input_"></p>
                            </div>
                        </div>

                        <a id="save" class="btn_dwn" download="lipSyncAvatar.mp4" href="">
                            <em class="far fa-arrow-alt-circle-down"></em> Download Result
                        </a>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_back2"><em class="fas fa-redo"></em>Reset</button>
                    </div>
                </div>
                <!--avr_3-->
            </div>
            <!--// demo_layout-->
        </div>
        <!-- //.avrdemo -->

        <!--.avrmenu-->
        <div class="demobox vision_menu" id="avrmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">API Guideline</div>

                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID & Key</p>
                    <p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                    <p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in.<br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                    <p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>

                <div class="guide_group">
                    <div class="title">Lip Sync Avatar</div>

                    <p class="sub_txt">A naturally speaking avatar is created following the inputted text.</p>
                    <span class="sub_title">Preparation</span>
                    <p class="sub_txt">- Input 1: Sentence text you want to hear</p>
                    <ul>
                        <li>Restriction 1) : Sentence must be within 1000 characters (Korean standard)</li>
                        <li>Restriction 2) : The longer the input sentence, the longer it takes to process the video.</li>
                    </ul>
                    <p class="sub_txt">- Input 2: Background image file</p>
                    <ul>
                        <li>Supported files : .jpg, .png</li>
                        <li>Image capacity : 10MB or less </li>
                    </ul>
                    <span class="sub_title">API Document</span>
                    <em>&#8203;Upload : Insert the text and background image to be spoken and receive a request key.</em>
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/lipsync/upload</li>
                    </ul>
                    <p class="sub_txt">② Request parameters</p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API ID. Request from is required for the ID.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>text</td>
                            <td>Sentence (*1000 characters or less)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>image</td>
                            <td>(optional) Background image</td>
                            <td>file</td>
                        </tr>
                        <tr>
                            <td>model</td>
                            <td>
                                (optional) Avatar model selection (default = baseline)
                            </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>transparent</td>
                            <td>
                                (optional) Transparency selection (default = false)
                                <br>* 'transparent=true' option can be objected when 'image' is given.
                            </td>
                            <td>boolean</td>
                        </tr>
                        <tr>
                            <td>resolution</td>
                            <td>(optional) Resolution selection (HD, SD). (default = HD)</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
                                <pre>
curl --location --request POST
'https://api.maum.ai/lipsync/upload' \
--header 'Content-Type: multipart/form-data' \
--form 'apiId= "Own API Id"' \
--form 'apiKey= "Own API Key"' \
--form 'text= "안녕하세요? 2021년 마인즈랩 마음 AI 아바타 인사드립니다."' \
--form 'image= @"{image file}"' \
--form 'model="baseline"' \
--form 'transparent="true"' \
--form 'resolution="HD"'
</pre>
                    </div>

                    <p class="sub_txt">④ Request parameters</p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message </td>
                            <td>API operation</td>
                            <td>object</td>
                        </tr>
                        <tr>
                            <td>payload</td>
                            <td>result</td>
                            <td>object</td>
                        </tr>
                    </table>

                    <span class="table_tit">message: API operation</span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>String describing the request processing status (Success/Fail)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>Status code for request processing status (0: Success)</td>
                            <td>number</td>
                        </tr>
                    </table>

                    <span class="table_tit">payload: result</span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>requestKey</td>
                            <td>Request key for avatar creation</td>
                            <td>string</td>
                        </tr>
                    </table>

                    <p class="sub_txt">⑤ Response example </p>

                    <div class="code_box">
                                <pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "payload": {
        "requestKey": "request key"
    }
}
</pre>
                    </div>
                    <!-- Upload END-->


                    <!-- Status check START-->
                    <em>&#8203;Status Check: Status of the request.</em>
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/lipsync/statusCheck</li>
                    </ul>
                    <p class="sub_txt">② Request parameters </p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API ID. Request from is required for the ID.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>requestKey</td>
                            <td>Request identification key issued at the time of upload request</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
                                <pre>
curl --location --request POST
'https://api.maum.ai/lipsync/statusCheck' \
--header 'Content-Type: application/json' \
--form 'apiId= "Own API Id"' \
--form 'apiKey= "Own API Key"' \
--form 'requestKey= "Request identification key issued at the time of upload request"'
</pre>
                    </div>

                    <p class="sub_txt">④ Response parameters </p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message </td>
                            <td>API operation</td>
                            <td>object</td>
                        </tr>
                        <tr>
                            <td>payload</td>
                            <td>result</td>
                            <td>object</td>
                        </tr>
                    </table>

                    <span class="table_tit">message: API operation</span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>String describing the request processing status  (Success/ Fail)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>Status code for request processing status (0: Success)</td>
                            <td>number</td>
                        </tr>
                    </table>

                    <span class="table_tit">payload: result</span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>statusCode</td>
                            <td>Request status code<br/>
                                &emsp;0 : Not yet processing started<br/>
                                &emsp;1 : Processing<br/>
                                &emsp;2 : Done<br/>
                                &emsp;3 : Invalid Request Key<br/>
                                &emsp;4 : Processing Error<br/>
                                &emsp;5 : Deleted
                            </td>
                            <td>number</td>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>String describing the request processing status</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>waiting</td>
                            <td>The number of requests waiting</td>
                            <td>number</td>
                        </tr>
                    </table>

                    <p class="sub_txt">⑤ Response example </p>

                    <div class="code_box">
                        <pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "payload": {
        "statusCode": 1,
        "message": "Now Processing",
        "waiting": 0
    }
}
                        </pre>
                    </div>
                    <!-- Status check END-->



                    <!-- Download START-->
                    <em>&#8203;Download: Download the output video.</em>
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/lipsync/download</li>
                    </ul>
                    <p class="sub_txt">② Request parameters </p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API ID. Request from is required for the ID.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>requestKey</td>
                            <td>Request identification key issued at the time of upload request</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
                                <pre>
curl --location --request POST
'https://api.maum.ai/lipsync/download' \
--header 'Content-Type: application/json' \
--form 'apiId= "Unique API ID. Request from is required for the ID."' \
--form 'apiKey= "Unique API key. Request from is required for the key."' \
--form 'requestKey= "Request identification key issued at the time of upload request"'
</pre>
                    </div>
                    <p class="sub_txt">④ Response parameters </p>
                    <div class="code_box">
                        Content-Type : application/octet-stream<br/>
                        (.mp4 File Download)
                    </div>
                    <!-- Download END-->



                </div>
            </div>
        </div>
        <!--//avrmenu-->

        <!--.avrexample-->
        <div class="demobox" id="avrexample">
            <p><em style="color:#f7778a;font-weight: 400;">Use Cases</em></p>
            <span class="sub">Find out how AI can be applied to diverse areas.</span>

            <!-- lip sync Avatar -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>Video Production</span>
                            </dt>
                            <dd class="txt">When creating an informational video, such as news content or an interview, it can be presented with the voice and video image of a virtual person.</dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>Virtual Assistant</span>
                            </dt>
                            <dd class="txt">Used for software agents, it can handle user-requested tasks and provide specialized services, just like personal assistants.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_bot"><span>Chatbot</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>Virtual<strong>Concierge Kiosk</strong></span>
                            </dt>
                            <dd class="txt">Used in the form of a virtual concierge, it provides all the services necessary of that of a hotel concierge.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_conciergeBot"><span>Hotel Concierge Chatbot</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //lip sync Avatar -->
        </div>
        <!--//.avrexample-->
    </div>
    <!-- //.content -->
</div>
<script>

    let sampleFileList = [
        {filePath: "${pageContext.request.contextPath}/aiaas/common/images/img_lipSync_bg1.png", fileName : "img_lipSync_bg1.png", file : null},
        {filePath: "${pageContext.request.contextPath}/aiaas/common/images/img_lipSync_bg2.png", fileName : "img_lipSync_bg2.png", file : null},
        {filePath: "${pageContext.request.contextPath}/aiaas/common/images/img_lipSync_bg3.png", fileName : "img_lipSync_bg3.png", file : null},
        <%--{filePath: "${pageContext.request.contextPath}/aiaas/common/images/img_lipSync_bg4.png", fileName : "img_lipSync_bg4.png", file : null}--%>
        {filePath: "", fileName : "transparent", file : null}
    ]

    let request = null;
    let timerId = null;
    let apiUrl = "${apiUrl}";


    function loadSample(fileObj, index){
        let blob = null;
        let xhr = new XMLHttpRequest();
        xhr.open("GET", fileObj.filePath);
        xhr.responseType = "blob";
        xhr.onload = function(){
            blob = xhr.response;
            sampleFileList[index].file = new File([blob], fileObj.fileName);
        }
        xhr.send();
    }


    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {

            // 샘플 이미지 로드
            sampleFileList.forEach((file, idx)=>{
                if(file.fileName !== "transparent"){
                    loadSample(file, idx);
                }
            });

            // 글자수 count
            countTextAreaLength();

            $('.sample_view video').on('')


            // 아바타 선택시 문장 변경
            $('.sample_list .radio label').on('click', function(){
                $('.sample_view video').each(function (idx, el){
                    $(el).get(0).pause();
                });
                $('.sample_view .sample_box').removeClass('active');
                $('.sample_view .sample_box').eq($('.sample_list .radio label').index($(this))).addClass('active');
                $('#text-contents').val($(this).data("text"));
                countTextAreaLength();
            });

            // step2->step1
            $('.btn_back1').on('click', function () {
                if(timerId != null) clearTimeout(timerId);
                if(request) request.abort();

                $('.avr_2').hide();
                $('.avr_1').fadeIn(300);
            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('#outputVideo').get(0).pause();
                $('.avr_3').hide();
                $('.avr_1').fadeIn(300);
            });

            // 문장 입력 글자수
            $('.lipSync_txt').on('input keyup paste change', countTextAreaLength);


            // api 호출
            $('#sub').on('click', function () {

                $('.sample_view video').each(function (idx, el){
                    $(el).get(0).pause();
                });

                let text = $('#text-contents').val();

                if(text.trim() === ""){
                    alert("Please enter the sentence you want to hear.");
                    return false;
                }

                let selectedIdx = $('.radio input[name=bg]').index( $('.radio input[name=bg]:checked') );
                let image = sampleFileList[selectedIdx].file;
                let model = $('.radio input[name=avatar]:checked').val();
                let transparent = (selectedIdx === 3);

                let resolution = $('.radio input[name=resolution]:checked').val();
                let width = 0;
                let height = 0;
                if(resolution == 'HD') {    // 720P인 경우
                    width = 1280;
                    height = 720
                } else if (resolution == 'SD') {    // 360P인 경우
                    width = 640;
                    height = 360;
                }

                requestUpload(image, text, model, transparent, width, height);

                $('#processingText').show();
                $('#waitingText').hide();
                $('#outputText').text(text);

                $('.avr_1').hide();
                $('.avr_2').fadeIn(200);
            });

        });
    });


    function requestUpload(image, text, model, transparent, width, height){
        console.log("image :" + image + "\ntext :" + text + "\nmodel : " + model + "\ntransparent : " + transparent + "\nwidth : " + width + "\nheight : " + height);

        let formData = new FormData();
        formData.append('image', image);
        formData.append('text', text);
        formData.append('model', model);
        formData.append('transparent', transparent);
        formData.append('width', width);
        formData.append('height', height);
        formData.append('${_csrf.parameterName}', '${_csrf.token}');

        request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if(request)
            if(request.readyState === 4 && request.status === 200) {
                if(!request.response){
                    handleError("Empty response : upload api");
                    return;
                }
                let reqKey = JSON.parse(request.response).payload.requestKey;
                callBackUpload(reqKey);
            }
        };
        request.open('POST', '/api/lipSyncAvatar');
        request.send(formData);
        request.timeout = 60000;
        request.ontimeout = function() { handleError("Timeout error : upload api"); }
        request.onabort = function(){ }
    }


    /** statusCheck API의 STATUS 값에 따라 분기 처리
     * 0 : 대기중 - 대기중인 사용자의 수를 화면에 보여줌 -> setTimeout으로 다시 api call
     * 1 : 처리중 - 대기가 끝나고 api 처리를 기다리는 중, 처리중 문구를 보여줌 -> setTimeout으로 다시 api call
     * 2 :  완료  - handleSuccess()
     * 3 : WRONG_KEY
     * 4 : ERROR
     * 5 : DELETED
    */
    function callBackUpload(reqKey){

        console.log("%c callBackUpload()",'color:green');

        timerId = setTimeout(function delayFunc(){

            getStatusPromise(reqKey).then((res) => {
                console.log("%c getStatusPromise() -> then()",'color:green', reqKey);
                if(res){
                    let payload = res;
                    console.log("STATUS : ", payload.statusCode, payload.message);
                    switch (payload.statusCode){
                        case 0:
                            $('#waitingText .waiting_num').text(payload.waiting);
                            $('#processingText').hide();
                            $('#waitingText').show();
                            timerId = setTimeout(delayFunc, 2000);
                            break;
                        case 1:
                            $('#waitingText .waiting_num').text("");
                            $('#waitingText').hide();
                            $('#processingText').show();
                            timerId = setTimeout(delayFunc, 2000);
                            break;
                        case 2:
                            handleSuccess(reqKey);
                            break;
                        default:
                            handleError("statusCheck message : " + payload.message);
                            return;
                    }
                }else{
                    handleError("Empty response : statusCheck api");
                }

            })
            .catch((errMsg) => {
                handleError(errMsg);
            });
        }, 0);
    }


    // 상태 확인 api
    function getStatusPromise(reqKey){
        let reqJson = {
            "apiId" : "${fn:escapeXml(sessionScope.accessUser.apiId)}",
            "apiKey": "${fn:escapeXml(sessionScope.accessUser.apiKey)}",
            "requestKey": reqKey
        }

        return new Promise((resolve, reject) => {
            request = new XMLHttpRequest();
            request.onreadystatechange = function(){
                if(request.readyState === XMLHttpRequest.DONE){
                    if(request.status === 200 ){
                        resolve(JSON.parse(request.response).payload);
                    }else{
                        reject('Error: in call getStatus with response status - ' + request.status);
                    }
                }
            };
            request.open('POST', apiUrl + '/lipsync/statusCheck');
            request.setRequestHeader("Content-Type", "application/json");
            request.send(JSON.stringify(reqJson));
            request.timeout = 60000;
            request.onerror = () => {
                reject('Error\n'
                    + "status : " + request.status
                    + "\n message : " + request.response);
            }
            request.onabort = function(){ }
        });
    }

    function handleSuccess(reqKey){

        console.log("%c handleSuccess() ",'color:blue');

        getOutputPromise(reqKey)
        .then((res)=>{
            let result = new Blob([res], {type: 'video/mp4'});
            let srcUrl = URL.createObjectURL(result);

            $('#outputVideo').attr('src', srcUrl);
            $('#save').attr('href', srcUrl );

            $('.avr_2').fadeOut(200);
            $('.avr_3').fadeIn(200);
        })
        .catch((errMsg)=>{
            handleError(errMsg);
        });
    }

    function handleError(errorMsg){
        alert(errorMsg);
        window.location.reload();
    }


    // 결과 요청 api
    function getOutputPromise(reqKey){

        let reqJson = {
            "apiId" : "${fn:escapeXml(sessionScope.accessUser.apiId)}",
            "apiKey": "${fn:escapeXml(sessionScope.accessUser.apiKey)}",
            "requestKey": reqKey
        }

        return new Promise((resolve, reject) => {
            request = new XMLHttpRequest();
            request.responseType = 'blob';
            request.onreadystatechange = function(){
                if(request.readyState === XMLHttpRequest.DONE){
                    if(request.status === 200 ){
                        request.response == null
                            ? reject("Empty response : output download api")
                            : resolve(request.response);
                    }
                    else{
                        reject('Error: ' + request.status);
                    }
                }
            };
            request.open('POST', apiUrl + '/lipsync/download');
            request.setRequestHeader("Content-Type", "application/json");
            request.send(JSON.stringify(reqJson));
            request.timeout = 60000;
            request.onerror = () => {
                reject('Error\n'
                    + "status : " + request.status
                    + "\n message : " + request.response);
            }
            request.onabort = function(){
                console.error("abort : getDownload api");
            }
        });
    }


    function countTextAreaLength (){
        $('#count').html($('#text-contents').val().length);
    }


    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

</script>