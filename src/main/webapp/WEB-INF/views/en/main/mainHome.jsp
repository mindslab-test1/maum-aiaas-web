<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/jquery-ui-1.10.3.custom.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/pop_common.css">

<%--<!-- .page loading -->--%>
<%--<div id="page_ldWrap" class="page_loading">--%>
<%--	<div class="loading_itemBox">--%>
<%--		<span></span>--%>
<%--		<span></span>--%>
<%--		<span></span>--%>
<%--		<span></span>--%>
<%--	</div>--%>
<%--</div>--%>


<!-- //.page loading -->
<div class="contents">
	<div class="content main_content">
		<h1>Minds Lab's AI Platform maum.ai</h1>

        <!-- egArea -->
        <div class="egArea">
            <ul class="eg_tab_nav">
                <li><a href="#eg_contents01">Recommended</a></li>
                <li><a href="#eg_contents02">Voice</a></li>
                <li><a href="#eg_contents03">Vision</a></li>
                <li><a href="#eg_contents04">Languages</a></li>
                <li><a href="#eg_contents05">Analysis</a></li>
                <li><a href="#eg_contents06">Conversation</a></li>
                <li><a href="#eg_contents07">English Education</a></li>
            </ul>
            <div class="eg_tab_container">
                <!-- 추천엔진 -->
                <div id="eg_contents01"
                     class="eg_contents">
                    <ul class="lst">
                        <!-- 음성:spe, 시각:vis, 언어:lang, 분석:ana, 대화:talk, 영어교육:eng  -->
                        <li class="vis">
                            <span class="imgBox img_vis_lsa">product image</span>
                            <span class="eg_info">
                                <em class="ico">
                                    <img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_lipSyncAvatar_w.svg" alt="engine icon">
                                </em>
                                <em class="name">
                                    <strong>Lip Sync Avatar</strong>
                                    Lip Sync Avatar
                                </em>
                                <em class="desc">A naturally speaking avatar is created following the inputted text.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/lipSyncAvatar/enLipSyncAvatarMain">Start Now</a>
                            </span>
                        </li>
                        <%--<li class="vis">
                            <span class="imgBox img_vis_aiAvatar">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_avatar_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>Face-to-Face Avatar</strong>
                                    Face-to-Face Avatar
                                </em>
                                <em class="desc">The engine creates a video of the specific portrait image mirroring the facial motions from the input video.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/avatar/enAvatarMain">Start Now</a>
                            </span>
                        </li>--%>

                        <li class="spe">
                            <span class="point">
                                <em class="best">BEST</em>
                            </span>
                            <span class="imgBox img_spe_tts">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_spe_1_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Speech Generation</strong>
                                    TTS (Text-to-Speech)
                                </em>
                                <em class="desc">Provides world-class sound quality and real-time generation
                                    speed.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/tts/enTtsMain">Start Now</a>
                                <a href="https://youtu.be/pe36jw6_93g"
                                   target="_blank"><em class="fab fa-youtube"></em>Tutorial</a>
                            </span>
                        </li>
                        <li class="spe">
                            <span class="point">
                                <em class="best">BEST</em>
                            </span>
                            <span class="imgBox img_spe_stt">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_spe_2_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Speech Recognition</strong>
                                    STT (Speech-to-Text)
                                </em>
                                <em class="desc">Converts speech into text. Provides high recognition rate
                                    and fast processing speed.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/stt/enSttMain">Start Now</a>
                                <a href="https://youtu.be/GulXlDZAxpw" target="_blank"><em class="fab fa-youtube"></em>Tutorial</a>
                            </span>
                        </li>
                        <%--<li class="vis">
                            <span class="point"><em>NEW</em></span>
                            <span class="imgBox img_vis_anmFc">product image</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_animeFace_w.svg"
                                                     alt="engine icon"></em>
								<em class="name">
									<strong>Anime Face</strong>
									Anime Face
								</em>
								<em class="desc">Generate an animated version of your face, customize, style and
									decorate.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/animeFace/enAnimeFaceMain">Start Now</a>
							</span>
                        </li>--%>
                        <li class="vis">
                            <span class="point"><em>NEW</em></span>
                            <span class="imgBox img_vis_litrDtc">product image</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_loiteringDetect_w.svg"
                                                     alt="engine icon"></em>
								<em class="name">
									<strong>Loitering Detection</strong>
									Loitering Detection
								</em>
								<em class="desc">By specifying an area, you will be notified when someone wanders the
									area for a long time.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/anomalyLoitering/enAnomalyLoiteringMain">Start Now</a>
							</span>
                        </li>
                        <li class="vis">
                            <span class="point"><em>NEW</em></span>
                            <span class="imgBox img_vis_fdDtc">product image</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_fallDownDetect_w.svg"
                                                     alt="engine icon"></em>
								<em class="name">
									<strong>Fall Down Detection</strong>
									Fall Down Detection
								</em>
								<em class="desc">Recognizes people fainting in a specific area.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/anomalyFalldown/enAnomalyFalldownMain">Start Now</a>
							</span>
                        </li>
                        <li class="vis">
                            <span class="point"><em>NEW</em></span>
                            <span class="imgBox img_vis_itrsDtc">product image</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_IntrusionDetect_w.svg"
                                                     alt="engine icon"></em>
								<em class="name">
									<strong>Intrusion Detection</strong>
									Intrusion Detection
								</em>
								<em class="desc">In a specific area, you will be notified when someone breaks into the
									zone.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/anomalyIntrusion/enAnomalyIntrusionMain">Start Now</a>
							</span>
                        </li>
                        <li class="lang">
                            <span class="point"><em>NEW</em></span>
                            <span class="imgBox img_lang_sts">product image</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_txtStyTrans_w.svg"
                                                     alt="engine icon"></em>
								<em class="name">
									<strong>Text Style Transfer</strong>
									Text Style Transfer
								</em>
								<em class="desc">It changes the sentence to the desired style while conveying the
									meaning consistently.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/styleTransfer/enStyleTransferMain">Start Now</a>
							</span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_itf">product image</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_lan_itf_w.svg" alt="engine icon"></em>
								<em class="name">
									<strong>ITF(Intent Finder)</strong>
									ITF(Intent Finder)
								</em>
								<em class="desc">Identify the intent of the question.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/itf/krItfMain">Start Now</a>
							</span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_xdc">product image</span>
                            <span class="eg_info">
        <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_lan_3_fold_w.svg" alt="engine icon"></em>
        <em class="name">
            <strong>XDC</strong>
            eXplainable Document Classifier
        </em>
        <em class="desc">Regardless of categories or length of the article, it accurately categorizes the documents.</em>
    </span>
                            <span class="btnBox">
        <a href="/cloudApi/xdc/enXdcMain">Start Now</a>
        <a href="https://youtu.be/e6xq912p_5Y" target="_blank"><em class="fab fa-youtube"></em>Tutorial</a>
    </span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_mrc">product image</span>
                            <span class="eg_info">
        <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_lan_2_fold_w.svg"
                             alt="engine icon"></em>
        <em class="name">
            <strong>MRC</strong>
            Machine Reading Comprehension
        </em>
        <em class="desc">Provide the most relevant answers by comprehending the
            whole passage.</em>
    </span>
                            <span class="btnBox">
        <a href="/cloudApi/mrc/enMrcMain">Start Now</a>
    </span>
                        </li>
                    </ul>
                </div>
                <!-- 추천엔진 -->


                <!-- 음성엔진 -->
                <div id="eg_contents02"
                     class="eg_contents">
                    <ul class="lst">
                        <!-- 음성:spe, 시각:vis, 언어:lang, 분석:ana, 대화:talk, 영어교육:eng  -->
                        <li class="spe">
                            <span class="point">
                                <em class="best">BEST</em>
                            </span>
                            <span class="imgBox img_spe_tts">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_spe_1_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Speech Generation</strong>
                                    TTS (Text-to-Speech)
                                </em>
                                <em class="desc">Provides world-class sound quality and real-time generation
                                    speed.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/tts/enTtsMain">Start Now</a>
                                <a href="https://youtu.be/pe36jw6_93g"
                                   target="_blank"><em class="fab fa-youtube"></em>Tutorial</a>
                            </span>
                        </li>
                        <li class="spe">
                            <span class="point">
                                <em class="best">BEST</em>
                            </span>
                            <span class="imgBox img_spe_stt">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_spe_2_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Speech Recognition</strong>
                                    STT (Speech-to-Text)
                                </em>
                                <em class="desc">Converts speech into text. Provides high recognition rate
                                    and fast processing speed.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/stt/enSttMain">Start Now</a>
                                <a href="https://youtu.be/GulXlDZAxpw"
                                   target="_blank"><em class="fab fa-youtube"></em>Tutorial</a>
                            </span>
                        </li>
                        <li class="spe">
                            <span class="imgBox img_spe_den">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_spe_3_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Denoise</strong>
                                    Speech Enhancement
                                </em>
                                <em class="desc">Removes various background noises in the voice recording,
                                    such as background music or audio feedbacks.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/cnnoise/enCnnoiseMain">Start Now</a>
                            </span>
                        </li>
                        <li class="spe">
                            <span class="imgBox img_spe_vf">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_spe_5_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Voice Filter</strong>
                                    Voice Filter
                                </em>
                                <em class="desc">Separate out a specific voice from a recording of a mixture
                                    of voices. MindsLab is the first to succeed in the development of this
                                    engine after Google.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/voicefilter/enVoicefilterMain">Start Now</a>
                            </span>
                        </li>
                        <%--<li class="spe">
                            <span class="imgBox img_spe_vr">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_spe_6_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Voice Recognition</strong>
                                    Voice Recognition
                                </em>
                                <em class="desc">Vectorize speakers’ voices into 512 dimensions and compare
                                    to authorize.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/vr/enVoiceRecogMain">Start Now</a>
                            </span>
                        </li>--%>
                    </ul>
                </div>
                <!-- 음성엔진 -->


                <!-- 시각엔진 -->
                <div id="eg_contents03"
                     class="eg_contents">
                    <ul class="lst">
                        <!-- 음성:spe, 시각:vis, 언어:lang, 분석:ana, 대화:talk, 영어교육:eng  -->
                        <li class="vis">
                            <span class="imgBox img_vis_lsa">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_lipSyncAvatar_w.svg" alt="아이콘"></em>
                                <em class="name">
                                    <strong>Lip Sync Avatar</strong>
                                    Lip Sync Avatar
                                </em>
                                <em class="desc">A naturally speaking avatar is created following the inputted text.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/lipSyncAvatar/enLipSyncAvatarMain">Start Now</a>
                            </span>
                        </li>
                        <%--<li class="vis">
                            <span class="imgBox img_vis_aiAvatar">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_avatar_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>Face-to-Face Avatar</strong>
                                    Face-to-Face Avatar
                                </em>
                                <em class="desc">The engine creates a video of the specific portrait image mirroring the facial motions from the input video.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/avatar/enAvatarMain">Start Now</a>
                            </span>
                        </li>--%>

                        <%--<li class="vis">
                            <span class="imgBox img_vis_fr">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_6_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Face Recognition</strong>
                                    Face Recognition
                                </em>
                                <em class="desc">Vectorize human faces into 512 dimensions and compare to
                                    authorize.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/fr/enFaceRecogMain">Start Now</a>
                            </span>
                        </li>--%>
                        <li class="vis">
                            <span class="imgBox img_vis_esr">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_5_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>ESR</strong>
                                    Enhanced Super Resolution
                                </em>
                                <em class="desc">Enlarges the size of the given while minimizing the loss
                                    with deep learning technology.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/superResolution/enSuperResolutionMain">Start
                                    Now</a>
                            </span>
                        </li>

                        <%--<li class="vis">
                            <span class="imgBox img_vis_vsr">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_vsr_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>VSR</strong>
                                    Video Super Resolution
                                </em>
                                <em class="desc">Doubles the resolution of low-quality videos.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/vsr/enVsrMain">Start Now</a>
                            </span>
                        </li>--%>
                        <%--<li class="vis">
							<span class="point"><em>NEW</em></span>
                            <span class="imgBox img_vis_hlst3D">product image</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/kr/images/ico_vis_hlst3D_w.svg"
                                                     alt="engine icon"></em>
								<em class="name">
									<strong>Holistic 3D</strong>
									Holistic 3D
								</em>
								<em class="desc">3D Model Synthesis from a 360 degree view panoramic image.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/holistic3d/enHolistic3dMain">Start Now</a>
							</span>
                        </li>--%>
                        <li class="vis">
                            <span class="imgBox img_vis_tti">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_2_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>AI Styling</strong>
                                    Text-to-Image for fashion
                                </em>
                                <em class="desc">Text-to-image for fashion; if you enter a text describing a
                                    look, the image of the look is created.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/tti/enTtiMain">Start Now</a>
                            </span>
                        </li>
                        <%--<li class="vis">
                            <span class="imgBox img_vis_tr">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_1_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Text Removal</strong>
                                    Text Removal
                                </em>
                                <em class="desc">Finds and removes text from original images.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/textRemoval/enTextRemovalMain">Start Now</a>
                            </span>
                        </li>--%>
                        <li class="vis">
                            <span class="imgBox img_vis_sr">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_10_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Subtitles Recognition</strong>
                                    Subtitle Recognition
                                </em>
                                <em class="desc">AI engine that can recognize and extract subtitles.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/subtitleRecog/enSubtitleRecogMain">Start Now</a>
                            </span>
                        </li>
                        <li class="vis">
                            <span class="imgBox img_vis_ipr">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_7_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Pose Recognition</strong>
                                    Pose Recognition
                                </em>
                                <em class="desc">The engine recognizes body positions of people in an image
                                    and renders accurate visualizations.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/poseRecog/enPoseRecogMain">Start Now</a>
                            </span>
                        </li>
                        <li class="vis">
                            <span class="imgBox img_vis_ft">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_8_fold_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>Face Tracking</strong>
                                    Face Tracking
                                </em>
                                <em class="desc">Engine which can recognize and extract faces from a video.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/faceTracking/enFaceTrackingMain">Start Now</a>
                            </span>
                        </li>
                        <li class="vis">
                            <span class="imgBox img_vis_fd">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_4_fold_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>Face Detection</strong>
                                    Face Detection
                                </em>
                                <em class="desc">After a human face is recognized and detected, it is made unidentifiable.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/avr/enFaceDetectionMain">Start Now</a>
                            </span>
                        </li>
                        <%--<li class="vis">
							<span class="point">
								<em>NEW</em>
							</span>
                            <span class="imgBox img_vis_anmFc">product image</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_animeFace_w.svg"
                                                     alt="engine icon"></em>
								<em class="name">
									<strong>Anime Face</strong>
									Anime Face
								</em>
								<em class="desc">Generate an animated version of your face, customize, style and
									decorate.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/animeFace/enAnimeFaceMain">Start Now</a>
							</span>
                        </li>--%>
                        <li class="vis">
                            <span class="imgBox img_vis_hs">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_hair_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>Hair Segmentation</strong>
                                    Hair Segmentation
                                </em>
                                <em class="desc">Recognizes and displays the area of hair in the image while classifying the color.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/hairSegmentation/enHairSegmentationMain">Start Now</a>
                            </span>
                        </li>
                        <li class="vis">
                            <span class="imgBox img_vis_cmad">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_cmad_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>Clothing Multi-Attributes Detection</strong>
                                    Clothing Multi-Attributes Detection
                                </em>
                                <em class="desc">Recognizes the characteristics of the person's clothing in the image and displays the results. Currently, the recognition rate is high only for the elderly.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/clothingDetection/enclothingDetectionMain">Start Now</a>
                            </span>
                        </li>
                        <li class="vis">
                            <span class="imgBox img_vis_lpr">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_4_fold_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>License Plate Recognition</strong>
                                    License Plate Recognition
                                </em>
                                <em class="desc">It detects the location of the license plate of a moving vehicle and recognizes the number.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/avr/enPlateRecogMain">Start Now</a>
                            </span>
                        </li>
                        <li class="vis">
                            <span class="imgBox img_vis_wd">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_windshield_white.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>Windshield Detection</strong>
                                    Windshield Detection
                                </em>
                                <em class="desc">Recognizes and de-identifies the location of the windshield of a vehicle on the road.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/avr/enAvrMain">Start Now</a>
                            </span>
                        </li>
                        <li class="vis">
                            <span class="imgBox img_vis_ad">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_9_fold_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>Reverse Direction Detection</strong>
                                    Reverse Direction Detection
                                </em>
                                <em class="desc">It detects the reverse driving of a walking person and informs the start point of the change of direction.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/anomalyReverse/enAnomalyReverseMain">Start Now</a>
                            </span>
                        </li>
                        <li class="vis">
                            <span class="point"><em>NEW</em></span>
                            <span class="imgBox img_vis_litrDtc">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_loiteringDetect_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>Loitering Detection</strong>
                                    Loitering Detection
                                </em>
                                <em class="desc">By specifying an area, you will be notified when someone wanders the area for a long time.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/anomalyLoitering/enAnomalyLoiteringMain">Start Now</a>
                            </span>
                        </li>
                        <li class="vis">
                            <span class="point"><em>NEW</em></span>
                            <span class="imgBox img_vis_fdDtc">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_fallDownDetect_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>Fall Down Detection</strong>
                                    Fall Down Detection
                                </em>
                                <em class="desc">Recognizes people fainting in a specific area.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/anomalyFalldown/enAnomalyFalldownMain">Start Now</a>
                            </span>
                        </li>
                        <li class="vis">
                            <span class="point"><em>NEW</em></span>
                            <span class="imgBox img_vis_itrsDtc">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_IntrusionDetect_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>Intrusion Detection</strong>
                                    Intrusion Detection
                                </em>
                                <em class="desc">In a specific area, you will be notified when someone breaks into the zone.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/anomalyIntrusion/enAnomalyIntrusionMain">Start Now</a>
                            </span>
                        </li>
                        <%--<li class="vis">
                            <span class="imgBox img_vis_bp">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_vis_positioning_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>Bracket Positioning</strong>
                                    Bracket Positioning
                                </em>
                                <em class="desc">The engine selects the location to attach the bracket on a given tooth.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/positioning/enPositioningMain">Start Now</a>
                            </span>
                        </li>--%>

                    </ul>
                </div>
                <!-- 시각엔진 -->
                <!-- 언어엔진 -->
                <div id="eg_contents04"
                     class="eg_contents">
                    <ul class="lst">
                        <!-- 음성:spe, 시각:vis, 언어:lang, 분석:ana, 대화:talk, 영어교육:eng  -->

                        <li class="lang">
                            <span class="imgBox img_lang_cor">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_lan_correct_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>Bert Correction</strong>
                                    Bert Correction
                                </em>
                                <em class="desc">Fixes incorrect Korean sentences according to the appropriate context.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/correction/enCorrectionMain">Start Now</a>
                            </span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_kon">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_lan_convers_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>Konglish</strong>
                                    Hangul Conversion
                                </em>
                                <em class="desc">Converts English words, or the English words mixed in a Korean sentence, into the closest possible Hangul notation.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/conversion/enConversionMain">Start Now</a>
                            </span>
                        </li>

                        <li class="lang">
                            <span class="imgBox img_lang_nlu">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_lan_1_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>NLU</strong>
                                    Natural Language Understanding
                                </em>
                                <em class="desc">Provides the stemming results from the lemmatization of the
                                    given sentence.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/nlu/enNluMain">Start Now</a>
                            </span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_mrc">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_lan_2_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>MRC</strong>
                                    Machine Reading Comprehension
                                </em>
                                <em class="desc">Provide the most relevant answers by comprehending the
                                    whole passage.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/mrc/enMrcMain">Start Now</a>
                            </span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_xdc">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_lan_3_fold_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>XDC</strong>
                                    eXplainable Document Classifier
                                </em>
                                <em class="desc">Regardless of categories or length of the article, it accurately categorizes the documents.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/xdc/enXdcMain">Start Now</a>
                                <a href="https://youtu.be/e6xq912p_5Y" target="_blank"><em class="fab fa-youtube"></em>Tutorial</a>
                            </span>
                        </li>
<%-- 아래 clickbait 링크 넣어야됨!!!!--%>
                        <%--<li class="lang">
							<span class="point"><em>NEW</em></span>
                            <span class="imgBox img_lang_clbt">product image</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_lan_clickbait_w.svg" alt="engine icon"></em>
								<em class="name">
									<strong>Clickbait News</strong>
									Clickbait News
								</em>
								<em class="desc">If the title of the news and the content of the body are different, it determines whether it is a clickbait news.</em>
							</span>
                            <span class="btnBox">
								<a href="해당 페이지 링크 넣어주세요">Start Now</a>
							</span>
                        </li>--%>
                        <li class="lang">
                            <span class="imgBox img_lang_gpt">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_lan_5_fold_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>GPT-2</strong>
                                    Generative Pre-Training
                                </em>
                                <em class="desc">Generates multiple sentences related to the input text.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/gpt/enGptMain">Start Now</a>
                            </span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_hmd">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_lan_6_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>HMD</strong>
                                    Hierarchical Multiple Dictionary
                                </em>
                                <em class="desc">Identifies and classifies the emotions of sentences through
                                    the patterns expressed.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/hmd/enHmdMain">Start Now</a>
                            </span>
                        </li>
                        <li class="lang">
                            <span class="imgBox img_lang_itf">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_lan_itf_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>ITF</strong>
                                    Intent Finder
                                </em>
                                <em class="desc">Identify the intent of the question.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/itf/krItfMain">Start Now</a>
                            </span>
                        </li>
                        <li class="lang">
                            <span class="point"><em>NEW</em></span>
                            <span class="imgBox img_lang_sts">product image</span>
                            <span class="eg_info">
								<em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_lan_txtStyTrans_w.svg" alt="engine icon"></em>
								<em class="name">
									<strong>Text Style Transfer</strong>
									Text Style Transfer
								</em>
								<em class="desc">It changes the sentence to the desired style while conveying the meaning consistently.</em>
							</span>
                            <span class="btnBox">
								<a href="/cloudApi/styleTransfer/enStyleTransferMain">Start Now</a>
							</span>
                        </li>
                    </ul>
                </div>
                <!-- 언어엔진 -->


                <!-- 분석엔진 -->
                <div id="eg_contents05"
                     class="eg_contents">
                    <ul class="lst">
                        <!-- 음성:spe, 시각:vis, 언어:lang, 분석:ana, 대화:talk, 영어교육:eng  -->
                        <li class="ana">
                            <span class="imgBox img_ana_data">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_ana_data_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Correlation Analysis</strong>
                                    Data Analysis
                                </em>
                                <em class="desc">Various algorithms to analyze data correlations.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/dataAnalysis/enDataAnalysis">Start Now</a>
                            </span>
                        </li>
                    </ul>
                </div>
                <!-- 분석엔진 -->
                <!-- 대화엔진 -->
                <div id="eg_contents06"
                     class="eg_contents">
                    <ul class="lst">
                        <!-- 음성:spe, 시각:vis, 언어:lang, 분석:ana, 대화:talk, 영어교육:eng  -->
                        <%--<li class="talk">
                            <span class="imgBox img_talk_nqa">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_bot_1_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>NQA Bot</strong>
                                </em>
                                <em class="desc">Our original chatbot using Natural Q&amp;A algorithms.
                                    Currently provides chatbot service within financial area.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/nqa/enNqaMain">Start Now</a>
                                <a href="https://youtu.be/adEiXM_Vn0U"
                                   target="_blank"><em class="fab fa-youtube"></em>Tutorial</a>
                            </span>
                        </li>--%>
                        <li class="talk">
                            <span class="imgBox img_talk_chat">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_bot_2_fold_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>Ready-made Bots</strong>
                                </em>
                                <em class="desc">Try our Wiki, News<!--, Weather--> Bot for your own needs.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/chatbot/enChatbotMain">Start Now</a>
                            </span>
                        </li>
                        <%--<li class="talk">
                            <span class="imgBox img_talk_kbqa">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_bot_2_fold_w.svg" alt="engine icon"></em>
                                <em class="name">
                                    <strong>KBQA Bots</strong>
                                </em>
                                <em class="desc">Infobox for Wikipedia is searched and answered using KBQA engine.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/chatbot/enKbqaMain">Start Now</a>
                            </span>
                        </li>--%>
                        <li class="talk">
                            <span class="imgBox img_talk_hotel">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_bot_3_fold_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Hotel Concierge Chatbot</strong>
                                </em>
                                <em class="desc">A chatbot to handle all kinds of hotel services incluuding
                                    inquiries and concierge.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/hotelbot/enHotelBot">Start Now</a>
                            </span>
                        </li>
                    </ul>
                </div>
                <!-- 대화엔진 -->
                <!-- 영어교육 -->
                <div id="eg_contents07"
                     class="eg_contents">
                    <ul class="lst">
                        <!-- 음성:spe, 시각:vis, 언어:lang, 분석:ana, 대화:talk, 영어교육:eng  -->
                        <li class="eng">
                            <span class="imgBox img_eng_stt">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_eng_1_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Speech to Text</strong>
                                    for English Education
                                </em>
                                <em class="desc">Recognizes the student's speech in English, and assesses
                                    the accuracy.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/engedu/enEngeduSttMain">Start Now</a>
                            </span>
                        </li>
                        <li class="eng">
                            <span class="imgBox img_eng_edu">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_eng_2_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Pronunciation Scoring</strong>
                                    Pronunciation Scoring
                                </em>
                                <em class="desc">Assesses the pronunciation of the student's English, by
                                    comparing it to that of a native speaker.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/engedu/enEngeduPronMain">Start Now</a>
                            </span>
                        </li>
                        <li class="eng">
                            <span class="imgBox img_eng_pa">product image</span>
                            <span class="eg_info">
                                <em class="ico"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_eng_3_w.svg"
                                                     alt="engine icon"></em>
                                <em class="name">
                                    <strong>Phonics Assessment</strong>
                                    Phonics Assessment
                                </em>
                                <em class="desc">Evaluates the phonics in the student's speech by specifying
                                    the phonemes in the required words.</em>
                            </span>
                            <span class="btnBox">
                                <a href="/cloudApi/engedu/enEngeduPhonicsMain">Start Now</a>
                            </span>
                        </li>
                    </ul>
                </div>
                <!-- 영어교육 -->
            </div>
        </div>
        <!-- //egArea -->
	</div>
</div>
<!-- //egArea -->
<%--<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/main.js"></script>--%>
<%--<script type="text/javascript">--%>
<%--	$(window).load(function() {--%>
<%--		$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {--%>
<%--			$(this).remove();--%>
<%--		});--%>
<%--		$('.list1').trigger('click');--%>
<%--		$('.list2').trigger('click');--%>
<%--	});--%>
<%--</script>--%>


<script type="text/javascript"
        src="${pageContext.request.contextPath}/aiaas/en/js/main.js"></script>
<!-- page Landing -->
<script type="text/javascript">
    $(window).load(function () {
        //page loading delete
        $('#pageldg').addClass('pageldg_hide').delay(300).queue(function () { $(this).remove(); });
    });
</script>
<script type="text/javascript">
    // jQuery(function () {
    //     jQuery("a.btn_movLayer").movLayer();
    // });

    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {
            $('.list1').trigger('click');
            $('.list2').trigger('click');

        });
    });
</script>
<script type="text/javascript">

    //	체크박스 한개만 선택
    function oneCheckbox(chk) {

        var obj = document.getElementsByName("select");

        for (var i = 0; i < obj.length; i++) {
            if (obj[i] != chk) {
                obj[i].checked = false;
            }
        }
    }
</script>