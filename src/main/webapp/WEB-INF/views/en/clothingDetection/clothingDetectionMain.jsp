<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020/11/24
  Time: 11:38 오전
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<!-- 5 .pop_simple -->
<div class="pop_simple" id="api_upload_fail_popup">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">close</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>Upload error</h5>
            <p>Error occurred due to the image or file size. <br>Please check the restriction again.</p>
            <span>
                * Supported files: .jpg<br>
                * Image capacity : 10MB or less<br>
                * Image size : 240x240 px or higher<br>
                * Must be at least one person in the image.<br>
                * Upper/lower/whole body are recognizable.<br>
            </span>
        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">OK</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .content -->
<div class="content api_content">
    <h1 class="api_tit">Clothing Multi-Attributes Detection</h1>
    <ul class="menu_lst vision_lst">
        <li class="tablinks" onclick="openTap(event, 'avrdemo')" id="defaultOpen">
            <button type="button">AI Engine</button>
        </li>
        <li class="tablinks" onclick="openTap(event, 'avrexample')">
            <button type="button">Use Case</button>
        </li>
        <li class="tablinks" onclick="openTap(event, 'avrmenu')">
            <button type="button">Manual</button>
        </li>
    </ul>

    <!-- .avrdemo -->
    <div class="demobox clothSegmentation" id="avrdemo">
        <p>
            <span>Clothing Multi-Attributes Detection</span></small>
        </p>
        <span class="sub">
                        Recognizes the characteristics of the person's clothing in the image and displays the results.<br>
                        Currently, the recognition rate is high only for <strong style="color:#f7768a;">the elderly</strong>.
                    </span>

        <!--demo_layout-->
        <div class="demo_layout">
            <!--avr_1-->
            <div class="avr_1">
                <div class="fl_box">
                    <p>
                        <em class="far fa-file-image"></em>
                        <strong>Use Sample File</strong>
                    </p>

                    <div class="sample_box">
                        <div class="sample_1">
                            <div class="radio">
                                <input type="radio" id="sample1" name="avr_option" value="plate" checked>
                                <label for="sample1" class="female">
                                    <div class="img_area">
                                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_ClotingSample.jpeg" alt="sample img 1">
                                    </div>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="fr_box">
                    <p>
                        <em class="far fa-file-image"></em>
                        <strong>Use My File</strong>
                    </p>

                    <div class="uplode_box">
                        <div class="btn" id="uploadFile">
                            <em class="fas fa-times hidden close"></em>
                            <em class="far fa-file-image hidden"></em>
                            <label for="demoFile" class="demolabel">Upload File</label>
                            <input type="file" id="demoFile" class="demoFile" accept=".jpg, .png">
                        </div>

                        <ul>
                            <li>* Supported files: .jpg</li>
                            <li>* Image capacity : 10MB or less</li>
                            <li>* Image size : 240x240 px or higher</li>
                            <li>* Must be at least one person in the image.</li>
                            <li>* Upper/lower/whole body are recognizable.</li>
                        </ul>
                    </div>
                </div>

                <div class="btn_area">
                    <button type="button" class="btn_start" id="sub">Process</button>
                </div>
            </div>
            <!--avr_1-->

            <!--avr_2-->
            <div class="avr_2">
                <p><em class="far fa-file-image"></em>In Progress</p>

                <div class="loding_box ">
                    <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve">
                                    <path fill="#fcc6ce" fill-opacity="0.42"
                                          d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z" />
                        <g>
                            <path fill="#f7778a" fill-opacity="1"
                                  d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z" />
                            <animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms"
                                              repeatCount="indefinite" />
                        </g>
                                </svg>
                    <p>AI processing takes a while...</p>
                </div>

                <div class="btn_area">
                    <button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
                </div>
            </div>
            <!--avr_2-->

            <!--avr_3-->
            <div class="avr_3">
                <p class="tit"><em class="far fa-file-image"></em>Input</p>
                <div class="origin_file file_box">
                    <div class="imgBox">
                        <img id="input_img" src="${pageContext.request.contextPath}/aiaas/common/images/img_ClotingSample.jpeg" alt="input img">
                    </div>
                </div>

                <p class="tit"><em class="far fa-file-image"></em>Result</p>
                <div class="result_file file_box">
                    <div class="result_box">
                        <div class="imgBox">
                            <img id="output_img" src="${pageContext.request.contextPath}/aiaas/common/images/img_ClotingSample.jpeg" alt="result img">
                        </div>
                    </div>

                    <div class="result_box">
                        <ul class="cmt_list">
<%--                            <li>--%>
<%--                                <dl>--%>
<%--                                    <dt>Person 1</dt>--%>
<%--                                    <dd>purple no pattern man autumn vest no sleeves gray dotted woman summer long skirt standinglong skirt standinglong skirt standing</dd>--%>
<%--                                </dl>--%>
<%--                            </li>--%>
<%--                            <li>--%>
<%--                                <dl>--%>
<%--                                    <dt>Person 2</dt>--%>
<%--                                    <dd>purple no pattern man autumn vest no sleeves gray dotted woman summer long skirt standing</dd>--%>
<%--                                </dl>--%>
<%--                            </li>--%>
<%--                            <li>--%>
<%--                                <dl>--%>
<%--                                    <dt>Person 3</dt>--%>
<%--                                    <dd>purple no pattern man autumn vest no sleeves gray dotted woman summer long skirt standing</dd>--%>
<%--                                </dl>--%>
<%--                            </li>--%>
                        </ul>
                    </div>
                </div>

                <div class="btn_area">
                    <button type="button" class="btn_back2"><em class="fas fa-redo"></em>Reset</button>
                </div>
            </div>
            <!--avr_3-->
        </div>
        <!--// demo_layout-->

        <div class="engineInfo">
            <img src="${pageContext.request.contextPath}/aiaas/common/images/logo_air.png" alt="AIR logo">
            <img src="${pageContext.request.contextPath}/aiaas/common/images/logo_etri.png" alt="ETRI logo">
            <p>This engine is the result of a research project of the Korea Electronics and Telecommunications Research Institute (ETRI) among the government's R&D project &ldquo;Real-Environment Human Care Robot Technology Development (AIR) to Repond to the Aged Society&rdquo; supported by the Ministry of Science, Technology and Communication of the Republic of Korea.</p>
        </div>
    </div>
    <!-- //.avrdemo -->

    <!--.avrmenu-->
    <div class="demobox vision_menu" id="avrmenu">
        <!--guide_box-->
        <div class="guide_box">
            <div class="guide_common">
                <div class="title">API Guideline</div>

                <p class="sub_title">Set up Environment</p>
                <p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
                <p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
                <p class="sub_title">ID & Key</p>
                <p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
                <p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                <p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in.<br>
                    <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                <p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                <p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
                <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
            </div>

            <div class="guide_group">
                <div class="title">Clothing Multi-Attributes Detection</div>

                <p class="sub_txt">Recognizes the characteristics of the person's clothing in the image and displays the results.</p>
                <span class="sub_title">Preparation</span>
                <p class="sub_txt">- Input: Image File</p>
                <ul>
                    <li>Supported files : .jpg</li>
                    <li>Image capacity: 10MB or less</li>
                    <li>Image size : 240x240 px or higher</li>
                    <li>Must be at least one person in the image.</li>
                    <li>Upper/lower/whole body are recognizable.</li>
                </ul>
                <span class="sub_title">API Document</span>
                <p class="sub_txt">① Request </p>
                <ul>
                    <li>Method : POST</li>
                    <li>URL : https://api.maum.ai/feat/getClothFeature</li>
                </ul>
                <p class="sub_txt">② Request parameters</p>
                <table>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>type</th>
                    </tr>
                    <tr>
                        <td>apiId </td>
                        <td>Unique API ID. Request from is required for the ID.</td>
                        <td>string</td>
                    </tr>
                    <tr>
                        <td>apiKey </td>
                        <td>Unique API key. Request from is required for the key.</td>
                        <td>string</td>
                    </tr>
                    <tr>
                        <td>image</td>
                        <td>type : Image File(.jpg)</td>
                        <td>string</td>
                    </tr>
                </table>
                <p class="sub_txt">③ Request example </p>
                <div class="code_box">
                                <pre>
curl --location --request POST
'https://api.maum.ai/feat/getClothFeature' \
--header 'Content-Type: multipart/form-data' \
--form 'apiId= Own API Id' \
--form 'apiKey= Own API Key' \
--form 'image= Image file for clothing recognition'
</pre>
                </div>

                <p class="sub_txt">④ Response example </p>

                <div class="code_box">
                                <pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "payload": {
        "metaData": [
            "key": 1,
              "infoSentence": "pink no-pattern man summer shirt
               short-sleeves  //  brown no-pattern long pants"
        ],
        "resultImage": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",
    }
}
</pre>
                </div>
            </div>
        </div>
    </div>
    <!--//avrmenu-->

    <!--.avrexample-->
    <div class="demobox" id="avrexample">
        <p><em style="color:#f7778a;font-weight: 400;">Use Cases</em></p>
        <span class="sub">Find out how AI can be applied to diverse areas.</span>

        <!-- 의상/패션 검색용 엔진 -->
        <div class="useCasesBox">
            <ul class="lst_useCases">
                <li>
                    <dl>
                        <dt>
                            <em>CASE 01</em>
                            <span>Clothing/Fashion<strong>search engine</strong></span>
                        </dt>
                        <dd class="txt">Used for extracting information from clothing/fashion search engines.</dd>
                    </dl>
                </li>
            </ul>
        </div>
        <!-- //의상/패션 검색용 엔진 -->
    </div>
    <!--//.avrexample-->
</div>
<!-- //.content -->

<script type="text/javascript">
    var request = null;
    var sample1File;
    var sample1SrcUrl;

    //파일명 변경
    document.querySelector("#demoFile").addEventListener('change', function (ev) {

        let file = this.files[0];

        if (file === undefined || file === null || file == "") { return; }

        let fileName = file.name;
        let fileSize = file.size;
        let maxSize = 1024 * 1024 * 10; //10MB

        if (!file.type.match('image.jp*')){
            console.log("It is not a .jpg or .png file.");
            this.value = null;
            $('#api_upload_fail_popup').fadeIn(300);

        } else if(fileSize > maxSize) {
            console.log("Please upload a file less than 10MB.");
            this.value = null;
            $('#api_upload_fail_popup').fadeIn(300);

        } else {
            let size = (fileSize / 1048576).toFixed(3); //size in mb
            $('input[type="radio"]:checked').prop("checked", false);
            $('.demolabel').text(fileName + ' (' + size + 'MB)');
            $('#uploadFile').removeClass('btn').addClass('btn_change');
            $('.fl_box').css("opacity", "0.5");
        }

    });


    jQuery.event.add(window,"load",function(){

        //샘플
        function loadSample1() {
            let blob = null;
            let xhr = new XMLHttpRequest();
            xhr.open("GET", "/aiaas/common/images/img_ClotingSample.jpeg");
            xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
            xhr.onload = function()
            {
                blob = xhr.response;//xhr.response is now a blob object
                sample1File = new File([blob], "img_ClotingSample.jpeg");
                sample1SrcUrl = URL.createObjectURL(blob);
            };

            xhr.send();
        }

        $(document).ready(function (){

            loadSample1();

            $('.radio label').on('click',function(){
                $('.fl_box').attr('opacity',1);
                $('em.close').click();
            });

            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });

            // step1->step2  (close button)
            $('em.close').on('click', function () {
                $('#demoFile').val(null);
                $('.demolabel').text('Upload File');
                $(this).parent().removeClass("btn_change").addClass("btn");
                $('.fl_box').css("opacity", "1");
            });


            // 처음으로 버튼
            $('.btn_back1').on('click', function () {
                request.abort();
                $('.avr_2').hide();
                $('.avr_1').fadeIn(300);
            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('.avr_3').hide();
                $('.avr_1').fadeIn(300);
                $('.cmt_list li').remove();
            });

            //결과보기 버튼
            $('#sub').on('click',function (){

                let $checked = $('input[type="radio"]:checked');
                let $demoFile = $("#demoFile");
                let formData = new FormData();
                let inputFile;
                let url;

                //내 파일로
                if ($checked.length === 0) {
                    if ($demoFile.val() === "" || $demoFile.val() === null) {
                        alert("Please select a sample or upload a file.");
                        return 0;
                    }
                    inputFile = $demoFile.get(0).files[0];
                    url = URL.createObjectURL(inputFile);
                }
                //샘플로
                else {
                    url = sample1SrcUrl;
                    inputFile = sample1File;
                }

                formData.append('image',inputFile);
                formData.append('${_csrf.parameterName}', '${_csrf.token}');

                $('#input_img').attr('src', url);
                $('.avr_1').hide();
                $('.avr_2').fadeIn(300);

                request = $.ajax({
                    type: "POST",
                    async: true,
                    url: '/api/feat/getClothFeature',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(result){
                        var resultList = $('.cmt_list')

                        /*결과 오류*/
                        if(result === undefined || result === ""){
                            setResultError_UI();
                            showResultDiv_UI();
                            resultList.append('<li><dl><dt style="color:#f7778a;">[Recognition error]</dt><dd> Please select an image again.</dd></dl></li>');
                            $('.clothSegmentation .avr_3 .result_file .result_box:nth-child(1) .imgBox').css('height','auto');
                            return;
                        }

                        let resultData = JSON.parse(result);

                        /*결과 정상*/
                        var metaData = resultData.payload.metaData;

                        console.log(metaData);
                        if(metaData == undefined){
                            $('.avr_2').hide();
                            resultList.append('<li><dl><dt style="color:#f7778a;">[Recognition error]</dt><dd> Please select an image again.</dd></dl></li>');
                            $('.clothSegmentation .avr_3 .result_file .result_box:nth-child(1) .imgBox').css('height','auto');
                        }else if (metaData.length >= 1){
                            for(var i=0; i < metaData.length; i++){
                                resultList.append('<li><dl><dt>Person '+metaData[i].key+'</dt><dd>'+metaData[i].infoSentence+'</dd></dl></li>');
                            }
                        }
                        $('#output_img').attr('src', "data:image/jpeg;base64," + resultData.payload.resultImage);
                        showResultDiv_UI();
                    },

                    error: function(jqXHR, error){
                        if(jqXHR.status === 0){
                            return false;
                        }
                        alert("There is no connection to the server.\nPlease try again later.");
                        console.dir(error);
                        window.location.reload();
                    }
                });
            });
        });
    });


    function downloadResultImg(){
        let img = document.getElementById('output_img');
        let link = document.getElementById("save");
        link.href = img.src.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
        link.download = "ClothingDetection.jpg";
    }

    function setResultError_UI(){
        $('#output_img').attr('src',"");
    }

    function showResultDiv_UI(){
        $('.avr_1').hide();
        $('.avr_2').hide();
        $('.avr_3').fadeIn(300);
    }

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

</script>
