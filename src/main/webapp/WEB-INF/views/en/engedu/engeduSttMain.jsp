<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
    <div class="content api_content">
        <h1>영어 교육용 STT</h1>

        <ul class="menu_lst eng_lst">
            <li class="tablinks" onclick="openTap(event, 'edustt_demo')" id="defaultOpen">
                <button type="button">AI Engine</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'edustt_example')">
                <button type="button">Use Case</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'edustt_menu')">
                <button type="button">Manual</button>
            </li>
<%--            <li class="tablinks"><a href="/member/enApiAccount">API ID, key</a></li>--%>
        </ul>
        <!-- [신규] 20190809 서비스개발팀 YMJ 영어교육용STT 콘텐츠 -->
        <div class="demobox en_demobox" id="edustt_demo">
            <p><span>STT for English Education</span></p>
            <span class="sub">Evaluates learner’s spoken English and gives feedback.</span>
            <!-- demoBox -->
            <div class="demoBox">
                <!-- commBox -->
                <div class="commBox">
                    <p class="commBox_txt">Click the button <img src="/aiaas/kr/images/img_mic_orange.png" alt="microphone"> to activate the microphone then read the sentence below.
                    </p>
                    <!-- stepBox -->
                    <ul class="stepBox">
                        <li id="assStep">
                            <ul class="nav">
                                <li class="active">Step 1</li>
                                <li class="disabled">Step 2</li>
                                <li class="disabled">Step 3</li>
                            </ul>
                            <div class="questionBox">
                                <p class="sound">
                                    <!-- [D] 오디오파일 링크은 하단 javascript에 넣어주세요. -->
                                    <button type="button" id="oriAudio1" class="btn_oriPlay" title="play">Play
                                    </button>
                                </p>
                                <p class="txt_en"><span>I went to school.</span></p>
<%--                                <p class="txt_kr">나는 학교에 갔어요.</p>--%>
                            </div>
                            <div class="answerBox">
                                <p class="txt">
                                    <span><!--스트리밍 결과값--><button type="button" id="recordAudio" class="btn_recordPlay"
                                                                 title="play">Play</button></span>
                                </p>
                                <dl class="assResult">
                                    <dt>Accuracy</dt>
                                    <dd>00점</dd>
                                </dl>
                            </div>
                            <div class="btnBox">
                                <!-- 버튼_평가전 -->
                                <p class="assBefore"><!-- 평가 완료되면 display:none; 스타일 선언 -->
                                    <button type="button" class="btn_orange btn_ass"><span>Evaluation</span></button>
                                </p>
                                <!-- //버튼_평가전 -->
                                <!-- 버튼_평가후 -->
                                <p class="assAfter"><!-- 평가 완료되면 display:block; 스타일 선언 -->
                                    <button type="button" class="btn_ass_again"><em class="fas fa-retweet"></em>Try Again</button>
                                    <button type="button" id="retry_btn" class="btn_orange btn_ass_next">
                                        <em class="fas fa-arrow-right"></em>Next Sentence
                                    </button>
                                </p>
                                <!-- //버튼_평가후 -->
                            </div>
                        </li>
                    </ul>
                    <!-- //stepBox -->
                </div>
                <!-- //commBox -->
            </div>
            <!-- //demoBox -->
        </div>
        <!-- //[신규] 20190809 서비스개발팀 YMJ 영어교육용STT 콘텐츠-->

        <!--.pron_menu-->
        <div class="demobox eng_menu" id="edustt_menu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API Guideline
                    </div>
                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID &amp; Key</p>
                    <p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                    <p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                    <p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        STT for English Education
                    </div>
                    <p class="sub_txt">STT specially used for English education.</p>
                    <span class="sub_title">Preparation</span>
                    <p class="sub_txt">① Input: Sound file (.wav/.pcm)</p>
                    <ul>
                        <li>File type: .wav, .pcm</li>
                        <li>Sample rate: 16K</li>
                        <li>Channel: mono</li>
                    </ul>
                    <p class="sub_txt">① Input: Answer Text </p>
                    <span class="sub_title">API Document</span>
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/edueng/stt</li>
                    </ul>
                    <p class="sub_txt">② Request parameters </p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API ID. Request from is required for the ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>userId</td>
                            <td>User ID</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>model</td>
                            <td>Selected model (baseline)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>answerText</td>
                            <td>The answer of the text</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>file</td>
                            <td>Input file (.wav, .pcm) 16K mono</td>
                            <td>file</td>
                        </tr>

                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
                        curl -X POST \ <br>
                        https://api.maum.ai/edueng/stt \<br>
                        -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \<br>
                        -F 'apiId= (*Own API ID)' \<br>
                        -F 'apiKey= (*Own API Key)' \<br>
                        -F 'userId= speaker1' \<br>
                        -F 'model= baseline' \<br>
                        -F 'answerText=What are you doing?'  \<br>
                        -F 'file=@eng_16k_1.wav' \<br>
                    </div>

                    <p class="sub_txt">④ Response parameters</p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>resultText</td>
                            <td>STT result</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>resCode</td>
                            <td>resCode API response (Success:200)</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>answerText</td>
                            <td>Answer sentence</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>recordUrl</td>
                            <td>Inputted file URL</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>sentenceScore</td>
                            <td>Score comparison between resultText and answerText </td>
                            <td>int</td>
                        </tr>
                    </table>
                    <p class="sub_txt">⑤ Response example </p>
                    <div class="code_box">
                        <pre>
{
    "result": {
        "resultText": "what are you doing",87
        "resCode": 200,
        "answerText": "what are you doing?",
        "recordUrl": "http://maieng.maum.ai:7776/record/engedu.api/apiId/userId/202002/20200210_143106_60.mp3",
        "sentenceScore": 100
    }
}
                        </pre>
                    </div>
                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--//pron_menu-->
        <!--.pron_example-->
        <div class="demobox" id="edustt_example">
            <p><em style="font-weight: 400; color: #fb8923;">Use Cases</em></p>
            <span class="sub">Find out how AI can be applied to diverse area.</span>
            <div class="useCasesBox">
                <p style="font-size:15px;">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>English Speaking</span>
<%--                                <span style="font-size:16px">(Speaking 활동)</span>--%>
                            </dt>
                            <dd class="txt">Repeat after the given sentence and accuracy can be judged by comparing STT results.
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_engeduStt"><span>engedu STT</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>Read Aloud English</span>
<%--                                <span style="font-size:16px">(Read Aloud 활동)</span>--%>
                            </dt>
                            <dd class="txt">Stories can be listened to then read aloud. The STT can give feedback.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_engeduStt"><span>engedu STT</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>Conversation</span>
                            </dt>
                            <dd class="txt">An English conversation can be held question and answer style.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_engeduStt"><span>engedu STT</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 04</em>
                                <span>TOEIC Speaking,<br> OPIc, and more</span>
                            </dt>
                            <dd class="txt"> Practice answering a questing or giving a presentation, for learners to see their speech transcribed.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_engeduStt"><span>engedu STT</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
                </p>
            </div>

        </div>
        <%--pron_example--%>

    </div>
    <!-- //.content -->
</div>

<script type="text/javascript">

    // $('.category_1').trigger("click");

    $(window).load(function () {
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function () {
            $(this).remove();
        });

    });
</script>

<script src="https://${engedu_host}/front/resources/static/js/maiEng-v1${engedu_surfix}.min.js"></script>

<script type="text/javascript">

    var stepNum = 0; //현재 스텝이 어디인지 표시
    var toggleFlag = false; //min 상태 표시

    function onResult_Stt(result) {

        showResult(result.userText, result.recordUrl, result.score);

        $('.btnBox .assBefore').css('display', 'none');
        $('.btnBox .assAfter').css('display', 'block');

        $('.btn_ass').removeClass('playing');
        toggleFlag = false;
    }

    function onFail(reason) {

    }

    function showResult(answerText, recordUrl, score) {

        recordUrl = recordUrl.replace(".wav", ".mp3");
        audioTemp = new Audio(recordUrl);

        $('.answerBox .txt span').html(answerText + "&nbsp;&nbsp;"
            + "<button type='button' id='recordAudio_n' class='btn_recordPlay' title='파일 재생' >Play</button>");

        $('#recordAudio_n').click(function () {
            audioTemp.play();
        });

        $('.answerBox .assResult dd').text(score + " Points");

        $('.answerBox span').css('display', 'table-cell');
        $('.btn_recordPlay').css('display', 'inline-block');

        $('.answerBox .assResult').css('display', 'block');
        $('.answerBox .assResult').show();
    }

    $(document).ready(function () {

        var sentence = ["I went to school.", "I went to school yesterday.", "I went to school yesterday with my brother."];
        var translation = ["나는 학교에 갔어요.", "나는 어제 학교를 갔어요.", "나는 어제 형이랑 학교를 갔어요."];

        if (!maiEng.isAvailableBrowser()) {
            alert("'getUserMedia()' is not supported on your browser.\n\nWe recommend using the latest version of Chrome, Firefox, Safari, or Microsoft Edge.");
        }

        function assStepColoring() {
            if (stepNum == 0) {
                $('#assStep .nav li:nth-child(1)').addClass("active");
                $('#assStep .nav li:nth-child(2)').addClass("disabled");
                $('#assStep .nav li:nth-child(3)').removeClass();
                $('#assStep .nav li:nth-child(3)').addClass("disabled");
            } else if (stepNum == 1) {
                $('#assStep .nav li:nth-child(1)').removeClass();
                $('#assStep .nav li:nth-child(2)').addClass("active");
                $('#assStep .nav li:nth-child(3)').addClass("disabled");
            } else {
                $('#assStep .nav li:nth-child(1)').removeClass();
                $('#assStep .nav li:nth-child(2)').removeClass();
                $('#assStep .nav li:nth-child(3)').addClass("active");
            }
        }

        //평가 시작
        $('.btn_ass').on('click', function () {

            if (toggleFlag) { // ON -> OFF 바뀌어야 함

                maiEng.cancel();

                toggleFlag = false;
                $(this).removeClass('playing');

            } else { // OFF -> ON 바뀌어야 함 (마이크를 사용하겠다고 방금 선언한 상태!!)

                if (!maiEng.isAvailableBrowser()) {
                    alert("'getUserMedia()' is not supported on your browser.\n\nWe recommend using the latest version of Chrome, Firefox, Safari, or Microsoft Edge.");
                    return;
                }

                $('#recordAudio').css('display', 'none');

                $(this).addClass('playing');
                $(this).parent().parent().parent().addClass('textOn');

                toggleFlag = true;

                maiEng.startStt('userId', null, sentence[stepNum], onResult_Stt, onFail);
            }
        });

        //평가 다시하기
        $('.btn_ass_again').on('click', function () {

            assStepColoring();

            $('.questionBox .txt_en span').html(sentence[stepNum]);
            $('.questionBox .txt_kr').text(translation[stepNum]);

            $('.answerBox .txt span').hide();
            $('.answerBox .assResult').hide();

            // assBefore block '평가하기' 버튼
            $('.btnBox .assBefore').css('display', 'block');
            $('.btnBox .assBefore').show();

            // assAfter none '다시하기, 다음문장' 버튼
            $('.btnBox .assAfter').css('display', 'none');
            $('.btnBox .assAfter').hide();

            // 결과창 초기화
            $('.answerBox .txt span').text("");
            // 점수 초기화
            $('.answerBox .assResult dd').text("");
        });

        // 다음 단계 넘어가기
        $('.btn_ass_next').on('click', function () {

            stepNum++;

            if (stepNum == 3) {
                stepNum = stepNum - 3;
            }

            assStepColoring();

            $('.questionBox .txt_en span').html(sentence[stepNum]);
            $('.questionBox .txt_kr').text(translation[stepNum]);

            $('.answerBox .txt span').hide();
            $('.answerBox .assResult').hide();

            // assBefore block '평가하기' 버튼
            $('.btnBox .assBefore').css('display', 'block');
            $('.btnBox .assBefore').show();

            // assAfter none '다시하기, 다음문장' 버튼
            $('.btnBox .assAfter').css('display', 'none');
            $('.btnBox .assAfter').hide();

            if (stepNum == 2) {
                $('#retry_btn').html('<em class="fas fa-redo"></em>Reset');
            } else {
                $('#retry_btn').html('<em class="fas fa-arrow-right"></em>Next Sentence');
            }

            // 결과값 초기화
            $('.answerBox .txt span').text("");

        });

        //sound play
        var audioFileUrl = "/aiaas/kr/audio/engedu/";
        var oriAudio = ["engedu_stt_ori1.mp3", "engedu_stt_ori2.mp3", "engedu_stt_ori3.mp3"];

        //원본파일 재생
        $('#oriAudio1').click(function () {
            new Audio(audioFileUrl + oriAudio[stepNum]).play();
        });
    });

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>
<script src="https://${engedu_host}/front/resources/static/js/init-v1.js"></script>