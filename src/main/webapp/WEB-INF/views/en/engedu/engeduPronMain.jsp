<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<script src="https://cdn.jsdelivr.net/npm/chart.js@2.8.0/dist/Chart.min.js"></script>

<!-- .contents -->
<div class="contents">
    <div class="content api_content">
        <h1>Evaluation of English Pronunciation</h1>
        <ul class="menu_lst eng_lst">
            <li class="tablinks" onclick="openTap(event, 'pron_demo')" id="defaultOpen">
                <button type="button">AI Engine</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'pron_example')">
                <button type="button">Use Case</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'pron_menu')">
                <button type="button">Manual</button>
            </li>
<%--            <li class="tablinks"><a href="/member/enApiAccount">API ID, key</a></li>--%>
        </ul>
        <!-- [신규] 20190809 서비스개발 YMJ 영어 문장 발음 평가 콘텐츠 -->
        <div class="demobox en_demobox" id="pron_demo">
            <p><span>Evaluation of English Pronunciation</span></p>
            <span class="sub">Gives a score by comparing learner’s fluency to a native speaker’s</span>
            <!-- demoBox -->
            <div class="demoBox">
                <!-- commBox -->
                <div class="commBox">
                    <p class="commBox_txt">Click the button <img src="/aiaas/kr/images/img_mic_orange.png" alt="microphone"> to activate the microphone then read the sentence below.</p>
                    <!-- stepBox -->
                    <ul class="stepBox">
                        <li id="assStep">
                            <ul class="nav">
                                <li class="active">Step 1</li>
                                <li class="disabled">Step 2</li>
                                <li class="disabled">Step 3</li>
                            </ul>
                            <div class="questionBox">
                                <p class="sound">
                                    <!-- [D] 오디오파일 링크은 하단 javascript에 넣어주세요. -->
                                    <button type="button" id="oriAudio2" class="btn_oriPlay" title="play">Play
                                    </button>
                                </p>
                                <p class="txt_en"><span>I want to buy a ticket.</span></p>
<%--                                <p class="txt_kr">나는 티켓을 사고 싶어요.</p>--%>
                            </div>
                            <div class="answerBox2 ">
                                <div class="assResult">
                                    <div id="chart" class="gaugeBox">
                                        <dl>
                                            <dt>Pronunciation</dt>
                                            <dd id="pronResult1">
                                                <!-- [D] 평가 점수만큼 <span>에 width값을 넣어주세요. -->
                                                <span id="pron1gauge" class="gauge" style="width:80%">80%</span>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>Intonation</dt>
                                            <dd id="pronResult2">
                                                <span id="pron2gauge" class="gauge" style="width:20%">20%</span>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>Speed</dt>
                                            <dd id="pronResult3">
                                                <span id="pron3gauge" class="gauge" style="width:100%">100%</span>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>Rhythm</dt>
                                            <dd id="pronResult4">
                                                <span id="pron4gauge" class="gauge" style="width:50%">50%</span>
                                            </dd>
                                        </dl>
                                        <dl>
                                            <dt>Articulation</dt>
                                            <dd id="pronResult5">
                                                <span id="pron5gauge" class="gauge" style="width:50%">50%</span>
                                            </dd>
                                        </dl>
                                    </div>
                                    <div id="star_box" class="starBox">
                                        <p id="img_box" class="imgBox">
                                            <!-- [D] 평가 점수에 따라 <em> 추가 -->
                                            <em id="star1" class="fas fa-star"></em>
                                            <em id="star2" class="fas fa-star"></em>
                                            <em id="star3" class="fas fa-star"></em>
                                            <em id="star4" class="fas fa-star"></em>
                                            <em id="star5" class="fas fa-star"></em>
                                        </p>
                                        <!-- [D] 평가 점수에 따라 문구 변경 -->
                                        <p id="result_txt" class="txt">Excellent</p>
                                        <!-- [D] 평가 총 점수 참고
                                        90-100: 별:5개 Excellent!
                                        80-89:  별:4개 Good!
                                        70-79:  별:3개 Good try!
                                        60-69:  별:2개 Not bad!
                                        ~59:    별:1개 Try again!
                                        -->
                                    </div>
                                </div>
                            </div>
                            <div class="btnBox">
                                <!-- 버튼_평가전 -->
                                <p class="assBefore"><!-- 평가 완료되면 display:none; 스타일 선언 -->
                                    <button type="button" class="btn_orange btn_ass"><span>Evaluation</span></button>
                                </p>
                                <!-- //버튼_평가전 -->
                                <!-- 버튼_평가후 -->
                                <p class="assAfter"><!-- 평가 완료되면 display:block; 스타일 선언 -->
                                    <button type="button" class="btn_ass_again"><em class="fas fa-retweet"></em>Try Again
                                    </button>
                                    <button type="button" id="retry_btn" class="btn_orange btn_ass_next"><em
                                            class="fas fa-arrow-right"></em>Next Sentence
                                    </button>
                                </p>
                                <!-- //버튼_평가후 -->
                            </div>
                        </li>
                    </ul>

                    <!-- //stepBox -->
                </div>
                <!-- //commBox -->
            </div>
            <!-- //demoBox -->
        </div>
        <!-- //[신규] 20190809 서비스개발 YMJ 영어 문장 발음 평가 콘텐츠-->
        <!--.pron_menu-->
        <div class="demobox eng_menu" id="pron_menu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API Guideline
                    </div>
                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID &amp; Key</p>
                    <p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                    <p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                    <p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        Pronunciation Scoring
                    </div>
                    <p class="sub_txt">Compare and score the speaker’s english utterance.</p>
                    <span class="sub_title">Preparation</span>
                    <p class="sub_txt">① Input: Sound file (.wav/.pcm)</p>
                    <ul>
                        <li>File type: .wav, .pcm</li>
                        <li>Sample rate: 16K</li>
                        <li>Channel: mono</li>
                    </ul>
                    <p class="sub_txt">② Input: Answer Text</p>
                    <span class="sub_title">API Document</span>
                    <p class="sub_txt">① Request </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/edueng/pron</li>
                    </ul>
                    <p class="sub_txt">② Request parameters </p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API ID. Request from is required for the ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>userId</td>
                            <td>User ID</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>model</td>
                            <td>Selected model (baseline)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>answerText</td>
                            <td>The answer of the text</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>file</td>
                            <td>Input file (.wav, .pcm)</td>
                            <td>file</td>
                        </tr>

                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
                        curl -X POST \<br>
                        https://api.maum.ai/edueng/pron \<br>
                        -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \<br>
                        -F 'apiId= (*Own API ID)' \<br>
                        -F 'apiKey= (*Own API Key)' \<br>
                        -F 'userId= speaker1' \<br>
                        -F 'model= baseline' \<br>
                        -F 'answerText=cool'  \<br>
                        -F 'file=@eng_16k_1.wav' \<br>
                    </div>

                    <p class="sub_txt">④ Response parameters </p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>resultText</td>
                            <td>STT result</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>resCode</td>
                            <td>API response (Success:200)</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>answerText</td>
                            <td>Answer sentence</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>recordUrl</td>
                            <td>Inputted file URL</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>words</td>
                            <td>Score by words</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>rhythmScore</td>
                            <td>Rhythm score</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>segmentalScore</td>
                            <td>Segmental score</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>totalScore</td>
                            <td>Total score</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>sentenceScore</td>
                            <td>Sentence score</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>pronScore</td>
                            <td>Pronunciation score</td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>Speed</td>
                            <td>Speed score </td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>intonationScore</td>
                            <td>Intonation score</td>
                            <td>int</td>
                        </tr>
                    </table>
                    <p class="sub_txt">⑤ Response example </p>
                    <div class="code_box">
                        <pre>
{
    "result": {
        "resultText": "Cold.",
        "answerText": "cool",
        "recordUrl": "http://maieng.maum.ai:7776/record/engedu.api/apiId/userId/202002/20200210_165127_2.mp3",
        "words": [
            {
                "word": "cold",
                "pronScore": 31
            }
        ],
        "rhythmScore": 56,
        "segmentalScore": 76,
        "totalScore": 68,
        "sentenceScore": 0,
        "pronScore": 23,
        "speedScore": 92,
        "resCode": 200,
        "intonationScore": 54
    }
}
                        </pre>
                    </div>
                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--//pron_menu-->
        <!--.pron_example-->
        <div class="demobox" id="pron_example">
            <p><em style="font-weight: 400; color: #fb8923;">Use Cases</em></p>
            <span class="sub">Find out how AI can be applied to diverse area.</span>
            <div class="useCasesBox">
                <p style="font-size:15px;">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>English evalutaion</span>
                            </dt>
                            <dd class="txt">When practicing speaking, intonation, accent, and pronunciation are scored based on comparison to a native speaker.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_engeduStt"><span>engedu STT</span></li>
                                    <li class="ico_engeduPron"><span>engedu Pron</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>Speech assessment</span>
                            </dt>
                            <dd class="txt">English speech receives feedback every sentence for immediate application.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_engeduStt"><span>engedu STT</span></li>
                                    <li class="ico_engeduPron"><span>engedu Pron</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
                </p>
            </div>

        </div>
        <%--pron_example--%>

    </div>
    <!-- //.content -->
</div>


<script type="text/javascript">

    // $('.category_1').trigger("click");

    $(window).load(function () {
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function () {
            $(this).remove();
        });

    });
</script>

<script src="https://${engedu_host}/front/resources/static/js/maiEng-v1${engedu_surfix}.min.js"></script>

<!-- [신규] 20190809 서비스개발 YMJ -->
<script type="text/javascript">

    var stepNum = 0; //현재 스텝 표시
    var toggleFlag = false; //min 상태 표시

    function onNoti_Stt(result) {
        // 발음 평가 서버를 거치는 동안 STT의 결과값 사용 가능
    }

    function onResult_Pron(result) {
        showResult(result.totalScore, result.pronScore, result.speedScore, result.rhythmScore, result.intonationScore, result.segmentalScore);
        console.log("" + result.totalScore);

        $(".btnBox .assBefore").css('display', 'none');
        $(".btnBox .assAfter").css('display', 'block');

        $('.btn_ass').removeClass('playing');
        toggleFlag = false;
    }

    function onFail(reason) {

    }

    function showResult(totalScore, pronScore, speedScore, rhythmScore, intoScore, segmentalScore) {

        // 점수 설정
        $('#pron1gauge').text(pronScore + '%');
        $('#pron1gauge').css('width', pronScore + '%');

        $('#pron2gauge').text(intoScore + '%');
        $('#pron2gauge').css('width', intoScore + '%');


        $('#pron3gauge').text(speedScore + '%');
        $('#pron3gauge').css('width', speedScore + '%');


        $('#pron4gauge').text(rhythmScore + '%');
        $('#pron4gauge').css('width', rhythmScore + '%');

        $('#pron5gauge').text(segmentalScore + '%');
        $('#pron5gauge').css('width', segmentalScore + '%');

        // starBox 설정
        $('#img_box em').css('display', 'inline-block');

        if (totalScore >= 90) {
            $('#result_txt').text("Excellent!");
        } else if (totalScore >= 80) {
            $('#star5').css('display', 'none');

            $('#result_txt').text("Good!");
        } else if (totalScore >= 70) {
            $('#star4').css('display', 'none');
            $('#star5').css('display', 'none');

            $('#result_txt').text("Good try!");
        } else if (totalScore >= 60) {
            $('#star3').css('display', 'none');
            $('#star4').css('display', 'none');
            $('#star5').css('display', 'none');

            $('#result_txt').text("Not bad!");
        } else {
            $('#star2').css('display', 'none');
            $('#star3').css('display', 'none');
            $('#star4').css('display', 'none');
            $('#star5').css('display', 'none');

            $('#result_txt').text("Try again!");
        }

        // 화면 출력
        $('.assResult').css('display', 'table');
        $('#chart').css('display', 'table-cell');
        $('#star_box').css('display', 'table-cell');
        $('#img_box').css('display', 'block');
    }

    function assStepColoring() {
        if (stepNum == 0) {
            $('#assStep .nav li:nth-child(1)').addClass("active");
            $('#assStep .nav li:nth-child(2)').addClass("disabled");
            $('#assStep .nav li:nth-child(3)').removeClass();
            $('#assStep .nav li:nth-child(3)').addClass("disabled");
        } else if (stepNum == 1) {
            $('#assStep .nav li:nth-child(1)').removeClass();
            $('#assStep .nav li:nth-child(2)').addClass("active");
            $('#assStep .nav li:nth-child(3)').addClass("disabled");
        } else {
            $('#assStep .nav li:nth-child(1)').removeClass();
            $('#assStep .nav li:nth-child(2)').removeClass();
            $('#assStep .nav li:nth-child(3)').addClass("active");
        }
    }

    $(document).ready(function () {

        var sentence = ["I want to buy a ticket.", "I would like to watch the movie.", "I need to take a rest."];
        var translation = ["나는 티켓을 사고 싶어요.", "나는 영화를 보고 싶어요.", "나는 휴식이 필요해요."];

        $('#chart').css('display', 'none');
        $('#img_box').css('display', 'none');
        $('#result_txt').css('display', 'block');
        $('#result_txt').text("");

        if (!maiEng.isAvailableBrowser()) {
            alert("'getUserMedia()' is not supported on your browser.\n\nWe recommend using the latest version of Chrome, Firefox, Safari, or Microsoft Edge.");
        }

        //평가 시작
        $('.btn_ass').on('click', function () {

            $('#result_txt').text("");
            $('#result_txt').css('display', 'block');

            if (toggleFlag) { // ON -> OFF 바뀌어야 함

                maiEng.cancel();

                toggleFlag = false;
                $(this).removeClass('playing');

            } else { // OFF -> ON 바뀌어야 함 (마이크를 사용하겠다고 방금 선언한 상태!!)

                if (!maiEng.isAvailableBrowser()) {
                    alert("'getUserMedia()' is not supported on your browser.\n\nWe recommend using the latest version of Chrome, Firefox, Safari, or Microsoft Edge.");
                    return;
                }

                $(this).addClass('playing');
                $(this).parent().parent().parent().addClass('textOn');

                toggleFlag = true;

                maiEng.startPron('userId', null, sentence[stepNum], onNoti_Stt, onResult_Pron, onFail);
            }
            //결과값을 받았다면
        });

        //평가 다시하기
        $('.btn_ass_again').on('click', function () {
            $('#chart').css('display', 'none');
            $('#img_box').css('display', 'none');
            $('#result_txt').text("");
            $('#result_txt').css('display', 'block');

            // assBefore block '평가하기' 버튼
            $('.btnBox .assBefore').css('display', 'block');
            $('.btnBox .assBefore').show();

            // assAfter none '다시하기, 다음문장' 버튼
            $('.btnBox .assAfter').css('display', 'none');
            $('.btnBox .assAfter').hide();

            /*
                        // 보이는 점수, 그래프 값 초기화
                        $('#pron1gauge').text('0%');
                        $('#pron1gauge').css('width', '0');
                        $('#pron2gauge').text('0%');
                        $('#pron2gauge').css('width', '0');
                        $('#pron3gauge').text('0%');
                        $('#pron3gauge').css('width', '0');
                        $('#pron4gauge').text('0%');
                        $('#pron4gauge').css('width', '0');

                        $('#star1').css('display', 'none');
                        $('#star2').css('display', 'none');
                        $('#star3').css('display', 'none');
                        $('#star4').css('display', 'none');
                        $('#star5').css('display', 'none');
            */
        });

        //다음 단계 넘어가기
        $('.btn_ass_next').on('click', function () {

            $('#chart').css('display', 'none');
            $('#img_box').css('display', 'none');
            $('#result_txt').text("");
            $('#result_txt').css('display', 'block');

            stepNum++;

            if (stepNum == 3) {
                stepNum = stepNum - 3;
            }

            assStepColoring();

            $('.questionBox .txt_en span').html(sentence[stepNum]);
            $('.questionBox .txt_kr').text(translation[stepNum]);

            // assBefore block '평가하기' 버튼
            $('.btnBox .assBefore').css('display', 'block');
            $('.btnBox .assBefore').show();

            // assAfter none '다시하기, 다음문장' 버튼
            $('.btnBox .assAfter').css('display', 'none');
            $('.btnBox .assAfter').hide();

            if (stepNum == 2) {
                $('#retry_btn').html('<em class="fas fa-redo"></em>Reset');
            } else {
                $('#retry_btn').html('<em class="fas fa-arrow-right"></em>Next Sentence\n');
            }


            $('#star1').css('display', 'none');
            $('#star2').css('display', 'none');
            $('#star3').css('display', 'none');
            $('#star4').css('display', 'none');
            $('#star5').css('display', 'none');
        });

        //sound play
        var audioFileUrl = "/aiaas/kr/audio/engedu/";
        var oriAudio = ["engedu_pron_ori1.mp3", "engedu_pron_ori2.mp3", "engedu_pron_ori3.mp3"];

        //원본파일 재생
        $('#oriAudio2').click(function () {
            new Audio(audioFileUrl + oriAudio[stepNum]).play();
        });
    });

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>
<script src="https://${engedu_host}/front/resources/static/js/init-v1.js"></script>