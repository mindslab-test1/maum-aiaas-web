<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-03-24
  Time: 16:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/hotelbot.css?ver=<%=fmt.format(lastModifiedStyle)%>" />

<!-- .contents -->
<div class="contents">
    <div class="content api_content bot_content">
        <h1 class="api_tit"></h1>
        <ul class="menu_lst bot_lst">
            <li class="tablinks" onclick="openTap(event, 'hotelbox')" id="defaultOpen"><button type="button">AI Engine</button></li>
            <li class="tablinks" onclick="openTap(event, 'hotelexample')"><button type="button">Use Case</button></li>
            <li class="tablinks" onclick="openTap(event, 'hotelmenu')"><button type="button">Manual</button></li>
<%--            <li class="tablinks"><a href="/member/enApiAccount">API ID, key</a></li>--%>
        </ul>
        <div class="demobox" id="hotelbox">
            <p>Hotel Concierge Chatbot using <span style="color: #2cabe5;">Maum SDS</span> <small>(Spoken Dialog System)</small></p>
            <span class="sub">A chatbot to handle hotel inquiries and concierge service from before reservation all the way through to check-out.<br>
Improves efficiency of responses to customers with 24/7 multilingual service.
</span>
            <p class="temp_txt" style="color: #2cabe5;font-weight: 400;">The service is currently available only in Korean. Other languages will be available soon.</p>
            <!-- chatbot_box -->
            <div class="chatbot_box" id="hotelBot">
                <div class="hotelbot_box">
                    <div class="hotelbot">
                        <iframe src="https://sds.maum.ai/chatbot?hostNo=7&intent=%EC%B2%98%EC%9D%8C%EC%9C%BC%EB%A1%9C"></iframe>
                    </div>
                </div>

            </div>
            <!-- //.chatbot_box -->

        </div>
        <!-- .demobox -->

        <!--.nqamenu-->
        <div class="demobox bot_menu" id="hotelmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API Guideline
                    </div>
                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID &amp; Key</p>
                    <p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                    <p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                    <p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        Hotel Concierge Chatbot <small></small>
                    </div>
                    <p class="sub_txt">A chatbot to handle hotel inquiries and concierge service from before reservation all the way through to check-out.<br>
                        Improves efficiency of responses to customers with 24/7 multilingual service.
                    </p>

                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--//nqamenu-->
        <!--.nqaexample-->
        <div class="demobox" id="hotelexample">
            <p><em style="color:#2cace5;font-weight: 400;">Use Cases</p>
            <span class="sub">Find out how AI can be applied to diverse area.</span>
            <%--챗봇(chatbot)--%>
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>Multilingual Chatbot</span>
                            </dt>
                            <dd class="txt">Using the latest AI language model ‘BERT’, you can quickly and easily create multilingual chatbots that are not dependent on a specific language.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_sds"><span>SDS</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>Voice Bot</span>
                            </dt>
                            <dd class="txt">You can implement a voice interface chatbot service which called Voice Bot.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_stt"><span>STT</span></li>
                                    <li class="ico_sds"><span>SDS</span></li>
                                    <li class="ico_tts"><span>TTS</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>Service-specific<br> Chatbot Platform</span>
                            </dt>
                            <dd class="txt">In addition to a single chatbot, you can build a chatbot platform specialized for a service, such as a hotel chatbot service.
<%--                                <br> 사업 제휴 문의 : <a href="/support/krContactUs" alt="contact us" style="color: #2dabe5;">Contact us</a> --%>
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_sds"><span>SDS</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //챗봇(chatbot) -->

        </div>
        <!--//.nqaexample-->


    </div>

</div>
<!-- //.contents -->

<!-- //wrap -->
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/common.js"></script>

<script type="text/javascript">
    jQuery.event.add(window,"load",function(){
        $(document).ready(function (){

        });
    });

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>


