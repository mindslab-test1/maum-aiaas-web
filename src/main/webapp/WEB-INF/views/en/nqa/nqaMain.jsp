﻿		<!-- .contents -->
		<div class="contents">
			<div class="content api_content bot_content">
				<h1 class="api_tit">NQA Bot</h1>
				<ul class="menu_lst bot_lst">
					<li class="tablinks" onclick="openTap(event, 'nqabox')" id="defaultOpen"><button type="button">AI Engine</button></li>
					<li class="tablinks" onclick="openTap(event, 'nqaexample')"><button type="button">Use Case</button></li>
					<li class="tablinks" onclick="openTap(event, 'nqamenu')"><button type="button">Manual</button></li>
<%--					<li class="tablinks"><a href="/member/enApiAccount">API ID, key</a></li>--%>
				</ul>
				<div class="demobox" id="nqabox">
					<p>Financial Service Chatbot using <span style="color:#2cace5;">NQA </span><small>(Natural QA)</small></p>
					<span class="sub">Ask about financial services such as funds, pensions, CMA, stocks, manuals, and more.<br>
						This is an example chatbot using NQA, which can be used with various contents.<br>
						Create your own custom chatbot with NQA and use it right now for your business.
					</span>
					<p class="temp_txt" style="color: #2cabe5;font-weight: 400;">The service is currently available only in Korean. Other languages will be available soon.</p>
					<!-- chatbot_box -->
					<div class="chatbot_box" id="NQA">
						<!-- .chat_mid -->
						<div class="chat_mid">
							<div class="bot_infoBox">
								<div class="thumb"><img src="${pageContext.request.contextPath}/aiaas/kr/images/chat_nqa_big.png" alt="chatbot"></div>
								<div class="txt">금융봇에게 질문해 보세요. </div>
								<ul class="info_btnBox">
									<li><button type="button">자동화기기 무통장 서비스 비번 바꾸려 하는데요</button></li>
									<li><button type="button">폰뱅 무통장 거래 내역 조회 업무 메뉴</button></li>
									<li><button type="button">주식 매매 수수료가 어떻게 되나요?</button></li>
								</ul>
							</div>
							<ul class="talkLst">
								<li class="newDate">
									<span><!-- 날짜는 스크립트가 정의--></span>
								</li>
<%--								<li class="user">--%>
<%--                                    <span class="cont">--%>
<%--                                    <em class="txt">사용자가 입력한 텍스트 사용자가 입력한 텍스트 사용자가 입력한 텍스트 사용자가 입력한 텍스트 사용자가 입력한 텍스트</em>--%>
<%--                                    </span>--%>
<%--								</li>--%>

<%--									<!-- bot 채팅+버튼 UI -->  --%>
<%--								<li class="bot">--%>
<%--									<span class="thumb"><img src="${pageContext.request.contextPath}/aiaas/kr/images/chat_nqa.png" alt="chatbot"></span>--%>
<%--									<!-- 봇 메세지 (평문) -->--%>
<%--									<span class="cont">--%>
<%--                                        <em class="txt">안오늘 서울 강남구 도곡동 날씨입니다.<br> 흐림, 현재 온도는 24 (최저: 17, 최고:25), 습도는 45% 입니다. </em>--%>
<%--                                    </span>--%>
<%--									<div class="hashbox">--%>
<%--										<ul>--%>
<%--											<li>반대매매</li>--%>
<%--											<li>주문유형</li>--%>
<%--											<li class="on">예약주문</li>--%>
<%--											<li>주문유형</li>--%>
<%--											<li>수수료</li>--%>
<%--										</ul>--%>
<%--									</div>--%>
<%--								</li>--%>

<%--									<!-- bot 로딩 UI -->  --%>
<%--								<li class="bot">--%>
<%--									<span class="thumb"><img src="${pageContext.request.contextPath}/aiaas/kr/images/chat_nqa.png" alt="sully"></span>--%>
<%--									<span class="cont">--%>
<%--                                        <!-- 로딩 -->--%>
<%--                                        <em class="txt">--%>
<%--                                            <span class="chatLoading">--%>
<%--                                                <b class="chatLoading_item01"></b>--%>
<%--                                                <b class="chatLoading_item02"></b>--%>
<%--                                                <b class="chatLoading_item03"></b>--%>
<%--                                            </span>--%>
<%--                                        </em>--%>
<%--                                    </span>--%>
<%--								</li>--%>
							</ul>
						</div>
						<!-- //.chat_mid -->
						<!-- .chat_btm -->
						<div class="chat_btm">
							<form method="post" action="" id="formChat" name="formChat">
								<textarea class="textArea" placeholder="Type in message (Korean only)"></textarea>
								<input type="button" name="btn_chat" id="btn_chat" class="btn_chat" title="send" value="send">
							</form>
						</div>
						<!-- //.chat_btm -->

					</div>
					<!-- //.chatbot_box -->
					
				</div>
				<!-- .demobox -->

				<!--.nqamenu-->
				<div class="demobox bot_menu" id="nqamenu">
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API Guideline
							</div>
							<p class="sub_title">Set up Environment</p>
							<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
							<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
							<p class="sub_title">ID &amp; Key</p>
							<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
							<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
							<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
								<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
							<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
							<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
							<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
						</div>
						<div class="guide_group">
							<div class="title">
								Natural QA <small>(NQA)</small>
							</div>
							<p class="sub_txt">It is used for services that provide answers to questions by using a pre-registered question-and-answer database.</p>

						</div>
					</div>
					<!--//.guide_box-->
				</div>
				<!--//nqamenu-->
				<!--.nqaexample-->
				<div class="demobox" id="nqaexample">
					<p><em style="font-weight: 400;color:#2cace5;">Use Cases</em>  </p>
					<span class="sub">Find out how AI can be applied to diverse area.</span>
					<%--챗봇(chatbot)--%>
					<div class="useCasesBox">
						<ul class="lst_useCases">
							<li>
								<dl>
									<dt>
										<em>CASE 01</em>
										<span>Chatbot</span>
									</dt>
									<dd class="txt">You can create a custom chatbot for your own purpose.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_sds"><span>SDS</span></li>
											<li class="ico_bqa"><span>NQA</span></li>
											<li class="ico_nlu"><span>NLU</span></li>
											<li class="ico_irqa"><span>IRQA</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>Knowledge portal</span>
									</dt>
									<dd class="txt">It can be used to build knowledge portals that specialize in specific areas.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_sds"><span>SDS</span></li>
											<li class="ico_bqa"><span>NQA</span></li>
											<li class="ico_nlu"><span>NLU</span></li>
											<li class="ico_mrc"><span>MRC</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 03</em>
										<span>Interactive training service</span>
									</dt>
									<dd class="txt">You can leverage your existing data to implement interactive educational services.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_sds"><span>SDS</span></li>
											<li class="ico_bqa"><span>NQA</span></li>
											<li class="ico_nlu"><span>NLU</span></li>
											<li class="ico_tts"><span>TTS</span></li>
										</ul>
									</dd>
								</dl>
							</li>
						</ul>
					</div>
					<!-- //챗봇(chatbot) -->

				</div>
				<!--//.nqaexample-->


			</div>

		</div>
		<!-- //.contents -->

<input type="hidden" id="AUTH_ID">
<input type="hidden" id="SESSION_ID">
<input type="hidden" id="thumb">
<!-- //wrap -->
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/common.js"></script>	
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/chatbot/nqa.js?v=20190902"></script>
<script type="text/javascript">   

chatSingIn();
function chatSingIn() {
	$.ajax({
		url: 'https://aicc-prd1.maum.ai:9980/api/v3/auth/signIn',
		async: false,
		type: 'POST',
		headers: {
			"Content-Type": "application/json",
			"m2u-auth-internal": "m2u-auth-internal"
		},
		data: JSON.stringify({
			"userKey": "admin",
			"passphrase": "1234"
		}),
		dataType: 'json',
		success: function(data) {
			console.log(data.directive.payload.authSuccess.authToken);

			document.getElementById('AUTH_ID').value = data.directive.payload.authSuccess.authToken;
			console.log(document.getElementById('AUTH_ID').value);
			console.log('login success!')

		}, error: function(err) {
			console.log("chatbot Login error! ", err);
		}
	});
}


</script>
<script type="text/javascript">
jQuery.event.add(window,"load",function(){
	$(document).ready(function (){
		var chatName = $('.chatbot_box').attr('id');
		document.getElementById('thumb').value='/aiaas/kr/images/chat_nqa.png';
		chatOpen(chatName);
		chatNm = chatName;
	});
});

//API 탭
function openTap(evt, menu) {
	var i, demobox, tablinks;
	demobox = document.getElementsByClassName("demobox");
	for (i = 0; i < demobox.length; i++) {
		demobox[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(menu).style.display = "block";
	evt.currentTarget.className += " active";
}
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();
</script>


