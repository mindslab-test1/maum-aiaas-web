<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-01-14
  Time: 10:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/subtitleRecog.css?ver=<%=fmt.format(lastModifiedStyle)%>" />

<!-- .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">close</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>Upload error</h5>
            <p>Error occurred due to the image or file size. <br>Please check the restriction again.</p>
            <span>* Supported languages: Korean <br>
                <small style="display: block;padding: 0 0 0 7px;">(Special characters are not supported.)</small>
                * Supported files:: .jpg, .png<br>
                * Image file size under 5MB</span>
        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">OK</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents api_content">
    <!-- .content -->
    <div class="content">
        <h1 class="api_tit">Recognition of Image Subtitles</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'iprdemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
            <li class="tablinks" onclick="openTap(event, 'iprexample')"><button type="button">Use Case</button></li>
            <li class="tablinks" onclick="openTap(event, 'iprmenu')"><button type="button">Manual</button></li>
<%--            <li class="tablinks"><a href="/member/enApiAccount">API ID, key</a></li>--%>
        </ul>
        <!-- .demobox -->
        <div class="demobox" id="iprdemo">
            <p><span>Subtitles Recognition</span></p>
            <span class="sub">AI engine that can recognize and extract subtitles.</span>
            <p class="temp_txt" style="color:#f7778a;">The service runs best in Korean. Other languages will be optimized soon.</p>
            <!--textremoval_box-->
            <div class="demo_layout poserecog_box subtitleRecog_box">
<%--                <span>The service runs best in Korean. Other languages will be optimized soon.</span>--%>
                <!--tr_1-->
                <div class="tr_1">
                    <div class="fl_box">
                        <p><em class="far fa-file-image"></em><strong>Use Sample File</strong></p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="option" value="1" checked>
                                    <label for="sample1" class="female">
                                        <div class="img_area">
                                            <img src="${pageContext.request.contextPath}/aiaas/common/images/img_subtitle_sample.jpg" alt="sample img 1" />
                                        </div>
                                    </label>
                                </div>
                            </div>

                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-image"></em><strong>Use My File</strong></p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-image hidden"></em>
                                <label for="demoFile" class="demolabel">Upload Image</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".jpg, .png">
                            </div>
                            <ul>
                                <li>* Supported languages: Korean</li>
                                <em style="display: block;padding: 0 0 0 7px;color: #2c3f51;font-size: 11px;">(Special characters are not supported.)</em>
                                <li>* Supported files:: .jpg, .png</li>
                                <li>* Image file size under 5MB</li>

                            </ul>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">Process</button>
                    </div>
                </div>
                <!--tr_1-->
                <!--tr_2-->
                <div class="tr_2">
                    <p><em class="far fa-file-image"></em>In progress</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#f7778a" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

                        <p>AI processing takes a while..</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
                    </div>

                </div>
                <!--tr_2-->
                <!--tr_3-->
                <div class="tr_3" >
                    <div class="origin_file">
                        <p><em class="far fa-file-image"></em>Input</p>
                        <div class="imgBox">
                            <img id="input_img" src="" alt="Sample file" />
                        </div>
                    </div>
                    <div class="result_file" >
                        <p><em class="far fa-file-alt"></em>Result</p>
                        <div class="subtitle_box">
                            <div class="subtitle_text">
                                <div id="resultTxt">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2"><em class="fas fa-redo"></em>Reset</button>
                    </div>
                </div>
                <!--tr_3-->
            </div>
            <!--//.poserecog_box-->

        </div>
        <!-- //.demobox -->
        <!--.iprmenu-->
        <div class="demobox vision_menu" id="iprmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API Guideline
                    </div>
                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID &amp; Key</p>
                    <p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                    <p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                    <p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        Recognition of Image Subtitles
                    </div>
                    <p class="sub_txt">AI engine that can recognize and extract subtitles. *Korean only*</p>

                    <span class="sub_title">
								Preparation
					</span>
                    <p class="sub_txt">- Input: Image file</p>
                    <ul>
                        <li>File type: .jpg, .png.</li>
                        <li>Size: Under 5MB</li>
                    </ul>
                    <span class="sub_title">
								 API Document
							</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/Vca/VCASubtitleExtract</li>
                    </ul>
                    <p class="sub_txt">② Request - parameters</p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API ID. Request from is required for the ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>scene_img </td>
                            <td>Input image file (.jpg,.png)</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
                        url --location --request POST 'http://api.maum.ai/Vca/VCASubtitleExtract' \<br>
                        --header 'Content-Type: multipart/form-data' \<br>
                        --form 'apiId= Own API ID' \<br>
                        --form 'apiKey= Own API KEY' \<br>
                        --form 'scene_img= Input image file'<br>
                    </div>

                    <p class="sub_txt">④ Response example </p>

                    <div class="code_box">
<pre>
{
    "result_img": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",
    "result_text": "별로 볼 거 없네"
}
</pre>
                    </div>
                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.iprmenu-->
        <!--.iprexample-->
        <div class="demobox" id="iprexample">
            <p><em style="color:#f7778a;font-weight: 400;">Use Cases</em></p>
            <span class="sub">Find out how AI can be applied to diverse area.</span>
            <!-- 이미지 자막 인식 -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>Illegal Video <br>Detection</span>
                            </dt>
                            <dd class="txt">The subtitles in the frame of the original video are extracted and registered in the search system database, and the subtitles of the suspected illegal video are read and searched to determine whether the video matches.
                                <span><em class="fas fa-book-reader"></em> Reference: Korea Copyright Protection Agency</span>
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_sr"><span>Subtitles Recognition</span></li>
                                    <li class="ico_ftr"><span>Face Tracking</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>Automatic Video <br>Translation</span>
                            </dt>
                            <dd class="txt">It detects subtitles in the video and utilizes them in the automatic translation system.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_sr"><span>Subtitles Recognition</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!--이미지 자막 인식  -->
        </div>
        <!--//.iprexample-->


    </div>
    <!-- //.content -->
</div>

<script type="text/javascript">
    var sampleImage1;

    //파일명 변경
    document.querySelector("#demoFile").addEventListener('change', function (ev) {
        var demoFileInput = document.getElementById('demoFile');
        var demoFile = demoFileInput.files[0];
        var demoFileSize = demoFile.size;
        var max_demoFileSize = 1024 * 5000;//1kb는 1024바이트
        var demofile = document.querySelector("#demoFile");
        if(demoFileSize > max_demoFileSize || !demofile.files[0].type.match(/image.*/)){
            $('.pop_simple').show();
            // 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
            $('.fl_box').css("opacity", "0.5");
            $('#demoFile').val('');
            $('.tr_1 .btn_area').append('<span class="disBox"></span>');

            return;
        }


        document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
        var element = document.getElementById( 'uploadFile' );
        element.classList.remove( 'btn' );
        element.classList.add( 'btn_change' );
        $('.fl_box').css("opacity", "0.5");
    });


    jQuery.event.add(window,"load",function(){

        //샘플
        function loadSample1() {
            var blob = null;
            var xhr = new XMLHttpRequest();
            xhr.open("GET", "/aiaas/common/images/img_subtitle_sample.jpg");
            xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
            xhr.onload = function()
            {
                blob = xhr.response;//xhr.response is now a blob object
                sampleImage1 = new File([blob], "img_subtitle_sample.jpg");
                var imgSrcURL = URL.createObjectURL(blob);
                var subtitle_output=document.getElementById('input_img');
                subtitle_output.setAttribute("src",imgSrcURL);
            };

            xhr.send();
        }


        $(document).ready(function (){
            loadSample1();


            $('.radio label').on('click',function(){
                $('.fl_box').attr('opacity',1);
                $('em.close').click();
            });


            // step1->step2  (close button)
            $('em.close').on('click', function () {
                $('.fl_box').css("opacity", "1");
                $('#uploadFile label').text('Upload Image');
                $('#uploadFile').removeClass("btn_change");
                $('#uploadFile').addClass("btn");
                $('#demoFile').val("");

                //파일명 변경
                document.querySelector("#demoFile").addEventListener('change', function (ev) {
                    document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
                    var element = document.getElementById( 'uploadFile' );
                    element.classList.remove( 'btn' );
                    element.classList.add( 'btn_change' );
                });
            });


            // 처음으로 버튼
            $('.btn_back1').on('click', function () {

                $('.tr_2').hide();
                $('.tr_1').fadeIn(300);
            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('.tr_3').hide();
                $('.tr_1').fadeIn(300);

                document.querySelector("#demoFile").addEventListener('change', function (ev) {
                    document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;

                });
            });

            //결과보기 버튼
            $('#sub').on('click',function (){

                var demoFileTxt = $('#demoFile').val(); //파일을 추가한 input 박스의 값
                var formData = new FormData();
                var demoFile;

                //파일을 추가한 input 값 없을 때
                if(demoFileTxt === ""){
                    var option = $("input[type=radio][name=option]:checked").val();
                    loadSample1();
                    demoFile = sampleImage1;

                }

                //파일을 추가한 input 값 있을 때
                else {
                    var demoFileInput = document.getElementById('demoFile');
                    // console.log(demoFileInput);
                    demoFile = demoFileInput.files[0];
                    // console.log(demoFile);


                }

                formData.append('file',demoFile);
                formData.append('${_csrf.parameterName}', '${_csrf.token}');

                $('.tr_1').hide();
                $('.tr_2').fadeIn(300);

                xhr = $.ajax({
                    type: "POST",
                    async: true,
                    url: '/api/subtitleRecog', //여기 url 추가
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(result){
                        let resultData = JSON.parse(result);
                        $('#resultTxt').text(resultData.result_text);

                        var imgSrcURL = URL.createObjectURL(demoFile);
                        var subTitRecog_output = document.getElementById('input_img');
                        subTitRecog_output.setAttribute("src",imgSrcURL);

                        $('.tr_1').hide();
                        $('.tr_2').hide();
                        $('.tr_3').fadeIn(300);

                    },
                    error: function(jqXHR, error){
                        if(jqXHR.status === 0){
                            return false;
                        }

                        alert("No connection with the server.\nPlease try again later.");
                        console.dir(error);
                        window.location.reload();
                    }
                });


            });
            //알림 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
        });
    });


//API 탭
function openTap(evt, menu) {
    var i, demobox, tablinks;
    demobox = document.getElementsByClassName("demobox");
    for (i = 0; i < demobox.length; i++) {
        demobox[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(menu).style.display = "block";
    evt.currentTarget.className += " active";
}
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();


</script>
