<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

		<!-- .contents -->
		<div class="contents">
			<div class="content api_content">
				<h1 class="api_tit">XDC</h1>
				<ul class="menu_lst">
					<li class="tablinks" onclick="openTap(event, 'xdcdemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
					<li class="tablinks" onclick="openTap(event, 'xdcexample')"><button type="button">Use Case</button></li>
					<li class="tablinks" onclick="openTap(event, 'xdcmenu')"><button type="button">Manual</button></li>
<%--					<li class="tablinks"><a href="/member/enApiAccount" >API ID, key</a></li>--%>
				</ul>
				<!--.demobox_xdc-->
				<div class="demobox demobox_xdc"  id="xdcdemo">
					<p><span>XDC</span> <small>(eXplainable Document Classifier)</small> </p>
					<span class="sub">Regardless of categories or length of the article, it accurately categorizes the documents.</span>
					<p class="temp_txt">The service is currently available only in Korean. Other languages will be available soon.</p>
					<!--.xdc_box-->
					<div class="xdc_box">	
						<div class="xdc_wrap">
							<div class="demo_top">
								<p>Insert Text</p>
<%--								<button type="button" class="btn_article" id="naver_search_option">Search News</button>--%>
							</div>
							<div class="text_area">
								<textarea rows="12" id="id_input_text" placeholder="Please copy and paste the news article.">스타 강사 김미경 MKYU 대표가 자신과 꼭 닮은 ‘인공지능(AI) 휴먼’(사진)으로 강의한다. 교육업계에 AI 휴먼이 도입되는 첫 사례로 기록될 전망이다.
13일 AI업계에 따르면 김 대표는 AI 스타트업 마인즈랩과 손잡고 AI 휴먼 강의를 추진한다. 김 대표가 설립한 온라인 대학 MKYU와 마인즈랩은 최근 ‘AI 휴먼 솔루션을 활용한 콘텐츠 제작 및 상호협력’ 업무협약(MOU)을 맺었다. 마인즈랩은 수천 개에 이르는 김 대표의 강의 영상 등을 기반으로 김미경 AI 휴먼을 제작 중이다.
AI 휴먼은 딥러닝 기반 음성·영상합성 기술로 구현한 일종의 AI 분신이다. AI 휴먼을 만든 뒤 텍스트만 주면 실제 인물과 똑같은 목소리, 말투, 몸짓으로 말한다. 올 3월 LG헬로비전이 AI 업체 머니브레인과 함께 선보인 ‘이지애 AI 아나운서’를 떠올리면 쉽다. 이를 강사에게 적용하면 실제로 강의하지 않고도 수십 수백 개의 강의 콘텐츠를 만들 수 있다. 영어·일본어·베트남어 등 외국어 자동 번역 기능도 있다. 한글 자료만 줘도 AI 휴먼이 다양한 외국어로 강의하는 게 가능하다.</textarea>
								</textarea>
							</div>	
							<!-- Modal -->
							<div id="news_modal" class="modal">
								  <!-- Modal content -->
								  <div class="modal_content search_modal naver_news_search_div">
									  <span class="close close_naver">×</span>
									  <div class="search_modal_wrap">
										  <span class="naver_search_tit">Search articles via Naver</span>
										  <input class="naver_search_box" type="text" id="search_input" placeholder="Search articles in Korean only ">
										  <button type="button" class="btn search_btn" id="search_button">Search</button>
										  <span class="naver_search_desc">Please select from the results.</span>
										  <input type="hidden" id="article_content" value=" " name="article_content">
										  <div class="loading_wrap" id="loading_gif"><img src="${pageContext.request.contextPath}/aiaas/en/images/spinner.gif" alt="spinner"></div>
										  <ul class="result_search" id="search_results">
											  <li class="article_item" name=""><a> </a></li>
											  <li class="article_item" name=""><a> </a></li>
											  <li class="article_item" name=""><a> </a></li>
											  <li class="article_item" name=""><a> </a></li>
											  <li class="article_item" name=""><a> </a></li>
										  </ul>
										  <button type="button" class="btn cont_btn" id="insert_selected_content">OK</button>
									  </div>
								  </div>
								 <!--//.Modal content -->
							</div>
							<!-- //.Modal -->
							<div class="btn_area">							
								<button type="button" class="start_btn" id="classify_content" disabled><em class="fas fa-file-invoice"></em>Process</button>
								<%--<span class="disBox"></span>--%>
							</div>
						</div>
						
						<!--.xdc_result_wrap-->						
						<div class="xdc_result_wrap">
							<div class="resultBoxTop">								
									<div class="tit"><span>XDC Result</span></div>
									<div class="xdc_recod">
										<!-- recording -->
										<div class="recording">
											<span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>
											<span></span><span></span><span></span><span></span><span></span><span></span><span></span>
										</div>
										<!-- //recording -->
									</div>
									<div class="txt" id="xdc_result_classification">
										<ol class="xdc_lst">
											<%-- <li>
												<span class="rank"><b>1st</b></span>
												<span class="label">IT</span>
												<span class="probability">
													<em>0.012341234</em>
												</span>
											</li>
											<li>
												<span class="rank"><b>2nd</b></span>
												<span class="label">모바일</span>
												<span class="probability">
													<em>0.012341234</em>
												</span>
											</li>
											<li>
												<span class="rank"><b>3rd</b></span>
												<span class="label">소프트웨어</span>
												<span class="probability">
													<em>0.012341234</em>
												</span>
											</li> --%>
										</ol>
									</div>
							  </div>
							  <div class="result_desc">
                                  	<span><img src="${pageContext.request.contextPath}/aiaas/en/images/mouse.png" alt="mouse icon"> Move your mouse over words or sentences. It will show the analysis of the words or sentences.</span> 
                              </div>
							   <div class="resultBoxBottom">
                                    <div class="balloon" id="xdc_result_weights" style="width:200px;">
                                        <div id="sentenceWeight"></div>
                                        <div id="wordWeight"></div>
                                    </div>
                                    <!--  <div class="txt" id="xdc_result_xdc" >『국어의 <span>기술</span>외전 독서활동 추천도서: 미래를 바꾼 아홉 가지 알고리즘』 소개
기왕에 <span>독서</span>를 해야 한다면 시험에도 도움이 될 만한 책을 고르는 것이 중요하다. 그런데 학생 입장에서 어떤 책이 수능 국어영역과 학생부 독서활동 모두에 도움이 되는지 판단하기 어렵다. 『국어의 기술 외전 독서활동 추천도서』는 적합한 책을 추천하고, 또 해당 책과 관련된 [한국교육과정평가원 기출지문+‘국어의 기술’식 해설]을 제공한다. 국어시험 점수,
 학생부 독서활동이라는 두 마리 토끼를 한꺼번에 잡을 수 있기 바란다.
『미래를 바꾼 아홉 가지 알고리즘』 소개
오늘날 우리는 궁금한 것이 있으면 인터넷 검색엔진에 검색어를 입력하고, 필요한 물건이 있으면 인터넷 쇼핑몰에서 구입한다. 일상생활의 모든 활동을 컴퓨터로 하고 있다고 해도 과언이 아니다. 그렇다면 우리가 컴퓨터를 사용할 때, 컴퓨터 안에서는 무슨 일이 일어나고 있을까? 존 맥코믹은 이를 가능케 한 아홉 가지 위대한 알고리즘 이면의 기본적 아이디어를 다양한 비유와 예를 활용해 쉽고 정확하게 설명한다. 독자들은 이 아홉 가지 알고리즘에 관한 설명에서 검색엔진, 데이터 압축, 암호화, 오류 정정, 패턴 인식 등 우리가 매일 이용하는 컴퓨터 기술의 이론적 아이디어를 흥미진진하게 이해할 수 있다.
기왕에 독서를 해야 한다면 시험에도 도움이 될 만한 책을 고르는 것이 중요하다. 그런데 학생 입장에서 어떤 책이 수능 국어영역과 학생부 독서활동 모두에 도움이 되는지 판단하기 어렵다. 『국어의 기술 외전 독서활동 추천도서』는 적합한 책을 추천하고, 또 해당 책과 관련된 [한국교육과정평가원 기출지문+‘국어의 기술’식 해설]을 제공한다. 국어시험 점수, 학생부 독서활동이라는 두 마리 토끼를 한꺼번에 잡을 수 있기 바란다.

앤서니 브라운
저자 앤서니 브라운
1946년 영국에서 태어났다. LEEDS COLLEGE OF ART에서 그래픽 디자인을 공부한 뒤, 3년 동안 맨체스터 왕립 병원에서 의학 전문 화가로 일했다. LEEDS COLLEGE OF ART에서 파트 타임으로 학생들을 가르쳤고 15년 동안 GORDON FRASER 갤러리에서 연하장을 디자인했다. 
아주 우연히 를 그리게 되면서 본격적인 그림책 작가가 되었다. 1983년 고릴라로 영국 도서관 협회에서 그 해 최고의 그림책에 주는 '케이트 그린어웨이' 상과 '커트 매쉴러' 상을 받았고 동물원으로 두 번째 '케이트 그린어웨이' 상을 받았다.
독특하고 뛰어난 작품으로 세계적으로 높은 평가와 주목을 받고 있는 작가로 군더더기 하나 없는 완벽한 구성, 간결하면서도 유머가 넘치는 글
</div> -->		
										<div class="txt" id="xdc_result_xdc" ></div>
							  </div>	
							  <!--.btn_area-->
							  <div class="btn_area">							
								<button type="button" class="btn_goback" id="back_to_article">Reset</button>
<!--							<button type="button" class="btn_save" id="">Save</button>	-->
							  </div>	
						</div>
						<!--//.xdc_result_wrap-->	
					</div>						
					<!--//.xdc_box-->			
					
				</div>
				<!-- .demobox_xdc -->
				<!--.xdcmenu-->
				<div class="demobox" id="xdcmenu">
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API Guideline
							</div>
							<p class="sub_title">Set up Environment</p>
							<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
							<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
							<p class="sub_title">ID &amp; Key</p>
							<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
							<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
							<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
								<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
							<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
							<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
							<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
						</div>
						<div class="guide_group">
							<div class="title">
								Bert XDC(eXplainable Document Classifier)
							</div>
							<p class="sub_txt">Deep learning based eXplainable Document Classifier (XDC) engine that not only categorizes text by classification criteria but also finds sentences and words that are the reason for classification.</p>

							<span class="sub_title">
								Preparation
							</span>
							<p class="sub_txt">① Input: Passage (text)</p>
							<p class="sub_txt">② Model: Choose one below </p>
							<ul>
								<li>Korean News classification (default)</li>
							</ul>
							<span class="sub_title">
							 API Document
							</span>
							<p class="sub_txt">① Request  </p>
							<ul>
								<li>Method : POST</li>
								<li>URL : https://api.maum.ai/api/bert.xdc/</li>
							</ul>
							<p class="sub_txt">② Request parameters </p>
							<table>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th>type</th>
								</tr>
								<tr>
									<td>apiId</td>
									<td>Unique API ID. Request from is required for the ID. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>apiKey </td>
									<td>Unique API key. Request from is required for the key. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>context</td>
									<td>Input passage (text) </td>
									<td>string</td>
								</tr>
							</table>
							<p class="sub_txt">③ Request example </p>
							<div class="code_box">
<pre>
curl -X POST \
https: //api.maum.ai/api/bert.xdc \
-H 'Content-Type: application/json' \
-d '{
	"apiId": "(*Request for ID)",
	"apiKey": "(*Request for key)",
	"context": "지난해 야구 국가대표 사령탑 지휘봉을 스스로 내려놓은 국보급 투수 선동열 전 감독이 새로운 도전에
   나섭니다. 선 전 감독은 오늘(11일) 서울 목동구장에서 기자 회견을 열어 내년 미국프로야구 메이저리그 뉴욕 양키스 구단의
   스프링캠프에 참가해 메이저리그 선진 야구를 배울 생각이라고 밝혔습니다. 기자회견에 동석한 스티브 윌슨 양키스 국제담당 총괄
   스카우트는 양키스 구단이 일본 지도자를 구단에 초청한 적은 있지만, 한국 지도자는 최초로 초청한다며 영광으로 생각한다고
   덧붙였습니다. 자세한 인터뷰 내용은 영상으로 확인하시죠. "
}</pre>
							</div>

							<p class="sub_txt">④ Response parameters </p>
							<span class="table_tit">Response</span>
							<table>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th>Type</th>
								</tr>
								<tr>
									<td>message</td>
									<td>Status of API request</td>
									<td>list</td>
								</tr>
								<tr>
									<td>labels</td>
									<td>Classification result list</td>
									<td>list</td>
								</tr>
								<tr>
									<td>wordIndices</td>
									<td>List of words that affected the classification results</td>
									<td>list</td>
								</tr>
								<tr>
									<td>sentenceIndices</td>
									<td>List of sentences that affected the classification results</td>
									<td>list</td>
								</tr>
							</table>
							<span class="table_tit">message: Status of API request</span>
							<table>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th>Type</th>
								</tr>
								<tr>
									<td>message</td>
									<td>Status (Success/ Fail)</td>
									<td>string</td>
								</tr>
								<tr>
									<td>status</td>
									<td>Status code (0: Success)</td>
									<td>number</td>
								</tr>
							</table>
							<span class="table_tit">labels: Classification result list</span>
							<table>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th>Type</th>
								</tr>
								<tr>
									<td>label</td>
									<td>Classification result (Order by highest reliability) </td>
									<td>string</td>
								</tr>
								<tr>
									<td>probability</td>
									<td>Reliability of the result (0~1) </td>
									<td>number</td>
								</tr>
							</table>
							<span class="table_tit">wordIndices: List of words that affected the classification results</span>
							<table>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th>Type</th>
								</tr>
								<tr>
									<td>startIdx</td>
									<td>Start point of the word </td>
									<td>int</td>
								</tr>
								<tr>
									<td>endIdx</td>
									<td>End point of the word</td>
									<td>int</td>
								</tr>
								<tr>
									<td>weight</td>
									<td>Word weight(0~1) </td>
									<td>number</td>
								</tr>
							</table>
							<span class="table_tit">sentenceIndices: List of sentences that affected the classification results</span>
							<table>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th>Type</th>
								</tr>
								<tr>
									<td>startIdx</td>
									<td>Start point of the sentence</td>
									<td>int</td>
								</tr>
								<tr>
									<td>endIdx</td>
									<td>End point of the sentence</td>
									<td>int</td>
								</tr>
								<tr>
									<td>weight</td>
									<td>Sentence weight(0~1)</td>
									<td>number</td>
								</tr>
							</table>
							<p class="sub_txt">⑤ Response example </p>
							<div class="code_box">
							<pre>
{
	"message": {
		"message": "Success",
		"status": 0
	},
	"labels": [
		{
			"label": "야구",
			"probability": 0.9995629191398621
		},
		{
			"label": "축구",
			"probability": 0.00013259267143439502
		},
		{
			"label": "스포츠기타",
			"probability": 0.00008373271703021601
		}
	],
	"wordIndices": [
		{
			"startIdx": 4,
			"endIdx": 6,
			"weight": 0.16227564215660095
		},
		{
			"startIdx": 79,
			"endIdx": 83,
			"weight": 0.2021310180425644
		},
		{
			"startIdx": 143,
			"endIdx": 145,
			"weight": 0.10961653292179108
		}
	],
	"sentenceIndices": [
		{
			"startIdx": 0,
			"endIdx": 60,
			"weight": 0.06997623294591904
		},
		{
			"startIdx": 162,
			"endIdx": 266,
			"weight": 0.099941685795784
		}
	]
}
</pre>

							</div>
						</div>
					</div>
					<!--//.guide_box-->
				</div>
				<!--//xdcmenu-->
				<!--.xdcexample-->
				<div class="demobox" id="xdcexample">
					<p><em style="font-weight: 400;color:#27c1c1;">Use Cases</em>  </p>
					<span class="sub">Find out how AI can be applied to diverse area.</span>
					<%--텍스트 분류(XDC)--%>
					<div class="useCasesBox">
						<ul class="lst_useCases">
							<li>
								<dl>
									<dt>
										<em>CASE 01</em>
										<span>Long text classification</span>
									</dt>
									<dd class="txt">You can classify categories, intentions, and emotions into long texts that were difficult to classify with traditional classifiers.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_xdc"><span>XDC</span></li>
											<li class="ico_nlu"><span>NLU</span></li>
											<li class="ico_hmd"><span>HMD</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>Unlimited Category Classification</span>
									</dt>
									<dd class="txt">You can use it to automatically classify text as many times as you like, without limiting the number of categories.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_xdc"><span>XDC</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 03</em>
										<span>Customer Satisfaction Analysis</span>
									</dt>
									<dd class="txt">In addition to automatically categorizing customer intent, it also tracks the reason of classification, enabling precise customer satisfaction analysis.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_xdc"><span>XDC</span></li>
											<li class="ico_stt"><span>STT</span></li>
											<li class="ico_nlu"><span>NLU</span></li>
											<li class="ico_hmd"><span>HMD</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 04</em>
										<span>News Supplementary Service</span>
									</dt>
									<dd class="txt">The latest news can be categorized across fields and used to implement various value-added services.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_xdc"><span>XDC</span></li>
											<li class="ico_mrc"><span>MRC</span></li>
											<li class="ico_bot"><span>Chatbot</span></li>
										</ul>
									</dd>
								</dl>
							</li>
						</ul>
					</div>
					<!-- //텍스트 분류(XDC) -->
				</div>
				
			</div>

		</div>
		<!-- //.contents -->



<script type="text/javascript">   
$(window).load(function() {    
	$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() { 
		$(this).remove(); 
	});
}); 
</script>

<script type="text/javascript">
jQuery.event.add(window,"load",function(){
	$(document).ready(function (){	
		//XDC

		$('.xdc_box .btn_area button').removeClass('disable');
		$('.xdc_box .btn_area button').removeAttr('disabled');
		$('.xdc_box .btn_area button').addClass('start_btn');

		$('.xdc_result_wrap').hide();
		
		$('.btn_article').on('click', function () {
			$('.demobox_xdc .modal').fadeIn(200);
			$("#loading_gif").hide();	
		});
		
		$('#insert_selected_content').on('click', function () {
			$('.demobox_xdc .modal').fadeOut(300);	
			$('body').css({
				'overflow': '',
			});
			$('.xdc_box .btn_area button').removeClass('disable');
			$('.xdc_box .btn_area button').removeAttr('disabled');
			$('.xdc_box .btn_area button').addClass('start_btn');
			$('.progress li:nth-child(2)').addClass('active');
			$('.xdc_box .btn_area .disBox').remove();
		});	
		
		$('.xdc_box .text_area textarea').on('input keyup paste', function() {			
			var txtValLth = $(this).val().length;			
			if ( txtValLth > 0) {
				$('.progress li:nth-child(2)').addClass('active'); 		
				$('.xdc_box .btn_area button').removeClass('disable');
				$('.xdc_box .btn_area button').removeAttr('disabled');
				$('.xdc_box .btn_area button').addClass('start_btn');
			} else {
				$('.progress li:nth-child(2)').removeClass('active');
				$('.xdc_box .btn_area button').addClass('disable');
				$('.xdc_box .btn_area button').attr('disabled');
				$('.xdc_box .btn_area button').removeClass('start_btn');
			}
		});
		
		// product layer popup     
		$('.close, .lyr_bg').on('click', function () {
			$('.demobox_xdc .modal').fadeOut(300);			
			$('body').css({
				'overflow': '',
			});
		});
		
//		step 2
		$('#classify_content').on('click', function () {
			$(this).parent().parent().hide();
			$('.xdc_result_wrap').show();
			$('.progress li:nth-child(3)').addClass('active'); 	
		});
		
		
		$('.btn_goback').on('click', function () {
            // $('#id_input_text').val("");
			$('.xdc_result_wrap').hide();
			$('.xdc_wrap').show(); 	
			$('.progress li:nth-child(1)').addClass('active'); 	
			$('.progress li:nth-child(2)').removeClass('active');
			$('.progress li:nth-child(3)').removeClass('active');
		});

		//XDC
		var hideResult = true;
		$("#classify_content").click(function(){

		    //$('#xdc_article_wrap').css('display', 'none');
		    //$('.demo_recording').css('display', 'block');
			$("#xdc_article_wrap").hide();
			$("#loading_gif").show();

			/* if(hideResult){
				var txt = document.getElementById('id_input_text').value;
				console.log("this is classify_content");
				console.log(txt);
				var sentences;
				var cls;
				var sent_attns;
				$.ajax({
					type: 'POST',
					url: '/xdc/xdcApi/',
					data: {
					    "text": txt,
                        "cmd": "runXDC",
                        "ID": "minds-api-sales-demo",
                        "key": "814826d160684a489dcbeb7445ecd644",
                        '${_csrf.parameterName}' : '${_csrf.token}'
                    },
					error: function(xhr, status, error){
						$("#xdc_article_wrap").show();
						$("#loading_gif").hide();
						alert('[에러] 문단을 분석하는 데에 실패했습니다.');
						console.dir(xhr.responseText);
						console.dir(status);
						console.dir(error);
					}, success: function(data) {
						if(data['status']=="error"){
							console.dir(data);
							alert('[에러] 문단을 분석하는 데에 실패했습니다.');
							return;
						}else{
							cls = data['data']['cls'];
	                        sent_attns = data['data']['sent_attns'];
	                        // console.log(data['data']);

	                        var top_sent_indices = data['data']['top_sent_indices'];
	                        var top_word_indices_lists = data['data']['top_word_indices_lists'];
	                        sentences = data['data']['sentences'];
	                        

	                        $("#xdc_result_wrap").css("display", "block");
	                        // $("#xdc_article_wrap").css("display", "none");
	                        $('.demo_recording').css('display', 'none');
	                        hideResult = false;
	                        for (var i = 0; i < cls.length; i++){
	                            $(".xdc_lst").append("<li><span class='rank'><b>"
	                            +(i + 1)
	                            + "</b>위</span><span class='label'>"
	                            + (cls[i]["label"]) + "</span><span class='probability'><em>"
	                            + parseFloat(cls[i]["probability"]).toFixed(6)
	                            + "</em></span></li>");
	                        }	                        

	                        for (var i = 0; i < sent_attns.length; i++){
	                            var word_attns = sent_attns[i].word_attns;
	                            var sent_weight = sent_attns[i]["weight"];
	                            sent_weight = parseFloat(sent_weight).toFixed(6);
	                            var start = -1;
	                            var end = -1;
	                            var appendBlank = true;
	                            for(var j = 0; j < word_attns.length; j++){
	                                //1st trial
	                                if(start != word_attns[j]['start']){
	                                    start = word_attns[j]['start'];
	                                    if(start == end) {
	                                        appendBlank = false;
	                                    } else {
	                                        appendBlank = true;
	                                    }
	                                    end = word_attns[j]['end'];
	                                    if (appendBlank) {
	                                        // console.log("logs: ", sentences[i], start, end);
	                                        var word = "<span class=s_" + i + " id=s_" + i + "_w_" + j + ">&nbsp;" + sentences[i].substring(start, end) + "</span>";
	                                    } else {
	                                        // console.log("logs: ", sentences[i], start, end);
	                                        var word = "<span class=s_" + i + " id=s_" + i + "_w_" + j + ">" + sentences[i].substring(start, end) + "</span>";
	                                    }
	                                    $("#xdc_result_xdc").append(word);
	                                    var word_weight = word_attns[j]['weight'];
	                                    word_weight = parseFloat(word_weight).toFixed(6);
	                                    $("#s_" + i + "_w_" + j).attr('onmouseover', "showWeight(" + i + "," + j + "," + sent_weight + "," + word_weight + ")" );
	                                    $("#s_" + i + "_w_" + j).attr('onmouseout', "hideWeight()" );
	                                }
	                            }
	                            if(top_sent_indices.indexOf(i) > -1) {
	                                $(".s_" + i).css("color", "#000000");
	                                $(".s_" + i).css("font-weight", "bold");
	                                for (j in top_word_indices_lists[i]) {	//영향력 높은 문장들 내의 영향력 높은 단어들에 대해서만 하이라이트 표시
	                                    $("#s_" + i + "_w_" + top_word_indices_lists[i][j]).css("color", "#ff6000");
	                                    $("#s_" + i + "_w_" + top_word_indices_lists[i][j]).css("font-weight", "bold");
	                                }
	                            }
	                            $("#xdc_result_xdc").append("<br>");
	                        }
	                        //var offset = $("#xdc_result_wrap").offset().top - 200;
	                        //$("html,body").animate({scrollTop: offset}, 1000);
							$("#xdc_result_wrap").css("display", "block");
							$("#loading_gif").hide();
							
						}
						
                    }
				}).done(function(data){
					console.dir(data)
				});
			}
		}); */
			if(hideResult){
				var txt = document.getElementById('id_input_text').value;
				//var language = document.getElementById('lang').value;
				console.log("this is classify_content");
				console.log(txt);
				var sentences;
				var cls;
				var sent_attns;
				$.ajax({
					type: 'POST',
					url: '/api/xdc/xdcApiv20/',
					data: {
						"context": txt,
						"apiId": "minds-api-performance-test",
						"apiKey": "4SBRGrq9",
						/* "language": language, */
						'${_csrf.parameterName}' : '${_csrf.token}'
					},
					error: function(xhr, status, error){
						$("#xdc_article_wrap").show();
						$("#loading_gif").hide();
						alert('[Server Error]');
						console.dir(xhr.responseText);
						console.dir(status);
						console.dir(error);
					}, success: function(data) {
						if(data['message']['status'] != 0) {
							console.dir(data);
							alert('[Server Error]');
							return;
						} else {
							var labels = data['labels'];
							for(var i=0; i<labels.length; i++) {
								$(".xdc_lst").append("<li><span class='rank'><b>"
									+(i + 1)
									+ "</b>위</span><span class='label'>"
									+ (labels[i]["label"]) + "</span><span class='probability'><em>"
									+ parseFloat(labels[i]["probability"]).toFixed(6)
									+ "</em></span></li>");
							}
							
							var sentenceIndices = data['sentenceIndices'];
							var wordIndices = data['wordIndices'];
							var sentence = txt;
							var appendedSentenceOffset = 0;
							
							// 1. 주요 문장 단위를 기준으로 배열로 나눈다.
							var sentenceArray = [];
							//var weightArray = [];
							var index = [];
							var lastEndSentIdx = 0;
							if(sentenceIndices != null && sentenceIndices.length > 0) {
								for(var s=0; s<sentenceIndices.length; s++) {
									var sent_weight = sentenceIndices[s]['weight'];
									var startSentIdx = sentenceIndices[s]['startIdx'];
									var endSentIdx = sentenceIndices[s]['endIdx'];
									
									// 문장별 인덱스 값을 저장
									// 첫문단 저장
									if(s==0 && startSentIdx > 0) {
										var indexSet = new Map();
										sentenceArray.push(sentence.substring(0, startSentIdx));
										indexSet.set("start", 0);
										indexSet.set("end", startSentIdx);
										index.push(indexSet);
									}
									// 낀 문장 저장
									else if(startSentIdx > lastEndSentIdx) {
										var indexSet = new Map();
										sentenceArray.push(sentence.substring(lastEndSentIdx, startSentIdx));
										indexSet.set("start", lastEndSentIdx);
										indexSet.set("end", startSentIdx);
										index.push(indexSet);
									}
									
									var indexSetModel = new Map();
									// 목록 문장 저장
									sentenceArray.push(sentence.substring(startSentIdx, endSentIdx));
									indexSetModel.set("start", startSentIdx);
									indexSetModel.set("end", endSentIdx);
									indexSetModel.set("weight", sent_weight);
									index.push(indexSetModel);
									
									// 끝 문장 저장
									if(s==sentenceIndices.length-1 && endSentIdx < txt.length) {
										var indexSet = new Map();
										sentenceArray.push(sentence.substring(endSentIdx));
										indexSet.set("start", endSentIdx);
										indexSet.set("end", txt.length);
										index.push(indexSet);
									}
									lastEndSentIdx = endSentIdx;
								}
							} else {
								// 문장 주요도 정보가 없을 때
								var indexSetModel = new Map();
								indexSetModel.set("start", 0);
								indexSetModel.set("end", txt.length);
								index.push(indexSetModel);
								
								sentenceArray.push(txt);
							}
							
							// 4. 태그와 이벤트를 단어에 삽입 한 후
							// 5. 치환한다.
							var spanOpen = "";
							var spanClose = "";
							if(wordIndices != null && wordIndices != 'undefined') {
								for(var i=wordIndices.length-1; i>=0; i--) {
									var word_weight = wordIndices[i]['weight'];
									var startIdx = wordIndices[i]['startIdx'];
									var endIdx = wordIndices[i]['endIdx'];
									var sentString = sentence;
									
									// 2. 단어가 주요 문장 내에 속해 있는지 판단하고 (모든 문장 Loop가 가능)
									for(var x=0; x<index.length; x++) {
										var mapStart = index[x].get('start');//["start"];
										var mapEnd = index[x].get('end');//["end"];
										var sent_weight = index[x].get('weight');
	
										if(startIdx >= mapStart && endIdx <= mapEnd) {
											// 3. 해당되는 문장 배열을 찾아서
											sentString = sentenceArray[x];
											startIdx = startIdx - mapStart;
											endIdx = endIdx - mapStart;
	
											spanOpen = "<span class=s_" + x + "_w_" + i + " id=s_" + x + "_w_" + i + " style='color: rgb(255, 96, 0); font-weight: bold;' onmouseover=showWeight(" + x + "," + i + "," + sent_weight + "," + word_weight + ") onmouseout=hideWeight()>";
											spanClose = "</span>";
											sentString = sentString.substring(0,startIdx)+spanOpen+sentString.substring(startIdx, endIdx)+spanClose+sentString.substring(endIdx);
											
											sentenceArray[x] = sentString;
										}
									}
								}
							}
							for(var x=0; x<index.length; x++) {
								var sent_weight = index[x].get('weight');
								if(sent_weight != null) {
									var spanOpen = "<span class=s_" + x + "_w_" + 9999 + " id=s_" + x + "_w_" + 9999 + " style='font-weight: bold;' onmouseover=showWeight(" + x + "," + 9999 + "," + sent_weight + ",null" + ") onmouseout=hideWeight()>";
									var spanClose = "</span>";
									var sentString = sentenceArray[x];
									
									sentString = spanOpen + sentString + spanClose;
									sentenceArray[x] = sentString;
								}
							}
							for(var s=0; s<sentenceArray.length; s++) {
								var resultXdc = $("#xdc_result_xdc").append(sentenceArray[s]);
							}
						}
					}
				}).done(function(data){
					console.dir(data)
				});
			}
		});
		$("#back_to_article").click(function() {
			$("#xdc_result_wrap").css("display", "none");
			$("#xdc_article_wrap").css("display", "block");
			$(".xdc_lst").empty();
			$("#xdc_result_xdc").empty();
			hideResult = true;
		});
		$("#insert_selected_content").click(function() {
			var txt = $("#article_content").val();
			document.getElementById("id_input_text").value = txt;
			document.getElementById("news_modal").style.display = "none";
			document.getElementById("search_input").value = "";
			$(".article_item").empty();
			$(".article_item").append("<a> </a>");
			$(".article_item").removeClass('chosen');
			document.getElementById("article_content").value = "";
		});
 		$('#search_input').keydown(function(e){
			if(e.keyCode==13)
			    $('#search_button').trigger('click');
		}); 
		$("#search_button").click(function(){
			var txt = document.getElementById("search_input").value;
			if (txt == null || txt == ""){
				alert("Please enter keywords for search.");
				return;
			}			
			var param = {'keyword' : txt, '${_csrf.parameterName}' : '${_csrf.token}'};
			$("#loading_gif").show();
			$.ajax({
				type: 'POST',
				url: '/api/xdc/search_news/',
				dataType: 'json',
				data: param,
				async:false,
				error: function(xhr, status, error){
					alert('[Error] Search term not found.');
					$("#loading_gif").hide();
					$("#search_results").show();
				}, success: function(data) {
// 					data = data['items']
					console.log(data);
					$(".article_item").empty();
					$(".article_item").append("<a> </a>");
					for (i = 0; i < Object.keys(data).length; i++){
						//console.log(data[i]["desc"]);
						$($("#search_results").children("li")[i]).attr('name', data[i]["desc"]);
						$($("#search_results").children("li")[i]).empty();
						$($("#search_results").children("li")[i]).append("<a>" + data[i]["title"]+"</a>");
					}
					$("#loading_gif").hide();
					$("#search_results").show();
				}
			});
		});
		$(".article_item").click(function(){
			var results = document.getElementById("search_results");
			$(".article_item").removeClass('chosen');
			document.getElementById("article_content").value = $(this).attr('name');
			$(this).toggleClass('chosen');
		});

		$("#naver_search_option").click(function(){
			document.getElementById("news_modal").style.display = "block";
			$("#loading_gif").hide();
			document.getElementById("search_input").focus();
		});
		$(".close").click(function(){
			document.getElementById("myModal").style.display = "none";
			document.getElementById("news_modal").style.display = "none";
			$(".article_item").removeClass('chosen');
			document.getElementById("article_content").value = "";
		});
	});	
});	

var wordFrame = false;
function showWeight(i, j, sent_w, word_w){
	/* $("#sentenceWeight").html("");
	$("#wordWeight").html(""); */
	
	if(!wordFrame && sent_w != null) {
		word_left = $("#s_" + i + "_w_" + j).position().left + 5;
		word_top = $("#s_" + i + "_w_" + j).position().top - 50;
		$("#xdc_result_weights").css('visibility', "visible");
		$("#xdc_result_weights").css('left', word_left);
		$("#xdc_result_weights").css('top', word_top);
		
		$("#sentenceWeight").html("Sentence influence: "+sent_w.toFixed(6));
	}
	if(word_w != null) {
		word_left = $("#s_" + i + "_w_" + j).position().left + 5;
		word_top = $("#s_" + i + "_w_" + j).position().top - 55;
		$("#xdc_result_weights").css('visibility', "visible");
		$("#xdc_result_weights").css('left', word_left);
		$("#xdc_result_weights").css('top', word_top);
		
		$("#sentenceWeight").html("");
		$("#wordWeight").html("Word influence: "+word_w.toFixed(6));
		wordFrame = true;
	}
}

function hideWeight(){
	$("#xdc_result_weights").css('visibility', "hidden");
	$("#sentenceWeight").html("");
	$("#wordWeight").html("");
	wordFrame = false;
}
</script>


<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/xdc.js"></script>

