﻿<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<!-- .contents -->
<div class="contents">
	<div class="content api_content">
		<h1 class="api_tit">HMD</h1>
		<ul class="menu_lst">
			<li class="tablinks" onclick="openTap(event, 'hmddemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
			<li class="tablinks" onclick="openTap(event, 'hmdexample')"><button type="button">Use Case</button></li>
			<li class="tablinks" onclick="openTap(event, 'hmdmenu')"><button type="button">Manual</button></li>
<%--			<li class="tablinks"><a href="/member/enApiAccount" >API ID, key</a></li>--%>
		</ul>
		<div class="demobox demobox_hmd" id="hmddemo">
			<p><strong style="font-weight:normal;font-size:25px;color: #2c3f51;">Classification of emotional pattern via</strong> <span>HMD</span> <small>(Hierarchical Multiple Dictionary) </small> </p>
			<!--				<span class="sub">문장에 들어 있는 감정표현을 분석합니다</span>-->
			<span class="sub">In reference to HMD consisted of morphemes, categorizes sentences and detects patterns of sentences.</span>
			<form>
				<!--.hmd_box-->
				<div class="hmd_box">
					<div class="" style ="width: 328px; height: 40px; border-radius: 5px; border: 1px solid #dce2f2; margin: 0 auto 25px; line-height: 40px;">
						<span style="font-size:13px;margin-right:28px; ">Language</span>
						<div class="radio">
							<input type="radio" name="language" id="language_en" checked value="eng"/>
							<label for="language_en">English</label>
						</div>
						<div class="radio">
							<input type="radio" name="language" id="language_ko" value="kor"/>
							<label for="language_ko">Korean</label>
						</div>

					</div>
<%--						<dl class="radio_area">--%>
<%--							<dt>Language</dt>--%>
<%--							<dd class="type_radio"><input type="radio" name="language" id="language_ko" checked value="kor"/><label for="language_ko">Korean</label></dd>--%>
<%--							<dd class="type_radio"><input type="radio" name="language" id="language_en" value="eng"/><label for="language_en">English</label></dd>--%>
<%--						</dl>--%>

					<div class="text_area">
						<textarea rows="12" id="text-contents" maxlength="1000" placeholder="Please type in sentences or upload text files.">:: Example of Positive expression ::
Awesome! Thank you for your good behavior.
I am very happy and astonished because of you.

:: Example of Negative expression ::
It was a horrible, unpleasant memory.
You disappointed me and made me miserable... I'll never forgive you again.</textarea>
					</div>
					<div class="text_info">
						<p>*Please type in words within 1,000 words and less than 30KB [txt format]</p>
						<span class=""><strong id="count">0</strong>/1000</span>
					</div>
				</div>
				<!--//.hmd_box-->
				<!--.btn_area-->
				<div class="btn_area btn_area_815 step_1btn">
					<div class="btn_float_l">
						<input type="file" id="file-down" class="demoFile" accept=".txt"/><label for="file-down" class="btn_type btn_gray">Upload Files</label>
					</div>
					<button type="button" id="hmdbtn" class="btn_type btn_blue"><em class="fas fa-file-invoice"></em>Process</button>
				</div>
				<!--//.btn_area-->
			</form>

			<!--posi_nega_area-->
			<div class="posi_nega_area">
				<!--posi_box-->
				<div class="posi_box">
					<div class="tit_posi"><span>Positive Expressions</span></div>
					<div class="posiNega_area">
						<ul class="tab_posi_nega">
							<li><span>Low</span></li>
							<li><span></span></li>
							<li><span></span></li>
							<li><span>High</span></li>
						</ul>
						<ul class="tab_posi_nega2">
							<li id='Positive_Cnt_1'></li>
							<li id='Positive_Cnt_2'></li>
							<li id='Positive_Cnt_3'></li>
							<li id='Positive_Cnt_4'></li>
						</ul>

						<div class="posi_nega_conts">
							<div class="posi_nega_list">
								<ul id='Positive_1'>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul id='Positive_2'>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul id='Positive_3'>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul id='Positive_4'>
								</ul>
							</div>
<%--							<a href="#none" class="btn_more">More</a>--%>
						</div>
					</div>
				</div>
				<!--//.posi_box-->
				<!--.nega_box-->
				<div class="nega_box">
					<div class="tit_posi tit_nega"><span>Negative Expressions</span></div>
					<div class="posiNega_area">
						<ul class="tab_posi_nega tab_nega">
							<li><span>Low</span></li>
							<li><span></span></li>
							<li><span></span></li>
							<li><span>High</span></li>
						</ul>
						<ul class="tab_posi_nega2">
							<li id='Negative_Cnt_1'></li>
							<li id='Negative_Cnt_2'></li>
							<li id='Negative_Cnt_3'></li>
							<li id='Negative_Cnt_4'></li>
						</ul>

						<div class="posi_nega_conts01">
							<div class="posi_nega_list">
								<ul id='Negative_1'>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul id='Negative_2'>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul id='Negative_3'>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul id='Negative_4'>
								</ul>
							</div>
<%--							<a href="#none" class="btn_more">More</a>--%>
						</div>
					</div>
				</div>
				<!--//.nega_box-->

				<!--.btn_area-->
				<div class="btn_area btn_area_815">
					<button type="button" class="btn_type btn_blue btn_goback"><em class="fas fa-redo-alt"></em>Reset</button>
				</div>
				<!--//.btn_area-->

			</div>
			<!--//posi_nega_area-->
		</div>
		<!-- .demobox -->

		<!--.hmdmenu-->
		<div class="demobox" id="hmdmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API Guideline
					</div>
					<p class="sub_title">Set up Environment</p>
					<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
					<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
					<p class="sub_title">ID &amp; Key</p>
					<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
					<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
					<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
						<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
					<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
					<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
					<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
				</div>
				<div class="guide_group">
					<div class="title">
						Hierarchical-Multiple-Dictionary (HMD)
					</div>
					<p class="sub_txt">Detects and categorizes words predefined by the user in text and is used for services that require precise classification.
					</p>

					<span class="sub_title">
						Preparation
					</span>
					<p class="sub_txt">① Input: String (text)</p>
					<p class="sub_txt">② Language: Choose one below </p>
					<ul>
						<li>Korean (kor)</li>
						<li>English (eng)</li>
					</ul>
					<span class="sub_title">
						API Document
					</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/hmd/</li>
					</ul>
					<p class="sub_txt">② Request parameters</p>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId</td>
							<td>Unique API ID. Request from is required for the ID.</td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>Unique API key. Request from is required for the key.</td>
							<td>string</td>
						</tr>
						<tr>
							<td>lang</td>
							<td>Selected language (kor / eng) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>reqText</td>
							<td>Input string (text) </td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request example </p>
					<div class="code_box">
<pre>
curl -X POST \
 https: //api.maum.ai/api/hmd \
 -H 'Content-Type: application/json' \
 -d '{
    "apiId": "(*Request for ID",
    "apiKey": "(*Request for key)",
    "lang": "eng",
    "reqText": "Today’s weather makes me happy."
}'
</pre>
					</div>

					<p class="sub_txt">④ Response parameters </p>
					<span class="table_tit">Response</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>message</td>
							<td>Status of API request</td>
							<td>list</td>
						</tr>
						<tr>
							<td>document</td>
							<td>Morphological analysis result</td>
							<td>list</td>
						</tr>
						<tr>
							<td>cls </td>
							<td>HMD classification result</td>
							<td>list</td>
						</tr>
					</table>
					<span class="table_tit">message: Status of API request</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>message</td>
							<td>Status (Success/ Fail)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>status</td>
							<td>Status code (0: Success)</td>
							<td>number</td>
						</tr>
					</table>

					<span class="table_tit">document: Morphological analysis result</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>category Weight</td>
							<td>Unused</td>
							<td>-</td>
						</tr>
						<tr>
							<td>sentences</td>
							<td>Morphological analysis result by sentence </td>
							<td>array</td>
						</tr>
					</table>

					<span class="table_tit">sentences: Morphological analysis result by sentence</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>sequence</td>
							<td>Sentence number (n)</td>
							<td>int</td>
						</tr>
						<tr>
							<td>text</td>
							<td>Original text</td>
							<td>string</td>
						</tr>
						<tr>
							<td>words</td>
							<td>Word phrase info </td>
							<td>list</td>
						</tr>
						<tr>
							<td>morps</td>
							<td>Morphological info </td>
							<td>list</td>
						</tr>
						<tr>
							<td>morphEvals</td>
							<td>Morphological analysis result by word phrase</td>
							<td>list</td>
						</tr>
					</table>

					<span class="table_tit">words: Word phrase info</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>seq</td>
							<td>Word phrase ID</td>
							<td>int</td>
						</tr>
						<tr>
							<td>text</td>
							<td>Word phrase</td>
							<td>string</td>
						</tr>
						<tr>
							<td>begin </td>
							<td>First morpheme ID from the word phrase</td>
							<td>int</td>
						</tr>
						<tr>
							<td>end </td>
							<td>Last morpheme ID from the word phase</td>
							<td>int</td>
						</tr>
						<tr>
							<td>beginSid</td>
							<td>Unused</td>
							<td>-</td>
						</tr>
						<tr>
							<td>endSid</td>
							<td>Unused </td>
							<td>-</td>
						</tr>
						<tr>
							<td>position </td>
							<td>Word phrase ID (only used in English)</td>
							<td>int</td>
						</tr>
					</table>
					<span class="table_tit">morps: Morphological info</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>seq</td>
							<td>Morpheme ID
								(Assigned by appearance order, starting from 0)</td>
							<td>int</td>
						</tr>
						<tr>
							<td>lemma </td>
							<td>Morphological word</td>
							<td>string</td>
						</tr>
						<tr>
							<td>type</td>
							<td>Morphological tag </td>
							<td>string</td>
						</tr>
						<tr>
							<td>position </td>
							<td>Byte position in the sentence</td>
							<td>int</td>
						</tr>
						<tr>
							<td>wid  </td>
							<td>Unused</td>
							<td>-</td>
						</tr>
						<tr>
							<td>weight  </td>
							<td>Reliability of the morphological result (0~1)</td>
							<td>long</td>
						</tr>
					</table>
					<span class="table_tit">morphEvals: Morphological analysis result by word phrase</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>seq</td>
							<td>Word phrase ID</td>
							<td>int</td>
						</tr>
						<tr>
							<td>target</td>
							<td>Word phrase</td>
							<td>string</td>
						</tr>
						<tr>
							<td>result </td>
							<td>Morphological analysis result </td>
							<td>string</td>
						</tr>
						<tr>
							<td>wordId </td>
							<td>Unused (same as ‘seq’)</td>
							<td>int</td>
						</tr>
						<tr>
							<td>mBegin </td>
							<td>First morpheme ID from the word phrase</td>
							<td>int</td>
						</tr>
						<tr>
							<td>mEnd</td>
							<td>Last morpheme ID from the word phrase </td>
							<td>int</td>
						</tr>
					</table>
					<span class="table_tit">cls: HMD classification result</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>sentenceSequence</td>
							<td>Classified sentence ID</td>
							<td>int</td>
						</tr>
						<tr>
							<td>category</td>
							<td>Category name</td>
							<td>string</td>
						</tr>
						<tr>
							<td>pattern</td>
							<td>Classified pattern</td>
							<td>string</td>
						</tr>
						<tr>
							<td>sentence</td>
							<td>Selected sentence</td>
							<td>string</td>
						</tr>
					</table>

					<p class="sub_txt">⑤ Response example </p>
					<div class="code_box">
<pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "document": {
        "categoryWeight": 0,
        "sentences": [
            {
                "sequence": 0,
                "text": "Today 's weather makes me happy .",
                "words": [
                    {
                        "seq": 0,
                        "text": "Today",
                        "type": "NN",
                        "begin": 0,
                        "end": 0,
                        "beginSid": 0,
                        "endSid": 0,
                        "position": 0
                    },
                    {
                        "seq": 1,
                        "text": "'s",
                        "type": "POS",
                        "begin": 0,
                        "end": 0,
                        "beginSid": 0,
                        "endSid": 0,
                        "position": 1
                    },
                    {
                        "seq": 2,
                        "text": "weather",
                        "type": "NN",
                        "begin": 0,
                        "end": 0,
                        "beginSid": 0,
                        "endSid": 0,
                        "position": 2
                    },
                    {
                        "seq": 3,
                        "text": "makes",
                        "type": "VBZ",
                        "begin": 0,
                        "end": 0,
                        "beginSid": 0,
                        "endSid": 0,
                        "position": 3
                    },
                    {
                        "seq": 4,
                        "text": "me",
                        "type": "PRP",
                        "begin": 0,
                        "end": 0,
                        "beginSid": 0,
                        "endSid": 0,
                        "position": 4
                    },
                    {
                        "seq": 5,
                        "text": "happy",
                        "type": "JJ",
                        "begin": 0,
                        "end": 0,
                        "beginSid": 0,
                        "endSid": 0,
                        "position": 5
                    },
                    {
                        "seq": 6,
                        "text": ".",
                        "type": ".",
                        "begin": 0,
                        "end": 0,
                        "beginSid": 0,
                        "endSid": 0,
                        "position": 6
                    }
                ],
                "morps": [
                    {
                        "seq": 0,
                        "lemma": "today",
                        "type": "NN",
                        "position": 0,
                        "wid": 0,
                        "weight": 0
                    },
                    {
                        "seq": 1,
                        "lemma": "'s",
                        "type": "POS",
                        "position": 1,
                        "wid": 0,
                        "weight": 0
                    },
                    {
                        "seq": 2,
                        "lemma": "weather",
                        "type": "NN",
                        "position": 2,
                        "wid": 0,
                        "weight": 0
                    },
                    {
                        "seq": 3,
                        "lemma": "make",
                        "type": "VBZ",
                        "position": 3,
                        "wid": 0,
                        "weight": 0
                    },
                    {
                        "seq": 4,
                        "lemma": "me",
                        "type": "PRP",
                        "position": 4,
                        "wid": 0,
                        "weight": 0
                    },
                    {
                        "seq": 5,
                        "lemma": "happy",
                        "type": "JJ",
                        "position": 5,
                        "wid": 0,
                        "weight": 0
                    },
                    {
                        "seq": 6,
                        "lemma": ".",
                        "type": ".",
                        "position": 6,
                        "wid": 0,
                        "weight": 0
                    }
                ]
            }
        ]
    },
    "cls": [
        {
            "sentenceSequence": 0,
            "category": "SENTIMENT_Positive_1",
            "pattern": "happy$-3!no$-3!not$-3!never$-3!n't$!ridiculous$!toassist$!waste",
            "sentence": "Today 's weather makes me happy ."
        },
        {
            "sentenceSequence": 0,
            "category": "SENTIMENT_Positive_4",
            "pattern": "-3!no$-3!not$-3!never$-3!n't$!nobody$!nothing",
            "sentence": "Today 's weather makes me happy ."
        }
    ]
}

</pre>

					</div>
				</div>
			</div>
			<!--//.guide_box-->
		</div>
		<!--//hmdmenu-->
		<!--.hmdexample-->
		<div class="demobox" id="hmdexample">
			<p><em style="font-weight: 400;color:#27c1c1;">Use Cases</em>  </p>
			<span class="sub">Find out how AI can be applied to diverse area.</span>
			<%--패턴 분류(HMD)--%>
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>Text analysis</span>
							</dt>
							<dd class="txt">Analyze unstructured documents and text in a variety of fields.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
									<li class="ico_if"><span>ITF</span></li>
									<li class="ico_xdc"><span>XDC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>Automatic classification</span>
							</dt>
							<dd class="txt">Automatically classify and analyze text by category.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
									<li class="ico_if"><span>ITF</span></li>
									<li class="ico_xdc"><span>XDC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //패턴 분류(HMD) -->
		</div>
	</div>
</div>
<!-- //.contents -->
<script type="text/javascript">
	document.getElementById('file-down').addEventListener('change', readFile, false);


	var content = $('#text-contents').val();
	$('#count').html(content.length);

	$('#text-contents').keyup(function (e){
		var content = $(this).val();
		$('#count').html(content.length);
	});

	$('.btn_goback').on('click', function () {
		$(this).parent().parent().hide();
		$('form').fadeIn(300);
	});

	function readFile(e) {
		var file = e.target.files[0];

		if (!file) {
			return;
		}
		var reader = new FileReader();
		reader.onload = function(e) {
			fileData = e.target.result;
			$('#text-contents').val(fileData);

			// var txtValLth = $('#text-contents').val().length;
			//
			// console.log("this is file upload");
			// if ( txtValLth > 0) {
			// 	$('.demobox_hmd .step_1btn .btn_type').removeClass('disable');
			// 	$('.demobox_hmd .step_1btn .btn_type').removeAttr('disabled');
			// 	$('.demobox_hmd .btn_area .disable .disBox').remove();
			// 	$('.demobox_hmd .step_1btn .disBox').remove();
			// }
		};

		reader.readAsText(file, "euc-kr");
	}


	$('#hmdbtn').on('click',function(){

		$(this).parent().parent().hide();
		$('.posi_nega_area').fadeIn(300);

		var language = $("input[type=radio][name=language]:checked").val();

		$.ajax({
			type:'POST',
			url:'/api/hmd/hmdApi',
			data: {
				"lang": language,
				"reqText": $('#text-contents').val(),
				"${_csrf.parameterName}" : "${_csrf.token}"
			},
			error: function(error){
				console.log(error);
			},
			success: function(data){
                var decodedData = decodeURIComponent(data);
				console.log("result:"+decodedData);

                procResult(decodedData);

				// $('.hmd_box .step02').fadeIn();

				var pnHeight = $(".posi_nega_conts").height();
				var pnHeight01 = $(".posi_nega_conts01").height();

				if(pnHeight>205){
					$(".posi_nega_conts").addClass('posi_nega_close');
				}else if (pnHeight01>205){
					$(".posi_nega_conts01").addClass('posi_nega_close');
				}

				var txtOpen = "More";
				$('.btn_more').click(function() {
					$(this).toggleClass('on');
					$(this).parent().toggleClass('on');

					if($(this).text() == txtOpen ){
						$(this).text("Folding");
						$('.hidden').fadeIn();
					}else{
						$(this).text("More");
						$('.hidden').css('display','none');
					}
				});
				$('.hidden').css('display','none');

				var span_ = $('.patternTxt');
				for(var ss = 0; ss < span_.length; ss++){
					var span_txt = span_[ss].innerText;
					if(span_txt == ''){
						$(span_[ss]).css('cursor','default');
						$(span_[ss]).next().remove();
					}
				}

			}
		});
	});

    /*
    ** 수신된 결과 메세지 처리
    */
    function procResult(data) {
        var jsonObj = JSON.parse(data);
        var cls = jsonObj.cls;

        var positiveCnts = new Array(0, 0, 0, 0);
        var negativeCnts = new Array(0, 0, 0, 0);

        initUI();

        for(var xx = 0; xx < cls.length; xx++) {
            var categories = cls[xx].category.split("_");
            var pattern = cls[xx].pattern;
            var level = parseInt(categories[2]);

            if(categories[1] == 'Positive') positiveCnts[level-1]++;
            else negativeCnts[level-1]++;

			//19. 09. 23 - 표기 변경 추가 LYJ
			var replaceDallarSymbol = "(" + pattern.toString() + ")";

			replaceDallarSymbol = replaceDallarSymbol.split("$").join(")(");
			console.log(replaceDallarSymbol);

			addResultUI(categories[1], level, replaceDallarSymbol, xx);
            // alert("<" + xx + "> <" + categories[1] + "> <" + level + "> <" + pattern + ">");
        }
        // alert("pCnt=" + positiveCnt + ", nCnt=" + negativeCnt);

        for(var xx = 0; xx < 4; xx++) {
            $('#Positive_Cnt_' + (xx+1)).append("" + positiveCnts[xx]);
            $('#Negative_Cnt_' + (xx+1)).append("" + negativeCnts[xx]);
        }
		//0916 추가
		// $('.patternTxt').each(function(){
		// 	var text_re = $(this).text();
		// 	if (text_re === ""){
		// 		$(this).parent().remove();
		// 	}
		// });
    }

    /*
    **
    */
    function addResultUI(type, level, pattern, count) {
		for (var xx = 0; xx < 4; xx++) {
			if(count<5){
				$('#' + type + '_' + (xx + 1)).append("<li>" + "<span class=\"patternTxt\">" + (level == xx + 1 ? pattern : "") + "</span>" + "<em>" + (level == xx + 1 ? pattern : "") + "</em>" + "</li>");

			}else{
				$('#' + type + '_' + (xx + 1)).append("<li class=\"\">" + "<span class=\"patternTxt\">" + (level == xx + 1 ? pattern : "") + "</span>" + "<em>" + (level == xx + 1 ? pattern : "") + "</em>" + "</li>");

			}


		}

	}

    function initUI() {
        for(var xx = 0; xx < 4; xx++) {
            $('#Positive_' + (xx+1)).empty();
            $('#Negative_' + (xx+1)).empty();
        }


        for(var xx = 0; xx < 4; xx++) {
            $('#Positive_Cnt_' + (xx+1)).empty();
            $('#Negative_Cnt_' + (xx+1)).empty();
        }
    }

	$('input[type=radio][name=language]').on('click',function(){
		var chk = $("input[type=radio][name=language]:checked").val();
		if(chk == 'kor'){
			$('#text-contents').val(':: 긍정 표현 예시::\n' +
					'훌륭하네요. 덕분에 기분이 매우 좋습니다.\n' +
					'문제를 해결해주셔서 감사합니다. 당신은 제 생명의 은인입니다.\n' +
					'\n' +
					':: 부정 표현 예시 ::\n' +
					'너무 불친절하고 무례하네요. 뻔뻔하고 최악이예요.\n' +
					'실망스럽습니다. 저는 이 최악의 제안을 받아들일 수가 없어요.');
		}else{
			$('#text-contents').val(':: Example of Positive expression ::\n' +
					'Awesome! Thank you for your good behavior.\n' +
					'I am very happy and astonished because of you.\n' +
					'\n' +
					':: Example of Negative expression ::\n' +
					'It was a horrible, unpleasant memory.\n' +
					'You disappointed me and made me miserable... I\'ll never forgive you again.');
		}
	});

	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}

	document.getElementById("defaultOpen").click();
</script>


<%--<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/hmd.js"></script>--%>
