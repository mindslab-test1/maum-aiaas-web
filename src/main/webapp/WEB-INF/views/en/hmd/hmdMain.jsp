﻿<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
	<div class="content api_content">
		<h1 class="api_tit">HMD</h1>
		<ul class="menu_lst">
			<li class="tablinks" onclick="openTap(event, 'hmddemo')" id="defaultOpen">Demo</li>
			<li class="tablinks" onclick="openTap(event, 'hmdmenu')">Manual</li>
			<li class="tablinks" onclick="openTap(event, 'hmdexample')">Use Case</li>
			<li class="tablinks"><a href="/member/enApiAccount">Purchase</a></li>
		</ul>
		<div class="demobox demobox_hmd" id="hmddemo">
			<p>Classification of emotional pattern via Hierarchical Multiple Dictionary (HMD)</p>
			<span class="sub">In reference to HMD consisted of morphemes, categorizes sentences and detects patterns of sentences.</span>
			<p class="temp_txt">The service is currently available only in Korean. Other languages will be available soon.</p>
			<form>
				<!--.hmd_box-->
				<div class="hmd_box">
					<!--
                                                <dl class="radio_area">
                                                    <dt>언어선택</dt>
                                                    <dd class="type_radio"><input type="radio" name="language" id="language_ko" checked /><label for="language_ko">한국어</label></dd>
                                                    <dd class="type_radio"><input type="radio" name="language" id="language_en" /><label for="language_en">영어</label></dd>
                                                </dl>
                    -->
					<div class="text_area">
						<textarea rows="12" id="text-contents" maxlength="1000" placeholder="Please type in sentences or upload text files."></textarea>
					</div>
					<div class="text_info">
						<p>*Please type in words within 1,000 words and less than 30KB [txt format]</p>
						<span class=""><strong id="count">0</strong>/1000</span>
					</div>
				</div>
				<!--//.hmd_box-->
				<!--.btn_area-->
				<div class="btn_area btn_area_815 step_1btn">
					<div class="btn_float_l">
						<input type="file" id="file-down" class="demoFile" accept=".txt"/><label for="file-down" class="btn_type btn_gray">Upload files</label>
					</div>
					<button type="button" id="hmdbtn" class="btn_type btn_blue" disabled><em class="fas fa-file-invoice"></em>Analyze<span class="disBox"></span></button>
				</div>
				<!--//.btn_area-->
			</form>

			<!--posi_nega_area-->
			<div class="posi_nega_area">
				<!--posi_box-->
				<div class="posi_box">
					<div class="tit_posi"><span>Positive Expressions</span></div>
					<div class="posiNega_area">
						<ul class="tab_posi_nega">
							<li><span>Low</span></li>
							<li><span></span></li>
							<li><span></span></li>
							<li><span>High</span></li>
						</ul>
						<ul class="tab_posi_nega2">
							<li></li>
							<li></li>
							<li></li>
							<li></li>
						</ul>

						<div class="posi_nega_conts posi_nega_close">
							<div class="posi_nega_list">
								<ul>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul>
								</ul>
							</div>
							<a href="#none" class="btn_more">More</a>
						</div>
					</div>
				</div>
				<!--//.posi_box-->
				<!--.nega_box-->
				<div class="nega_box">
					<div class="tit_posi tit_nega"><span>Negative Expressions</span></div>
					<div class="posiNega_area">
						<ul class="tab_posi_nega tab_nega">
							<li><span>Low</span></li>
							<li><span></span></li>
							<li><span></span></li>
							<li><span>High</span></li>
						</ul>
						<ul class="tab_posi_nega2">
							<li></li>
							<li></li>
							<li></li>
							<li></li>
						</ul>

						<div class="posi_nega_conts01 posi_nega_close">
							<div class="posi_nega_list">
								<ul>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul>
								</ul>
							</div>
							<div class="posi_nega_list">
								<ul>
								</ul>
							</div>
							<a href="#none" class="btn_more">More</a>
						</div>
					</div>
				</div>
				<!--//.nega_box-->

				<!--.btn_area-->
				<div class="btn_area btn_area_815">
					<button type="button" class="btn_type btn_blue btn_goback">Start Again</button>
				</div>
				<!--//.btn_area-->

			</div>
			<!--//posi_nega_area-->
		</div>
		<!-- .demobox -->
		<!--.hmdmenu-->
		<div class="demobox" id="hmdmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API Guideline
					</div>
					<p class="sub_title">Set up Environment</p>
					<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
					<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
					<p class="sub_title">ID &amp; Key</p>
					<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
					<p class="sub_txt">2&#41; Send an email to hello@mindslab.ai with your name and email address.</p>
					<p class="sub_txt">3&#41; After the agreement, Minds Lab would send you an ID with a key.</p>
					<p class="sub_txt">2&#41; Remember the ID and key.</p>
					<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s ID and key.</p>
				</div>
			</div>
			<!--//.guide_box-->
			<div class="coming_txt">
				<h5>Coming Soon.</h5>
			</div>
		</div>
		<!--//hmdmenu-->
		<!--.hmdexample-->
		<div class="demobox" id="hmdexample">
			<div class="coming_txt">
				<h5>Coming Soon.</h5>
			</div>
		</div>
		<!--//.hmdexample-->


	</div>
</div>
<!-- //.contents -->
<script type="text/javascript">
document.getElementById('file-down').addEventListener('change', readFile, false);

$('#text-contents').keyup(function (e){
	console.log("count");
	var content = $(this).val();
	$('#count').html(content.length);
});

function readFile(e) {
var file = e.target.files[0];

if (!file) {
return;
}
var reader = new FileReader();
reader.onload = function(e) {
fileData = e.target.result;
$('#text-contents').val(fileData);
var txtValLth = $('#text-contents').val().length;
console.log("this is file upload");
if ( txtValLth > 0) {
$('.progress li:nth-child(2)').addClass('active');
$('.demobox_hmd .step_1btn .btn_type').removeClass('disable');
$('.demobox_hmd .step_1btn .btn_type').removeAttr('disabled');
$('.demobox_hmd .btn_area .disable .disBox').remove();
$('.demobox_hmd .step_1btn .disBox').remove();
}
};

reader.readAsText(file);
}
</script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/hmd.js"></script>
<script src="${pageContext.request.contextPath}/aiaas/kr/js/hmd/app.js"></script>