<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-07-02
  Time: 16:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<form id="logout-form" action='/logout' method="POST">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>

<!-- .lyr_event -->
<div class="lyr_consultant">
    <div class="lyr_consultant_bg"></div>
    <!-- .lyrWrap -->
    <div class="lyrWrap">
        <div class="lyr_hd">
            <h1>Become an <span>AI Consultant</span><br>
                in <span>4 Easy Steps</span></h1>
            <em class="fas fa-times btn_lyrWrap_close"></em>
        </div>

        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <ul>
                <li>
                    <p>
                        <strong>1. Join us</strong>
                        <span>Fill out your information and submit through our Google form to get started!</span>

                    </p>
                    <em class="fas fa-angle-double-down"></em>
                </li>
                <li>
                    <p>
                        <strong>2. Study & Learn</strong>
                        <span>Go through our training modules and explore our maum.ai to expand your knowledge in AI</span>
                        <%--                        <a href="/home/academyForm" target="_blank" class="academy_btn">아카데미 바로가기 <em class="fas fa-angle-right"></em></a>--%>
                    </p>
                    <em class="fas fa-angle-double-down"></em>
                </li>
                <li>
                    <p>
                        <strong>3. Take a Test</strong>
                        <span>Take a test to show what you have learned</span>
                    </p>
                    <em class="fas fa-angle-double-down"></em>
                </li>
                <li>
                    <p>
                        <strong>4. Get Started!</strong>
                        <span>Receive your certification and begin your AI Consultant journey</span>
                    </p>
                </li>
            </ul>

            <a href="https://docs.google.com/forms/d/e/1FAIpQLSczF2ih31tXMFZgJ5yF_5gzLEK-kRvGO25U3fJddEzGTNC4SA/viewform"
               target="_blank">Apply Now <em class="fas fa-angle-right"></em></a>

        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.lyrWrap -->
</div>
<!-- //.lyr_event -->



<!-- #header -->
<div id="header">
    <!-- .header_box -->
    <div class="header_box">
        <h1 class="fl"><a href="/?lang=en">
            <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/logo_maumai_bk.svg"
                 alt="maum ai logo">
        </a></h1>

        <ul class="gnb fr">
            <li><a href="/#our_services">Service</a></li>
            <li><a href="/#ecoMINDs">ecoMINDs</a></li>
            <li><a href="#none" class="go_const">AI Consultant</a></li>
            <li><a href="/home/pricingPage?lang=en">Pricing</a></li>
            <li><a href="https://mindslab.ai:8080/en" target="_blank" title="MINDsLab">About Us</a></li>
            <!-- 로그인 X인 경우 -->
            <sec:authorize access="isAnonymous()">
                <li class="brd_btn"><a href="javascript:login()">Sign In</a></li>
            </sec:authorize>

            <!-- 로그인 O인 경우 -->
            <sec:authorize access="isAuthenticated()">
                <li class="user_info">
                    <div class="userBox">
                        <ul>
                            <li>
                                <p class="ico_user">
                                    <em class="far fa-user"></em>
                                    <span>${fn:escapeXml(sessionScope.accessUser.email)}</span>
                                    <em class="fas fa-angle-down"></em>
                                    <em class="fas fa-angle-up"></em>
                                </p>
                                <ul class="lst">
                                    <li class="ico_profile"><a target="_self" href="/user/profileMain">PROFILE</a></li>
                                    <li class="ico_account"><a target="_self" href="/user/apiAccountMain">API ACCOUNT</a></li>
                                    <li class="ico_payment"><a target="_self" href="/user/paymentInfoMain">PAYMENT</a></li>
                                    <li class="ico_logout"><a href="#" onclick="document.getElementById('logout-form').submit();">LOGOUT</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </li>
            </sec:authorize>
            <li class="lang">
                <div class="lang_box">
                    <span>English</span>
                    <span><a href="/?lang=kr" target="_self">Korean</a></span>
                </div>
            </li>
        </ul>
    </div>
    <!-- //.header_box -->
</div>
<!-- //#header -->