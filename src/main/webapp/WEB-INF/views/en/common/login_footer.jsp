<%--
  Created by IntelliJ IDEA.
  User: smr
  Date: 2020-11-17
  Time: 2:09 p.m.
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<!-- #footer -->
<div id="footer">
    <footer>
        <div class="footer">
            <a href="/home/enTermsMain" target="_blank">Terms &amp; Conditions </a> ㅣ
            <a href="/home/enTermsMain#conditions" target="_blank"> Privacy Policy </a> ㅣ
            <a href="/?lang=en#inquiry" target="_blank"> Contact Us </a>
            <span>ㅣ</span>
            <div class="lang">
                <span class="lang_select">English <em class="fas fa-chevron-up"></em></span>
                <ul>
                    <li class="active">English</li>
                    <li><a href="https://maum.ai/?lang=ko" target="_blank">한국어</a></li>
                </ul>
            </div>
        </div>
        <p>&copy; Copyright 2020 Minds Lab</p>
        <div class="m_lang">English <a href="https://maum.ai/?lang=ko" title="한국어"></a></div>
    </footer>
</div>
<!-- //#footer -->

<script>

    //footer 언어 체크
    $('.lang_select').on('click', function () {
        $(this).next().toggleClass('active');
        return false;
    });
    $(document).on('click', function (e) {
        var langOpen = $('footer .lang ul.active');
        if( langOpen.has(e.target).length === 0){
            langOpen.removeClass('active');
        }
    });
</script>