<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
	String styleCss = application.getRealPath("${pageContext.request.contextPath}/aiaas/kr/css/login_renew.css");
	File style = new File(styleCss);
	Date lastModifiedStyle = new Date(); 
 
	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="format-detection" content="telephone=no" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no" />
<meta name="AdsBot-Google" content="index">
<meta name="AdsBot-Google-Mobile" content="index">

<!-- icon_favicon -->
<link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/en/images/ico_maumAI_60x60.png" />
<link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/en/images/ico_maumAI_60x60.ico"/>
<title>maum.ai platform</title>
<!-- resources -->
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/reset.css" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/font.css" />
<!--<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/common.css" />-->

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/pop_common.css?ver=<%=fmt.format(lastModifiedStyle)%>">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/login_renew.css?ver=<%=fmt.format(lastModifiedStyle)%>" />
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/common/css/all.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">


<!-- //resources -->

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/jquery-1.11.0.min.js"></script>	
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/common.js?ver=<%=fmt.format(lastModifiedStyle)%>"></script>



</head>