<%--
  Created by IntelliJ IDEA.
  User: smpar
  Date: 2020-06-29
  Time: 2:09 p.m.
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>


<!-- #footer -->
<div id="footer">
    <div class="btm_menu">
        <div class="cont_box">
            <a href="/?lang=en" target="_self" title="maum AI"><img
                    src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/logo_maumai_bk.svg"
                    alt="maumAI logo"></a>

            <div class="menu_area">
                <div class="contact">
                    <p>Contact Us</p>
                    <a href="mailto:hello@mindslab.ai"><span class="far fa-envelope"></span>hello@mindslab.ai</a>
                    <div class="info_box">
                        <a href="https://mindslab.ai:8080/en" target="_blank" title="Company">About us</a>
                        <a href="/home/enTermsMain" target="_blank" title="Terms & Conditions">Terms &amp;
                            Conditions</a>
                    </div>
                </div>
                <div class="service">
                    <div class="ai_service">
                        <p>Application</p>
                        <a href="https://builder.maum.ai" title="AI builder">AI Builder</a>
                        <a href="https://minutes.maum.ai/" title="maum Minutes" class="">maum Minutes</a>
                        <a href="https://fast-aicc.maum.ai" target="_blank" title="FAST AI">FAST AI</a>
                    </div>
                    <div class="engine_api">
                        <p>AI Engine</p>
                        <a href="/main/enMainHome" title="Cloud API">Cloud API</a>
                    </div>
                    <div class="data">
                        <p>Data</p>
                        <a href="https://data.maum.ai/?lang=en" target="_blank" title="maum Data">maum Data</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="copyright">
        <div class="cont_box">
            <span>&copy; Copyright 2020</span>
            <span>Minds Lab</span>
            <span>601 Dasan Tower, 49 Daewangpangyo-ro 644beon-gil, Bundang-gu, Seongnam-si, Gyeonggi-do, Republic of Korea</span>
            <span>| &nbsp;&nbsp;CEO Taejoon Yoo&nbsp;&nbsp; |</span>
            <span>Company Registration Number 314-86-55446</span>
        </div>
    </div>
</div>
<!-- //#footer -->