﻿<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
				
		<!-- .contents -->
		<div class="contents">
			<div class="content api_content">
				<h1 class="api_tit">Natural Language</h1>
				<ul class="menu_lst">
					<li class="tablinks" onclick="openTap(event, 'nludemo')" id="defaultOpen">Demo</li>
					<li class="tablinks" onclick="openTap(event, 'nlumenu')">Manual</li>
					<li class="tablinks" onclick="openTap(event, 'nluexample')">Use Case</li>
					<li class="tablinks"><a href="/support/enPricing">Purchase</a></li>
				</ul>
				<!--.demobox_nlu-->
				<div class="demobox demobox_nlu" id="nludemo">
					<p>Natural Language</p>					
					<span class="sub">It is the basis of artificial intelligence technology that expresses daily speech that people use in a form that the computer can understand.</span>	
					<p class="temp_txt">The service is currently available only in Korean. Other languages will be available soon.</p>

					<!--.nlu_box-->
					<div class="nlu_box">
						<!--.step01-->
						<div class="step01">
							<div class="demo_top">								
							<img src="${pageContext.request.contextPath}/aiaas/en/images/ico_xdc.png" alt="nlu text">
							</div>
							<div class="text_area">
								<textarea rows="7" placeholder="" id="text-contents-nlp">The true beauty of the desert is the oasis that is hidden.</textarea>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_search" id="nlpbtn"><em class="fas fa-file-invoice"></em>Analyze</button>
<%--								<span class="disBox"></span>--%>
							</div>
						</div>	
						<!--//.step01-->
						
						<!--.step02-->
						<div class="step02">
							<ul class="radio_area">
								<li class="type_radio type_01"><input type="checkbox" name="syntax" id="part" value="part" checked /><label for="part">Part of Speech</label></li>
								<li class="type_radio type_02"><input type="checkbox" name="syntax" id="lemma" value="lemma" /><label for="lemma">Lemma</label></li>
								<li class="type_radio type_03"><input type="checkbox" name="syntax" id="entity" value="entity"/><label for="entity">Named entity</label></li>
							</ul>
							<table>
								<tbody>
									<tr class="text_word">
										<%--<td>Google</td>
										<td>,</td>
										<td>headquartered</td>
										<td>in</td>
										<td>Mountain</td>
										<td>View</td>
										<td>,</td>
										<td>unveiled</td>
										<td>the</td>
										<td>new</td>
										<td>Android</td>
										<td>phone</td>
										<td>at</td>
										<td>the</td>
										<td>Consumer</td>
										<td>Electronic</td>
										<td>Show</td>
										<td>.</td>--%>
									</tr>
									<tr class="result_text text_lemma" hidden="hidden">
										<%--<td></td>
										<td></td>
										<td>headquarter</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td>unveil</td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>--%>
									</tr>
									<tr class="result_text text_part">
									</tr>
									<tr class="result_text text_entity" hidden="hidden" style="...">
									</tr>
								</tbody>
							</table>
						</div>	
						<!--//.step02-->
					</div>						
					<!--//.nlu_box-->	
				</div>
				<!-- .<!--.demobox_nlu -->
				<!--.nlumenu-->
				<div class="demobox" id="nlumenu">	
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API Guideline				
							</div>
							<p class="sub_title">Set up Environment</p>
							<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
							<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
							<p class="sub_title">ID &amp; Key</p>
							<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
							<p class="sub_txt">2&#41; Send an email to hello@mindslab.ai with your name and email address.</p>
							<p class="sub_txt">3&#41; After the agreement, Minds Lab would send you an ID with a key.</p>
							<p class="sub_txt">2&#41; Remember the ID and key.</p>
							<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s ID and key.</p>
						</div>
					</div>
					<!--//.guide_box-->		
					<div class="coming_txt">
						<h5>Coming Soon.</h5>
					</div>
				</div>
				<!--//nlumenu-->
				<!--.nluexample-->
				<div class="demobox" id="nluexample">	
					<div class="coming_txt">
						<h5>Coming Soon.</h5>
					</div>
				</div>
				<!--//.nluexample-->
			</div>
		</div>
		<!-- //.contents -->


<script type="text/javascript">
	$(window).load(function() {
		$('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
			$(this).remove();
		});
	});
</script>

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/nlu.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/nlu/app.js"></script>

<script src="${pageContext.request.contextPath}/aiaas/kr/js/nlu/dist/main.js"></script>


<script type="text/javascript">
	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){

			//nlu

			$('.nlu_box .step01 textarea').on('input keyup paste', function() {
				var txtValLth = $(this).val().length;

				if ( txtValLth > 0) {
					$('.nlu_box .step01 .btn_area button').removeClass('disable');
					$('.nlu_box .step01 .btn_area button').removeAttr('disabled');
					$('.nlu_box .step01 .btn_area .disBox').remove();
				} else {
					$('.nlu_box .step01 .btn_area button').addClass('disable');
					$('.nlu_box .step01 .btn_area button').attr('disabled');
					$('.nlu_box .step01 .btn_area').append('<span class="disBox"></span>');
					$('.nlu_box .step02').fadeOut();
					$('.progress li:nth-child(2)').removeClass('active');
				}
			});

			$('.nlu_box .step01 .btn_search').on('click',function(){
				$('.nlu_box .step02').fadeIn();
				$('.progress li:nth-child(2)').addClass('active');
			});

			$('input[name="syntax"]').click(function() {

				if($(this).prop('checked')){
					$('input[type="checkbox"][name="syntax"]').prop('checked', false);
					$(this).prop('checked', true);
				}

				console.dir($('.result_text'));
				console.dir($('input[name="syntax"]:checked').val());

				$(".result_text").attr("hidden", "hidden");

				if ($('input[name="syntax"]:checked').val() == 'part') {
					$(".text_part").removeAttr("hidden");
					return;
				}
				if ($('input[name="syntax"]:checked').val() == 'lemma') {
					$(".text_lemma").removeAttr("hidden");
					return;
				}
				if ($('input[name="syntax"]:checked').val() == 'entity'){
					$(".text_entity").removeAttr("hidden")
				}
			})
			//nlu


		});

	});

</script>
