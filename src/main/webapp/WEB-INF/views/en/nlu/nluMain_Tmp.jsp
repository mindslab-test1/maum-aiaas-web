﻿<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
	<div class="content api_content">
		<h1 class="api_tit">NLU</h1>
		<ul class="menu_lst">
			<li class="tablinks" onclick="openTap(event, 'nludemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
			<li class="tablinks" onclick="openTap(event, 'nluexample')"><button type="button">Use Case</button></li>
			<li class="tablinks" onclick="openTap(event, 'nlumenu')"><button type="button">Manual</button></li>
<%--			<li class="tablinks"><a href="/member/enApiAccount">API ID & Key</a></li>--%>
		</ul>
		<!--.demobox_nlu-->
		<div class="demobox demobox_nlu" id="nludemo">
			<p><span>NLU</span> <small>(Natural Language Understanding)</small></p>
			<span class="sub">It is the basis of artificial intelligence technology that expresses daily speech that people use in a form that the computer can understand.</span>

			<!--.nlu_box-->
			<div class="nlu_box">
				<div class="" style ="width: 328px; height: 40px; border-radius: 5px; border: 1px solid #dce2f2; margin: 0 auto 25px; line-height: 40px;">
					<span style="font-size:13px;margin-right:28px; ">Language</span>
					<div class="radio">
						<input type="radio" id="2" name="lang" checked="checked"  value="eng">
						<label for="2"><span style=" color: #576068;">English</span></label>
					</div>
					<div class="radio">
						<input type="radio" id="1" name="lang"  value="kor">
						<label for="1"><span style=" color: #576068;">Korean</span></label>
					</div>

				</div>


				<!--.step01-->
				<div class="step01">
					<div class="demo_top">
						<!--	<img src="${pageContext.request.contextPath}/aiaas/en/images/ico_xdc.png" alt="nlu text">-->
					</div>
					<div class="text_area">
						<textarea rows="7" placeholder="" id="text-contents-nlp">The true beauty of the desert is the oasis that is hidden.</textarea>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_search " id="nlpbtn"><em class="fas fa-file-invoice"></em>Process</button>
					</div>
				</div>
				<!--//.step01-->

				<!--.step02-->
				<div class="step02">
					<ul class="radio_area">
						<li class="type_radio type_01"><input type="checkbox" name="syntax" id="part" value="part" checked /><label for="part">Part of Speech</label></li>
						<li class="type_radio type_02"><input type="checkbox" name="syntax" id="lemma" value="lemma" checked /><label for="lemma">Lemma</label></li>

						<!--
						<li class="type_radio type_03"><input type="checkbox" name="syntax" id="entity" value="entity" checked /><label for="entity">Named entity</label></li>
						-->
					</ul>
					<table>
						<tbody>
						<tr class="text_word">

						</tr>
						<tr class="result_text text_lemma">

						</tr>
						<tr class="result_text text_part">

						</tr>
						<tr class="result_text text_entity" hidden="hidden" style="...">

						</tr>
						</tbody>
					</table>

				</div>
				<!--//.step02-->
			</div>
			<!--//.nlu_box-->
		</div>
		<!-- .demobox_nlu -->
		<!--.nlumenu-->
		<div class="demobox" id="nlumenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API Guideline
					</div>
					<p class="sub_title">Set up Environment</p>
					<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
					<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
					<p class="sub_title">ID &amp; Key</p>
					<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
					<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
					<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
						<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
					<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
					<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
					<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
				</div>
				<div class="guide_group">
					<div class="title">
						NLU <small>(Natural Language Understanding)</small>
					</div>
					<p class="sub_txt">Analyzes, extracts and understands meaningful information in texts, can be used for all services utilizing natural language.</p>
					<span class="sub_title">
						Preparation
						</span>
					<p class="sub_txt">① Input: String (text) </p>
					<p class="sub_txt">② Language: Choose one below </p>
					<ul>
						<li>Korean (kor)</li>
						<li>English (eng)</li>
					</ul>
					<span class="sub_title">
						API Document
						</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/nlu/</li>
					</ul>
					<p class="sub_txt">② Request parameters </p>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId</td>
							<td>Unique API ID. Request from is required for the ID.</td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>Unique API key. Request from is required for the key. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>lang</td>
							<td>Selected language (kor / eng) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>reqText </td>
							<td>Input text </td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request example </p>
					<div class="code_box">
						{<br>
						<span>"apiId": "(*Request for ID)",</span><br>
						<span>"apiKey": "(*Request for key)",</span><br>
						<span>"lang": "eng",</span><br>
						<span>"reqText": "I'm having a bad day."</span><br>
						}
					</div>

					<p class="sub_txt">④ Response parameters </p>
					<span class="table_tit">Response</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>message </td>
							<td>Status of API request </td>
							<td>list</td>
						</tr>
						<tr>
							<td>document</td>
							<td>Result data</td>
							<td>list</td>
						</tr>
					</table>
					<span class="table_tit">message: ​Status of API request</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>message </td>
							<td>Status (Success/ Fail)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>status</td>
							<td>Status code (0: Success)</td>
							<td>number</td>
						</tr>
					</table>
					<span class="table_tit">document: Result data</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>category Weight</td>
							<td>Unused </td>
							<td>-</td>
						</tr>
						<tr>
							<td>sentences </td>
							<td>Result data by sentences</td>
							<td>array</td>
						</tr>
					</table>
					<span class="table_tit">sentences: Result data by sentences</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>sequence</td>
							<td>Sentence number (n)</td>
							<td>int</td>
						</tr>
						<tr>
							<td>text </td>
							<td>Input text</td>
							<td>string</td>
						</tr>
						<tr>
							<td>words </td>
							<td>Word phrase info</td>
							<td>list</td>
						</tr>
						<tr>
							<td>morps  </td>
							<td>Morphological info </td>
							<td>list</td>
						</tr>
						<tr>
							<td>namedEntities  </td>
							<td>Entity info </td>
							<td>list</td>
						</tr>
					</table>
					<span class="table_tit">words: Word phrase info</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>seq </td>
							<td>Word Phrase ID </td>
							<td>int</td>
						</tr>
						<tr>
							<td>text </td>
							<td>Word Phrase</td>
							<td>string</td>
						</tr>
						<tr>
							<td>begin </td>
							<td>First morpheme ID from the word phrase</td>
							<td>int</td>
						</tr>
						<tr>
							<td>end</td>
							<td>Last morpheme ID from the word phrase </td>
							<td>int</td>
						</tr>
						<tr>
							<td>beginSid </td>
							<td>Unused </td>
							<td>-</td>
						</tr>
						<tr>
							<td>endSid </td>
							<td>Unused</td>
							<td>-</td>
						</tr>
						<tr>
							<td>position </td>
							<td>Word phrase ID (only used in English)</td>
							<td>int</td>
						</tr>
					</table>
					<span class="table_tit">morps: Morphological info</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>seq </td>
							<td>Morpheme ID
								(Assigned by appearance order, starting from 0)</td>
							<td>int</td>
						</tr>
						<tr>
							<td>lemma </td>
							<td>Morphological word </td>
							<td>string</td>
						</tr>
						<tr>
							<td>type </td>
							<td>Morphological tag</td>
							<td>string</td>
						</tr>
						<tr>
							<td>position</td>
							<td>Byte position in the sentence </td>
							<td>int</td>
						</tr>
						<tr>
							<td>wid  </td>
							<td>Unused </td>
							<td>-</td>
						</tr>
						<tr>
							<td>weight  </td>
							<td>Reliability of the morphological result (0~1)</td>
							<td>long</td>
						</tr>
					</table>
					<span class="table_tit">namedEntities: Entity info</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>Type</th>
						</tr>
						<tr>
							<td>seq </td>
							<td>Entity ID
								(Assigned by appearance order, starting from 0)</td>
							<td>int</td>
						</tr>
						<tr>
							<td>text  </td>
							<td>Entity word</td>
							<td>string</td>
						</tr>
						<tr>
							<td>type </td>
							<td>Entity type </td>
							<td>string</td>
						</tr>
						<tr>
							<td>begin </td>
							<td>First morpheme ID from the entity</td>
							<td>int</td>
						</tr>
						<tr>
							<td>end </td>
							<td>Last morpheme ID from the entity </td>
							<td>int</td>
						</tr>
						<tr>
							<td>weight  </td>
							<td>Reliability of the morphological result (0~1)</td>
							<td>long</td>
						</tr>
						<tr>
							<td>commonNoun </td>
							<td>Proper noun:0 ; Common noun:1 </td>
							<td>int</td>
						</tr>
						<tr>
							<td>beginSid  </td>
							<td>Unused </td>
							<td>-</td>
						</tr>
						<tr>
							<td>endSid </td>
							<td>Unused </td>
							<td>-</td>
						</tr>
					</table>
					<p class="sub_txt">⑤ Response example </p>
					<div class="code_box">
						{ <br>
						<span> "message": {</span><br>
						<span class="twoD">"message": "Success",</span><br>
						<span class="twoD">"status": 0</span><br>
						<span> },</span><br>
						<span>"document": {</span><br>
						<span class="twoD">"categoryWeight": 0,	</span><br>
						<span class="twoD">"sentences": [</span><br>
						<span class="thirD">{</span><br>
						<span class="fourD">"sequence": 0,</span><br>
						<span class="fourD">"text": "I 'm having a bad day .",</span><br>
						<span class="fourD">"words": [</span><br>
						<span class="fivD">{</span><br>
						<span class="sixD">"seq": 0,</span><br>
						<span class="sixD">"text": "I",</span><br>
						<span class="sixD">"type": "PRP",</span><br>
						<span class="sixD">"begin": 0,</span><br>
						<span class="sixD">"end": 0,</span><br>
						<span class="sixD">"beginSid": 0,</span><br>
						<span class="sixD">"endSid": 0,</span><br>
						<span class="sixD">"position": 0</span><br>
						<span class="fivD">},</span><br>
						<span class="fivD">...</span><br>
						<span class="fivD">{</span><br>
						<span class="sixD">"seq": 6,</span><br>
						<span class="sixD">"text": ".",</span><br>
						<span class="sixD">"type": ".",</span><br>
						<span class="sixD">"begin": 0,</span><br>
						<span class="sixD">"end": 0,</span><br>
						<span class="sixD">"beginSid": 0,</span><br>
						<span class="sixD">"endSid": 0,</span><br>
						<span class="sixD">"position": 6</span><br>
						<span class="fivD">}</span><br>
						<span class="fourD">],</span><br>
						<span class="fourD">"morps": [</span><br>
						<span class="fivD">{</span><br>
						<span class="sixD">"seq": 0,</span><br>
						<span class="sixD">"lemma": "i",</span><br>
						<span class="sixD">"type": "PRP",</span><br>
						<span class="sixD">"position": 0,</span><br>
						<span class="sixD">"wid": 0,</span><br>
						<span class="sixD">"weight": 0</span><br>
						<span class="fivD">},</span><br>
						<span class="fivD">...</span><br>
						<span class="fivD">{</span><br>
						<span class="sixD">"seq": 6,</span><br>
						<span class="sixD">"lemma": ".",</span><br>
						<span class="sixD">"type": ".",</span><br>
						<span class="sixD">"position": 6,</span><br>
						<span class="sixD">"wid": 0,</span><br>
						<span class="sixD">"weight": 0</span><br>
						<span class="fivD">}</span><br>
						<span class="fourD">],</span><br>
						<span class="fourD">"namedEntities": [</span><br>
						<span class="fivD">{</span><br>
						<span class="sixD">"seq": 0,</span><br>
						<span class="sixD">"text": "a bad day",</span><br>
						<span class="sixD">"type": "DATE",</span><br>
						<span class="sixD">"begin": 3,</span><br>
						<span class="sixD">"end": 5,</span><br>
						<span class="sixD">"weight": 0,</span><br>
						<span class="sixD">"commonNoun": 0,</span><br>
						<span class="sixD">"beginSid": 0,</span><br>
						<span class="sixD">"endSid": 0</span><br>
						<span class="fivD">}</span><br>
						<span class="fourD">]</span><br>
						<span class="thirD">}</span><br>
						<span class="twoD">]</span><br>
						<span>}</span><br>
						}
					</div>
				</div>
			</div>
			<!--//.guide_box-->
		</div>
		<!--//nlumenu-->
		<!--.nluexample-->
		<div class="demobox" id="nluexample">
			<p><em style="font-weight: 400;color:#27c1c1;">Use Cases</em>  </p>
			<span class="sub">Find out how AI can be applied to diverse area.</span>
			<%--자연어 이해(NLU)--%>
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>Conversation analysis</span>
							</dt>
							<dd class="txt">Analyze various types of conversations in chatbot or phone calls.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_bqa"><span>NQA</span></li>
									<li class="ico_sds"><span>SDS</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>Text analysis</span>
							</dt>
							<dd class="txt">Analyze unstructured documents and text in a variety of fields.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
									<li class="ico_if"><span>ITF</span></li>
									<li class="ico_xdc"><span>XDC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 03</em>
								<span>Automatic classification</span>
							</dt>
							<dd class="txt">Automatically classify and analyze text by category.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
									<li class="ico_if"><span>ITF</span></li>
									<li class="ico_xdc"><span>XDC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //자연어 이해(NLU) -->
		</div>
	</div>
</div>
<!-- //.contents -->



<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/nlu.js"></script>

<%--<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/nlu/app.js"></script>--%>

<%--<script src="${pageContext.request.contextPath}/aiaas/kr/js/nlu/dist/main.js"></script>--%>


<script type="text/javascript">
	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){
			//nlu

			$('.nlu_box .step01 textarea').on('input keyup paste', function() {
				var txtValLth = $(this).val().length;

				if ( txtValLth > 0) {
					$('.nlu_box .step01 .btn_area button').removeClass('disable');
					$('.nlu_box .step01 .btn_area button').removeAttr('disabled');
					$('.nlu_box .step01 .btn_area .disBox').remove();
				} else {
					// $('.nlu_box .step01 .btn_area button').addClass('disable');
					// $('.nlu_box .step01 .btn_area button').attr('disabled');
					// $('.nlu_box .step01 .btn_area').append('<span class="disBox"></span>');
					$('.nlu_box .step02').fadeOut();
				}
			});

            $('.nlu_box .step01 .btn_search').on('click',function(){
				var language = $("input[type=radio][name=lang]:checked").val();

                $.ajax({
                    type:'POST',
                    url:'/api/nlu/nluApi',
                    data: {
                        "lang": language,
                        "reqText": $('#text-contents-nlp').val(),
                        "${_csrf.parameterName}" : "${_csrf.token}"
                    },
                    error: function(error){
                        console.log(error);
                    },
                    success: function(data){
                        var decodedData = decodeURIComponent(data);

                        console.log("result:"+decodedData);
                        $('.nlu_box .step02').fadeIn();

                        procResult(decodedData);
                    }
                });
            });
			//
			// $('input[name="syntax"]').click(function() {
			//
			// 	if($(this).prop('checked')){
			// 		$('input[type="checkbox"][name="syntax"]').prop('checked', false);
			// 		$(this).prop('checked', true);
			// 	}
			//
			// 	console.dir($('.result_text'));
			// 	console.dir($('input[name="syntax"]:checked').val());
			//
			// 	$(".result_text").attr("hidden", "hidden");
			//
			// 	if ($('input[name="syntax"]:checked').val() == 'part') {
			// 		$(".text_part").removeAttr("hidden");
			// 		return;
			// 	}
			// 	if ($('input[name="syntax"]:checked').val() == 'lemma') {
			// 		$(".text_lemma").removeAttr("hidden");
			// 		return;
			// 	}
			// 	if ($('input[name="syntax"]:checked').val() == 'entity'){
			// 		$(".text_entity").removeAttr("hidden")
			// 	}
			// })
			//nlu


		});

	});

    /*
    ** 수신된 결과 메세지 처리
    */
    function procResult(data) {
		initUI();

		var language = $("input[type=radio][name=lang]:checked").val();
		if(language == "kor") procResult_Korean(data);
		else procResult_English(data);
	}

	function procResult_Korean(data) {

		var jsonObj = JSON.parse(data);
		var sentences = jsonObj.document.sentences;

        for(var zz = 0; zz < sentences.length; zz++) {
			var morphEvals = jsonObj.document.sentences[zz].morphEvals;

			for (var xx = 0; xx < morphEvals.length; xx++) {
				var part = "";
				var lemma = "";
				var target = morphEvals[xx].target;
				var results = morphEvals[xx].result.split("+");
				for (var yy = 0; yy < results.length; yy++) {
					var tokens = results[yy].split("/");
					lemma += (yy == 0 ? tokens[0] : "+" + tokens[0]);
					part += (yy == 0 ? tokens[1] : "+" + tokens[1]);
				}

				addResultUI(target, part, lemma);
			}
		}
    }

	function procResult_English(data) {

		var jsonObj = JSON.parse(data);
		var sentences = jsonObj.document.sentences;

		for(var zz = 0; zz < sentences.length; zz++) {
			var morps = jsonObj.document.sentences[zz].morps;
			var words = jsonObj.document.sentences[zz].words;

			for (var xx = 0; xx < morps.length; xx++) {
				var word = words[xx].text;
				var lemma = morps[xx].lemma;
				var part = morps[xx].type;

				addResultUI(word, part, lemma);
			}
		}
	}

    /*
    **
    */
	function addResultUI(target, part, lemma) {
		$('.text_word').append("<td>" + target + "</td>");
		$('.text_lemma').append("<td>" + lemma + "</td>");
		$('.text_part').append("<td>" + part + "</td>");
	}

	function initUI() {
		$('.text_word').empty();
		$('.text_lemma').empty();
		$('.text_part').empty();
	}

	$('#part').on('click',function(){
		var chk = $(this).is(":checked");
		if(chk) $(".text_part").fadeIn(300);
		else  $(".text_part").fadeOut(400);

	});

	$('#lemma').on('click',function(){
		var chk = $(this).is(":checked");
		if(chk) $(".text_lemma").fadeIn(300);
		else  $(".text_lemma").fadeOut(400);
	});
	
	$('input[type=radio][name=lang]').on('click',function(){
		var chk = $("input[type=radio][name=lang]:checked").val();
		if(chk == 'kor'){
			$('#text-contents-nlp').val('사막이 아름다운 건 어디엔가 우물이 숨어있기 때문이야');
		}else{
			$('#text-contents-nlp').val('The true beauty of the desert is the oasis that is hidden.');
		}
	});

	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}

	document.getElementById("defaultOpen").click();
</script>
