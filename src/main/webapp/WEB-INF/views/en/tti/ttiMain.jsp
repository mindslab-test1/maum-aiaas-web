<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<script type="text/javascript">
	var data;
	$(document).ready(function(){
		
		$('#sub').on('click',function () {
			var formData = new FormData();

			var length = $("#length").val();
			var type = $("#type").val();
			var color = $("#color").val();

			if(length == ''){
				alert("Please choose the sleeve length.");
				$("#length").focus();
				return;
			}
			if(type == ''){
				alert("Please select the type of clothes.");
				$("#type").focus();
				return;
			}
			if(color == ''){
				alert("Please select a color.");
				$("#color").focus();
				return;
			}			 		
			
			var reqText = '';

			if($("#length").val() != ''){
				reqText = $("#length").val();
			}
			if($("#fabric").val() != ''){
				reqText = reqText+' '+$("#fabric").val();
			}
			if($("#type").val() != ''){
				reqText = reqText+' '+$("#type").val();
			}
			if($("#color").val() != ''){
				reqText = reqText+' in '+$("#color").val()+'.';
			}
			if($("#neck").val() != ''){
				reqText = reqText+' '+$("#neck").val();
			}	
			if($("#zip").val() != ''){
				reqText = reqText+' with '+$("#zip").val()+'.';
			}			
			
			$("#reqText").val(reqText);
			//alert(reqText);
			
			$(".sentence").text(reqText);			
			
			formData.append('reqText',reqText);
			formData.append('${_csrf.parameterName}','${_csrf.token}');

			var request = new XMLHttpRequest();

			// var imageTag = document.querySelector('img');
			request.responseType="blob";
			request.onreadystatechange=function () {
				if(request.readyState===4){
					console.log(request.response);
					var blob = request.response;
					var imgSrcURL = URL.createObjectURL(blob);
					console.log(imgSrcURL);
					console.log(blob.size);
					
					data = new Blob([request.response], {type:'image/jpeg'});
					downloadResultImg();					

					if(blob.size === 1175){
						alert('TTI image does not exist.');
						// $("#reqText").focus();
						$('.tti_2').hide();
						$('.tti_1').fadeIn(300);
					}else{
						// imageTag.src=imgSrcURL;
						var tti_img = document.getElementById('ttiImg');
						//var blob = request.response;
						tti_img.setAttribute("src", imgSrcURL );
						$('.tti_1').hide();
						$('.tti_2').hide();
						$('.tti_3').fadeIn(300);										
					}
				}
			};
			request.open('POST','/api/getTtiImage');
			request.send(formData);
			
			$('.tti_1').hide();
			$('.tti_2').fadeIn(300);

		})
	});

	function downloadResultImg(){
		var link = document.getElementById("save");	
		link.href = URL.createObjectURL(data);
		link.download = "TTI_IMAGE.JPG" ;					
	}

	/** 이미지 생성  */
</script>

		<!-- .contents -->
		<div class="contents">
			<!-- .content -->
			<div class="content api_content">
				<h1 class="api_tit">AI Styling</h1>
				<ul class="menu_lst vision_lst">
					<li class="tablinks" onclick="openTap(event, 'ttidemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
					<li class="tablinks" onclick="openTap(event, 'ttiexample')"><button type="button">Use Case</button></li>
					<li class="tablinks" onclick="openTap(event, 'ttimenu')"><button type="button">Manual</button></li>
<%--					<li class="tablinks"><a href="/member/enApiAccount" >API ID, key</a></li>--%>
				</ul>
				<!-- .demobox -->
				<div class="demobox" id="ttidemo">
					<p>AI Styling using <span>TTI</span> <small> (Text-To-Image)</small></p>
					<span class="sub">Create your own fashion style using TTI. Input text would produce a corresponding image.</span>
					<!--tti_box-->
					<div class="tti_box">
					<input type="hidden" id="reqText" name="reqText" value="" />
						<!--tti_box-->
						<div class="demo_layout tti_box">
							<!--tti_1-->
							<div class="tti_1" >
								<div class="tti_selectarea">
									<div class="box_select necessary">
										<span>*Required</span>
										<div class="selectType02">
											<label for="length">Sleeve</label>
											<select id="length">
												<option value="">*Sleeve</option>
												<option value="Long sleeve">Long sleeve</option>
												<option value="Short sleeve">Short sleeve</option>
												<option value="Sleeveless">Sleeveless</option>
											</select>
										</div>
										<div class="selectType02">
											<label for="type">Type</label>
											<select id="type">
												<option value="">*Type</option>
												<option value="Jacket">Jacket</option>
												<option value="Maxi Dress">Maxi Dress</option>
												<option value="Shirt">Shirt</option>
												<option value="T-shirt">T-shirt</option>
											</select>
										</div>										
										<div class="selectType02">
											<label for="color">Color</label>
											<select id="color">
												<option value="">*Color</option>
												<option value="Beige">Beige</option>
												<option value="Black">Black</option>
												<option value="Blue">Blue</option>
												<option value="Brown">Brown</option>
												<option value="Dark blue">Dark blue</option>
												<option value="Green">Green</option>
												<option value="Grey">Grey</option>
												<option value="Indigo blue">Indigo blue</option>
												<option value="Light blue">Light blue</option>
												<option value="Red">Red</option>
												<option value="White">White</option>
											</select>
										</div>
									</div>
									<div class="box_select optional">
										<span>Optional</span>
										<div class="selectType02">
											<label for="fabric">Fabric</label>
											<select id="fabric">
												<option value="">Fabric</option>
												<option value="Cotton">Cotton</option>
												<option value="Knit">Knit</option>
												<option value="Linen">Linen</option>
											</select>
										</div>										
										<div class="selectType02">
											<label for="neck">Neck type</label>
											<select id="neck">
												<option value="">Neck type</option>
												<option value="Band collar">Band collar</option>
												<option value="Crewneck">Crewneck</option>
												<option value="V-neck-collar">V-neck collar</option>
											</select>
										</div>
										<div class="selectType02">
											<label for="zip">Button/Zipper</label>
											<select id="zip">
												<option value="">Button/Zipper</option>
												<option value="Button closure">Button closure</option>
												<option value="Zip closure">Zip closure</option>
											</select>
										</div>
									</div>
								</div>
								<div class="styling_box">
									<p><em class="fas fa-tshirt"></em>Selected items</p>
									<ul>

									</ul>
									<div class="sentence_box">
										<p>Input Text</p>
										<div class="sentence"></div>
									</div>
									
								</div>
								<div class="btn_area">
									<button type="button" class="btn_start" id="sub">Process</button>
								</div>
							</div>
							<!--tti_1-->
							<!--tti_2-->
							<div class="tti_2">
								<p><em class="fas fa-tshirt"></em>In progress</p>
								<div class="loding_box ">
									<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#f7778a" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>
									<p>AI processing takes a while..</p>
								</div>
								<div class="btn_area">
									<button type="button" class="btn_back1" id=""><em class="fas fa-redo"></em>Reset</button>
								</div>

							</div>
							<!--tti_2-->
							<!--tti_3-->
							<div class="tti_3">
								<p><em class="fas fa-tshirt"></em>Result</p>
								<div class="result_img">
									<img id="ttiImg" src="" alt="ttiImg" alt="Result image">
								</div>
								<a id="save" onclick="downloadResultImg();" class="dwn_link" ><em class="far fa-arrow-alt-circle-down"></em> Download Result</a>

								<div class="styling_box">
									<ul>
										<li>Concealed zip closure at side</li>
										<li>Shirt</li>
										<li>Mini Dress</li>
										<li>Dark Blue</li>
										<li>Japanese selvedge denim</li>
										<li>Mini Dress</li>
										<li>Dark Blue</li>
									</ul>
									<div class="sentence_box">
										<p>Input Text</p>
										<div class="sentence"></div>
									</div>
								</div>

								<div class="btn_area">
									<button type="button" class="btn_back2" id=""><em class="fas fa-redo"></em>Reset</button>
								</div>
							</div>
							<!--tti_3-->
						</div>
						<!--tti_box-->
						<span class="remark">*AI Styling is a sample to show how TTI works. Contact us if you need any other models.</span>
						<!--// tti-->
					
					</div>
				</div>
				<!-- //.demobox -->

				<!--.ttimenu-->
				<div class="demobox vision_menu" id="ttimenu">
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API Guideline
							</div>
							<p class="sub_title">Set up Environment</p>
							<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
							<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
							<p class="sub_title">ID &amp; Key</p>
							<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
							<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
							<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
								<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
							<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
							<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
							<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
						</div>
						<div class="guide_group">
							<div class="title">
								Text-To-Image (TTI)
							</div>
							<p class="sub_txt"> Image is shown as the input text description. Currently, only clothes styles (AI styling service) are available.</p>

							<span class="sub_title">
								Preparation
							</span>
							<p class="sub_txt">- Input: Description of the clothes, following the format below (text)</p>
							<ul>
								<li>Top format <br>
									(*required) Sleeve + Type + Color<br>
									(optional) Fit, neck type(ex. v-neck collar, crewneck) button or zip closure<br>
									etc
								</li>
								<li>Bottom format<br>
									(*required) Fit + Type + Color<br>
									(optional) Fabric(ex.denim, linen) button or zip fly etc.<br>
								</li>
							</ul>
							<p class="sub_txt">- Input example</p>
							<ul>
								<li>Top example 1)<br>
									Long sleeve flannel shirt in burgundy and white. Signature check print throughout. V-neck collar. Button closure at front. Single-button barrel cuffs. Tonal stitching.</li>
								<li>Top example 2)<br>
									Long sleeve padded nylon bomber jacket in 'khaki' green. Oversized fit.
									Slips on. Rib knit modified stand collar, cuffs, and hem. Vented central
									seam and flap pockets at front. Utility pocket at upper sleeve. Tonal
									stitching.
								</li>
								<li>Bottom example 1)<br>
									Slim-fit jeans in 'dark vintage' indigo. High-rise. Japanese selvedge denim. Fading, whiskering, and honeycombing throughout. cutting left knee. Five-pocket styling. Zip-fly. Silver-tone hardware. Contrast stitching in tan.
								</li>
								<li>Bottom example 2)<br>
									Circle skirt in black. Concealed zip closure at side. Tonal stitching
								</li>
							</ul>
							<span class="sub_title">
								 API Document
							</span>
							<p class="sub_txt">① Request  </p>
							<ul>
								<li>Method : POST</li>
								<li>URL : https://api.maum.ai/api/tti_image/</li>
							</ul>
							<p class="sub_txt">② Request parameters </p>
							<table>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th>type</th>
								</tr>
								<tr>
									<td>apiId </td>
									<td>Unique API ID. Request from is required for the ID. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>apiKey </td>
									<td>Unique API key. Request from is required for the key. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>reqText </td>
									<td>Text description of the clothes, following the format </td>
									<td>string</td>
								</tr>
							</table>
							<p class="sub_txt">③ Request example </p>
							<div class="code_box">
								curl -X POST \<br>
								https://api.maum.ai/api/tti_image/ \<br>
								-H 'Content-Type: application/json' \<br>
								-d '{<br>
								"apiId": "(*Request for ID)",<br>
								"apiKey": "(*Request for key)",<br>
								"reqText": "Long sleeve flannel shirt in burgundy<br>
								and white. Signature check print throughout. V-neck<br>
								collar. Button closure at front. Single-button barrel<br>
								cuffs. Tonal stitching."<br>
								}<br>
							</div>

							<p class="sub_txt">④ Response example </p>
							<div class="code_box">
								<img src="${pageContext.request.contextPath}/aiaas/kr/images/img_tti.png" alt="text removal image" style="width:20%;">
							</div>
						</div>
					</div>
					<!--//.guide_box-->
				</div>
				<!--.ttimenu-->
				<!--.ttiexample-->
				<div class="demobox" id="ttiexample">
					<p><em style="font-weight: 400;color:#f7778a;">Use Cases</em>  </p>
					<span class="sub">Find out how AI can be applied to diverse area.</span>
					<!-- AI 스타일링(TTI) -->
					<div class="useCasesBox">
						<ul class="lst_useCases">
							<li>
								<dl>
									<dt>
										<em>CASE 01</em>
										<span>Image Search for <strong>Shopping</strong></span>
									</dt>
									<dd class="txt">You can search of the product you want more easily by entering the features such as color, size, pattern, etc</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_tti"><span>TTI</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>Montage Creation</span>
									</dt>
									<dd class="txt">By describing the features of the face such as eyes, nose, and mouth, you can create an image of a specific person by creating an image as it is.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_tti"><span>TTI</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 03</em>
										<span>Design Mockup</span>
									</dt>
									<dd class="txt">You can easily create and combine images at the design identification stage.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_tti"><span>TTI</span></li>
										</ul>
									</dd>
								</dl>
							</li>
						</ul>
					</div>
					<!-- //AI 스타일링(TTI) -->
				</div>
				<!--//.ttiexample-->

			</div>
			<!-- //.content -->
		</div>
		<!-- //.contents -->

<script>
	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){
			
			$('.selectType02').on('click', function () {
				textMake();
				$(".styling_box ul li").remove();
				
				var selected1 = $("#length option:selected");
				var selected2 = $("#type option:selected");
				var selected3 = $("#color option:selected");
				var selected4 = $("#neck option:selected");
				var selected5 = $("#fabric option:selected");
				var selected6 = $("#zip option:selected");
				
				var output = "";
				
				if(selected1.val() != 0){
					output = "<li>" + selected1.text() +"</li>";
					$(".styling_box ul").append(output);
				}
				
				if(selected5.val() != 0){
					output = "<li class='optional'>" + selected5.text() +"</li>";
					$(".styling_box ul").append(output); 
				}				

				if(selected2.val() != 0){
					output = "<li>" + selected2.text() +"</li>";
					$(".styling_box ul").append(output);
				}				
				
				if(selected3.val() != 0){
					output = "<li>" + selected3.text() +"</li>";
					$(".styling_box ul").append(output); 
				}
				
				if(selected4.val() != 0){
					output = "<li class='optional'>" + selected4.text() +"</li>";
					$(".styling_box ul").append(output); 
				}				
								
				if(selected6.val() != 0){
					output = "<li class='optional'>" + selected6.text() +"</li>";
					$(".styling_box ul").append(output); 
				}				

				if(output == ''){
					$(".styling_box ul").append(output); 
				}

			});			


			// step1->step2
/* 			$('.btn_start').on('click', function () {
				$('.tti_1').hide();
				$('.tti_2').fadeIn(300).move();
			}); */

			// step2->step3
			$('.btn_back1').on('click', function () {
				$('.tti_2').hide();
				$('.tti_3').fadeIn(300);

			});

			// step3->step1
			$('.btn_back2').on('click', function () {
/* 				$(".styling_box ul li").remove();
				$("#length").val('');
				$("#fabric").val('');
				$("#type").val('');
				$("#color").val('');
				$("#neck").val('');
				$("#zip").val('');	 */			
				
				$('.tti_3').hide();
				$('.tti_1').fadeIn(300);

			});


			$("#type").change(onSelectChange1);          //select  id를 이용하여 셀렉트 변경시마다 onSelectChange함수 실행

			function onSelectChange1(){
				var selected = $("#type option:selected");
				var output = "";
				if(selected.val() != 0){
					output = "<li>" + selected.text() +"</li>";
				}
				$(".styling_box ul").append(output);          //div에 output 변수에 담은 text HTML로 출력하기
			}

			$("#length").change(onSelectChange2);          //select  id를 이용하여 셀렉트 변경시마다 onSelectChange함수 실행

			function onSelectChange2(){
				var selected = $("#length option:selected");
				var output = "";
				if(selected.val() != 0){
					output = "<li>" + selected.text() +"</li>";
				}
				$(".styling_box ul").append(output);          //div에 output 변수에 담은 text HTML로 출력하기
			}


			$("#color").change(onSelectChange3);          //select  id를 이용하여 셀렉트 변경시마다 onSelectChange함수 실행

			function onSelectChange3(){
				var selected = $("#color option:selected");
				var output = "";
				if(selected.val() != 0){
					output = "<li>" + selected.text() +"</li>";
				}
				$(".styling_box ul").append(output);          //div에 output 변수에 담은 text HTML로 출력하기
			}

			$("#neck").change(onSelectChange4);          //select  id를 이용하여 셀렉트 변경시마다 onSelectChange함수 실행

			function onSelectChange4(){
				var selected = $("#neck option:selected");
				var output = "";
				if(selected.val() != 0){
					output = "<li>" + selected.text() +"</li>";
				}
				$(".styling_box ul").append(output);          //div에 output 변수에 담은 text HTML로 출력하기
			}

			$("#fabric").change(onSelectChange5);          //select  id를 이용하여 셀렉트 변경시마다 onSelectChange함수 실행

			function onSelectChange5(){
				var selected = $("#fabric option:selected");
				var output = "";
				if(selected.val() != 0){
					output = "<li>" + selected.text() +"</li>";
				}
				$(".styling_box ul").append(output);          //div에 output 변수에 담은 text HTML로 출력하기
			}
			$("#zip").change(onSelectChange6);          //select  id를 이용하여 셀렉트 변경시마다 onSelectChange함수 실행

			function onSelectChange6(){
				var selected = $("#zip option:selected");
				var output = "";
				if(selected.val() != 0){
					output = "<li>" + selected.text() +"</li>";
				}
				$(".styling_box ul").append(output);          //div에 output 변수에 담은 text HTML로 출력하기
			}
			
			function textMake(){
				var reqText = '';

				if($("#length").val() != ''){
					reqText = $("#length").val();
				}
				if($("#fabric").val() != ''){
					reqText = reqText+' '+$("#fabric").val();
				}
				if($("#type").val() != ''){
					reqText = reqText+' '+$("#type").val();
				}
				if($("#color").val() != ''){
					reqText = reqText+' in '+$("#color").val()+'.';
				}
				if($("#neck").val() != ''){
					reqText = reqText+' '+$("#neck").val();
				}	
				if($("#zip").val() != ''){
					reqText = reqText+' with '+$("#zip").val()+'.';
				}
				
				$(".sentence").text(reqText);				
			}


		});
	});

	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();

</script>