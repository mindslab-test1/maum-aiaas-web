<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2019-10-25
  Time: 14:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/vr.css?ver=<%=fmt.format(lastModifiedStyle)%>" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/pop_common.css" />
<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>Upload error</h5>
            <p>Error occurred due to the voice or file size.<br>
                Please check the restriction again.</p>
            <span> * .wav file only<br>
                   * Sample rate 16000<br>
                   * mono channel<br>
                   * Under 2MB file size<br>
                   * No spaces or special characters in the file name is allowed.</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">OK</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->


<!-- .contents -->
<div class="contents">
    <div class="content api_content">
        <h1 class="api_tit">Voice Recognition</h1>
        <ul class="menu_lst voice_menulst">
            <li class="tablinks" onclick="openTap(event, 'vrdemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
            <li class="tablinks" onclick="openTap(event, 'vrexample')"><button type="button">Use Case</button></li>
            <li class="tablinks" onclick="openTap(event, 'vrmenu')"><button type="button">Manual</button></li>
<%--            <li class="tablinks"><a href="/member/enApiAccount">API ID & Key</a></li>--%>
        </ul>
        <!--demobox-->
        <div class="demobox" id="vrdemo">
            <p><span>Voice Recognition</span></p>
            <span class="sub">Vectorize human voices into 512 dimensions and compare the values to recognize and identify each speaker. <br>
                <small>* Surely noted that we do not save any sound files. We only save the vectorized values of the registered voices and these are discarded immediately after the result.</small></span>
            <!--faceR_box-->
            <div class="demo_layout voiceR_box">
                <div class="sampletest_box">
                    <p><em class="far fa-file-image"></em>Use Sample File</p>
                    <div class="demo_layout">
                        <div class="small_layout fl-data">
                            <p><em class="fas fa-database"></em> Preregistered Voices</p>
                            <ul>
                                <li>
                                    <div class="img_area">
                                        <img src="${pageContext.request.contextPath}/aiaas/common/images/ico-vr-data-1.svg" alt="sample img" style="width:28px;"/>
                                    </div>
                                    <P>VoiceID :<span>Seonkyun</span></P>
                                    <!--player-->
                                    <div class="player">
                                        <div class="button-items">
                                            <audio id="music1" class="music" preload="auto" onended="audioEnded($(this))" src="${pageContext.request.contextPath}/aiaas/common/audio/voiceRecog/한글_이중건.wav">
                                                <%--<source src="${pageContext.request.contextPath}/aiaas/common/audio/voiceRecog/한글_이중건.wav" >--%>
                                                <p>Your browser doesn't support html5 audio.</p>
                                            </audio>
                                            <div class="slider">
                                                <div class="elapsed"></div>
                                            </div>
                                            <p class="timer">0:00</p>
                                            <p class="timer_fr">0:00</p>
                                            <div class="controls">
                                                <div class="play" >
                                                </div>
                                                <div class="pause" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--player-->
                                </li>
                                <li>
                                    <div class="img_area">
                                        <img src="${pageContext.request.contextPath}/aiaas/common/images/ico-vr-data-2.svg" alt="sample img" style="width:34px;"/>
                                    </div>
                                    <P>VoiceID :<span>Suzie</span></P>
                                    <!--player-->
                                    <div class="player">
                                        <div class="button-items">
                                            <audio id="music2" class="music" preload="auto" onended="audioEnded($(this))" src="${pageContext.request.contextPath}/aiaas/common/audio/voiceRecog/애국가_이미수.wav">
                                                <%--<source src="${pageContext.request.contextPath}/aiaas/common/audio/voiceRecog/애국가_이미수.wav" >--%>
                                                <p>Alas, your browser doesn't support html5 audio.</p>
                                            </audio>
                                            <div class="slider">
                                                <div class="elapsed"></div>
                                            </div>
                                            <p class="timer">0:00</p>
                                            <p class="timer_fr">0:00</p>
                                            <div class="controls">
                                                <div class="play" >
                                                </div>
                                                <div class="pause" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--player-->
                                </li>
                                <li>
                                    <div class="img_area">
                                        <img src="${pageContext.request.contextPath}/aiaas/common/images/ico-vr-data-3.svg" alt="sample img" style="width:28px;"/>
                                    </div>
                                    <P>VoiceID :<span>Woo</span></P>
                                    <!--player-->
                                    <div class="player">
                                        <div class="button-items">
                                            <audio id="music3" class="music" preload="auto" onended="audioEnded($(this))" src="${pageContext.request.contextPath}/aiaas/common/audio/voiceRecog/장동환_3.wav">
                                                <%--<source src="${pageContext.request.contextPath}/aiaas/common/audio/voiceRecog/장동환_3.wav" >--%>
                                                <p>Alas, your browser doesn't support html5 audio.</p>
                                            </audio>
                                            <div class="slider">
                                                <div class="elapsed"></div>
                                            </div>
                                            <p class="timer">0:00</p>
                                            <p class="timer_fr">0:00</p>
                                            <div class="controls">
                                                <div class="play" >
                                                </div>
                                                <div class="pause" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--player-->
                                </li>
                                <li>
                                    <div class="img_area">
                                        <img src="${pageContext.request.contextPath}/aiaas/common/images/ico-vr-data-4.svg" alt="sample img" style="width:34px;"/>
                                    </div>
                                    <P>VoiceID :<span>Youngeah</span></P>
                                    <!--player-->
                                    <div class="player">
                                        <div class="button-items">
                                            <audio id="music4" class="music" preload="auto" onended="audioEnded($(this))" src="${pageContext.request.contextPath}/aiaas/common/audio/voiceRecog/백선자_2.wav">
                                                <%--<source src="${pageContext.request.contextPath}/aiaas/common/audio/voiceRecog/백선자_2.wav" >--%>
                                                <p>Alas, your browser doesn't support html5 audio.</p>
                                            </audio>
                                            <div class="slider">
                                                <div class="elapsed"></div>
                                            </div>
                                            <p class="timer">0:00</p>
                                            <p class="timer_fr">0:00</p>
                                            <div class="controls">
                                                <div class="play" >
                                                </div>
                                                <div class="pause" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--player-->
                                </li>
                            </ul>
                        </div>
                        <div class="small_layout fr_voice">
                            <p><em class="fas fa-file-audio"></em> Voice to Recognize</p>
                            <ul>
                                <li class="radio">
                                    <input type="radio" id="sample1" name="option" checked src="${pageContext.request.contextPath}/aiaas/common/audio/voiceRecog/영어_이중건.wav">
                                    <label for="sample1" class="sample">
                                        <span>Voice 1</span>
                                        <!--player-->
                                        <div class="player">
                                            <div class="button-items">
                                                <audio id="music5" class="music" preload="auto" onended="audioEnded($(this))">
                                                    <source src="${pageContext.request.contextPath}/aiaas/common/audio/voiceRecog/영어_이중건.wav" >
                                                    <p>Alas, your browser doesn't support html5 audio.</p>
                                                </audio>
                                                <div class="slider">
                                                    <div class="elapsed"></div>
                                                </div>
                                                <p class="timer">0:00</p>
                                                <p class="timer_fr">0:00</p>
                                                <div class="controls">
                                                    <div class="play" >
                                                    </div>
                                                    <div class="pause" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--player-->
                                    </label>
                                </li>
                                <li class="radio">
                                    <input type="radio" id="sample2" name="option" src="${pageContext.request.contextPath}/aiaas/common/audio/voiceRecog/클래식_이미수.wav">
                                    <label for="sample2" class="sample">
                                        <span>Voice 2</span>
                                        <!--player-->
                                        <div class="player">
                                            <div class="button-items">
                                                <audio id="music6" class="music" preload="auto" onended="audioEnded($(this))">
                                                    <source src="${pageContext.request.contextPath}/aiaas/common/audio/voiceRecog/클래식_이미수.wav" >
                                                    <p>Alas, your browser doesn't support html5 audio.</p>
                                                </audio>
                                                <div class="slider">
                                                    <div class="elapsed"></div>
                                                </div>
                                                <p class="timer">0:00</p>
                                                <p class="timer_fr">0:00</p>
                                                <div class="controls">
                                                    <div class="play" >
                                                    </div>
                                                    <div class="pause" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--player-->
                                    </label>
                                </li>
                                <li class="radio">
                                    <input type="radio" id="sample3" name="option" src="${pageContext.request.contextPath}/aiaas/common/audio/voiceRecog/윤현주_1.wav">
                                    <label for="sample3" class="sample">
                                        <span>Voice 3</span>
                                        <!--player-->
                                        <div class="player">
                                            <div class="button-items">
                                                <audio id="music7" class="music" preload="auto" onended="audioEnded($(this))">
                                                    <source src="${pageContext.request.contextPath}/aiaas/common/audio/voiceRecog/윤현주_1.wav" >
                                                    <p>Alas, your browser doesn't support html5 audio.</p>
                                                </audio>
                                                <div class="slider">
                                                    <div class="elapsed"></div>
                                                </div>
                                                <p class="timer">0:00</p>
                                                <p class="timer_fr">0:00</p>
                                                <div class="controls">
                                                    <div class="play" >
                                                    </div>
                                                    <div class="pause" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--player-->
                                    </label>
                                </li>

                            </ul>
                        </div>
                        <div class="btn_area">
                            <button type="button" class="btn_start" id="sampleTest">Process</button>
                        </div>
                    </div>
                </div>
                <div class="myfile_box">
                    <p><em class="far fa-file-image"></em>Use My File</p>
                    <div class="demo_layout">
                        <div class="small_layout">
                            <p><em class="fas fa-database"></em> Voice Registration</p>
                            <div class="data_regist">
                                <div class="example_box">
                                    <P>VoiceID :<span>MindsLab</span></P>
                                    <!--player-->
                                    <div class="player">
                                        <div class="button-items">
                                            <audio id="music8" class="music" preload="auto" onended="audioEnded($(this))">
                                                <source src="${pageContext.request.contextPath}/aiaas/common/audio/voiceRecog/한글_이중건.wav" >
                                                <p>Alas, your browser doesn't support html5 audio.</p>
                                            </audio>
                                            <div class="slider">
                                                <div class="elapsed"></div>
                                            </div>
                                            <p class="timer">0:00</p>
                                            <p class="timer_fr">0:00</p>
                                            <div class="controls">
                                                <div class="play" >
                                                </div>
                                                <div class="pause" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--player-->
                                </div>

                                <div class="example_desc">
                                    <p>Type VoiceID</p>
                                    <p class="faceid_input">Upload voice file</p>
                                </div>

                                <div class="data_upload">
                                    <div class="uplode_box">
                                        <em id="close1" class="fas fa-times hidden close"></em>
                                        <div class="input_id">
                                            <p>VoiceID :</p>
                                            <div class="input_level1" id="levelInput1">
                                                <input type="text" placeholder="10 Characters" id = "demoName1"  title="Type voice ID"/>
                                                <button type="submit" id="save1">save</button>
                                            </div>
                                            <div class="input_level2" id="levelText1">
                                                <span></span>
<%--                                                <em class="fas fa-times hidden close"></em>--%>
                                            </div>
                                        </div>
                                        <div id="uploadFile1" class="btn">
                                            <input type="file" id="demoFile1" class="demoFile voiceFile" accept=".wav" >
                                            <label for="demoFile1">Upload File</label>
                                        </div>

                                        <!--player-->
                                        <div class="player">
                                            <div class="button-items">
                                                <audio id="upload_sound1" class="music" preload="auto" onended="audioEnded($(this))" src="${pageContext.request.contextPath}/aiaas/kr/audio/voicefilter/voice_result.wav">
                                                    <p>Your browser doesn't support html5 audio.</p>
                                                </audio>
                                                <div class="slider">
                                                    <div class="elapsed"></div>
                                                </div>
                                                <p class="timer">0:00</p>
                                                <p class="timer_fr">0:00</p>
                                                <div class="controls">
                                                    <div class="play sample_play" >
                                                    </div>
                                                    <div class="pause" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--player-->
                                    </div>
                                    <div class="uplode_box">
                                        <em id="close2" class="fas fa-times hidden close"></em>
                                        <div class="input_id">
                                            <p>VoiceID :</p>
                                            <div class="input_level1" id="levelInput2">
                                                <input type="text" placeholder="10 Characters" id = "demoName2" title="Type voice ID"/>
                                                <button type="submit" id="save2">save</button>
                                            </div>
                                            <div class="input_level2" id="levelText2">
                                                <span></span>
                                            </div>
                                        </div>
                                        <div id="uploadFile2" class="btn">
                                            <input type="file" id="demoFile2" class="demoFile voiceFile" accept=".wav">
                                            <label for="demoFile2">Upload File</label>
                                        </div>
                                        <!--player-->
                                        <div class="player">
                                            <div class="button-items">
                                                <audio id="upload_sound2" class="music" preload="auto" onended="audioEnded($(this))" src="${pageContext.request.contextPath}/aiaas/kr/audio/voicefilter/voice_result.wav" >
                                                    <!--<source src="${pageContext.request.contextPath}/aiaas/kr/audio/voicefilter/voice_result.wav">-->
                                                    <p>Your browser doesn't support html5 audio.</p>
                                                </audio>
                                                <div class="slider">
                                                    <div class="elapsed"></div>
                                                </div>
                                                <p class="timer">0:00</p>
                                                <p class="timer_fr">0:00</p>
                                                <div class="controls">
                                                    <div class="play sample_play" >
                                                    </div>
                                                    <div class="pause" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--player-->

                                    </div>
                                    <div class="uplode_box">
                                        <em id="close3" class="fas fa-times hidden close"></em>
                                        <div class="input_id">
                                            <p>VoiceID :</p>
                                            <div class="input_level1" id="levelInput3">
                                                <input type="text" placeholder="10 Characters" id = "demoName3"  title="Type voice ID"/>
                                                <button type="submit" id="save3">save</button>
                                            </div>
                                            <div class="input_level2" id="levelText3">
                                                <span></span>
                                            </div>
                                        </div>
                                        <div id="uploadFile3" class="btn">
                                            <input type="file" id="demoFile3" class="demoFile voiceFile" accept=".wav">
                                            <label for="demoFile3">Upload File</label>
                                        </div>
                                        <!--player-->
                                        <div class="player">
                                            <div class="button-items">
                                                <audio id="upload_sound3" class="music" preload="auto" onended="audioEnded($(this))" src="${pageContext.request.contextPath}/aiaas/kr/audio/voicefilter/voice_result.wav">
                                                    <!--<source src="${pageContext.request.contextPath}/aiaas/kr/audio/voicefilter/voice_result.wav">-->
                                                    <p>Your browser doesn't support html5 audio.</p>
                                                </audio>
                                                <div class="slider">
                                                    <div class="elapsed"></div>
                                                </div>
                                                <p class="timer">0:00</p>
                                                <p class="timer_fr">0:00</p>
                                                <div class="controls">
                                                    <div class="play sample_play" >
                                                    </div>
                                                    <div class="pause" >
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <!--player-->
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="small_layout vr_voice">
                            <p><em class="fas fa-user-check"></em> Voice to Recognize</p>
                            <div class="uplode_box">
                                <div id="uploadFile" class="btn uploadFile1">
                                    <em class="fas fa-times hidden close2"></em>
                                    <input type="file" id="demoFile" class="demoFile" accept=".wav" >
                                    <label for="demoFile" class="demolabel">Upload File</label>

                                    <!--player-->
                                    <div class="player">
                                        <div class="button-items">
                                            <audio id="upload_sound4" class="music" preload="auto" onended="audioEnded($(this))" src="${pageContext.request.contextPath}/aiaas/kr/audio/voicefilter/voice_result.wav">
                                                <p>Your browser doesn't support html5 audio.</p>
                                            </audio>
                                            <div class="slider">
                                                <div class="elapsed" id="elapsed"></div>
                                            </div>
                                            <p class="timer">0:00</p>
                                            <p class="timer_fr">0:00</p>
                                            <div class="controls">
                                                <div class="play sample_play" >
                                                </div>
                                                <div class="pause" id="pause">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!--player-->
                                </div>
                                <p class="upload_desc">
                                    * .wav file only<br>
                                    * Sample rate 16000<br>
                                    * mono channel<br>
                                    * Under 2MB file size</p>

                            </div>
                        </div>
                        <div class="vr_desc">
                            <em class="fas fa-exclamation-circle"></em>
                            <ul>
                                <li>* Upload a mono speaker voice file within 20 seconds.</li>
                                <li>* Remove background music or noises included the file. </li>
                                <li>* The recognition rate will be higher with clear sounds. </li>
                            </ul>
                        </div>
                        <div class="btn_area">
                            <button type="button" class="btn_start" id="demoResult">Process</button>
                        </div>

                    </div>
                </div>
            </div>
            <!--//voiceR_box-->
            <!-- .vr_2 -->
            <div class="vr_2">
                <p><em class="far fa-file-image"></em>In progress</p>
                <div class="loding_box ">
                    <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#c9c3ec" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#7e71d1" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

                    <p>AI processing takes a while..</p>
                </div>
            </div>
            <!-- //.vr_2 -->
            <!-- .vr_3 -->
            <div class="demo_layout vr_3">
                <div class="">
                    <div class="result_box">
                        <p><em class="fas fa-file-image"></em> Input</p>
                        <div class="img_file">
                            <p class="input_tit" id ="voiceName">Voice 1</p>
                            <div class="input_radio">
                                <!--player-->
                                <div class="player">
                                    <div class="button-items">
                                        <audio id="result_audio" class="music" preload="auto" onended="audioEnded($(this))" src="${pageContext.request.contextPath}/aiaas/kr/audio/voicefilter/voice_mixed.wav">
                                            <p>Your browser doesn't support html5 audio.</p>
                                        </audio>
                                        <div class="slider">
                                            <div class="elapsed"></div>
                                        </div>
                                        <p class="timer">0:00</p>
                                        <p class="timer_fr">0:00</p>
                                        <div class="controls">
                                            <div class="play" >
                                            </div>
                                            <div class="pause" >
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--player-->
                            </div>
                        </div>
                    </div>
                    <div class="result_box">
                        <p><em class="fas fa-code"></em> Result</p>
                        <div class="code_value">
                            <p class="result_t">We found a VoiceID which matches your file.</p>
                            <p class="voice_id"><span id="voiceId">VoiceID:</span> <span id="voice_name"> </span></p>
                            <textarea id="result_txt">
                                <%--{
                                    "result": {
                                        "width": 900,
                                        "height": 780,
                                        "faces": [
                                            {
                                                "facial_attributes": {
                                                    "gender": {
                                                        "male": 0.0001852084242273122,
                                                        "female": 0.9998148083686829
                                                    },--%>
                            </textarea>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="reset_btn" id=""><em class="fas fa-redo"></em>Reset</button>
                    </div>
                </div>
            </div>
            <!-- .vr_3 -->
        </div>
        <!-- .demobox -->

        <!--.vrmenu-->
        <div class="demobox voice_menu" id="vrmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API Guideline
                    </div>
                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID &amp; Key</p>
                    <p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                    <p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                    <p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        Voice Recognition
                    </div>
                    <p class="sub_txt">Vectorize speakers’ voices into 512 dimensions and compare to authorize. </p>

                    <span class="sub_title">
						Preparation
					</span>
                    <p class="sub_txt">Input: Sound file  </p>
                    <ul>
                        <li>File type: .wav</li>
                        <li>Sample rate: 16000</li>
                        <li>Channels: mono</li>
                        <li>File size: Under 2MB</li>
                        <li>Note: Clean voice file of a mono speaker</li>
                    </ul>
                    <span class="sub_title">
						 API Document
					</span>
                    <em>​setVoice : Save the 512 dimension vector value of a speaker’s voice (Json Type) </em>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : PUT</li>
                        <li>URL : https://api.maum.ai/dap/app/setVoice</li>
                    </ul>
                    <p class="sub_txt">② Request parameters </p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API  ID. Request from is required for the ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>voiceId</td>
                            <td>Voice ID of the sound file </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>file</td>
                            <td>type:file (.wav). Speaker’s voice file </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>dbId</td>
                            <td>Database ID to save the value (or default)</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
<pre>
curl -X PUT \
  https://api.maum.ai/dap/app/setVoice \
  -H 'content-type: multipart/form-data; boundary=----
WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -F apiId=(*Request for ID) \
  -F apiKey=(*Request for key) \
  -F voiceId=Sample \
  -F 'file=@/sample.wav' \
  -F dbId=default
</pre>
                    </div>

                    <p class="sub_txt">④ Response example </p>
                    <div class="code_box">
<pre>
{
   "message": {
       "message": "success",
       "status": 0
   }
}
</pre>
                    </div>

                    <em>​getVoiceList :View the list of voiceIds and the vector values of the inquired database  </em>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/dap/app/getVoiceList</li>
                    </ul>
                    <p class="sub_txt">② Request parameters </p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API  ID. Request from is required for the ID.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>dbId</td>
                            <td>Selected database ID to view (or  ‘default’) </td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
<pre>
curl -X POST \
  https://api.maum.ai/dap/app/getVoiceList \
  -H 'content-type: multipart/form-data; boundary=----
WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -F apiId=(*Request for ID) \
  -F apiKey=(*Request for key) \
  -F dbId=default
</pre>
                    </div>

                    <p class="sub_txt">④ Response example</p>
                    <div class="code_box" style="height: 400px; overflow: auto;">
<pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "payload": [
        {
            "id": "sample",
            "voiceVector": [
                0.009809233,
                -0.027861923,
                0.060465597,
                -0.037335824,
                0.03953928,
                -0.025660083,
                0.01694968,
                0.022995884,
                2.4288324E-4,
                0.057133432,
                0.031094104,
                0.063374735,
                -0.027848812,
                0.029863311,
                -0.008488096,
                0.034156967,
                0.14236897,
                -0.0062950193,
                0.0036646775,
                -0.0021958333,
                -0.0320131,
                -1.76954E-4,
                0.036208954,
                -0.055880904,
                0.035570636,
                0.020627875,
                0.006681259,
                -0.0019796093,
                0.037381947,
                -0.028503101,
                6.738202E-5,
                0.03817538,
                0.024415942,
                -0.010238754,
                0.030451765,
                -0.005601272,
                -2.3510722E-4,
                0.020820923,
                0.060872715,
                0.03906997,
                0.005054118,
                -0.03369077,
                0.0730733,
                -0.07868075,
                0.0027053927,
                -0.010016247,
                0.040443655,
                -0.07490667,
                0.07754631,
                -0.035335265,
                0.07193332,
                -1.1013727E-5,
                0.040653642,
                -0.05279494,
                0.02453671,
                -0.020987665,
                -0.008240587,
                0.011726744,
                0.011972768,
                -0.003926306,
                0.022958433,
                -0.013988343,
                0.009016266,
                -0.0858423,
                0.012110166,
                0.010407494,
                -0.041388858,
                0.036851082,
                -0.0141512975,
                -0.017419497,
                0.017512677,
                -0.03311504,
                0.0043473984,
                0.011327461,
                0.031291828,
                -0.029129338,
                0.07922917,
                0.103310324,
                -0.01525637,
                0.017001107,
                -0.025571242,
                -0.004663984,
                -0.042515498,
                0.011574684,
                0.001828526,
                0.04303329,
                -0.10616526,
                0.022349887,
                -0.01342476,
                -0.05554427,
                -0.05793003,
                2.1502345E-4,
                -0.039191674,
                -3.533895E-4,
                0.019958127,
                -0.002522439,
                -0.044083796,
                0.06438759,
                0.10039652,
                0.017085595,
                -0.07231451,
                -0.01656865,
                -0.062545925,
                0.0039045727,
                0.03886925,
                0.021331912,
                -0.0012101132,
                0.07945559,
                0.0986749,
                -0.048246328,
                -0.10055209,
                0.0065054265,
                0.03214955,
                -0.04959223,
                -0.09960715,
                0.005613625,
                0.064961284,
                -0.031243833,
                0.029929308,
                -0.07417744,
                0.035052262,
                0.0027970455,
                0.022888662,
                -0.035681725,
                -0.0029807293,
                0.020994946,
                0.017806549,
                0.05107863,
                0.046685055,
                0.050038848,
                -0.044458337,
                -0.0029464283,
                0.0010194837,
                0.0074195107,
                0.02554408,
                -0.03215673,
                -0.010279824,
                -0.0052537075,
                0.035765152,
                0.007952438,
                -0.004114616,
                -0.011665499,
                0.0857483,
                0.07563582,
                -0.040942866,
                -0.065931424,
                -0.006752581,
                0.042961795,
                8.8007475E-4,
                0.02059066,
                -0.064116776,
                -0.014791866,
                0.020248856,
                0.009028454,
                0.0056533944,
                -0.043099575,
                0.006110476,
                0.03066875,
                -0.030606028,
                -0.007895153,
                0.13300896,
                0.025322953,
                -0.016618777,
                -0.030923229,
                -0.01147133,
                0.028637918,
                -0.0760449,
                -0.06692001,
                2.9230854E-4,
                -0.07823184,
                -0.014634549,
                -0.034325052,
                0.062458996,
                -0.008574475,
                0.056351613,
                -0.03218825,
                0.019695122,
                0.013588774,
                -0.010834069,
                0.05055752,
                -0.07259933,
                -0.004892468,
                0.011273691,
                0.02889353,
                0.116513304,
                -0.015315207,
                -0.014717047,
                0.011938759,
                -0.016767293,
                0.046930835,
                0.06919864,
                0.035431504,
                -0.007778358,
                0.051606867,
                -0.04833493,
                -0.026436396,
                0.03429136,
                -0.0151697835,
                -0.081333935,
                0.006049792,
                -0.0026798707,
                -0.04634912,
                0.030275181,
                -0.0824539,
                -0.08748281,
                0.0060032047,
                0.009597121,
                0.0077144313,
                0.0045434902,
                -0.04858411,
                0.029183403,
                -0.012379064,
                0.10281219,
                0.008949709,
                -0.0041039507,
                0.07591977,
                0.09370062,
                -0.02668652,
                -0.04892249,
                -0.029867183,
                0.03979596,
                -0.05476532,
                0.051676657,
                -0.042236056,
                -0.048522443,
                -0.025473235,
                -0.025055256,
                -0.03393868,
                0.054841466,
                0.09184276,
                -0.056983713,
                -0.04603919,
                0.09848297,
                -0.019956052,
                -0.052536283,
                -0.06605742,
                5.4155337E-4,
                0.0017049549,
                -0.057911247,
                0.01937376,
                -0.049909536,
                0.03386487,
                -0.005862444,
                -0.06692171,
                -0.025703322,
                -6.5114145E-5,
                0.004126781,
                -0.0043084836,
                0.0067751757,
                0.07197635,
                -0.0025322703,
                -0.022022074,
                0.0783847,
                -0.0335708,
                -0.004212825,
                0.008761962,
                -0.001258802,
                -0.07231878,
                0.09587904,
                -0.056090962,
                0.03251013,
                -0.07442126,
                -0.02653164,
                -0.03064639,
                -4.7030355E-4,
                -0.0016678302,
                0.023706514,
                0.050543044,
                -0.06068748,
                -0.022602,
                0.002412744,
                0.026288778,
                0.0017399582,
                -0.0827529,
                0.008240493,
                0.07821927,
                -0.070574075,
                0.067374535,
                -0.013294012,
                -1.3942047E-4,
                -0.05338184,
                0.016748434,
                2.705665E-4,
                0.014579005,
                2.0948991E-4,
                -0.05706299,
                0.03605697,
                0.014696972,
                0.037742164,
                -0.020939453,
                0.028436694,
                0.035362076,
                0.0018619923,
                -0.0075466856,
                0.014967456,
                -0.032054868,
                0.031270053,
                -0.002186239,
                0.05699926,
                0.016739566,
                0.031157434,
                -0.0010109356,
                -0.04696268,
                0.005870991,
                0.027271578,
                0.028848674,
                0.038611643,
                -0.009547326,
                0.07059283,
                1.8445656E-4,
                -0.089277476,
                -0.0022379213,
                0.04282663,
                0.012358607,
                -0.0016425856,
                0.016536076,
                0.05467483,
                0.043642677,
                0.08581251,
                0.055999473,
                0.12031413,
                -0.041482188,
                0.011157499,
                -0.0242339,
                -0.01367876,
                -0.032231208,
                -3.8520477E-5,
                -0.035425536,
                0.016156442,
                0.04588835,
                0.023154348,
                0.05657452,
                -0.033261564,
                -0.017362894,
                -0.021316752,
                0.11136617,
                -0.02980488,
                -0.06715543,
                0.025796436,
                -0.01935141,
                -0.028323812,
                0.06936444,
                0.03610966,
                -0.029284999,
                0.080496624,
                0.031949017,
                0.09113094,
                0.030555924,
                0.031623937,
                -0.014675703,
                0.037986346,
                -0.02168005,
                0.025968686,
                -0.07465135,
                0.05659841,
                0.018133953,
                0.076631024,
                0.0013326105,
                -0.0041708443,
                0.057540894,
                -0.071719825,
                6.45497E-5,
                -0.03489294,
                -0.045025125,
                -2.5996316E-4,
                -0.046387482,
                -0.055191796,
                -0.056554873,
                -0.057101563,
                0.011048426,
                0.030312032,
                0.020642363,
                -0.025908545,
                0.0020847768,
                -0.009446726,
                -0.017694984,
                0.030607734,
                0.0077685965,
                0.025650209,
                0.0042204,
                -0.043082446,
                -0.032081213,
                -0.031769518,
                -0.0058249063,
                0.048719674,
                -0.0034788852,
                0.09682542,
                0.01283506,
                -0.047615483,
                -0.037369475,
                0.032892384,
                0.05012456,
                0.077855065,
                -0.02178995,
                0.012280451,
                0.14538018,
                0.07249008,
                -0.06332537,
                0.013439314,
                0.023898449,
                -0.011217777,
                0.008283438,
                -0.074870385,
                0.035783168,
                0.004126748,
                0.0062911683,
                -0.0556917,
                0.0056409365,
                -4.090233E-4,
                -0.008330953,
                -0.070801824,
                0.022367505,
                0.013850163,
                0.011464977,
                -0.07630842,
                -0.021319168,
                0.049539562,
                -0.020083012,
                0.08292538,
                -0.003890593,
                -0.06826295,
                0.07833868,
                0.11102353,
                0.048478078,
                0.04874929,
                0.01674752,
                -0.010122156,
                -0.0017015542,
                0.052444953,
                -0.025292192,
                6.438819E-4,
                0.03322604,
                -0.013280386,
                -0.051285855,
                0.044584952,
                0.0028471963,
                -0.033327993,
                -0.0032210338,
                -7.6964665E-5,
                -0.022906937,
                -0.049165934,
                0.068918124,
                0.0036678151,
                0.056815505,
                -0.0107945055,
                -0.030212734,
                -0.02295425,
                0.012817274,
                -0.05890077,
                -0.010713068,
                -0.06934958,
                0.009253514,
                0.004393559,
                -0.014541829,
                0.015927406,
                -0.03768849,
                -0.009447295,
                -0.015030585,
                0.0012733116,
                -0.08658214,
                0.031349394,
                -0.029448317,
                0.021259015,
                -0.08594763,
                -0.10209632,
                -0.012497564,
                -0.059923995,
                0.04270445,
                -0.017855436,
                6.219463E-4,
                0.05256127,
                -0.004552336,
                -0.017230436,
                0.020000972,
                0.040126193,
                0.03495596,
                -0.0052100243,
                0.0043259785,
                -0.07870951,
                0.025534788,
                0.057605546,
                0.010947904,
                0.061197907,
                -0.061371718,
                0.050409377,
                0.01699761,
                -0.052189495,
                0.027720395,
                0.03315917,
                -0.005267677,
                0.052154183,
                -0.049223747,
                -0.050246976,
                -0.0796312,
                0.017008174,
                -0.017603898,
                0.007403597,
                0.03156758,
                0.0044497293,
                0.01723488,
                0.067717455,
                -0.063556455,
                -0.016297605,
                -0.081181884,
                -0.06485796,
                -0.0059245736,
                -0.070768185,
                -0.021648716,
                -0.08410224,
                3.4673224E-4,
                -0.043180965,
                -0.047195412
            ],
            "metaData": {
                "createTime": "2019-12-09 11:50:25",
                "updateTime": "2019-12-09 11:50:25"
            }
        }
    ]
}
</pre>
                    </div>

                    <em>​recogVoice : Compare the inputted sound file to the saved vector value. </em>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/dap/app/recogVoice</li>
                    </ul>
                    <p class="sub_txt">② Request parameters</p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description </th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API  ID. Request from is required for the ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>dbId</td>
                            <td>Selected database ID  (or ‘default’) </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>file</td>
                            <td>Sound file to compare. type:file (.wav) </td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
<pre>
curl -X POST \
  https://api.maum.ai/dap/app/recogVoice \
  -H 'content-type: multipart/form-data; boundary=----
WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -F apiId=(*Request for ID) \
  -F apiKey=(*Request for key) \
  -F dbId=default
  -F 'file=@/sample2.wav' \
</pre>
                    </div>

                    <p class="sub_txt">④ Response example </p>
                    <div class="code_box">
<pre>
{
    "message": {
        "message": "success",
        "status": 0
    },
    "result": {
        "id": "__no__match__",
        "metaData": {
            "createTime": "1970-01-01 00:00:00",
            "updateTime": "1970-01-01 00:00:00"
        }
    }
}
</pre>
                    </div>


                    <em>​deleteVoice : Delete voice ID from the given database </em>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/dap/app/deleteVoice</li>
                    </ul>
                    <p class="sub_txt">② Request parameters</p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API  ID. Request from is required for the ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>dbId</td>
                            <td>Selected database ID (or  ‘default’)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>voiceId</td>
                            <td>Voice ID to delete </td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
<pre>
curl -X POST \
  https://api.maum.ai/dap/app/deleteVoice \
  -H 'content-type: multipart/form-data; boundary=----
WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -F apiId=(*Request for ID) \
  -F apiKey=(*Request for key) \
  -F dbId=default
  -F 'voiceId=sample'
</pre>
                    </div>
                    <p class="sub_txt">④ Response example </p>
                    <div class="code_box">
<pre>
{
   "message": {
       "message": "success",
       "status": 0
   }
}
</pre>
                    </div>

                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--//vrmenu-->
        <!--.vrexample-->
        <div class="demobox" id="vrexample">
            <p><em style="font-weight: 400;">Use Cases</em></p>
            <span class="sub">Find out how AI can be applied to diverse area.</span>
            <!--voice Recog -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>Security Access Control</span>
                            </dt>
                            <dd class="txt">Register your voice and use it for identification. If access control is required, voices can be registered in devices such as kiosk, for convenient access control using voice recognition.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_vr"><span>Voice Recognition</span></li>
                                    <li class="ico_stt"><span>STT</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>Hands-free mobile authentication</span>
                            </dt>
                            <dd class="txt">Use your mobile in a hands-free environment, where facial recognition, fingerprint recognition and other forms of biometric authentication are inconvenient, such as in automobiles.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_vr"><span>Voice Recognition</span></li>
                                    <li class="ico_stt"><span>STT</span></li>
                                    <li class="ico_den"><span>Denoise</span></li>
                                    <li class="ico_vf"><span>Voice Filter</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>Various voice authentication</span>
                            </dt>
                            <dd class="txt">Authenticate yourself using voice recognition in AI speakers, customer center calls, and more. It is used for authentication by voice only without a separate means of authentication.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_vr"><span>Voice Recognition</span></li>
                                    <li class="ico_stt"><span>STT</span></li>
<%--                                    <li class="ico_dia"><span>Diarization</span></li>--%>
                                    <li class="ico_vf"><span>Voice Filter</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!--//voice Recog -->
        </div>
        <!--//.vrexample-->

    </div>
</div>
<!-- //.contents -->

<script>

    var timelineWidth = $('.slider').get(0).offsetWidth;

    var sampleAudio1;
    var sampleAudio2;
    var sampleAudio3;
    var sampleAudio4;

    var level1 = false;
    var level2 = false;
    var level3 = false;

    /* 샘플 오디오 파일 로드 */
    function loadSample(url, audio) {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", url);
        xhr.responseType = "blob";
        xhr.onload = function() {
            blob = xhr.response;

            if(audio === 1) {
                sampleAudio1 = new File([blob], "sampleAudio1.wav");
            } else if(audio === 2) {
                sampleAudio2 = new File([blob], "sampleAudio2.wav");
            } else if(audio === 3) {
                sampleAudio3 = new File([blob], "sampleAudio3.wav");
            } else if(audio === 4){
                sampleAudio4 = new File([blob], "sampleAudio4.wav");
            }
        };

        xhr.send();
    }

    $(document).ready(function () {

        var formData = new FormData();
        var nameArray = [];
        var fileArray = [];

        loadSample("/aiaas/common/audio/voiceRecog/한글_이중건.wav", 1);
        loadSample("/aiaas/common/audio/voiceRecog/클래식_이미수.wav", 2);
        loadSample("/aiaas/common/audio/voiceRecog/장동환_3.wav", 3);
        loadSample("/aiaas/common/audio/voiceRecog/백선자_2.wav", 4);

        /* 샘플 테스트 실행 */
        $('#sampleTest').on('click', function() {
            console.log("Sample test start !! ");

            $('.voiceR_box').hide();
            $('.vr_2').fadeIn(300);
            $("html").scrollTop(0);

            //var formData = new FormData();
            var sampleUrl = $('input[name="option"]:checked').attr('src');
            var blob = null;
            var xhr = new XMLHttpRequest();


            xhr.open("GET", sampleUrl);
            xhr.responseType = "blob";

            /* 사용자가 선택한 sample 파일 업로드 */
            xhr.onload = function() {
                blob = xhr.response;
                var sample = new File([blob], "sample.wav");

                formData.append("sampleAudio1", sampleAudio1);
                formData.append("sampleAudio2", sampleAudio2);
                formData.append("sampleAudio3", sampleAudio3);
                formData.append("sampleAudio4", sampleAudio4);
                formData.append("sample", sample);

                formData.append('${_csrf.parameterName}', '${_csrf.token}');

                var request = new XMLHttpRequest();
                request.enctype = "multipart/form-data";
                request.processData = false;
                request.contentType = false;

                /* 샘플 파일을 통해 voice recognition */
                request.onreadystatechange = function() {
                     if(request.readyState === 4) {
                         if(request.status != 200) {
                             alert("There are currently many server requests. Please try again.");
                             $('.reset_btn ').trigger("click");
                         } else {
                             var name = JSON.parse(request.response)['result']['id']; // voiceId
                             var voiceName = $('input[name="option"]:checked').next('label').children('span').text() ;

                             console.log("name = ", name);
                             console.log(voiceName);

                             var txtarea = document.getElementById("result_txt");

                             /* 일치하는 audio가 없는 경우 */
                             if (name === "__no__match__") {
                                 console.log("__no__match__");
                                 $(".result_t").html('There is no Voice ID matches your file.');
                                 $("#voiceId").html('VoiceID : ');
                                 $("#voice_name").html('No match');
                                 $("#voiceName").text(voiceName);
                                 txtarea.value = JSON.stringify(JSON.parse(request.response)['result']);
                             } else { /* 일치하는 audio가 있을 경우 */
                                 $(".result_t").html('We found a VoiceID which matches your file.');
                                 $("#voiceId").html('VoiceID : ');
                                 $("#voice_name").html(name);
                                 $("#voiceName").text(voiceName);
                                 txtarea.value = "voiceVector : \r\n" + JSON.stringify(JSON.parse(request.response)['result']['voiceVector']).replace(/,/g, ', \r\n');	// ,부분에서 줄바꿈
                             }

                             document.getElementById('result_audio').src = sampleUrl; // 입력된 오디오 파일로 설정
                             $('.loding_box').trigger("click");
                         }
                     }
                };
                request.open('POST', '/api/sampleVoiceRecog');
                request.send(formData);

                /* 값 초기화 */
                formData = new FormData();
                nameArray = [];
                fileArray = [];
            };
            xhr.send();
        });

        function setFormData(demoF, demoN, flag){
            if(($('#'+demoF).val() != "") && (flag == true)) {
                var data = $('#'+demoN).val();
                nameArray.push(data);
                formData.append(demoF, document.getElementById(demoF).files[0]);
            }else{
                var data = "default";
                nameArray.push(data);
                formData.append(demoF, new File([], "default.wav"));
            }
        }
        /* 내 파일로 해보기 */
        $('#demoResult').on('click', function() {

            if( $('#demoFile').val() == "") { //인식할 음성이 업로드되지 않은 경우
                alert("Please select a voice to recognize");
            } else if( (level1 || level2 || level3 ) == false) { //데이터 베이스에 등록할 음성을 선택하지 않은 경우
                alert("Please select a voice to register in the database.");
            } else {
                $('.voiceR_box').hide();
                $('.vr_2').fadeIn(300);
                $("html").scrollTop(0);

                formData.append('demoFile', document.getElementById('demoFile').files[0]);

                setFormData('demoFile1', 'demoName1', level1);
                setFormData('demoFile2', 'demoName2', level2);
                setFormData('demoFile3', 'demoName3', level3);

                formData.append("nameArray", nameArray);
                formData.append('${_csrf.parameterName}', '${_csrf.token}');

                var request = new XMLHttpRequest();

                request.enctype = "multipart/form-data";
                request.processData = false;
                request.contentType = false;
                request.onreadystatechange = function() {
                    if (request.readyState === 4) {
                        if (request.status != 200) {
                            alert("There are currently many server requests. Please try again.");
                            $('.voiceR_box').show();
                            $('.vr_2').hide();
                            $('.reset_btn ').trigger("click");
                        } else {
                            var name = JSON.parse(request.response)['result']['id']; //voiceId
                            var txtarea = document.getElementById("result_txt");
                            /* 인식할 얼굴이 DB에 없는 경우 */
                            if (name === "__no__match__") {
                                $(".result_t").html('There is no Voice ID matches your file.');
                                $("#voiceId").html('');
                                $("#voice_name").html('No match');
                                $("#voiceName").text('No match');
                                txtarea.value = JSON.stringify(JSON.parse(request.response)['result']);
                            } else { /* 인식할 얼굴이 DB에 있는 경우 */
                                $(".result_t").html('We found a VoiceID which matches your file.');
                                $("#voiceId").html('voiceID : ');
                                $("#voice_name").html(name);
                                $("#voiceName").text(name);
                                txtarea.value = "voiceVector : \r\n" + JSON.stringify(JSON.parse(request.response)['result']['voiceVector']).replace(/,/g, ', \r\n');	// ,부분에서 줄바꿈
                            }
                            /* 값 초기화 */
                            formData = new FormData();
                            nameArray = [];
                            fileArray = [];

                            $('.loding_box ').trigger("click");
                        }
                    }
                };
                request.open('POST', '/api/getVoiceRecog');
                request.send(formData);
            }
        });

        // Time update event
        $('.music').on("timeupdate", function(){
            var curIdx = $('.music').index($(this));
            timeUpdate($(this), curIdx);
        });


        // Audio play
        $('.play').on('click', function (me) {
            var curIdx = $('.play').index(this);

            $('.play').each(function (idx,e) {
                if (e !== me.currentTarget) {
                    $('.pause').eq(idx).css("display","none");
                    $('.play').eq(idx).css("display","block");
                    $('.music').eq(idx).trigger("pause");
                }
            });

            $('.music').eq(curIdx).trigger("play");
            $(this).css("display","none");
            $('.pause').eq(curIdx).css("display","block");
        });

        // Audio pause
        $('.pause').on('click', function () {
            var curIdx = $('.pause').index(this);

            $('.music').eq(curIdx).trigger("pause");
            $(this).css("display","none");
            $('.play').eq(curIdx).css("display","block");
        });

        // 데이터 베이스 음성 파일 업로드 후 이벤트
        $(".voiceFile").on('change', function(){
            var thisObj = $(this).get(0);
            var file = thisObj.files[0];
            var fileSize= file.size;
            var maxSize = 1024 * 1024 * 2; //2MB
            if (this.files[0].type.match(/audio\/wav/)  && fileSize < maxSize  ){
                console.log("upload complete !");

                $('input[type="radio"]:checked').prop("checked", false);
                var fileBox = thisObj.parentNode;
                var emClose = fileBox.parentNode.firstElementChild;

                fileBox.style.display = "none";
                emClose.style.display = "inline";

                var id_by_class = $(this).attr('id');
                var selectItem = 'upload_sound' + id_by_class.toString()[8];

                document.getElementById(selectItem).src = URL.createObjectURL(file);
            }
            else{
                thisObj.value = null;
                $('.pop_simple').fadeIn(300);
                $('.pop_close, .pop_bg, .btn a').on('click', function () {
                    $('.pop_simple').fadeOut(300);
                    $('body').css({
                        'overflow': ''
                    });
                });
            }
        });

        // 인식할 음성 파일 업로드 후 이벤트
        document.querySelector("#demoFile").addEventListener('change', function (ev) {
            if(this.files[0] === undefined){
                $('em.close2').trigger('click');
                return;
            }

            var file = this.files[0];
            // var name = file.name;
            var fileSize= file.size;
            var maxSize = 1024 * 1024 * 2; //2MB

            //console.log(fileSize);
            //console.log(maxSize);

            if (this.files[0].type.match(/audio\/wav/)  && fileSize < maxSize  ){
                console.log("upload complete !");
                var emClose = this.parentNode.firstElementChild;
                $('input[type="radio"]:checked').prop("checked", false);
                // $('.demolabel').text(name);
                $('.demolabel').hide();
                $('#uploadFile').removeClass( 'btn' ).addClass( 'btn_change' );
                emClose.style.display = "inline";
                document.getElementById('upload_sound4').src = URL.createObjectURL(file);

                document.getElementById('result_audio').src = URL.createObjectURL(file);
            }
            else{
                this.value = null;

                $('.pop_simple').fadeIn(300);
                $('.pop_close, .pop_bg, .btn a').on('click', function () {
                    $('.pop_simple').fadeOut(300);
                    $('body').css({
                        'overflow': ''
                    });
                });
            }
        });

        // Set audio duration
        $('.music').each(function(){
            $(this).on("canplay", function () {
                var $parent = $(this).parent();
                var dur = this.duration;
                var fl_dur = Math.floor(dur);
                var endTime = toTimeFormat(fl_dur+"");
                $parent.children('.timer_fr').text(endTime);
                // if (fl_dur <= 9) {
                // 	$parent.children('.timer_fr').text("0:0" + fl_dur);
                // } else {
                // 	$parent.children('.timer_fr').text("0:" + fl_dur);
                // }
            });
        });

        // 데이터 베이스 음성 파일 닫기 버튼
        $('em.close').on('click', function () {
            var closeIdx = $(this).attr('id').substring(5);
            var uploadfile = $('#uploadFile'+closeIdx);
            var audio = $('#upload_sound'+closeIdx);
            //console.log(closeIdx);

            if(closeIdx == 1) {
                level1 = false;
            } else if (closeIdx == 2) {
                level2 = false;
            } else if (closeIdx == 3) {
                level3 = false;
            }

            $(this).hide();
            $('#levelText'+closeIdx).hide();
            $('#levelInput'+closeIdx).fadeIn().children('input').val('');

            uploadfile.fadeIn();
            uploadfile.children('label').text('File Upload');
            uploadfile.children('input').val('');

            audio.next('.slider').children('.elapsed').css('width','0'); //업로드 오디오 플레이어 재생바 초기화
            audio.attr('src', '');
            audioEnded();
        });

        // 인식할 음성 파일 닫기 버튼
        $('em.close2').on('click', function () {
            var uploadFile = $("#uploadFile");
            $(this).hide();
            uploadFile.removeClass( 'btn_change' ).addClass( 'btn' );
            uploadFile.children('label').fadeIn();
            uploadFile.children('label').text('File Upload');
            uploadFile.children('input').val('');

            $('#pause').trigger('click');
            $('#elapsed').width('0px');
            $("#upload_sound4").attr('src', '');

            audioEnded();
        });

        /* 사용자가 저장하기 버튼을 통해 오디오를 저장했을 경우  */
        $('#save1').on('click', function(){
            var text = $("#demoName1").val();
            var emClose = $(this).parent().parent().parent().children('em');

            if($('#demoName1').val() == "" || ($('#demoFile1').val() == "")){
                alert("After voice upload, please input voice ID.");
            }else{
                $("#levelInput1").css('display','none');
                $("#levelText1").css('display','block');
                emClose.fadeIn();
                $("#levelText1 span").text(text);
                level1 = true;
            }
        });
        $('#save2').on('click', function(){
            var text = $("#demoName2").val();
            var emClose = $(this).parent().parent().parent().children('em');

            if($('#demoName2').val() == "" || ($('#demoFile2').val() == "")){
                alert("After voice upload, please input voice ID.");
            }else{
                $("#levelInput2").css('display','none');
                $("#levelText2").css('display','block');
                emClose.fadeIn();
                $("#levelText2 span").text(text);
                level2 = true;
            }
        });
        $('#save3').on('click', function(){
            var text = $("#demoName3").val();
            var emClose = $(this).parent().parent().parent().children('em');

            if ($('#demoName3').val() == "" || ($('#demoFile3').val() == "")){
                alert("After voice upload, please input voice ID.");
            }else{
                $("#levelInput3").css('display','none');
                $("#levelText3").css('display','block');
                emClose.fadeIn();
                $("#levelText3 span").text(text);
                level3 = true;
            }
        });

        $('.hidden').on('click', function(){
            var id = this.id;
            if((id == "level1") || (id == "level2") || (id == "level3")){
                $("#"+id+"_back").css('display','block');
                $("#"+id+"_text").css('display','none');

                if(id == "level1") {
                    level1 = false;
                }else if(id == "level2") {
                    level2 = false;
                }else if(id == "level3") {
                    level3 = false;
                }
            }
        });

        // step1->step2
/*
        $('.btn_start').on('click', function () {

            if( ($('#demoFile').val() != "") && ((level1 || level2 || level3) == true) ) {
                alert("ewwrwerew");
            }

            //$('.voiceR_box').hide();
            //$('.vr_2').fadeIn(300);
           // $("html").scrollTop(0);
        });
*/

        // step2->step3
        $('.loding_box ').on('click', function () {
            $('.vr_2').hide();
            $('.vr_3').fadeIn(300);
        });

        // step3->step1
        $('.reset_btn').on('click', function () {
            $('.vr_3').hide();
            $('.voiceR_box').fadeIn(300);

            var label_change = $('.demolabel');
            label_change.text('File Upload');
            //label_change.parent().removeClass('btn_change');
            label_change.parent().addClass('btn')
            //$('.demoFile').val('');

            //$('.input_level1').css('display','block');
            //$('.input_level2').css('display','none');
            //$('.input_level1 input').val('')

            //$('.em.close').trigger();
        });
    });

    //------------------ Functions --------------------------

    // Update current play time and player bar
    //      obj : current .music jquery object
    //      idx : currnet .music index (= audio player index)
    function timeUpdate(obj, idx) {
        var cur_music = obj.get(0);
        var playHead = $('.elapsed').eq(idx).get(0);
        var timer = $('.timer').eq(idx).get(0);

        var playPercent = timelineWidth * (cur_music.currentTime / cur_music.duration);
        playHead.style.width = playPercent + "px";

        var secondsIn = Math.floor(cur_music.currentTime);
        var curTime = toTimeFormat(secondsIn+"");
        timer.innerHTML = curTime;
    }

    function audioEnded(audio){
        var idx = $('.music').index(audio);
        // audio UI setting
        $('.pause').eq(idx).trigger('click');
        $('.music').eq(idx).get(0).currentTime = 0;
        $('.elapsed').eq(idx).css("width", "0px");
        $('.timer').eq(idx).text("0:00");
    }

    function toTimeFormat (text) {
        var sec_num = parseInt(text, 10); // don't forget the second param
        var hours   = Math.floor(sec_num / 3600);
        var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
        var seconds = sec_num - (hours * 3600) - (minutes * 60);

        // if (hours   < 10) {hours   = "0"+hours;}
        if (minutes < 10) {minutes = minutes;}
        if (seconds < 10) {seconds = "0"+seconds;}

        return minutes+':'+seconds;
    }

    //API 탭
function openTap(evt, menu) {
    var i, demobox, tablinks;
    demobox = document.getElementsByClassName("demobox");
    for (i = 0; i < demobox.length; i++) {
        demobox[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(menu).style.display = "block";
    evt.currentTarget.className += " active";
}
document.getElementById("defaultOpen").click();


</script>