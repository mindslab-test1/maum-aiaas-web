﻿<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
	<div class="content api_content">
		<h1  class="api_tit">ITF</h1>
		<ul class="menu_lst">
			<li class="tablinks" onclick="openTap(event, 'itfdemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
			<li class="tablinks" onclick="openTap(event, 'itfexample')"><button type="button">Use Case</button></li>
			<li class="tablinks" onclick="openTap(event, 'itfmenu')"><button type="button">Manual</button></li>
<%--			<li class="tablinks"><a href="/member/enApiAccount" >API ID, key</a></li>--%>
		</ul>
		<!--.itfdemo-->
		<div class="demobox demobox_nlu" id="itfdemo">
			<p><span>Intent Finder</span></p>
			<span class="sub">Find the intent of bank customers. Below is an sample of Intent Finder.<br>
Intent Finder can be applied in anywhere. Check out how we can improve your business.</span>
            <p class="temp_txt">The service is currently available only in Korean. Other languages will be available soon.</p>
			<!--.itf_box-->
			<div class="itf_box">
				<!--.step01-->
				<div class="step01">
					<div class="demo_top">
						<em class="far fa-list-alt"></em><span>Sample</span>
					</div>
					<div class="ift_topbox">
						<p>Select an sample question of bank customers</p>
						<ul class="ift_lst">
							<li>
								<div class="radio">
									<input type="radio" id="radio" name="radio_article" value="최근 이체했던 계좌 목록" checked="checked">
									<label for="radio">최근 이체했던 계좌 목록</label>
								</div>
							</li>
							<li>
								<div class="radio">
									<input type="radio" id="radio2" name="radio_article" value="지로요금 납부할래">
									<label for="radio2">지로요금 납부할래</label>
								</div>
							</li>
							<li>
								<div class="radio">
									<input type="radio" id="radio3" name="radio_article" value="환전하고 싶어">
									<label for="radio3">환전하고 싶어</label>
								</div>
							</li>
						</ul>
					</div>
					<p class="txt_or">or</p>
					<div class="demo_top">
						<em class="far fa-keyboard"></em><span>Input Text</span>
					</div>
					<div class="text_area">
						<textarea rows="10" placeholder="Input the question for Intent Finder (Korean only)"></textarea>
					</div>

					<div class="btn_area">
						<button type="button" class="btn_search" id="find_btn"><em class="fas fa-search"></em>Process</button>
					</div>
				</div>
				<!--//.step01-->

				<!--.step02-->
				<div class="step02">
					<div class="demo_top">
						<em class="far fa-list-alt"></em><span>Result</span>
					</div>
					<div class="ift_topbox">
						<p>Intent (Skill)</p>
						<div class="ift_result">
							<em class="fas fa-chevron-right"></em>
							<p>보험상품 문의</p>
						</div>
					</div>
					<div class="demo_top">
						<em class="fas fa-code"></em><span>Process</span>
					</div>
					<div class="text_area">
						<textarea id="result_textArea"  rows="7" placeholder=" "></textarea>
					</div>

					<div class="btn_area">
						<button type="button" class="btn_reset" id="btn_reset"><em class="fas fa-redo-alt"></em>Reset</button>
					</div>
				</div>
				<!--//.step02-->
			</div>
			<!--//.itf_box-->

		</div>
		<!-- .itfdemo -->

		<!--.itfmenu-->
		<div class="demobox" id="itfmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API Guideline
					</div>
					<p class="sub_title">Set up Environment</p>
					<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
					<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
					<p class="sub_title">ID &amp; Key</p>
					<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
					<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
					<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
						<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
					<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
					<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
					<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
				</div>
				<div class="guide_group">
					<div class="title">
						ITF <small>(Intend Finder)</small>
					</div>
					<p class="sub_txt">An engine that understands and categorizes users' intent, allowing chatbots to find the most appropriate answers and answer them.</p>

					<span class="sub_title">
							Preparation
						</span>
					<p class="sub_txt">① Input: Sentence or question (text)</p>
					<p class="sub_txt">② Model: Choose one below</p>
					<ul>
						<li>Financial Service Consultation (Only Korean)</li>
					</ul>
					<span class="sub_title">
							 API Document
						</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/itf/</li>
					</ul>
					<p class="sub_txt">② Request Parameters </p>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId</td>
							<td>Unique API  ID. Request from is required for the ID.</td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>Unique API key. Request from is required for the key. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>utter</td>
							<td>Sentence/Question to analyze (text) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>lang</td>
							<td>Selected language (ko_KR)</td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request example</p>
					<div class="code_box">
							<pre>
curl -X POST \
  http://api.maum.ai/api/itf \
  -H 'Content-Type: application/json' \
  -d '{
	"apiId": "(*ID 요청필요)",
	"apiKey": "(*key 요청필요)"",
	"utter": "통장 잔액이 얼마야",
	"lang": "ko_KR"
}'
</pre>
					</div>

					<p class="sub_txt">④ Response example </p>

					<div class="code_box">
<pre>
{
    "testFindIntentCase_": 1,
    "testFindIntent_": {
        "bitField0_": 0,
        "skill_": "잔액조회",
        "probability_": 1.0,
        "isDefaultSkill_": false,
        "history_": {
            "runResults_": [
                {
                    "type_": 11,
                    "name_": "itf_cca_sc",
                    "hasResult_": false,
                    "isHint_": false,
                    "skill_": "",
                    "hint_": "",
                    "probability_": 0.0,
                    "detail_": "",
                    "memoizedIsInitialized": -1,
                    "unknownFields": {
                        "fields": {}
                    },
                    "memoizedSize": -1,
                    "memoizedHashCode": 0
                },
                {
                    "type_": 12,
                    "name_": "itf_eca_hmd",
                    "hasResult_": true,
                    "isHint_": false,
                    "skill_": "잔액조회",
                    "hint_": "",
                    "probability_": 1.0,
                    "detail_": "%잔액$!계좌 별$!통장 별$!전체 계좌$!전 계좌$!계좌 전체$!계좌 전부$!전체 통장$!통장 전체$!통장 전부$!모든 계좌$!적금$!펀드$!대출$!예금$!증권$!이체$!외화",
                    "memoizedIsInitialized": -1,
                    "unknownFields": {
                        "fields": {}
                    },
                    "memoizedSize": -1,
                    "memoizedHashCode": 0
                }
            ],
            "memoizedIsInitialized": -1,
            "unknownFields": {
                "fields": {}
            },
            "memoizedSize": -1,
            "memoizedHashCode": 0
        },
        "nluResult_": {
            "bitField0_": 0,
            "category_": "",
            "categoryWeight_": 0.0,
            "lang_": 0,
            "sentences_": [
                {
                    "bitField0_": 0,
                    "seq_": 0,
                    "text_": "통장 잔액이 얼마야",
                    "words_": [
                        {
                            "seq_": 0,
                            "text_": "통장",
                            "taggedText_": "",
                            "type_": "",
                            "begin_": 0,
                            "end_": 0,
                            "beginSid_": 0,
                            "endSid_": 0,
                            "position_": 0,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        },
                        {
                            "seq_": 1,
                            "text_": "잔액이",
                            "taggedText_": "",
                            "type_": "",
                            "begin_": 1,
                            "end_": 2,
                            "beginSid_": 0,
                            "endSid_": 0,
                            "position_": 0,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        },
                        {
                            "seq_": 2,
                            "text_": "얼마야",
                            "taggedText_": "",
                            "type_": "",
                            "begin_": 3,
                            "end_": 3,
                            "beginSid_": 0,
                            "endSid_": 0,
                            "position_": 0,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        }
                    ],
                    "morps_": [
                        {
                            "seq_": 0,
                            "lemma_": "통장",
                            "type_": "NNG",
                            "position_": 0,
                            "wid_": 0,
                            "weight_": 0.9,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        },
                        {
                            "seq_": 1,
                            "lemma_": "잔액",
                            "type_": "NNG",
                            "position_": 7,
                            "wid_": 0,
                            "weight_": 0.9,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        },
                        {
                            "seq_": 2,
                            "lemma_": "이",
                            "type_": "JKS",
                            "position_": 13,
                            "wid_": 0,
                            "weight_": 0.03607228016640132,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        },
                        {
                            "seq_": 3,
                            "lemma_": "얼마야",
                            "type_": "NNG",
                            "position_": 17,
                            "wid_": 0,
                            "weight_": 0.0,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        }
                    ],
                    "morphEvals_": [
                        {
                            "seq_": 0,
                            "target_": "통장",
                            "result_": "통장/NNG",
                            "wordId_": 0,
                            "mBegin_": 0,
                            "mEnd_": 0,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        },
                        {
                            "seq_": 1,
                            "target_": "잔액이",
                            "result_": "잔액/NNG+이/JKS",
                            "wordId_": 1,
                            "mBegin_": 1,
                            "mEnd_": 2,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        },
                        {
                            "seq_": 2,
                            "target_": "얼마야",
                            "result_": "얼마야/NNG",
                            "wordId_": 2,
                            "mBegin_": 3,
                            "mEnd_": 3,
                            "memoizedIsInitialized": -1,
                            "unknownFields": {
                                "fields": {}
                            },
                            "memoizedSize": -1,
                            "memoizedHashCode": 0
                        }
                    ],
                    "nes_": [],
                    "wsds_": [],
                    "chunks_": [],
                    "dependencyParsers_": [],
                    "phraseClauses_": [],
                    "srls_": [],
                    "srlInfos_": [],
                    "relations_": [],
                    "sas_": [],
                    "zas_": [],
                    "memoizedIsInitialized": -1,
                    "unknownFields": {
                        "fields": {}
                    },
                    "memoizedSize": -1,
                    "memoizedHashCode": 0
                }
            ],
            "keywordFrequencies_": [],
            "memoizedIsInitialized": -1,
            "unknownFields": {
                "fields": {}
            },
            "memoizedSize": -1,
            "memoizedHashCode": 0
        },
        "memoizedIsInitialized": -1,
        "unknownFields": {
            "fields": {}
        },
        "memoizedSize": -1,
        "memoizedHashCode": 0
    },
    "replace_": {
        "bitField0_": 0,
        "replacedUtter_": "",
        "memoizedIsInitialized": -1,
        "unknownFields": {
            "fields": {}
        },
        "memoizedSize": -1,
        "memoizedHashCode": 0
    },
    "memoizedIsInitialized": 1,
    "unknownFields": {
        "fields": {}
    },
    "memoizedSize": -1,
    "memoizedHashCode": 0
}

</pre>

					</div>
				</div>
			</div>

		</div>
		<!--//itfmenu-->
		<!--.itfexample-->
		<div class="demobox" id="itfexample">
			<p><em style="font-weight: 400;color:#27c1c1;">Use Cases</em>  </p>
			<span class="sub">Find out how AI can be applied to diverse area.</span>
			<div class="useCasesBox">
				<ul class="lst_useCases">

					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>Text analysis</span>
							</dt>
							<dd class="txt">Analyze unstructured documents and text in a variety of fields.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_if"><span>ITF</span></li>
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
									<li class="ico_xdc"><span>XDC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>Automatic classification</span>
							</dt>
							<dd class="txt">Automatically classify and analyze text by category.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_if"><span>ITF</span></li>
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
									<li class="ico_xdc"><span>XDC</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //itf -->
		</div>
		<!--//.itfexample-->


	</div>
</div>
<!-- //.contents -->

<script type="text/javascript">
	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){
			var $textArea = $('.itf_box .step01 textarea');

			//===== radioBox event =====
			$('input[type=radio]').on('click', function(){
				$textArea.text("");
				$textArea.val("");
			});

			//===== textArea event =====
			$textArea.on('change paste input keyup', function(){
				$('input[type=radio]:checked').prop("checked", false);
			});

			$textArea.keypress(function(e){
				if(e.keyCode === 13){
					$('#find_btn').trigger('click');
				}
			});

			//===== 의도 찾기 =====
			$('#find_btn').on('click',function(){
				var $checkedRadio = $('input[type=radio]:checked');
				var utter = "";
				var lang = "";

				if( $textArea.val().trim() === ""){
					if($checkedRadio.length === 0){
						alert("Select sample or input text.");
					    return;
					}
                    utter = $checkedRadio.val();
				}else{
					utter = $textArea.val();
				}

				lang = "ko";
				console.log("utter: "+utter + " lang: "+lang);

				var param = {
					'utter' : utter,
					'lang' : lang,
					'${_csrf.parameterName}' : '${_csrf.token}'
				};

				$.ajax({
					url: '/api/itf/',
					async: false,
					type: 'POST',
					headers: {
						"Content-Type": "application/x-www-form-urlencoded"
					},
					data: param,
					error: function(request, status, error){
						if (request.statusText === ""){
							alert("Error");
						}else if (request.statusText === "INTERNAL SERVER ERROR"){
							alert("Error");
						}else{
							alert(request.statusText);
						}
					},
					success: function(data){
						var responseData = JSON.parse(data);
						var responseString = JSON.stringify(responseData,undefined,8);
						var textAreaObj = document.getElementById('result_textArea');
						var skill_ = "";

						textAreaObj.scrollTop = textAreaObj.offsetHeight- 300;
						textAreaObj.innerHTML = responseString;
						skill_ = responseData.testFindIntent_.skill_;

						if(skill_ === "9999"){
							skill_ = "Unknown result";
						}
						$('.ift_result p').text(skill_);

						$('.itf_box .step01').hide();
						$('.itf_box .step02').fadeIn();
					}
				});
			});


			$('#btn_reset').on('click',function(){
				var textAreaObj = document.getElementById('result_textArea');
				textAreaObj.scrollTop = textAreaObj.offsetHeight - 300;
				document.getElementById('result_textArea').innerHTML = "";

				$('.itf_box .step02').hide();
				$('.itf_box .step01').fadeIn();
			});

		});

	});

	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();



</script>
