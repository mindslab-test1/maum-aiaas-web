<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">

<!-- 5 .pop_simple -->
<div class="pop_simple" id="api_upload_fail_popup">
	<div class="pop_bg"></div>
	<!-- .popWrap -->
	<div class="popWrap pop_sr_noti">
		<button class="pop_close" type="button">Close</button>
		<!-- .pop_bd -->
		<div class="pop_bd">
			<em class="fas fa-sad-cry"></em>
			<h5>File upload failed</h5>
			<p>Check the requirement below</p>
			<span>
				* .wav file only<br>
				* channels: mono<br>
				* Under 5MB file size<br>
				* Length should be less than 30 minutes
			</span>

		</div>
		<!-- //.pop_bd -->
		<div class="btn">
			<a class="#">OK</a>
		</div>
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- 5 .pop_simple -->
<div class="pop_simple " id="api_fail_popup">
	<div class="pop_bg"></div>
	<!-- .popWrap -->
	<div class="popWrap" style="position: inherit">
		<button class="pop_close" type="button">Close</button>
		<!-- .pop_bd -->
		<div class="pop_bd">
			<em class="fas fa-exclamation-triangle"></em>
			<p>Timeout error</p>
			<p style="font-size: small"><strong>Please check the uploaded file.</strong></p>
			<p style="font-size: small">* .wav file only<br>
				* channels: mono<br>
				* Under 5MB file size<br>
				* Length should be less than 30 minutes</p>

		</div>
		<!-- //.pop_bd -->
		<div class="btn" id="close_api_fail_popup">
			<a class="">OK</a>
		</div>
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_simple -->


<!-- .contents -->
<div class="contents">
	<!-- .content -->
	<div class="content api_content">
		<h1 class="api_tit">Denoise</h1>
		<ul class="menu_lst voice_menulst">
			<li class="tablinks" onclick="openTap(event, 'dndemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
			<li class="tablinks" onclick="openTap(event, 'dnexample')"><button type="button">Use Case</button></li>
			<li class="tablinks" onclick="openTap(event, 'dnmenu')"><button type="button">Manual</button></li>
<%--			<li class="tablinks"><a href="/member/enApiAccount">API ID, key</a></li>--%>
		</ul>
		<!-- .demobox -->
		<div class="demobox" id="dndemo">
			<p>Enhance sound quality using <span>Denoise</span></p>
			<span class="sub">Remove noise, echo, background music, harsh ess and plosives.</span>

			<!--dn_box-->
			<div class="demo_layout dn_box">
				<!--dn_1-->
				<div class="dn_1" >
					<div class="fl_box">
						<p><em class="far fa-file-audio"></em><strong>Use Sample File</strong></p>
						<div class="sample_box">
							<div class="sample first_sample">
								<div class="radio">
									<input type="radio" id="sample1" name="option" value="Sample1" checked>
									<label for="sample1" class="">Sample 1</label>
								</div>
								<!--player-->
								<div class="player">
									<div class="button-items">
										<audio id="music" class="music" preload="auto" onended="audioEnded($(this))">
											<source src="${pageContext.request.contextPath}/aiaas/en/audio/denoise/msl-seo.wav" type="audio/wav">
											<p>Alas, your browser doesn't support html5 audio.</p>
										</audio>
										<div id="slider" class="slider">
											<div id="elapsed" class="elapsed"></div>
										</div>
										<p id="timer" class="timer">0:00</p>
										<p class="timer_fr">0:00</p>
										<div class="controls">
											<div id="play" class="play sample_play">
											</div>
											<div id="pause" class="pause">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="sample">
								<div class="radio">
									<input type="radio" id="sample2" name="option" value="Sample2">
									<label for="sample2" class="">Sample 2</label>
								</div>
								<!--player-->
								<div class="player">
									<div class="button-items">
										<audio id="music2" class="music" preload="auto" onended="audioEnded($(this))">
											<source src="${pageContext.request.contextPath}/aiaas/en/audio/denoise/trump-cut.wav" type="audio/wav">
											<p>Alas, your browser doesn't support html5 audio.</p>
										</audio>
										<div class="slider">
											<div class="elapsed"></div>
										</div>
										<p class="timer">0:00</p>
										<p class="timer_fr">0:00</p>
										<div class="controls">
											<div class="play sample_play" >
											</div>
											<div class="pause" >
											</div>
										</div>
									</div>
								</div>
								<!--player-->
							</div>
						</div>
					</div>
					<div class="fr_box">
						<p><em class="far fa-file-audio"></em><strong>Use My File</strong></p>
						<div class="uplode_box">
							<div class="btn" id="uploadFile">
								<em class="fas fa-times hidden close"></em>
								<em class="far fa-file-image hidden"></em>
								<label for="demoFile" class="demolabel">Upload .wav file</label>
								<input type="file" id="demoFile" class="demoFile" accept=".wav">
							</div>
							<ul>
								<li>* .wav file only</li>
								<li>* channels: mono</li>
								<li>* Under 5MB file size</li>
								<li>* Length should be less than 30 minutes</li>
<%--								<li>* No spaces or special characters in the file name is allowed.</li>--%>
							</ul>
						</div>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_start" id="change_txt"><span>Process</span></button>
					</div>

				</div>
				<!--dn_1-->
				<!--dn_2-->
				<div class="dn_2">
					<p><em class="far fa-file-audio"></em>Processing</p>
					<div class="loding_box ">
						<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#c9c3ec" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#7e71d1" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

						<p>AI processing takes a while…</p>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
					</div>

				</div>
				<!--dn_2-->
				<!--dn_3-->
				<div class="dn_3">
					<div class="origin_file">
						<p><em class="far fa-file-audio"></em> Input</p>
						<div class="origin">
							<!--player-->
							<div class="player">
								<div class="button-items">
									<audio id="music3" class="music" preload="auto" onended="audioEnded($(this))">
										<source src="${pageContext.request.contextPath}/aiaas/kr/audio/denoise.wav" type="audio/wav">
										<p>Alas, your browser doesn't support html5 audio.</p>
									</audio>
									<div class="slider">
										<div class="elapsed"></div>
									</div>
									<p class="timer">0:00</p>
									<p class="timer_fr">0:00</p>
									<div class="controls">
										<div class="play" >
										</div>
										<div class="pause" >
										</div>
									</div>
								</div>
							</div>
							<!--player-->
						</div>
					</div>
					<div class="result_file" >
						<p><em class="far fa-file-audio"></em> Result </p>
						<div class="result">
							<!--player-->
							<div class="player">
								<div class="button-items">
									<audio id="music4" class="music" preload="auto" onended="audioEnded($(this))">
										<source src="${pageContext.request.contextPath}/aiaas/kr/audio/denoise.wav" type="audio/wav">
										<p>Alas, your browser doesn't support html5 audio.</p>
									</audio>
									<div class="slider">
										<div class="elapsed"></div>
									</div>
									<p class="timer">0:00</p>
									<p class="timer_fr">0:00</p>
									<div class="controls">
										<div class="play" >
										</div>
										<div class="pause" >
										</div>
									</div>
								</div>
							</div>
							<!--player-->
							<a id="btn_dwn" class="btn_dwn"><em class="far fa-arrow-alt-circle-down"></em> Download Result</a>
						</div>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_back2"><em class="fas fa-redo"></em>Reset</button>
					</div>

				</div>
				<!--dn_3-->
			</div>
			<!--// dn_box-->
			<span class="remark">* The performance of the output would be poor if the background music contains audible lyrics.</span>
		</div>


		<!--.dnmenu-->
		<div class="demobox voice_menu" id="dnmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API Guideline
					</div>
					<p class="sub_title">Set up Environment</p>
					<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
					<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
					<p class="sub_title">ID &amp; Key</p>
					<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
					<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
					<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
						<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
					<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
					<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
					<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
				</div>
				<div class="guide_group">
					<div class="title">
						Sound denoise
					</div>
					<p class="sub_txt">Mines Lab's denoise API improves the quality of the speaker voice by removing all kind of noises including background music and echo voices.</p>

					<span class="sub_title">
						 Preparation
					</span>
					<p class="sub_txt">Input: Sound file with noise</p>
					<ul>
						<li>File type: .wav</li>
						<li>Channels: mono</li>
						<li>Length: Less than 30 minutes</li>
					</ul>
					<span class="sub_title">
						 API Document
					</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/denoise/stream</li>
					</ul>
					<p class="sub_txt">② Request parameters </p>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId </td>
							<td>Unique API ID. Request from is required for the ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>Unique API key. Request from is required for the key.</td>
							<td>string</td>
						</tr>
						<tr>
							<td>file</td>
							<td>Input sound file (wav) </td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request example </p>
					<div class="code_box">
<pre>
curl -X POST \
  'https://api.maum.ai/denoise/stream \
  -H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \
  -F apiId=(*Request for ID) \
  -F apiKey=(*Request for key) \
  -F 'file=@sample.wav'
</pre>
					</div>

					<p class="sub_txt">④ Response example </p>

					<div class="code_box">
						<audio src="${pageContext.request.contextPath}/aiaas/kr/audio/denoise.wav" preload="auto" controls></audio>
					</div>
				</div>
			</div>
			<!--//.guide_box-->
		</div>
		<!--.dnmenu-->
		<!--.dnexample-->
		<div class="demobox" id="dnexample">
			<p><em style="font-weight: 400;">Use Cases</em>  </p>
			<span class="sub">Find out how AI can be applied to diverse area.</span>
			<!-- //화폐, 고지서 인식(DIARL) -->
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>Voice file archive</span>
							</dt>
							<dd class="txt">You can keep only the clear sounds from any existing poor quality voice file.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_den"><span>Denoise</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>Improvement of <strong>STT Performance</strong></span>
							</dt>
							<dd class="txt">In order to improve the recognition rate of various speech recognition service devices, voice can be refined before actual service.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_den"><span>Denoise</span></li>
									<li class="ico_stt"><span>STT</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //음성정제(Denoise) -->
		</div>
		<!--//.dnexample-->


		<!-- //.content -->
	</div>
	<!-- //.contents -->
</div>

<script type="text/javascript">

	var timelineWidth = $('#slider').get(0).offsetWidth;
	var sample1File;
	var sample2File;
	var sample1SrcUrl;
	var sample2SrcUrl;


	// Set audio duration
	$('.music').each(function(){
		$(this).on("canplay", function () {
			var $parent = $(this).parent();
			var dur = this.duration;
			var fl_dur = Math.floor(dur);
			var endTime = toTimeFormat(fl_dur+"");
			$parent.children('.timer_fr').text(endTime);
		});
	});

	$(document).ready(function () {
		loadSample1();
		loadSample2();


		// Time update event
		$('.music').on("timeupdate", function(){
			var curIdx = $('.music').index($(this));
			timeUpdate($(this), curIdx);
		});

		// Audio play
		$('.play').on('click', function (me) {
			var curIdx = $('.play').index(this);

			$('.play').each(function (idx,e) {
				if (e !== me.currentTarget) {
					$('.pause').eq(idx).css("display","none");
					$('.play').eq(idx).css("display","block");
					$('.music').eq(idx).trigger("pause");
				}
			});

			$('.music').eq(curIdx).trigger("play");
			$(this).css("display","none");
			$('.pause').eq(curIdx).css("display","block");
		});

		// Audio pause
		$('.pause').on('click', function () {
			var curIdx = $('.pause').index(this);

			$('.music').eq(curIdx).trigger("pause");
			$(this).css("display","none");
			$('.play').eq(curIdx).css("display","block");
		});


		$('.sample .sample_play').on('click', function(){
			var sampleNum = $('.sample .sample_play').index(this);
			$('input[type="radio"]:eq('+sampleNum+')').trigger('click');
		});

		$('input[type="radio"]').on('click', function () {
			$('em.close').trigger('click');
		});

		$('.fl_box').on('click', function(){
			if($('.fl_box').css("opacity") === "0.5"){
				$('.fl_box').css("opacity", "1");
			}
		});


		$('.pop_close, .pop_bg, .btn a').on('click', function () {
			$('.pop_simple').fadeOut(300);
			$('body').css({
				'overflow': '',
			});
		});


		// 파일 업로드 후 이벤트
		document.querySelector("#demoFile").addEventListener('change', function (ev) {

			let file = this.files[0];

			if (file === undefined || file === null || file == "") { return; }

			let name = file.name;
			let fileSize = file.size;
			let maxSize = 1024 * 1024 * 5; //5MB

			if (!file.type.match(/audio\/wav/)){
				console.log("not .wav file");
				this.value = null;
				$('#api_upload_fail_popup').fadeIn(300);

			} else if(fileSize > maxSize) {
				console.log("Upload file under 5MB size");
				this.value = null;
				$('#api_upload_fail_popup').fadeIn(300);

			} else{

				let audio = document.createElement('audio');
				audio.preload = 'metadata';
				audio.onloadedmetadata = function() {
					window.URL.revokeObjectURL(audio.src);
					let dur =  audio.duration;
					console.log (dur);

					if (dur > 1800) { // 30분 제한
						console.log("You cannot upload files longer than 30 minutes.");
						$("#demoFile").val(null);
						$('#api_upload_fail_popup').fadeIn(300);

					} else {

						let size = (fileSize / 1048576).toFixed(3); //size in mb
						$('input[type="radio"]:checked').prop("checked", false);
						$('.demolabel').text(name + ' (' + size + 'MB)');
						$('#uploadFile').removeClass('btn').addClass('btn_change');
						$('.fl_box').css("opacity", "0.5");
					}
				};
				audio.src = URL.createObjectURL(file);
				const delete1 = audio.delete;
			}
		});


		// Remove uploaded file
		$('em.close').on('click', function () {
			$('#demoFile').val(null);
			$('.demolabel').text('Upload .wav file');
			$(this).parent().removeClass("btn_change");
			$(this).parent().addClass("btn");
			$('.fl_box').css('opacity', '1');
		});


		$('#change_txt, .btn_back2, .demolabel, .tablinks').on('click', function(){
			$('.music').each(function(){
				$('.pause').trigger('click');
			});
		});

		$('#api_fail_popup .pop_close, #api_fail_popup .btn').on('click', function () {
			$('.btn_back2').click();
		});

		// 음성 정제하기
		$('#change_txt').on('click',function (){
			var $checked = $('input[type="radio"]:checked');
			var $demoFile = $("#demoFile");
			var inputFile;
			var url;

			//내 파일로
			if ( $checked.length === 0 ){
				if ( $demoFile.val() === "" || $demoFile.val() === null){
					alert("Select a sample or upload a file.");
					return 0;
				}
				inputFile = $demoFile.get(0).files[0];
				url = URL.createObjectURL(inputFile);
			}
			//샘플로
			else {
				if( $checked.val() === "Sample1"){
					url = sample1SrcUrl;
					inputFile = sample1File;
				}else if( $checked.val() === "Sample2"){
					url = sample2SrcUrl;
					inputFile = sample2File;
				}
			}

			$('#music3').attr('src',url);

			$('.dn_1').hide();
			$('.dn_2').fadeIn(300);

			startRequest(inputFile);

		});


		// Result -> 처음으로
		$('.btn_back2').on('click', function () {
			$('.dn_2').hide();
			$('.dn_3').hide();
			$('.dn_1').fadeIn(300);

			// audio UI setting
			$('.music').each(function(){ this.currentTime = 0; });
			$('.elapsed').css("width", "0px");
			$('.timer').text("0:00");
		});
	});



	//------------------ Functions --------------------------

	// Update current play time and player bar
	//      obj : current .music jquery object
	//      idx : currnet .music index (= audio player index)
	function timeUpdate(obj, idx) {
		var cur_music = obj.get(0);
		var playHead = $('.elapsed').eq(idx).get(0);
		var timer = $('.timer').eq(idx).get(0);

		var playPercent = timelineWidth * (cur_music.currentTime / cur_music.duration);
		playHead.style.width = playPercent + "px";

		var secondsIn = Math.floor(cur_music.currentTime);
		var curTime = toTimeFormat(secondsIn+"");
		timer.innerHTML = curTime;
	}


	function audioEnded(audio){
		var idx = $('.music').index(audio);
		// audio UI setting
		$('.pause').eq(idx).trigger('click');
		$('.music').eq(idx).get(0).currentTime = 0;
		$('.elapsed').eq(idx).css("width", "0px");
		$('.timer').eq(idx).text("0:00");
	}

	function loadSample1() {
		var blob = null;
		var xhr = new XMLHttpRequest();
		xhr.open("GET", "/aiaas/en/audio/denoise/msl-seo.wav");
		xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
		xhr.onload = function()
		{
			blob = xhr.response;//xhr.response is now a blob object
			sample1File = new File([blob], "msl-seo.wav");
			sample1SrcUrl = URL.createObjectURL(blob);
		};

		xhr.send();
	}

	function loadSample2() {
		var blob = null;
		var xhr = new XMLHttpRequest();
		xhr.open("GET", "/aiaas/en/audio/denoise/trump-cut.wav");
		xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
		xhr.onload = function()
		{
			blob = xhr.response;//xhr.response is now a blob object
			sample2File = new File([blob], "trump-cut.wav");
			sample2SrcUrl = URL.createObjectURL(blob);
		};

		xhr.send();
	}


	function startRequest(inputFile){
		var formData = new FormData();
		formData.append('noiseFile',inputFile);
		formData.append($("#key").val(), $("#value").val());

		var request = new XMLHttpRequest();

		request.responseType ="blob";
		request.onreadystatechange = function(){
			if (request.readyState === 4 && request.status !== 0){
				console.log(request.response);

				var blob = request.response;
				var audioSrcURL = window.URL.createObjectURL(blob);

				var link = document.getElementById("btn_dwn");
				link.href = audioSrcURL;
				link.download = "denoise.wav" ;

				$('#music4').attr('src',audioSrcURL);
				$('.dn_2').hide();
				$('.dn_3').fadeIn(300);
				$('.play').eq(3).trigger('click');
			}
		};

		request.open('POST', '/api/denoise/stream');
		request.send(formData);
		request.timeout = 90000;
		request.ontimeout = function() {
			$('#api_fail_popup').show();
		};

		request.addEventListener("abort", function(){ });

		// step2->step1
		$('.btn_back1').on('click', function () {
			if(request){ request.abort(); }

			// audio UI setting
			$('.music').each(function(){ this.currentTime = 0; });
			$('.elapsed').css("width", "0px");
			$('.timer').text("0:00");

			$('.dn_2').hide();
			$('.dn_1').fadeIn(300);
		});
	}

	function toTimeFormat (text) {
		var sec_num = parseInt(text, 10); // don't forget the second param
		var hours   = Math.floor(sec_num / 3600);
		var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
		var seconds = sec_num - (hours * 3600) - (minutes * 60);

		// if (hours   < 10) {hours   = "0"+hours;}
		if (minutes < 10) {minutes = minutes;}
		if (seconds < 10) {seconds = "0"+seconds;}

		return minutes+':'+seconds;
	}

</script>

<script>
	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();

</script>
