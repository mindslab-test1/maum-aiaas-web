<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-07-08
  Time: 10:08
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
    <div class="content api_content">
        <ul class="menu_lst">
            <li class="tablinks" onclick="openTap(event, 'correct_demo')" id="defaultOpen"><button type="button">AI Engine</button></li>
            <li class="tablinks" onclick="openTap(event, 'correct_example')"><button type="button">Use Case</button></li>
            <li class="tablinks" onclick="openTap(event, 'correct_menu')"><button type="button">Manual</button></li>
            <%--			<li class="tablinks"><a href="/member/krApiAccount" >API ID, key 발급</a></li>--%>
        </ul>
        <!--.correct_demo-->
        <div class="demobox demobox_nlu" id="correct_demo">
            <p><span>Bert Correction</span> </p>
            <span class="sub">Fixes incorrect Korean sentences according to the appropriate context. </span>
            <p class="temp_txt">The service is currently available only in Korean. Other languages will be available soon.</p>

            <!--.correct_box-->
            <div class="itf_box correct_box">
                <!--.step01-->
                <div class="step01">
                    <div class="demo_top">
                        <em class="far fa-list-alt"></em><span>Sample</span>
                    </div>
                    <div class="ift_topbox">
                        <p>Select the sentence you wish to correct.</p>
                        <ul class="ift_lst">
                            <li>
                                <div class="radio">
                                    <input type="radio" id="radio" name="sentence" value="한꾹인뜰만 알아뽈 쑤 있께 짝썽하껬씁니따." checked="checked">
                                    <label for="radio">한꾹인뜰만 알아뽈 쑤 있께 짝썽하껬씁니따.</label>
                                </div>
                            </li>
                            <li>
                                <div class="radio">
                                    <input type="radio" id="radio2" name="sentence" value="늗게 베운 도듁이 냘 세는 쥴 모룬다.">
                                    <label for="radio2">늗게 베운 도듁이 냘 세는 쥴 모룬다.</label>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <p class="txt_or">Or</p>
                    <div class="demo_top">
                        <em class="far fa-keyboard"></em><span>Input Text</span>
                    </div>
                    <div class="text_area">
                        <textarea id="text-contents" rows="10" placeholder="Input sentences to be corrected. " maxlength="120"></textarea>
                        <div class="text_info">
                            <span class=""><strong id="count">0</strong>/120</span>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_search" id="find_btn">Correct</button>
                    </div>
                </div>
                <!--//.step01-->
                <div class="loading_wrap">
                    <p><em class="far fa-file-alt"></em> In progress</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#c9c3ec" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#27c1c1" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

                        <p>AI processing takes a while..</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1" ><em class="fas fa-redo"></em>Reset</button>
                    </div>

                </div>
                <!--.step02-->
                <div class="step02">
                    <div class="demo_top">
                        <em class="far fa-file-alt"></em><span> Result</span>
                    </div>
                    <div class="result_box">
                        <ul>
                            <li>
                                <span>Input</span>
                                <p id="input_sentence"></p>
                            </li>
                            <li>
                                <span>Correction</span>
                                <p id="result_sentence"></p>
                            </li>
                        </ul>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_reset" id="btn_reset"><em class="fas fa-redo-alt"></em>Reset</button>
                    </div>
                </div>
                <!--//.step02-->
            </div>
            <!--//.itf_box-->

        </div>
        <!-- .correct_demo -->

        <!--correct_menu-->
        <div class="demobox" id="correct_menu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API Guideline
                    </div>
                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID &amp; Key</p>
                    <p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                    <p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                    <p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>
                <div class="guide_group">
                    <div class="title">Bert Correction</div>
                    <p class="sub_txt">Fixes incorrect Korean sentences according to the appropriate context.</p>
                    <span class="sub_title">
							Preparation
						</span>
                    <p class="sub_txt">① Input: Sentence (Korean)</p>
                    <span class="sub_title">
							API Document
						</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/correct/sentence</li>
                    </ul>
                    <p class="sub_txt">② Request parameters</p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API ID. Request from is required for the ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>sentence</td>
                            <td>Korean sentence (Under 120 characters)</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
							<pre>
curl --location --request POST 'http://api.maum.ai/correct/sentence' \
--header 'Content-Type: application/json' \
--data-raw '{
    "apiId": "{Own API Id}",
    "apiKey": "{Own API Key}",
    "sentence": "한꾹인뜰만 알아뽈 쑤 있께 짝썽하껬씁니따."
}'
</pre>
                    </div>

                    <p class="sub_txt">④ Response parameters </p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>Status of API request</td>
                            <td>list</td>
                        </tr>
                        <tr>
                            <td>result</td>
                            <td>Output answer (text)</td>
                            <td>string</td>
                        </tr>

                    </table>
                    <span class="table_tit">message: Status of API request </span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>Status  (Success/ Fail)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>Status code (0: Success)</td>
                            <td>number</td>
                        </tr>
                    </table>

                    <p class="sub_txt">⑤ Response example </p>
                    <div class="code_box">
<pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "result": "한국인들만 알아볼 수 있게 작성하겠습니다."
}
</pre>

                    </div>
                </div>
            </div>
        </div>
        <!--//correct_menu-->
        <!--.correct_example-->
        <div class="demobox" id="correct_example">
            <p><em style="color:#27c1c1;font-weight: 400;">Use Cases</em> </p>
            <span class="sub">Find out how AI can be applied to diverse area.</span>
            <%--correct--%>
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>Improve STT performance</span>
                            </dt>
                            <dd class="txt">Text created by the STT is modified to the correct spelling and sentences to create a more complete result. (Application example) Minutes, etc.)</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_sds"><span>SDS</span></li>
                                    <li class="ico_bqa"><span>NQA</span></li>
                                    <li class="ico_nlu"><span>NLU</span></li>
                                    <li class="ico_stt"><span>STT</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>Chatbot</span>
                            </dt>
                            <dd class="txt">The chatbot’s conversations are continuously corrected according to the context, allowing the conversation to progress smoothly.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_weatherBot"><span>Chatbot</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>Automatic Document Creation</span>
                            </dt>
                            <dd class="txt">Utilizing OCR, document texts are corrected to show higher accuracy.</dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!-- //correct -->
        </div>
        <!--//.correct_example-->
    </div>
</div>
<!-- //.contents -->




<script type="text/javascript">

    $(document).ready(function (){
        var ajaxXHR;
        var $textArea = $('#text-contents');

        //===== radioBox event =====
        $('input[type=radio]').on('click', function(){
            $textArea.text("");
            $textArea.val("");
        });

        //===== textArea event =====
        $textArea.on('change paste input keyup', function(){
            $('input[type=radio]:checked').prop("checked", false);
            countTextAreaLength ()
        });

        //===== 문장교정 =====
        $('#find_btn').on('click',function(){
            var $checkedRadio = $('input[type=radio]:checked');
            var $checkedtxt =$checkedRadio.val();
            var sentence = "";

            if( $textArea.val().trim() === ""){
                if($checkedRadio.length === 0){
                    alert("Please select an example sentence or input sentences to be corrected.");
                    return;
                }
                sentence = $checkedtxt;
            }else{
                sentence = $textArea.val();
            }
            $('.itf_box .step01').hide();
            $('.itf_box .loading_wrap').fadeIn();

            ajaxXHR = $.ajax({
                url: '/api/correct/sentence',
                async: true,
                headers: {
                    "Content-Type": "application/json"
                },
                data: {
                    "context": sentence,
                    "${_csrf.parameterName}" : "${_csrf.token}"
                },
                error: function(error){
                    if (error.status === 0) {
                        return false;
                    }
                    console.log(error);
                },
                success: function(data){
                    console.log(unescape(data));
                    var responseData = JSON.parse(data);

                    $('#input_sentence').text(sentence);
                    $('#result_sentence').text(responseData.result);


                    $('.itf_box .step02').fadeIn();
                    $('.itf_box .loading_wrap').hide();
                    // correctResult(responseData);
                }
            });
        });


        $('#btn_reset, .btn_back1').on('click',function(){
            ajaxXHR.abort();
            countTextAreaLength ();
            var textAreaObj = document.getElementById('result_sentence');
            textAreaObj.scrollTop = textAreaObj.offsetHeight - 300;
            document.getElementById('result_sentence').innerHTML = "";

            $('.itf_box .step02').hide();
            $('.itf_box .step01').fadeIn();
            $('.itf_box .loading_wrap').hide();
        });

        function countTextAreaLength (){
            var content = $('#text-contents').val();
            if (content.length >= 120) {
                $('#count').html(120);
                return;
            }
            $('#count').html(content.length);
        }
    });


    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }
    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();



</script>
