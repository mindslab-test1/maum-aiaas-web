<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>Upload error</h5>
            <p>Error occurred due to the image or file size. <br>Please check the restriction again.</p>
            <span> * Supported files: .mp4<br>* Video file size under 50MB.</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">OK</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents api_content">
    <!-- .content -->
    <div class="content">
        <h1 class="api_tit">Reverse Direction Detection</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'ardemo')" id="defaultOpen">
                <button type="button">AI Engine</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'arexample')">
                <button type="button">Use Case</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'armenu')">
                <button type="button">Manual</button>
            </li>
<%--            <li class="tablinks"><a href="/member/enApiAccount">API ID, key</a></li>--%>
        </ul>
        <!-- .demobox -->
        <div class="demobox" id="ardemo">
            <p><span>Reverse Direction Detection</span></p>
            <span class="sub">It detects the reverse driving of a walking person and informs the start point of the change of direction.</span>
            <!--faceTracking_box-->
            <div class="demo_layout faceTracking_box">
                <!--tr_1-->
                <div class="tr_1">
                    <div class="fl_box">
                        <p><em class="far fa-file-video"></em><strong>Use Sample File</strong></p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="anomaly_option" value="1" checked>
                                    <label for="sample1" class="female">
                                        <video controls width="300">
                                            <source src="${pageContext.request.contextPath}/aiaas/common/video/anomaly.reverse.01.mp4"
                                                    type="video/mp4">
                                            IE 8 and below does not produce video. Please update the IE version.
                                        </video>
                                    </label>
                                </div>
                            </div>
<%--                            <div class="sample_2">--%>
<%--                                <div class="radio">--%>
<%--                                    <input type="radio" id="sample2" name="anomaly_option" value="2">--%>
<%--                                    <label for="sample2" class="male">--%>
<%--                                        <video controls width="300">--%>
<%--                                            <source src="${pageContext.request.contextPath}/aiaas/common/video/anomaly.reverse.03.mp4"--%>
<%--                                                    type="video/mp4">--%>
<%--                                            IE 8 이하는 비디오가 나오지 않습니다. IE 버전을 업데이트 하시길 바랍니다.--%>
<%--                                        </video>--%>
<%--                                    </label>--%>
<%--                                </div>--%>
<%--                            </div>--%>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-video"></em><strong>Use My file</strong></p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-video hidden"></em>
                                <label for="demoFile" class="demolabel">Upload Video</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".mp4">
                            </div>
                            <ul>
                                <li>* Supported files: .mp4</li>
                                <li>* Video file size under 50MB</li>
                            </ul>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">Process</button>
                    </div>
                </div>
                <!--tr_1-->
                <!--tr_2-->
                <div class="tr_2">
                    <p><em class="far fa-file-video"></em>In progress</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px"
                             viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42"
                                                                             d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/>
                            <g>
                                <path fill="#f7778a" fill-opacity="1"
                                      d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/>
                                <animatetransform attributeName="transform" type="translate"
                                                  values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0"
                                                  calcMode="discrete" dur="1820ms" repeatCount="indefinite"/>
                            </g></svg>

                        <p>AI processing takes a while..</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
                    </div>

                </div>
                <!--tr_2-->
                <!--tr_3-->
                <div class="tr_3">
                    <div class="origin_file">
                        <p id="result_title"><em class="far fa-file-video"></em>Input</p>
                        <div class="videoBox">
                            <video controls width="300"
                                   src="${pageContext.request.contextPath}/aiaas/common/video/anomaly.reverse.03.mp4"
                                   type="video/mp4" id="input_video">
                                IE 8 and below does not produce video. Please update the IE version.
                            </video>
                        </div>
                    </div>
                    <div class="result_file">
                        <p><em class="far fa-file-image"></em>Result</p>
                        <div class="start_point">
                            <ul id="result_ul">
                                <li>
                                    <span>Timestamp for the start</span>

                                    <img src="" alt="역주행 시작 시점" id="snap_left">
                                    <p id="snap_time">-1 s</p>

                                </li>
                                <li>
                                    <span></span>
                                    <img src="" alt="역주행 0.5초 뒤" id="snap">
                                    <p id="snap_pre"> + 0.5 s</p>
                                </li>
                                <li>
                                    <span></span>
                                    <img src="" alt="역주행 1초 뒤" id="snap_right">
                                    <p id="snap_next"> + 0.5 s</p>
                                </li>
                            </ul>
                            <p class="fail_noti" style="padding:10px 0 0 0;font-size:13px;color:#2c3f51;margin:0;">No Reverse Direction Detection was detected.</p>
                        </div>

                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2"><em class="fas fa-redo"></em>Reset</button>
                    </div>
                </div>
                <!--tr_3-->
            </div>
            <!--//.faceTracking_box-->

        </div>
        <!-- //.demobox -->
        <!--.ftmenu-->
        <div class="demobox vision_menu" id="armenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API Guideline
                    </div>
                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID &amp; Key</p>
                    <p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                    <p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                    <p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        Reverse Direction Detection
                    </div>
                    <p class="sub_txt">Reverse Direction Detection in videos.</p>
                    <span class="sub_title">
								Preparation
					</span>
                    <p class="sub_txt">- Input: Video file</p>
                    <ul>
                        <li>File type: .mp4</li>
                        <li>Size: Under 50MB</li>
                    </ul>
                    <span class="sub_title">
								 API Document
							</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/smartXLoad/anomalyDetect</li>
                    </ul>
                    <p class="sub_txt">② Request parameters </p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API ID. Request from is required for the ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>video</td>
                            <td>Input video file (.mp4)</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
                        url --location --request POST 'http://api.maum.ai/smartXLoad/anomalyDetect' \<br>
                        --header 'Content-Type: multipart/form-data' \<br>
                        --form 'apiId= Own API ID' \<br>
                        --form 'apiKey= Own API KEY' \<br>
                        --form 'video= Input video file'<br>
                    </div>

                    <p class="sub_txt">④ Response example </p>

                    <div class="code_box">
<pre>
{
    "frame_1": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAgICAg...",

    "frame_2": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAgICAg...",

    "frame_3": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAgICAg...",

     "result_text": "0:0:6.000"
}
</pre>
                    </div>


                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.ftmenu-->
        <!--.ftexample-->
        <div class="demobox" id="arexample">
            <p><em style="color:#f7778a;font-weight: 400;">Use Cases</em></p>
            <span class="sub">Find out how AI can be applied to diverse area.</span>
            <div class="useCasesBox">
                <!-- 역주행 감지-->
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>Airport Checkpoint</span>
                            </dt>
                            <dd class="txt">The immigration checkpoint at the airport detected a reverse.
                                It allows us to respond quickly to events that occur in unexpected situations.
                                <span><em class="fas fa-book-reader"></em> Reference: A Airport</span>
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_anor"><span>Reverse direction Detection</span></li>
                                    <li class="ico_pr"><span>Pose Recognition</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>Detecting Anomalous Objects</span>
                            </dt>
                            <dd class="txt">When an abnormal movement is detected within a specific time or place, an abnormal signal is signaled to prevent an accident.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_anor"><span>Reverse direction Detection</span></li>
                                    <li class="ico_tr"><span>ESR</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
                <!--역주행 감지-->
            </div>
        </div>
        <!--//.ftexample-->


    </div>
    <!-- //.content -->
</div>


<script>
    var sampleImage1;
    var sampleImage2;
    var ajaxXHR;

    //파일명 변경
    document.querySelector("#demoFile").addEventListener('change', function (ev) {

        $('.fl_box').on('click', function () {
            $(this).css("opacity", "1");
            $('.tr_1 .btn_area .disBox').remove();
        });

        //파일 용량 체크
        var demoFileInput = document.getElementById('demoFile');
        var demoFile = demoFileInput.files[0];
        var demoFileSize = demoFile.size;
        var max_demoFileSize = 1024 * 1024 * 50;//1kb는 1024바이트
        var demofile = document.querySelector("#demoFile");

        if (demoFileSize > max_demoFileSize || !demofile.files[0].type.match(/video.mp4/)) {
            $('.pop_simple').show();
            // 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
            $('.fl_box').css("opacity", "0.5");
            $('#demoFile').val('');
            $('.tr_1 .btn_area').append('<span class="disBox"></span>');

        } else {
            document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
            var element = document.getElementById('uploadFile');
            element.classList.remove('btn');
            element.classList.add('btn_change');
            $('.fl_box').css("opacity", "0.5");
            $('.tr_1 .btn_area .disBox').remove();
            $('#sample1').removeAttr('checked');
        }
    });

    jQuery.event.add(window, "load", function () {

        function loadSample1() {
            var blob = null;
            var xhr = new XMLHttpRequest();
            xhr.open("GET", "/aiaas/common/video/anomaly.reverse.01.mp4");
            xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
            xhr.onload = function () {
                blob = xhr.response;//xhr.response is now a blob object
                sampleImage1 = new File([blob], "anomaly.reverse.01.mp4");

                var imgSrcURL = URL.createObjectURL(blob);
                var textr_output = document.getElementById('input_video');
                textr_output.setAttribute("src", imgSrcURL);
            }

            xhr.send();
        }

        $(document).ready(function () {
            loadSample1();

            // close button
            $('em.close').on('click', function () {
                $('.fl_box').css("opacity", "1");
                $(this).parent().removeClass("btn_change");
                $(this).parent().addClass("btn");
                $(this).parent().children('.demolabel').text('Upload Video');
                $('#demoFile').val('');
                //파일명 변경
                document.querySelector("#demoFile").addEventListener('change', function (ev) {
                    document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
                    var element = document.getElementById('uploadFile');
                    element.classList.remove('btn');
                    element.classList.add('btn_change');

                    $('#sample1').prop('checked', false);
                });

            });
            $('.radio input').on('click', function () {
                $('em.close').trigger('click');

            });

            $('#sub').on('click', function (blob) {
                //console.log("영상 분석 시작");
                var $demoFile = $("#demoFile");
                //샘플전송
                var formData = new FormData();

                if ($demoFile.val() === "" || $demoFile.val() === null) {
                    var demoFile;
                    var option = $("input[type=radio][name=anomaly_option]:checked").val();
                    if (option == 1) {
                        // console.log('1');
                        loadSample1();
                        demoFile = sampleImage1;
                    }
                    // console.log("File size : " + demoFile.size);
                    formData.append('file', demoFile);
                    formData.append('${_csrf.parameterName}', '${_csrf.token}');
                } else { // 사용자 파일로 해보기
                    // console.log("Your local file !! ");

                    const blob = document.getElementById('demoFile').files[0];

                    var filename = Date.now() + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).toString(2, 15);
                    filename += '.' + blob.type.split('/').pop();

                    var file = new File([blob], filename, {type: blob.type, lastModified: Date.now()});
                    // console.log("File size : " + file.size);

                    formData.append('file', file);
                    formData.append('${_csrf.parameterName}', '${_csrf.token}');

                    var imgSrcURL = URL.createObjectURL(blob);
                    $('#input_video').attr('src', imgSrcURL);
                }
                ajaxXHR = $.ajax({
                    type: "POST",
                    async: true,
                    url: "/api/multiAnomaly",
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function (result) {
                        //console.log('result', result);

                        if(result == undefined || result == ""){
                            $('#result_ul').css("display", "none");
                            $('.fail_noti').css("display", "block");

                            $('#snap_left').attr('src', "");
                            $('#snap').attr('src', "");
                            $('#snap_right').attr('src', "");
                            $('#snap_time').text("");
                            return;
                        }

                        let resultData = JSON.parse(result);

                        $('.tr_2').hide();
                        $('.tr_3').fadeIn(300);


                        $('#result_ul').fadeIn(200);
                        $('.fail_noti').css("display", "none");

                        $('#snap_left').attr('src', "data:image/jpeg;base64," + resultData.frame_1);
                        $('#snap').attr('src', "data:image/jpeg;base64," + resultData.frame_2);
                        $('#snap_right').attr('src', "data:image/jpeg;base64," + resultData.frame_3);

                        var second = resultData.result_text;

                        var substring = second.substr(2,3);
                        var number= substring.substring(2);
                        var minute = substring.substr(0,2);
                        var numberto = parseInt(number);

                        if (numberto < 10){
                            numberto = "0" + numberto;
                            $('#snap_time').text(minute+numberto);
                        }else {
                            $('#snap_time').text(substring);
                        }
                    },
                    error: function (jqXHR) {
                        if (jqXHR.status === 0) {
                            return false;
                        }
                        alert("ERROR");
                        console.dir(error);
                        window.location.reload();
                    }
                });

                $('.tr_1').hide();
                $('.tr_2').fadeIn(300);

            });

            // 처음으로 버튼
            $('.btn_back1').on('click', function () {
                ajaxXHR.abort();
                $('.tr_3').hide();
                $('.tr_2').hide();
                $('.tr_1').fadeIn(300);
                $('.fl_box').css("opacity", "1");
                $('#sample1').trigger('click');
                $('#demoFile').val("");
            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('.tr_3').hide();
                $('.tr_1').fadeIn(300);
                $('.fl_box').css("opacity", "1");
                document.querySelector("#demoFile").addEventListener('change', function (ev) {
                    document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;

                });
            });

            //알림 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
        });
    });

    function sendMultiRequest(anomaly_option, file, url) {
        var formData = new FormData();
        formData.append('file', file);
        formData.append('option', anomaly_option);
        formData.append($("#key").val(), $("#value").val());
        console.log("MultiRequest");
        $.ajax({
            type: "POST",
            async: true,
            url: url,
            data: formData,
            processData: false,
            contentType: false,
            success: function (result) {

                $('.tr_2').hide();
                $('.tr_3').fadeIn(300);
                $('#snap_left').attr('src', "data:image/jpeg;base64," + result[0].body);
                $('#snap').attr('src', "data:image/jpeg;base64," + result[1].body);
                $('#snap_right').attr('src', "data:image/jpeg;base64," + result[2].body);
                //$('#snap_time').text(result[3].body);
                var second = result[3].body;
                //console.log(second);
                var substring = second.substr(2,3);
                var number= substring.substring(2);
                var minute = substring.substr(0,2);
                var numberto = parseInt(number);
                //var addtxt = substring.substr(0,2);
                //console.log(substring);
                //console.log(addtxt);
                //console.log(jQuery.type(numberto));

                if (numberto<10){
                    var minus = numberto - 1;
                    var plus = numberto + 1;
                    numberto = "0"+numberto;
                    //minus = "0"+minus;
                    //plus = "0"+plus;
                    $('#snap_time').text(minute+numberto);
                    // $('#snap_pre').text(addtxt + minus);
                    // $('#snap_next').text(addtxt + plus);

                }else {
                    $('#snap_time').text(substring);
                    // $('#snap_pre').text(addtxt + (numberto - 1));
                    // $('#snap_next').text(addtxt + (numberto + 1));

                }


            },
            error: function (error) {
                alert("Error");
                console.dir(error);
                window.location.reload();
            }
        });
    }

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

</script>