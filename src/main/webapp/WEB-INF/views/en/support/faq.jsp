<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
		
		<!-- .contents -->
		<div class="contents"> 
			<!--content-->
            <div class="content">
				<h1>FAQ</h1>				
				<!-- .notice -->
				<!-- 검색 -->
				<div class="searchType_wrap02">
					<a class="txtWrite" href="faq_write.html">Write</a>
					<input type="text" class="input_textType" title="" placeholder="">
					<button type="button" class="btn_search">Search</button>
				</div>
				<ul class="qa_lst">
					<li class="tablinks" onclick="openCity(event, 'fqaBox')" id="defaultOpen">Getting started</li>
					<li class="tablinks" onclick="openCity(event, 'fqaBox2')">Pricing &amp; plans</li>
					<li class="tablinks" onclick="openCity(event, 'fqaBox3')">Usage Guide</li>
					<li class="tablinks" onclick="openCity(event, 'fqaBox4')">Sales question</li>
				</ul>
				
				<div id="fqaBox" class="tabcontent faqBox">	
					<div class="lst_stn">
						<button class="noticetit">Puchasing <strong>process</strong></button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p><strong>Lorem</strong> ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>	
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
					</div>
				</div>
				<!-- //.notice -->
				<div id="fqaBox2" class="tabcontent faqBox">	
					<div class="lst_stn">
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>	
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
					</div>
				</div>
				<!-- //.notice -->
				<div id="fqaBox3" class="tabcontent faqBox">	
					<div class="lst_stn">
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>	
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
					</div>
				</div>
				<!-- //.notice -->
				<div id="fqaBox4" class="tabcontent faqBox">						
					<div class="lst_stn">
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>	
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process</button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
					</div>
				</div>
				<!-- //.notice -->
				
			</div>	
			<!--content-->
		</div>
		<!-- //.contents -->

	
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/faq.js"></script>
