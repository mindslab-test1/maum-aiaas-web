<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<%@ include file="../common/common_header_v2.jsp" %>

<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/aiaas/en/css/landing_page_v2/pricing_en.css"/>

<form id="logout-form" action='/logout' method="POST">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
</form>

<!-- .lyr_info -->
<div class="lyr_info">
    <div class="lyr_plan_bg"></div>
    <!-- .lyrWrap -->
    <div class="lyrWrap">
        <button class="btn_lyrWrap_close" type="button">닫기</button>
        <div class="inquiry_box">
            <div class="ipt_box">
                <input type="text" id="name">
                <label for="name"><span class="fas fa-user"></span>Name</label>
            </div>

            <div class="ipt_box">
                <input type="tel" pattern="[0-9]{3}-[0-9]{4}-[0-9]{4}" id="phone">
                <label for="phone"><span class="fas fa-mobile-alt"></span>Phone Number</label>
            </div>

            <div class="ipt_box">
                <input type="email" id="mail">
                <label for="mail"><span class="fas fa-envelope"></span>Email</label>
            </div>

            <div class="ipt_box">
                <textarea id="txt"></textarea>
                <label for="txt"><span class="fas fa-comment-dots"></span>Message</label>
            </div>

            <button type="submit" id="sendMailToHello_landing" class="btn_send">Send</button>
        </div>

        <!-- //.lyr_bd -->
    </div>
    <!-- //.lyrWrap -->
</div>
<!-- //.lyr_info -->

<!-- .lyr_event -->
<div class="lyr_consultant">
    <div class="lyr_consultant_bg"></div>
    <!-- .lyrWrap -->
    <div class="lyrWrap">
        <div class="lyr_hd">
            <h1>Become an <span>AI Consultant</span><br>
                in <span>4 Easy Steps</span></h1>
            <em class="fas fa-times btn_lyrWrap_close"></em>
        </div>

        <!-- .lyr_bd -->
        <div class="lyr_bd">
            <ul>
                <li>
                    <p>
                        <strong>1. Join us</strong>
                        <span>Fill out your information and submit through our google form to get started!</span>

                    </p>
                    <em class="fas fa-angle-double-down"></em>
                </li>
                <li>
                    <p>
                        <strong>2. Study & Learn</strong>
                        <span>Go through our training modules and explore our maum.ai </span>
                        <span>to expand your knowledge in AI</span>
                        <%--                        <a href="/home/academyForm" target="_blank" class="academy_btn">아카데미 바로가기 <em class="fas fa-angle-right"></em></a>--%>
                    </p>
                    <em class="fas fa-angle-double-down"></em>
                </li>
                <li>
                    <p>
                        <strong>3. Take a Test</strong>
                        <span>Take a test to showcase what you have learned</span>
                    </p>
                    <em class="fas fa-angle-double-down"></em>
                </li>
                <li>
                    <p>
                        <strong>4. Get Started!</strong>
                        <span>Receive your certification and begin your AI Consultant journey</span>
                    </p>
                </li>
            </ul>

            <a href="https://docs.google.com/forms/d/e/1FAIpQLSczF2ih31tXMFZgJ5yF_5gzLEK-kRvGO25U3fJddEzGTNC4SA/viewform"
               target="_blank">Apply Now <em class="fas fa-angle-right"></em></a>

        </div>
        <!-- //.lyr_bd -->
    </div>
    <!-- //.lyrWrap -->
</div>
<!-- //.lyr_event -->


<!-- 2 .pop_confirm -->
<div class="pop_confirm" id="mail_success">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap">
        <!-- .pop_bd -->
        <div class="pop_bd">

            <!--내용 부분-->
            <p>Your question has been sent. <br>Someone will get back to you <br>as soon as possible.</p>
        </div>
        <!-- //.pop_bd -->
        <!--창닫기 버튼 -->
        <div class="btn">
            <button class="btn_close">OK</button>
        </div>
        <!--창닫기 버튼 -->
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_confirm -->

<!-- .price_detail -->
<div class="price_detail api_month">
    <div class="lyr_bg"></div>
    <!-- .lyrBox -->
    <div class="lyrBox">
        <!-- .lyr_top -->
        <div class="lyr_top">
            <h3>API Monthly Usage Limit</h3>
            <em class="fas fa-times close"></em>
        </div>
        <!-- //.lyr_top -->

        <!-- .lyr_mid -->
        <div class="lyr_mid">
            <table class="tbl_view">
                <colgroup>
                    <col>
                    <col>
                    <col>
                    <col>
                </colgroup>
                <thead>
                    <tr>
                        <th scope="col" class="cat-column">Category</th>
                        <th scope="col">Engine API</th>
                        <th scope="col">Type</th>
                        <th scope="col">Monthly Usage</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="rowgroup" rowspan="7">Voice</th>
                        <td rowspan="2" class="col_1">TTS</td>
                        <td class="col_2">Korean</td>
                        <td>4,000,000 characters</td>
                    </tr>
                    <tr>
                        <td>English</td>
                        <td>6,666,666 characters</td>
                    </tr>
                    <tr>
                        <td>STT</td>
                        <td>Eng/Kor</td>
                        <td>5,000 minutes</td>
                    </tr>
                    <tr>
                        <td>Denoise</td>
                        <td>-</td>
                        <td>50,000 minutes</td>
                    </tr>
                    <tr>
                        <td>Diarization</td>
                        <td>-</td>
                        <td>5,000 minutes</td>
                    </tr>
                    <tr>
                        <td>Voice Filter</td>
                        <td>-</td>
                        <td>25,000 minutes</td>
                    </tr>
                    <tr>
                        <td>Voice Recognition</td>
                        <td>-</td>
                        <td>25,000 minutes</td>
                    </tr>
                </tbody>
            </table>

            <table class="tbl_view">
                <colgroup>
                    <col>
                    <col>
                    <col>
                    <col>
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="rowgroup" rowspan="10" class="cat-column">Vision</th>
                        <td class="col_1">Text Removal</td>
                        <td class="col_2">-</td>
                        <td>80,000 cases</td>
                    </tr>
                    <tr>
                        <td>TTI</td>
                        <td>AI Styling</td>
                        <td>80,000 cases</td>
                    </tr>
                    <tr>
                        <td>OCR</td>
                        <td>Document<br>Analysis</td>
                        <td>80,000 cases</td>
                    </tr>
                    <tr>
                        <td>AI Vehicle Recognition</td>
                        <td>Entity<br>Detection</td>
                        <td>80,000 cases</td>
                    </tr>
                    <tr>
                        <td>Enhanced Super<br>Resolution</td>
                        <td>-</td>
                        <td>80,000 cases</td>
                    </tr>
                    <tr>
                        <td>Face Recognition</td>
                        <td>-</td>
                        <td>80,000 cases</td>
                    </tr>
                    <tr>
                        <td>Image Pose<br>Recognition</td>
                        <td>-</td>
                        <td>80,000 cases</td>
                    </tr>
                    <tr>
                        <td>Face Tracking</td>
                        <td>-</td>
                        <td>50,000 seconds</td>
                    </tr>
                    <tr>
                        <td>Anomaly Detection</td>
                        <td>-</td>
                        <td>50,000 seconds</td>
                    </tr>
                </tbody>
            </table>

            <table class="tbl_view">
                <colgroup>
                    <col>
                    <col>
                    <col>
                    <col>
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="rowgroup" rowspan="6" class="cat-column">Language</th>
                        <td class="col_1">Natural Language<br>Understanding (NLU)</td>
                        <td class="col_2">Eng/Kor</td>
                        <td>20,000 units</td>
                    </tr>
                    <tr>
                        <td>Machine Reading<br>Comprehension (MRC)</td>
                        <td>Eng/Kor</td>
                        <td>200,000 cases</td>
                    </tr>
                    <tr>
                        <td>eXplainable Document<br>Classifier (XDC)</td>
                        <td>Eng/Kor</td>
                        <td>40,000 cases</td>
                    </tr>
                    <tr>
                        <td>Generative Pre-Training (GPT-2)</td>
                        <td>Eng/Kor</td>
                        <td>100,000 cases</td>
                    </tr>
                    <tr>
                        <td>Hierarchical Multiple<br>Dictionary (HMD)</td>
                        <td>Eng/Kor</td>
                        <td>400,000 cases</td>
                    </tr>
                </tbody>
            </table>

            <table class="tbl_view">
                <colgroup>
                    <col>
                    <col>
                    <col>
                    <col>
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="row" class="cat-column">Analysis</th>
                        <td class="col_1">Correlation Analysis</td>
                        <td class="col_2">-</td>
                        <td>200,000 cases</td>
                    </tr>
                </tbody>
            </table>
            <table class="tbl_view">
                <colgroup>
                    <col>
                    <col>
                    <col>
                    <col>
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="rowgroup" rowspan="3" class="cat-column">Conversation</th>
                        <td class="col_1">NQA Bot</td>
                        <td class="col_2">Korean</td>
                        <td>40,000 cases</td>
                    </tr>
                    <tr>
                        <td rowspan="2">Ready Made Bots</td>
                        <td>Weather Bot(Korean)</td>
                        <td>20,000 cases</td>
                    </tr>
                    <tr>
                        <td>Wiki Bot<br>(Korean)</td>
                        <td>20,000 cases</td>
                    </tr>
                </tbody>
            </table>
            <table class="tbl_view">
                <colgroup>
                    <col>
                    <col>
                    <col>
                    <col>
                </colgroup>
                <tbody>
                    <tr>
                        <th scope="rowgroup" rowspan="3" class="cat-column">English<br>Education</th>
                        <td class="col_1">STT for Eng. Education</td>
                        <td class="col_2">English</td>
                        <td>4,000 minutes</td>
                    </tr>
                    <tr>
                        <td>Automatic <br>Pronounciation Scoring (APS)</td>
                        <td>English</td>
                        <td>20,000 cases</td>
                    </tr>
                    <tr>
                        <td>Phonics Assessment</td>
                        <td>English</td>
                        <td>20,000 cases</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- //.lyr_mid -->
    </div>
    <!-- //.lyrBox -->
</div>
<!-- //.price_detail -->

<!-- #header -->
<div id="header">
    <!-- .header_box -->
    <div class="header_box">
        <h1 class="fl"><a href="/?lang=en">
            <img src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/logo_maumai_bk.svg"
                 alt="maum ai logo">
        </a></h1>

        <ul class="gnb fr">
            <li><a href="/#our_services">Service</a></li>
            <li><a href="/#ecoMINDs">ecoMINDs</a></li>
            <li><a href="#" class="go_const">AI Consultant</a></li>
            <li><a href="#none">Pricing</a></li>
            <li><a href="https://mindslab.ai:8080/en" target="_blank" title="MINDsLab">About Us</a></li>
            <!-- 로그인 X인 경우 -->
            <sec:authorize access="isAnonymous()">
                <li class="brd_btn"><a href="javascript:login()">Sign In</a></li>
            </sec:authorize>

            <!-- 로그인 O인 경우 -->
            <sec:authorize access="isAuthenticated()">
                <li class="user_info">
                    <div class="userBox">
                        <ul>
                            <li>
                                <p class="ico_user">
                                    <em class="far fa-user"></em>
                                    <span>${fn:escapeXml(sessionScope.accessUser.email)}</span>
                                    <em class="fas fa-angle-down"></em>
                                    <em class="fas fa-angle-up"></em>
                                </p>
                                <ul class="lst">
                                    <li class="ico_profile"><a target="_self" href="/user/profileMain">PROFILE</a></li>
                                    <li class="ico_account"><a target="_self" href="/user/apiAccountMain">API
                                        ACCOUNT</a></li>
                                    <li class="ico_payment"><a target="_self" href="/user/paymentInfoMain">PAYMENT</a>
                                    </li>
                                    <li class="ico_logout"><a href="#"
                                                              onclick="document.getElementById('logout-form').submit();">LOGOUT</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </li>
            </sec:authorize>
            <li class="lang">
                <div class="lang_box">
                    <span>English</span>
                    <span><a href="/?lang=kr" target="_self">Korean</a></span>
                </div>
            </li>
        </ul>
    </div>
    <!-- //.header_box -->
</div>
<!-- //#header -->

<!-- #wrap -->
<div id="wrap">
    <!-- aside -->
    <div class="aside" style="">
        <!-- .aside_top -->
        <div class="aside_top">
            <h1><a href="/?lang=en"><img
                    src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/logo_maumai_bk.svg"
                    alt="maumAI logo"></a></h1>
            <!-- 로그인 X인 경우 -->
            <sec:authorize access="isAnonymous()">
                <a class="btn_sign" href="javascript:login()">Sign In</a>
            </sec:authorize>

            <!-- 로그인 O인 경우 -->
            <sec:authorize access="isAuthenticated()">
                <a class="btn_sign" href="#none">${fn:escapeXml(sessionScope.accessUser.email)}</a>
            </sec:authorize>
        </div>
        <!-- //.aside_top -->
        <!-- .aside_mid -->
        <div class="aside_mid">
            <ul class="m_nav">
                <li>
                    <h2><a href="#none" class="">My Page <em class="fas fa-chevron-down"></em></a></h2>
                    <ul class="m_lst">
                        <li>
                            <h3><a href="/user/profileMain" target="_blank" title="profile"
                                   class="layer_m_btn">PROFILE</a></h3>
                            <h3><a href="/user/apiAccountMain" target="_blank" title="api_account" class="layer_m_btn">API
                                ACCOUNT</a></h3>
                            <h3><a href="/user/paymentInfoMain" target="_blank" title="payment" class="layer_m_btn">PAYMENT</a>
                            </h3>
                            <h3><a href="#"
                                   onclick="document.getElementById('logout-form').submit();">LOGOUT</a></h3>
                        </li>

                    </ul>
                </li>

                <li>
                    <h2><a href="#none" class="">Service <em class="fas fa-chevron-down"></em></a></h2>
                    <ul class="m_lst">
                        <li>
                            <h3><a href="https://builder.maum.ai/enLanding" class="layer_m_btn">AI Builder</a></h3>
                        </li>
                        <li>
                            <h3><a href="/main/enMainHome" class="layer_m_btn">Cloud API</a></h3>
                        </li>
                        <li>
                            <h3><a href="https://fast-aicc.maum.ai" class="layer_m_btn">FAST Conversational AI</a></h3>
                        </li>
                        <li>
                            <h3><a href="https://minutes.maum.ai/" class="layer_m_btn">Maum Minutes</a></h3>
                        </li>
                    </ul>
                </li>

                <li><h2><a href="/#ecoMINDs" class="go_ecominds">ecoMINDs</a></h2></li>
                <li><a href="#" class="go_const">AI Consultant</a></li>
                <li><h2><a href="/home/pricingPage?lang=en">Pricing</a></h2></li>
                <li><h2><a href="https://mindslab.ai:8080/en" target="_blank" title="MINDsLab">About Us</a></h2></li>

            </ul>
        </div>
        <!-- //.aside_mid -->

        <!-- .aside_btm -->
        <div class="aside_btm">
            <span>English</span>
            <span><a href="/?lang=kr" target="_self">Korean</a></span>
        </div>
        <!-- .aside_btm -->
        <%-- //.aside --%>
    </div>
    <div class="bg_aside"></div>
    <a class="btn_header_ham" href="#none"><span class="hamburger_icon"></span></a>
    <!-- //.aside -->
    <!-- #contents -->
    <div id="contents">
        <!-- .cont_box -->
        <div class="cont_box">
            <!-- .priceBox -->
            <div class="priceBox">
                <h1>Pricing Plan</h1>
                <span>Make your own AI service with maum.ai<br><em>Join Free for a Month</em></span>
                <!-- .price_stn -->
                <div class="price_stn">
                    <div class="cardBox business">
                        <h4>Business</h4>
                        <div class="inner_area">
                            <p><small>1Month</small> &dollar;89 <strong>Free <em>1st</em> Month</strong></p>
                            <div>
                                <span>Cloud API<br>AI Builder</span>
                                <span>maum DATA<br>FAST Conv. AI</span>
                            </div>
                            <ul>
                                <li><em class="fas fa-check"></em>Customized Training
                                    <small>&lpar;30&plus; AI engines and customizable models&rpar;</small>
                                </li>
                                <li><em class="fas fa-check"></em>REST API ID &amp; Key</li>
                                <li><em class="fas fa-check"></em>Usage Tracking</li>
                                <li><em class="fas fa-check"></em>Technical Support</li>
                                <li><em class="fas fa-check"></em>Download Result File</li>
                                <li><em class="fas fa-check"></em>File Archiving &lpar;3 months&rpar;</li>
                            </ul>
                            <button type="button" id="api_month">API Usage Limit &nbsp;<em
                                    class="fas fa-angle-right"></em></button>
                            <!-- 로그인 X인 경우 -->
                            <sec:authorize access="isAnonymous()">
                            <a href="javascript:login()" title="Start Business">TRY NOW</a>
                            </sec:authorize>
                            <!-- 로그인 O인 경우 -->
                            <sec:authorize access="isAuthenticated()">
                                <a href="/" title="Business 시작하기">TRY NOW</a>
                            </sec:authorize>
                        </div>
                    </div>
                    <div class="cardBox enterprise">
                        <h4>Enterprise</h4>
                        <div class="inner_area">
                            <p><small>Customized To Your</small> Business</p>
                            <div>
                                <span>Personal AI Consultant</span>
                            </div>
                            <ul>
                                <li><em class="fas fa-check"></em>Customized Training
                                    <small>&lpar;&plus;30 AI engines and models&rpar;</small>
                                </li>
                                <li><em class="fas fa-check"></em>REST API ID &amp; Key</li>
                                <li><em class="fas fa-check"></em>Usage Tracking</li>
                                <li><em class="fas fa-check"></em>Technical Support</li>
                                <li><em class="fas fa-check"></em>Download Result File</li>
                                <li><em class="fas fa-check"></em>File Archiving &lpar;3 months&rpar;</li>
                            </ul>
                            <ul class="list">
                                <li><em class="fas fa-check"></em>Individual Site</li>
                                <li><em class="fas fa-check"></em>Multiple Accounts</li>
                                <li><em class="fas fa-check"></em>My AI Voice &lpar;est. &dollar;1,000&rpar;</li>
                            </ul>
                            <a href="#" class="contact_btn" title="Inquiry Enterprise">ASK NOW</a>
                        </div>
                    </div>
                </div>
                <!-- //.price_stn -->
            </div>
            <!-- //.priceBox -->
        </div>
        <!-- //.cont_box -->
    </div>
    <!-- //#contents -->

    <%@ include file="../common/footer.jsp" %>
    <!-- //#footer -->

    <script>
        window.onload = function () {
            // 소개서 다운받기 버튼
            $('.contact_btn').on('click', function (e) {
                $('.lyr_info').fadeIn();
            });

            // 팝업 API 열기
            $('#api_month').on('click', function () {
                $('.price_detail').fadeIn(300);
            })

            // 팝업 API 닫기
            $('.close').on('click', function () {
                $('.price_detail').fadeOut(300);
            });

            // Hide Header on on scroll down
            var didScroll;
            var lastScrollTop = 0;
            var delta = 5;
            $(".service_btn").on('click', function () {
                $(this).toggleClass('active');
                $(".dropdown-menu").fadeToggle(200);
            });


            $(window).scroll(function (event) {
                didScroll = true;
            });
            setInterval(function () {
                if (didScroll) {
                    hasScrolled();
                    didScroll = false;
                }
            }, 250);

            function hasScrolled() {
                var scrollTop = $(window).scrollTop();
                // Make sure they scroll more than delta
                if (Math.abs(lastScrollTop - scrollTop) <= delta)
                    return;
                if (scrollTop > 20) {
                    // Scroll Down
                    $('.main_header').addClass('fixed');
                    $('.btn_header_ham').addClass('fixed_ham')


                } else {
                    // Scroll Up
                    if (scrollTop + $(window).height() < $(document).height()) {
                        $('.main_header').removeClass('fixed');
                        $('.btn_header_ham').removeClass('fixed_ham')
                    }
                }
                lastScrollTop = scrollTop;

                //서비스 구조 보기 모바일
                $('.layer_m_btn').on('click', function () {
                    $('.lyr_service').fadeIn();
                    $('.btn_header_ham').removeClass('active');
                    $('.aside').animate({
                        width: '0',
                    }, {duration: 200, queue: false});
                    $('.btn_goTop').show();

                    $('.bg_aside').animate({
                        opacity: 0,
                    }, {duration: 150, queue: false});
                    $('.bg_aside').css({
                        display: 'none',
                    });
                    $('body').css({
                        'overflow': 'hidden',
                    });

                });

                var browse = navigator.userAgent.toLowerCase();

                if ((navigator.appName == 'Netscape' && browse.indexOf('trident') != -1) || (browse.indexOf("msie") != -1)) {
                    $('.pop_confirm').fadeIn();
                }

                // 공통 팝업창 닫기
                $('.pop_close, .pop_bg, .btn a, .btn button').on('click', function () {
                    $('.pop_confirm').fadeOut(300);
                });

            }
        };
    </script>

    <script type="text/javascript">
        function login() {
            var path = window.location.pathname;
            location.href = "${google_url}" + "?targetUrl=" + path;

        }
    </script>
