<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

		<!-- .contents -->
		<div class="contents"> 
			<!-- The Modal -->
			<div id="myModal" class="modal">
				<div class="modalImg">
				  <a href="#none" class="layer_close"></a>
				  <img class="modal-content" id="img01" alt="업로드 이미지">
				</div>
			</div>
			<div id="myModal2" class="modal">
				<div class="modalImg">
				  <a href="#none" class="layer_close"></a>
				  <img class="modal-content" id="img02" alt="업로드 이미지">
				</div>
			</div>
			<!-- //The Modal -->
			
			<!--content-->
            <div class="content">
				<h1>My requests</h1>
				<!-- .notice -->
				<div id="noticeBox" class="request">	
					<div class="chackboxarea">
						<div class="checkbox">
							<input type="checkbox" id="checkboxType01"><label for="checkboxType01">Complete</label>
						</div>
						<div class="checkbox">
							<input type="checkbox" id="checkboxType02" checked="checked"><label for="checkboxType02">In Progress</label>
						</div>
					</div>
					<div class="lst_stn">
						<button class="noticetit">Puchasing process<div class="right_txt"><span>2019.03.09 17:25</span> <em class="incomplete">In Progress</em></div></button>						
						<div class="noticetxt">
							<div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit"><span class="tag">&#91;Speech to Text&#93; &#91;bug&#93;</span>Puchasing process<div class="right_txt"><span>2019.03.09 17:25</span> <em class="complete">Complete</em></div></button>
						<div class="noticetxt">
						     <div class="txt_area">	
								 <em class="writer">Writer : admin@gmail.com</em>
								 <p>Hello<br>asd df df df df df dasdfasdfasdfasdf</p>
								 <div class="imgarea">
									<span class="img">
										<img id="myImg" src="${pageContext.request.contextPath}/aiaas/en/images/capture.jpg" alt="불러온 이미지">										
									</span>	
									 <span class="img">
										<img id="myImg2" src="${pageContext.request.contextPath}/aiaas/en/images/capture.jpg" alt="불러온 이미지">								
									</span>	
								 </div>									 
								 <span class="attach_file">Attachment</span>
								 <span class="attach_file_tit">asd.JPG <img src="${pageContext.request.contextPath}/aiaas/en/images/ico_download_c.png" alt="다운로드"></span>	
								 <span class="attach_file_tit">dfdf.JPG <img src="${pageContext.request.contextPath}/aiaas/en/images/ico_download_c.png" alt="다운로드"></span>
							 </div>							
							 <div class="txt_area">
								<div class="answer_area">
									<textarea rows="4" cols="50"></textarea>
									 <button type="submit">Submit</button>
								</div>
								 <div class="answer_complete">
									 <span>maum.ai C/S</span>
									 <em>2019.03.09 17:25</em>
									 <p>확인을 해보니 현재 정상적으로 합계가 되어지는 것으로 보입니다.<br>
다시 한 번 시도 후에도 같은 현상이 발생하신다면 정확한 상황 설명 부탁드립니다. 감사합니다.</p>
								</div>
							 </div>
						</div>
						<button class="noticetit">Puchasing process<div class="right_txt"><span>2019.03.09 17:25</span> <em class="complete">Complete</em></div></button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>	
						<button class="noticetit">Puchasing process<div class="right_txt"><span>2019.03.09 17:25</span> <em class="complete">Complete</em></div></button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit">Puchasing process<div class="right_txt"><span>2019.03.09 17:25</span> <em class="complete">Complete</em></div></button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
						<button class="noticetit last">Puchasing process<div class="right_txt"><span>2019.03.09 17:25</span> <em class="complete">Complete</em></div></button>
						<div class="noticetxt">
						    <div class="txt_area">	
						  		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
							</div>	
						</div>
					</div>
					<!-- 페이징 -->
                    <div class="pageing">
                        <a href="#" class="first">처음페이지</a>
                        <a href="#" class="prev">이전페이지</a>
                        <strong>1</strong>
                        <a href="#">2</a>
                        <a href="#">3</a>
                        <a href="#">4</a>
                        <a href="#">5</a>
                        <a href="#">6</a>
                        <a href="#" class="next">다음페이지</a>
                        <a href="#" class="end">마지막페이지</a>
                    </div>
                    <!-- //페이징 -->
				</div>
				<!-- //.notice -->
			</div>	
			<!--content-->
		</div>
		<!-- //.contents -->

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/request.js"></script>
