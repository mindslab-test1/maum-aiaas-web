﻿<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

		<!-- .contents -->
		<div class="contents">
        	
            <div class="content">
				<h1>Pricing</h1>
				
				<!--.demobox-->
				<div class="demobox">
					<!-- .priceBox -->
					<div class="priceBox">
						<h1>Pricing Plan <span>Make your own AI service with maum.ai <strong>Join free for a month.</strong> </span></h1>
						<!--			<a href="https://console.maum.ai/" target="_blank" title="새창열림" class="btn_try">무료 체험하기</a>-->

						<!-- .stn -->
						<div class="price_stn">
							<!-- .lyrmore -->
							<div class="lyrmore">
								<div class="lyr_bg"></div>
								<!-- .lyr_more_premium -->
								<div id="lyr_more_premium" class="lyrBox">
									<div class="lyr_top">
										<h3>Business</h3>
									</div>
									<div class="lyr_mid">
										<table class="tbl_view">
											<colgroup>
												<col><col><col>
											</colgroup>
											<tbody>
											<tr>
												<th>Service</th>
												<th>Type</th>
												<th>Usage (1 month) <br>Based on individual service</th>
											</tr>
											<tr>
												<td><strong>Speech to Text</strong> </td>
												<td>Kor / Eng</td>
												<td>6,000 minutes</td>
											</tr>
											<tr>
												<td rowspan="2"><strong>AI Voice</strong> </td>
												<td>Korean</td>
												<td>4,800,000 characters</td>
											</tr>
											<tr>
												<td>English</td>
												<td>8,000,000 characters</td>
											</tr>
											<tr>
												<td><strong>MRC</strong></td>
												<td>Kor / Eng </td>
												<td>240,000 calls</td>
											</tr>
											<tr>
												<td><strong>XDC</strong> </td>
												<td>Kor / Eng </td>
												<td>48,000 calls</td>
											</tr>
											<tr>
												<td><strong>NLU</strong></td>
												<td>Kor / Eng </td>
												<td>24,000 units<br><small>* 1 unit = 1,000words</small></td>
											</tr>
											<tr>
												<td><strong>HMD</strong> </td>
												<td>Kor / Eng </td>
												<td>TBD</td>
											</tr>
											<tr>
												<td><strong>DIR</strong></td>
												<td></td>
												<td>48,000 images</td>
											</tr>

											<tr>
												<td rowspan="2"><strong>Chatbot</strong></td>
												<td>Weather Bot</td>
												<td>24,000 sessions</td>
											</tr>
											<tr>
												<td>Wiki Bot</td>
												<td>24,000 sessions</td>
											</tr>
											</tbody>
										</table>
									</div>
									<div class="lyr_btm">
										<ul class="btn_lst">
											<li><button type="button" class="btn_lyr_close">닫기</button></li>
										</ul>
									</div>
								</div>
								<!-- //.lyr_more_premium -->
							</div>
							<!-- //.lyrmore -->

							<table class="tbl_lst">
								<colgroup>
									<col width="60"><col width="200"><col width="60"><col width="200"><col width="60"><col width="200"><col width="60">
								</colgroup>
								<thead>
								<tr>
									<th scope="col"></th>
									<th scope="col"></th>
									<th scope="col"></th>
									<th scope="col">Business</th>
									<th scope="col"></th>
									<th scope="col">Enterprise</th>
									<th scope="col"></th>
								</tr>
								</thead>
								<tbody>
								<tr class="row_1">
									<td></td>
									<td class="price">
										<p>Pricing / <strong></strong></p>
										<em><strong>Types of Services</strong></em>
									</td>
									<td></td>
									<td class="price_1">
										<p>1Month <span>$ 89 </span></p>
										<em class="em"><em class="far fa-bookmark"></em>Individual API ID &amp; Key</em>
										<div><a class="more premium">+ more</a></div>
<%--										<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">--%>
<%--											<input type="hidden" name="cmd" value="_s-xclick">--%>
<%--											<input type="hidden" name="hosted_button_id" value="SDDPS3EGM9RDG">--%>
<%--											<button type="submit" class="buybtn">Subscribe</button>--%>
<%--										</form>--%>
									</td>
									<td></td>
									<td class="price_1">
										<p><span>Flexible</span></p>
										<em class="em"><em class="far fa-bookmark"></em>Customized AI <strong>Application Service</strong></em>
										<div><a class="email" href="#">hello@mindslab.ai</a></div>
									</td>
									<td></td>
								</tr>

								<tr class="lst_row">
									<td></td>
									<td><span>Plus <em class="fas fa-chevron-down"></em></span>
										<ul class="ul_txt">
											<li><strong>Customized training</strong></li>
											<li><strong>REST API ID &amp; Key</strong></li>
											<li><strong>Individual site</strong></li>
											<li><strong>Multiple accounts</strong></li>
											<li class="li_df">Create My AI Voice
												<span>(Apporx. $1,000)</span>
											</li>
											<li>Usage tracking</li>
											<li>Technical Support</li>
											<li>Download result file</li>
											<li>File Archiving (3 months)</li>
										</ul>
									</td>
									<td></td>
									<td><span></span>
										<ul>
											<li><a href="/support/enContactUs">Contact us</a></li>
											<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
											<li></li>
											<li></li>
											<li class="li_df"></li>
											<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
											<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
											<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
											<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
										</ul>
									</td>
									<td></td>
									<td><span></span>
										<ul>
											<li><a href="/support/enContactUs">Contact us</a></li>
											<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
											<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
											<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
											<li class="li_df"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
											<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
											<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
											<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
											<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
										</ul>
									</td>
									<td></td>
								</tr>
								<tr class="one_row">
									<td></td>
									<td><span class="folder">Usage Limit  <em class="fas fa-chevron-up"></em></span></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
								</tr>
								<tr class="lst_row" id="folder_box" style="display: none;">
									<td></td>
									<td>
										<ul class="ul_txt or_txt">
											<li><strong>Voice Recognition</strong> Kor./Eng.</li>
											<li><strong>Voice Generation </strong> <strong>Korean</strong></li>
											<li><strong>Voice Generation </strong> <strong>English</strong></li>
										</ul>
									</td>
									<td></td>
									<td>
										<ul>
											<li>6,000 minutes <span class="span">(84 hrs)</span></li>
											<li>4,800,000 characters</li>
											<li>8,000,000 characters</li>
										</ul>
									</td>
									<td></td>
									<td>
										<ul>
											<li>Unlimited</li>
										</ul>
									</td>
									<td></td>
								</tr>

								</tbody>
							</table>
						</div>
						<!-- //.price_stn -->
					</div>
					<!-- //.priceBox -->
					<!-- .pricingBox_m -->
					<div class="pricingBox_m">

						<h1>Pricing Plan<span>Make your own AI service with maum.ai. <strong>Join free for a month.</strong> </span></h1>
						<!-- .lyrmore -->
						<div class="lyrmore">
							<div class="lyr_bg"></div>

							<!-- .lyr_more_premium -->
							<div id="lyr_more_premium_m" class="lyrBox">
								<div class="lyr_top">
									<h3>PREMIUM</h3>
								</div>
								<div class="lyr_mid">
									<table class="tbl_view">
										<colgroup>
											<col><col><col>
										</colgroup>
										<tbody>
										<tr>
											<th>Service</th>
											<th>Type</th>
											<th>Usage (1 month) <br>Based on individual service</th>
										</tr>
										<tr>
											<td><strong>Speech to Text</strong> </td>
											<td>Speech to Text</td>
											<td>6,000 minutes</td>
										</tr>
										<tr>
											<td rowspan="2"><strong>AI Voice</strong> </td>
											<td>Korean</td>
											<td>4,800,000 characters</td>
										</tr>
										<tr>
											<td>English</td>
											<td>8,000,000 characters</td>
										</tr>
										<tr>
											<td><strong>MRC</strong></td>
											<td>Kor / Eng </td>
											<td>240,000 calls</td>
										</tr>
										<tr>
											<td><strong>XDC</strong> </td>
											<td>Kor / Eng </td>
											<td>48,000 calls</td>
										</tr>
										<tr>
											<td><strong>NLU</strong></td>
											<td>Kor / Eng </td>
											<td>24,000 units<br><small>* 1 unit = 1,000words</small></td>
										</tr>
										<tr>
											<td><strong>HMD</strong> </td>
											<td>Kor / Eng </td>
											<td>TBD</td>
										</tr>
										<tr>
											<td><strong>DIR</strong></td>
											<td></td>
											<td>48,000 images</td>
										</tr>

										<tr>
											<td rowspan="2"><strong>Chatbot</strong></td>
											<td>Weather Bot</td>
											<td>24,000 sessions</td>
										</tr>
										<tr>
											<td>Wiki Bot</td>
											<td>24,000 sessions</td>
										</tr>
										</tbody>
									</table>
								</div>
								<div class="lyr_btm">
									<ul class="btn_lst">
										<li><button type="button" class="btn_lyr_close">닫기</button></li>
									</ul>
								</div>
							</div>
							<!-- //.lyr_more_premium -->
						</div>
						<!-- //.lyrmore -->
						<!-- .price_tab -->
						<div class="price_tab">
							<div class="tab">
								<button class="tablinks" onclick="openPrice(event, 'premium')" id="defaultOpen">BUSINESS</button>
								<button class="tablinks enterprise" onclick="openPrice(event, 'enterprise')">ENTERPRISE</button>
							</div>

							<!-- .premium -->
							<div id="premium" class="tabcontent">
								<table class="tbl_lst_m">
									<colgroup>
										<col><col><col><col><col>
									</colgroup>
									<tbody>
									<tr class="row_1 border_add">
										<td class="price">Pricing / <strong></strong></td>
										<td class="price_1"><span>$ 89</span></td>
									</tr>
									<tr class="lst_row">
										<td><span>Types of Services</span></td>
										<td>
											<em class="em"><em class="far fa-bookmark"></em>Individual API ID &amp; Key</em>
											<div><a class="more premium_m">+ more</a></div>
										</td>
									</tr>

									<tr class="lst_row">
										<td><span>Plus <em class="fas fa-chevron-down"></em></span>
											<ul class="ul_txt">
												<li><strong>Customized training</strong></li>
												<li><strong>REST API ID &amp; Key</strong></li>
												<li><strong>Individual site</strong></li>
												<li><strong>Multiple accounts</strong></li>
												<li class="li_df">Create My AI Voice
													<span>(Apporx. $1,000)</span>
												</li>
												<li>Usage tracking</li>
												<li>Technical Support</li>
												<li>Download result file</li>
												<li>File Archiving (3 months)</li>
											</ul>
										</td>
										<td><span></span>
											<ul>
												<li><a href="/support/enContactUs">Contact us</a></li>
												<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
												<li></li>
												<li></li>
												<li class="li_df"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
												<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
												<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
												<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
												<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
											</ul>
										</td>
									<tr class="lst_row">
										<td><span>Usage Limit <em class="fas fa-chevron-down"></em></span>
											<ul class="ul_txt or_txt">
												<li><strong>Voice Recognition</strong> Kor./Eng.</li>
												<li><strong>Voice Generation </strong> <strong>Korean</strong></li>
												<li><strong>Voice Generation </strong> <strong>English</strong></li>
											</ul>
										</td>
										<td><span></span>
											<ul>
												<li>6,000 minutes <span class="span">(84 hrs)</span></li>
												<li>4,800,000 <span class="span">characters</span></li>
												<li>8,000,000 <span class="span">characters</span></li>
											</ul>
										</td>
									</tr>

									<!--							<tr>
                                                                        <td scope="col"></td>
                                                                        <td scope="col" class="form_td">
                                                                            <button type="button" class="buybtn" onclick="">결제하기</button>

                                                                            <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank" class="form">
                                                                                <input type="hidden" name="cmd" value="_s-xclick">
                                                                                <input type="hidden" name="hosted_button_id" value="SDDPS3EGM9RDG">
                                                                                <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                                                                                <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                                                                            </form>

                                                                        </td>
                                                                    </tr>-->
									</tbody>
								</table>
							</div>
							<!-- //.premium -->
							<!-- .enterprise -->
							<div id="enterprise" class="tabcontent">
								<!-- .tbl_lst_m -->
								<table class="tbl_lst_m">
									<colgroup>
										<col><col>
									</colgroup>
									<tbody>
									<tr class="row_1 border_add">
										<td class="price">Pricing / <strong></strong></td>
										<td class="price_1"><a href="#api_contact"> hello@mindslab.ai</a></td>
									</tr>
									<tr class="lst_row">
										<td><span>Types of Services</span></td>
										<td><em class="em"><em class="far fa-bookmark"></em>Customized AI<br>Application Service</em></td>
									</tr>

									<tr class="lst_row">
										<td><span>Plus <em class="fas fa-chevron-down"></em></span>
											<ul class="ul_txt">
												<li><strong>Customized training</strong></li>
												<li><strong>REST API ID &amp; Key</strong></li>
												<li><strong>Individual site</strong></li>
												<li><strong>Multiple accounts</strong></li>
												<li class="li_df">Create My AI Voice
													<span>(Apporx. $1,000)</span>
												</li>
												<li>Usage tracking</li>
												<li>Technical Support</li>
												<li>Download result file</li>
												<li>File Archiving (3 months)</li>
											</ul>
										</td>
										<td><span></span>
											<ul>
												<li><a href="/support/enContactUs">Contact us</a></li>
												<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
												<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
												<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
												<li class="li_df"><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
												<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
												<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
												<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
												<li><img src="${pageContext.request.contextPath}/aiaas/en/images/ico_pricing_check.png" alt="pricing check"></li>
											</ul>
										</td>
									</tr>
									<tr class="lst_row">
										<td><span>Usage Limit <em class="fas fa-chevron-down"></em></span>
											<ul class="ul_txt or_txt">
												<li><strong>Voice Recognition</strong> Kor./Eng.</li>
												<li><strong>Voice Generation </strong> <strong>Korean</strong></li>
												<li><strong>Voice Generation </strong> <strong>English</strong></li>
											</ul>
										</td>
										<td><span></span>
											<ul>
												<li>Unlimited</li>
											</ul>
										</td>
									</tr>
									<!--
                                                                    <tr>
                                                                        <td scope="col"></td>
                                                                        <td scope="col" class="form_td">
                                                                            <a href="#" class="buybtn">문의하기</a>
                                                                        </td>
                                                                    </tr>
                                    -->
									</tbody>
								</table>
								<!-- //.tbl_lst_m -->
							</div>
							<!-- //.enterprise -->
						</div>
						<!-- //.price_tab -->
					</div>
					<!-- //.pricingBox_m -->
				</div>
				
			<!--//.demobox-->						
			</div>

		</div>
		<!-- //.contents -->

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/pricing.js"></script>
