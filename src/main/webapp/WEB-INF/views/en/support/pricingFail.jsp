﻿           <div class="content">
				<h1>Pricing</h1>
				
				<!--.demobox-->
				<div class="demobox">
					<!-- .priceBox -->
					<div class="failure_box">
						<h1><em class="fas fa-exclamation-circle"></em> 결제에 실패하였습니다</h1>
						<!--<a href="https://console.maum.ai/" target="_blank" title="새창열림" class="btn_try">무료 체험하기</a>-->
						<table class="failure_tb">
							<tr>
								<th>실패사유</th>								
								<td>결제실패 &#91;${reason }&#93; , 주문번호 :${moid }
									<span>결제승인이 실패되었습니다. 실패사유를 확인하신 후 재시도를 하시거나, 계속 실패되시는 경우 고객센터로 문의주시기 바랍니다.</span>
								</td>
							</tr>
							<tr>
								<th>문의</th>
								<td class="emaildesc">support@mindslab.ai</td>
							</tr>
						</table>
						<a href="/support/enPricing">돌아가기</a>
					</div>
					<!-- //.priceBox -->
				
				</div>
				<!--//.demobox-->						
			</div>