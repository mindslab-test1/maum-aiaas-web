<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

		<!-- .contents -->
		<div class="contents"> 
			<!--content-->
            <div class="content">
				<h1>FAQ</h1>
				<!-- .notice -->
				<div id="noticeBox" class="writebox">	
					<div class="notice_write">					
						<div class="wide_box">
							<label for="title">Title</label>
							<input type="text" id="title" class="ipt_txt" placeholder="">   
						</div>
						<div class="short_box">
							<label for="data">Type</label>
							<div class="selectbox"> 
								<label for="select">select</label> 
								<select id="select"> 
									<option selected>select</option> 
									 <option value="1">1</option> 
									<option value="2">2</option>                          
								</select> 
							</div>
						</div>
						<div class="short_box">
							<label for="data">date</label>
							<input type="text" id="" class="ipt_txt ipt_short" placeholder="">  
						</div>
						<div class="short_box">
							<label for="data">공개여부</label>
							<div class="selectbox"> 
								<label for="">공개</label> 
								<select id=""> 
									<option selected>공개</option> 
									 <option value="1">1</option> 
									<option value="2">2</option>                          
								</select> 
							</div>
						</div>	
					</div>
					
					<textarea class="writeArea"></textarea>
					<div class="btn_area">
						<a class="btn gray">Cancel</a>
						<a class="btn blue">Save</a>
					</div>
					
					
				</div>
				<!-- //.notice -->		
			</div>	
			<!--content-->

		</div>
		<!-- //.contents -->


<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/faq.js"></script>
