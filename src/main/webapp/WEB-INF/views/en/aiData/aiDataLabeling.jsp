<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-01-10
  Time: 16:38
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<%--  CSS/JS 파일 캐싱 방지 --%>
<%

    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">

    <!-- Open Graph Tag -->
    <meta property="og:title"            content="maum.ai"/>
    <meta property="og:type"             content="website"/><!-- 웹 페이지 타입 -->
    <meta property="og:url"              content="https://maum.ai"/>
    <meta property="og:image"            content="${pageContext.request.contextPath}/aiaas/common/images/maum.ai_web.png"/>
    <meta property="og:description"      content="웹인공지능이 필요할땐 마음AI"/>

    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/common/css/all.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/en/css/reset.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/en/css/font.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/en/css/login_renew.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/en/css/data_common.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/en/css/data_labelling_en.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/data_labelling.js"></script>


    <title>AI Data Labeling Services</title>
</head>
<body>
<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->

<header class="common_header">
    <div class="common_header">
        <div class="header_box">
            <h1><a href="/?lang=en"><img src="${pageContext.request.contextPath}/aiaas/en/images/logo_title_navy.svg" alt="maum.ai logo"></a>
                <span>Make your own AI Service</span>
            </h1>
            <!--.sta-->
            <div class="sta">
                <a href="/home/pricingPage?lang=en" class="go_price">Pricing <span>Plan</span></a>
                <!--.etcMenu-->
                <div class="etcMenu">
                    <ul>
                        <li class="lang">
                            <p class="lang_select">English<em class="fas fa-chevron-down"></em></p>
                            <ul class="lst">
                                <li><a href="/?lang=ko" target="_self"><em>한국어</em></a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <!--//.etcMenu-->
            </div>
            <!--//.sta-->
        </div>
    </div>
</header>

<!-- #container -->
<div id="container">
    <!-- .contents -->
    <div class="contents">
        <!-- .stn stn_title -->
        <div class="stn stn_title">
            <!-- .cont_box -->
            <div class="cont_box clearfix">
                <div class="img_box">
                    <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_data_labelling.png" alt="AI 데이터 라벨링 이미지">
                </div>

                <div class="txt_box">
                    <h2 class="title"><span>AI Data Labeling</span> Services</h2>
                    <h2 class="m_title"><span>AI Data<br>Labeling</span> Services</h2>

                    <h3>Experience the highest quality labeled data</h3>

                    <p>Easy to use and easily customizable tool with<br>
                        professional service offered by data scientists.</p>

                    <button class="btn_inquiry">Contact Us</button>
                </div>
            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn stn_title -->

        <!-- .stn stn_service -->
        <div class="stn stn_service">
            <!-- .cont_box -->
            <div class="cont_box clearfix">
                <h3>Data Annotation Tools and Services<br>
                    from Collection to Analysis</h3>

                <div class="cont_lst">
                    <div class="img_box">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_service_list1.png" alt="데이터 구축 리스트 이미지1">
                    </div>

                    <dl>
                        <dt>Collect, annotate, and analyze</dt>
                        <dd>From collection to analysis, we process the basics of AI training data: voice, image, video, text.</dd>
                    </dl>
                </div>

                <div class="cont_lst">
                    <div class="img_box">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_service_list2.png" alt="데이터 구축 리스트 이미지1">
                    </div>

                    <dl>
                        <dt>Efficient and effective process</dt>
                        <dd>By taking advantage of our expertise in AI platform and machine learning-based projects, we minimize time and resources on data processing.</dd>
                    </dl>
                </div>

                <div class="cont_lst">
                    <div class="img_box">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_service_list3.png" alt="데이터 구축 리스트 이미지1">
                    </div>

                    <dl>
                        <dt>Best Quality Data for AI projects</dt>
                        <dd>We analyze and select the data before the annotation to make the AI engine's best performance.</dd>
                    </dl>
                </div>
            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn stn_service -->

        <!-- .stn stn_process -->
        <div class="stn stn_process">
            <!-- .cont_box -->
            <div class="cont_box">
                <h3>Data Annotation Services</h3>

                <ol class="process_lst">
                    <li>
                        <em>1</em>
                        <span class="txt">Consultation and<br>data design with data scientists</span>
                        <span class="fas fa-angle-double-right"></span>
                    </li>
                    <li>
                        <em>2</em>
                        <span class="txt">Preprocessing of data<br>by using AI engines</span>
                        <span class="fas fa-angle-double-right"></span>
                    </li>
                    <li>
                        <em>3</em>
                        <span class="txt">Labeling and QA</span>
                        <span class="fas fa-angle-double-right"></span>
                    </li>
                    <li class="change_lst_num">
                        <em>6</em>
                        <em>4</em>
                        <span class="txt">Project completed</span>
                        <span class="txt_m">Interim review with the customer</span>
                        <span class="fas fa-angle-double-left"></span>
                    </li>
                    <li>
                        <em>5</em>
                        <span class="txt">Data design modification<br>if needed</span>
                        <span class="fas fa-angle-double-left"></span>
                    </li>
                    <li class="change_lst_num">
                        <em>4</em>
                        <em>6</em>
                        <span class="txt">Interim review with<br>the customer</span>
                        <span class="txt_m">Project completed</span>
                    </li>
                </ol>
            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn stn_process -->

        <!-- .stn stn_case -->
        <div class="stn stn_case">

            <!-- .cont_box -->
            <div class="cont_box">
                <h3>Use Cases</h3>

                <div class="cont_lst">
                    <div class="img_box">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_case_list1.png" alt="노후경유차 차량번호 인식 이미지">
                    </div>

                    <dl>
                        <dt>Detecting License Plate Number of Old Cars, Seoul Metropolitan Government</dt>
                        <dd>The AI-based vehicle detection on types and numbers of vehicles. Labeled data of license plate and the location of the plate are used.</dd>
                    </dl>
                </div>

                <div class="cont_lst">
                    <div class="img_box">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_case_list2.png" alt="노후경유차 차량번호 인식 이미지">
                    </div>

                    <dl>
                        <dt>Anomaly Detection from Surveillance Camera, Suwon Metropolitan City</dt>
                        <dd>Detects twelve types of abnormal events by labeling subjects and the motions and connecting the relationship between the two.</dd>
                    </dl>
                </div>

                <div class="cont_lst">
                    <div class="img_box">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_case_list3.png" alt="노후경유차 차량번호 인식 이미지">
                    </div>

                    <dl>
                        <dt>Data for Speech Recognition &amp; Generation</dt>
                        <dd>Written data to create various types of speech patterns and audio files for accurate transcription of speech data.</dd>
                    </dl>
                </div>
            </div>
            <!-- //.cont_box -->

        </div>
        <!-- //.stn stn_case -->

        <!-- .stn stn_inquiry -->
        <div class="stn stn_inquiry" id="inquiry">
            <!-- .cont_box -->
            <div class="cont_box">
                <div class="info_box">
                    <h3>Consult with our Data Science Team</h3>
                    <p class="detail">Please leave your contact information and inquiries.</p>
                    <p class="m_detail">Please leave your contact<br>information and inquiries.</p>
                    <p class="number">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_phone.png" alt="전화기">
                        <span>1661&minus;3222</span>
                    </p>
                    <p class="mail">
                        <img src="${pageContext.request.contextPath}/aiaas/common/images/aidata/img_mail.png" alt="메일">
                        <span>maumdata@mindslab.ai</span>
                    </p>
                </div>

                <div class="inquiry_box">
                    <%--<form>--%>
                        <div class="my_info">
                            <input type="text" id="office" placeholder="">
                            <label for="office"><span class="fas fa-building"></span>Company</label>
                        </div>

                        <div class="my_info">
                            <input type="text" id="name">
                            <label for="name"><span class="fas fa-user"></span>Name</label>
                        </div>
                        <div class="my_info">
                            <input type="email" id="mail">
                            <label for="mail"><span class="fas fa-envelope"></span>Email</label>
                        </div>
                        <div class="my_info">
                            <input type="tel" pattern="[0-9]{3}-[0-9]{4}-[0-9]{4}" id="phone" >
                            <label for="phone"><span class="fas fa-mobile-alt"></span>Phone Number</label>
                        </div>

                        <div class="my_txt">
                            <textarea id="txt" ></textarea>
                            <label for="txt"><span class="fas fa-file-alt"></span>Inquiries</label>
                        </div>

                        <button class="btn_inquiry" id="sendMailToHello_aiDataEn">Submit</button>
                    <%--</form>--%>
                </div>
            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn stn_inquiry -->
    </div>
    <!-- //.contents -->
</div>
<!-- //#container -->

<script type="text/javascript">
    $(window).load(function() {
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
    });
    $(document).ready(function() {

        $('#sendMailToHello_aiDataEn').on('click', function(){

            var title = $("#name").val() + " (" + $("#phone").val() + ") 님의 문의 사항입니다.";
            var msg = "이름 : " + $("#name").val() + "\n연락처 : " + $("#phone").val() + "\n회사 : " + $("#office").val() + "\n문의 내용 : " + $("#txt").val();

            var formData = new FormData();

            formData.append('fromaddr', $('#mail').val());
            formData.append('toaddr', 'maumdata@mindslab.ai');
            formData.append('subject', title);
            formData.append('message', msg);
            formData.append($("#key").val(), $("#value").val());

            if($('.my_info input').val()==='' || $('.my_txt textarea').val()==='') {

                alert("Please fill out all required fields.")

            }else{

                $.ajax({
                    type 	: 'POST',
                    async   : true,
                    url  	: '/support/sendContactMail',
                    dataType : 'text',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success : function(obj) {
                        $("#name").val();
                        $("#office").val();
                        $("#phone").val();
                        $("#txt").val();
                        $("#mail").val();

                        alert("Your request has been submitted. :)");
                        window.location.href = "/aiData/krAiDataLabeling";
                    },
                    error 	: function(xhr, status, error) {
                        console.log("error");
                        alert("Please resubmit your request.");
                        window.location.href = "/aiData/krAiDataLabeling";
                    }
                });
            }
        });

        // language (pc)
        $('.header_box .sta .etcMenu ul li.lang').each(function(){
            $(this).on('click',function(){
                $(this).toggleClass('active');
            });
            $('.header_box .sta .etcMenu ul li.lang ul.lst li a').on('click',function(){
                $(this).removeClass('active');
            });
            $('#container').on('click',function(){
                $('.header_box .sta .etcMenu ul li.lang').removeClass('active');
            });
        });

        // language (mobile)
        $('ul.m_etcMenu li.lang').each(function(){
            $(this).on('click',function(){
                $(this).toggleClass('active');
            });
            $('ul.m_etcMenu li.lang ul.lst li a').on('click',function(){
                $(this).removeClass('active');
            });
        });

    });

</script>

</body>
</html>
