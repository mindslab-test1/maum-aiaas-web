<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020/08/12
  Time: 4:50 오후
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>
<%--  CSS/JS 파일 캐싱 방지 --%>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>
<html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="format-detection" content="telephone=no">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
    <title>maum.ai platform</title>
    <!-- resources -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/reset.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/font.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/error.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
    <!-- //resources -->

</head>
<body>
<header>
    <a href="https://maum.ai/" title="마음에이아이" target="_self"><img src="${pageContext.request.contextPath}/aiaas/common/images/maumai.svg" alt="maum.ai logo" /></a>
</header>
<div class="content">
    <img src="${pageContext.request.contextPath}/aiaas/common/images/error.svg" alt="error Image" />
    <p id="notiMsg"> We can't sign you in with the given account. <br>Please contact: hello@mindslab.ai </p>
    <a href="/" id="homeBtn" title="home"><em class="fas fa-chevron-left"></em> Home</a>
</div>

<script>
    function back(){
        window.history.back()
    }
</script>
</body>
</html>