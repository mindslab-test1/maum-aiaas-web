<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
		
		<!-- .contents -->
		<div class="contents">
			<div class="content">
				<h1>maum VOC</h1>
				<div class="demobox">
					<p>Omni Channel AI Contact Center, maum VOC</p>
					<span class="sub">Monitor the calls and Detect risks quickly via the latest voice recognition and text analysis. Carefully listen to your customer.</span>

					<!--voc_box-->
					<div class="voc_box">
						<div class="voc_step1">
							<p class="voc_desc">Please select sample voice or upload your voice file. </p>
							<div class="btn_box">
								<button class="btn">Insurance Consultant</button>
								<button class="btn">Complain Receipt</button>
								<button class="btn">Service Join</button>
								<button class="las_btn">
									<label for="demoFile"><span>File Upload</span></label> 
									<input type="file" id="demoFile" class="demoFile"> 
								</button>
							</div>
<!--							<p class="file_desc"><span>Samples</span><b>.mp3 / 100MB 이하</b></p>-->
						</div>
						
						<div class="voc_step2">	
							<div class="txt_box">
								<div class="file_box">
									<p>insurance_consultant.mp3</p>
								</div>
								<a class="analysis">Analysis</a>	
							</div>
							<div class="voc_recod">
								<!-- recording -->
								<div class="recording">
									<span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>
									<span></span><span></span><span></span><span></span><span></span><span></span><span></span>
								</div>
								<!-- //recording -->	
							</div>
						</div>
						
						<div class="voc_step4">
							<div class="playerbox">
								<div class="player">
									<audio class="justwave" data-wave_color="#87b5d6" data-prog_color="#0c1b25" data-back_color="#C0C0C0" width="815" height="90" data-showtimes="0" data-showname="0" data-buttoncolor="#337AB7">
											<source src="${pageContext.request.contextPath}/aiaas/en/audio/aekukka.ogg" type="audio/ogg">
										  <source src="${pageContext.request.contextPath}/aiaas/en/audio/aekukka.mp3" type="audio/mpeg">
									</audio>
								</div>
								<button class="btn playbtn"></button>
								<button class="btn stopbtn"></button>
							</div>
							<div class="result_boxs">
								<input type="search" name="q" placeholder="Search in conversation" autocomplete="off" > 
								<button type="submit">Search</button>
								<ul>
									<li><p>My favorite weather is a sunny day!</p><span>0:12</span></li>
									<li class="active"><p>My favorite <strong>weather</strong> is a sunny day!vorite ddweather isd a sunnky </p><span>0:12</span></li>
									<li><p>My favorite weather is a sunny day!</p><span>0:12</span></li>
								</ul>
							</div>
							<a class="btn_backstart">Go to First Step</a>
						</div>
						
					</div>				
				</div>
				<!-- .demobox -->
			</div>
		</div>
		<!-- //.contents -->
	
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/croppie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/voc.js"></script>

