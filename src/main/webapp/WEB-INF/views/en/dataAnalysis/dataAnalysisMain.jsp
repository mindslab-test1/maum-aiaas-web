<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-03-24
  Time: 10:20
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/data_analysis.css">

<div class="contents api_content">
    <div class="content">
        <h1>Data Correlation Analysis using Feature Selection</h1>
        <ul class="menu_lst analy_lst">
            <li class="tablinks" onclick="openTap(event, 'data_demo')" id="defaultOpen"><button type="button">AI Engine</button></li>
            <li class="tablinks" onclick="openTap(event, 'data_example')"><button type="button">Use Case</button></li>
            <li class="tablinks" onclick="openTap(event, 'data_menu')"><button type="button">Manual</button></li>
<%--            <li class="tablinks"><a href="/member/enApiAccount">API ID, key</a></li>--%>
        </ul>
        <div class="demobox voicefont_demobox" id="data_demo">
            <p><span style="color:#5E77FF">Data Correlation Analysis</span> using Feature Selection</p>
            <span class="sub">Various algorithms to analyze data correlations.<br>
View the sample data below to see how AI data analysis works. Inquiry for business: hello@mindslab.ai </span>

            <!--dataAnalysis_box-->
            <div class="demo_layout dataAnalysis_box">
                <div class="data_1">
                    <p><em class="fas fa-table"></em>Two prepared data set for analysis</p>
                    <div class="csv_box">
                        <strong>Input 1: Variables definition</strong>
                        <a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vTqKRsruDmWzbHlLa3SRim29UAd70WTxS4y_t0rZIA8kbAdUZ8gcqQIOYNkufEsruBDBoukJrf7GUzS/pub?output=xlsx" download="Variables definition">.xlsx Download</a>
                        <div class="csv_file">
                            <iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vTqKRsruDmWzbHlLa3SRim29UAd70WTxS4y_t0rZIA8kbAdUZ8gcqQIOYNkufEsruBDBoukJrf7GUzS/pubhtml"></iframe>
                        </div>
                    </div>
                    <div class="csv_box">
                        <strong>Input 2: Data set</strong>
                        <a href="https://docs.google.com/spreadsheets/d/e/2PACX-1vR8AVHTZ-UG6zI7yvVbza1yXgZNbE_ONZw_fDflJsfahK9GNXp3zPF3vOnHkS1HSd9l9nPVbHNkuA_M/pub?output=xlsx" download="Data set">.xlsx Download</a>
                        <div class="csv_file">
                            <iframe src="https://docs.google.com/spreadsheets/d/e/2PACX-1vR8AVHTZ-UG6zI7yvVbza1yXgZNbE_ONZw_fDflJsfahK9GNXp3zPF3vOnHkS1HSd9l9nPVbHNkuA_M/pubhtml"></iframe>
                        </div>
                    </div>
                    <p><em class="fas fa-chart-bar"></em>Algorithms for calculating correlation</p>
                    <div class="algorithm">
                        <ul>
                            <li><em class="fas fa-check"></em> <span>F-regression</span> (Find variable's importance through F-test)</li>
                            <li><em class="fas fa-check"></em> <span>Mutual Info. Regression </span> (Calculate Mutual Info. Value to measure correlation)</li>
                            <li><em class="fas fa-check"></em> <span>Random Forest Classifier</span>  (Measure correlation using the Random Forest model)</li>
                        </ul>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="start_btn" id="">Process</button>
                    </div>
                </div>

                <!--data_2-->
                <div class="data_2">
                    <p><em class="far fa-chart-bar"></em>In progress</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#c9c3ec" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#5E77FF" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

                        <p>AI processing takes a while..</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
                    </div>


                </div>
                <!--data_2-->
                <!--data_3-->
                <div class="data_3">
                    <p><em class="far fa-chart-bar"></em>Result</p>
                    <div class="data_result">
                        <textarea id="resultTxt" placeholder="Text result"></textarea>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
                    </div>


                </div>
                <!--data_3-->

            </div>
            <!--// dataAnalysis_box-->
        </div>

        <!--.data_menu-->
        <div class="demobox analy_menu" id="data_menu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API Guideline
                    </div>
                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID &amp; Key</p>
                    <p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                    <p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                    <p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        Feature Selection
                    </div>
                    <p class="sub_txt">Various feature selection algorithms to analyze data correlations.</p>

                    <span class="sub_title">
								Preparation
					</span>
                    <p class="sub_txt">- Input: Variable definition file(.csv) & Data set(.csv)</p>
                    <ul>
                        <li>File type: .csv</li>
                        <li>Find more information on our website </li>
                    </ul>
                    <span class="sub_title">
								API Document
							</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/ocr/dataSelection</li>
                    </ul>
                    <p class="sub_txt">② Request parameters </p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API  ID. Request from is required for the ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>var_file</td>
                            <td>Variable definition (.csv) (Find more on our website)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>data_file</td>
                            <td>Dataset (.csv) (Find more on our website)</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
                        url --location --request POST 'http://api.maum.ai/ocr/dataSelection' \<br>
                        --header 'Content-Type: multipart/form-data' \<br>
                        --form 'apiId= Own API ID' \<br>
                        --form 'apiKey= Own API KEY' \<br>
                        --form 'var_file= Variable definition'\<br>
                        --form 'data_file= Dataset'
                    </div>

                    <p class="sub_txt">④ Response - example </p>

                    <div class="code_box">
<pre>
{
    "result_file":

"# Total data sample number = 99

Variable name | Type | Index | Priority | Null num | Outer num | Null+Outer | Mean | Std |

************************************************************************************************************************

X1 | number | 1 | -1 | 1 | 0 | 1 | 60.102 | 5.955 |
X2 | number | 2 | -1 | 0 | 2 | 2 | 0.055 | 0.028 |
X3 | number | 3 | -1 | 0 | 2 | 2 | 100.216 | 5.838 |
Y | number | 4 | 0 | 1 | 0 | 1 | 14.918 | 3.050 |

Processing refine_dataset_col_by_priority : 100 x 4 -> 100 x 4

Processing refine_dataset_col_by_null_replacement...

************************************************************************************************************************

# Dataset refinement process by deleting columns(s) which has null number greater than threshold, 0.50

> 0 columns detected and deleted.

> dataset dim : 99 x 4 -> 99 x 4

Processing refine_dataset_row_by_nan_replacement...

************************************************************************************************************************

# Dataset refinement process by deleting row vector in which one of them doesn't satisfy its range requirement.
> Null data in sample vector
- X1 : 1
- Y : 1
> Number type data in sample vector not satisfying range information
- X2 : 2
- X3 : 2
> String type data in sample vector not satisfying range information

Processing refine_dataset_row_by_range :
100 x 4 -> 95 x 4

************************************************************************************************************************

# Divide dataset by type, input vs output and number vs string.
> Input number features = 3
> Input string features = 0
> Output number features = 1
> Output string features = 0

************************************************************************************************************************


# Feature selection by variance threshold normalized by output variance
> Sorted variance of number type input variables
1 : X1 : 3.856313
2 : X3 : 3.738211
3 : X2 : 0.000083
---------------------------threshold : 0.000001

> num_type input dataset : 94 x 3 -> 94 x 3

************************************************************************************************************************


# Feature selection by f_regression
> Sorted variance of number type input variables
1 : X2 : 0.425449
-------------------p_value threshold : 0.500000
2 : X1 : 0.806054
3 : X3 : 0.883038

> num_type input dataset : 94 x 3 -> 94 x 3

************************************************************************************************************************


# Feature selection by mutual_info_regression
> Sorted variance of number type input variables
1 : X1 : 0.000000
2 : X2 : 0.000000
3 : X3 : 0.000000

> num_type input dataset : 94 x 3 -> 94 x 3

************************************************************************************************************************


# Feature selection by random forest classifier
> Sorted weight of number type input variables
1 : X2 : 0.452163
2 : X3 : 0.298808
3 : X1 : 0.249028
------------------mi_value threshold : 0.001000

> num_type input dataset : 94 x 3 -> 94 x 3"

}
</pre>
                    </div>
                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.data_menu-->

        <!--.data_example-->
        <div class="demobox" id="data_example">
            <p><em style="color:#5E77FF;font-weight: 400;">Use Cases</em> </p>
            <span class="sub">Find out how AI can be applied to diverse area.</span>
            <!-- 데이터 상관분석 -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>POSCO Data Analysis</span>
                            </dt>
                            <dd class="txt">Statistical characteristics are identified using data such as the amount of alloy, slag generated, the time required, and the temperature of molten steel, and determine the correlation between the X-Y factors.
                                <span><em class="fas fa-book-reader"></em> Reference: POSCO</span>
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_dataAnaly"><span>Feature Selection</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>Correlation Analysis</span>
                            </dt>
                            <dd class="txt">Analyze the correlation with the statistical characteristics of the input data and feature selection algorithm (F-regression, Mutual Info. Regression, Random Forest Classifier).</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_dataAnaly"><span>Feature Selection</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!--데이터 상관분석 -->
        </div>
        <!--//.data_example-->
    </div>

    <input type="file" id="varFile" class="demoFile" hidden>
    <input type="file" id="dataFile" class="demoFile" hidden>

</div>
<!-- .contents -->


<script type="text/javascript">
    var ajaxXHR;

    $(document).ready(function () {

        $('.start_btn').on('click', function() {
            $('.data_1').hide();
            $('.data_2').fadeIn();
            $("html").scrollTop(0);

            var formData = new FormData();
            formData.append('var_file', varFile);
            formData.append('data_file', dataFile);
            formData.append('${_csrf.parameterName}', '${_csrf.token}');

            ajaxXHR = $.ajax({
                type: "POST",
                async: true,
                url: '/api/dataAnalysis', //여기 url 추가
                data: formData,
                processData: false,
                contentType: false,
                success: function(result){

                    console.log(result);
                    $('textarea[id=resultTxt]').val(result[0].body);

                    $('.data_2').hide();
                    $('.data_3').fadeIn();
                },
                error: function(jqXHR, error){
                    if(jqXHR.status === 0){
                        return false;
                    }

                    alert("[Server Error]");
                    console.dir(error);
                    window.location.reload();
                }
            });
        });

        $('.btn_back1').on('click', function() {
            ajaxXHR.abort();
            $('.data_2').hide();
            $('.data_1').fadeIn();
            $('.data_3').hide();

        });

        initDataFile();
    });


    var varFile;
    var dataFile;

    function initDataFile() {
        // loadContent("/aiaas/common/files/data_analysis___dat_file.csv", "data_analysis___dat_file.csv", "data")
        loadContent("/aiaas/common/files/data_analysis___var_file.csv", "data_analysis___var_file.csv", "var")
        loadContent("/aiaas/common/files/data_analysis___dat_file.csv", "data_analysis___dat_file.csv", "data")
    }

    function loadContent(path, filename, target) {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", path);
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function()
        {
            blob = xhr.response;//xhr.response is now a blob object
            if(target == "var") varFile = new File([blob], filename);
            else dataFile = new File([blob], filename);
        };

        xhr.send();
    }

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
</script>