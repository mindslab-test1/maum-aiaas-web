﻿<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

		<!-- .contents -->
		<div class="contents">
			<div class="content api_content">
				<h1 class="api_tit ">Ready-made Bots</h1>
				<ul class="menu_lst bot_lst">
					<li class="tablink" onclick="openTap(event, 'chatdemo')" id="default"><button type="button">AI Engine</button></li>
					<li class="tablink" onclick="openTap(event, 'chatexample')"><button type="button">Use Case</button></li>
					<li class="tablink" onclick="openTap(event, 'chatmenu')"><button type="button">Manual</button></li>
<%--					<li class="tablink"><a href="/member/enApiAccount" >API ID, key</a></li>--%>
				</ul>

				<!-- .demobox -->
				<div class="demobox" id="chatdemo">
					<p><span style="color:#2cace5;">Machine Reading Comprehension Bot</span></p>
					<span class="sub">Reads, searches, and answers even the longest text. Now, bots read the text of the wiki and news.</span>
					<p class="temp_txt" style="color: #2cabe5;font-weight: 400;">The service is currently available only in Korean. Other languages will be available soon.</p>

					<div class="chatbot_tab">
						<button class="tablinks wiki" onclick="openCity(event, 'wiki')" id="defaultOpen">
							<img src="${pageContext.request.contextPath}/aiaas/common/images/img_chatbot_wiki.png" alt="wikibot">
							<span>Wiki Bot</span>
						</button>
						<button class="tablinks news" onclick="openCity(event, 'news')">
							<img src="${pageContext.request.contextPath}/aiaas/common/images/img_chatbot_news.png" alt="newsbot">
							<span>News Bot</span>
						</button>
<%--						<button class="tablinks weather" onclick="openCity(event, 'weather')">--%>
<%--							<img src="${pageContext.request.contextPath}/aiaas/common/images/img_chatbot_weather.png" alt="weatherbot">--%>
<%--							<span>Weather Bot</span>--%>
<%--						</button>--%>
					</div>


					<!-- chatbot_box -->
					<div class="chatbot_box" id="wiki">
						<!-- .chat_mid -->
						<div class="chat_mid">
							<div class="bot_infoBox">
								<div class="thumb"><img src="${pageContext.request.contextPath}/aiaas/common/images/img_chatbot_wiki.png" alt="wikibot"></div>
								<div class="txt">My specialty is on Wikipedia questions.<br>
									Please ask me anything.</div>
								<ul class="info_btnBox">
									<li><button type="button">2002년 월드컵 우승 국가는 어디야?</button></li>
									<li><button type="button">브라질 수도는?</button></li>
									<li><button type="button">아이폰을 만든 회사는?</button></li>
								</ul>
							</div>
							<ul class="talkLst">
								<li class="newDate">
									<span><!-- 날짜는 스크립트가 정의--></span>
								</li>

							</ul>
						</div>
						<!-- //.chat_mid -->
						<!-- .chat_btm -->
						<div class="chat_btm">
							<form id="formChat1" name="formChat1">
								<textarea class="textArea" placeholder="Type in message (Korean only)"></textarea>
								<input type="button" class="btn_chat" title="전송" value="전송">
							</form>
						</div>
						<!-- //.chat_btm -->
<%--						<div class="remark">발표: 기상청 &nbsp;&nbsp;&nbsp;제공: 공공데이터포털</div>--%>
					</div>
					<!-- //.chatbot_box -->


					<!-- chatbot_box -->
					<div class="chatbot_box" id="news">
						<!-- .chat_mid -->
						<div class="chat_mid">
							<div class="bot_infoBox">
								<div class="thumb"><img src="${pageContext.request.contextPath}/aiaas/common/images/img_chatbot_news.png" alt="newsbot"></div>
								<div class="txt">
									Ask me anything on the news.<br>
									I will search 15 million articles for you.
								</div>
								<ul class="info_btnBox">
									<li><button type="button">2018 동계올림픽 개최지역은?</button></li>
									<li><button type="button">신조어 소확행의 뜻은?</button></li>
									<li><button type="button">4월27일 판문점 평화의 집에서 열린 회의는?</button></li>
								</ul>
							</div>
							<ul class="talkLst">
								<li class="newDate">
									<span><!-- 날짜는 스크립트가 정의--></span>
								</li>
							</ul>
						</div>
						<!-- //.chat_mid -->
						<!-- .chat_btm -->
						<div class="chat_btm">
							<form id="formChat2" name="formChat2">
								<textarea class="textArea" placeholder="Type in message (Korean only)"></textarea>
								<input type="button" class="btn_chat" title="전송" value="전송">
							</form>
						</div>
						<!-- //.chat_btm -->
<%--						<div class="remark">발표: 기상청 &nbsp;&nbsp;&nbsp;제공: 공공데이터포털</div>--%>
					</div>
					<!-- //.chatbot_box -->


					<!-- chatbot_box -->
					<div class="chatbot_box" id="weather">						
						<!-- .chat_mid -->
						<div class="chat_mid">
							<div class="bot_infoBox">
								<div class="thumb"><img src="${pageContext.request.contextPath}/aiaas/common/images/img_chatbot_weather.png" alt="weatherbot"></div>
								<div class="txt">
									Feel free to ask me about today's weather.<br>
									I am happy to assist you.
								</div>
								<ul class="info_btnBox">
									<li><button type="button">오늘 날씨 알려줘</button></li>
									<li><button type="button">오늘 성남 날씨는?</button></li>
									<li><button type="button">내일 비 와?</button></li>
								</ul>
							</div>
							<ul class="talkLst">								
								<li class="newDate">
									<span><!-- 날짜는 스크립트가 정의--></span>
								</li>
							</ul>
						</div>
						<!-- //.chat_mid -->
						<!-- .chat_btm -->
						<div class="chat_btm">
							<form id="formChat3" name="formChat3">
								<textarea class="textArea" placeholder="Type in message (Korean only)"></textarea>
								<input type="button" class="btn_chat" title="전송" value="전송">
							</form>
						</div>
						<!-- //.chat_btm -->
						<div class="remark">발표: 기상청 &nbsp;&nbsp;&nbsp;제공: 공공데이터포털</div>
					</div>
					<!-- //.chatbot_box -->

				</div>
				<!-- .demobox -->



				<!--.chatmenu-->
				<div class="demobox bot_menu" id="chatmenu">
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API Guideline
							</div>
							<p class="sub_title">Set up Environment</p>
							<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
							<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
							<p class="sub_title">ID &amp; Key</p>
							<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
							<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
							<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
								<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
							<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
							<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
							<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
						</div>
						<div class="guide_group">
							<div class="title">
								IRQA (Information Retrieval Question Answering)
							</div>
							<p class="sub_txt">Answers various types of question such as Wikipedia based on world-class NLP technology.</p>

						</div>
					</div>
					<!--//.guide_box-->
				</div>
				<!--//chatmenu-->
				<!--.chatexample-->
				<div class="demobox" id="chatexample">
					<p><em style="font-weight: 400;color:#2cace5;">Use Cases</em>  </p>
					<span class="sub">Find out how AI can be applied to diverse area.</span>
					<!-- 날씨봇n위키봇 -->
					<div class="useCasesBox">
						<ul class="lst_useCases">
							<li>
								<dl>
									<dt>
										<em>CASE 01</em>
										<span>AI Assistant</span>
									</dt>
									<dd class="txt">Just attach the weather bot and the AI ​​assistant will tell you the weather.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_weatherBot"><span>Weather Bot</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>AI Tutor</span>
									</dt>
									<dd class="txt">The simplest existing knowledge communication AI software that can provide a variety of knowledge information in a conversational format and can be integrated into various fields of content.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_wikiBot"><span>Wiki Bot</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 03</em>
										<span>Scholarship Quiz Winner</span>
									</dt>
									<dd class="txt">More than 13 million unstructured knowledge bases and 38 million learning data it provides accurate answers.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_wikiBot"><span>Wiki Bot</span></li>
										</ul>
									</dd>
								</dl>
							</li>
						</ul>
					</div>
					<!-- //날씨봇n위키봇 -->
				</div>


			</div>
		</div>
		<!-- //.contents -->

<input type="hidden" id="AUTH_ID">
<input type="hidden" id="SESSION_ID">
<input type="hidden" id="thumb">
<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">

<%--<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/chatbot_old.js?v=20190902"></script>--%> <%-- 추후에 문제 없을 시 제거 [작성:2021.01.07 YGE]--%>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/chatbot/app.js?v=20200106"></script>
<script type="text/javascript">

	/* news봇 추가, wiki봇 변경으로 weather봇만 기존 코드를 사용하고 나머지는 새로 추가한 코드 사용
	*  공통 코드 및 news,wiki봇 코드는 여기에 있고, weather봇용 코드는 /aiaas/kr/js/chatbot/app.js(영문과 공용) 에 있음.
	* -- 2021.01.06 YGE
	*/

	var chatName = '';

	chatSingIn();
	document.getElementById("defaultOpen").click();
	document.getElementById("default").click();

	//	챗봇 탭 메뉴
	function openCity(evt, botName) {
		let i, tabContent, tabLinks;
		tabContent = document.getElementsByClassName("chatbot_box");
		for (i = 0; i < tabContent.length; i++) {
			tabContent[i].style.display = "none";
		}
		tabLinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tabLinks.length; i++) {
			tabLinks[i].className = tabLinks[i].className.replace(" active", "");
		}
		document.getElementById(botName).style.display = "block";
		evt.currentTarget.className += " active";
		chatName = botName;

		if($('#'+chatName+' .chat_mid .talkLst li').length===1){
			$('#'+chatName+' .bot_infoBox').css({
				'display':'block'
			});
			$('#'+chatName+' .talkLst').css({
				'display':'none'
			});
		}

		switch (botName){
			case "wiki":
				document.getElementById('thumb').value='/aiaas/common/images/img_chatbot_wiki.png';
				break;
			case "news":
				document.getElementById('thumb').value='/aiaas/common/images/img_chatbot_news.png';
				break;
			case "weather":
				document.getElementById('thumb').value='/aiaas/common/images/img_chatbot_weather.png';
				chatOpen("Weather");
				break;
		}
	}


	function getAmPm(){
		return new Date().getHours() >= 12 ? '오후' : '오전';
	}
	function getTime(){
		let	thisHours = new Date().getHours() >=13 ?  new Date().getHours()-12 : new Date().getHours(); //현재 시
		let	thisMinutes = new Date().getMinutes() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes(); //현재 분
		return thisHours + ':' + thisMinutes;
	}

	// chat_mid height값 조정
	function changeChatMidHeight() {
		let winWidth = $(window).width();
		let cahtbotWrapHeight = $('#cahtbotWrap').height();
		let minusHeight = (winWidth < 760 ? 130 : 145);
		$('#cahtbotWrap').each(function () {
			$('.chatbot_box .chat_mid').css({'height': Math.round(cahtbotWrapHeight - minusHeight),});
		});
	}

	function appendAnswerTalk(result){
		$('#'+chatName+' .chat_mid .talkLst').append(
				'<li class="bot">'+
				'<span class="thumb"><img src="' + document.getElementById('thumb').value + '"></span>'+
				'<span class="cont"> '+
				'<em class="txt">' + result + '</em> '+
				'</span> '+
				'</li>'
		);
	}


	function appendTalkTime(){
		$('#'+chatName+' .chat_mid .talkLst .bot:last-child .cont:last-child').append(
				'<em class="date"><strong>' + getAmPm() + '</strong>' + getTime() + '</em>'
		);
	}

	// textArea disabled 해제 & 로딩 UI 제거
	function endTalkLoadingUI(){
		$('#'+chatName+' .chat_btm .textArea').prop('disabled', false);
		$('#'+chatName+' .chat_mid .talkLst li.bot:last-child').remove();
	}



	$(document).ready(function () {
		// 날짜, 요일 시간 정의
		let year = new Date().getFullYear();  //현재 년도
		let month = new Date().getMonth() + 1;  //현재 월
		let date = new Date().getDate();  //현재 일
		let week = ['일요일', '월요일', '화요일', '수요일', '목요일', '금요일', '토요일'];	  //요일 정의
		let thisWeek = week[new Date().getDay()];	//현재 요일


		// 오늘 날짜 입력
		$('.talkLst li.newDate span').each(function () {
			$(this).append(year + '년 ' + month + '월 ' + date + '일 ' + thisWeek);
		});

		// 첫멘트 시간
		$('.chatbot_box .chat_mid .talkLst li.bot .cont:last-child').append('<em class="date"><strong>' + getAmPm() + '</strong>' + getTime() + '</em>');

		// 내용있을 시 스크롤 최하단
		$('.chatbot_box .chat_mid').scrollTop($('.chatbot_box .chat_mid')[0].scrollHeight);

		// 채팅입력 (Enter)
		$('.chatbot_box .chat_btm .textArea').keyup(function (event) {
			if (event.keyCode === 13) {
				$('#'+chatName+' .btn_chat').trigger('click');
			}
		});



		// 추천질문 (text 출력)
		$('.info_btnBox li button').on('click', function () {
			var recomQust = $(this).text();

			$('.chatbot_box .chat_btm .textArea').val(recomQust);
			$('#'+chatName+' .btn_chat').trigger('click');

			$('.chatbot_box .bot_infoBox').css({'display': 'none'});
			$('.chatbot_box .talkLst').css({'display': 'block'});

			changeChatMidHeight();

			$('.chatbot_box .chat_mid').scrollTop($('.chatbot_box .chat_mid')[0].scrollHeight);
			$('.chatbot_box .chat_btm .textArea').val('');

		});

		// 채팅입력 (text 출력)
		$('.btn_chat').on('click', function() {
			let $chatTextArea = $('#'+chatName+' .chat_btm .textArea');

			// textarea 텍스트 값 및 엔터처리
			let textValue = $chatTextArea.val().trim().replace(/(?:\r\n|\r|\n)/g, '<br>');

			$chatTextArea.prop('disabled', true);

			if( $chatTextArea.val().replace(/\s/g,"").length === 0){
				$('#'+chatName+' .chat_btm .textArea').val('');

				return;
			}

			// 채팅창에 text 출력
			$('#'+chatName+' .chat_mid .talkLst').append(
					'<li class="user"> '+
					'<span class="cont"> '+
					'<em class="txt">' + textValue + '</em> '+
					'<em class="date"><strong>' + getAmPm() + '</strong>' + getTime() + '</em> '+
					'</span> '+
					'</li>'
			);

			//로딩 UI 추가
			$('#'+chatName+' .chat_mid .talkLst').append(
					'<li class="bot">'+
					'<span class="thumb"><img src="' + document.getElementById('thumb').value + '" alt="chatbot_img"></span>'+
					'<span class="cont">'+
					'<em class="txt">'+
					'<span class="chatLoading">'+
					'<strong class="chatLoading_item01"></strong>'+
					'<strong class="chatLoading_item02"></strong>'+
					'<strong class="chatLoading_item03"></strong>'+
					'</span>'+
					'</em> '+
					'</span> '+
					'</li>'
			);

			if(chatName === "weather"){
				sendTalk(textValue);
			}else{
				sendSimpleQaReq(textValue);
			}


			$('.chatbot_box .bot_infoBox').css({ 'display':'none' });
			$('.chatbot_box .talkLst').css({ 'display':'block' });

			$('#'+chatName+' .chat_btm .textArea').val('');

			changeChatMidHeight();
			$('#'+chatName+' .chat_mid').scrollTop($('#'+chatName+' .chat_mid')[0].scrollHeight);

		});

	});

	//---------------------------------- news, wiki봇용 ---------------------------------------//
	function sendSimpleQaReq(talkMessage){
		let data = {
			"question": talkMessage,
			"ntop": 1,
			"domain": chatName,
			"${_csrf.parameterName}" : "${_csrf.token}"
		};

		$.ajax({
			url		: "/api/chat/simpleQA",
			data    : data,
			type	: "post",
			success : function(response) {

				if(response === null || response === ""){
					endTalkLoadingUI();
					console.log("chatbot SendTalk error! ");
					alert("Failed to get response from server.\nPlease try again");
					return;
				}

				console.log(response);

				// textArea disabled 해제 & 로딩 UI 제거
				endTalkLoadingUI();

				let answer = response.payload.answer;
				appendAnswerTalk(answer);
				appendTalkTime();

				$('#'+chatName+' .chat_mid').scrollTop($('#'+chatName+' .chat_mid')[0].scrollHeight);
			},
			error : function(xhr) {
				endTalkLoadingUI();
				console.log("chatbot SendTalk error! ", xhr);
				alert("Failed to get response from server.\nPlease try again");
			}

		});
	}

	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablink");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}

</script>