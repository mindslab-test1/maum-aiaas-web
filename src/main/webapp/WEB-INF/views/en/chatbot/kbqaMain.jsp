<%--
  Created by IntelliJ IDEA.
  User: YeJun Lee
  Date: 2021-04-23
  Time: 오후 2:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<html>
<head>
	<title>maum.ai platform</title>

	<!-- -------------------- General Resources -------------------- -->
	<!-- General CSS -->
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/common/css/all.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/reset.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/font.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/pop_common.css">
	<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/common.css">

	<!-- General Script -->
	<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/jquery-1.11.0.min.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/common.js"></script>
	<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/jsrender.min.js"></script>

</head>
<body>

<div class="contents">
	<!-- .content -->
	<div class="content api_content">
		<h1 class="api_tit ">KBQA Bot</h1>
		<ul class="menu_lst bot_lst">
			<li class="tablinks" onclick="openTap(event, 'chatdemo')" id="defaultOpen">
				<button type="button">AI Engine</button>
			</li>
			<li class="tablinks" onclick="openTap(event, 'chatexample')">
				<button type="button">Use Case</button>
			</li>
			<li class="tablinks" onclick="openTap(event, 'chatmenu')">
				<button type="button">Manual</button>
			</li>
		</ul>

		<!-- .demobox -->
		<div class="demobox" id="chatdemo">
			<p><span style="color:#2cace5;">KBQA(Knowledge Base Question Answering)</span> Wiki Bot</p>
			<span class="sub">Infobox for Wikipedia is searched and answered using KBQA engine.</span>
			<p class="temp_txt" style="color: #2cabe5;font-weight: 400;">The service is currently available only in Korean. Other languages will be available soon.</p>

			<!-- chatbot_box -->
			<div class="chatbot_box" id="wiki">
				<!-- .chat_mid -->
				<div class="chat_mid">
					<div class="bot_infoBox">
						<div class="thumb"><img src="${pageContext.request.contextPath}/aiaas/common/images/img_chatbot_wiki.png" alt="chatbot"></div>
						<div class="txt">
							My specialty is on Wikipedia questions.<br>
							Please ask me anything.
						</div>
						<ul class="info_btnBox">
							<li><button type="button">우리나라 국보 1호는?</button></li>
							<li><button type="button">세종대왕의 아버지는?</button></li>
							<li><button type="button">호메로스의 주요 작품은?</button></li>
						</ul>
					</div>
					<ul class="talkLst">
						<li class="newDate">
							<span><!-- 날짜는 스크립트가 정의--></span>
						</li>
					</ul>
				</div>
				<!-- //.chat_mid -->
				<!-- .chat_btm -->
				<div class="chat_btm">
					<form method="post" action="" id="formChat1" name="formChat1">
						<textarea class="textArea" placeholder="Type in message (Korean only)"></textarea>
						<input type="button" name="btn_chat2" id="btn_chat2" class="btn_chat" title="전송" value="전송">
					</form>
				</div>
				<!-- //.chat_btm -->
				<div class="remark">발표: 기상청 &nbsp;&nbsp;&nbsp;제공: 공공데이터포털</div>
			</div>
			<!-- //.chatbot_box -->
		</div>
		<!-- .demobox -->

		<!--.chatmenu-->
		<div class="demobox bot_menu" id="chatmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">API Guideline</div>

					<p class="sub_title">Set up Environment</p>
					<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
					<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
					<p class="sub_title">ID & Key</p>
					<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
					<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
					<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
						<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
					<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
					<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
					<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
				</div>
				<div class="guide_group">
					<div class="title">
						KBQA (Knowledge Base Question Answering)
					</div>
					<p class="sub_txt">Answers various types of question such as Wikipedia based on world-class NLP technology.</p>

				</div>
			</div>
			<!--//.guide_box-->
		</div>
		<!--//chatmenu-->

		<!--.chatexample-->
		<div class="demobox" id="chatexample">
			<p><em style="color:#2cace5;font-weight: 400;">Use Cases</em></p>
			<span class="sub">Find out how AI can be applied to diverse area.</span>
			<!-- 날씨봇n위키봇 -->
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>AI Tutor</span>
							</dt>
							<dd class="txt">The simplest existing knowledge communication AI software that can provide a variety of knowledge information in a conversational format and can be integrated into various fields of content.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_wikiBot"><span>Wiki Bot</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //chatexample -->
		</div>
		<!--//.avrexample-->
	</div>
	<!-- //.content -->
</div>
<!-- //.contents -->

<input type="hidden" id="AUTH_ID">
<input type="hidden" id="SESSION_ID">
<input type="hidden" id="thumb">
<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">

<!-- Local Script -->
<script type="text/javascript">

	$(document).ready(function () {
		// 날짜, 요일 시간 정의
		let year = new Date().getFullYear();  //현재 년도
		let month = new Date().getMonth() + 1;  //현재 월
		let date = new Date().getDate();  //현재 일
		let week = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];	  //요일 정의
		let thisWeek = week[new Date().getDay()];	//현재 요일


		// 오늘 날짜 입력
		$('.talkLst li.newDate span').each(function () {
			$(this).append(year + '년 ' + month + '월 ' + date + '일 ' + thisWeek);
		});

		// 첫멘트 시간
		$('.chatbot_box .chat_mid .talkLst li.bot .cont:last-child').append('<em class="date"><strong>' + getAmPm() + '</strong>' + getTime() + '</em>');

		// 내용있을 시 스크롤 최하단
		$('.chatbot_box .chat_mid').scrollTop($('.chatbot_box .chat_mid')[0].scrollHeight);

		// 채팅입력 (Enter)
		$('.chatbot_box .chat_btm .textArea').keyup(function (event) {
			if (event.keyCode === 13) {
				$('.btn_chat').trigger('click');
			}
		});

		// 추천질문 (text 출력)
		$('.info_btnBox li button').on('click', function () {
			var recomQust = $(this).text();

			$('.chatbot_box .chat_btm .textArea').val(recomQust);
			$('.btn_chat').trigger('click');

			$('.chatbot_box .bot_infoBox').css({'display': 'none'});
			$('.chatbot_box .talkLst').css({'display': 'block'});

			//changeChatMidHeight();

			$('.chatbot_box .chat_mid').scrollTop($('.chatbot_box .chat_mid')[0].scrollHeight);
			$('.chatbot_box .chat_btm .textArea').val('');

		});


		// 채팅입력 (text 출력)
		$('.btn_chat').on('click', function () {
			let $chatTextArea = $('.chat_btm .textArea');

			// textarea 텍스트 값 및 엔터처리
			let textValue = $chatTextArea.val().trim().replace(/(?:\r\n|\r|\n)/g, '<br>');

			$chatTextArea.prop('disabled', true);

			if ($chatTextArea.val().replace(/\s/g, "").length === 0) {
				$('.chat_btm .textArea').val('');

				return;
			}

			// 채팅창에 text 출력
			$('.chat_mid .talkLst').append(
					'<li class="user"> ' +
					'<span class="cont"> ' +
					'<em class="txt">' + textValue + '</em> ' +
					'<em class="date"><strong>' + getAmPm() + '</strong>' + getTime() + '</em> ' +
					'</span> ' +
					'</li>'
			);

			//로딩 UI 추가
			$('.chat_mid .talkLst').append(
					'<li class="bot">' +
					'<span class="thumb"><img src="' + document.getElementById('thumb').value + '" alt="chatbot_img"></span>' +
					'<span class="cont">' +
					'<em class="txt">' +
					'<span class="chatLoading">' +
					'<strong class="chatLoading_item01"></strong>' +
					'<strong class="chatLoading_item02"></strong>' +
					'<strong class="chatLoading_item03"></strong>' +
					'</span>' +
					'</em> ' +
					'</span> ' +
					'</li>'
			);

			sendApiRequest('kor', textValue);

			$('.chatbot_box .bot_infoBox').css({'display': 'none'});
			$('.chatbot_box .talkLst').css({'display': 'block'});

			$('.chat_btm .textArea').val('');

			//changeChatMidHeight();
			$('.chat_mid').scrollTop($('.chat_mid')[0].scrollHeight);

		});
	});

	function getAmPm(){
		return new Date().getHours() >= 12 ? 'PM' : 'AM';
	}
	function getTime(){
		let	thisHours = new Date().getHours() >=13 ?  new Date().getHours()-12 : new Date().getHours(); //현재 시
		let	thisMinutes = new Date().getMinutes() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes(); //현재 분
		return thisHours + ':' + thisMinutes;
	}

	// chat_mid height값 조정
	function changeChatMidHeight() {
		let winWidth = $(window).width();
		let cahtbotWrapHeight = $('#cahtbotWrap').height();
		let minusHeight = (winWidth < 760 ? 130 : 145);
		$('#cahtbotWrap').each(function () {
			$('.chatbot_box .chat_mid').css({'height': Math.round(cahtbotWrapHeight - minusHeight),});
		});
	}

	function appendAnswerTalk(result){
		$('.chat_mid .talkLst').append(
				'<li class="bot">'+
				'<span class="thumb"><img src="/aiaas/common/images/img_chatbot_wiki.png"></span>'+
				'<span class="cont"> '+
				'<em class="txt">' + result + '</em> '+
				'</span> '+
				'</li>'
		);
	}


	function appendTalkTime(){
		$('.chat_mid .talkLst .bot:last-child .cont:last-child').append(
				'<em class="date"><strong>' + getAmPm() + '</strong>' + getTime() + '</em>'
		);
	}

	// textArea disabled 해제 & 로딩 UI 제거
	function endTalkLoadingUI(){
		$('.chat_btm .textArea').attr('disabled', false);
		$('.chat_mid .talkLst li.bot:last-child').remove();
	}


	/* API 요청 ----------------------------------------------------------------------------------------------------- */
	function sendApiRequest(lang, question) {

		let data = {
			"lang": lang,
			"question": question,
			"${_csrf.parameterName}" : "${_csrf.token}"
		};

		$.ajax({
			url		: "/api/chat/kbqa",
			data    : data,
			type	: "post",
			success : function(response) {

				if(response === null || response === ""){
					endTalkLoadingUI();
					console.log("chatbot SendTalk error! ");
					alert("Failed to get response from server.\nPlease try again");
					return;
				}

				//console.log(response);

				// textArea disabled 해제 & 로딩 UI 제거
				endTalkLoadingUI();

				let answer = "";
				if(response.payload == null) {
					answer = "답변을 찾을 수 없습니다.";
				} else {
					answer = response.payload;
				}

				appendAnswerTalk(answer);
				appendTalkTime();

				$('.chat_mid').scrollTop($('.chat_mid')[0].scrollHeight);
			},
			error : function(xhr) {
				endTalkLoadingUI();
				console.log("chatbot SendTalk error! ", xhr);
				alert("Failed to get response from server.\nPlease try again");
			}
		});
	}

	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}

	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();
</script>
</body>
</html>
