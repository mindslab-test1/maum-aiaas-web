﻿<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/idr/croppie.css">


<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">

<!-- 1 .pop_common -->
<div class="pop_common sample_pop">
	<!-- .pop_bg 팝업 바탕 어두운 색 -->
	<div class="pop_bg"></div>
	<!-- //.pop_bg 팝업 바탕 어두운 색 -->

	<!-- .popWrap -->
	<div class="popWrap">
		<!-- 4 .pop_2btn 버튼 두개 팝업 -->
		<div class="pop_2btn">
			<div class="pop_bg2"></div>
			<!-- .popWrap -->
			<div class="popWrap">
				<em class="fas fa-times minipop_close"></em>
				<!-- .pop_bd -->
				<div class="pop_bd">
					<em class="fas fa-money-bill"></em>
					<p>Selected</p>
					<span>Proceed the test with the <strong>selected sample</strong> file?
			            </span>
				</div>
				<!-- //.pop_bd -->
				<div class="btn">
					<button class="btn_gray">No</button>
					<button class="btn_blue" id="">Yes</button>
				</div>
			</div>
			<!-- //.popWrap -->
		</div>
		<!-- //.pop_2btn -->

		<!-- .pop_hd -->
		<div class="pop_hd">
			<h3><em class="far fa-file-image"></em>&nbsp; Sample</h3>
			<em class="fas fa-times pop_sample_close"></em>
		</div>
		<!-- //.pop_hd -->
		<!-- .pop_bd -->
		<div class="pop_bd">

			<div class="country">
				<p class="p_kr"><img src="${pageContext.request.contextPath}/aiaas/common/images/img_kr.svg"  alt="South Korean flag"/> Korea</p>
				<div class="option_box">
					<div class="radio">
						<input type="radio" id="sample_kr1" name="option" value="sample_1" checked>
						<label for="sample_kr1" class="">
							<em class="img_area">
								<img id="sample_1" src="${pageContext.request.contextPath}/aiaas/common/images/img_ko1.png" alt="1000won" />
							</em>
						</label>
					</div>
					<div class="radio">
						<input type="radio" id="sample_kr2" name="option" value="sample_2" >
						<label for="sample_kr2" class="">
							<em class="img_area">
								<img id="sample_2" src="${pageContext.request.contextPath}/aiaas/common/images/img_ko2.png" alt="5000won" />
							</em>
						</label>
					</div>
					<div class="radio">
						<input type="radio" id="sample_kr3" name="option" value="sample_3" >
						<label for="sample_kr3" class="">
							<em class="img_area">
								<img id="sample_3" src="${pageContext.request.contextPath}/aiaas/common/images/img_ko3.png" alt="10000won" />
							</em>
						</label>
					</div>
					<div class="radio">
						<input type="radio" id="sample_kr4" name="option" value="sample_4" >
						<label for="sample_kr4" class="">
							<em class="img_area">
								<img id="sample_4" src="${pageContext.request.contextPath}/aiaas/common/images/img_ko4.png" alt="50000won" />
							</em>
						</label>
					</div>
				</div>
			</div>
			<div class="country">
				<p><img src="${pageContext.request.contextPath}/aiaas/common/images/img_usd.svg"  alt="American flag"/> United States of America</p>
				<div class="option_box">
					<div class="radio">
						<input type="radio" id="sample_us5" name="option" value="sample_5" checked>
						<label for="sample_us5" class="">
							<em class="img_area">
								<img id="sample_5" src="${pageContext.request.contextPath}/aiaas/common/images/img_usd1.png" alt="1 dollar" />
							</em>
						</label>
					</div>
					<div class="radio">
						<input type="radio" id="sample_us6" name="option" value="sample_6" >
						<label for="sample_us6" class="">
							<em class="img_area">
								<img id="sample_6" src="${pageContext.request.contextPath}/aiaas/common/images/img_usd2.png" alt="2 dollar" />
							</em>
						</label>
					</div>
					<div class="radio">
						<input type="radio" id="sample_us7" name="option" value="sample_7" >
						<label for="sample_us7" class="">
							<em class="img_area">
								<img id="sample_7" src="${pageContext.request.contextPath}/aiaas/common/images/img_usd3.png" alt="5 dollar" />
							</em>
						</label>
					</div>
					<div class="radio">
						<input type="radio" id="sample_us8" name="option" value="sample_8" >
						<label for="sample_us8" class="">
							<em class="img_area">
								<img id="sample_8" src="${pageContext.request.contextPath}/aiaas/common/images/img_usd4.png" alt="20 dollar" />
							</em>
						</label>
					</div>
				</div>
			</div>
			<div class="country">
				<p><img src="${pageContext.request.contextPath}/aiaas/common/images/img_eu.svg"  alt="Europe flag"/>  Europe</p>
				<div class="option_box">
					<div class="radio">
						<input type="radio" id="sample_eu9" name="option" value="sample_9" checked>
						<label for="sample_eu9" class="">
							<em class="img_area">
								<img id="sample_9" src="${pageContext.request.contextPath}/aiaas/common/images/img_eur1.png" alt="20유로" />
							</em>
						</label>
					</div>
					<div class="radio">
						<input type="radio" id="sample_eu10" name="option" value="sample_10" >
						<label for="sample_eu10" class="">
							<em class="img_area">
								<img id="sample_10" src="${pageContext.request.contextPath}/aiaas/common/images/img_eur2.png" alt="100유로" />
							</em>
						</label>
					</div>
				</div>
			</div>
			<div class="country">
				<p><img src="${pageContext.request.contextPath}/aiaas/common/images/img_cn.svg"  alt="China flag"/> China</p>
				<div class="option_box">
					<div class="radio">
						<input type="radio" id="sample_cn11" name="option" value="sample_11" checked>
						<label for="sample_cn11" class="">
							<em class="img_area">
								<img id="sample_11" src="${pageContext.request.contextPath}/aiaas/common/images/img_cny1.png" alt="10 yuan" />
							</em>
						</label>
					</div>
					<div class="radio">
						<input type="radio" id="sample_cn12" name="option" value="sample_12" >
						<label for="sample_cn12" class="">
							<em class="img_area">
								<img id="sample_12" src="${pageContext.request.contextPath}/aiaas/common/images/img_cny2.png" alt="20 yuan" />
							</em>
						</label>
					</div>
					<div class="radio">
						<input type="radio" id="sample_cn13" name="option" value="sample_13" >
						<label for="sample_cn13" class="">
							<em class="img_area">
								<img id="sample_13" src="${pageContext.request.contextPath}/aiaas/common/images/img_cny3.png" alt="50 yuan" />
							</em>
						</label>
					</div>

				</div>
			</div>
			<div class="country">
				<p><img src="${pageContext.request.contextPath}/aiaas/common/images/img_jp.svg" alt="Japan flag"/>  Japan</p>
				<div class="option_box">
					<div class="radio">
						<input type="radio" id="sample_jp14" name="option" value="sample_14" checked>
						<label for="sample_jp14" class="">
							<em class="img_area">
								<img id="sample_14" src="${pageContext.request.contextPath}/aiaas/common/images/img_jp1.png" alt="5000 yen" />
							</em>
						</label>
					</div>
					<div class="radio">
						<input type="radio" id="sample_jp15" name="option" value="sample_15" >
						<label for="sample_jp15" class="">
							<em class="img_area">
								<img id="sample_15" src="${pageContext.request.contextPath}/aiaas/common/images/img_jp2.png" alt="10000 yen" />
							</em>
						</label>
					</div>


				</div>
			</div>
		</div>
		<!-- //.pop_bd -->
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_common -->

<!-- 1 .pop_common -->
<div class="pop_common samplebill_pop">
	<!-- .pop_bg 팝업 바탕 어두운 색 -->
	<div class="pop_bg"></div>
	<!-- //.pop_bg 팝업 바탕 어두운 색 -->

	<!-- .popWrap -->
	<div class="popWrap">
		<!-- .pop_hd -->
		<div class="pop_hd">
			<h3><em class="far fa-file-image"></em>&nbsp; Sample</h3>
			<em class="fas fa-times pop_sample_close"></em>
		</div>
		<!-- //.pop_hd -->
		<!-- .pop_bd -->
		<div class="pop_bd">
			<img id="samplebill" class="bill_img" src="${pageContext.request.contextPath}/aiaas/common/images/sample_bill.png" alt="bill sample image" />
		</div>
		<!-- //.pop_bd -->
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_common -->

<!-- 2 .pop_common -->
<div class="pop_common samplebill_pop" id="hospital_pop">
	<!-- .pop_bg 팝업 바탕 어두운 색 -->
	<div class="pop_bg"></div>
	<!-- //.pop_bg 팝업 바탕 어두운 색 -->

	<!-- .popWrap -->
	<div class="popWrap">
		<!-- .pop_hd -->
		<div class="pop_hd">
			<h3 id="hospital_origin"><em class="far fa-file-image"></em>&nbsp;Billing Calculator. Sample Medical Bill.</h3>
			<em class="fas fa-times pop_sample_close"></em>
		</div>
		<!-- //.pop_hd -->
		<!-- .pop_bd -->
		<div class="pop_bd">
			<img id="samplem_edical" class="bill_img" src="${pageContext.request.contextPath}/aiaas/common/images/hospital-sample.jpg" alt="Billing Calculator. Sample Medical Bill." />
		</div>
		<!-- //.pop_bd -->
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_common -->

<!-- 3 .pop_common -->
<div class="pop_common upload_popup">
	<!-- .pop_bg 팝업 바탕 어두운 색 -->
	<div class="pop_bg"></div>
	<!-- //.pop_bg 팝업 바탕 어두운 색 -->
	<!-- .popWrap -->
	<div class="popWrap">
		<!-- .pop_hd -->
		<div class="pop_hd">
			<h3><em class="far fa-file-image"></em> Upload Image</h3>
			<em class="fas fa-times pop_sample_close"></em>
		</div>
		<!-- //.pop_hd -->
		<!-- .pop_bd -->
		<div class="pop_bd">
			<p>Select image type and upload</p>
			<div class="option_box">
				<div class="radio">
					<input type="radio" id="opt1" name="pop_option" value="Currency" checked>
					<label for="opt1" class="">
						<span>Currency</span>
					</label>
				</div>
				<div class="radio">
					<input type="radio" id="opt2" name="pop_option" value="Billing">
					<label for="opt2" class="">
						<span>Billing Statement</span>
					</label>
				</div>
				<div class="radio">
					<input type="radio" id="opt3" name="pop_option" value="MedicalBill">
					<label for="opt3" class="">
						<span>Medical Bill</span>
					</label>
				</div>
				<div class="btn" id="uploadFile">
					<em class="fas fa-times hidden close"></em>
					<em class="far fa-file-image hidden"></em>
					<label for="demoFile" class="demolabel">Choose file</label>
					<input type="file" id="demoFile" class="demoFile" accept=".jpg, .png" >
				</div>
			</div>
			<div class="btn_area">
				<button type="button" class="btn_start editImgBtn" id="editImgBtn2">Edit Image</button>
			</div>
		</div>
		<!-- //.pop_bd -->
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_common -->
		<!-- .contents -->
		<div class="contents">
			<div class="content api_content">
				<h1 class="api_tit">Document Recognition</h1>
				<ul class="menu_lst vision_lst">
					<li class="tablinks" onclick="openTap(event, 'recogdemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
					<li class="tablinks" onclick="openTap(event, 'recogexample')"><button type="button">Use Case</button></li>
					<li class="tablinks" onclick="openTap(event, 'recogmenu')"><button type="button">Manual</button></li>
<%--					<li class="tablinks"><a href="/member/enApiAccount">API ID, key</a></li>--%>
				</ul>
				<div class="demobox" id="recogdemo">
					<p><span>DIARL</span> <small>(Document Image Analysis, Recognition and Learning)</small></p>
					<span class="sub">Extracts information from money or billing statements.</span>
					<!--이미지인식box-->
					<div class="demo_layout pattern_box">
						<div class="pattern_1" >
							<div class="fl_box" id="opacity">
								<p><em class="far fa-file-image"></em><strong>Use Sample File</strong></p>
								<div class="sample_box">
									<div class="sample_1">
										<div class="radio">
											<input type="radio" id="sample1" name="sample" value="Currency" checked>
											<label for="sample1" class="">
												<em class="img_area">
													<img id="sampleImg_1" src="${pageContext.request.contextPath}/aiaas/kr/images/img_diarl_sample1.jpg" alt="sample img 1" />
												</em>
												<span>Money</span>
											</label>
											<button class="sample_more"><em class="fas fa-plus-circle"></em>&nbsp; More</button>
										</div>
									</div>
									<div class="sample_2">
										<div class="radio">
											<input type="radio" id="sample2" name="sample" value="Billing" >
											<label for="sample2"  class="">
												<em class="img_area bill_area">
													<img id="sampleImg_2" class="bill_img" src="${pageContext.request.contextPath}/aiaas/common/images/img_diarl_sample2.png" alt="sample img 1" />
													<strong class="bill_detail"><em class="fas fa-search"></em> See more</strong>
												</em>
												<span>Bill</span>
											</label>
											<span class="bill_desc">Click for more details</span>
										</div>
									</div>
									<div class="sample_3">
										<div class="radio">
											<input type="radio" id="sample3" name="sample" value="MedicalBill" >
											<label for="sample3"  class="">
												<em class="img_area bill_area">
													<img id="sampleImg_3" class="bill_img" src="${pageContext.request.contextPath}/aiaas/common/images/hospital-sample.jpg" alt="진료비 영수증 샘플 이미지" />
													<strong class="hospital_detail"><em class="fas fa-search"></em> See more</strong>
												</em>
												<span>Major hospitals' medical bill and receipt (KR)
                                        </span>
											</label>
											<span class="bill_desc">Click for more details</span>
										</div>
									</div>
								</div>
							</div>
							<div class="fr_box">
								<p><em class="far fa-file-image"></em><strong>Use My File</strong></p>
								<div class="uplode_box">
									<button type="button" class="upload_pop">Upload Image</button>
									<%--                            <div class="btn" id="uploadFile">--%>
									<%--                                <em class="fas fa-times hidden close"></em>--%>
									<%--                                <em class="far fa-file-image hidden"></em>--%>
									<%--                                <label for="demoFile" class="demolabel">이미지 업로드</label>--%>
									<%--                                <input type="file" id="demoFile" class="demoFile" accept=".jpg, .png" >--%>
									<%--                            </div>--%>
									<p class="img_desc"><em class="fas fa-exclamation"></em> Notice for file upload</p>
									<div>
										<span><em>Money</em> : 5 currencies (Won, Yuan, Yen, Euro, Dollar) from photos taken directly. </span>
										<span><em>Bills</em> : Utilies, phone bills, insurance, etc. </span>
										<span><em>Medical bill and receipt </em> : Format as used in major hospitals in South Korea</span>
									</div>

									<ul>
										<li>* Supported files: .jpg, .png</li>
										<li>* Image file size under 2MB</li>
									</ul>
								</div>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_start" id="editImgBtn">Next</button>
							</div>
						</div>
						<!--pattern_2-->
						<div class="pattern_2" >
							<p><em class="far fa-file-image"></em>Edit Image</p>
							<p class="desc">Please fit the image within the yellow top left corner.</p>
							<div class="img_box">
								<em class="fas fa-minus minus"></em>
								<em class="fas fa-plus plus"></em>
								<!--불러온 이미지 들어갈 곳-->
								<img src="" alt="불러온 이미지" id="previewImg">
								<div id="rotate_left" class="redobox">
									<em class="fas fa-undo-alt"></em>
									<span>Rotation</span>
								</div>
							</div>
							<p class="desctxt">Click and move your mouse to resize or rotate the image.</p>
							<span>Eg) Please position it like the image below</span>
							<div class="bill_sample">
								<img src="${pageContext.request.contextPath}/aiaas/common/images/img_cropsample_big.png" alt="bill_sample" />
							</div>
							<div class="hospital_sample">
								<img src="${pageContext.request.contextPath}/aiaas/common/images/img_cropsample_medical.png" alt="hospital_sample" />
							</div>
							<div class="money_sample">
								<img src="${pageContext.request.contextPath}/aiaas/common/images/img_cropsample_money.png" alt="money_sample" />
							</div>
							<div class="btn">
								<a id="editCancel">Cancel</a>
								<a class="active" id="recogButton">View Result</a>
								<img id="loading_img" class="loading_img" src="${pageContext.request.contextPath}/aiaas/kr/images/loading.gif" alt="loading">
							</div>
						</div>
						<!--pattern_2-->
						<!--loding_area-->
						<div class="loding_area">
							<p><em class="far fa-file-image"></em>In progress</p>
							<div class="loding_box ">
								<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#f7778a" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

								<p>AI processing takes a while..</p>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_back1" id="btn_back1"><em class="fas fa-redo"></em>Reset</button>
							</div>
						</div>
						<!--loding_area-->
						<!--pattern_3-->
						<div class="pattern_3">
							<div class="img_box">
								<p><em class="far fa-file-image"></em>Result</p>
								<!--결과 이미지 들어갈 곳-->
								<img id="resultImg" src="" alt="Result Image" >
							</div>
							<div class="resultBox" >
								<div class="txtBox" id="note_table">
									<dl class="dltype02">
										<dt>Category</dt>
										<dd>Money</dd>
									</dl>
									<dl class="dltype02">
										<dt>Currency</dt>
										<dd id="currency"></dd>
									</dl>
									<dl class="dltype02">
										<dt>Country</dt>
										<dd id="country"></dd>
									</dl>
									<dl class="dltype02">
										<dt>Amount</dt>
										<dd id="amount"></dd>
									</dl>
								</div>
								<div class="txtBox" id="bill_table" style="display: none;">
									<dl class="dltype02">
										<dt>Category</dt>
										<dd>Bill</dd>
									</dl>
									<dl class="dltype02">
										<dt>Types of the bill</dt>
										<dd id="bill_type"></dd>
									</dl>
									<dl class="dltype02">
										<dt>Electronic payment number</dt>
										<dd id="bill_code"></dd>
									</dl>
								</div>
								<div class="btn_area">
									<button type="button" class="btn_back2"><em class="fas fa-redo"></em>Reset</button>
								</div>
							</div>
							<div class="result_hospital">
								<p><em class="far fa-file-excel"></em>Result File(.csv)</p>
								<div class="table_hospital">
									<table class="tbl_view">
										<colgroup id="col_group">
										</colgroup>
										<thead>
										<%--                                    <tr>--%>
										<%--                                        <th rowspan="2" colspan="2">항목</th>--%>
										<%--                                        <th colspan="4">급여</th>--%>
										<%--                                        <th colspan="2">비급여</th>--%>
										<%--                                    </tr>--%>
										<%--                                    <tr>--%>

										<%--                                        <th colspan="3">일반 본인부담</th>--%>
										<%--                                        <th rowspan="2">전액 본인부담</th>--%>
										<%--                                        <th rowspan="2">선택 진료료</th>--%>
										<%--                                        <th rowspan="2">선택 진료료<br>이외</th>--%>
										<%--                                    </tr>--%>
										<tr id="item_category">
											<th>Item ID</th>
											<th>Item Name</th>
											<th colspan=""> Code Item Price</th>
											<%--                                        <th>공단부담금</th>--%>
											<%--                                        <th>소계</th>--%>
										</tr>
										</thead>
										<tbody id="item_result">
										<%-- 결과 데이터 불러오는 곳--%>
										<!--
                                            <tr class="">
                                                <td class="bold_txt">39500</td>
                                                <td class="align_left">한방물리요법료</td>
                                                <td class="align_right">2,455</td>
                                                <td class="align_right">10,455</td>
                                                <td class="align_right" id="sub_total">130,455</td>
                                                <td class="align_right"></td>
                                                <td class="align_right">10,455</td>
                                                <td class="align_right">130,455</td>
                                                <td class="align_right">130,455</td>
                                            </tr>

                                            <tr class="sum">
                                                <td colspan="2" class="bold_txt">합계</td>
                                                <td class="align_right">2,455</td>
                                                <td class="align_right">10,455</td>
                                                <td class="align_right"></td>
                                                <td class="align_right">2,455</td>
                                                <td class="align_right">2,455</td>
                                                <td class="align_right">130,455</td>
                                            </tr>
                                        -->
										</tbody>
									</table>

								</div>
								<a class="img_pop" href="#" alt="원본 이미지"><em class="far fa-file-image"></em> View original image</a>
								<a id="save" onclick="downloadResultImg();" class="dwn_link" ><em class="far fa-arrow-alt-circle-down"></em> Download Result(.csv)</a>
								<div class="btn_area">
									<button type="button" class="btn_back2"><em class="fas fa-redo"></em>Reset</button>
								</div>

							</div>
							<!--인식하지 못했을 때 style="display: block;" -->
							<div class="resultBox_fail" style="display: none;">
								<div class="no_recog">The image is not recognizable</div>
								<div class="btn_area">
									<button type="button" class="btn_back2"><em class="fas fa-redo"></em>Reset</button>
								</div>
							</div>
						</div>
						<!--pattern_3-->
					</div>

				</div>
				<!-- .demobox -->

				<!--.recogmenu-->
				<div class="demobox vision_menu" id="recogmenu">
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API Guideline
							</div>
							<p class="sub_title">Set up Environment</p>
							<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
							<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
							<p class="sub_title">ID &amp; Key</p>
							<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
							<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
							<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
								<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
							<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
							<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
							<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
						</div>
						<div class="guide_group">
							<div class="title">
								OCR <small>(Optical Character Recognition)</small>
							</div>
							<p class="sub_txt">Recognize specific character in images. API provides Medical receipt recognition only.</p>

							<span class="sub_title">
								Preparation
					</span>
							<p class="sub_txt">- Input: Image file </p>
							<ul>
								<li>File type: .jpg, .png.</li>
								<li>Size: Under 2MB</li>
							</ul>
							<span class="sub_title">
								 API Document
							</span>
							<p class="sub_txt">① Request  </p>
							<ul>
								<li>Method : POST</li>
								<li>URL : https://api.maum.ai/ocr/hospitalReceipt</li>
							</ul>
							<p class="sub_txt">② Request parameters </p>
							<table>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th>type</th>
								</tr>
								<tr>
									<td>apiId </td>
									<td>Unique API ID. Request from is required for the ID. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>apiKey </td>
									<td>Unique API key. Request from is required for the key. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>receipt_img</td>
									<td>Input image file (.jpg,.png) </td>
									<td>string</td>
								</tr>
							</table>
							<p class="sub_txt">③ Request example </p>
							<div class="code_box">
								url --location --request POST 'http://api.maum.ai/ocr/hospitalReceipt' \<br>
								--header 'Content-Type: application/x-www-form-urlencoded' \<br>
								--form 'apiId= Own API ID' \<br>
								--form 'apiKey= Own API KEY' \<br>
								--form 'receipt_img= Receipt file'<br>
							</div>

							<p class="sub_txt">④ Response parameters </p>

							<div class="code_box">
<pre>
{
    "message": {
        "message": "Success",
        "status": 0
    },
    "result_img": {
        "name": "ori_4_rst.jpg",
        "data": "MzAzMjAs7JW97ZKI67mELCwsLC...LOy5mOujjOyerOujjOuMgCwsLCwsLA0K"
    },
    "result_csv": {
        "name": "ori_4_output.csv",
        "data": "MzAzMjAs7JW97ZKI67mELCwsLC...LOy5mOujjOyerOujjOuMgCwsLCwsLA0K"
    }
}
</pre>
							</div>
						</div>
					</div>
				</div>
				<!--//recogmenu-->
				<!--.recogexample-->
				<div class="demobox" id="recogexample">
					<p><em style="font-weight: 400;color:#f7778a;">Use Cases</em>  </p>
					<span class="sub">Find out how AI can be applied to diverse area.</span>
					<!-- //화폐, 고지서 인식(DIARL) -->
					<div class="useCasesBox">
						<ul class="lst_useCases">
							<li>
								<dl>
									<dt>
										<em>CASE 01</em>
										<span>Currency recognition</span>
									</dt>
									<dd class="txt">It can be used to implement additional services such as exchange rate service using automatically recognition of currencies from around the world
										<span><em class="fas fa-book-reader"></em> Reference: A Bank</span>
									</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_dir"><span>DIARL</span></li>
											<li class="ico_bot"><span>Chatbot</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>Bill recognition</span>
									</dt>
									<dd class="txt">It can be used to automatically recognize bills, receipts, etc. and create services that make automatic payments.
										<span><em class="fas fa-book-reader"></em> Reference: A Bank</span>
									</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_dir"><span>DIARL</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 03</em>
										<span>Recognition of consent</span>
									</dt>
									<dd class="txt">Create a business automation system that automatically recognizes the signature or checkbox of a consent form and determines whether it is signed or checked.
										<span><em class="fas fa-book-reader"></em> Reference: A Insurance company</span>
									</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_dir"><span>DIARL</span></li>
										</ul>
									</dd>
								</dl>
							</li>
						</ul>
					</div>
					<!-- //화폐, 고지서 인식(DIARL) -->
				</div>
				<!--//.recogexample-->


			</div>
		</div>
		<!-- //.contents -->

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/idr/croppie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/idr/recog.js?20190813"></script>
<script>

	var sample1File;
	var sample2File;
	var sample3File;
	var sample1SrcUrl;
	var sample2SrcUrl;
	var sample3SrcUrl;


	jQuery.event.add(window,"load",function(){

		$(document).ready(function (){
			loadSample1();
			loadSample2();
			loadSample3();

			$('.sample_more').on('click', function () {
				$('.sample_pop').fadeIn(300);
			});

			$('.sample_pop .pop_bd .country .option_box .radio').on('click', function () {
				$('.pop_2btn').fadeIn(300);
			});

			$('.bill_detail').on('click', function () {
				$('#samplebill_pop').fadeIn(300);
			});
			$('.hospital_detail').on('click', function () {
				$('#hospital_pop').fadeIn(300);
			});

			$('.upload_pop').on('click', function () {
				$('.upload_popup').fadeIn(300);

			});

			// 공통 팝업창 닫기
			$('.pop_sample_close, .pop_bg').on('click', function () {
				$('.pop_common').fadeOut(300);
				$('body').css({
					'overflow': '',
				});
			});

			$('.btn_gray, .minipop_close').on('click', function () {
				$('.pop_2btn').fadeOut(300);
				$('body').css({
					'overflow': '',
				});
			});

			//샘플 화폐 선택하기 버튼
			$('.btn_blue').on('click', function () {
				var option = $("input[type=radio]:checked").val();
				var imgget = $('#'+option).attr('src');
				console.log(imgget);
				$('.pop_common').fadeOut(300);
				$('.pop_2btn').fadeOut(300);
				$('#sampleImg_1').trigger("click");
				$('#sampleImg_1').attr('src', imgget);
				loadSample1();
				$('body').css({
					'overflow': '',
				});
			});

			// Remove uploaded image
			$('em.close').on('click', function () {
				$('#demoFile').val(null);
				$(this).parent().removeClass("btn_change");
				$(this).parent().addClass("btn");
				$(this).parent().children('.demolabel').text('Upload File');
				$('.fl_box').css('opacity', '1');
				$('.upload_popup .btn_area').hide();
			});
			$('em.pop_sample_close').on('click', function () {
				console.log('팝업창 닫기')
				$('#demoFile').val(null);
				$(this).parent().removeClass("btn_change");
				$(this).parent().addClass("btn");
				$(this).parent().children('.demolabel').text('Upload File');
				$('.fl_box').css('opacity', '1');
				$('.upload_popup .btn_area').hide();

			});

			// 파일 업로드 후 이벤트
			document.querySelector("#demoFile").addEventListener('change', function (ev) {

				if (this.files[0].type.match(/image.*/)){
					$('input[type="radio"]:checked').prop("checked", true);

					document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
					var url = URL.createObjectURL(this.files[0]);

					var element = document.getElementById( 'uploadFile' );
					element.classList.remove( 'btn' );
					element.classList.add( 'btn_change' );

					$('.fl_box').css("opacity", "0.5");
					var btn = $('.upload_popup .pop_bd .btn_area');
					btn.fadeIn(300);

				}
				else{
					this.value = null;
					alert('Upload Image file.');
				}
			});

			// 샘플 선택 후 인식하기 누를 때
			$('#editImgBtn').on('click', function () {
				var $checked = $('input[name="sample"]:checked').val();
				if( $checked === "Currency"){
					activateCroppie();
					$('.desc').removeClass('desc_3');
					$('.money_sample').fadeIn();
					$('.bill_sample').hide();
					$('.hospital_sample').hide();

					$('em.close').trigger('click');
					$('.upload_popup .btn_area').hide();
					$('.upload_popup').hide();
					$('.pattern_1').hide();
					$('.pattern_2').fadeIn(300);
				}else if( $checked === "Billing"){
					activateCroppie2();
					$('.desc').removeClass('desc_3');
					$('.bill_sample').fadeIn();
					$('.money_sample').hide();
					$('.hospital_sample').hide();

					$('em.close').trigger('click');
					$('.upload_popup .btn_area').hide();
					$('.upload_popup').hide();
					$('.pattern_1').hide();
					$('.pattern_2').fadeIn(300);
				}else if($checked === "MedicalBill"){
					console.log("진료비 MedicalBill");

					let file;

					loadSample3();
					file = sample3File;

					$('.pattern_1').hide();
					$('.loding_area').fadeIn();

					sendReqHospitalReceipt(file);
				}
			});

			//팝업 옵션 선택 후 인식하기 누를 때
			$('#editImgBtn2').on('click', function () {
				let $demoFile = $("#demoFile");
				let url;
				let $checked = $('input[name="pop_option"]:checked').val();

				$('input[name="sample"]').prop('checked',false);

				//내 파일로
				if ( $demoFile.val() === "" || $demoFile.val() === null){
					alert("Please select a sample or upload an image file.");
					return 0;
				}

				url = URL.createObjectURL($demoFile.get(0).files[0]);

				if( $checked === "Currency"){
					console.log("Currency");
					$('#previewImg').attr('src', url);
					popCroppie(url);
					$('.desc').removeClass('desc_3');
					$('.money_sample').fadeIn();
					$('.bill_sample').hide();
					$('.hospital_sample').hide();
					$('.pattern_2').fadeIn(300);

					$('em.close').trigger('click');
					$('.upload_popup .btn_area').hide();

				}else if( $checked === "Billing"){
					console.log("Billing");
					$('#previewImg').attr('src', url);
					popCroppie();
					$('.desc').removeClass('desc_3');
					$('.bill_sample').fadeIn();
					$('.money_sample').hide();
					$('.hospital_sample').hide();
					$('.pattern_2').fadeIn(300);

					$('em.close').trigger('click');
					$('.upload_popup .btn_area').hide();

				}else if( $checked === "MedicalBill"){
					console.log("MedicalBill");
					let file = $demoFile.get(0).files[0];
					$('#samplem_edical').attr('src', url);
					$('.loding_area').fadeIn();

					sendReqHospitalReceipt(file);
				}
				$('.pattern_1').hide();
				$('.upload_popup').hide();
			});

			$('.img_pop').on('click', function () {
				$('#hospital_pop').fadeIn();
			});

		});


		function loadSample1() {
			let blob = null;
			let xhr = new XMLHttpRequest();
			let imgsrc = $('#sampleImg_1').attr('src');
			//console.log (imgsrc);
			xhr.open("GET", imgsrc);
			xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
			xhr.onload = function()
			{
				blob = xhr.response;//xhr.response is now a blob object
				if(!navigator.msSaveBlob){
					sample1File = new File([blob], imgsrc);
				}else{
					sample1File = new Blob([blob], imgsrc);
				}
				sample1SrcUrl = URL.createObjectURL(blob);
			};

			xhr.send();
		}

		function loadSample2() {
			let blob = null;
			let xhr = new XMLHttpRequest();
			xhr.open("GET", "/aiaas/common/images/img_diarl_sample2.png");
			xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
			xhr.onload = function()
			{
				blob = xhr.response;//xhr.response is now a blob object
				if(!navigator.msSaveBlob){
					sample2SrcUrl = new File([blob], "img_diarl_sample2.png");
				}else{
					sample2SrcUrl = new Blob([blob], "img_diarl_sample2.png");
				}
				sample2SrcUrl = URL.createObjectURL(blob);
			};

			xhr.send();
		}
		function loadSample3() {
			let blob = null;
			let xhr = new XMLHttpRequest();
			xhr.open("GET", "/aiaas/common/images/hospital-sample.jpg");
			xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
			xhr.onload = function()
			{
				blob = xhr.response;//xhr.response is now a blob object
				if(!navigator.msSaveBlob){
					sample3File = new File([blob], "hospital-sample.jpg");
				}else{
					sample3File = new Blob([blob], "hospital-sample.jpg");
				}
				sample3SrcUrl = URL.createObjectURL(blob);
			};

			xhr.send();
		}

	});

</script>