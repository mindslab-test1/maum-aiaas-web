<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/kr/css/avr/croppie.css" />


<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/avr/avr.js?ver=20210603"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/avr/croppie.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/avr/filesaver.js"></script>
<input id="key" type="hidden" value="${_csrf.parameterName}">
<input id="value" type="hidden" value="${_csrf.token}">


<!-- .contents -->
<div class="contents">
	<!-- .content -->
	<div class="content api_content">
		<h1 class="api_tit">Face Detection</h1>
		<ul class="menu_lst vision_lst">
			<li class="tablinks" onclick="openTap(event, 'avrdemo')" id="defaultOpen">
				<button type="button">AI Engine</button>
			</li>
			<li class="tablinks" onclick="openTap(event, 'avrexample')">
				<button type="button">Use Case</button>
			</li>
			<li class="tablinks" onclick="openTap(event, 'avrmenu')">
				<button type="button">Manual</button>
			</li>
		</ul>

		<!-- .avrdemo -->
		<div class="demobox facedetect_box" id="avrdemo">
			<p>
				<span>Face Detection</span></small>
			</p>
			<span class="sub">After a human face is recognized and detected, it is made unidentifiable.</span>

			<!--textremoval_box-->
			<div class="demo_layout avr_box">
				<!--avr_1-->
				<div class="avr_1">
					<div class="fl_box">
						<p><em class="far fa-file-image"></em><strong>Use Sample File</strong></p>
						<div class="sample_box">
							<div class="sample_1">
								<div class="radio">
									<input type="radio" id="sample1" name="avr_option" value="face" checked>
									<label for="sample1" class="female">
										<div class="img_area">
											<img src="${pageContext.request.contextPath}/aiaas/common/images/img_facedetection.png" alt="sample img 1" />
										</div>
									</label>
								</div>
							</div>
						</div>
					</div>

					<div class="fr_box">
						<p><em class="far fa-file-image"></em><strong>Use My File</strong></p>
						<div class="uplode_box">
							<div class="btn" id="uploadFile">
								<em class="fas fa-times hidden close"></em>
								<em class="far fa-file-image hidden"></em>
								<label for="demoFile" class="demolabel">Upload File</label>
								<input type="file" id="demoFile" class="demoFile" accept=".jpg, .png" >
							</div>
							<ul>
								<li>* Supported files: .jpg, .png</li>
								<li>* Image file size is 2MB or less.</li>
								<li>* Face size minimum of 30 pixels, and must be<br>&nbsp;&nbsp;&nbsp;front-facing.</li>
								<li>* If the face is covered with a hat or mask, recognition<br>&nbsp;&nbsp;&nbsp;may be difficult.</li>
							</ul>
						</div>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_start" id="change_txt">Next</button>
					</div>
				</div>
				<!--avr_1-->

				<!--edit_box-->
				<div class="edit_box">
					<p><em class="far fa-file-image"></em>Image Editor</p>
					<div class="img_box">
						<em class="fas fa-minus minus"></em>
						<em class="fas fa-plus plus"></em>
						<!--불러온 이미지 들어갈 곳-->
						<img src="" alt="불러온 이미지" id="previewImg">
					</div>
					<p class="desc">Move the mouse to adjust the image position, and the bottom bar to resize.</p>
					<div class="btn_area">
						<a class="btn_start btn_cancel" href="">Reset</a>
						<a class="btn_start" id="recogButton">Process</a>
					</div>
				</div>
				<!--//edit_box-->

				<!--avr_2-->
				<div class="avr_2">
					<p><em class="far fa-file-image"></em>In Progress</p>
					<div class="loding_box ">
						<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#f7778a" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

						<p>AI processing takes a while...(about 3 seconds)</p>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
					</div>
				</div>
				<!--avr_2-->

				<!--avr_3-->
				<div class="avr_3 face_avr" >
					<div class="result_file" >
						<div class="origin">
							<p><em class="far fa-file-image"></em>Input</p>
							<div class="imgBox">
								<img id="input_img" src="" alt="input img" />
							</div>
						</div>
						<div class="result" >
							<p><em class="far fa-file-image"></em>Result</p>
							<div class="imgBox">
								<img src="" id="resultImg" alt="result img" />
							</div>
							<a id="save" onclick="downloadResultImg();" class="btn_dwn"><em class="far fa-arrow-alt-circle-down"></em> Download Result</a>
						</div>
<%--						<div class="recogbox" id="recogbox" style="display:none">--%>
<%--							<span>Result</span>--%>
<%--							<div class="carnumber"><img src="" id="carnumber" alt="번호판 이미지"/></div>--%>
<%--							<div class="carnumber_txt">33호 5598</div>--%>
<%--							<div class="errortxt"><em class="fas fa-exclamation-triangle"></em>&nbsp;Please resize and reposition the photo. </div>--%>
<%--						</div>--%>
					</div>
					<div class="btn_area">
						<button type="button" class="btn_back2"><em class="fas fa-redo"></em>Reset</button>
					</div>
				</div>
				<!--avr_3-->
			</div>
			<!--// textremoval_box-->
		</div>
		<!-- //.avrdemo -->

		<!--.avrmenu-->
		<div class="demobox vision_menu" id="avrmenu">
			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">API Guideline</div>

					<p class="sub_title">Set up Environment</p>
					<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
					<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
					<p class="sub_title">ID & Key</p>
					<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
					<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
					<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in.<br>
						<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
					<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
					<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
					<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
				</div>
				<div class="guide_group">
					<div class="title">Face Detection</div>
					<p class="sub_txt">After a human face is recognized and detected, it is made unidentifiable.</p>
					<span class="sub_title">
        								Preparation
        					</span>
					<p class="sub_txt">- Input: Image file containing human face(s)</p>
					<ul>
						<li>Supported files: .jpg, .png.</li>
						<li>Image capacity: 2MB or less.</li>
					</ul>
					<span class="sub_title">
        								 API Document
        							</span>
					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/api/avr</li>
					</ul>
					<p class="sub_txt">② Request parameters</p>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId </td>
							<td>Unique API ID. Request from is required for the ID.</td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey </td>
							<td>Unique API key. Request from is required for the key.</td>
							<td>string</td>
						</tr>
						<tr>
							<td>file</td>
							<td>type:file (.jpg,.png) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>option</td>
							<td>face</td>
							<td>string</td>
						</tr>
					</table>
					<p class="sub_txt">③ Request example </p>
					<div class="code_box">
<pre>
curl -X POST \
https://api.maum.ai/api/avr \
-H 'Accept: */*' \
-H 'Accept-Encoding: gzip, deflate' \
-H 'Cache-Control: no-cache' \
-H 'Connection: keep-alive' \
-H 'Host: api.maum.ai' \
-H 'content-type: multipart/form-data; boundary=----
WebKitFormBoundary7MA4YWxkTrZu0gW' \
-F apiId={Own API Id} \
-F apiKey={Own API Key} \
-F file=@{file path}
-F option={face}
</pre>
					</div>

					<p class="sub_txt">④ Response example </p>

					<div class="code_box">
						<img style="width:50%;" src="${pageContext.request.contextPath}/aiaas/common/images/img_face_sample.png" alt="sample">
					</div>
				</div>
			</div>
		</div>
		<!--//avrmenu-->

		<!--.avrexample-->
		<div class="demobox" id="avrexample">
			<p><em style="color:#f7778a;font-weight: 400;">Use Cases</em></p>
			<span class="sub">Find out how AI can be applied to diverse areas.</span>

			<!-- 얼굴 마스킹 엔진 -->
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>CCTV Anomaly Detection</span>
							</dt>
							<dd class="txt">Through video recognition, you can recognize and monitor specific objects or people in real time.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_avr"><span>AVR</span></li>
								</ul>
							</dd>
						</dl>
					</li>

					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>De-identifying<strong>video images</strong></span>
							</dt>
							<dd class="txt">Protect personal information by automatically de-identifying facial images taken when speeding vehicles are detected.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_avr"><span>AVR</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //얼굴 마스킹 엔진 -->
		</div>
		<!--//.avrexample-->
	</div>
	<!-- //.content -->
</div>
<!-- //.contents -->
<script>

var sampleImage1;
var data;


//파일명 변경
document.querySelector("#demoFile").addEventListener('change', function (ev) {
	document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
	var element = document.getElementById( 'uploadFile' );
	element.classList.remove( 'btn' );
	element.classList.add( 'btn_change' );
	$('.fl_box').css("opacity", "0.5");
});



jQuery.event.add(window,"load",function(){
	$(document).ready(function (){
		loadSample1();

		// step1->step2  (이미지 선택)
		$('.btn').on('click', function () {
//			$('.fl_box').css("opacity", "0.5");
			$('#change_txt').text('Next');

		});
		// step1->step2  (close button)
		$('em.close').on('click', function () {
			$(this).parent().removeClass("btn_change");
			$(this).parent().addClass("btn");
			$(this).parent().children('.demolabel').text('Upload File');
			$('.fl_box').css('opacity', '1');
			//파일명 변경
			document.querySelector("#demoFile").addEventListener('change', function (ev) {
				document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
				var element = document.getElementById( 'uploadFile' );
				element.classList.remove( 'btn' );
				element.classList.add( 'btn_change' );
			});
		});
		// step1->step2
		$('#change_txt').on('click', function () {
			var $checked = $('input[name="avr_option"]:checked');
			var uploadfile = $('#demoFile');
			var demoFileTxt = uploadfile.val();
			if (demoFileTxt === "") {
				loadSample1();
				console.log("Sample");

				if( $checked.val() === "plate"){
					console.log("Option 2");

				}
				activateCroppie();
			}else{

				console.log("Upload File");
				var url = URL.createObjectURL(uploadfile.get(0).files[0]);

				$('#previewImg').attr('src', url);
				$('#input_img').attr('src', url);

				activateCroppie();
			}
			$('.avr_1').hide();
			$('.edit_box').fadeIn(300);

		});


		// step2->step3
		$('.btn_back1').on('click', function () {
			// if(request){ request.abort(); }
			document.getElementById('recogbox').style.display = "block";
			window.location.reload();
		});

		// step3->step1
		$('.btn_back2').on('click', function () {
			window.location.reload();
		});


	});
});


function loadSample1() {
	var blob = null;
	var xhr = new XMLHttpRequest();
	xhr.open("GET", "/aiaas/common/images/img_facedetection.png");
	xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
	xhr.onload = function()
	{
		blob = xhr.response;//xhr.response is now a blob object
		sampleImage1 = new File([blob], "img_avr.jpg");

		var imgSrcURL = URL.createObjectURL(blob);
		var textr_output=document.getElementById('previewImg');
		var inputImg=document.getElementById('input_img');
		textr_output.setAttribute("src",imgSrcURL);
		inputImg.setAttribute("src",'${pageContext.request.contextPath}/aiaas/common/images/img_facedetection.png');

	};

	xhr.send();
}

function downloadResultImg(){
	var img = document.getElementById('resultImg');
	var link = document.getElementById("save");
	link.href = img.src.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
	link.download = "FaceDetection.JPG";
}
//API 탭
function openTap(evt, menu) {
var i, demobox, tablinks;
demobox = document.getElementsByClassName("demobox");
for (i = 0; i < demobox.length; i++) {
demobox[i].style.display = "none";
}
tablinks = document.getElementsByClassName("tablinks");
for (i = 0; i < tablinks.length; i++) {
tablinks[i].className = tablinks[i].className.replace(" active", "");
}
document.getElementById(menu).style.display = "block";
evt.currentTarget.className += " active";
}

document.getElementById("defaultOpen").click();

</script>