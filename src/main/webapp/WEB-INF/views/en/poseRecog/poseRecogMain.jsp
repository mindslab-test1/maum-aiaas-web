<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<script type="text/javascript">

</script>

		<!-- .contents -->
		<div class="contents api_content">
			<!-- .content -->
			<div class="content">
				<h1 class="api_tit">Recognition of Poses</h1>
				<ul class="menu_lst vision_lst">
					<li class="tablinks" onclick="openTap(event, 'iprdemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
					<li class="tablinks" onclick="openTap(event, 'iprexample')"><button type="button">Use Case</button></li>
					<li class="tablinks" onclick="openTap(event, 'iprmenu')"><button type="button">Manual</button></li>
<%--					<li class="tablinks"><a href="/member/enApiAccount">API ID, key</a></li>--%>
				</ul>
				<!-- .demobox -->
				<div class="demobox" id="iprdemo">
					<p><span>Recognition of Poses</span></p>
					<span class="sub">The engine recognizes body positions of people in an image and renders accurate visualizations. </span>
					<!--textremoval_box-->
					<div class="demo_layout poserecog_box">
						<!--tr_1-->
						<div class="tr_1">
							<div class="fl_box">
								<p><em class="far fa-file-image"></em><strong>Use Sample File</strong></p>
								<div class="sample_box">
									<div class="sample_1">
										<div class="radio">
											<input type="radio" id="sample1" name="option" value="1" checked>
											<label for="sample1" class="female">
												<div class="img_area">
													<img src="${pageContext.request.contextPath}/aiaas/common/images/pose01.jpg" alt="sample img 1" />
												</div>
											</label>
										</div>
									</div>
									<div class="sample_2">
										<div class="radio">
											<input type="radio" id="sample2" name="option" value="2" >
											<label for="sample2"  class="male">
												<div class="img_area">
													<img src="${pageContext.request.contextPath}/aiaas/common/images/pose02_.png" alt="sample img 2" />
												</div>
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="fr_box">
								<p><em class="far fa-file-image"></em><strong>Use My File</strong></p>
								<div class="uplode_box">
									<div class="btn" id="uploadFile">
										<em class="fas fa-times hidden close"></em>
										<em class="far fa-file-image hidden"></em>
										<label for="demoFile" class="demolabel">Upload image</label>
										<input type="file" id="demoFile" class="demoFile" accept=".jpg, .png">
									</div>
									<ul>
										<li>* Supported files: .jpg, .png</li>
										<li>* Image file size under 2MB</li>
										<li>* Please use a image with a full body captured more than 100 pixel</li>
										<li>* Higher image resolution for better cognition</li>
									</ul>
								</div>
							</div>
<%--							<div class="bottom_box" style="display:none">--%>
<%--								<p><em class="fas fa-sort-amount-up"></em><strong>지우기 강도 선택</strong></p>--%>
<%--								<div class="range_box">--%>
<%--									<div class="range_num"><span>1</span><span>2</span><span>3</span><span>4</span><span class="lstnum">5</span></div>--%>
<%--									<div class="slidecontainer">--%>
<%--										<input type="range" min="1" max="5" value="2" step="1" class="slider" id="myRange">--%>
<%--									</div>--%>
<%--								</div>--%>
<%--							</div>--%>
							<div class="btn_area">
								<button type="button" class="btn_start" id="sub" >Process</button>
							</div>
						</div>
						<!--tr_1-->
						<!--tr_2-->
						<div class="tr_2">
							<p><em class="far fa-file-image"></em>In progress</p>
							<div class="loding_box ">
								<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#f7778a" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

								<p>AI processing takes a while..</p>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_back1" id=""><em class="fas fa-redo"></em>Reset</button>
							</div>

						</div>
						<!--tr_2-->
						<!--tr_3-->
						<div class="tr_3" >
							<div class="origin_file">
								<p><em class="far fa-file-image"></em>Input</p>
								<div class="imgBox">
									<img id="input_img" src="" alt="Input image" />
								</div>
							</div>
							<div class="result_file" >
								<p><em class="far fa-file-image"></em>Result</p>
								<div class="imgBox">
									<img id="output_img" src="" alt="Result image" />
								</div>
								<a id="save" onclick="downloadResultImg();" class="btn_dwn"><em class="far fa-arrow-alt-circle-down"></em> Download Result</a>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_back2" id=""><em class="fas fa-redo"></em>Reset</button>
							</div>
						</div>
						<!--tr_3-->
					</div>
					<!--//.poserecog_box-->

				</div>
				<!-- //.demobox -->
				<!--.iprmenu-->
				<div class="demobox vision_menu" id="iprmenu">
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API Guideline
							</div>
							<p class="sub_title">Set up Environment</p>
							<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
							<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
							<p class="sub_title">ID &amp; Key</p>
							<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
							<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
							<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
								<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
							<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
							<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
							<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
						</div>
						<div class="guide_group">
							<div class="title">
								Recognition of Poses <small></small>
							</div>
							<p class="sub_txt">Recognize poses of multiple or singular people.</p>
							<span class="sub_title">
								Preparation
					</span>
							<p class="sub_txt">- Input: Image file </p>
							<ul>
								<li>File type: .jpg, .png.</li>
								<li>Size: Under 2MB</li>
							</ul>
							<span class="sub_title">
								API Document
							</span>
							<p class="sub_txt">① Request  </p>
							<ul>
								<li>Method : POST</li>
								<li>URL : https://api.maum.ai/smartXLoad/poseExtract</li>
							</ul>
							<p class="sub_txt">② Request parameters </p>
							<table>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th>type</th>
								</tr>
								<tr>
									<td>apiId </td>
									<td>Unique API ID. Request from is required for the ID. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>apiKey </td>
									<td>Unique API key. Request from is required for the key. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>pose_img</td>
									<td>Input image file (.jpg,.png) </td>
									<td>string</td>
								</tr>
							</table>
							<p class="sub_txt">③ Request example </p>
							<div class="code_box">
								url --location --request POST 'http://api.maum.ai/smartXLoad/poseExtract' \<br>
								--header 'Content-Type: multipart/form-data' \<br>
								--form 'apiId= Own API ID' \<br>
								--form 'apiKey= Own API KEY' \<br>
								--form 'pose_img= Person image file'<br>
							</div>

							<p class="sub_txt">④ Response - example </p>

							<div class="code_box">
<pre>
{
    "result_img":
	"iVBORw0KGgoAAAANSUhEUgAAAwIAAAIBCAYAAAD6XABHNCSV...",
}
</pre>
							</div>
						</div>
					</div>
					<!--//.guide_box-->
				</div>
				<!--.iprmenu-->
				<!--.iprexample-->
				<div class="demobox" id="iprexample">
					<p><em style="color:#f7778a;font-weight: 400;">Use Cases</em>  </p>
					<span class="sub">Find out how AI can be applied to diverse area.</span>
					<!-- 이미지 포즈 인식 -->
					<div class="useCasesBox">
						<ul class="lst_useCases">
							<li>
								<dl>
									<dt>
										<em>CASE 01</em>
										<span>Anomaly Detection</span>
									</dt>
									<dd class="txt">Used to accurately detect the behavior of a specific person.
										<span><em class="fas fa-book-reader"></em> Reference: A Airport, Suwon City Government</span>
									</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_pr"><span>Pose Recognition</span></li>
											<li class="ico_tr"><span>ESR</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>Motion Recognition</span>
									</dt>
									<dd class="txt">It can be used in sports, posture correction, and other areas where joint movement is required.<dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_pr"><span>Pose Recognition</span></li>
										</ul>
									</dd>
								</dl>
							</li>

						</ul>
					</div>
					<!--이미지 포즈 인식  -->
				</div>
				<!--//.iprexample-->


			</div>
			<!-- //.content -->
		</div>
		
<script type="text/javascript">
var sampleImage1;
var sampleImage2;
var data;
var xhr;

jQuery.event.add(window,"load",function(){
	function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/aiaas/common/images/pose01.jpg");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function()
        {
            blob = xhr.response;//xhr.response is now a blob object
            sampleImage1 = new File([blob], "pose01.jpg");
            var imgSrcURL = URL.createObjectURL(blob);
    		var textr_output=document.getElementById('input_img');
    		textr_output.setAttribute("src",imgSrcURL);            
        };
		
        xhr.send();
	}

	function loadSample2() {
	    var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/aiaas/common/images/pose02_.png");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function()
        {
            blob = xhr.response;//xhr.response is now a blob object
            sampleImage2 = new File([blob], "pose02_.png");
            
            var imgSrcURL = URL.createObjectURL(blob);
    		var textr_output=document.getElementById('input_img');
    		textr_output.setAttribute("src",imgSrcURL);            
        };
		
        xhr.send();
	}
	
	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#input_img').attr('src', e.target.result);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}

	$(document).ready(function (){
		loadSample2();
		loadSample1();

 		$("#demoFile").change(function(){
			readURL(this);
		});

 		$('.radio label').on('click',function(){
			$('.fl_box').attr('opacity',1);
			$('em.close').click();
		});

		$('#sub').on('click',function (){

			var demoFileTxt = $('#demoFile').val(); //파일을 추가한 input 박스의 값
			var formData = new FormData();
			var demoFile;

			if(demoFileTxt === ""){
				var option = $("input[type=radio][name=option]:checked").val();
				if(option == 1){
					loadSample1();
					demoFile = sampleImage1;
				}else{
					loadSample2();
					demoFile = sampleImage2;
				}
			}
			else {				
				var demoFileInput = document.getElementById('demoFile');
				// console.log(demoFileInput);
				demoFile = demoFileInput.files[0];
				// console.log(demoFile);
			}

			formData.append('file',demoFile);
			formData.append('${_csrf.parameterName}', '${_csrf.token}');

			$('.tr_1').hide();
			$('.tr_2').fadeIn(300);

			xhr = $.ajax({
				type: "POST",
				async: true,
				url: '/api/poseRecog/poseApi',
				data: formData,
				processData: false,
				contentType: false,
				success: function(result){
					let resultData = JSON.parse(result);
					$('.tr_1').hide();
					$('.tr_2').hide();
					$('.tr_3').fadeIn(300);

					$('#output_img').attr('src', "data:image/jpeg;base64," + resultData.result_img);
				},
				error: function(jqXHR, error){
					if(jqXHR.status === 0){
						return false;
					}

					alert("Error");
					console.dir(error);
					window.location.reload();
				}
			});


		});

	});	
});
function downloadResultImg(){
	var img = document.getElementById('output_img');
	var link = document.getElementById("save");
	link.href = img.src.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
	link.download = "PoseRecognition_IMAGE.PNG";
}

</script>
<script>
	//파일명 변경
	document.querySelector("#demoFile").addEventListener('change', function (ev) {
		document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
		var element = document.getElementById( 'uploadFile' );
		element.classList.remove( 'btn' );
		element.classList.add( 'btn_change' );
		$('.fl_box').css("opacity", "0.5");
	});


	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){

			// step1->step2  (close button)
			$('em.close').on('click', function () {
				$('.fl_box').css("opacity", "1");
				$('#uploadFile label').text('Upload Image');
				$('#uploadFile').removeClass("btn_change");
				$('#uploadFile').addClass("btn");
				$('#demoFile').val("");

				//파일명 변경
				document.querySelector("#demoFile").addEventListener('change', function (ev) {
					document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
					var element = document.getElementById( 'uploadFile' );
					element.classList.remove( 'btn' );
					element.classList.add( 'btn_change' );
				});
			});


			// 처음으로 버튼
			$('.btn_back1').on('click', function () {
				xhr.abort();
				$('.tr_2').hide();
				$('.tr_1').fadeIn(300);
			});

			// step3->step1
			$('.btn_back2').on('click', function () {
				$('.tr_3').hide();
				$('.tr_1').fadeIn(300);

				// $('em.close').parent().removeClass("btn_change");
				// $('em.close').parent().addClass("btn");
				// $('.fl_box').css("opacity", "1");
				// $('em.close').parent().children('.demolabel').text('이미지 업로드');

				document.querySelector("#demoFile").addEventListener('change', function (ev) {
					document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
					
				});
			});
			
		});
	});
	//API 탭
	function openTap(evt, menu) {
		var i, demobox, tablinks;
		demobox = document.getElementsByClassName("demobox");
		for (i = 0; i < demobox.length; i++) {
			demobox[i].style.display = "none";
		}
		tablinks = document.getElementsByClassName("tablinks");
		for (i = 0; i < tablinks.length; i++) {
			tablinks[i].className = tablinks[i].className.replace(" active", "");
		}
		document.getElementById(menu).style.display = "block";
		evt.currentTarget.className += " active";
	}
	// Get the element with id="defaultOpen" and click on it
	document.getElementById("defaultOpen").click();

</script>