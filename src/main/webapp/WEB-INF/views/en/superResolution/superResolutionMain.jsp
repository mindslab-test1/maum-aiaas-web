<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<%--  CSS/JS 파일 캐싱 방지 --%>
<%
	Date lastModifiedStyle = new Date();
	SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>

<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/superresoution.css?ver=<%=fmt.format(lastModifiedStyle)%>" />
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/en/css/pop_common.css?ver=<%=fmt.format(lastModifiedStyle)%>" />


<!-- 5 .pop_simple -->
<div class="pop_simple">
	<div class="pop_bg"></div>
	<!-- .popWrap -->
	<div class="popWrap pop_sr_noti">
		<button class="pop_close" type="button">close</button>
		<!-- .pop_bd -->
		<div class="pop_bd">
			<em class="fas fa-sad-cry"></em>
			<h5>Upload error</h5>
			<p>Error occurred due to the image or file size. <br>Please check the restriction again.</p>
			<span>* .jpg or .png<br>
				* File size under 30KB<br>
				* Image size under 200 x 200 pixels</span>

		</div>
		<!-- //.pop_bd -->
		<div class="btn">
			<a class="">OK</a>
		</div>
	</div>
	<!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
		<div class="contents api_content">
			<!-- .content -->
			<div class="content">
				<h1 class="api_tit">Enhanced Super Resolution</h1>
				<ul class="menu_lst vision_lst">
					<li class="tablinks" onclick="openTap(event, 'srdemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
					<li class="tablinks" onclick="openTap(event, 'srexample')"><button type="button">Use Case</button></li>
					<li class="tablinks" onclick="openTap(event, 'srmenu')"><button type="button">Manual</button></li>
<%--					<li class="tablinks"><a href="/member/enApiAccount">API ID, key</a></li>--%>
				</ul>
				<!-- .demobox -->

				<div class="demobox" id="srdemo">
					<p><span>Enhanced Super Resolution </span><small>(ESR)</small> </p>
					<span class="sub">Enlarge the image size and reduce the loss by enhancing resolution.</span>
					<!--superR_box-->
					<div class="demo_layout superR_box">
						<!-- 2 .pop_confirm -->
						<div class="pop_confirm">
							<div class="pop_bg"></div>
							<!-- .popWrap -->
							<div class="popWrap">
								<em class="fas fa-times close"></em>
								<!-- .pop_bd -->
								<div class="pop_bd">
									<div class="img">
										<p><em class="far fa-file-image"></em>Input</p>
										<div class="origin_img">
											<img src="" id="origin_img"alt="Input"/>
										</div>
									</div>
									<div class="img">
										<p><em class="far fa-file-image"></em> Result</p>
										<div class="result_img">
											<img src="" id="result_img" alt="Result"/>
										</div>
										<a id="save" onclick="downloadResultImg();" class="btn_dwn"><em class="far fa-arrow-alt-circle-down"></em> Download Result</a>
									</div>
								</div>
								<!-- //.pop_bd -->
							</div>
							<!-- //.popWrap -->
						</div>
						<!-- //.pop_confirm -->

						<!--tr_1-->
						<div class="tr_1">
							<div class="fl_box">
								<p><em class="far fa-file-image"></em><strong>Use Sample File</strong></p>
								<div class="sample_box">
									<div class="sample_1">
										<div class="radio">
											<input type="radio" id="sample1" name="option" value="1" checked>
											<label for="sample1" class="female">
												<div class="img_area">
													<img src="${pageContext.request.contextPath}/aiaas/en/images/img_sr_sample1.png" alt="sample img 1" />
												</div>
												<span>Sample 1</span>
											</label>
										</div>
									</div>
									<div class="sample_2">
										<div class="radio">
											<input type="radio" id="sample2" name="option" value="2" >
											<label for="sample2"  class="male">
												<div class="img_area">
													<img src="${pageContext.request.contextPath}/aiaas/en/images/img_sr_sample2.png" alt="sample img 2" />
												</div>
												<span>Sample 2</span>
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="fr_box">
								<p><em class="far fa-file-image"></em><strong>Use My File</strong></p>
								<div class="uplode_box">
									<div class="btn" id="uploadFile">
										<em class="fas fa-times hidden close"></em>
										<em class="far fa-file-image hidden"></em>
										<label for="demoFile" class="demolabel">Upload File</label>
										<input type="file" id="demoFile" class="demoFile" accept=".jpg, .png">
									</div>
									<ul>
										<li>* .jpg or .png</li>
										<li>* File size under 30KB</li>
										<li>* Image size under 200 x 200 pixels</li>
									</ul>
								</div>
							</div>

							<div class="btn_area">
								<button type="button" class="btn_start" id="sub">Process</button>
							</div>
						</div>
						<!--tr_1-->
						<!--tr_2-->
						<div class="tr_2">
							<p><em class="far fa-file-image"></em>In progress</p>
							<div class="loding_box ">
								<svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/><g><path fill="#f7778a" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/><animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/></g></svg>

								<p>AI processing takes a while..</p>
							</div>
							<div class="btn_area">
								<button type="button" class="btn_back1 reset_btn" id=""><em class="fas fa-redo"></em>Reset</button>
							</div>

						</div>
						<!--tr_2-->
						<!--tr_3-->
						<div class="tr_3" >
							<p><em class="far fa-file-image"></em> Preview</p>
							<div class="origin_file">
								<div class="imgBox">
									<img id="input_img" src="" alt="원본 이미지" />
								</div>
							</div>
							<div class="result_file" >
								<div class="imgBox">
									<img id="output_img" src="" alt="결과 이미지" />
								</div>
							</div>
							<span class="noti">Click the button below to check the difference.</span>
							<a class="result_check"><em class="far fa-eye"></em>See Result & Download</a>
							<div class="btn_area">
								<button type="button" class="btn_back2 reset_btn" id=""><em class="fas fa-redo"></em>Reset</button>
							</div>
						</div>
						<!--tr_3-->
					</div>
					<!--// superR_box-->

				</div>
				<!-- //.srmobox -->
				<!--.srmenu-->
				<div class="demobox vision_menu" id="srmenu">
					<!--guide_box-->
					<div class="guide_box">
						<div class="guide_common">
							<div class="title">
								API Guideline
							</div>
							<p class="sub_title">Set up Environment</p>
							<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
							<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
							<p class="sub_title">ID &amp; Key</p>
							<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
							<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
							<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
								<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
							<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
							<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
							<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
						</div>
						<div class="guide_group">
							<div class="title">
								ESR <small>(Enhanced Super Resolution)</small>
							</div>
							<p class="sub_txt"> Enlarge the size of small images while reducing the loss of the picture.</p>

							<span class="sub_title">
								 Preparation
							</span>
							<p class="sub_txt">- Input: Image file </p>
							<ul>
								<li>File type: .jpg, .png.</li>
								<li>Size: Under 200 x 200 pixel, 30KB</li>
							</ul>
							<span class="sub_title">
								 API Document
							</span>
							<p class="sub_txt">① Request  </p>
							<ul>
								<li>Method : POST</li>
								<li>URL : https://api.maum.ai/api/esr</li>
							</ul>
							<p class="sub_txt">② Request parameters </p>
							<table>
								<tr>
									<th>Name</th>
									<th>Description</th>
									<th>type</th>
								</tr>
								<tr>
									<td>apiId </td>
									<td>Unique API ID. Request from is required for the ID. </td>
									<td>string</td>
								</tr>
								<tr>
									<td>apiKey </td>
									<td>Unique API key. Request from is required for the key.</td>
									<td>string</td>
								</tr>
								<tr>
									<td>file</td>
									<td>Input image file (.jpg,.png) (Under 200 x 200 pixel) </td>
									<td>string</td>
								</tr>
							</table>
							<p class="sub_txt">③ Request example </p>
							<div class="code_box">
								curl -X POST \<br>
								&nbsp; https://api.maum.ai/api/esr \<br>
								&nbsp; -H 'content-type: multipart/form-data;<br>
								boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \<br>
								&nbsp; -F apiId=(*Request for ID) \<br>
								&nbsp; -F apiKey=(*Request for key) \<br>
								&nbsp; -F 'file=@sample.jpg'
							</div>

							<p class="sub_txt">④ Response example </p>

							<div class="code_box">
								<img src="${pageContext.request.contextPath}/aiaas/en/images/img_sr_response.png" alt="super resolution image" style="width:20%;">
							</div>
						</div>
					</div>
					<!--//.guide_box-->
				</div>
				<!--.srmenu-->
				<!--.srexample-->
				<div class="demobox" id="srexample">
					<p><em style="font-weight: 400;color:#f7778a;">Use Cases</em>  </p>
					<span class="sub">Find out how AI can be applied to diverse area.</span>
					<!-- super resolution(super resolution) -->
					<div class="useCasesBox">
						<ul class="lst_useCases">
							<li>
								<dl>
									<dt>
										<em>CASE 01</em>
										<span>CCTV Detection</span>
									</dt>
									<dd class="txt">Identify specific individuals in the CCTV footage by enlarging small pictures of them with less image loss. In addition to CCTV, it can also be used to enlarge small images of people in video or photos. </dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_tr"><span>ESR</span></li>
											<li class="ico_avr"><span>AVR</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>Photo Restoration</span>
									</dt>
									<dd class="txt">It can be utilized to restore old photos or low quality images. Use deep learning technology to restore low resolution photos.</dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_tr"><span>ESR</span></li>
										</ul>
									</dd>
								</dl>
							</li>
							<li>
								<dl>
									<dt>
										<em>CASE 02</em>
										<span>Photo Identification</span>
									</dt>
									<dd class="txt">It enlarges the image size of the identification documents such as photos in each driver's license. By increasing the resolution, photo restoration can be used for identification. </dd>
									<dd class="api_itemBox">
										<ul class="lst_api">
											<li class="ico_tr"><span>ESR</span></li>
										</ul>
									</dd>
								</dl>
							</li>
						</ul>
					</div>
					<!-- //super resolution -->
				</div>
				<!--//.srexample-->
			</div>
			<!-- //.content -->
		</div>
		
<script type="text/javascript">


//파일명 변경
document.querySelector("#demoFile").addEventListener('change', function (ev) {

	$('.fl_box').on('click', function(){
		$(this).css("opacity", "1");
		$('.tr_1 .btn_area .disBox').remove();
	});

	//이미지 용량 체크
	var demoFileInput = document.getElementById('demoFile');
	var demoFile = demoFileInput.files[0];
	var demoFileSize = demoFile.size;
	var max_demoFileSize = 1024 * 30;//1kb는 1024바이트
	var demofile = document.querySelector("#demoFile");

	if(demoFileSize > max_demoFileSize || !demofile.files[0].type.match(/image.*/)){
		$('.pop_simple').show();
		// 팝업창 닫기
		$('.pop_close, .pop_bg, .btn a').on('click', function () {
			$('.pop_simple').fadeOut(300);
			$('body').css({
				'overflow': '',
			});
		});
		$('.fl_box').css("opacity", "0.5");
		$('#demoFile').val('');
		$('.tr_1 .btn_area').append('<span class="disBox"></span>');

	}else{
		document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
		var element = document.getElementById( 'uploadFile' );
		element.classList.remove( 'btn' );
		element.classList.add( 'btn_change' );
		$('.fl_box').css("opacity", "0.5");
		$('.tr_1 .btn_area .disBox').remove();
	}

});


var sampleImage1;
var sampleImage2;
var data;

jQuery.event.add(window,"load",function(){
	function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/aiaas/en/images/img_sr_sample1.png");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function()
        {
            blob = xhr.response;//xhr.response is now a blob object
            sampleImage1 = new File([blob], "img_sr_sample1.png");
            
            var imgSrcURL = URL.createObjectURL(blob);
    		var esr_output=document.getElementById('input_img');
    		esr_output.setAttribute("src",imgSrcURL);

			var output=document.getElementById('origin_img');
			output.setAttribute("src",imgSrcURL);
		};
		
        xhr.send();
    }

	function loadSample2() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/aiaas/en/images/img_sr_sample2.png");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function()
        {
            blob = xhr.response;//xhr.response is now a blob object
            sampleImage2 = new File([blob], "img_sr_sample2.png");
            
            var imgSrcURL = URL.createObjectURL(blob);
    		var esr_output=document.getElementById('input_img');
    		esr_output.setAttribute("src",imgSrcURL);

			var output=document.getElementById('origin_img');
			output.setAttribute("src",imgSrcURL);
        };
		
        xhr.send();
    }	
	
	function readURL(input) {

		if (input.files && input.files[0]) {
			var reader = new FileReader();

			reader.onload = function (e) {
				$('#input_img').attr('src', e.target.result);
			};

			reader.readAsDataURL(input.files[0]);
		}
	}
	$(document).ready(function (){
		loadSample1();
		loadSample2();

 		$("#demoFile").change(function(){
			readURL(this);
		});

 		//결과보기 버튼 누를 때
		$('#sub').on('click',function (){
			var demoFileTxt = $('#demoFile').val(); //파일을 추가한 input 박스의 값

			if(demoFileTxt == ""){
				var demoFile;
				var option = $("input[type=radio][name=option]:checked").val();
				if(option == 1){
					loadSample1();
					demoFile = sampleImage1;
				}else{
					loadSample2();
					demoFile = sampleImage2;
				}

				//데이터를 보내기 위해서
				//샘플전송
				var formData = new FormData();

				formData.append('file',demoFile);
				formData.append('${_csrf.parameterName}', '${_csrf.token}');//csrf 토큰 세팅

				var request = new XMLHttpRequest();
				//
				request.responseType ="blob";
				request.onreadystatechange = function(){
					if (request.readyState === 4){
						console.log(request.response);
						var blob = request.response; // 응답 받은 것
						var imgSrcURL = URL.createObjectURL(blob);//블롭을 url로 생성

						var esr_output=document.getElementById('output_img');
						esr_output.setAttribute("src",imgSrcURL);
						$("#result_img").attr("src", imgSrcURL);

						// data = new Blob([request.response], {type:'image/jpeg'});

						$('.tr_1').hide();
						$('.tr_2').hide();
						$('.tr_3').fadeIn(300);

					} else{
						//에러처리 추가
					}
				};

				//url로 맵핑된 컨트롤러로 간다. 포스트 방식으로 이 url에 데이터를 보낸다.
				request.open('POST', '/api/getSuperResolution');
				request.send(formData);

				$('.tr_1').hide();
				$('.tr_2').fadeIn(300);
			}
			else {
				var demoFileInput = document.getElementById('demoFile');
				var demoFile = demoFileInput.files[0];
				//데이터를 보내기 위해서
				var formData = new FormData();

				formData.append('file',demoFile);
				formData.append('${_csrf.parameterName}', '${_csrf.token}');

				var request = new XMLHttpRequest();
				request.responseType ="blob";
				request.onreadystatechange = function(){
					if (request.readyState === 4){
						console.log(request.response);
						var blob = request.response;
						var imgSrcURL = URL.createObjectURL(blob);

						//팝업 원본
						var origin_img= $('#input_img').attr('src');
						$("#origin_img").attr("src", origin_img);

						//결과
						var esr_output=document.getElementById('output_img')
						esr_output.setAttribute("src",imgSrcURL);
						//결과 (팝업)
						$("#result_img").attr("src", imgSrcURL);
						// data = new Blob([blob], {type:'image/jpeg'});


						$('.tr_1').hide();
						$('.tr_2').hide();
						$('.tr_3').fadeIn(300);
					}
				};
				request.open('POST', '/api/getSuperResolution');
				request.send(formData);

				$('.tr_1').hide();
				$('.tr_2').fadeIn(300);
			}
		});


		// step1 (close button)
		$('em.close').on('click', function () {
			$('.fl_box').css("opacity", "1");
			$(this).parent().removeClass("btn_change");
			$(this).parent().addClass("btn");
			$(this).parent().children('.demolabel').text('이미지 업로드');
			$('#demoFile').val('');
			//파일명 변경
			document.querySelector("#demoFile").addEventListener('change', function (ev) {
				document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
				var element = document.getElementById( 'uploadFile' );
				element.classList.remove( 'btn' );
				element.classList.add( 'btn_change' );
			});
		});

		// step2->step3
		$('.btn_back1').on('click', function () {
			$('.tr_2').hide();
			$('.tr_1').fadeIn(300);
			$('.fl_box').css("opacity", "1");
		});

		// step3->step1
		$('.btn_back2').on('click', function () {
			$('.tr_3').hide();
			$('.tr_1').fadeIn(300);
			$('.fl_box').css("opacity", "1");
			var label_change = $('.demolabel');
			label_change.text('Upload File');
			label_change.parent().removeClass('btn_change');
			label_change.parent().addClass('btn');
			$('#demoFile').val('');
		});

		// 결과파일 팝업 (새창)
		$('.result_check').on('click', function () {
			$('.pop_confirm').fadeIn(300);

		});

		// 결과 팝업창 닫기
		$('.close, .pop_bg').on('click', function () {
			$('.pop_confirm').fadeOut(300);
			$('body').css({
				'overflow': '',
			});
		});
	});
});

function downloadResultImg(){
	var img = document.getElementById('result_img');
	var link = document.getElementById("save");
	link.href = img.src.replace(/^data:image\/[^;]/, 'data:application/octet-stream');
	link.download = "SuperResolution_IMAGE.JPG";
}

//API 탭
function openTap(evt, menu) {
	var i, demobox, tablinks;
	demobox = document.getElementsByClassName("demobox");
	for (i = 0; i < demobox.length; i++) {
		demobox[i].style.display = "none";
	}
	tablinks = document.getElementsByClassName("tablinks");
	for (i = 0; i < tablinks.length; i++) {
		tablinks[i].className = tablinks[i].className.replace(" active", "");
	}
	document.getElementById(menu).style.display = "block";
	evt.currentTarget.className += " active";
}
document.getElementById("defaultOpen").click();

</script>
