<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>



<div class="contents">
	<div class="content api_content">
		<h1 class="api_tit">Speech Recognition</h1>
		<ul class="menu_lst voice_menulst">
			<li class="tablinks" onclick="openTap(event, 'sttdemo')" id="defaultOpen"><button type="button">AI Engine</button></li>
			<li class="tablinks" onclick="openTap(event, 'sttexample')"><button type="button">Use Case</button></li>
			<li class="tablinks" onclick="openTap(event, 'sttmenu')"><button type="button">Manual</button></li>
<%--			<li class="tablinks"><a href="/member/enApiAccount">API ID, key</a></li>--%>
		</ul>
		<div class="demobox sttdemo apisttdemo" id="sttdemo">
			<p>The Latest <span>Speech Recognition</span><small>(STT)</small> based on Deep Learning</p>
			<span class="sub">Convert speech into texts regardless of types of voice or content.</span>

			<div class="stt_demo">
				<!-- #demo04 -->
				<div id="demo04" class="tab_contents">

					<!-- .rightBox -->
					<div class="rightBox">
						<div class="selectarea">
							<div class="radio" name="level" id="level">
								<input type="radio" id="3" value="eng_baseline" name="voice" checked="checked">
								<label for="3"><span>English</span></label>
							</div>
							<div class="radio">
								<input type="radio" id="1" value="kor_baseline" name="voice">
								<label for="1"><span>Korean</span></label>
							</div>
<%--							<div class="radio">
								<input type="radio" id="2" value="kor_address" name="voice">
								<label for="2"><span>Korean <strong>Address</strong></span></label>
							</div>--%>
							<div class="radio minipopbtn">
								<em class="fas fa-plus-circle"></em>
								<span>more</span>
								<strong class="minipop">Please contact us for the following API:  Korean, English(Contact Center), and English(Educational).<br>(hello@mindslab.ai)</strong>
							</div>

						</div>
						<!-- .step01 -->
						<div class="demo_intro">
							<div class="demo_fileTxt">Click Record and speak,<br> or upload files</div>
							<div class="demo_recordTxt">Start speaking.</div>
							<div class="btnupload">
								<label for="demoFile"><span><em class="fas fa-file-upload"></em>Upload files</span></label>
								<input type="file" id="demoFile" class="demoFile" accept=".wav">
							</div>
							<div class="btnBox">
								<span><em>Record</em></span>
							</div>
							<div class="btnreset">
								<button class="demoReset"><span>Reset</span></button>
								<button class="demoStop"><span>Stop</span></button>
							</div>
<%--							<div class="holeBox">--%>
<%--								<div class="hole">--%>
<%--									<em></em><em></em><em></em><em></em><em></em>--%>
<%--								</div>--%>
<%--							</div>--%>
						</div>
						<!-- //.step01 -->
						<!-- .step03 -->
						<div class="demo_recording">
							<div class="demo_infoTxt">Please wait.<br> The speech is currently being converted into texts.</div>
							<!-- recording -->
							<div class="recording">
								<span></span><span></span><span></span><span></span><span></span><span></span><span></span><span></span>
								<span></span><span></span><span></span><span></span><span></span><span></span><span></span>
							</div>
							<!-- //recording -->
						</div>
						<!-- //.step03 -->
						<!-- .step04 -->
						<div class="demo_result">
							<div class="resultTxt" id="demo_result_text">Default text
							</div>
							<div class="btnBox btnReset">
								<button class="demoReset"><span>Reset</span></button>
							</div>
 							<div class="btnBox btndwn">
								<button class="dwn"><a id="save"><span>Download</span></a></button>
							</div>
						</div>
						<!-- //.step04 -->

					</div>
					<!-- //.rightBox -->

				</div>
				<!-- //#demo04 -->


				<div class="remark">
					*Please upload the file in following format: .wav less than 5MB.<br>
					*Only sample rate 8K is supported.<br>
					*Please contact us if you inquire languages other than Korean and English.
				</div>
			</div>
		</div>

		<!--.ttsmenu-->
		<div class="demobox voice_menu" id="sttmenu">

			<!--guide_box-->
			<div class="guide_box">
				<div class="guide_common">
					<div class="title">
						API Guideline
					</div>
					<p class="sub_title">Set up Environment</p>
					<p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
					<p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
					<p class="sub_title">ID &amp; Key</p>
					<p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
					<p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
					<p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
						<small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
					<p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
					<p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
					<p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
				</div>
				<!--guide_group-->
				<div class="guide_group">

					<div class="title">
						Speech Recognition (Speech-To-Text)
					</div>
					<p class="sub_txt">Minds Lab’s STT API transforms speeches into texts</p>

					<span class="sub_title">
					Preparation
					</span>
					<p class="sub_txt">① Input: Sound file (.wav) </p>
					<p class="sub_txt">② Model: Choose one below </p>
					<ul>
						<li>Korean for general use (baseline, kor, 8000)</li>
						<li>English for general use (baseline, eng, 8000)</li>
					</ul>
					<span class="sub_title">
					API Document
					</span>

					<p class="sub_txt">① Request  </p>
					<ul>
						<li>Method : POST</li>
						<li>URL : https://api.maum.ai/stt/cnnSttSimple</li>
					</ul>
					<p class="sub_txt">② Request parameters </p>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>type</th>
						</tr>
						<tr>
							<td>apiId</td>
							<td>Unique API ID. Request from is required for the ID. </td>
							<td>string</td>
						</tr>
						<tr>
							<td>apiKey</td>
							<td>Unique API key. Request from is required for the key.</td>
							<td>string</td>
						</tr>
						<tr>
							<td>model</td>
							<td>Selected model (baseline_kor_8k_default, baseline_eng_8k_default)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>file</td>
							<td>Input sound file (.wav) </td>
							<td>file</td>
						</tr>
						<%--<tr>
							<td>cmd</td>
							<td>Command for the API (runFileStt)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>lang</td>
							<td>Selected language ( kor / eng )</td>
							<td>string</td>
						</tr>
						<tr>
							<td>sampling</td>
							<td>Selected sampling rate (8000 / 16000) </td>
							<td>string</td>
						</tr>
						<tr>
							<td>level</td>
							<td>Selected level (baseline / address)</td>
							<td>string</td>
						</tr>--%>

					</table>
					<p class="sub_txt">③ Request example </p>
					<div class="code_box">
						curl -X POST \<br>
						https://api.maum.ai/stt/cnnSttSimple \<br>
						-H 'content-type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW' \<br>
						-F apiID= (*Request for ID) \<br>
						-F apiKey= (*Request for key) \<br>
						-F model= Selected model \<br>
						-F 'file=@eng_8k_1.wav' \<br>

					</div>

					<p class="sub_txt">④ Response parameters </p>
					<%--<span class="table_tit">Response</span>--%>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>type</th>
						</tr>

						<tr>
							<td>commonMsg</td>
							<td>Success/Fail, Status Code </td>
							<td>object</td>
						</tr>
						<tr>
							<td>result</td>
							<td>STT result (text) </td>
							<td>string</td>
						</tr>
						<%--<tr>
							<td>status</td>
							<td>Status of the API request (Success / Fail)</td>
							<td>string</td>
						</tr>--%>
					</table>
					<%--<span class="table_tit">extra_data: Data of the input file</span>
					<table>
						<tr>
							<th>Name</th>
							<th>Description</th>
							<th>type</th>
						</tr>
						<tr>
							<td>stt_length</td>
							<td>Character length of the text result</td>
							<td>int</td>
						</tr>
						<tr>
							<td>stt_data</td>
							<td>STT result (text)</td>
							<td>string</td>
						</tr>
						<tr>
							<td>stt_duration</td>
							<td>Duration of the input file (seconds) </td>
							<td>number</td>
						</tr>
					</table>--%>
					<p class="sub_txt">⑤ Response example </p>
					<div class="code_box">
<pre>
{
	"status": "Success",
	"commonMsg": {
		"message": "success",
		"status": 0
	},
	"result": "Let's play badminton."
}
</pre>
					</div>
				</div>
				<!--//.guide_group-->
			</div>
			<!--//.guide_box-->
		</div>
		<!--//ttsmenu-->
		<!--.sttexample-->
		<div class="demobox" id="sttexample">
			<p><em style="font-weight: 400;">Use Cases</em>  </p>
			<span class="sub">Find out how AI can be applied to diverse area.</span>
			<div class="useCasesBox">
				<ul class="lst_useCases">
					<li>
						<dl>
							<dt>
								<em>CASE 01</em>
								<span>Contact Center Analysis</span>
							</dt>
							<dd class="txt">Transcript calls into text and analyze the details of customers’ calls in contact centers.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_stt"><span>STT</span></li>
<%--									<li class="ico_dia"><span>Diarization</span></li>--%>
									<li class="ico_nlu"><span>NLU</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 02</em>
								<span>Address Management</span>
							</dt>
							<dd class="txt">You can systematically manage the address information collected by calls by transferring into text.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_stt"><span>STT</span></li>
									<li class="ico_hmd"><span>HMD</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 03</em>
								<span>Interview files</span>
							</dt>
							<dd class="txt">Interviews or conversations that you have as a recorded file can be archived after converting into text.</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_stt"><span>STT</span></li>
<%--									<li class="ico_dia"><span>Diarization</span></li>--%>
									<li class="ico_vf"><span>Voice Filter</span></li>
								</ul>
							</dd>
						</dl>
					</li>
					<li>
						<dl>
							<dt>
								<em>CASE 04</em>
								<span>Lecture materials</span>
							</dt>
							<dd class="txt">You can save the contents of lectures that you have as recorded files by converting them into text</dd>
							<dd class="api_itemBox">
								<ul class="lst_api">
									<li class="ico_stt"><span>STT</span></li>
									<li class="ico_den"><span>Denoise</span></li>
								</ul>
							</dd>
						</dl>
					</li>
				</ul>
			</div>
			<!-- //음성 생성(STT) -->
		</div>

	</div>

</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/app.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/particles.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/recorder.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/kr/js/bootstrap-datepicker.js"></script>


<script type="text/javascript">

	var timeLimit_trigger=1;
	var trigger_demostop;

	//Expose globally your audio_context, the recorder instance and audio_stream
	var audio_context;
	var recorder;
	var audio_stream;
	var modelStr = '';

	/**
	 * Stops the recording process. The method expects a callback as first
	 * argument (function) executed once the AudioBlob is generated and it
	 * receives the same Blob as first argument. The second argument is
	 * optional and specifies the format to export the blob either wav or mp3
	 */
	function stopRecording(callback, AudioFormat) {
		// Stop the recorder instance
		recorder && recorder.stop();
// 	console.log('Stopped recording.');

		// Stop the getUserMedia Audio Stream !
		audio_stream.getAudioTracks()[0].stop();

		// Use the Recorder Library to export the recorder Audio as a .wav file
		// The callback providen in the stop recording method receives the blob
		if(typeof(callback) == "function"){

			/**
			 * Export the AudioBLOB using the exportWAV method.
			 * Note that this method exports too with mp3 if
			 * you provide the second argument of the function
			 */
			recorder && recorder.exportPCM(function (blob) {
				callback(blob);

				// create WAV download link using audio data blob
				// createDownloadLink();

				// Clear the Recorder to start again !
				recorder.clear();
			}, (AudioFormat || "audio/pcm"));
		}
	}

	function replaceAll(strTemp, strValue1, strValue2){
		while (1) {
			if (strTemp.indexOf(strValue1) != -1)
				strTemp = strTemp.replace(strValue1, strValue2);
			else
				break;
		}
		return strTemp;
	}

	function downloadText(){
		var test = $('#demo_result_text').text();
		var data = new Blob([test]);
		var link = document.getElementById("save");
		link.href = URL.createObjectURL(data);
		link.download = "STT.txt" ;
	}

	function post_submit(AudioBLOB, modelStr){
		var formData = new FormData();

        console.log("==submit==\nmodel: " + $('input:radio[name="voice"]:checked').val());
        var model = $('input:radio[name="voice"]:checked').val().split('_')
		var txtlang = model[0];

		var audio_file = new File([AudioBLOB], 'stt_file.wav');
		formData.append('file', audio_file);

		formData.append('${_csrf.parameterName}', '${_csrf.token}');

		if($('input:radio[name="voice"]:checked').val() == 'kor_baseline') {	// 한국어
			formData.append('lang', txtlang);
			formData.append('model', 'baseline_kor_8k_default');
		var request = new XMLHttpRequest();
		request.onreadystatechange = function(){
			if (request.readyState === 4){
				var jsonResult = JSON.parse(request.response)['result'];

				$('.demo_result .resultTxt').text(jsonResult);

				downloadText();

				$('.rightBox .demo_recording').hide();
				$('.rightBox .demo_result').show();
				$('.demo_result div').show();
			}
		};

		request.open('POST', '/api/stt/cnnSttSimple');
		request.send(formData);

		} else if ($('input:radio[name="voice"]:checked').val() == 'eng_baseline') {	// 영어
			formData.append('lang', txtlang);
			formData.append('model', 'baseline_eng_8k_default');

			var request = new XMLHttpRequest();
			request.onreadystatechange = function() {
				if (request.readyState === 4) {
					var jsonResult = JSON.parse(request.response)['result'];
					console.log(jsonResult);

					$('.demo_result .resultTxt').text(jsonResult);

					downloadText();

					$('.rightBox .demo_recording').hide();
					$('.rightBox .demo_result').show();
					$('.demo_result div').show();
				}
			};

			request.open('POST', '/api/stt/cnnSttSimple');
			request.send(formData);
		} else {	// 한국어 주소

			formData.append('lang', txtlang);
			formData.append('level', txtlevel);
			formData.append('sampling', txtsampleRate);
			formData.append('cmd', 'runFileStt');

			var request = new XMLHttpRequest();
			request.onreadystatechange = function () {
				if (request.readyState === 4) {

					var jsonResult = JSON.parse(request.response)['data'];
					console.log(jsonResult);

					$('.demo_result .resultTxt').text(jsonResult);

					downloadText();
					/* $('.demoTab .rightBox .demo_recording').hide();
                    $('.demoTab .rightBox .demo_result').show(); */

					$('.rightBox .demo_recording').hide();
					$('.rightBox .demo_result').show();
					$('.demo_result div').show();
				}
			};

			request.open('POST', '/stt');
			request.send(formData);
		}
	}


	function upload_file_submit(){
		var formData = new FormData();
		console.log("==file_submit==\nmodel: " + $('input:radio[name="voice"]:checked').val());
        var model = $('input:radio[name="voice"]:checked').val().split('_')
		var txtlang = model[0];
		var txtlevel = model[1];
		var txtsampleRate = "8000";
		if(txtlevel=="baseline" && txtlang=="kor"){
			//txtsampleRate = "16000";
		}

		formData.append('file', document.getElementById('demoFile').files[0]);
		formData.append('${_csrf.parameterName}', '${_csrf.token}');

		if($('input:radio[name="voice"]:checked').val() == 'kor_baseline') {	// 한국어
			formData.append('lang', txtlang);
			formData.append('model', 'baseline_kor_8k_default');

			var request = new XMLHttpRequest();
			request.onreadystatechange = function() {
				if (request.readyState === 4) {
					var jsonResult = JSON.parse(request.response)['result'];
					console.log(jsonResult);

					$('.demo_result .resultTxt').text(jsonResult);

					downloadText();

					$('.rightBox .demo_recording').hide();
					$('.rightBox .demo_result').show();
					$('.demo_result div').show();
				}
			};

			request.open('POST', '/api/stt/cnnSttSimple');
			request.send(formData);

		} else if ($('input:radio[name="voice"]:checked').val() == 'eng_baseline') {	// 영어
			formData.append('lang', txtlang);
			formData.append('model', 'baseline_eng_8k_default');

			var request = new XMLHttpRequest();
			request.onreadystatechange = function() {
				if (request.readyState === 4) {
					var jsonResult = JSON.parse(request.response)['result'];
					console.log(jsonResult);

					$('.demo_result .resultTxt').text(jsonResult);

					downloadText();

					$('.rightBox .demo_recording').hide();
					$('.rightBox .demo_result').show();
					$('.demo_result div').show();
				}
			};

			request.open('POST', '/api/stt/cnnSttSimple');
			request.send(formData);
		}
	}

	jQuery.event.add(window,"load",function(){
		$(document).ready(function (){
			$('.demo_recording').hide();
			$('.demo_result').hide();
			$('.holeBox').hide();

			$('.minipopbtn').on('click', function(){
				if($('.minipop').is(':visible')){
					$('.minipop').hide();
				}else{
					$('.minipop').show();
				}
			});

			// 리셋 버튼 누를때
			$('.demo_result button.demoReset').on('click', function(){
				$('#demoFile').val("");

				$('#demo04 .demo_fileTxt').removeClass('errorTxt').html('Click Record and speak,  <br>or upload files.');
				$('.rightBox .demo_result').hide();
				$('.demo_recordTxt').hide();
				$('.stt_demo .rightBox .selectarea').show();
                $('.rightBox .demo_intro').show();
                $('.rightBox .demo_intro div').show();
                $('.rightBox .demo_intro .demo_recordTxt').hide();
                $('.rightBox .demo_intro .btnreset').hide();
                $('.rightBox .demo_intro .holeBox').hide();
			});

			// Demo 파일업로드
			$('#demoFile').on('change', function() {
				console.log("model: " + $('input:radio[name="voice"]:checked').val());
				var fileTxt = $('.demoFile').val(); //파일을 추가한 input 박스의 값
				var fneTxt = fileTxt.split(".").pop(-1).toLowerCase();

				if ( fneTxt != "mp3" && fneTxt != "wav" && fneTxt != "pcm") {// 음성파일 체크
					// 실패
					$('#demo04 .demo_fileTxt').addClass('errorTxt').text('Please upload files in wav format.');
                    $('.rightBox .demo_intro .demo_recordTxt').hide();
                    $('.rightBox .demo_intro .btnreset').hide();
                    $('.rightBox .demo_intro .holeBox').hide();
                } else {
                    // 성공
					upload_file_submit();

					$('.stt_demo .rightBox .demo_intro').hide();
					$('.stt_demo .rightBox .selectarea').hide();
					$('.stt_demo .rightBox .demo_recording').show();
				}
			});

			// Demo 가능 여부 체크
			function hasGetUserMedia() {
				return !!(navigator.mediaDevices || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia);
			}

			if (hasGetUserMedia() ) {
				console.log("음성녹음지원");
				// Demo 가능 브라우저
				// $('.demoTab .leftBox .btnBox .btn_infoTxt').each(function(){
				// 	$(this).append('<li>Press the “Record” button and start speaking.</li>');
				// });

				$('.demo_intro .btnupload').css("left","439px");
				$('.demo_intro .demo_fileTxt').html("Click Record and speak,<br> or upload files");
			} else {
			console.log("음성녹음지원안함");
				// Demo 불가능 브라우저
                $('.demo_intro .btnBox').hide();
				$('.demo_intro .btnupload').css("left","358px");
				// 브라우저 업데이트 안내 메세지
				var agent = navigator.userAgent.toLowerCase();
				if ( (navigator.appName == 'Netscape' && navigator.userAgent.search('Trident') != -1) || (agent.indexOf("msie") != -1) ) {
					$('.demo_intro .demo_fileTxt').html("In order to record your own voice via a microphone, please update your browser to the most recent version of Microsoft Edge, Firefox, Chrome and other types of WebRTC browser.");
				}
			}

			// Demo 음성
			// step01 > step02
			$('.demo_intro .btnBox').on('click', function(){

				audio_context = new AudioContext;
				// Access the Microphone using the navigator.getUserMedia method to obtain a stream
				var promise = navigator.mediaDevices.getUserMedia({ audio: true })
						.then(function (stream) {
							audio_stream = stream;

							// Create the MediaStreamSource for the Recorder library
							var input = audio_context.createMediaStreamSource(stream);

							recorder = new Recorder(input, {sampleRate: 44100});

							// Start recording !
							recorder && recorder.record();

							$('.rightBox .selectarea').hide();
                            $('.rightBox .demo_intro').show();
                            $('.rightBox .demo_intro div').hide();
                            $('.rightBox .demo_intro .demo_recordTxt').fadeIn(200);
                            $('.rightBox .demo_intro .btnreset').show();
                            $('.rightBox .demo_intro .btnreset .demoReset').hide();
							$('.rightBox .demo_intro .btnreset .demoStop').show();
							$('.holeBox').fadeIn(200);

							//녹음 시간 제한
							trigger_demostop = setTimeout(function (){
								if(timeLimit_trigger==1) {
									$('.demoStop').trigger('click');
								}
							}, 9000);

						}).catch(function (e) {
							// 사용자 마이크 연결 안 됐을 때
							alert('The microphone is not connected. Please connect the microphone and try again.');
						});
			});

			// step02 > step03
			$('.demoStop').on('click', function(){
				timeLimit_trigger = 0;
				clearTimeout(trigger_demostop);

				var _AudioFormat = "audio/wav";
				stopRecording(function(AudioBLOB){
					// Note:
					// Use the AudioBLOB for whatever you need, to download
					// directly in the browser, to upload to the server, you name it !

					// In this case we are going to add an Audio item to the list so you
					// can play every stored Audio
					var url = URL.createObjectURL(AudioBLOB);
					var li = document.createElement('li');
					var au = document.createElement('audio');
					var hf = document.createElement('a');

					au.controls = true;
					au.src = url;
					hf.href = url;
					// Important:
					// Change the format of the file according to the mimetype
					// e.g for audio/wav the extension is .wav
					//     for audio/mpeg (mp3) the extension is .mp3

					// hf.download = new Date().toISOString() + '.pcm';
					// hf.innerHTML = hf.download;
					// li.appendChild(au);
					// li.appendChild(hf);
					// recordingslist.appendChild(li);


					// stop 버튼을 누르는 순간 submit 하도록 바꾸자.....
					post_submit(AudioBLOB, modelStr);

				}, _AudioFormat);

				$('.rightBox .demo_intro').hide();
				$('.rightBox .demo_recording').show();
				$('.progress li:nth-child(2)').addClass('active');
				$('.rightBox .demo_infoTxt').removeClass('errorTxt');
			});

			(function() {
				// strict mode
				"use strict";
				// configurable options that determine particle behavior and appearance
				var options = {
							// connector line options
							connector: {
								// color of line
								color: "white",
								// base line width of each connector
								lineWidth: 0.5
							},
							// individual particle options
							particle: {
								// base color
								color: "white",
								// number of particles to render onto stage
								count: 75,
								// size of particle influence when drawing connectors (in pixels)
								influence: 75,
								// a range of sizes for an individual particle (falls within max-min)
								sizeRange: {
									min: 1,
									max: 5
								},
								// a range of velocities for each particle to inherit (falls within max-min)
								velocityRange: {
									min: 10,
									max: 60
								}
							}
						},
						particles,
						stage;
				// sets the render loop state based on page's visibility
				function handleDocumentVisibilityChange() {
					document.hidden ?
							stage.render.pause() :
							stage.render.resume();
				}

				function initPage() {
					initObjects();
					initListeners();
				}
				// invoked when page has loaded
				// registers window and document level event listeners
				// required post object instantiation
				function initListeners() {
					// resize canvas on window resize
					window.addEventListener(
							"resize",
							stage.resize);
					// pause and resume rendering loop when visibility state is changed
					document.addEventListener(
							"visibilitychange",
							handleDocumentVisibilityChange);
				}
				// invoked when page has loaded
				// performs setup of stage and particles
				// triggers the rendering loop
				function initObjects() {
					particles = new ParticleGroup();
					// create our stage instance
					stage = new Stage(document.querySelector("canvas"));
					// init the canvas bounds and fidelity
					stage.resize();
					// populate particle group collection
					particles.populate();
					// begin stage rendering with the renderAll draw routine on each tick
					stage.render.begin(
							particles.render.bind(particles));
				}
				// renders an FPS value to the canvas top-left corner
				function renderFPS(value) {
					context.save();
					context.font = 10 * stage.pixelRatio + "px sans-serif";
					context.fillStyle = "white";
					context.textBaseline = "top";
					context.fillText(value, 5 * stage.pixelRatio, 5 * stage.pixelRatio);
					context.restore();
				}
				// Paricle object construct:
				// stores critical particle information regarding trajectory and location
				// 		v: distance delta coefficient
				// 		t: angular theta in degrees
				//		r: particle radius
				//		p: position on stage, x:y coordinate properties
				// 		influence: radial influence of connectivity to other particles
				// defines methods relating to particle rendering and comparison to other particle locations
				function Particle(props) {
					// private particle properties
					var x = props.x || 0,
							y = props.y || 0,
							v = props.speed,
							t = props.theta,
							r = props.radius,
							influence = props.influence,
							color = props.color;
					// public properties and methods
					return {
						// position property getter: read-only
						get x() {
							return x;
						},
						get y() {
							return y;
						},
						// compares this particle location to another
						// returns a coefficient describing the ratio of closeness to particle influence
						influencedBy: function(a, b) {
							var hyp = Math.sqrt(Math.pow(a - x, 2) + Math.pow(b - y, 2));
							return Math.abs(hyp) <= influence ?
									hyp / influence : 0;
						},
						// render this particle to a given context
						render: function(ctx) {
							ctx.strokeStyle = color;
							ctx.lineWidth = 1 / r * r * stage.pixelRatio;
							ctx.beginPath();
							ctx.arc(x || 10, y || 10, r, 0, 8);
							ctx.stroke();
						},
						// sets the position of this particle compared to its previous position
						// in relation to a total time delta since last positioning
						// positions infinitely loop within canvas bounds
						setPosition: function(timeDelta) {
							x += timeDelta / v * Math.cos(t);
							x = x > stage.width + r ? 0 - r : x;
							x = x < 0 - r ? stage.width + r : x;
							y += timeDelta / v / 2 * Math.sin(t);
							y = y > stage.height + r ? 0 - r : y;
							y = y < 0 - r ? stage.height + r : y;
						}
					}
				}
				// ParticleGroup object construct
				// an object which controls higher level methods to create, destroy and compare
				// a grouping of Particle instances. Holds a private collection of all existing particles
				function ParticleGroup() {
					// particle instance array
					var _collection = [],
							// cache connector option property values
							_connectColor = options.connector.color,
							_connectWidth = options.connector.lineWidth,
							_particleCount = options.particle.count;
					// generates and returns a new particle instance with
					// randomized properties within ranges defined in the options object
					function _generateNewParticle() {
						return new Particle({
							x: stage.width * Math.random(),
							y: stage.height * Math.random(),
							speed: getRandomWithinRange(options.particle.velocityRange) / stage.pixelRatio,
							radius: getRandomWithinRange(options.particle.sizeRange),
							theta: Math.round(Math.random() * 360),
							influence: options.particle.influence * stage.pixelRatio,
							color: options.particle.color
						});
						// random number generator within a given range object defined by a max and min property
						function getRandomWithinRange(range) {
							return ((range.max - range.min) * Math.random() + range.min) * stage.pixelRatio;
						}
					}
					// loops through particle collection
					// sets the new particle location given a time delta
					// queries other particles to see if a connection between particles should be rendered
					function _checkForNeighboringParticles(ctx, p1) {
						// cache particle influence method
						var getInfluenceCoeff = p1.influencedBy;
						// particle collection iterator
						for (var i = 0, p2, d; i < _particleCount; i++) {
							p2 = _collection[i];
							// conditional checks - ignore if p2 is the same as p1
							// or if p2 has already been iterated through to check for neighbors
							if (p1 !== p2 && !p2.checked) {
								// compare the distance delta between the two particles
								d = getInfluenceCoeff(p2.x, p2.y);
								// render the connector if coefficient is non-zero
								if (d) {
									_connectParticles(ctx, p1.x, p1.y, p2.x, p2.y, d);
								}
							}
						}
					}
					// given two particles and an influence coefficient between them,
					// renders a connecting line between the two particles
					// the coefficient determines the opacity (strength) of the connection
					function _connectParticles(ctx, x1, y1, x2, y2, d) {
						ctx.save();
						ctx.globalAlpha = 1 - d;
						ctx.strokeStyle = _connectColor;
						ctx.lineWidth = _connectWidth * (1 - d) * stage.pixelRatio;
						ctx.beginPath();
						ctx.moveTo(x1, y1);
						ctx.lineTo(x2, y2);
						ctx.stroke();
						ctx.restore();
					}
					// public object
					return {
						// returns the size of the collection
						get length() {
							return _collection.length;
						},
						// adds a particle object instance to the collection
						add: function(p) {
							_collection.push(p || _generateNewParticle());
						},
						// initial population of bar collection
						populate: function() {
							for (var i = 0; i < _particleCount; i++) {
								this.add();
							}
						},
						// loops through all particle instances within collection and
						// invokes instance rendering method on a given context at tick coefficient t
						render: function(ctx, t) {
							// loop through each particle, position it and render
							for (var i = 0, p; i < _particleCount; i++) {
								p = _collection[i];
								p.checked = false;
								p.setPosition(t);
								p.render(ctx);
							}
							// loop through each particle, check for connectors to be rendered with neighboring particles
							for (var i = 0, p; i < _particleCount; i++) {
								p = _collection[i];
								_checkForNeighboringParticles(ctx, p);
								p.checked = true;
							}
						}
					};
				}
				// Stage object construct
				// provides a structure to store a canvas and its context upon isntantiation
				// provides methods to interact with and adjust the canvas
				function Stage(canvasEl) {
					// canvas element reference
					var canvas = canvasEl instanceof Node ?
							canvasEl :
							document.querySelector(canvasEl),
							// 2d context reference
							context = canvas.getContext("2d"),
							// cache the device pixel ratio
							pixelRatio = window.devicePixelRatio;
					// resize the canvas initially to fit its containing element
					function _adjustCanvasBounds() {
						var parentSize = canvas.parentNode.getBoundingClientRect();
						canvas.width = parentSize.width;
						canvas.height = parentSize.height;
					}
					// updates the canvas dimensions relative to the device's pixel ratio for highest fidelity and accuracy
					function _adjustCanvasFidelity() {
						canvas.style.width = canvas.width + "px";
						canvas.style.height = canvas.height + "px";
						canvas.width *= pixelRatio;
						canvas.height *= pixelRatio;
					}
					// public object returned
					return {
						// dimension getters
						get height() {
							return canvas.height;
						},
						get width() {
							return canvas.width;
						},
						get pixelRatio() {
							return pixelRatio;
						},
						init: function(el) {
							canvas = el;
							context = canvas.getContext("2d");
						},
						resize: function() {
							_adjustCanvasBounds();
							_adjustCanvasFidelity();
						},
						render: (function() {
							// a flag indicating state of animation loop
							var paused = false,
									// animation request ID reported during loop
									requestID = null,
									// reference to a function detailing all draw operations per frame of animation
									renderMethod;
							// public methods
							return {
								// once invoked, creates a rendering loop which in turn invokes
								// drawMethod argument, passing along the delta (in milliseconds) since the last frame draw
								begin: function(drawMethod) {
									// cache the draw method to invoke per frame
									renderMethod = drawMethod;
									// a reference to requestAnimationFrame object
									// this should also utilize some vendor prefixing and a setTimeout fallback
									var requestFrame = window.requestAnimationFrame,
											// get initial start time
											latestTime, startTime = Date.now(),
											// during each interval, clear the canvas and invoke renderMethod
											intervalMethod = function(tick) {
												this.clear();
												renderMethod(
														context,
														tick);
											}.bind(this);
									// start animation loop
									(function loop() {
										// calculate tick time between frames
										var now = Date.now(),
												tick = now - latestTime || 1;
										// update latest time stamp
										latestTime = now;
										// report tick value to intervalCallback
										intervalMethod(tick);
										// loop iteration if no pause state is set
										requestID = paused ?
												null :
												requestAnimationFrame(loop);
									})();
								},
								// clears the stage's canvas
								clear: function() {
									context.clearRect(0, 0, canvas.width, canvas.height);
								},
								// pause the canvas rendering
								pause: function() {
									paused = true;
									cancelAnimationFrame(requestID);
								},
								// resumes the animation with a given rendering method
								resume: function() {
									paused = false;
									this.begin(renderMethod);
								}
							}
						}())
					};
				}

				// our init is triggered when the window is loaded/ready
				window.addEventListener(
						"load",
						initPage);
			}());



		});
	});


//API 탭  	
function openTap(evt, menu) {
  var i, demobox, tablinks;
  demobox = document.getElementsByClassName("demobox");
  for (i = 0; i < demobox.length; i++) {
    demobox[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(menu).style.display = "block";
  evt.currentTarget.className += " active";
}	
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();	

</script>
