<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-05-21
  Time: 15:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<%@ include file="../common/common_header_v2.jsp" %>

<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/aiaas/en/css/landing_page_v2/white_header.css"/>


<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->


<%@ include file="../common/header.jsp" %>


<div id="wrap">
    <!-- aside -->
    <div class="aside">
        <!-- .aside_top -->
        <div class="aside_top">
            <h1><a href="/?lang=en"><img
                    src="${pageContext.request.contextPath}/aiaas/en/images/landing_page_v2/logo_maumai_bk.svg"
                    alt="maumAI logo"></a></h1>

            <!-- 로그인 X인 경우 -->
            <sec:authorize access="isAnonymous()">
                <a class="btn_sign" href="javascript:login()">Sign In</a>
            </sec:authorize>

            <!-- 로그인 O인 경우 -->
            <sec:authorize access="isAuthenticated()">
                <a class="btn_sign" href="#none">${fn:escapeXml(sessionScope.accessUser.email)}</a>
            </sec:authorize>

        </div>
        <!-- //.aside_top -->
        <!-- .aside_mid -->
        <div class="aside_mid">
            <ul class="m_nav">
                <li>
                    <h2><a href="#none" class="">My Page <em class="fas fa-chevron-down"></em></a></h2>
                    <ul class="m_lst">
                        <li>
                            <h3><a href="/user/profileMain" target="_blank" title="profile"
                                   class="layer_m_btn">PROFILE</a></h3>
                            <h3><a href="/user/apiAccountMain" target="_blank" title="api_account" class="layer_m_btn">API
                                ACCOUNT</a></h3>
                            <h3><a href="/user/paymentInfoMain" target="_blank" title="payment" class="layer_m_btn">PAYMENT</a>
                            </h3>
                            <h3><a href="#"
                                   onclick="document.getElementById('logout-form').submit();">LOGOUT</a></h3>
                        </li>

                    </ul>
                </li>

                <li>
                    <h2><a href="#none" class="">Service <em class="fas fa-chevron-down"></em></a></h2>
                    <ul class="m_lst">
                        <li>
                            <h3><a href="https://builder.maum.ai/enLanding" class="layer_m_btn">AI Builder</a></h3>
                        </li>
                        <li>
                            <h3><a href="/main/enMainHome" class="layer_m_btn">Cloud API</a></h3>
                        </li>
                        <li>
                            <h3><a href="https://fast-aicc.maum.ai" class="layer_m_btn">FAST Conversational AI</a></h3>
                        </li>
                        <li>
                            <h3><a href="https://minutes.maum.ai/" class="layer_m_btn">Maum Minutes</a></h3>
                        </li>
                    </ul>
                </li>

                <li><h2><a href="#ecoMINDs" class="go_ecominds">ecoMINDs</a></h2></li>
                <li><a href="#" class="go_const">AI Consultant</a></li>
                <li><h2><a href="/home/pricingPage?lang=en">Pricing</a></h2></li>
                <li><h2><a href="https://mindslab.ai:8080/en" target="_blank" title="MINDsLab">About Us</a></h2></li>

            </ul>
        </div>
        <!-- //.aside_mid -->

        <!-- .aside_btm -->
        <div class="aside_btm">
            <span>English</span>
            <span><a href="/?lang=kr" target="_self">Korean</a></span>
        </div>
        <!-- .aside_btm -->

    </div>
    <div class="bg_aside"></div>
    <a class="btn_header_ham" href="#none"><span class="hamburger_icon"></span></a>
    <!-- //.aside -->


    <!-- #container -->
    <div class="user_container">
        <!-- .contents -->
        <div class="contents">
            <!--content-->
            <div class="content">
                <h1>PROFILE</h1>
                <!-- . -->

                <div class="mypageBox">
                    <div class="contentarea">
                        <div class="basic_tit">
                            <!--						<img src="resources/images/img_profile.png" alt="profile img">-->
                            <form id="form" runat="server">
                                <div class="imginput">
                                    <img id="image_section"
                                         src="${pageContext.request.contextPath}/aiaas/kr/images/img_profile.png"
                                         alt="your image"/>
                                </div>
                            </form>

                        </div>
                        <div class="basic_info">
                            <form id="memberForm" name="memberForm" method="post">
                                <input type="hidden" id="userEmail" name="email" value="${fn:escapeXml(sessionScope.accessUser.email)}"/>
                                <input type="hidden" id="category2" name="category2" value="en"/>
                                <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                <h5>Basic Information</h5>
                                <ul>
                                    <li>
                                        <span>Name</span>
                                        <input type="text" id="name" name="name" value="" maxlength="20"
                                               placeholder="Name"
                                               class="username">
                                    </li>
                                    <li>
                                        <span>Email</span>
                                        <em>${fn:escapeXml(sessionScope.accessUser.email)}</em>
                                    </li>
                                    <!-- <li>
                                        <span>Password</span>
                                        <button class="pw_edit">비밀번호 변경</button>
                                    </li>
                                    <li>
                                        <span>언어 선택</span>
                                        <div class="selectType">
                                            <label for="language">언어선택하기</label>
                                            <select id="language">
                                                <option value="langChoice">언어</option>
                                                <option value="kor">한국어</option>
                                                <option value="eng">영어</option>
                                            </select>
                                        </div>
                                    </li> -->
                                </ul>
                                <h5>Optional</h5>
                                <ul>
                                    <li>
                                        <span>Company</span>
                                        <input type="text" id="company" name="company" value="" maxlength="40">
                                    </li>
                                    <li>
                                        <span>Business Email</span>
                                        <input type="text" id="companyEmail" name="companyEmail" value="" maxlength="40">
                                    </li>
                                    <li>
                                        <span>Job Title</span>
                                        <input type="text" id="job" name="job" value="" maxlength="40">
                                    </li>
                                </ul>
                            </form>
                        </div>
                        <div class="btn">
                            <a onclick="setUpdate();" class="btn_profile_save">Save</a>
                        </div>
                    </div>
                </div>

            </div>
            <!--content-->

        </div>
        <!-- //.contents -->
    </div>

</div>

<script type="text/javascript">

    $(document).ready(function () {
        // getNationList();
        getMemberDetail();
    });

    // /** 국적 리스트 조회  */
    // function getNationList() {
    //     $.ajax({
    //         url: "/common/getNationSelectList",
    //         data: $("#memberForm").serialize(),
    //         dataType: "JSON",
    //         cache: false,
    //         async: true,
    //         type: "POST",
    //         success: function (data) {
    //             $("#nationlist").html($("#nationTemplate").render(data));
    //         },
    //         error: function (xhr, status, error) {
    //         }
    //     });
    // }

    /** 계정 - 상세 조회  */
    function getMemberDetail() {
        let email = $("#userEmail").val();

        if (email != "") {
            $.ajax({
                url: "/member/getDetail",
                data: $("#memberForm").serialize(),
                dataType: "JSON",
                cache: false,
                async: true,
                type: "POST",
                success: function (obj) {
                    getMemberDetailCallback(obj);
                },
                error: function (xhr, status, error) {
                }
            });
        }
    }


    /** 계정 - 상세 조회  콜백 함수 */
    function getMemberDetailCallback(obj) {

        if (obj != null) {
            let name = obj.name;
            let companyEmail = obj.companyEmail;
            let company = obj.company;
            let job = obj.job;

            $("#name").val(name);
            $("#company").val(company);
            $("#companyEmail").val(companyEmail);
            $("#job").val(job);

        }
    }


    /** 프로필 저장 */
    function setUpdate() {

        let name = $("#name").val();

        if (name == "") {
            alert("Input your name, please.");
            $("#name").focus();
            return;
        }

        let yn = confirm("Do you want to save it?");
        if (yn) {

            $.ajax({
                type: "POST",
                url: '/member/setUpdate',
                data: $("#memberForm").serialize(),
                dataType: "JSON",
                cache: false,
                async: true,
                success: function (obj) {
                    setUpdateCallback(obj);
                },
                error: function (xhr, status, error) {
                }
            });
        }
    }

    function setUpdateCallback(obj) {

        if (obj != null) {

            let result = obj.result;

            if (result !== "SUCCESS") {
                alert("Save failed.");
            }
        }
    }


</script>

<%--<script id="nationTemplate" type="text/x-jsrender">
									<span>Location</span>
									<div class="selectType">
										<label for="nationcd">한국</label>
										<select id="nationcd" name="nationcd" title="국적선택하기">
{{for data.list}}
											<option value="{{>category3}}">{{>name}}</option>
{{/for}}
										</select>
									</div>

</script>--%>