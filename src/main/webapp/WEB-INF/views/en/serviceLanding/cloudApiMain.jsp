<%--
  Created by IntelliJ IDEA.
  User: bliss
  Date: 2020-03-04
  Time: 오후 3:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<%--  CSS/JS 파일 캐싱 방지 --%>
<%

    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">

    <!-- Open Graph Tag -->
    <meta property="og:title"            content="maum.ai"/>
    <meta property="og:type"             content="website"/><!-- 웹 페이지 타입 -->
    <meta property="og:url"              content="https://maum.ai"/>
    <meta property="og:image"            content="${pageContext.request.contextPath}/aiaas/common/images/maum.ai_web.png"/>
    <meta property="og:description"      content="Make you own AI Service with maum.ai"/>

    <!-- icon_favicon -->
    <link rel="apple-touch-icon-precomposed" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.png">
    <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/aiaas/kr/images/ico_maumAI_60x60.ico">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/common/css/all.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/en/css/reset.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/en/css/font.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/en/css/login_renew.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/en/css/sublanding.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/jquery-1.11.2.min.js"></script>


    <title>Cloud API</title>
</head>
<body>
<!-- .page loading -->
<div id="page_ldWrap" class="page_loading">
    <div class="loading_itemBox">
        <span></span>
        <span></span>
        <span></span>
        <span></span>
    </div>
</div>
<!-- //.page loading -->
<div class="aside" style="display:none;">
    <!-- .aside_top -->
    <div class="aside_top">
        <h1><a href="/"><img src="${pageContext.request.contextPath}/aiaas/common/images/maumai.svg" alt="maumAI logo"></a></h1>
    </div>
    <!-- //.aside_top -->
    <!-- .aside_mid -->
    <div class="aside_mid">
        <ul class="m_nav">
            <li>
                <h2>Service view all <em class="fas fa-chevron-down"></em></h2>
                <ul class="m_lst">
                    <li>
                        <h3><a href="https://builder.maum.ai/?lang=en">AI Builder</a></h3>
                    </li>
                    <li class="active">
                        <h3><a href="#none">Cloud API</a></h3>
                    </li>
                    <li>
                        <h3><a href="https://minutes.maum.ai/?lang=en">maum Minutes</a></h3>
                    </li>
                    <li>
                        <h3><a href="https://fast-aicc.maum.ai/login?lang=en">FAST Conversational AI</a></h3>
                    </li>
                    <li>
                        <h3><a href="https://data.maum.ai/?lang=en">maum DATA</a></h3>
                    </li>
                </ul>
            </li>
            <li>
                <h2><a href="/home/pricingPage?lang=en">Pricing</a></h2>
            </li>
        </ul>
    </div>
    <!-- //.aside_mid -->

    <!-- .aside_btm -->
    <div class="aside_btm">
        <ul>
            <li class=""><span>English</span></li>
            <li class=""><span><a href="/serviceLanding/krCloudApiMain">한국어</a></span></li>
        </ul>
    </div>
    <!-- //.aside_btm -->
</div>

<div class="bg_aside"></div>
<a class="btn_header_ham" href="#none"><span class="hamburger_icon"></span></a>
<!-- //aside -->
<!-- #header -->
<div id="header">
    <!-- .group_flex -->
    <div class="group_flex">
        <h1>
            <a href="https://maum.ai/" target="_blank"><img src="${pageContext.request.contextPath}/aiaas/common/images/maumai.svg" alt="maum.ai logo" /></a>
        </h1>
        <!-- .sta -->
        <div class="sta">

            <!-- .GNB-->
            <div class="gnb">
                <ul class="nav">
                    <li><a href="https://maum.ai/home/pricingPage" target="_blank">Pricing</a></li>
                </ul>
            </div>
            <!-- .GNB-->

            <!-- #menu(service menu) -->
            <div id="menu">
                <ul>
                    <li>
                        <p class="service_btn">Our Service &nbsp;&nbsp;
                            <em class="fas fa-angle-down"></em>
                            <em class="fas fa-angle-up"></em>
                        </p>
                        <ul class="dropdown-menu">
                            <li><a href="https://builder.maum.ai/?lang=en" title="AI Builder" target="_blank">AI Builder</a></li>
                            <li class="active"><a href="https://maum.ai/main/enMainHome" title="Cloud API" target="_blank">Cloud API</a></li>
                            <li><a href="https://minutes.maum.ai/?lang=en"  title="maum Minutes" target="_blank">maum Minutes</a></li>
                            <li><a href="https://fast-aicc.maum.ai/login?lang=en" target="_blank" title="FAST Conversational AI">FAST Conversational AI</a></li>
                            <li><a href="https://data.maum.ai/?lang=en" target="_blank" title="maum DATA">maum DATA</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <!-- //#menu(service menu) -->

            <!-- language -->
            <div class="lang_box">
                <span>English</span>
                <span><a href="/serviceLanding/krCloudApiMain" target="_self" title="한국어">한국어</a></span>
            </div>
            <!-- //language -->
        </div>
        <!-- //.sta -->
    </div>
</div>
<!-- //#header -->
<!-- #container -->
<div id="container">
    <!-- .contents -->
    <div class="contents">
        <h1>Cloud API</h1>
        <h5>The easiest way to use AI right now! </h5>
        <!-- .stn  -->
        <div class="stn ">
            <!-- .cont_box -->
            <div class="cont_box">
                <!-- .fl_box -->
                <div class="fl_box cloud_box">
                    <svg class="arrow_01 arrow_up" width="34px" height="30px" viewBox="0 0 34 30" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 63.1 (92452) - https://sketch.com -->
                        <title>arrow_01</title>
                        <desc>Created with Sketch.</desc>
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g class="arrow_path" id="cloud_path1" transform="translate(-226.000000, -475.000000)" fill="#5E4DF8" fill-rule="nonzero">
                                <path d="M229.213022,501.556993 L229.874644,502.306831 L226.875292,504.953318 L226.21367,504.20348 L229.213022,501.556993 Z"></path>
                                <path d="M235.211725,496.26402 L235.873346,497.013858 L232.873995,499.660345 L232.212373,498.910507 L235.211725,496.26402 Z"></path>
                                <path d="M241.210428,490.971047 L241.872049,491.720885 L238.872698,494.367372 L238.211076,493.617534 L241.210428,490.971047 Z"></path>
                                <path d="M247.20913,485.678074 L247.870752,486.427912 L244.871401,489.074399 L244.209779,488.324561 L247.20913,485.678074 Z"></path>
                                <path d="M259.874919,475.169189 L256.103676,484.498054 L253.456,481.498 L250.870103,483.781425 L250.208482,483.031588 L252.795,480.748 L250.149081,477.749514 L259.874919,475.169189 Z" id="arrow_01"></path>

                            </g>

                        </g>
                    </svg>
                    <svg class="arrow_02 arrow_down" width="20px" height="61px" viewBox="0 0 20 61" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 63.1 (92452) - https://sketch.com -->
                        <title>arrow_02</title>
                        <desc>Created with Sketch.</desc>
                        <g id="Page-2" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g class="arrow_path" id="cloud_path2" transform="translate(-289.000000, -475.000000)" fill="#5E4DF8" fill-rule="nonzero">
                                <path d="M289.487552,526.095211 L298.146692,528.548634 L291.363699,535.981063 L289.487552,526.095211 Z"></path>
                                <path d="M294.934013,521.545782 L295.89614,521.818384 L294.80573,525.666891 L293.843603,525.394288 L294.934013,521.545782 Z"></path>
                                <path d="M297.114834,513.848768 L298.07696,514.12137 L296.98655,517.969877 L296.024423,517.697275 L297.114834,513.848768 Z"></path>
                                <path d="M299.295654,506.151754 L300.257781,506.424357 L299.167371,510.272863 L298.205244,510.000261 L299.295654,506.151754 Z"></path>
                                <path d="M301.476475,498.45474 L302.438602,498.727343 L301.348191,502.57585 L300.386065,502.303247 L301.476475,498.45474 Z"></path>
                                <path d="M303.657295,490.757726 L304.619422,491.030329 L303.529012,494.878836 L302.566885,494.606233 L303.657295,490.757726 Z M305.838116,483.060713 L306.800243,483.333315 L305.709832,487.181822 L304.747706,486.909219 L305.838116,483.060713 Z M308.018937,475.363699 L308.981063,475.636301 L307.890653,479.484808 L306.928526,479.212206 L308.018937,475.363699 Z" id="arrow_02"></path>
                            </g>
                        </g>
                    </svg>
                    <svg class="arrow_03" width="10px" height="44px" viewBox="0 0 10 44" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 63.1 (92452) - https://sketch.com -->
                        <title>arrow_03</title>
                        <desc>Created with Sketch.</desc>
                        <g id="Page-3" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g class="arrow_path"  id="cloud_path3" transform="translate(-362.000000, -475.000000)" fill="#5E4DF8" fill-rule="nonzero">
                                <path d="M367.5,516 L367.5,519 L366.5,519 L366.5,516 L367.5,516 Z"></path>
                                <path d="M367.5,508 L367.5,512 L366.5,512 L366.5,508 L367.5,508 Z"></path>
                                <path d="M367.5,500 L367.5,504 L366.5,504 L366.5,500 L367.5,500 Z"></path>
                                <path d="M367.5,492 L367.5,496 L366.5,496 L366.5,492 L367.5,492 Z"></path>
                                <path d="M367,475.5 L371.5,484.5 L367.5,484.5 L367.5,488 L366.5,488 L366.5,484.5 L362.5,484.5 L367,475.5 Z" id="arrow_03"></path>
                            </g>
                        </g>
                    </svg>
                    <svg class="arrow_04" width="10px" height="44px" viewBox="0 0 10 44" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 63.1 (92452) - https://sketch.com -->
                        <title>arrow_04</title>
                        <desc>Created with Sketch.</desc>
                        <g id="Page-4" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g class="arrow_path" id="cloud_path4" transform="translate(-387.000000, -476.000000)" fill="#5E4DF8" fill-rule="nonzero">
                                <path d="M392.5,508 L392.5,510.5 L396.5,510.5 L392,519.5 L387.5,510.5 L391.5,510.5 L391.5,508 L392.5,508 Z"></path>
                                <path d="M392.5,500 L392.5,504 L391.5,504 L391.5,500 L392.5,500 Z"></path>
                                <path d="M392.5,492 L392.5,496 L391.5,496 L391.5,492 L392.5,492 Z "></path>
                                <path d="M392.5,484 L392.5,488 L391.5,488 L391.5,484 L392.5,484 Z"></path>
                                <path d="M392.5,476 L392.5,480 L391.5,480 L391.5,476 L392.5,476 Z" id="arrow_04"></path>
                            </g>
                        </g>
                    </svg>
                    <svg class="arrow_05 arrow_up" width="23px" height="58px" viewBox="0 0 23 58" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 63.1 (92452) - https://sketch.com -->
                        <title>arrow_05</title>
                        <desc>Created with Sketch.</desc>
                        <g id="Page-5" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g class="arrow_path" id="cloud_path5" transform="translate(-457.000000, -475.000000)" fill="#5E4DF8" fill-rule="nonzero">
                                <path d="M477.729711,528.035239 L479.07101,531.80365 L478.128907,532.138974 L476.787609,528.370564 L477.729711,528.035239 Z"></path>
                                <path d="M475.047114,520.498418 L476.388413,524.266829 L475.44631,524.602153 L474.105011,520.833743 L475.047114,520.498418 Z"></path>
                                <path d="M472.364517,512.961597 L473.705815,516.730008 L472.763713,517.065332 L471.422414,513.296922 L472.364517,512.961597 Z"></path>
                                <path d="M469.681919,505.424776 L471.023218,509.193187 L470.081115,509.528511 L468.739817,505.760101 L469.681919,505.424776 Z"></path>
                                <path d="M466.999322,497.887955 L468.340621,501.656366 L467.398518,501.99169 L466.057219,498.22328 L466.999322,497.887955 Z"></path>
                                <path d="M464.316725,490.351134 L465.658023,494.119545 L464.715921,494.454869 L463.374622,490.686459 L464.316725,490.351134 Z"></path>
                                <path d="M458.332338,475.028949 L465.589721,481.998911 L461.821,483.339 L462.975426,486.582723 L462.033323,486.918048 L460.879,483.674 L457.110798,485.016833 L458.332338,475.028949 Z" id="arrow_05"></path>
                            </g>
                        </g>
                    </svg>
                    <svg class="arrow_06 arrow_down" width="41px" height="31px" viewBox="0 0 41 31" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                        <!-- Generator: Sketch 63.1 (92452) - https://sketch.com -->
                        <title>arrow_06</title>
                        <desc>Created with Sketch.</desc>
                        <g id="Page-6" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g class="arrow_path" id="cloud_path6" transform="translate(-513.000000, -475.000000)" fill="#5E4DF8" fill-rule="nonzero">
                                <path d="M549.4,496.8 L553.9,505.8 L544,504 L546.4,500.8 L545.2,499.9 L545.8,499.1 L547,500 L549.4,496.8 Z"></path>
                                <path d="M539.4,494.3 L542.6,496.7 L542,497.5 L538.8,495.1 L539.4,494.3 Z"></path>
                                <path d="M533,489.5 L536.2,491.9 L535.6,492.7 L532.4,490.3 L533,489.5 Z"></path>
                                <path d="M526.6,484.7 L529.8,487.1 L529.2,487.9 L526,485.5 L526.6,484.7 Z"></path>
                                <path d="M520.2,479.9 L523.4,482.3 L522.8,483.1 L519.6,480.7 L520.2,479.9 Z"></path>
                                <path d="M513.8,475.1 L517,477.5 L516.4,478.3 L513.2,475.9 L513.8,475.1 Z" id="arrow_06"></path>
                            </g>
                        </g>
                    </svg>


                </div>
                <!-- //.fl_box -->
                <!-- .fr_box -->
                <div class="fr_box">
                    <ul>
                        <li>1 Month Free Trial</li>
                        <li><span>1</span>Explore our 30+ AI engines</li>
                        <li><span>2</span>Apply AI to my business (using REST API)</li>
                        <li><span>3</span>Check and track your usage</li>
                    </ul>
                    <a href="javascript:go_sso_login()" title="Cloud API Start" target="_parent" style="padding:31px;">
                        Start Cloud API
                        <%--<span>Sign in with Google</span>--%>
                    </a>
                </div>
                <!-- //.fr_box -->
            </div>
            <!-- //.cont_box -->
        </div>
        <!-- //.stn  -->
    </div>
    <!-- //.contents -->

    <iframe id="footerFrame" src="https://maum.ai/serviceLanding/enFooter"></iframe>


</div>
<!-- //#container -->

<script type="text/javascript">

    let $ClientId = "${client_id}";
    let $RedirectUri = "${redirect_uri}";
    let $SsoUrl = "${sso_url}";

    $(window).load(function() {
        $('.page_loading').addClass('pageldg_hide').delay(300).queue(function() {
            $(this).remove();
        });
    });
    $(document).ready(function() {

        var svg_lang = $('.arrow_path').length;
        console.log(svg_lang);

        var step_term =setInterval(step, 800);

        var j = 1;


        function step() {

            var str = $('#cloud_path'+j).attr('id');
            var id_value = str.substring(10,11);
            var item = $('#cloud_path'+j).children('path');
            var item_lng = item.length;

            item.animate({opacity:0},200);

            if(id_value == '1') {

                var stop =setInterval(changeColor, 150);
                var i = 1;
                function changeColor(){
                    var cont = $('#cloud_path1 path:nth-child('+i+')');
                    $(cont).animate({opacity:1},200);
                    i++;
                    if ( i > 5){
                        clearInterval(stop);
                    }

                }
            }else if (id_value == '2' ){

                var stop =setInterval(changeColor, 150);
                var i = 6;
                function changeColor(){
                    var cont = $('#cloud_path2 path:nth-child('+i+')');
                    $(cont).animate({opacity:1},200);
                    i--;

                    if ( i < 1){
                        clearInterval(stop);
                    }

                }

            }else if (id_value == '3' ){

                var stop =setInterval(changeColor, 150);
                var i = 1;
                function changeColor(){
                    var cont = $('#cloud_path3 path:nth-child('+i+')');
                    $(cont).animate({opacity:1},200);
                    i++;

                    if ( i > 5){
                        clearInterval(stop);
                    }

                }

            }else if (id_value == '4' ){

                var stop =setInterval(changeColor, 150);
                var i = 5;
                function changeColor(){
                    var cont = $('#cloud_path4 path:nth-child('+i+')');
                    $(cont).animate({opacity:1},200);
                    i--;

                    if ( i < 1){
                        clearInterval(stop);
                    }

                }

            }else if (id_value == '5' ){

                var stop =setInterval(changeColor, 150);

                var i = 1;
                function changeColor(){
                    var cont = $('#cloud_path5 path:nth-child('+i+')');
                    $(cont).animate({opacity:1},200);
                    i++;

                    if ( i > 7){
                        clearInterval(stop);
                    }
                }
            }else if (id_value == '6' ){

                var stop =setInterval(changeColor, 150);

                var i = 6;
                function changeColor(){
                    var cont = $('#cloud_path6 path:nth-child('+i+')');
                    $(cont).animate({opacity:1},200);
                    i--;

                    if ( i < 1 ){
                        clearInterval(stop);
                    }
                }
            }
            j++;
            if (j > 6 ){
                j = 1;
            }
        }
        $(".dropdown-menu li a").on('click', function () {
            $('.dropdown-menu').hide().parent().parent().removeClass('active');
            $(".lst").hide().parent().parent().removeClass('active');
        });

        $(".service_btn").on('click', function () {
            $(this).parent().parent().toggleClass('active');
            $(".dropdown-menu").slideToggle(200);
        });
        $(".ico_user").on('click', function () {
            $(this).parent().parent().toggleClass('active');
            $(".lst").slideToggle(200);
        });
        $('#container').on('click', function () {
            $('.dropdown-menu').hide().parent().parent().removeClass('active');
            $(".lst").hide().parent().parent().removeClass('active');

        });

        //aside menu
        var clicked = false;
        var asideWidth = $(window).width();
        $('.aside').show();

        $('a.btn_header_ham').click(function(){
            console.log(clicked);
            if (!clicked) {
                $(this).addClass('active');
                $('.aside').animate({
                    width : asideWidth
                },{duration:200,queue:false});
                $('body').css({
                    overflow : 'hidden'
                });
                clicked=true;
            } else {
                $(this).removeClass('active');
                $('.aside').animate({
                    width: '0'
                },{duration:200,queue:false});
                $('body').css({
                    overflow : ''
                });
                clicked=false;
            }
        });

        $('.bg_aside').on('click',function(){
            $('a.btn_header_ham').removeClass('active');
            $('.aside').animate({
                width: '0'
            },{duration:200,queue:false});
            $('.btn_goTop').show();

            $('.bg_aside').animate({
                opacity : 0,
                display : 'none'
            },{duration:150,queue:false});

            $('body').css({
                overflow : ''
            });
            clicked=false;
        });

    });

    function go_sso_login() {
        window.location.href = $SsoUrl+"/maum/oauthLoginMain" + "?response_type=code&client_id=" + $ClientId + "&redirect_uri=" + encodeURIComponent($RedirectUri);
    }

</script>
</body>
</html>
