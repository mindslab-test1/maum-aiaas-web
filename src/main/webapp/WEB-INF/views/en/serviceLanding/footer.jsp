<%--
  Created by IntelliJ IDEA.
  User: bliss
  Date: 2020-03-25
  Time: 오후 3:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page import="java.io.*, java.util.*, java.text.*" %>

<%--  CSS/JS 파일 캐싱 방지 --%>
<%
    Date lastModifiedStyle = new Date();
    SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMddhhmmssSSS");
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="format-detection" content="telephone=no">

    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/en/css/reset.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/en/css/font.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/aiaas/en/css/footer.css?ver=<%=fmt.format(lastModifiedStyle)%>">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/aiaas/common/css/all.css">
    <script type="text/javascript" src="${pageContext.request.contextPath}/aiaas/en/js/jquery-1.11.2.min.js"></script>

    <title>maum.ai footer</title>
</head>
<body>

<!-- #footer -->
<div id="footer" >
    <div class="btm_menu">
        <div class="cont_box">
            <div class="menu_area">
                <div class="contact">
                    <p>Contact Us</p>
                    <a href="mailto:${email}"><span class="far fa-envelope"></span>${email}</a>
                    <a href="tel:+8216613222"><span class="far fa-comment-dots"></span>1661-3222</a>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="cont_box">
            <span>&copy; Copyright 2020</span>
            <span>Minds Lab</span>
            <span>601 Dasan Tower, 49 Daewangpangyo-ro 644beon-gil, Bundang-gu, Seongnam-si, Gyeonggi-do, Republic of Korea</span>
            <span>CEO Taejoon Yoo </span>
            <span>Company Registration Number 314-86-55446</span>
            <p>
                <a href="https://mindslab.ai:8080/en/company" target="_blank">Company</a>
                <a href="/home/enTermsMain" class="co_link " target="_blank">Terms & Conditions</a>
            </p>
        </div>
    </div>
</div>
<!-- //#footer -->

</body>
</html>
