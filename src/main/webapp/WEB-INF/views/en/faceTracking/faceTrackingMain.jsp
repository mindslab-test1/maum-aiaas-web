<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>Upload error</h5>
            <p>Error occurred due to the image or file size. <br>Please check the restriction again.</p>
            <span> * Supported files: .avi, .mp4<br>* Video file size under 50MB.</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">OK</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents api_content">
    <!-- .content -->
    <div class="content">
        <h1 class="api_tit">Face Tracking</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'ftdemo')" id="defaultOpen">
                <button type="button">AI Engine</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'ftexample')">
                <button type="button">Use Case</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'ftmenu')">
                <button type="button">Manual</button>
            </li>
<%--            <li class="tablinks"><a href="/member/enApiAccount">API ID, key</a></li>--%>
        </ul>
        <!-- .demobox -->
        <div class="demobox" id="ftdemo">
            <p><span>Face Tracking</span></p>
            <span class="sub">Engine which can recognize and extract faces from a video. </span>
            <!--faceTracking_box-->
            <div class="demo_layout faceTracking_box">
                <!--tr_1-->
                <div class="tr_1">
                    <div class="fl_box">
                        <p><em class="far fa-file-video"></em><strong>Use Sample File</strong></p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="option" value="1" checked>
                                    <label for="sample1" class="">
                                        <video id="sampleVideo1" controls width="300" height="200">
                                            <source src="${pageContext.request.contextPath}/aiaas/common/video/faceTracking2.mp4"
                                                    type="video/mp4">
                                            IE 8 and below does not produce video. Please update the IE version.
                                        </video>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-video"></em><strong>Use My File</strong></p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-image hidden"></em>
                                <label for="demoFile" class="demolabel">Upload video</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".avi, .mp4">
                            </div>
                            <ul>
                                <li>* Supported files: .avi, .mp4</li>
                                <li>* Video file size under 50MB.</li>
                                <li>* Use video with a person frontal face, more than 120 x 160 pixels.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">Process</button>
                    </div>
                </div>
                <!--tr_1-->
                <!--tr_2-->
                <div class="tr_2">
                    <p><em class="far fa-file-video"></em>In progress</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px"
                             viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42"
                                                                             d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/>
                            <g>
                                <path fill="#f7778a" fill-opacity="1"
                                      d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/>
                                <animatetransform attributeName="transform" type="translate"
                                                  values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0"
                                                  calcMode="discrete" dur="1820ms" repeatCount="indefinite"/>
                            </g></svg>

                        <p>AI processing takes a while..</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
                    </div>

                </div>
                <!--tr_2-->
                <!--tr_3-->
                <div class="tr_3">

                    <div class="result_file">
                        <p><em class="far fa-file-image"></em>Result</p>
                        <ul class="scenebox">
                            <li class="scene">
                                <div class="imgbox" id="imgBox1">
                                    <img id="resultImg1" src="" alt="Result 1">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime1">Scene 1 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt1">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox" id="imgBox2">
                                    <img id="resultImg2" src="" alt="Result 2">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime2">Scene 2 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt2">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox" id="imgBox3">
                                    <img id="resultImg3" src="" alt="Result 3">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime3">Scene 3 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt3">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox" id="imgBox4">
                                    <img id="resultImg4" src="" alt="Result 4">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime4">Scene 4 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt4">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox" id="imgBox5">
                                    <img id="resultImg5" src="" alt="Result 5">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime5">Scene 5 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt5">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox" id="imgBox6">
                                    <img id="resultImg6" src="" alt="Result 6">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime6">Scene 6 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt6">
                                        </textarea>
                                    </div>
                                </div>
                            </li>

                            <li class="scene">
                                <div class="imgbox" id="imgBox7">
                                    <img id="resultImg7" src="" alt="Result 7">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime7">Scene 7 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt7">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox" id="imgBox8">
                                    <img id="resultImg8" src="" alt="Result 8">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime8">Scene 8 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt8">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox" id="imgBox9">
                                    <img id="resultImg9" src="" alt="Result 9">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime9">Scene 9 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt9">
                                        </textarea>
                                    </div>
                                </div>
                            </li>
                            <li class="scene">
                                <div class="imgbox" id="imgBox10">
                                    <img id="resultImg10" src="" alt="Result 10">
                                </div>
                                <div class="textbox">
                                    <p id="resultTime10">Scene 10 : </p>
                                    <div class="scene_text">
                                        <textarea id="resultTxt10">
                                        </textarea>
                                    </div>
                                </div>
                            </li>

                        </ul>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2" id=""><em class="fas fa-redo"></em>Reset</button>
                    </div>
                </div>
                <!--tr_3-->
            </div>
            <!--//.faceTracking_box-->

        </div>
        <!-- //.demobox -->
        <!--.ftmenu-->
        <div class="demobox vision_menu" id="ftmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API Guideline
                    </div>
                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID &amp; Key</p>
                    <p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                    <p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                    <p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        Face Tracking
                    </div>
                    <p class="sub_txt">Engine which can recognize and extract faces from a video. </p>
                    <span class="sub_title">
								Preparation
					</span>
                    <p class="sub_txt">- Input: Video file </p>
                    <ul>
                        <li>File type: .mp4, .avi</li>
                        <li>Size: Under 50MB </li>
                    </ul>
                    <span class="sub_title">
								API Document
							</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/Vca/faceTracking</li>
                    </ul>
                    <p class="sub_txt">② Request parameters </p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API ID. Request from is required for the ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>video</td>
                            <td>Input video file (.mp4, .avi)</td>
                            <td>string</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
                        url --location --request POST 'http://api.maum.ai/Vca/faceTracking' \<br>
                        --header 'Content-Type: multipart/form-data' \<br>
                        --form 'apiId= Own API ID' \<br>
                        --form 'apiKey= Own API KEY' \<br>
                        --form 'video= Input video file'<br>
                    </div>

                    <p class="sub_txt">④ Response example </p>

                    <div class="code_box">
<pre>
{
    "frame_1": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_1": "[{\"sceneTime\": 80, \"faceId\": null, \"faceRectangle\": {\"left\": null, \"top\": null, \"width\": null, \"height\": null}}]",

    "frame_2": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_2": "[{\"sceneTime\": 82, \"faceId\": null, \"faceRectangle\": {\"left\": null, \"top\": null, \"width\": null, \"height\": null}}]",

    "frame_3": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_3": "[{\"sceneTime\": 84, \"faceId\": \"64\", \"faceRectangle\": {\"left\": 518, \"top\": 166, \"width\": 128, \"height\": 179}}]",

    "frame_4": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_4": "[{\"sceneTime\": 86, \"faceId\": \"64\", \"faceRectangle\": {\"left\": 438, \"top\": 198, \"width\": 131, \"height\": 163}}]",

    "frame_5": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_5": "[{\"sceneTime\": 88, \"faceId\": \"45\", \"faceRectangle\": {\"left\": 267, \"top\": 119, \"width\": 149, \"height\": 180}}]",

    "frame_6": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_6": "[{\"sceneTime\": 90, \"faceId\": \"44\", \"faceRectangle\": {\"left\": 507, \"top\": 144, \"width\": 134, \"height\": 190}}]",

    "frame_7": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_7": "[{\"sceneTime\": 92, \"faceId\": \"64\", \"faceRectangle\": {\"left\": 499, \"top\": 150, \"width\": 134, \"height\": 192}}]",

    "frame_8": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_8": "[{\"sceneTime\": 94, \"faceId\": \"64\", \"faceRectangle\": {\"left\": 481, \"top\": 197, \"width\": 137, \"height\": 184}}]",

    "frame_9": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_9": "[{\"sceneTime\": 96, \"faceId\": \"64\", \"faceRectangle\": {\"left\": 478, \"top\": 183, \"width\": 136, \"height\": 187}}]",

    "frame_10": "/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAg...",

    "result_json_10": "[{\"sceneTime\": 98, \"faceId\": \"64\", \"faceRectangle\": {\"left\": 484, \"top\": 171, \"width\": 137, \"height\": 187}}]"

}
</pre>
                    </div>

                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.ftmenu-->
        <!--.ftexample-->
        <div class="demobox" id="ftexample">
            <p><em style="color:#f7778a;font-weight: 400;">Use Cases</em></p>
            <span class="sub">Find out how AI can be applied to diverse area.</span>
            <!-- 얼굴 추적 -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>Copyright Protection</span>
                            </dt>
                            <dd class="txt">Video recognition is used to track and identify a specific person to determine whether it is a copyrighted video.
                                <span><em class="fas fa-book-reader"></em> Reference: Korea Copyright Protection Agency</span>
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_ftr"><span>Face Tracking</span></li>
                                    <li class="ico_sr"><span>Subtitles Recognition</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>Criminal Tracking</span>
                            </dt>
                            <dd class="txt">Through face tracking in CCTV, it identifies the path and location of a specific criminal.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_ftr"><span> Face Tracking</span></li>
                                    <li class="ico_anor"><span>Anomaly Detection</span></li>
                                    <li class="ico_tr"><span>ESR</span></li>
                                    <li class="ico_fr"><span>Face Recognition</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>Find missing children</span>
                            </dt>
                            <dd class="txt">It can be used in certain areas through surveillance camera to provide services for finding lost children.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_ftr"><span>Face Tracking</span></li>
                                    <li class="ico_fr"><span>Face Recognition</span></li>
                                    <li class="ico_tr"><span>ESR</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!--얼굴 추적   -->
        </div>
        <!--//.ftexample-->


    </div>
    <!-- //.content -->
</div>

<script>

    var sample1SrcUrl;
    var sample1File;
    var ajaxXHR;

    //파일명 변경
    document.querySelector("#demoFile").addEventListener('change', function (ev) {

        $('.fl_box').on('click', function () {
            $(this).css("opacity", "1");
            $('.tr_1 .btn_area .disBox').remove();
        });

        //파일 용량 체크
        var demoFileInput = document.getElementById('demoFile');
        var demoFile = demoFileInput.files[0];
        var demoFileSize = demoFile.size;
        var max_demoFileSize = 1024 * 1024 * 50;//1kb는 1024바이트
        var demofile = document.querySelector("#demoFile");

        if (demoFileSize > max_demoFileSize || !demofile.files[0].type.match(/video.mp4/)) {
            $('.pop_simple').show();
            // 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
            $('.fl_box').css("opacity", "0.5");
            $('#demoFile').val('');
            $('.tr_1 .btn_area').append('<span class="disBox"></span>');

        } else {
            document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
            var element = document.getElementById('uploadFile');
            element.classList.remove('btn');
            element.classList.add('btn_change');
            $('.fl_box').css("opacity", "0.5");
            $('.tr_1 .btn_area .disBox').remove();

            $('#sample1').prop('checked', false);
        }

    });

    function sendApiRequest(file) {
        var formData = new FormData();
        formData.append('file', file);
        formData.append('${_csrf.parameterName}', '${_csrf.token}');

        //console.log(" @ Multipart Request start");

        ajaxXHR = $.ajax({
            type: "POST",
            async: true,
            url: "/api/faceTracking",
            data: formData,
            processData: false,
            contentType: false,
            success: function (result) {
                $('.tr_2').hide();
                $('.tr_3').fadeIn(300);

                //console.log(result);
                let resultData = JSON.parse(result);

                for(var i=1 ; i<=10 ; i++) {

                    // result image
                    if( typeof resultData["frame_" + i] !== 'undefined')
                        $('#resultImg' + i).attr('src', "data:image/jpeg;base64," + resultData["frame_" + i]);
                    else {
                        //$('#resultImg' + i).attr('src', "${pageContext.request.contextPath}/aiaas/common/images/img_faceTracking_no_result.png");
                        $('#imgBox' + i).css('background-color', '#FFFFFF');
                        $('#resultImg' + i).hide();
                    }

                    // result text (time, text)
                    if( typeof resultData["result_json_" + i] !== 'undefined') {
                        // result time
                        $('#resultTime' + i).html("SceneTime : " + JSON.parse(resultData["result_json_" + i])[0]["sceneTime"] + " s");

                        // result text (sceneTime, faceId, faceRectangle)
                        var responseString = JSON.stringify( JSON.parse(resultData["result_json_" + i]), undefined, 8);
                        var textAreaObj = document.getElementById('resultTxt' + i);
                        textAreaObj.scrollTop = textAreaObj.offsetHeight - 300;
                        textAreaObj.innerHTML = responseString;
                    } else {
                        $('#resultTime' + i).html("SceneTime : ");
                        $('#resultTxt' + i).html("Video finished.");
                    }
                }
            },
            error: function (jqXHR) {
                if (jqXHR.status === 0) {
                    return false;
                }
                alert("ERROR");
                window.location.reload();
            }
        });
    }

    function initResult() {

        for (var i = 1; i <= 10; i++) {
            $('#resultImg' + i).attr('src', '');
            $('#resultTxt' + i).text();
            $('#resultTime' + i).text();
        }
    }

    function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/common/video/faceTracking2.mp4");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function () {
            blob = xhr.response; //xhr.response is now a blob object
            sample1File = new File([blob], "sample1.mp4");
            sample1SrcUrl = URL.createObjectURL(blob);
        };
        xhr.send();
    }

    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {

            loadSample1();

            $('.btn_start').on('click', function () {
                //console.log(" @ faceTracking recogButton click");

                if ($('#sample1').prop('checked')) { // 샘플 파일로 해보기
                    // console.log("Sample file !! ");

                    var blob = sample1File;

                    var filename = Date.now() + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).toString(2, 15);
                    /*
                    console.log("blob");
                    console.log(blob);
                    console.log("type");
                    */
                    if (blob == null) {
                        alert("Video file loading. Please wait^^");
                        return;

                    } else {
                        console.log(blob.type);
                        filename += '.' + blob.type.split('/').pop();

                        var file = new File([blob], filename, {type: blob.type, lastModified: Date.now()});
                    }
                    // console.log("File size : " + file.size);
                } else { // 내 파일로 해보기
                    // console.log("Your local file !! ");

                    var blob = document.getElementById('demoFile').files[0];

                    var filename = Date.now() + Math.random().toString(36).substring(2, 15) + Math.random().toString(36).toString(2, 15);

                    if (blob == null) {
                        alert("Video file loading. Please wait^^");
                        return;

                    } else {
                        console.log(blob.type.split('/'));
                        filename += '.' + blob.type.split('/').pop();

                        var file = new File([blob], filename, {type: blob.type, lastModified: Date.now()});
                        // console.log("File size : " + file.size);
                    }
                }
                $('.tr_1').hide();
                $('.tr_2').fadeIn(300);
                sendApiRequest(file);
            });

            // close button
            $('em.close').on('click', function () {
                //console.log('업로드 취소');
                $('.fl_box').css("opacity", "1");
                $(this).parent().removeClass("btn_change");
                $(this).parent().addClass("btn");
                $(this).parent().children('.demolabel').text('Upload Video');
                $('#demoFile').val('');
                //파일명 변경
                document.querySelector("#demoFile").addEventListener('change', function (ev) {
                    document.querySelector(".demolabel").innerHTML = ev.target.files[0].name;
                    var element = document.getElementById('uploadFile');
                    element.classList.remove('btn');
                    element.classList.add('btn_change');
                });
            });

            $('.sample_box').on('click', function () {
                $('em.close').trigger('click');
                $('#sample1').prop('checked', true);
            });

            // 처음으로 버튼
            $('.btn_back1').on('click', function () {
                ajaxXHR.abort();
                // console.log('처음으로 버튼 1번');
                $('.tr_2').hide();
                $('.tr_1').fadeIn(300);
                $('.fl_box').css("opacity", "1");
                $('#sample1').trigger('click');
                $('#demoFile').val("");


            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                // console.log('처음으로 버튼 2번');
                $('.tr_3').hide();
                $('.tr_1').fadeIn(300);
                $('.fl_box').css("opacity", "1");
                $('#sample1').trigger('click');
                $('#demoFile').val("");

            });


            //알림 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });
        });
    });

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

</script>