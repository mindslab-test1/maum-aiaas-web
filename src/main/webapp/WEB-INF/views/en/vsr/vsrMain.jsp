<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2020-07-10
  Time: 17:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">Close</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>Upload error</h5>
            <p>Error occurred due to the image or file size. <br>Please check the restriction again.</p>
            <span>* Supported files: .mp4, .avi<br>
                  * Resolution 720p or less, length limited to 15 seconds or less<br>
                 * Note: the higher the fps the slower the processing speed.</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">OK</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents api_content">
    <!-- .content -->
    <div class="content">
        <h1 class="api_tit">VSR</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks" onclick="openTap(event, 'vsrdemo')" id="defaultOpen">
                <button type="button">AI Engine</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'vsrexample')">
                <button type="button">Use Case</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'vsrmenu')">
                <button type="button">Manual</button>
            </li>
            <%--            <li class="tablinks"><a href="/member/krApiAccount">API ID, key 발급</a></li>--%>
        </ul>
        <!-- .demobox -->
        <div class="demobox" id="vsrdemo">
            <p><span>Video Super Resolution</span></p>
            <span class="sub">Doubles the resolution of low-quality videos.</span>
            <!--vsr_box-->
            <div class="demo_layout faceTracking_box vsr_box">
                <!--tr_1-->
                <div class="tr_1">
                    <div class="fl_box">
                        <p><em class="far fa-file-video"></em><strong>Use Sample File</strong></p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="option" value="1" checked>
                                    <label for="sample1" class="">
                                        <video id="sampleVideo1" controls width="485" height="269" poster="${pageContext.request.contextPath}/aiaas/common/images/vsr_sample.png">
                                            <source src="${pageContext.request.contextPath}/aiaas/common/video/VSR_sample.mp4?ver=20200716"
                                                    type="video/mp4">
                                            IE 8 and below does not produce video. Please update the IE version.
                                        </video>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fr_box">
                        <p><em class="far fa-file-video"></em><strong>Use My File</strong></p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-video hidden"></em>
                                <label for="demoFile" class="demolabel">Upload Video</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".avi, .mp4, .mkv">
                            </div>
                            <ul>
                                <li>* Supported files: .mp4, .avi</li>
                                <li>* Resolution 720p or less, length limited to 15 seconds or less</li>
                                <li>* Note: the higher the fps the slower the processing speed.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start" id="sub">Generate</button>
                    </div>
                </div>
                <!--tr_1-->
                <!--tr_2-->
                <div class="tr_2">
                    <p><em class="far fa-file-video"></em>In progress</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px"
                             viewBox="0 0 128 16" xml:space="preserve"><path fill="#fcc6ce" fill-opacity="0.42"
                                                                             d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/>
                            <g>
                                <path fill="#f7778a" fill-opacity="1"
                                      d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/>
                                <animatetransform attributeName="transform" type="translate"
                                                  values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0"
                                                  calcMode="discrete" dur="1820ms" repeatCount="indefinite"/>
                            </g></svg>

                        <p>AI processing takes a while...</p>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
                    </div>

                </div>
                <!--tr_2-->
                <!--tr_3-->
                <div class="tr_3">

                    <div class="result_file">
                        <p><em class="far fa-file-video"></em>Input</p>
                        <div class="file_box">
                            <video id="inputVideo" controls width="485" height="269">
                                <source src="" type="video/mp4">
                                IE 8 and below does not produce video. Please update the IE version.
                            </video>
                        </div>
                        <p style="padding:17px 0 0 0;"><em class="far fa-file-video"></em>Result</p>
                        <div class="file_box">
                            <video id="outputVideo" controls width="485" height="269">
                                <source src="" type="video/mp4">
                                IE 8 and below does not produce video. Please update the IE version.
                            </video>
                            <a href="" id="resultDwn" class="dwn" download="vsr.mp4" ><em class="far fa-arrow-alt-circle-down"></em> Download Result</a>
                        </div>
                    </div>
                    <div class="btn_area">
                        <button type="button" class="btn_back2" id=""><em class="fas fa-redo"></em>Reset</button>
                    </div>
                </div>
                <!--tr_3-->
            </div>
            <!--//.vsr_box-->

        </div>
        <!-- //.demobox -->
        <!--.vsr-->
        <div class="demobox vision_menu" id="vsrmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">
                        API Guideline
                    </div>
                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1&#41; REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2&#41; It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID &amp; Key</p>
                    <p class="sub_txt">1&#41; You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2&#41; Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                    <p class="sub_txt">3&#41; After subscription, request an ID and key with required information filled in. <br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4&#41; After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                    <p class="sub_txt">5&#41; Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>
                <div class="guide_group">
                    <div class="title">
                        Video Super Resolution <small></small>
                    </div>
                    <p class="sub_txt">Doubles the resolution of low-quality videos.</p>

                    <span class="sub_title">
								Preparation
					</span>
                    <p class="sub_txt">- Input: Low-quality video </p>
                    <ul>
                        <li>File type: .mp4, .avi </li>
                        <li>Resolution: Under 720p </li>
                        <li>Length: Less than 15 seconds </li>
                        <li>Note: the higher the fps the slower the processing speed </li>
                    </ul>
                    <span class="sub_title">
								API Document
							</span>
                    <p class="sub_txt">① Request  </p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/vsr/download</li>
                    </ul>
                    <p class="sub_txt">② Request parameters </p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId </td>
                            <td>Unique API ID. Request from is required for the ID. </td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey </td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>file</td>
                            <td>Input file</td>
                            <td>file</td>
                        </tr>
                    </table>
                    <p class="sub_txt">③ Request example </p>
                    <div class="code_box">
                        curl --location --request POST 'http://api.maum.ai/vsr/download' \<br>
                        --form 'apiId= {Own API Id}'<br>
                        --form 'apiKey= {Own API Key}'<br>
                        --form 'file= {file  path}'
                    </div>

                    <p class="sub_txt">④ Response example</p>

                    <div class="code_box">
                        (.mp4 File Download)
                    </div>
                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.vsrmenu-->
        <!--.vsrexample-->
        <div class="demobox" id="vsrexample">
            <p><em style="color:#f7778a;font-weight: 400;">Use Cases</em> </p>
            <span class="sub">Find out how AI can be applied to diverse area.</span>
            <!-- vsr -->
            <div class="useCasesBox">
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>CCTV Detection</span>
                            </dt>
                            <dd class="txt">Identify specific individuals in the CCTV footage by enlarging small pictures of them with less image loss. In addition to CCTV, it can also be used to enlarge small images of people in video or photos.</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_tr"><span>ESR</span></li>
                                    <li class="ico_avr"><span>AVR</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 02</em>
                                <span>Video Restoration</span>
                            </dt>
                            <dd class="txt">It can be utilized to restore old  or low-quality videos. Use deep learning technology to restore low resolution videos.
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_den"><span>Denoise</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 03</em>
                                <span>Automatic Document Creation</span>
                            </dt>
                            <dd class="txt">Utilizing OCR, document texts are corrected to show higher accuracy.
                            </dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_ftr"><span>Face Tracking</span></li>
                                    <li class="ico_anor"><span>Anomaly Detection</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
            <!--vsr  -->
        </div>
        <!--//.vsrexample-->
    </div>
    <!-- //.content -->
</div>

<script>

    var sample1SrcUrl;
    var sample1File;
    var ajaxXHR;


    function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "${pageContext.request.contextPath}/aiaas/common/video/VSR_sample.mp4");
        xhr.responseType = "blob";//force the HTTP response, response-type header to be blob
        xhr.onload = function () {
            blob = xhr.response; //xhr.response is now a blob object
            sample1File = new File([blob], "sample1.mp4", {type:"video/mp4"});
            sample1SrcUrl = URL.createObjectURL(blob);
        };
        xhr.send();
    }



    jQuery.event.add(window, "load", function () {
        $(document).ready(function () {

            loadSample1();

            //파일명 변경
            document.querySelector("#demoFile").addEventListener('change', function (ev) {

                let demoFile = ev.target.files[0];

                if(demoFile == null) return; // x 를 눌러 업로드할 파일을 지운 경우

                //파일 용량 체크
                var max_demoFileSize = 1024 * 1024 * 15;//1kb는 1024바이트


                if (!demoFile.type.match(/video.mp4/) && !demoFile.type.match(/video.avi/) && !demoFile.type.match(/video.mkv/)) {
                    $('.pop_simple').show();
                    $('#demoFile').val(null);

                } else {

                    let video = document.createElement('video');
                    video.preload = 'metadata';
                    video.onloadedmetadata = function() {
                        window.URL.revokeObjectURL(video.src);
                        let dur =  video.duration;
                        console.log (dur);

                        if (dur > 15) {
                            console.log("Videos longer than 15 seconds cannot be uploaded.");
                            $('.pop_simple').show();
                            $('#demoFile').val('');

                        } else {
                            document.querySelector(".demolabel").innerHTML = demoFile.name;
                            $('#uploadFile').removeClass('btn').addClass('btn_change');
                            $('.fl_box').css("opacity", "0.5");
                            $('#sample1').prop('checked', false);
                        }
                    };
                    video.src = URL.createObjectURL(demoFile);
                    const delete1 = video.delete;
                    // console.log(delete1);
                }
            });


            // close button
            $('em.close').on('click', function () {
                //console.log('업로드 취소');
                $('.fl_box').css("opacity", "1");
                $(this).parent().removeClass("btn_change").addClass("btn");
                $(this).parent().children('.demolabel').text('Upload Video');
                $('#demoFile').val('');
                $('#sample1').prop('checked', true);

            });

            $('.fl_box').on('click', function () {
                $(this).css("opacity", "1");
            });


            $('.sample_box').on('click', function () {
                $('em.close').trigger('click');
                $('#sample1').prop('checked', true);
            });


            // 처음으로 버튼
            $('.btn_back1').on('click', function () {
                ajaxXHR.abort();
                $('.tr_2').hide();
                $('.tr_1').fadeIn(300);
            });

            // step3->step1
            $('.btn_back2').on('click', function () {
                $('#inputVideo').get(0).pause();
                $('#outputVideo').get(0).pause();
                $('.tr_3').hide();
                $('.tr_1').fadeIn(300);
                $('.fl_box').css("opacity", "1");
                $('#sample1').trigger('click');
                $('#demoFile').val('');
            });


            //알림 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({
                    'overflow': '',
                });
            });


            $('.btn_start').on('click', function () {

                let inputFile;
                let inputFileUrl;

                if ($('#sample1').prop('checked')) { // 샘플 파일로 해보기

                    let blob;
                    blob = sample1File;
                    inputFileUrl = sample1SrcUrl;

                    if (blob == null) {
                        alert("The video file is being loaded. Please try again in a few minutes.");
                        return;

                    } else {
                        // filename += '.' + file.type.split('/').pop();
                        inputFile = sample1File;
                    }

                } else { // 내 파일로 해보기
                    inputFile = $('#demoFile').get(0).files[0];

                    if(inputFile == null || inputFile == ""){
                        alert("Please select a sample or upload a video!");
                        return;
                    }

                    inputFileUrl = URL.createObjectURL(inputFile);
                    console.log(typeof(inputFile));
                }

                $('.tr_1').hide();
                $('.tr_2').fadeIn(300);
                $('#inputVideo').attr('src', inputFileUrl);

                sendApiRequest(inputFile);
            });
        });
    });


    function sendApiRequest(file) {

        var formData = new FormData();
        formData.append('file', file);
        formData.append('${_csrf.parameterName}', '${_csrf.token}');

        ajaxXHR = new XMLHttpRequest();
        ajaxXHR.responseType ="blob";
        ajaxXHR.onreadystatechange = function(){
            if (ajaxXHR.readyState === 4 && ajaxXHR.status === 200){

                let resultBlob = new Blob([ajaxXHR.response], {type: 'video/mp4'});
                let srcUrl = URL.createObjectURL(resultBlob);
                $('#outputVideo').attr('src', srcUrl);
                $('#resultDwn').attr('href', srcUrl);

                $('.tr_2').hide();
                $('.tr_3').fadeIn(300);
                $('.vsr_box').css('border','none');

                $('#inputVideo').get(0).play();
                $('#outputVideo').get(0).play();
            }
        };

        ajaxXHR.open('POST', '/api/vsr');
        ajaxXHR.send(formData);

        ajaxXHR.timeout = 90000;
        ajaxXHR.ontimeout = function() {
            alert("timeout...!");
            window.location.reload();
        };

        ajaxXHR.addEventListener("abort", function(){
            // $('.tr_3').hide();
            // $('.tr_1').fadeIn(300);
        });
    }

    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();

</script>