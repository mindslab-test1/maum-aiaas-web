<%--
  Created by IntelliJ IDEA.
  User: YGE
  Date: 2021-06-23
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- 5 .pop_simple -->
<div class="pop_simple">
    <div class="pop_bg"></div>
    <!-- .popWrap -->
    <div class="popWrap pop_sr_noti">
        <button class="pop_close" type="button">닫기</button>
        <!-- .pop_bd -->
        <div class="pop_bd">
            <em class="fas fa-sad-cry"></em>
            <h5>Upload error</h5>
            <p>Error occurred due to filename extension or file size. <br>Please check the restriction again.</p>
            <span> * Supported file: .png<br>* Image file size under 2MB.</span>

        </div>
        <!-- //.pop_bd -->
        <div class="btn">
            <a class="">OK</a>
        </div>
    </div>
    <!-- //.popWrap -->
</div>
<!-- //.pop_simple -->

<!-- .contents -->
<div class="contents api_content">
    <!-- .content -->
    <div class="content">
        <h1 class="api_tit">Holistic 3D</h1>
        <ul class="menu_lst vision_lst">
            <li class="tablinks active" onclick="openTap(event, 'hlstdemo')" id="defaultOpen">
                <button type="button">AI Engine</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'hlstexample')">
                <button type="button">Use Case</button>
            </li>
            <li class="tablinks" onclick="openTap(event, 'hlstmenu')">
                <button type="button">Manual</button>
            </li>
        </ul>
        <!-- .demobox -->
        <div class="demobox" id="hlstdemo">
            <p><span>Holistic 3D</span></p>
            <span class="sub">3D Model Synthesis from a 360 degree view panoramic image.</span>
            <!--hlst3D_box-->
            <!-- [D] 결과화면(tr_3)으로 넘어갈 때는 .hlst3D_box에 addClass('result_ver')해줘야하고, 다시 초기 화면으로 돌아갈 경우 removeClass 해줘야 합니다. -->
            <div class="demo_layout hlst3D_box">
                <!--tr_1-->
                <div class="tr_1">
                    <div class="fl_box">
                        <p><em class="far fa-file-image"></em><strong>Use Sample File</strong></p>
                        <div class="sample_box">
                            <div class="sample_1">
                                <div class="radio">
                                    <input type="radio" id="sample1" name="option" value="1" checked>
                                    <label for="sample1" class="">
                                        <img src="${pageContext.request.contextPath}/aiaas/common/images/img_hlst_sample.png" alt="sample image">
                                    </label>
                                </div>
                            </div>
                            <div class="source_info">source: https://pixexid.com</div>
                        </div>
                    </div>

                    <div class="fr_box">
                        <p><em class="far fa-file-image"></em><strong>Use My File</strong></p>
                        <div class="uplode_box">
                            <div class="btn" id="uploadFile">
                                <em class="fas fa-times hidden close"></em>
                                <em class="far fa-file-image hidden"></em>
                                <label for="demoFile" class="demolabel">Upload File</label>
                                <input type="file" id="demoFile" class="demoFile" accept=".png">
                            </div>
                            <ul>
                                <li>* Supported extensions: .png</li>
                                <li>* Images under 2 MB.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_start">Process</button>
                    </div>
                </div>
                <!--tr_1-->

                <!--tr_2-->
                <div class="tr_2">
                    <p><em class="far fa-file-image"></em>In progress</p>
                    <div class="loding_box ">
                        <svg xmlns:svg="http://www.w3.org/2000/svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.0" width="144px" height="18px" viewBox="0 0 128 16" xml:space="preserve">
									<path fill="#fcc6ce" fill-opacity="0.42" d="M6.4,4.8A3.2,3.2,0,1,1,3.2,8,3.2,3.2,0,0,1,6.4,4.8Zm12.8,0A3.2,3.2,0,1,1,16,8,3.2,3.2,0,0,1,19.2,4.8ZM32,4.8A3.2,3.2,0,1,1,28.8,8,3.2,3.2,0,0,1,32,4.8Zm12.8,0A3.2,3.2,0,1,1,41.6,8,3.2,3.2,0,0,1,44.8,4.8Zm12.8,0A3.2,3.2,0,1,1,54.4,8,3.2,3.2,0,0,1,57.6,4.8Zm12.8,0A3.2,3.2,0,1,1,67.2,8,3.2,3.2,0,0,1,70.4,4.8Zm12.8,0A3.2,3.2,0,1,1,80,8,3.2,3.2,0,0,1,83.2,4.8ZM96,4.8A3.2,3.2,0,1,1,92.8,8,3.2,3.2,0,0,1,96,4.8Zm12.8,0A3.2,3.2,0,1,1,105.6,8,3.2,3.2,0,0,1,108.8,4.8Zm12.8,0A3.2,3.2,0,1,1,118.4,8,3.2,3.2,0,0,1,121.6,4.8Z"/>
                            <g>
                                <path fill="#f7778a" fill-opacity="1" d="M-42.7,3.84A4.16,4.16,0,0,1-38.54,8a4.16,4.16,0,0,1-4.16,4.16A4.16,4.16,0,0,1-46.86,8,4.16,4.16,0,0,1-42.7,3.84Zm12.8-.64A4.8,4.8,0,0,1-25.1,8a4.8,4.8,0,0,1-4.8,4.8A4.8,4.8,0,0,1-34.7,8,4.8,4.8,0,0,1-29.9,3.2Zm12.8-.64A5.44,5.44,0,0,1-11.66,8a5.44,5.44,0,0,1-5.44,5.44A5.44,5.44,0,0,1-22.54,8,5.44,5.44,0,0,1-17.1,2.56Z"/>
                                <animatetransform attributeName="transform" type="translate" values="23 0;36 0;49 0;62 0;74.5 0;87.5 0;100 0;113 0;125.5 0;138.5 0;151.5 0;164.5 0;178 0" calcMode="discrete" dur="1820ms" repeatCount="indefinite"/>
                            </g>
								</svg>

                        <p>AI processing takes a while...(about 10 seconds)</p>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_back1"><em class="fas fa-redo"></em>Reset</button>
                    </div>
                </div>
                <!--tr_2-->

                <!--tr_3-->
                <!-- [D] 결과화면(tr_3)으로 넘어갈 때는 .hlst3D_box에 addClass('result_ver')해줘야하고, 다시 초기 화면으로 돌아갈 경우 removeClass 해줘야 합니다. -->
                <!-- [D] addClass('input_sample')하면 샘플파일 결과화면이고, removeClass('input_sample')하면 내 파일 결과화면입니다. -->
                <div class="tr_3 input_sample">
                    <p><em class="fas fa-cube"></em>Result</p>

                    <div class="result_file">
                        <div id="pcdBox" class="imgBox">

                        </div>
                        <div class="btnBox">
                            <a id="saveResult" class="btn_result_download"><em class="far fa-arrow-alt-circle-down"></em>Download Result</a>
                        </div>
                        <div class="source_info">
                            <p>* You can zoom in/out the screen using the mouse wheel.</p>
                            <p>* You can view from various angles by dragging the mouse.</p>
                        </div>
                    </div>

                    <div class="btn_area">
                        <button type="button" class="btn_reset"><em class="fas fa-redo"></em>Reset</button>
                    </div>
                </div>
                <!--tr_3-->
            </div>
            <!--//.hlst3D_box-->


            <div class="remark">
                * The resulting file is supported as a <strong>.pcd</strong> file.
            </div>
        </div>
        <!-- //.demobox -->


        <!--.afmenu-->
        <div class="demobox vision_menu" id="hlstmenu">
            <!--guide_box-->
            <div class="guide_box">
                <div class="guide_common">
                    <div class="title">API Guideline</div>

                    <p class="sub_title">Set up Environment</p>
                    <p class="sub_txt">1) REST API is available wherever you can send HTTP requests.</p>
                    <p class="sub_txt">2) It can be used in Python, Web, Javascript and Java.</p>
                    <p class="sub_title">ID &amp; Key</p>
                    <p class="sub_txt">1) You need a unique ID and key for Minds Lab’s API services.</p>
                    <p class="sub_txt">2) Subscribe ‘Business Plan’ or ‘Enterprise Plan’ on maum.ai platform. (https://maum.ai)</p>
                    <p class="sub_txt">3) After subscription, request an ID and key with required information filled in.<br>
                        <small>(ex. Service description, Requesting API, Company info. etc.)</small></p>
                    <p class="sub_txt">4) After an agreement, Minds Lab would send you an ID, key within 1-2 business days through email.</p>
                    <p class="sub_txt">5) Remember the ID and key and follow the instruction below.</p>
                    <p class="sub_txt">※ Disclosure is prohibited strictly for Minds Lab’s API ID and key.</p>
                </div>
                <div class="guide_group">
                    <div class="title">Holistic 3D</div>
                    <p class="sub_txt">3D Model Synthesis from a 360 degree view panoramic image.</p>

                    <span class="sub_title">Preparation</span>
                    <p class="sub_txt">- Input: image file</p>
                    <ul>
                        <li>File type: .png</li>
                        <li>Size: Under 2MB</li>
                    </ul>

                    <span class="sub_title">API Document</span>
                    <em>Upload : Make a creation request through pcd 3d model creation request, and receive a request key to know the processing status.</em>
                    <p class="sub_txt">① Request</p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL :  https://api.maum.ai/holistic3d/upload</li>
                    </ul>

                    <p class="sub_txt">② Request parameters</p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId</td>
                            <td>Unique API  ID. Request from is required for the ID.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey</td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>image</td>
                            <td>type:file (.png) image file</td>
                            <td>file</td>
                        </tr>
                    </table>

                    <p class="sub_txt">③ Request example</p>
                    <div class="code_box">
<pre>
curl -X POST 'https://api.maum.ai/holistic3d/upload' \
-H 'Content-Type: multipart/form-data' \
-F 'apiId= Own API Id' \
-F 'apiKey= Own API KEY' \
-F 'image= Input image file' \
</pre>
                    </div>

                    <p class="sub_txt">④ Response parameters</p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>API operation</td>
                            <td>object</td>
                        </tr>
                        <tr>
                            <td>payload</td>
                            <td>result </td>
                            <td>object</td>
                        </tr>
                    </table>

                    <span class="table_tit">message: API operation</span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>String describing the request processing status (Success/Fail)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>Status code for request processing status (0: Success)</td>
                            <td>int</td>
                        </tr>
                    </table>

                    <span class="table_tit">payload: result</span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>
                                follow status code of mmsr<br>
                                status(1): message -&gt; file_key<br>
                                status(2): upload error
                            </td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>fileKey</td>
                            <td>Unique Key for Request Identification</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>msg</td>
                            <td>msg for request</td>
                            <td>string</td>
                        </tr>
                    </table>

                    <p class="sub_txt">⑤ Response example</p>
                    <div class="code_box">
<pre>
{
	"message": {
		"message": "Success",
		"status": 0
	},
	"payload": {
		"status": "1"
		"fileKey": "369f5394-dca7-4356-813e-60ae9a74be80"
		"msg": "success"
	}
}
</pre>
                    </div>

                    <em>Check process : Check the processing status of the request.</em>
                    <p class="sub_txt">① Request</p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/holistic3d/check:process</li>
                    </ul>

                    <p class="sub_txt">② Request parameters</p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId</td>
                            <td>Unique API  ID. Request from is required for the ID.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey</td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>fileKey</td>
                            <td>Unique Key for Request Identification</td>
                            <td>string</td>
                        </tr>
                    </table>

                    <p class="sub_txt">③ Request example</p>
                    <div class="code_box">
<pre>
curl -X POST 'https://api.maum.ai/holistic3d/check:process' \
    -H 'Content-Type: application/json' \
    -d '{
        "apiId": "Own API ID",
        "apiKey": "Own API KEY",
        "fileKey": "Unique Key for Request Identification"
    }'
</pre>
                    </div>

                    <p class="sub_txt">④ Response parameters</p>
                    <span class="table_tit">Response</span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>API operation</td>
                            <td>object</td>
                        </tr>
                        <tr>
                            <td>payload</td>
                            <td>result</td>
                            <td>object</td>
                        </tr>
                    </table>

                    <span class="table_tit">message: API operation</span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>message</td>
                            <td>String describing the request processing status (Success/Fail)</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>Status code for request processing status (0: Success)</td>
                            <td>int</td>
                        </tr>
                    </table>

                    <span class="table_tit">payload: result</span>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>status</td>
                            <td>
                                NOT_YET = 6;<br>
                                PROCESSING = 7;<br>
                                DONE = 8;<br>
                                WRONG_KEY = 10;<br>
                                ERROR = 11;<br>
                                DELETED = 9;
                            </td>
                            <td>int</td>
                        </tr>
                        <tr>
                            <td>msg</td>
                            <td>Phrases according to status code</td>
                            <td>string</td>
                        </tr>
                    </table>

                    <p class="sub_txt">⑤ Response example</p>
                    <div class="code_box">
<pre>
{
	"message": {
		"message": "Success",
		"status": 0
	},
	"payload": {
		"status": 8,
		"msg": "Process Completed(can download)"
	}
}
</pre>
                    </div>

                    <em>Download: 처리 완료된 3D .pcd 파일을 다운로드 받습니다.</em>
                    <p class="sub_txt">① Request</p>
                    <ul>
                        <li>Method : POST</li>
                        <li>URL : https://api.maum.ai/holistic3d/download</li>
                    </ul>

                    <p class="sub_txt">② Request 파라미터 설명</p>
                    <table>
                        <tr>
                            <th>Name</th>
                            <th>Description</th>
                            <th>type</th>
                        </tr>
                        <tr>
                            <td>apiId</td>
                            <td>Unique API  ID. Request from is required for the ID.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>apiKey</td>
                            <td>Unique API key. Request from is required for the key.</td>
                            <td>string</td>
                        </tr>
                        <tr>
                            <td>fileKey</td>
                            <td>Unique Key for Request Identification</td>
                            <td>string</td>
                        </tr>
                    </table>

                    <p class="sub_txt">③ Request example</p>
                    <div class="code_box">
<pre>
curl -X POST 'https://api.maum.ai/holistic3d/download' \
-H 'Content-Type: application/json' \
-d '{
    "apiId": "Own API ID",
    "apiKey": "Own API KEY",
    "fileKey": "Unique Key for Request Identification"
}'
</pre>
                    </div>

                    <p class="sub_txt">④ Response example</p>
                    <div class="code_box">
<pre>
Content-Type: application/octet-stream
(.pcd File Download)
</pre>
                    </div>
                </div>
            </div>
            <!--//.guide_box-->
        </div>
        <!--.afmenu-->

        <!--.afexample-->
        <div class="demobox" id="hlstexample">
            <p><em style="color:#f7778a;font-weight: 400;">Use Cases</em></p>
            <span class="sub">Find out how AI can be applied to diverse areas.</span>
            <div class="useCasesBox">
                <!-- 3D 인테리어 -->
                <ul class="lst_useCases">
                    <li>
                        <dl>
                            <dt>
                                <em>CASE 01</em>
                                <span>3D Interior Modeling</span>
                            </dt>
                            <dd class="txt">You can experience a virtually modeled space from the 3D Interior view</dd>
                            <dd class="api_itemBox">
                                <ul class="lst_api">
                                    <li class="ico_hlst3D"><span>Holistic 3D</span></li>
                                </ul>
                            </dd>
                        </dl>
                    </li>
                </ul>
                <!-- 3D 인테리어 -->
            </div>
        </div>
        <!--//.afexample-->
    </div>
</div>

<script type="module">

    import {init, animate} from "${pageContext.request.contextPath}/aiaas/common/js/holistic3d/pcd/index.module.js";

    var sampleImage1;

    let request = null;
    let timerId = null;
    let apiUrl = "${apiUrl}";
    let resultObjUrl = "";

    jQuery.event.add(window,"load",function(){
        $(document).ready(function (){

            loadSample1();
            document.getElementById("defaultOpen").click();


            // 샘플 클릭시 업로드된 파일 제거
            $('.radio input, .radio img').on('click',function(){
                $('em.close').click();
                $('#sample1').prop('checked', true);
            });

            //파일명 변경
            document.querySelector("#demoFile").addEventListener('change', function () {
                let demoFileInput = document.getElementById('demoFile');
                let demoFile = demoFileInput.files[0];
                let demoFileSize = demoFile.size;
                let max_demoFileSize = 1024 * 1024 * 2; //1kb는 1024바이트
                //파일 용량, 확장자 체크
                if(demoFileSize > max_demoFileSize || !demoFile.type.match(/image.png/)){
                    $('.pop_simple').show();
                    $('#demoFile').val('');
                } else {
                    $('.demolabel').html(demoFile.name);
                    $('#uploadFile').removeClass('btn');
                    $('#uploadFile').addClass('btn_change');
                    $('.fl_box').css("opacity", "0.5");
                    $('#sample1').prop('checked', false);
                }
            });


            // 업로드된 파일명박스 제거
            $('em.close').on('click', function () {
                $('.fl_box').css("opacity", "1");
                $('#uploadFile').removeClass("btn_change");
                $('#uploadFile').addClass("btn");
                $('.demolabel').text('Upload File');
                $('#demoFile').val("");
                $('#sample1').prop('checked', true);
            });


            // 팝업창 닫기
            $('.pop_close, .pop_bg, .btn a').on('click', function () {
                $('.pop_simple').fadeOut(300);
                $('body').css({ 'overflow': '', });
            });


            $('.btn_start').on('click', function(){
                let imgSrc;
                let $demoFile = $("#demoFile");

                if ($demoFile.val() === "" || $demoFile.val() === null) {
                    imgSrc = sampleImage1;
                } else {
                    const blob = document.getElementById('demoFile').files[0];
                    let filename = Date.now() + Math.random().toString(36).substring(2, 15);
                    filename += '.' + blob.type.split('/').pop();
                    imgSrc = new File([blob], filename, {type : "image/png", lastModified: Date.now()});
                }

                requestUpload(imgSrc);

                $('.remark').hide();
                $('.tr_1').hide();
                $('.tr_2').show();
            });

            $('.btn_back1').on('click', function(){
                if(timerId != null) clearTimeout(timerId);
                if(request) request.abort();

                $('.remark').show();
                $('.tr_2').hide();
                $('.tr_1').show();
            });

            $('.btn_reset').on('click', function(){
                URL.revokeObjectURL(resultObjUrl);
                $('#pcdBox').children().remove();

                $('.hlst3D_box').removeClass('result_ver');
                $('.remark').show();
                $('.tr_3').hide();
                $('.tr_1').show();
            });


        });
    });


    function requestUpload(image){
        console.log("image :" + image.size);

        let formData = new FormData();
        formData.append('image', image);
        formData.append('${_csrf.parameterName}', '${_csrf.token}');

        request = new XMLHttpRequest();
        request.onreadystatechange = function() {
            if(request)
                if(request.readyState === 4 && request.status === 200) {
                    if(!request.response){
                        handleError("Empty response : upload api");
                        return;
                    }

                    let response = JSON.parse(request.response);
                    if(response.message.status !== 0){
                        handleError("Upload API fail : " + response.message.message);
                        return;
                    }

                    let fileKey = response.payload.fileKey;
                    callBackUpload(fileKey);
                }
        };
        request.open('POST', '/api/holistic3d/upload');
        request.send(formData);
        request.timeout = 60000;
        request.ontimeout = function() { handleError("Timeout error : upload api"); }
        request.onabort = function(){ }
    }


    /** statusCheck API의 STATUS 값에 따라 분기 처리
     *  6 : 대기중 - 대기중 -> setTimeout으로 다시 api call
     *  7 : 처리중 - 대기가 끝,api 처리를 기다리는 중 -> setTimeout으로 다시 api call
     *  8 : 완료  - handleSuccess()
     *  9 : File is deleted(old file key)
     * 10 : Wrong key
     * 11 : File Processing Error
     */
    function callBackUpload(fileKey){

        console.log("%c callBackUpload()",'color:green');

        timerId = setTimeout(function delayFunc(){

            getStatusPromise(fileKey).then((res) => {
                console.log("%c getStatusPromise() -> then(%s)",'color:green', fileKey);
                if(res){
                    let payload = res;
                    console.log("STATUS : ", payload.status, payload.msg);
                    switch (payload.status){
                        case 6:
                            timerId = setTimeout(delayFunc, 1000);
                            break;
                        case 7:
                            timerId = setTimeout(delayFunc, 1000);
                            break;
                        case 8:
                            handleSuccess(fileKey);
                            break;
                        default:
                            handleError("statusCheck message : " + payload.message);
                            return;
                    }
                }else{
                    handleError("Empty response : statusCheck api");
                }

            })
                .catch((errMsg) => {
                    handleError(errMsg);
                });
        }, 0);
    }


    // 상태 확인 api
    function getStatusPromise(fileKey){
        let reqJson = {
            "apiId" : "${fn:escapeXml(sessionScope.accessUser.apiId)}",
            "apiKey": "${fn:escapeXml(sessionScope.accessUser.apiKey)}",
            "fileKey": fileKey
        }

        return new Promise((resolve, reject) => {
            request = new XMLHttpRequest();
            request.onreadystatechange = function(){
                if(request.readyState === XMLHttpRequest.DONE){
                    if(request.status === 200 ){
                        resolve(JSON.parse(request.response).payload);
                    }else{
                        console.log(request.response);
                        reject('Error: in call getStatus with response status - ' + request.status);
                    }
                }
            };
            request.open('POST', apiUrl + '/holistic3d/check:process');
            request.setRequestHeader("Content-Type", "application/json");
            request.send(JSON.stringify(reqJson));
            request.timeout = 60000;
            request.onerror = () => {
                reject('Error\n'
                    + "status : " + request.status
                    + "\n message : " + request.response);
            }
            request.onabort = function(){ }
        });
    }


    function handleSuccess(fileKey){

        console.log("%c handleSuccess() ",'color:blue');

        getOutputPromise(fileKey)
            .then((res)=>{
                let result = new Blob([res], {type: 'image/pcd'});
                console.log(res);
                resultObjUrl = URL.createObjectURL(result);

                $('#saveResult').get(0).href = resultObjUrl;
                $('#saveResult').get(0).download = "holistic3d.pcd";

                $('.tr_2').hide();
                $('.tr_3').show();
                $('.hlst3D_box').addClass('result_ver');

                init(resultObjUrl);
                animate();

            })
            .catch((errMsg)=>{
                handleError(errMsg);
            });
    }


    function handleError(errorMsg){
        alert(errorMsg);
        window.location.reload();
    }


    // 결과 요청 api
    function getOutputPromise(fileKey){

        let reqJson = {
            "apiId" : "${fn:escapeXml(sessionScope.accessUser.apiId)}",
            "apiKey": "${fn:escapeXml(sessionScope.accessUser.apiKey)}",
            "fileKey": fileKey
        }

        return new Promise((resolve, reject) => {
            request = new XMLHttpRequest();
            request.responseType = 'blob';
            request.onreadystatechange = function(){
                if(request.readyState === XMLHttpRequest.DONE){
                    if(request.status === 200 ){
                        request.response == null
                            ? reject("Empty response : output download api")
                            : resolve(request.response);
                    }
                    else{
                        reject('Error: ' + request.status);
                    }
                }
            };
            request.open('POST', apiUrl + '/holistic3d/download');
            request.setRequestHeader("Content-Type", "application/json");
            request.send(JSON.stringify(reqJson));
            request.timeout = 60000;
            request.onerror = () => {
                reject('Error\n'
                    + "status : " + request.status
                    + "\n message : " + request.response);
            }
            request.onabort = function(){
                console.error("abort : getDownload api");
            }
        });
    }


    //샘플
    function loadSample1() {
        var blob = null;
        var xhr = new XMLHttpRequest();
        xhr.open("GET", "/aiaas/common/images/img_hlst_sample.png");
        xhr.responseType = "blob";
        xhr.onload = function()
        {
            blob = xhr.response;
            sampleImage1 = new File([blob], "img_hlst_sample.png", {type : "image/png"});
        };

        xhr.send();
    }

</script>

<script type="text/javascript">
    //API 탭
    function openTap(evt, menu) {
        var i, demobox, tablinks;
        demobox = document.getElementsByClassName("demobox");
        for (i = 0; i < demobox.length; i++) {
            demobox[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(menu).style.display = "block";
        evt.currentTarget.className += " active";
    }

</script>