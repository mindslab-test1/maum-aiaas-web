<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2019-09-19
  Time: 16:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
    <div class="content">
        <h1>Data available for purchase</h1>
        <div class="demobox data_demobox">
            <p><span>Need a data set?</span></p>
            <span class="sub">Datasets are available to build, train, and deploy AI engine models.<br>
Please contact our data team to learn more. Contact: <em>maumdata@mindslab.ai</em></span>
            <div class="data_box">
                <ul>
                    <li>
                        <h3>Standard Script for <br>TTS Recording </h3>
                        <p>Basic dataset for converting texts into a spoken language. A standard recording script with various emotional expressions to make the AI TTS more natural. Build your own AI TTS model right now with our standard script and accumulated specialties in TTS.</p>
                    </li>
                    <li>
                        <h3>Agent’s recording file</h3>
                        <p>An actual recording file of a female agent of Minds Lab call center. Not only it is a generic call center script, but also it is recorded according to the actual agent's speech pattern. It is a calm and stable tone. 3 hours of audio files and the scripts are also provided.</p>
                    </li>
                    <li>
                        <h3>MRC data </h3>
                        <p>It contains 250,000 texts and question pairing data and 100,000 news data sets for an accurate AI machine reading comprehension (MRC). In addition to MRC, it can be used to develop and train various language intelligence AI engines.</p>
                    </li>

                </ul>
                <div class="etc_box">And more +</div>
            </div>


        </div>

    </div>
    <!-- //.content -->
</div>
<!-- //.contents -->