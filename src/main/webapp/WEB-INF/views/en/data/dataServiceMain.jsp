<%--
  Created by IntelliJ IDEA.
  User: miryoung
  Date: 2019-09-19
  Time: 16:06
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<!-- .contents -->
<div class="contents">
    <div class="content">
        <h1>Data Refinery Services</h1>
        <div class="demobox data_demobox">
            <p><span>Want to utilize the data you already have?</span></p>
            <span class="sub">Professional AI experts in Minds Lab provide a precise consultation to create a personalized AI engine via your data. <br>The service could be proceeded even with a small amount of data. Contact: <em>maumdata@mindslab.ai</em></span>
            <div class="data_box">
                <ul class="dt_service">
                    <li>
                        <h3>Close consultation with<br>professional AI experts</h3>
                        <p>Want to use AI, but don't know what data you need? Our AI consultants give a guidance for refining and processing your data. Please contact us now.</p>
                    </li>
                    <li>
                        <h3>Knowledge of AI specialists <br>who are involved in engine R&D </h3>
                        <p>From algorithms to data, Minds Lab has enhanced an AI engine technology. We obtain technological expertise and facilities that our competitors could not catch up. Also, you can try a data cleansing service from researchers who are directly involved in AI engine development.</p>
                    </li>
                    <li>
                        <h3>AI data service has <br>already proven in the market </h3>
                        <p>The data sets we have built are over 100 AI projects, which is one of the largest size in the industry. We could provide the service on your own purpose.</p>
                    </li>

                </ul>

            </div>


        </div>


    </div>
    <!-- //.content -->
</div>
<!-- //.contents -->